package com.apptechnik.appinabox.android.dao

import androidx.room.Dao
import androidx.room.OnConflictStrategy
import androidx.room.RawQuery
import androidx.room.Update
import androidx.room.Insert
import androidx.room.Delete
import androidx.sqlite.db.SimpleSQLiteQuery

@Dao
abstract class CoreDao<T> {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(t: T): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(list: List<T>): Long

    @Update
    abstract fun update(t: T): Int

    @Update
    abstract fun update(list: List<T>): Int

    @Delete
    abstract fun delete(t: T): Int

    @Delete
    abstract fun delete(list: List<T>): Int

    @RawQuery
    abstract fun count(query: SimpleSQLiteQuery): Int
}