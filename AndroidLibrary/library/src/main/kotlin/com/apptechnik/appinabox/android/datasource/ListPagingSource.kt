package com.apptechnik.appinabox.android.repository
import androidx.paging.PagingSource

import androidx.paging.PagingState

/**
 * Simple paging source which takes a fixed list for data.
 */
class ListPagingSource<T>(var items: List<T>) : PagingSource<Int, T>()  where T : Any  {

    /**
     * Load doesn't pay attention to the source data size, so bound the range to the list size.
     */
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, T> {
        params?.key?.let { start ->
            return LoadResult.Page(
                data = items.subList(Math.min(start, items.size), Math.min(params.loadSize, items.size)),
                prevKey = start,
                nextKey = start + params.loadSize
            )
        } ?: run {
            val firstPage = 0
            val nextKey = params.loadSize

            return LoadResult.Page(
                data = items.subList(Math.min(firstPage, items.size), Math.min(nextKey, items.size)),
                prevKey = null,
                nextKey = nextKey
            )
        }
    }

    // from https://developer.android.com/topic/libraries/architecture/paging/v3-paged-data
    override fun getRefreshKey(state: PagingState<Int, T>): Int? {
        // Try to find the page key of the closest page to anchorPosition, from
        // either the prevKey or the nextKey, but you need to handle nullability
        // here:
        //  * prevKey == null -> anchorPage is the first page.
        //  * nextKey == null -> anchorPage is the last page.
        //  * both prevKey and nextKey null -> anchorPage is the initial page, so
        //    just return null.
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }
}
