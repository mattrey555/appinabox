package com.apptechnik.appinbox.android.intercept

import android.content.Context
import android.content.pm.PackageManager
import com.apptechnik.appinabox.android.BuildConfig

import okhttp3.Interceptor
import okhttp3.Response

/**
 * Adds "User-Agent" header to the request.
 */
class UserAgentInterceptor(appContext: Context) : Interceptor {

    init {
        initUserAgentValue(
            appContext
        )
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        return chain.proceed(chain.request()
            .newBuilder()
            .header("User-Agent", userAgentValue)
            .build())
    }

    companion object {

        private val defaultUserAgent by lazy {
            BuildConfig.LIBRARY_PACKAGE_NAME + "/" + BuildConfig.BUILD_TYPE
        }

        private lateinit var userAgentValue: String

        /**
         * Sets the user-agent name to be used for okHttp Network Calls, returns
         * Application Package Name, Version Name, Build Number
         * eg response: com.climate.android.climatehttp.test/1.0.0/1
         * @param context
         */
        private fun initUserAgentValue(context: Context) {
            userAgentValue = try {
                val pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
                if (pInfo != null) {
                    "${pInfo.packageName}/${pInfo.versionName}/${pInfo.versionCode}"
                } else {
                    defaultUserAgent
                }
            } catch (e: PackageManager.NameNotFoundException) {
                defaultUserAgent
            }
        }
    }
}
