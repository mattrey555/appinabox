package com.apptechnik.appinbox.android.network

import android.app.Application
import android.content.Context

import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit
import com.apptechnik.appinbox.android.intercept.UserAgentInterceptor

const val TIMEOUT_CONNECT_SECONDS = 10L
const val TIMEOUT_READ_SECONDS = 60L
const val TIMEOUT_WRITE_SECONDS = 60L

class Network(val context : Context, val baseUrl : String) {
    private val httpClient: OkHttpClient by lazy { getHttpClientBuilder(context).build() }

    fun <T> createNetworkAdapter(type: Class<T>): T {
        return createNetworkAdapter(type, httpClient)
    }

    fun <T> createNetworkAdapter(type: Class<T>, client: OkHttpClient): T {
        val moshi = Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .add(Date::class.java, Rfc3339DateJsonAdapter().nullSafe())
            .build()

        // IMPORTANT NOTE: ScalarsConverterFactory must appear before the other converters.
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create(moshi).asLenient())
            .client(client)
            .build()
            .create(type)
    }

    companion object {
        fun getHttpClientBuilder(context: Context): OkHttpClient.Builder {
            val httpClientBuilder = OkHttpClient.Builder()
                .connectTimeout(TIMEOUT_CONNECT_SECONDS, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT_READ_SECONDS, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT_WRITE_SECONDS, TimeUnit.SECONDS)
                .addNetworkInterceptor(UserAgentInterceptor(context))
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
            return httpClientBuilder
        }
    }
}
