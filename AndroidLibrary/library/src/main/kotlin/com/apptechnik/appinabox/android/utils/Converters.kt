package com.apptechnik.appinabox.android.utils

import android.widget.DatePicker
import android.widget.TimePicker
import java.util.*

object Converters {
    fun set(datePicker : DatePicker, date : Date) {
        val calendar = Calendar.getInstance()
        calendar.time = date

        datePicker.updateDate(calendar.get(Calendar.YEAR),
                              calendar.get(Calendar.MONTH),
                              calendar.get(Calendar.DAY_OF_MONTH))
    }

    fun get(datePicker : DatePicker) : Date {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.YEAR, datePicker.year)
        calendar.set(Calendar.MONTH, datePicker.month)
        calendar.set(Calendar.DAY_OF_MONTH, datePicker.dayOfMonth)
        return calendar.time
    }

    fun set(timePicker : TimePicker, date : Date) {
        val calendar = Calendar.getInstance()
        calendar.time = date

        timePicker.hour = calendar.get(Calendar.HOUR_OF_DAY)
        timePicker.minute = calendar.get(Calendar.MINUTE)
    }

    fun get(timePicker : TimePicker) : Date {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, timePicker.hour)
        calendar.set(Calendar.MINUTE, timePicker.minute)
        return calendar.time
    }
}