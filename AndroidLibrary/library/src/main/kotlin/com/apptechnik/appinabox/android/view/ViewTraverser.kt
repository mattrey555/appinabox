package com.apptechnik.appinabox.android.view

import android.view.View
import android.view.ViewGroup
import android.widget.*
import java.util.*
import java.util.function.Consumer

object ViewTraverser {
    fun traverse(v: View, consumer: Consumer<View>) {
        consumer.accept(v)
        if (v is ViewGroup) {
            val vg = v as ViewGroup
            for (i in 0..vg.childCount - 1) {
                traverse(v.getChildAt(i), consumer)
            }
        }
    }

    class ValueConsumer(val map: MutableMap<String, Object>) : Consumer<View> {
        override fun accept(v: View) {
            val id = v.context.resources.getResourceEntryName(v.id)
            if (v is EditText) {
                val text = (v as EditText).text.toString()
                map.put(id, text as Object)
            } else if (v is Checkable) {
                val checked = (v as Checkable).isChecked
                map.put(id, checked as Object)
            } else if (v is ProgressBar) {
                val position = (v as ProgressBar).progress
                map.put(id, position as Object)
            } else if (v is DatePicker) {
                val day = (v as DatePicker).dayOfMonth
                val month = (v as DatePicker).month
                val year = (v as DatePicker).year
                val date = Calendar.getInstance().set(year, month, day)
                map.put(id, date as Object)
            } else if (v is Spinner) {
                val selection = (v as Spinner).selectedItem
                map.put(id, selection as Object)
            } else if (v is CalendarView) {
                val date = Date((v as CalendarView).date)
                map.put(id, date as Object)
            } else if (v is NumberPicker) {
                val num = (v as NumberPicker).value
                map.put(id, num as Object)
            } else if (v is Switch) {
                val switched = (v as Switch).isActivated
                map.put(id, switched as Object)
            } else if (v is TimePicker) {
                val hour = (v as TimePicker).hour
                val min =(v as TimePicker).minute
                val time = (hour*60 + min)*60*1000L
                map.put(id, time as Object)
            }
        }
    }
}