package com.apptechnick.appinaboxsample.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.apptechnik.appinabox.android.repository.ListPagingSource

/**
 * ViewModel which takes a fixed list and creates a Flow of paging data.
 */
class ListViewModel<T>(val list: List<T>) : ViewModel() where T : Any {
    suspend fun getAllData(): Flow<PagingData<T>> = Pager(
        config = PagingConfig(pageSize = 20, prefetchDistance = 2),
        pagingSourceFactory = { ListPagingSource(list)  }
    ).flow
}