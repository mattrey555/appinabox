package com.apptechnik.appinabox.android.viewmodels

import androidx.lifecycle.ViewModel

/**
 * simple wrapper view model for an arbitary object.
 */
class ObjectViewModel<T>(val obj: T) : ViewModel() {
    fun get() : T {
        return obj
    }
}