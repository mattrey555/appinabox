package ${PACKAGE}.app.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import ${PACKAGE}.app.R
import ${PACKAGE}.app.adapters.StringAdapter
import ${PACKAGE}.app.databinding.ListLayoutBinding
import java.util.*


class EndpointFragment : Fragment() {
    private lateinit var binding: ListLayoutBinding

    companion object {
        fun newInstance() = EndpointFragment()
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding = ListLayoutBinding.inflate(inflater)
        val view = binding.root
        binding.recyclerView.layoutManager = LinearLayoutManager(activity)
        val stringList = getResources().getTextArray(R.array.endpoints)
        binding.recyclerView.adapter = StringAdapter(stringList, NavigateEndpointListener())
        return view
    }

    class NavigateEndpointListener : StringAdapter.ClickStringListener {
        override fun onClick(v : View, s : String) {

        }
    }
}