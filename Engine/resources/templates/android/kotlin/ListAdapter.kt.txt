package ${PACKAGE}.app.adapters
import android.view.View
import android.view.ViewGroup
import android.view.LayoutInflater
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import ${PACKAGE}.query.${TYPE}
import ${PACKAGE}.app.databinding.${BINDING}
import ${PACKAGE}.app.R


/**
 * Adapter for a list of strings bound to a single text item.
 */
class ${TYPE}Adapter : PagingDataAdapter<${TYPE}, ${TYPE}Adapter.ViewHolder>(${TYPE}Comparator) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(${BINDING}.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }

    inner class ViewHolder(private val binding: ${BINDING}) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ${TYPE}) = with(binding) {
           ${ITEM_BINDING}
        }
    }

    object ${TYPE}Comparator : DiffUtil.ItemCallback<${TYPE}>() {
        override fun areItemsTheSame(oldItem: ${TYPE}, newItem: ${TYPE}) =
            ${COMPARE_ID}

        override fun areContentsTheSame(oldItem: ${TYPE}, newItem: ${TYPE}) =
            ${COMPARE_CONTENTS}
    }
}
