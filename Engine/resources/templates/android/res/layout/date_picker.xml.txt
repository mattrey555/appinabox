<?xml version="1.0" encoding="utf-8"?>
<DatePicker
        android:id="@+id/${NAME}"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="@string/${DESCRIPTION}/>
