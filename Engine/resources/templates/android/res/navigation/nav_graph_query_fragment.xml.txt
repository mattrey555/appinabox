<fragment
        android:id="@+id/${FRAGMENT_NAME}"
        android:name="${PACKAGE}.${FRAGMENT_NAME}"
        android:label="@string/${FRAGMENT_LABEL}"
        tools:layout="@layout/${FRAGMENT_LAYOUT}">
        <action
            android:id="@+id/${FRAGMENT_ACTION}"
            app:destination="@id/${DESTINATION}" />
</fragment>
