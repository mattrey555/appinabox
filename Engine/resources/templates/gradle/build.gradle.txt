// Top-level build file where you can add configuration options common to all sub-projects/modules.
buildscript {
    repositories {
        google()
        mavenLocal()
        jcenter()
    }
    dependencies {
        classpath "com.android.tools.build:gradle:4.2.0"
        classpath "org.jetbrains.kotlin:kotlin-gradle-plugin:1.5.20"
    }
}

allprojects {
    repositories {
        google()
        mavenLocal()
        jcenter()
        maven { url 'https://maven.google.com' }
    }
    gradle.projectsEvaluated {
        tasks.withType(JavaCompile) {
            options.compilerArgs << "-Xmaxerrs" << "4000"
            options.compilerArgs << "-Xmaxwarns" << "4000"
        }
    }
}
