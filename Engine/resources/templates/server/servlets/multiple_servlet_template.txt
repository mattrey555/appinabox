package ${PACKAGE}.servlet;
${IMPORTS}
${QUERY_IMPORTS}
${UPDATE_IMPORTS}
import ${PACKAGE}.RequestResponse;


public class ${CLASSNAME}Servlet extends HttpServlet {
    private static DataSource datasource = null;
    private static String databaseName = null;
    private Logger logger;

    @Override
    public void init(ServletConfig config) throws ServletException {
        try {
            logger = LogManager.getLogger(config.getServletName());
            getLogger().debug("initialize");
            InitialContext initialContext = new InitialContext();
            if (initialContext == null) {
                String message = "There was no InitialContext. We're about to have some problems.";
                throw new ServletException(message);
            }
            Context envCtx = (Context) initialContext.lookup("java:comp/env");
            if (envCtx == null) {
                String message = "There was no Java Environment Context. We're about to have some problems.";
                throw new ServletException(message);
            }
            // actual jndi name is "jdbc/<database>"
            // NOTE: make this a constant.
            String databaseName = config.getServletContext().getInitParameter("database");
            getLogger().debug("init parameter database = " + databaseName);
            datasource = (DataSource) envCtx.lookup(databaseName);
        } catch (NamingException nex) {
            throw new ServletException(nex);
        }
    }

    @Override
    public void do${HTTP_METHOD}(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        if (!beforeParse(request, response)) {
            return;
        }
        response.setHeader("Content-Type", "application/json");
        Connection conn = null;
        try {
            conn = getConnection();
            RequestResponse requestResponse = new RequestResponse(request, response);
            if (!beforeUpdate(requestResponse)) {
                return;
            }
            ${INSERT_UPDATE_LIST}
            if (!beforeQuery(requestResponse)) {
                return;
            }
            ${CLASSNAME_OUT} result = new ${CLASSNAME_OUT}();
            ${SELECT_LISTS_INTO_FIELDS}
            ${REQUEST_RESPONSE_ASSIGNMENT}
            if (!beforeResult(response, requestResponse)) {
                return;
            }
            ${SERVLET_WRITE_OUTPUT}
            freeConnection(conn);
        } catch (SQLException sqlex) {
            getLogger().error(sqlex.getMessage());
            throw new ServletException(sqlex);
        } finally {
            if (conn != null) {
                freeConnection(conn);
            }
        }
    }

    /**
     * Dole out the connections here.
     */
    public static synchronized Connection getConnection() throws SQLException {
        Connection conn = datasource.getConnection();
        conn.setAutoCommit(false);
        return conn;
    }

    /**
     * Must close the database connection to return it to the pool.
     */
    public static synchronized void freeConnection(Connection connection) {
        try {
            connection.close();
        } catch (Exception e) {
            System.err.println("DBBroker: Threw an exception closing a database connection");
            e.printStackTrace();
        }
    }

    public Logger getLogger() {
        return logger;
    }

    public boolean beforeParse(HttpServletRequest request,
                               HttpServletResponse response) { return true; }
    public boolean beforeUpdate(RequestResponse requestResponse) { return true; }
    public boolean beforeQuery(RequestResponse requestResponse) { return true; }
    public boolean beforeResult(HttpServletResponse response, RequestResponse requestResponse) { return true; }
}
