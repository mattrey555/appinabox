package com.apptechnik.appinabox.engine;

import java.util.HashMap;
import java.util.Map;

/**
 * Constants, semantically grouped.
 * Grouping allows the reader to understand the purpose of the constant, such as that it's
 * a SQL keyword, or a File extension,
 */
public class Constants {
    public static final String WARNING = "WARNING";

    public static class SQLKeywords {
        public static final String SELECT = "select";
        public static final String UPDATE = "update";
        public static final String INSERT = "insert";
        public static final String DELETE = "delete";
        public static final String TAG_NOT = "NOT";
        public static final String TAG_NULL = "NULL";
        public static final String TAG_PRIMARY = "PRIMARY";
        public static final String TAG_KEY = "KEY";
        public static final String TAG_REFERENCES = "REFERENCES";
        public static final String POSTGRES_SERIAL = "SERIAL";
        public static final String SQLTYPE_INTEGER = "INTEGER";
        public static final String SQLTYPE_TIMESTAMP = "TIMESTAMP";
        public static final String ALL_COLUMNS = "*";
        public static final String UNIQUE = "UNIQUE";
        public static final String TABLE = "TABLE";
        public static final String INDEX = "INDEX";
    }

    // strings used in URLs
    public static class URL {
        public static final String PATH_DELIM = "/";
        public static final String QUERY_START = "?";
        public static final String QUERY_DELIM = "&";
        public static final String QUERY_EQUALS = "=";
    }

    public static class Gradle {
        public static final String INCLUDE = "include";
    }

    // SQL types.
    public static class SQLTypes {
        public static String BIT = "BIT";
        public static String TINYINT = "TINYINT";
        public static String SMALLINT = "SMALLINT";
        public static String INTEGER = "INTEGER";
        public static String LONG = "LONG";
        public static String SERIAL = "SERIAL";
        public static String BIGINT = "BIGINT";
        public static String FLOAT = "FLOAT";
        public static String REAL = "REAL";
        public static String DOUBLE = "DOUBLE";
        public static String NUMERIC = "NUMERIC";
        public static String DECIMAL = "DECIMAL";
        public static String CHAR = "CHAR";
        public static String NCHAR = "NCHAR";
        public static String VARCHAR = "VARCHAR";
        public static String NVARCHAR = "NVARCHAR";
        public static String TEXT = "TEXT";
        public static String CHARACTER_VARYING = "CHARACTER VARYING";
        public static String CHARACTER = "CHARACTER";
        public static String VARYING = "VARYING";
        public static String LONGVARCHAR = "LONGVARCHAR";
        public static String NLONGVARCHAR = "NLONGVARCHAR";
        public static String DATE = "DATE";
        public static String SMALLDATETIME = "SMALLDATETIME";
        public static String DATETIME = "DATETIME";
        public static String DATETIMEOFFSET = "DATETIMEOFFSET";
        public static String TIME = "TIME";
        public static String TIMESTAMP = "TIMESTAMP";
        public static String TIMESTAMP_WITHOUT_TIME_ZONE = "TIMESTAMP WITHOUT TIME ZONE";
        public static String BINARY = "BINARY";
        public static String VARBINARY = "VARBINARY";
        public static String LONGVARBINARY = "LONGVARBINARY";
        public static String NULL = "NULL";
        public static String OTHER = "OTHER";
        public static String JAVA_OBJECT = "JAVA_OBJECT";
        public static String DISTINCT = "DISTINCT";
        public static String STRUCT = "STRUCT";
        public static String ARRAY = "ARRAY";
        public static String BLOB = "BLOB";
        public static String CLOB = "CLOB";
        public static String NCLOB = "NCLOB";
        public static String REF = "REF";
        public static String TSVECTOR = "TSVECTOR";
        public static String TSQUERY = "TSQUERY";
        public static String BOOLEAN = "BOOLEAN";                   // only used in boolean expressions, never a column spec
        public static String STRING = "STRING";                     // only used in text expressions, never a column spec
        public static String SAME = "SAME";                         // only used in type inference NOTE NOT SAME AS EQUALS
        public static String NAMED_PARAMETER = "NAMED_PARAMETER";   // only used in type inference
        public static String INTERVAL = "INTERVAL";                 //
        public static String ANY = "ANY";                           // type inference same as argument, but restricted to numeric
        public static String ANY_NUMBER = "ANY_NUMBER";             // type inference same as argument, but restricted to numeric
    }

    /**
     * Reserved column names.
     */
    public static class SQLColumns {
        public static final String DELETED = "deleted";                 // soft delete flag.
        public static final String MODIFIED_DATE = "modified_date";     // modified_date for delta sync.
    }

    /**
     * List of SQL functions.
     */
    public static class SQLFunctions {
        public static final String BIT_LENGTH = "bit_length";
        public static final String CHAR_LENGTH = "char_length";
        public static final String CHARACTER_LENGTH = "character_length";
        public static final String LOWER = "lower";
        public static final String OCTET_LENGTH = "octet_length";
        public static final String OVERLAY = "overlay";
        public static final String POSITION = "position";
        public static final String SUBSTRING = "substring";
        public static final String TRIM = "trim";
        public static final String UPPER = "upper";
        public static final String COUNT = "COUNT";
        public static final String MAX = "MAX";
        public static final String MIN = "MIN";
        public static final String SUM = "SUM";
        public static final String AVG = "AVG";
        public static final String ABS = "ABS";
        public static final String ACOS = "ACOS";
        public static final String ASIN = "ASIN";
        public static final String ATAN = "ATAN";
        public static final String ATN2 = "ATN2";
        public static final String CEILING = "CEILING";
        public static final String COS = "COS";
        public static final String COT = "COT";
        public static final String DEGREES = "DEGREES";
        public static final String EXP = "EXP";
        public static final String FLOOR = "FLOOR";
        public static final String LOG = "LOG";
        public static final String LOG10 = "LOG10";
        public static final String PI = "PI";
        public static final String POWER = "POWER";
        public static final String RADIANS = "RADIANS";
        public static final String RAND = "RAND";
        public static final String ROUND = "ROUND";
        public static final String SIGN = "SIGN";
        public static final String SIN = "SIN";
        public static final String SQRT = "SQRT";
        public static final String SQUARE = "SQUARE";
        public static final String TAN = "TAN";
        public static final String AGE = "AGE";

        public static final String CURRENT_TIMESTAMP = "CURRENT_TIMESTAMP";
        public static final String CLOCK_TIMESTAMP = "CLOCK_TIMESTAMP";
        public static final String CURRENT_DATE = "CURRENT_DATE";
        public static final String CURRENT_TIME = "CURRENT_TIME";
        public static final String DATE_PART = "DATE_PART";
        public static final String DATE_TRUNC = "DATE_TRUNC";
        public static final String EXTRACT = "EXTRACT";
        public static final String ISFINITE = "ISFINITE";
        public static final String JUSTIFY_DAYS = "JUSTIFY_DAYS";
        public static final String JUSTIFY_HOURS = "JUSTIFY_HOURS";
        public static final String JUSTIFY_INTERVAL = "JUSTIFY_INTERVAL";
        public static final String NOW = "NOW";
        public static final String STATEMENT_TIMESTAMP = "STATEMENT_TIMESTAMP";
        public static final String TIMEOFDAY = "TIMEOFDAY";
        public static final String TRANSACTION_TIMESTAMP = "TRANSACTION_TIMESTAMP";



        public static final String DATEADD = "DATEADD";
        public static final String DATEDIFF = "DATEDIFF";
        public static final String DATEFROMPARTS = "DATEFROMPARTS";
        public static final String DATENAME = "DATENAME";
        public static final String DATEPART = "DATEPART";
        public static final String DAY = "DAY";
        public static final String GETDATE = "GETDATE";
        public static final String GETUTCDATE = "GETUTCDATE";
        public static final String ISDATE = "ISDATE";
        public static final String MONTH = "MONTH";
        public static final String SYSDATETIME = "SYSDATETIME";
        public static final String YEAR = "YEAR";
        public static final String CAST = "CAST";
        public static final String COALESCE = "COALESCE";
        public static final String CONVERT = "CONVERT";
        public static final String IIF = "IIF";
        public static final String ISNULL = "ISNULL";
        public static final String ISNUMERIC = "ISNUMERIC";
        public static final String NULLIF = "NULLIF";
    }

    // variables used in generated code.
    public static class Variables {
        public static final String NPS = "nps";
        public static final String RS = "rs";
        public static final String MIN_ELAPSED_TIME = "minElapsedTime";
        public static final String MAX_ATTEMPTS = "maxAttempts";
        public static final String TEMP = "Temp";
        public static final String PKSC = "pksc";
        public static final String OBJECT_NAME = "o";
        public static final String CONN = "conn";
        public static final String READ_FLAG = "readFlag";
        public static final String ARGS = "args";
        public static final String SQL = "SQL";
        public static final String STATEMENT = "STATEMENT";
        public static final String INPUT = "input";
        public static final String OUTPUT = "output";
        public static final String RESULT = "result";
        public static final String DB_CONNECTION = "dbConnection";
        public static final String DB_CONNECTION_TEST = "dbConnectionTest";
        public static final String LIST = "list";
        public static final String PATH = "path";
        public static final String ITEM = "item";
        public static final String ITEM_BINDING = "item_binding";
        public static final String TEXT = "text";
        public static final String LIST_TYPE = "listType";
        public static final String READER = "reader";
        public static final String WRITER = "writer";
        public static final String GSON = "gson";
        public static final String REQUEST_RESPONSE = "requestResponse";
        public static final String SYSTEM = "System";
        public static final String OUT = "out";
        public static final String OLD_ITEM = "oldItem";
        public static final String NEW_ITEM = "newItem";
        public static final String CALL = "call";
        public static final String RESPONSE = "response";
        public static final String BODY = "body";
        public static final String NETWORK = "network";
    }

    // directives used in configuration files.
    public static class Directives {
        public static final String JSON_REQUEST = "json-request";       // json-request: <json-mapping>
        public static final String NAME_REQUEST = "name-request";       // name-request: <identifier>
        public static final String JSON_RESPONSE = "json-response";     // json-response: <json-mapping>
        public static final String NAME_RESPONSE = "name-response";     // name-response: <identifier>
        public static final String PACKAGE = "package";                 // package: <java-package-name>
        public static final String NAME = "name";
        public static final String TYPE = "type";                       // type: <param-name> <type>
        public static final String URL = "url";
        public static final String HEADER = "header";
        public static final String OPTIONAL = "optional";                   // optional: <param-name> [<default>]
        public static final String METHOD = "method";
        public static final String NO_SERVLET = "do-not-generate-servlet";  // Do not generate servlet code
        public static final String NO_MAIN = "do-not-generate-main";        // Do not generate main() code
        public static final String ANDROID = "android";                     // generate android endpoint + objects
        public static final String ENDPOINTS = "endpoints";                 // generate android endpoint + objects
        public static final String REPOSITORY = "repository";               // client repository name.
        public static final String DESCRIPTION = "description";             // url description
        public static final String SYNC = "sync";                           // sync: <table> server soft deletes, client DB generation
        public static final String DELETED = "deleted";                     // actual table and column name for deleted flag.
        public static final String TIMESTAMP = "timestamp";                 // actual table and column name for timestamp.
        public static final String LAYOUT = "layout";                       // XML layout file.
        public static final String HIDDEN = "hidden";                       // field is hidden (not displayed in UI).
    }

    public static class Files {
        public static final String BUILD_GRADLE = "build.gradle";
        public static final String GRADLE_PROPERTIES = "gradle.properties";
        public static final String SETTINGS_GRADLE = "settings.gradle";
        public static final String ANDROID_MANIFEST = "AndroidManifest.xml";
        public static final String ANDROID_ENDPOINT_XML = "endpoints.xml";
        public static final String ANDROID_STRINGS_XML = "strings.xml";
        public static final String ANDROID_ENVIRONMENT_XML = "environment.xml";
        public static final String ANDROID_STYLES_XML = "styles.xml";
        public static final String ANDROID_NAV_GRAPH_XML = "nav_graph.xml";
    }

    public static class Directories {
        public static final String SERVER = "server";
        public static final String SERVER_GENERATED_MAIN = "/src/main/java";
        public static final String SERVER_GENERATED_TEST = "/src/test/java";
        public static final String ANDROID = "android";
        public static final String ANDROID_LIBRARY = "android/library";
        public static final String ANDROID_APP = "android/app";
        public static final String ANDROID_MAIN = "/src/main";
        public static final String ANDROID_GENERATED_MAIN = "src/main/kotlin";
        public static final String ANDROID_GENERATED_TEST = "src/test/kotlin";
        public static final String ANDROID_RES = "src/main/res";
        public static final String ANDROID_RES_VALUES = "src/main/res/values";
        public static final String ANDROID_RES_NAVIGATION = "src/main/res/navigation";
        public static final String ANDROID_NAVIGATION = "navigation";
        public static final String ANDROID_RES_LAYOUT = "src/main/res/layout";
        public static final String ANDROID_LAYOUT = "layout";
        public static final String ANDROID_KOTLIN = "kotlin";

    }

    public static class Fragments {
        public static final String MAIN_WRITE_OUTPUT = "templates/server/fragments/main_write_output.txt";
        public static final String SERVLET_WRITE_OUTPUT = "templates/server/fragments/main_write_output.txt";
    }

    public static class Resources {
        public static final String ENGINE = "engine";
        public static final String SQL_MAP_TYPE = ENGINE + "/sql-map-type.json";
    }

    // template files with ${SUBSTITUION} expressions or lists imported packages.
    public static class Templates {
        public static final String TEMPLATES = "templates";
        public static final String ANDROID_TEMPLATES = TEMPLATES + "/android";
        public static final String ANDROID_RESOURCE_TEMPLATES = ANDROID_TEMPLATES + "/res";
        public static final String ANDROID_LAYOUT_TEMPLATES = ANDROID_RESOURCE_TEMPLATES + "/layout";
        public static final String ANDROID_VALUE_TEMPLATES = ANDROID_RESOURCE_TEMPLATES + "/values";
        public static final String ANDROID_NAVIGATION_TEMPLATES = ANDROID_RESOURCE_TEMPLATES + "/navigation";

        public static final String ANDROID_KOTLIN_TEMPLATES = ANDROID_TEMPLATES + "/kotlin";
        public static final String ANDROID_LIST_ADAPTER = ANDROID_KOTLIN_TEMPLATES + "/ListAdapter.kt.txt";
        public static final String KOTLIN_OBJECT_IMPORTS = ANDROID_TEMPLATES + "/imports/kotlin_object_imports.txt";
        public static final String ANDROID_ENDPOINT_IMPORTS = ANDROID_TEMPLATES + "/imports/endpoint_imports.txt";
        public static final String ANDROID_LIBRARY_MANIFEST = ANDROID_TEMPLATES + "/manifest/library_AndroidManifest.xml";
        public static final String ANDROID_APP_MANIFEST = ANDROID_TEMPLATES + "/manifest/app_AndroidManifest.xml.txt";
        public static final String ANDROID_GRADLE_TEMPLATES = ANDROID_TEMPLATES + "/gradle";
        public static final String ANDROID_LIBRARY_BUILD_GRADLE = ANDROID_GRADLE_TEMPLATES + "/library_build.gradle.txt";
        public static final String ANDROID_APP_BUILD_GRADLE = ANDROID_GRADLE_TEMPLATES + "/app_build.gradle.txt";
        public static final String ANDROID_BUILD_GRADLE = ANDROID_GRADLE_TEMPLATES + "/build.gradle.txt";
        public static final String ANDROID_GRADLE_PROPERTIES = ANDROID_GRADLE_TEMPLATES + "/gradle.properties.txt";
        public static final String ANDROID_SETTINGS_GRADLE = ANDROID_GRADLE_TEMPLATES + "/settings.gradle.txt";
        public static final String STRING_ARRAY_XML = ANDROID_VALUE_TEMPLATES + "/string-array.xml.txt";
        public static final String ANDROID_ENDPOINT_ACTIVITY = ANDROID_KOTLIN_TEMPLATES + "/EndpointListActivity.kt.txt";
        public static final String ANDROID_MAIN_APPLICATION = ANDROID_KOTLIN_TEMPLATES + "/MainApplication.kt.txt";
        public static final String ANDROID_STRING_ADAPTER = ANDROID_KOTLIN_TEMPLATES + "/StringAdapter.kt.txt";
        public static final String ANDROID_ENDPOINT_FRAGMENT = ANDROID_KOTLIN_TEMPLATES + "/EndpointFragment.kt.txt";
        public static final String ANDROID_QUERY_PARAMS_FRAGMENT = ANDROID_KOTLIN_TEMPLATES + "/QueryParamsFragment.kt.txt";
        public static final String ANDROID_STRINGS_XML = ANDROID_VALUE_TEMPLATES + "/strings.xml.txt";
        public static final String ANDROID_STYLES_XML = ANDROID_VALUE_TEMPLATES + "/styles.xml.txt";
        public static final String ANDROID_STYLES_NIGHT_XML = ANDROID_RESOURCE_TEMPLATES + "/values-night/styles.xml.txt";
        public static final String ANDROID_STYLES_V21_XML = ANDROID_RESOURCE_TEMPLATES + "/values-v21/styles.xml.txt";
        public static final String ANDROID_STYLES_NIGHT_V21_XML = ANDROID_RESOURCE_TEMPLATES + "/values-night-v21/styles.xml.txt";
        public static final String NAV_GRAPH_XML = ANDROID_NAVIGATION_TEMPLATES + "/nav_graph.xml.txt";
        public static final String NAV_GRAPH_QUERY_FRAGMENT_XML = ANDROID_NAVIGATION_TEMPLATES + "/nav_graph_query_fragment.xml.txt";
        public static final String NAV_GRAPH_RESULTS_FRAGMENT_XML = ANDROID_NAVIGATION_TEMPLATES + "/nav_graph_results_fragment.xml.txt";
        public static final String NAV_GRAPH_RESULTS_LIST_FRAGMENT_XML = ANDROID_NAVIGATION_TEMPLATES + "/nav_graph_results_list_fragment.xml.txt";

        public static final String ACTION_XML = ANDROID_NAVIGATION_TEMPLATES + "/action.xml.txt";
        public static final String ANDROID_QUERY_FRAGMENT_XML = ANDROID_LAYOUT_TEMPLATES + "/query_fragment.xml.txt";
        public static final String ANDROID_DISPLAY_FRAGMENT_XML = ANDROID_LAYOUT_TEMPLATES + "/display_fragment.xml.txt";
        public static final String ANDROID_LIST_FRAGMENT_XML = ANDROID_LAYOUT_TEMPLATES + "/list_fragment.xml.txt";

        public static final String EXECUTE_BUTTON = ANDROID_LAYOUT_TEMPLATES + "/execute_button.xml.txt";
        public static final String SERVER_TEMPLATES = TEMPLATES + "/server";
        public static final String SERVER_APP_TEMPLATES = SERVER_TEMPLATES + "/app";
        public static final String SQL_MAIN_SINGLE_RESPONSE = SERVER_APP_TEMPLATES + "/sql_main_single_response.txt";
        public static final String SQL_MAIN_MULTIPLE_RESPONSE = SERVER_APP_TEMPLATES + "/sql_main_multiple_response.txt";
        public static final String OBJECT_IMPORTS = SERVER_TEMPLATES + "/imports/object_imports.txt";

        public static final String SQLFNS_IMPORTS = SERVER_TEMPLATES + "/imports/sqlfns_imports.txt";
        public static final String MAIN_IMPORTS = SERVER_TEMPLATES + "/imports/main_imports.txt";
        public static final String REQUEST_RESPONSE_IMPORTS = SERVER_TEMPLATES + "/imports/request_response_imports.txt";
        public static final String SERVLET_IMPORTS = SERVER_TEMPLATES + "/imports/servlet_imports.txt";
        public static final String PRIMARY_KEY_IMPORTS = SERVER_TEMPLATES + "/imports/primary_key_imports.txt";
        public static final String PROJECT_BUILD_GRADLE = TEMPLATES + "/gradle/build.gradle.txt";
        public static final String SERVER_BUILD_GRADLE = SERVER_TEMPLATES + "/gradle/build.gradle.txt";
        public static final String WEB_XML = SERVER_TEMPLATES + "/serverconfig/web.xml.txt";
        public static final String LOG4J2 = SERVER_TEMPLATES + "/serverconfig/log4j2.xml.txt";
        public static final String CONTEXT_DATASOURCE = SERVER_TEMPLATES + "/serverconfig/context_datasource.xml.txt";
        public static final String NAVIGATE_CHILD_LISTENER = ANDROID_KOTLIN_TEMPLATES + "/NavigateChildListener.kt.txt";
        public static final String NAVIGATE_CHILD_LIST_LISTENER = ANDROID_KOTLIN_TEMPLATES + "/NavigateChildListListener.kt.txt";
        public static final String NAVIGATE_LIST_ITEM_LISTENER = ANDROID_KOTLIN_TEMPLATES + "/NavigateListItemListener.kt.txt";

        public static final String RESULTS_FRAGMENT = ANDROID_KOTLIN_TEMPLATES +"/ResultsFragment.kt.txt";
        public static final String RESULTS_LIST_FRAGMENT = ANDROID_KOTLIN_TEMPLATES +"/ResultsListFragment.kt.txt";
        public static final String REPOSITORY_IMPORTS = ANDROID_TEMPLATES + "/imports/repository_imports.txt";
        public static final String VIEW_MODEL_FACTORY_IMPORTS = ANDROID_TEMPLATES + "/imports/view_model_factory_imports.txt";
        public static final String VIEW_MODEL = ANDROID_KOTLIN_TEMPLATES + "/ViewModel.kt.txt";
        public static final String ANDROID_WIDGET_TEMPLATES = ANDROID_TEMPLATES + "/widgets";
        public static final String WIDGET_CONVERTERS = ANDROID_WIDGET_TEMPLATES + "/widget-converters.json";
    }

    public static class LayoutTemplates {
        public static final String FRAGMENT_LAYOUT = Templates.ANDROID_LAYOUT_TEMPLATES + "/fragment_layout.xml.txt";
        public static final String LIST_ELEMENT_1_XML = Templates.ANDROID_LAYOUT_TEMPLATES + "/list_element_1.xml.txt";
        public static final String LIST_ELEMENT_2A_XML = Templates.ANDROID_LAYOUT_TEMPLATES + "/list_element_2a.xml.txt";
        public static final String LIST_ELEMENT_2B_XML = Templates.ANDROID_LAYOUT_TEMPLATES + "/list_element_2b.xml.txt";
        public static final String LIST_ELEMENT_3A_XML = Templates.ANDROID_LAYOUT_TEMPLATES + "/list_element_3a.xml.txt";
        public static final String LIST_ELEMENT_3B_XML = Templates.ANDROID_LAYOUT_TEMPLATES + "/list_element_3b.xml.txt";
        public static final String LIST_ELEMENT_3C_XML = Templates.ANDROID_LAYOUT_TEMPLATES + "/list_element_3c.xml.txt";
        public static final String TEXT_VIEW_XML = "text_view.xml.txt";
        public static final String CONSTRAINT_LAYOUT_XML = "constraint_layout.xml.txt";
        public static final String BUTTON_XML = "button.xml.txt";
        public static final String SWITCH_XML = "switch.xml.txt";
        public static final String EDIT_TEXT_XML = "edit_text.xml.txt";
        public static final String DATE_PICKER_XML = "date_picker.xml.txt";
        public static final String TIME_PICKER_XML = "time_picker.xml.txt";
    }

    public static class ServletTemplates {
        public static final String POST_SINGLE_SERVLET = "templates/server/servlets/single_servlet_template.txt";
        public static final String POST_MULTIPLE_SERVLET = "templates/server/servlets/multiple_servlet_template.txt";
    }

    public static class HttpMethods {
        public static final String POST = "POST";
        public static final String PUT = "PUT";
        public static final String GET = "GET";
        public static final String DELETE = "DELETE";
        public static final String[] METHODS =  { POST, PUT, GET, DELETE };
    }

    // file extensions/Suffixes (TODO: consistent: dot or no dot)
    public static class Extensions {
        public static final String SQL = "sql";
        public static final String JAVA = ".java";
        public static final String KOTLIN = ".kt";
        public static final String SERVLET = "Servlet";
        public static final String SQLFNS = "SQLFns";
        public static final String TEST_JAVA = "Test.java";
        public static final String MAIN_JAVA = "Main.java";
        public static final String MAIN = "Main";
        public static final String XML_TEXT = ".xml.txt";
        public static final String XML = ".xml";
        public static final String LAYOUT_XML = "layout.xml";
        public static final String FRAGMENT_XML = "fragment.xml";
        public static final String LIST_ITEM_XML = "list_item.xml";
    }

    public static class JavaKeywords {
        public static final String PUBLIC = "public";
        public static final String PROTECTED = "protected";
        public static final String PRIVATE = "private";
        public static final String STATIC = "static";
        public static final String FINAL = "final";
        public static final String PACKAGE = "package";
        public static final String IMPORT = "import";
        public static final String RETURN = "return";
        public static final String NEW = "new";
        public static final String CLASS = "class";
        public static final String INTERFACE = "interface";
        public static final String IMPLEMENTS = "implements";
        public static final String EXTENDS = "extends";
        public static final String THROWS = "throws";
        public static final String THROW = "throw";
        public static final String THIS = "this";
        public static final String FOR = "for";
        public static final String WHILE = "while";
        public static final String ELSE = "else";
        public static final String IF = "if";
        public static final String NULL = "null";
        public static final String TRY = "try";
        public static final String CATCH = "catch";
        public static final String VALUE = "value";
    }

    public static class KotlinTypes {
        public static final String CALL = "Call";
        public static final String CALLBACK = "Callback";
        public static final String RESPONSE = "Response";
        public static final String BOOLEAN = "Boolean";
        public static final String INT = "Int";
        public static final String LONG = "Long";
        public static final String SHORT = "Short";
        public static final String BYTE = "Byte";
        public static final String CHAR = "Char";
        public static final String FLOAT = "Float";
        public static final String DOUBLE = "Double";
        public static final String STRING = "String";
        public static final String BYTEARRAY = "ByteArray";
        public static final String DATE = "Date";
        public static final String LIST = "List";
        public static final String MUTABLE_LIST = "MutableList";
        public static final String ARRAY_REF = "[]";
        public static final String NETWORK = "Network";
        public static final String VIEW_MODEL_PROVIDER_FACTORY  = "ViewModelProvider.Factory";
        public static final String CLASS = "Class";
        public static final String T = "T";
        public static final String EXCEPTION = "Exception";
        public static final String VIEW_MODEL = "ViewModel";
        public static final String REST_SERVER = "RestServer";
        public static final String MUTABLE_LIVE_DATA = "MutableLiveData";
        public static final String LIVE_DATA = "LiveData";
        public static final String THROWABLE = "Throwable";
        public static final String APPLICATION = "Application";
    }

    public static class KotlinKeywords {
        public static final String AS = "as";
        public static final String PUBLIC = "public";
        public static final String PROTECTED = "protected";
        public static final String PRIVATE = "private";
        public static final String COMPANION = "companion";
        public static final String OBJECT = "object";
        public static final String FUN = "fun";
        public static final String CONSTRUCTOR = "constructor";
        public static final String IN = "in";
        public static final String VAL = "val";
        public static final String VAR = "var";
        public static final String LATEINIT = "lateinit";
        public static final String OPEN = "open";
        public static final String STATIC = "static";
        public static final String DATA = "data";
        public static final String NULL = "null";
        public static final String UNIT = "Unit";
        public static final String SUSPEND = "suspend";
        public static final String OVERRIDE = "override";
        public static final String THIS = "this";
        public static final String BY = "by";
        public static final String THROW = "throw";
        public static final String IMPORT = "import";
    }

    public static class KotlinFunctions {
        public static final String CREATE = "create";
        public static final String IS_ASSIGNABLE_FROM = "isAssignableFrom";
        public static final String TO_MUTABLE_LIST = "toMutableList";
        public static final String GET_INSTANCE = "getInstance";
        public static final String CREATE_NETWORK_ADAPTER = "createNetworkAdapter";
        public static final String TO_STRING = "toString";
        public static final String ON_RESPONSE = "onResponse";
        public static final String ON_FAILURE = "onFailure";
        public static final String SET_VALUE = "setValue";
        public static final String POST_VALUE = "postValue";
        public static final String BODY = "body";
        public static final String ENQUEUE = "enqueue";
        public static final String ARE_ITEMS_THE_SAME = "areItemsTheSame";
        public static final String MUTABLE_LIST_OF = "mutableListOf";
    }

    public static class KotlinVariables {
        public static final String MODEL_CLASS = "modelClass";
        public static final String VIEW_MODEL_CLASS_JAVA = "ViewModel::class.java";
        public static final String REPOSITORY = "repository";
        public static final String INSTANCE = "INSTANCE";
        public static final String NETWORK = "network";
        public static final String V = "v";
        public static final String CALL = "call";
        public static final String RESPONSE = "response";
        public static final String LIST_OF = "listOf";
        public static final String T = "t";
        public static final String ERROR_MESSAGE = "errorMessage";
        public static final String MESSAGE = "message";
        public static final String APPLICATION = "application";
        public static final String OLD_ITEM = "oldItem";
        public static final String NEW_ITEM = "newItem";
    }

    public static class Annotations {
        public static final String GENERATED = "Generated";
        public static final String PATH = "Path";
        public static final String QUERY = "Query";
        public static final String HEADER = "Header";
        public static final String BODY = "Body";
        public static final String DAO = "Dao";
        public static final String PRIMARY_KEY = "PrimaryKey";
        public static final String ENTITY = "Entity";
        public static final String SUPPRESS = "Suppress";
        public static final String UNCHECKED_CAST = "UNCHECKED_CAST";

    }

    public static class KotlinOperators {
        public static final String EQUALS = " == ";
        public static final String AND = " && ";
        public static final String DOT_NULLABLE = "?.";
        public static final String AT = "@";
    }

    public static class JavaOperators {
        public static final String EQUALS = " == ";
        public static final String NOT_EQUALS = " != ";
        public static final String ASSIGNMENT = " = ";
        public static final String GREATER_THAN = " > ";
        public static final String GREATER_EQ = " >= ";
        public static final String LESS_THAN = " < ";
        public static final String LESS_EQ = " <= ";
        public static final String PLUS = " + ";
        public static final String OR = " || ";
        public static final String AND = " && ";
        public static final String MINUS = " - ";
        public static final String ADD = " + ";
        public static final String DOT = ".";
    }

    public static class JavaTypes {
        public static final String VOID = "void";
        public static final String LIST = "List";
        public static final String ARRAY_LIST = "ArrayList";
        public static final String TYPE_TOKEN = "TypeToken";
        public static final String TYPE = "Type";
        public static final String RESULT_SET = "ResultSet";
        public static final String CONNECTION = "Connection";
        public static final String NAMED_PARAM_STATEMENT = "NamedParamStatement";
        public static final String REQUEST_RESPONSE = "RequestResponse";
        public static final String SERVLET_REQUEST = "HttpServletRequest";
        public static final String SERVLET_RESPONSE = "HttpServletResponse";

        public static final String STRING = "String";
        public static final String BYTE = "byte";
        public static final String BOXED_BYTE = "byte";
        public static final String INT = "int";
        public static final String BOXED_INT = "Integer";
        public static final String BOXED_INT_LIST = "List<Ineger>";

        public static final String LONG = "long";
        public static final String BOXED_LONG = "Long";
        public static final String BOXED_LONG_LIST = "List<Long>";

        public static final String SHORT = "short";
        public static final String BOXED_SHORT = "Short";
        public static final String BOXED_SHORT_LIST = "List<Short>";

        public static final String DOUBLE = "double";
        public static final String BOXED_DOUBLE = "Double";
        public static final String BOXED_DOUBLE_LIST = "List<Double>";

        public static final String FLOAT = "float";
        public static final String BOXED_FLOAT = "Float";
        public static final String BOXED_FLOAT_LIST = "List<Float>";

        public static final String CHAR = "char";
        public static final String BOXED_CHAR = "Character";
        public static final String DATE = "Date";
        public static final String DATE_LIST = "List<Date>";

        public static final String BYTE_ARRAY = "byte[]";
        public static final String BYTE_ARRAY_LIST = "List<byte[]>";

        public static final String BOOLEAN = "boolean";
        public static final String BOXED_BOOLEAN = "Boolean";
        public static final String BOXED_BOOLEAN_LIST = "List<Boolean>";

        public static final String EXCEPTION = "Exception";
        public static final String SQL_EXCEPTION = "SQLException";
        public static final String PRIMARY_KEY_SOURCE_COLUMNS = "PrimaryKeySourceColumns";

        public static final String INPUT_STREAM_READER = "InputStreamReader";
        public static final String OUTPUT_STREAM_WRITER = "OutputStreamWriter";

        public static final String FILE = "File";
        public static final String BUFFERED_READER = "BufferedReader";
        public static final String BUFFERED_WRITER = "BufferedWriter";
        public static final String WRITER = "Writer";


        public static final String FILE_READER = "FileReader";
        public static final String GSON_BUILDER = "GsonBuilder";
        public static final String GSON = "Gson";
        public static final String HTTP_SERVLET_REQUEST = "HttpServletRequest";
        public static final String HTTP_SERVLET_RESPONSE = "HttpServletResponse";
        public static final String COLLECTORS = "Collectors";
        public static final String SERIALIZABLE = "Serializable";
    }

    public static final String[] PRIMITIVE_JAVA_TYPES = {
        JavaTypes.STRING,
        JavaTypes.BYTE,
        JavaTypes.BOXED_BYTE,
        JavaTypes.INT,
        JavaTypes.BOXED_INT,
        JavaTypes.LONG,
        JavaTypes.BOXED_LONG,
        JavaTypes.SHORT,
        JavaTypes.BOXED_SHORT,
        JavaTypes.DOUBLE,
        JavaTypes.BOXED_DOUBLE,
        JavaTypes.FLOAT,
        JavaTypes.BOXED_FLOAT,
        JavaTypes.CHAR,
        JavaTypes.BOXED_CHAR,
        JavaTypes.DATE,
        JavaTypes.BYTE_ARRAY,
        JavaTypes.BOOLEAN,
        JavaTypes.BOXED_BOOLEAN
    };

    public static Map<String, String> JAVA_CONTAINER_DEFAULTS = initJavaContainerDefaults();

    private static Map<String, String> initJavaContainerDefaults() {
        Map<String, String> map = new HashMap<>();
        map.put(JavaTypes.LIST, JavaTypes.ARRAY_LIST);
        return map;
    }

    public static Map<String, String> JAVA_DEFAULTS = initJavaDefaults();

    private static Map<String, String> initJavaDefaults() {
        Map<String, String> map = new HashMap<>();
        map.put(JavaTypes.STRING, "null");
        map.put(JavaTypes.BYTE, "0x0");
        map.put(JavaTypes.BOXED_BYTE, "0x0");
        map.put(JavaTypes.INT, "0");
        map.put(JavaTypes.BOXED_INT, "0");
        map.put(JavaTypes.LONG, "0L");
        map.put(JavaTypes.BOXED_LONG,"0L");
        map.put(JavaTypes.SHORT, "0");
        map.put(JavaTypes.BOXED_SHORT, "0");
        map.put(JavaTypes.DOUBLE, "0.0");
        map.put(JavaTypes.BOXED_DOUBLE, "0.0");
        map.put(JavaTypes.FLOAT, "0.0F");
        map.put(JavaTypes.BOXED_FLOAT, "0.0F");
        map.put(JavaTypes.CHAR, "'\\0'");
        map.put(JavaTypes.BOXED_CHAR, "'\\0'");
        map.put(JavaTypes.DATE, "null");
        map.put(JavaTypes.BYTE_ARRAY, "null");
        map.put(JavaTypes.BOOLEAN, "false");
        map.put(JavaTypes.BOXED_BOOLEAN, "false");
        return map;
    }

    public static final String[] PRIMITIVE_KOTLIN_TYPES = {
            KotlinTypes.INT,
            KotlinTypes.LONG,
            KotlinTypes.SHORT,
            KotlinTypes.BYTE,
            KotlinTypes.CHAR,
            KotlinTypes.FLOAT,
            KotlinTypes.DOUBLE,
            KotlinTypes.BYTEARRAY,
            KotlinTypes.STRING,
            KotlinTypes.DATE,
            KotlinTypes.BOOLEAN
    };

    public static Map<String, String> KOTLIN_DEFAULTS = initKotlinDefaults();

    private static Map<String, String> initKotlinDefaults() {
        Map<String, String> map = new HashMap<>();
        map.put(KotlinTypes.INT, "0");
        map.put(KotlinTypes.LONG, "0L");
        map.put(KotlinTypes.SHORT, "0");
        map.put(KotlinTypes.BYTE, "0x0");
        map.put(KotlinTypes.CHAR, "'\\0'");
        map.put(KotlinTypes.FLOAT, "0.0F");
        map.put(KotlinTypes.DOUBLE, "0.0");
        map.put(KotlinTypes.BYTEARRAY, "null");
        map.put(KotlinTypes.STRING, "\"\"");
        return map;
    }

    public static class Encoding {
        public static final String UTF_8 = "UTF-8";
    }
    // exceptions thrown in generated code.
    public static class JavaExceptions {
        public static final String SQL_EXCEPTION = "SQLException";
        public static final String IO_EXCEPTION = "IOException";
        public static final String UNSUPPORTED_ENCODING_EXCEPTION = "UnsupportedEncodingException";
    }

    // functions used in generated code.
    public static class Functions {
        public static final String SELECT_LIST = "selectList";
        public static final String READ_FROM_RESULT_SET = "readFromResultSet";
        public static final String READ_LIST_FROM_RESULT_SET = "readListFromResultSet";
        public static final String INSERT_UPDATE ="insertUpdate";
        public static final String INSERT_UPDATE_LIST ="insertUpdateList";
        public static final String COMPARE_PRIMARY_KEYS = "comparePrimaryKeys";
        public static final String ADD = "add";
        public static final String SIZE = "size";
        public static final String GET = "get";
        public static final String NEXT = "next";
        public static final String EXECUTE_QUERY = "executeQuery";
        public static final String EXECUTE = "execute";
        public static final String GET_RESULT_SET = "getResultSet";
        public static final String WAS_NULL = "wasNull";
        public static final String EQUALS = "equals";
        public static final String EXTRACT = "extract";
        public static final String STREAM = "stream";
        public static final String MAP = "map";
        public static final String FLAT_MAP = "flatMap";
        public static final String TO_LIST = "toList";
        public static final String COLLECT = "collect";
        public static final String COLLECTION_STREAM = "Collection::stream";
        public static final String FROM_JSON = "fromJson";
        public static final String GET_INPUT_STREAM = "getInputStream";
        public static final String GET_OUTPUT_STREAM = "getOutputStream";
        public static final String SET_DATE_FORMAT = "setDateFormat";
        public static final String CREATE = "create";
        public static final String GET_TYPE = "getType";
        public static final String SYSTEM_ERR_PRINTLN = "System.err.println";
        public static final String WRITE_OUTPUT = "writeOutput";
        public static final String SET_OUTPUT = "setOutput";
        public static final String GET_OUTPUT = "getOutput";
        public static final String SET_INPUT = "setInput";
        public static final String GET_INPUT = "getInput";
        public static final String TO_JSON = "toJson";
        public static final String FLUSH = "flush";
        public static final String COMMIT = "commit";

    }

    public static class ServletFunctions {
        public static final String GET_REQUEST_URI = "getRequestURI";
        public static final String GET_PARAMETER = "getParameter";
        public static final String GET_HEADER = "getHeader";
        public static final String GET_DELIM_ELEMENT = "getDelimElement";
        public static final String WRITE = "write";
        public static final String SEND_ERROR = "sendError";
        public static final String EXTRACT_VALUE = "extractValue";
    }

    public static class ServletErrorCodes {
        public static final String SC_BAD_REQUEST = "SC_BAD_REQUEST";

    }

    public static class ServletVariables {
        public static final String DB_CONNECTION = "conn";
        public static final String SERVLET_REQUEST = "request";
        public static final String SERVLET_RESPONSE = "response";
        public static final String WRITER = "writer";
        public static final String EX = "ex";
    }


    public static class ServletClasses {
        public static final String SERVLET_STRING_UTIL = "ServletStringUtil";
    }

    // substitutions referenced in ${IDENTIFIER} form in template files.
    public static class Substitutions {
        public static final String IMPORT_QUERY_PACKAGE_NAME = "IMPORT_QUERY_PACKAGE_NAME";
        public static final String IMPORT_UPDATE_PACKAGE_NAME = "IMPORT_UPDATE_PACKAGE_NAME";
        public static final String CLASSNAME = "CLASSNAME";
        public static final String CLASSNAME_IN = "CLASSNAME_IN";
        public static final String CLASSNAME_OUT = "CLASSNAME_OUT";
        public static final String SQL_FNS = "SQL_FNS";
        public static final String SQL = "SQL";
        public static final String IMPORTS = "IMPORTS";
        public static final String CREATE_TABLES = "CREATE_TABLES";
        public static final String PACKAGE = "PACKAGE";
        public static final String STATEMENT_FILES_DIR = "STATEMENT_FILES_DIR";
        public static final String PROPERTIES_FILE = "PROPERTIES_FILE";
        public static final String DATE_FORMAT = "DATE_FORMAT";
        public static final String SQL_PACKAGE_NAME = "SQL_PACKAGE_NAME";
        public static final String CONVERT_ARGS = "CONVERT_ARGS";
        public static final String SELECT_LISTS_INTO_FIELDS = "SELECT_LISTS_INTO_FIELDS";
        public static final String SELECT_LIST = "SELECT_LIST";
        public static final String INSERT_UPDATE_LIST = "INSERT_UPDATE_LIST";
        public static final String INSERT_UPDATE = "INSERT_UPDATE";

        public static final String QUERY = "QUERY";
        public static final String UPDATE = "UPDATE";
        public static final String QUERY_IMPORTS = "QUERY_IMPORTS";
        public static final String UPDATE_IMPORTS = "UPDATE_IMPORTS";
        public static final String VALUE_UNDEFINED = "VALUE UNDEFINED";
        public static final String HTTP_METHOD = "HTTP_METHOD";
        public static final String EXTRACT_LISTS = "EXTRACT_LISTS";
        public static final String REQUEST_RESPONSE_ALLOCATION = "REQUEST_RESPONSE_ALLOCATION";
        public static final String REQUEST_RESPONSE_ASSIGNMENT = "REQUEST_RESPONSE_ASSIGNMENT";
        public static final String MIXIN = "Mixin";
        public static final String OUTPUT_DIR = "OUTPUT_DIR";
        public static final String ARCHIVE_NAME = "ARCHIVE_NAME";
        public static final String MAIN_WRITE_OUTPUT = "MAIN_WRITE_OUTPUT";
        public static final String SERVLET_WRITE_OUTPUT = "SERVLET_WRITE_OUTPUT";
        public static final String VAR_NAME = "VAR_NAME";
        public static final String FRAGMENT_NAME = "FRAGMENT_NAME";
        public static final String FRAGMENT_LABEL = "FRAGMENT_LABEL";
        public static final String FRAGMENT_LAYOUT = "FRAGMENT_LAYOUT";
        public static final String FRAGMENT_ACTION = "FRAGMENT_ACTION";
        public static final String FRAGMENT_CHILD_NAME = "FRAGMENT_CHILD_NAME";
        public static final String DESTINATION = "DESTINATION";
        public static final String NAME = "NAME";
        public static final String TYPE = "TYPE";
        public static final String ITEM_BINDING = "ITEM_BINDING";
        public static final String BINDING = "BINDING";
        public static final String COMPARE_CONTENTS = "COMPARE_CONTENTS";
        public static final String COMPARE_ID = "COMPARE_ID";
        public static final String DESCRIPTION = "DESCRIPTION";
        public static final String WIDGET = "WIDGET";
        public static final String VALUE = "VALUE";
        public static final String VAR = "VAR";
        public static final String WIDGET_PROPERTIES = "WIDGET_PROPERTIES";
        public static final String VARIABLES = "VARIABLES";
        public static final String ASSIGN_VARIABLES = "ASSIGN_VARIABLES";
        public static final String SET_NAVIGATION_LISTENERS = "SET_NAVIGATION_LISTENERS";
        public static final String NAVIGATION_LISTENERS = "NAVIGATION_LISTENERS";
        public static final String ENDPOINT_NAME = "ENDPOINT_NAME";
        public static final String REPOSITORY_NAME = "REPOSITORY_NAME";
        public static final String PARAM_DECLS = "PARAM_DECLS";
        public static final String ARGS = "ARGS";
        public static final String ENDPOINT_ARGS = "ENDPOINT_ARGS";
        public static final String ENDPOINT = "ENDPOINT";
        public static final String WIDGET_IMPORTS  = "WIDGET_IMPORTS";
        public static final String ENDPOINT_DECL  = "ENDPOINT_DECL";
        public static final String ENDPOINT_CALL  = "ENDPOINT_CALL";
        public static final String PRIMARY_KEY  = "PRIMARY_KEY";
        public static final String VISIBLE  = "VISIBLE";
        public static final String CREATE_STATEMENTS = "CREATE_STATEMENTS";
    }

    // strings read from properties file.
    public static class Properties {
        public static final String DATABASE_URL = "url";
        public static final String SERVER_URL = "server_url";
        public static final String DATABASE = "database";
        public static final String PACKAGE = "PACKAGE";
        public static final String ENDPOINT_NAME = "ENDPOINT_NAME";
    }

    // hacks for jdbc prefix handling in web.xml
    public static class PropertyHacks {
        public static final String JDBC_PREFIX = "jdbc/";
    }

    public static class PropertyDefaults {
        public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
    }

    // SQL ResultSet functions
    public static class ResultSetFns {
        public static final String GET_BYTE_AS_CHAR = "(char) getByte";
        public static final String GET_BOOLEAN = "getBoolean";
        public static final String GET_BYTE = "getByte";
        public static final String GET_BYTES = "getBytes";
        public static final String GET_DATE = "getDate";
        public static final String GET_DOUBLE = "getDouble";
        public static final String GET_FLOAT = "getFloat";
        public static final String GET_INT = "getInt";
        public static final String GET_LONG = "getLong";
        public static final String GET_SHORT = "getShort";
        public static final String GET_STRING = "getString";
    }

    // functions to set values in NamedParameterStatements.
    public static class NamedParamFns {
        public static final String SET_BYTE_AS_CHAR = "setByte";
        public static final String SET_BOOLEAN = "setBoolean";
        public static final String SET_BYTE = "setByte";
        public static final String SET_BYTES = "setBytes";
        public static final String SET_DATE = "setDate";
        public static final String SET_DOUBLE = "setDouble";
        public static final String SET_FLOAT = "setFloat";
        public static final String SET_INT = "setInt";
        public static final String SET_LONG = "setLong";
        public static final String SET_SHORT = "setShort";
        public static final String SET_STRING = "setString";
        public static final String SET_BOOLEAN_LIST = "setList";
        public static final String SET_BYTE_LIST = "setList";
        public static final String SET_BYTES_LIST = "setList";
        public static final String SET_DATE_LIST = "setList";
        public static final String SET_DOUBLE_LIST = "setList";
        public static final String SET_FLOAT_LIST = "setList";
        public static final String SET_INT_LIST = "setList";
        public static final String SET_LONG_LIST = "setList";
        public static final String SET_SHORT_LIST = "setList";
        public static final String SET_STRING_LIST = "setList";
    }

    // suffixes used in substitutions and code generation.
    public static class Suffixes {
        public static final String SQL = "SQL";
        public static final String STATEMENT = "Stmt";
        public static final String RESULT_SET = "ResultSet";
        public static final String ITEM = "Item";
        public static final String LIST = "List";
        public static final String SQL_FNS = "SQLFns";
        public static final String PREVIOUS = "Previous";
        public static final String CHANGED = "Changed";
        public static final String LAYOUT = "layout";
        public static final String LIST_LAYOUT = "list_layout";
        public static final String LABEL = "label";
        public static final String VALUE = "value";
        public static final String ADAPTER = "Adapter";
        public static final String FRAGMENT = "Fragment";
        public static final String QUERY_PARAMS_FRAGMENT = "QueryParamsFragment";
        public static final String RESULTS_FRAGMENT = "ResultsFragment";
        public static final String RESULTS_LIST_FRAGMENT = "ResultsListFragment";
        public static final String DESCRIPTION = "description";
        public static final String QUERY_PARAMS = "query_params";
        public static final String REPOSITORY = "Repository";
        public static final String VIEW_MODEL = "ViewModel";
        public static final String VIEW_MODEL_FACTORY = "ViewModelFactory";
        public static final String CACHE = "Cache";
        public static final String LIVE_DATA = "LiveData";
        public static final String MUTABLE_LIVE_DATA = "MutableLiveData";
        public static final String CLASS_JAVA = "::class.java";
    }

    // java package names
    public static class Packages {
        public static final String SQL = "sql";
        public static final String PARSE_UTILS = "parseUtils";
        public static final String STRING_UTILS = "StringUtils";
        public static final String UPDATE = "update";
        public static final String QUERY = "query";
        public static final String SERVLET = "servlet";
        public static final String FRAGMENTS = "fragments";
        public static final String ADAPTERS = "adapters";
        public static final String ACTIVITIES = "activities";
        public static final String REPOSITORIES = "repositories";
        public static final String ANDROID = "android";
        public static final String VIEW_MODELS = "viewmodels";
        public static final String NETWORK = "network";
        public static final String DAO = "dao";
        public static final String WILDCARD = "*";
        public static final String REST_SERVER = "RestServer";
    }

    public static class SQLStatementTypes {
        public static final int INSERT = 1;
        public static final int DELETE = 2;
        public static final int UPDATE = 3;
        public static final int SELECT = 4;
    }

    // to parse from strings in URL queries and the command line.
    public static class StringConversionFns {
        public static final String PARSE_DOUBLE = "parseDouble";
        public static final String PARSE_DOUBLE_LIST = "parseDoubleList";
        public static final String PARSE_FLOAT = "parseFloat";
        public static final String PARSE_FLOAT_LIST = "parseFloatList";

        public static final String PARSE_INT = "parseInt";
        public static final String PARSE_INT_LIST = "parseIntegerList";
        public static final String PARSE_BYTE = "parseByte";
        public static final String PARSE_BYTE_LIST = "parseByteList";

        public static final String PARSE_BYTE_AS_CHAR = "parseByteAsChar";
        public static final String PARSE_SHORT = "parseShort";
        public static final String PARSE_SHORT_LIST = "parseShortList";

        public static final String PARSE_LONG = "parseLong";
        public static final String PARSE_LONG_LIST = "parseLongList";

        public static final String PARSE_BOOLEAN = "parseBoolean";
        public static final String PARSE_BOOLEAN_LIST = "parseBooleanList";

        public static final String PARSE_BASE64 = "fromBase64";
        public static final String PARSE_BYTEARRAY_LIST = "parseByteArrayList";
        public static final String PARSE_STRING_LIST = "parseStringList";
        public static final String PARSE_DATE_LIST = "parseDateList";

        public static final String FROM_BASE64 = "fromBase64";

        // TODO Move this to parseUtils
        public static final String PARSE_DATE = "new SimpleDateFormat(Constants.ISO_8601_DATE_FORMAT).parseDate";
    }

    public static class NavigationXML {
        public static final String NAVIGATION = "navigation";
        public static final String ACTION = "action";
        public static final String FRAGMENT = "fragment";
    }

    public static class WebXML {
        public static final String SERVLET = "servlet";
        public static final String SERVLET_NAME = "servlet-name";
        public static final String SERVLET_CLASS = "servlet-class";
        public static final String CONTEXT_PARAM = "context-param";
        public static final String PARAM_NAME = "param-name";
        public static final String PARAM_VALUE = "param-value";
        public static final String URL_PATTERN = "url-pattern";
        public static final String SERVLET_MAPPING = "servlet-mapping";
        public static final String SECURITY_CONSTRAINT = "security-constraint";
        public static final String WEB_RESOURCE_COLLECTION = "web-resource-collection";
        public static final String WEB_RESOURCE_NAME = "web-resource-name";
        public static final String USER_DATA_CONSTRAINT = "user-data-constraint";
        public static final String TRANSPORT_GUARANTEE = "transport-guarantee";
        public static final String CONFIDENTIAL = "CONFIDENTIAL";
        public static final String SECURE_RESOURCES = "Secure Resources";
        public static final String WILDCARD_PATTERN = "/*";
        public static final String[] INIT_PARAM_VALUES = new String[] { Constants.Properties.DATABASE };
        public static final String DESCRIPTION = "description";
        public static final String RESOURCE_REF = "resource-ref";
        public static final String RES_REF_NAME = "res-ref-name";
        public static final String RES_TYPE = "res-type";
        public static final String RES_AUTH = "res-auth";
        public static final String POSTGRES_DATASOURCE = "postgreSQL Datasource";
        public static final String JDBC= "jdbc";
        public static final String JAVAX_SQL_DATASOURCE = "javax.sql.DataSource";
        public static final String CONTAINER = "Container";
    }

    public static class ErrorTypes {
        public static final int ERROR = 0;
        public static final int WARNING = 1;
        public static final int INFO = 2;
    }

    /**
     * Commonly used Regular Expressions
     */
    public static class Regexes {
        public static final String JAVA_PACKAGE = "^[A-Za-z][A-Za-z0-9_]*(\\.[A-Za-z0-9_]+)+[0-9A-Za-z_]$";
        public static final String IDENTIFIER = "^[A-Za-z_][A-Za-z0-9_]*$";
        public static final String SETTINGS_INCLUDE = "[\'\"][:]?[A-Za-z_][A-Za-z0-9_]*[\'\"]|[A-Za-z_][A-Za-z0-9_]*";
        public static final String VARIABLE_REFERENCE = "\\$\\{[A-Za-z_][A-Za-z0-9_]*\\}";
        public static final String NAMED_DOT_PARAM_REGEXP = ":[_A-Za-z][A-Za-z0-9_\\.]*";
    }

    public static class Formats {
        public static final String ISO_8601_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
    }

    public static String[] ErrorNames = new String[] { "ERROR", "WARNING", "INFO" };

    public static class AndroidResources {
        public static final String STRING_ARRAY = "string-array";
        public static final String STRING = "string";
        public static final String ITEM = "item";
        public static final String NAME = "name";
        public static final String RESOURCES = "resources";
        public static final String IC_LAUNCHER_XML = "ic_launcher.xml";
        public static final String IC_LAUNCHER_ROUND_XML = "ic_launcher_round.xml";
        public static final String IC_LAUNCHER_PNG = "ic_launcher.png";
        public static final String IC_LAUNCHER_ROUND_PNG = "ic_launcher_round.png";
        public static final String IC_LAUNCHER_BACKGROUND_XML = "ic_launcher_background.xml";
        public static final String IC_LAUNCHER_FOREGROUND_XML = "ic_launcher_foreground.xml";
        public static final String ENDPOINT_LIST_ACTIVITY_XML = "endpoint_list_activity.xml";
        public static final String ENDPOINT_LIST_FRAGMENT_XML = "endpoint_list_fragment.xml";

        public static final String LIST_LAYOUT_XML = "list_layout.xml";
        public static final String TEXT_ITEM_XML = "text_item.xml";
        public static final String COLORS_XML = "colors.xml";
    }

    public static class AndroidDirs {

        // NOTE: This is questionable.
        public static final String RES = "res";
        public static final String DRAWABLE = "drawable";
        public static final String NAVIGATION = "navigation";
        public static final String DRAWABLE_V24 = "drawable-v24";
        public static final String MIPMAP_ANYDPI_V26 = "mipmap-anydpi-v26";
        public static final String MIPMAP = "mipmap";
        public static final String MIPMAP_HDPI = "mipmap-hdpi";
        public static final String MIPMAP_MDPI = "mipmap-mdpi";
        public static final String MIPMAP_XHDPI = "mipmap-xhdpi";
        public static final String MIPMAP_HXXDPI = "mipmap-xxhdpi";
        public static final String MIPMAP_XXXHDPI = "mipmap-xxxhdpi";
        public static final String VALUES = "values";
        public static final String VALUES_NIGHT = "values-night";
        public static final String VALUES_V21 = "values-v21";
        public static final String VALUES_NIGHT_V21 = "values-night-v21";
        public static final String LAYOUT = "layout";
    }

    public static class AndroidLabels {
        public static final String EXECUTE = "execute";
    }

    public static class AndroidThings {
        public static final String ENDPOINT_FRAGMENT = "EndpointFragment";
        public static final String LIST_ITEM_BINDING = "ListItemBinding";
        public static final String R_ID = "R.id";
        public static final String VIEW = "view";
        public static final String NAVIGATION = "navigation";
    }

    public static class AndroidFns {
        public static final String FIND_VIEW_BY_ID = "findViewById";
        public static final String GET_VIEW = "getView";
    }

    public static class UIElements {
        public static final String CONSTRAINT_LAYOUT = "androidx.constraintlayout.widget.ConstraintLayout";
        public static final String EDIT_TEXT = "EditText";
        public static final String SWITCH = "Switch";
        public static final String CHECK_BOX = "CheckBox";
        public static final String DATE_PICKER = "DatePicker";
        public static final String TIME_PICKER = "TImePicker";
        public static final String TEXT_VIEW = "TextView";
        public static final String NONE = "None";
    }

    public static class AndroidControlImports {
        public static final String EDIT_TEXT = "android.widget.EditText";
        public static final String SWITCH = "android.widget.Switch";
        public static final String TEXT_VIEW = "android.widget.TextView";
        public static final String DATE_PICKER = "android.widget.DatePicker";
        public static final String TIME_PICKER = "android.widget.TimePicker";
    }

    public static class UIValues {
        public static final String CHECKED = "checked";
        public static final String TEXT = "text";
        public static final String PROGRESS = "progress";
    }

    public static class UIProperties {
        public static final String VALUE = "value";
    }

    /**
     * I would much prefer to find some way to get these strings r
     */
    public static class ConstraintLayout {
        public static final String TOP_TO_TOP_OF = "app:layout_constraintTop_toTopOf";
        public static final String START_TO_START_OF = "app:layout_constraintStart_toStartOf";
        public static final String END_TO_END_OF = "app:layout_constraintEnd_toEndOf";
        public static final String TOP_TO_BOTTOM_OF = "app:layout_constraintTop_toBottomOf";
        public static final String BASELINE_TO_BASELINE_OF = "app:layout_constraintBaseline_toBaselineOf";
        public static final String BOTTOM_TO_BOTTOM_OF = "app:layout_constraintBottom_toBottomOf";
        public static final String PARENT = "parent";
        public static final String ID = "android:id";
    }

    public static class AndroidAttributes {
        public static final String ID = "android:id";
    }
    public static class AndroidAttributeValues {
        public static final String GONE = "gone";
    }

    public static class Meanings {
        public static final String GENDER = "gender";
        public static final String FIRST_NAME = "FirstName";
        public static final String LAST_NAME = "lastName";
        public static final String MIDDLE_NAME = "middleName";
        public static final String MIDDLE_INITIAL = "middleInitial";
        public static final String FULL_NAME = "fullName";
        public static final String ADDRESS = "address";
        public static final String STREET_ADDRESS = "streetAddress";
        public static final String CITY = "city";
        public static final String STATE = "state";
        public static final String ZIP = "zip";
        public static final String COUNTRY = "country";
        public static final String DATE = "date";
        public static final String TIME_HOURS = "timeHours";
        public static final String TIME_HOURS_MINUTES = "timeHoursMinutes";
        public static final String TIME_HOURS_MINUTES_SECONDS = "timeHoursMinutesSeconds";
        public static final String RANGE = "range";
        public static final String ON_OFF = "onOff";
        public static final String COLOR = "color";
        public static final String PHONE = "phone";
    }
}
