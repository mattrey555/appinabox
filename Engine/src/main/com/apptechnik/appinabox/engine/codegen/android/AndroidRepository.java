package com.apptechnik.appinabox.engine.codegen.android;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.mixin.CreateMixin;
import com.apptechnik.appinabox.engine.codegen.objects.CreateCommon;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.codegen.pojos.kotlin.KotlinCodeGen;
import com.apptechnik.appinabox.engine.codegen.util.CodegenUtil;
import com.apptechnik.appinabox.engine.parser.create.StatementParser;
import com.apptechnik.appinabox.engine.util.*;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Repository starts off as a network request, but will be extended to use the database for syncing.
 * Split-syncing the repository to make network requests.  Callers should automatically discard unused
 * requests by scoping.
 *
 * parameters are passed, basically, the same as endpoint generation, with Kotlin Decoration.
 * How do we map endpoints to repository functions?  1 per endpoint, 1 for all endpoints,
 * or specify a grouping?
 * Default grouping is by referenced table list.
 * for each endpoint, get the list of referenced tables, they are the group
 * Create a HashMap of referenced tables -> endpoint list.
 * endpoint can be assigned to a repository by directive.
 * repository:
 * Inverse
 */
public class AndroidRepository {

    public static EndpointsMap create(File sqlDir,
                                       StatementParser statementParser,
                                       File outputDir,
                                       File createTableFile,
                                       File propertiesFile) throws Exception {
        EndpointsMap endpointsMap = new EndpointsMap(sqlDir, statementParser, outputDir, createTableFile, propertiesFile);
        ExtraProperties properties = new ExtraProperties(propertiesFile);
        String endpointName = properties.getProp(Constants.Properties.ENDPOINT_NAME, "Android Endpoint API name");
        String packageName = properties.getProp(Constants.Properties.PACKAGE, "package name");
        String repoPackage = OutputPackages.androidRepositories(packageName);
        String networkPackage = OutputPackages.androidNetwork(packageName);
        KotlinCodeGen codegen = new KotlinCodeGen();
        for (String key : endpointsMap.keySet()) {
            List<EndpointsMap.EndpointInfo> endpointList = endpointsMap.get(key);
            String repositoryName = key + Constants.Suffixes.REPOSITORY;
            ClassDefinition classDef = repositoryClass(codegen, repositoryName, endpointName, endpointList);
            StatementList importList = codegen.importStatementList(Constants.Templates.REPOSITORY_IMPORTS);
            importList.addStatement(codegen.importStmt(networkPackage + "." + Constants.Packages.REST_SERVER));
            importList.addStatement(codegen.importStmt(networkPackage + "." + Constants.Packages.REST_SERVER + "." + endpointName));
            codegen.classFile(codegen.packageStmt(repoPackage), importList, classDef).write(outputDir);
        }
        return endpointsMap;
    }

    /**
     * Generate:
     * {@Code
     * class AndroidRepository(val network: Network) {
     * ex:
     *     private var select_all_filmCache : MutableList<com.appinabox.dvdrental.select_all_film.query.select_all_film>? = null
     *     suspend public fun select_all_film(film_id : Int?) {
     *         select_all_filmCache = network.select_all_film(film_id).execute().body()?.toMutableList()
     *     }
     *
     *     }
     * }
     * @param codegen
     * @param repositoryName
     * @param endpointName
     * @param endpointList
     * @return
     */
    private static ClassDefinition repositoryClass(CodeGen codegen,
                                                   String repositoryName,
                                                   String endpointName,
                                                   List<EndpointsMap.EndpointInfo> endpointList) {
        ClassDeclaration classDecl = codegen.classDecl(repositoryName);
        List<FunctionDefinition> methods = endpointList.stream()
                .map(endpoint -> retrofitGetFn(codegen, endpoint))
                .collect(Collectors.toList());
        MemberDeclaration networkMember =
                codegen.memberDecl(Constants.KotlinKeywords.PRIVATE, false, false, true,
                        codegen.classType(endpointName), codegen.var(Constants.Variables.NETWORK), null, null, null);
        Type mutableListData = codegen.genericType(codegen.classType(Constants.KotlinTypes.MUTABLE_LIVE_DATA),
                                                   codegen.simpleType(Constants.KotlinTypes.STRING));

        // private val errorMessage = MutableLiveData<String>();
        MemberDeclaration errorMessageMember =
                codegen.memberDecl(Constants.KotlinKeywords.PUBLIC, false, false,
                                   mutableListData, codegen.var(Constants.KotlinVariables.ERROR_MESSAGE),
                                   codegen.alloc(mutableListData));

        List<MemberDeclaration> members =
                endpointList.stream()
                        .map(CheckedFunction.wrap(e -> repositoryMember(codegen, e)))
                        .filter(Objects::nonNull)
                        .collect(Collectors.toList());
        members.addAll(endpointList.stream()
                .map(CheckedFunction.wrap(e -> visibleRepositoryMember(codegen, e)))
                .filter(Objects::nonNull)
                .collect(Collectors.toList()));
        members.add(errorMessageMember);
        methods.add(getInstanceDef(codegen, repositoryName, endpointName));
        members.add(instanceDeclaration(codegen, repositoryName));

        members.add(networkMember);
        return codegen.classDef(classDecl, members,
                        Collections.singletonList(repositoryConstructor(codegen, endpointName, repositoryName)),
                        methods);
    }

    /**
     * {@code
     * public<name>(network: <api-name>) {
     *     this.network = network
     * }
     * }
     * @param repositoryName
     * @return
     */
    private static ConstructorDefinition repositoryConstructor(CodeGen codegen, String endpointName, String repositoryName) {
        ParameterDeclaration retrofitParam = codegen.paramDecl(codegen.classType(endpointName),
                Constants.Variables.NETWORK);
        ConstructorDeclaration decl = codegen.constructorDecl(repositoryName, codegen.paramList(retrofitParam));
        AssignmentStatement assignThis =
            codegen.assignStmt(codegen.dot(codegen.var(Constants.KotlinKeywords.THIS),
                                           codegen.var(Constants.Variables.NETWORK)),
                               codegen.var(Constants.Variables.NETWORK));
        return codegen.constructorDef(decl, codegen.stmtList(assignThis));
    }

    /**
     * in companion object:
     * private var INSTANCE: {repository-name}? = null;
     * @param codegen
     * @return
     */

    private static MemberDeclaration instanceDeclaration(CodeGen codegen, String repositoryName) {
        Type type = codegen.simpleType(repositoryName, true);
        Variable var = codegen.var(Constants.KotlinVariables.INSTANCE);
        return codegen.memberDecl(Constants.KotlinKeywords.PRIVATE, false, true, false,
                                  type, var, null, null, codegen.nullExpr());
    }

    /**
     * in companion object:
     * fun getInstance(network: Network) {
     *     if (INSTANCE == null) {
     *         INSTANCE = new {repository-name}(dvdRentalApi.getInstance(network))
     *     }
     *     return INSTANCE
     * }
     * @param codegen
     * @param repositoryName
     * @return
     */
    public static FunctionDefinition getInstanceDef(CodeGen codegen, String repositoryName, String endpointName) {
        Variable instanceVar = codegen.var(Constants.KotlinVariables.INSTANCE);
        ParameterDeclaration param = codegen.paramDecl(codegen.classType(Constants.KotlinTypes.NETWORK),
                Constants.KotlinVariables.NETWORK);
        FunctionDeclaration decl = codegen.functionDecl(Constants.KotlinFunctions.GET_INSTANCE, true,
                                                        codegen.paramList(param), codegen.simpleType(repositoryName, true));

        CodeExpression apiGetInstance = codegen.dot(codegen.var(Constants.KotlinTypes.REST_SERVER),
                                                    codegen.methodCall(Constants.KotlinFunctions.GET_INSTANCE, codegen.var(Constants.KotlinVariables.NETWORK)));
        AssignmentStatement assignInstance = codegen.assignStmt(instanceVar, codegen.alloc(codegen.classType(repositoryName), apiGetInstance));
        IfStatement ifStatement = codegen.ifStmt(codegen.binaryExpr(Constants.KotlinOperators.EQUALS, instanceVar, codegen.nullExpr()),
                                                 assignInstance);
        ReturnStatement returnStatement = codegen.returnStmt(instanceVar);
        return codegen.functionDef(decl, codegen.stmtList(ifStatement, returnStatement));
    }

    /**
     * Convert the endpoint declaration to a function definition and calls to the endpoint:
     * ex:
     *      fun getItems(@Query("code") code: Int, @Query("name") name: String) : Call<Result>
     *      suspend fun cachegetItems(code: Int, name: String) {
     *          getItemsCache = retrofitService.getItems(x,y)
     *      }
     *
     * TODO: Add error checking for HTTP response code.
     * @param endpoint
     * @return
     */
    private static FunctionDefinition cacheMethod(CodeGen codegen, EndpointsMap.EndpointInfo endpoint) {
        List<ParameterDeclaration> paramDeclList =
            endpoint.getFn().getParameterList().getParameterDeclarationList().stream()
                    .map(p -> codegen.paramDecl(p.getType(), p.getVariable()))
                    .collect(Collectors.toList());
        ArgumentList argList = codegen.argList(CodegenUtil.declarationsToParameters(paramDeclList));

        // getItemsCache = retrofitService.getItems(x,y)
        //  select_all_filmCache = network.select_all_film(film_id).execute().body()
        String endpointName = endpoint.getFn().getName();
        Variable cacheVar = codegen.var(endpoint.getFn().getName() + Constants.Suffixes.CACHE);
        CodeExpression cacheVal =
                codegen.dot(codegen.var(Constants.Variables.NETWORK),
                        codegen.dot(codegen.methodCall(endpointName, argList),
                                codegen.dot(codegen.methodCall(Constants.Functions.EXECUTE),
                                        codegen.dotNullable(codegen.methodCall(Constants.Variables.BODY),
                                                            codegen.methodCall(Constants.KotlinFunctions.TO_MUTABLE_LIST)))));
        AssignmentStatement cacheAssignment = codegen.assignStmt(cacheVar, cacheVal);

        FunctionDeclaration methodDecl =
            codegen.functionDecl(false, true, Constants.KotlinKeywords.PUBLIC, false, endpoint.getFn().getName(),
                                 null, codegen.paramList(paramDeclList), null, null, null);
        return codegen.functionDef(methodDecl, codegen.stmtList(cacheAssignment));
    }


     /**
     * map
     * {endpoint}() : Call<{type}> to {type}
      *     private var select_film_categoryCache : MutableLiveData<List<com.appinabox.dvdrental.select_film_category.query.select_film_category>> =
      *         MutableLiveData<List<com.appinabox.dvdrental.select_film_category.query.select_film_category>>();
     * @param codegen
     * @param endpoint
     * @return
     */
    private static MemberDeclaration repositoryMember(CodeGen codegen, EndpointsMap.EndpointInfo endpoint) throws Exception {
        Type liveDataType = codegen.genericType(codegen.classType(Constants.KotlinTypes.MUTABLE_LIVE_DATA),
                                                endpoint.endpointReturnBaseType());
        Variable mutableLiveDataVar = codegen.var(endpoint.getFn().getName() + Constants.Suffixes.MUTABLE_LIVE_DATA);
        return codegen.memberDecl(Constants.KotlinKeywords.PRIVATE,  false, false, liveDataType,
                mutableLiveDataVar, codegen.alloc(liveDataType));
    }

    private static MemberDeclaration visibleRepositoryMember(CodeGen codegen, EndpointsMap.EndpointInfo endpoint) throws Exception {
        Type liveDataType = codegen.genericType(codegen.classType(Constants.KotlinTypes.LIVE_DATA),
                                                endpoint.endpointReturnBaseType());
        Variable liveDataVar = codegen.var(endpoint.getFn().getName() + Constants.Suffixes.LIVE_DATA);
        Variable mutableLiveDataVar = codegen.var(endpoint.getFn().getName() + Constants.Suffixes.MUTABLE_LIVE_DATA);
        return codegen.memberDecl(Constants.KotlinKeywords.PUBLIC,  false, false, liveDataType,
                                  liveDataVar, mutableLiveDataVar);
    }

    /**
     * {@Code
     * // shared between queries in the repository.
     * val errorMessage = MutableLiveData<String>()
     * private var listOf{T} = MutableLiveData<List<T>>()
     *
     * public  suspend fun get{T}({params}) : MutableLiveData<List<T>> {
     * // list response:
     * val call = network.{endpoint}({endpoint-args}}
     * call.enqueue(object : Callback<List<T>> {
     *    override fun onResponse(call : Call<List<T>>,
     *                            response : Response<List<T>> ) {
     *      listOf{T}.setValue(response.body());
     *    }
     *    override fun onFailure(call : Call<List<T>>, t : Throwable) {
     *       errorMessage.postValue(null);
     *    }
     * });
     */

    /**
     * public suspend fun get{T}({params}) : MutableLiveData<List<T>> {
     * // list response:
     * call.enqueue(object : Callback<List<T>> {
     *    override fun onResponse(call : Call<List<T>>,
     *                            response : Response<List<T>> ) {
     *      listOf{T}.setValue(response.body());
     *    }
     *    override fun onFailure(call : Call<List<T>>, t : Throwable) {
     *       errorMessage.postValue(null);
     *    }
     * });
     * @param endpoint
     * @return
     */
    public static FunctionDefinition retrofitGetFn(CodeGen codegen, EndpointsMap.EndpointInfo endpoint) {
        Type mutableGenericType = codegen.genericType(codegen.classType(Constants.KotlinTypes.MUTABLE_LIVE_DATA),
                                                      endpoint.endpointReturnBaseType());
        FunctionDeclaration decl =
            codegen.functionDecl(false, true, Constants.KotlinKeywords.PUBLIC,
                             false, endpoint.getFn().getName(), null, endpoint.stripAnnotations(codegen),
                              null, null, null);
        Variable callVar = codegen.var(Constants.KotlinVariables.CALL);
        AssignmentStatement assignCall =
            codegen.typedAssignStmt(callVar, codegen.dot(codegen.var(Constants.KotlinVariables.NETWORK),
                                                CodegenUtil.toMethodCall(codegen, endpoint.getFn())));

        Type callbackType = codegen.genericType(codegen.classType(Constants.KotlinTypes.CALLBACK),
                                                endpoint.endpointReturnBaseType());
        List<FunctionDefinition> fnDefList = new ArrayList<FunctionDefinition>(2);
        fnDefList.add(retrofitOnResponse(codegen, endpoint));
        fnDefList.add(retrofitOnFailure(codegen, endpoint));
        LambdaClass callbackLambda = codegen.lambdaClass(callbackType, fnDefList);
        CodeStatement enqueueStmt = codegen.methodCallStmt(callVar, Constants.KotlinFunctions.ENQUEUE, codegen.argList(callbackLambda));
        return codegen.functionDef(decl, codegen.stmtList(assignCall, enqueueStmt));
    }

    /**
     *    override fun onResponse(call : Call<List<T>>,
     *                            response : Response<List<T>> ) {
     *      listOf{T}.setValue(response.body());
     *    }
     */
    public static FunctionDefinition retrofitOnResponse(CodeGen codegen, EndpointsMap.EndpointInfo endpoint) {

        //  Call<T> -> T -> Response<T>
        Type endpointType = endpoint.endpointReturnBaseType();
        Type responseType = codegen.genericType(codegen.classType(Constants.KotlinTypes.RESPONSE),endpointType);
        ParameterList paramList = codegen.paramList(codegen.paramDecl(endpoint.getFn().getReturnType(),
                                                    codegen.var(Constants.KotlinVariables.CALL)),
                                                    codegen.paramDecl(responseType, Constants.KotlinVariables.RESPONSE));
        FunctionDeclaration functionDecl =
                codegen.functionDecl(true, false, Constants.KotlinKeywords.PUBLIC, false,
                                     Constants.KotlinFunctions.ON_RESPONSE, null, paramList, null,
                             null, null);
        CodeExpression responseBody = codegen.dot(codegen.var(Constants.KotlinVariables.RESPONSE),
                                                  codegen.methodCall(Constants.KotlinFunctions.BODY));

        MethodCall setValueExpr = codegen.methodCall(
            codegen.var(endpoint.getFn().getName() + Constants.Suffixes.MUTABLE_LIVE_DATA),
            Constants.KotlinFunctions.SET_VALUE, responseBody);
        return codegen.functionDef(functionDecl, codegen.stmtList(codegen.methodCallStmt(setValueExpr)));
    }

    /**
     * override fun onFailure(call : Call<List<T>>, t : Throwable) {
     *       errorMessage.postValue(t.message);
     *    }
     */

    public static FunctionDefinition retrofitOnFailure(CodeGen codegen, EndpointsMap.EndpointInfo endpoint) {
        ParameterDeclaration callParam = codegen.paramDecl(endpoint.getFn().getReturnType(),
                                                           codegen.var(Constants.KotlinVariables.CALL));
        ParameterDeclaration throwableParam = codegen.paramDecl(codegen.classType(Constants.KotlinTypes.THROWABLE),
                Constants.KotlinVariables.T);
        ParameterList paramList = codegen.paramList(callParam, throwableParam);
        FunctionDeclaration functionDecl =
            codegen.functionDecl(true, false, Constants.KotlinKeywords.PUBLIC, false,
                    Constants.KotlinFunctions.ON_FAILURE, null, paramList, null,
                    null, null);
        MethodCall postValue =
            codegen.methodCall(codegen.var(Constants.KotlinVariables.ERROR_MESSAGE),
                Constants.KotlinFunctions.POST_VALUE, codegen.dot(codegen.var(Constants.KotlinVariables.T),
                                                                  codegen.var(Constants.KotlinVariables.MESSAGE)));
        StatementList stmtList = codegen.stmtList(codegen.methodCallStmt(postValue));
        return codegen.functionDef(functionDecl, stmtList);
    }
}
