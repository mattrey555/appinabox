package com.apptechnik.appinabox.engine.codegen.android;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.mixin.CreateMixin;
import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;
import com.apptechnik.appinabox.engine.codegen.pojos.ClassFile;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeGen;
import com.apptechnik.appinabox.engine.codegen.pojos.ImportStatement;
import com.apptechnik.appinabox.engine.codegen.update.RequestObjects;
import com.apptechnik.appinabox.engine.parser.pojos.FieldReferenceNode;
import com.apptechnik.appinabox.engine.parser.pojos.MergedFieldNode;
import com.apptechnik.appinabox.engine.parser.pojos.Tree;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Traversal function for INSERT/UPDATE
 */
class AndroidRequestObjects implements Tree.ConsumerException<Stack<Tree<MergedFieldNode>>> {
    private final CreateObjects createObjects;
    private List<ImportStatement> endpointImportStatements;

    public AndroidRequestObjects(CreateObjects createObjects) {
        this.createObjects = createObjects;
        this.endpointImportStatements = new ArrayList<ImportStatement>();
    }

    @Override
    public void accept(Stack<Tree<MergedFieldNode>> stack) throws Exception {
        CodeGen codegen = createObjects.getKotlinCodeGen();
        Tree<MergedFieldNode> currentTreeNode = stack.peek();
        String packageName = createObjects.getQueryPackageName();

        if (!FieldReferenceNode.isSingleField(currentTreeNode)) {
            RequestObjects requestObjects = new RequestObjects(createObjects);
            String importName = packageName + "." + currentTreeNode + "." + Constants.Packages.UPDATE + "." + Constants.Packages.WILDCARD;
            endpointImportStatements.add(codegen.importStmt(importName));
            ClassFile requestClassFile = requestObjects.toKotlinClassFile(codegen, currentTreeNode);
            CreateMixin.writeKotlinMixinClass(codegen, createObjects, requestClassFile);
        }
    }
}
