package com.apptechnik.appinabox.engine.codegen.android;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.exceptions.KeywordNotFoundException;
import com.apptechnik.appinabox.engine.util.CheckedConsumer;
import com.apptechnik.appinabox.engine.util.FileUtil;
import com.apptechnik.appinabox.engine.util.OutputPaths;
import com.apptechnik.appinabox.engine.util.Substitute;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

/**
 * Copy and transform Android resources from template files.
 */
public class AndroidResources {

    /**
     * Copy and transform the resource files for the android application.
     * @param outputDir output directory (all directories relative to this.
     * @param packageName
     * @param properties
     * @throws IOException
     * @throws KeywordNotFoundException
     */
    public static void copyResourceFiles(File outputDir,
                                         String packageName,
                                         Properties properties)
            throws Exception {
        String[] xmlFiles = new String[] { Constants.AndroidResources.IC_LAUNCHER_XML,
                                           Constants.AndroidResources.IC_LAUNCHER_ROUND_XML };
        String[] pngFiles = new String[] { Constants.AndroidResources.IC_LAUNCHER_PNG,
                                           Constants.AndroidResources.IC_LAUNCHER_ROUND_PNG };
        String[] drawableFiles = new String[] { Constants.AndroidResources.IC_LAUNCHER_BACKGROUND_XML };
        String[] drawableV24Files = new String[] { Constants.AndroidResources.IC_LAUNCHER_FOREGROUND_XML };
        String[] colorsFile = new String[] { Constants.AndroidResources.COLORS_XML };

        // copy mipmaps
        OutputPaths.mipmaps(outputDir)
                .forEach(CheckedConsumer.wrap(mipmapDir -> FileUtil.copyResourceDirectory(CreateAndroidApp.class,
                        OutputPaths.androidResourceTemplate(mipmapDir.getName()),
                        pngFiles,
                        mipmapDir)));

        String mipmapResPath = OutputPaths.androidResourceTemplate(Constants.AndroidDirs.MIPMAP_ANYDPI_V26);
        FileUtil.copyResourceDirectory(CreateAndroidApp.class, mipmapResPath, xmlFiles, OutputPaths.androidMipmapAnyDPI26(outputDir));

        // copy drawables
        String drawableResPath = OutputPaths.androidResourceTemplate(Constants.AndroidDirs.DRAWABLE);
        FileUtil.copyResourceDirectory(CreateAndroidApp.class, drawableResPath, drawableFiles, OutputPaths.androidDrawableDir(outputDir));

        String drawableV24ResPath = OutputPaths.androidResourceTemplate(Constants.AndroidDirs.DRAWABLE_V24);
        FileUtil.copyResourceDirectory(CreateAndroidApp.class, drawableV24ResPath, drawableV24Files, OutputPaths.androidDrawableV24Dir(outputDir));

        // copy colors
        String valuesResPath = OutputPaths.androidResourceTemplate(Constants.AndroidDirs.VALUES);
        FileUtil.copyResourceDirectory(CreateAndroidApp.class, valuesResPath, colorsFile, OutputPaths.androidValuesDir(outputDir));


        // transform strings.xml

        File stringsXmlFile = OutputPaths.androidStringsXml(outputDir);
        Substitute.transformResource(Constants.Templates.ANDROID_STRINGS_XML, stringsXmlFile, properties);

        // transform styles.xml (4 flavors)
        File stylesXMLFile = OutputPaths.androidStylesXml(outputDir);
        Substitute.transformResource(Constants.Templates.ANDROID_STYLES_XML, stylesXMLFile, properties);

        File stylesNightXMLFile = OutputPaths.androidNightStylesXml(outputDir);
        Substitute.transformResource(Constants.Templates.ANDROID_STYLES_NIGHT_XML, stylesNightXMLFile, properties);

        File stylesV21XMLFile = OutputPaths.androidNightStylesV21Xml(outputDir);
        Substitute.transformResource(Constants.Templates.ANDROID_STYLES_V21_XML, stylesV21XMLFile, properties);

        File stylesNightV21XMLFile = OutputPaths.androidStylesV21Xml(outputDir);
        Substitute.transformResource(Constants.Templates.ANDROID_STYLES_NIGHT_V21_XML, stylesNightV21XMLFile, properties);

        // transform layouts.
        String layoutResPath = OutputPaths.androidResourceTemplate(Constants.AndroidDirs.LAYOUT);

        String[] layoutFiles = new String[] { Constants.AndroidResources.ENDPOINT_LIST_ACTIVITY_XML,
                                              Constants.AndroidResources.ENDPOINT_LIST_FRAGMENT_XML,
                                              Constants.AndroidResources.LIST_LAYOUT_XML,
                                              Constants.AndroidResources.TEXT_ITEM_XML};
        File layoutDir = OutputPaths.androidLayoutDir(outputDir);
        for (String layout : layoutFiles) {
            String layoutSrcName = layoutResPath + "/" + layout;
            File resFile = new File(layoutDir, layout);
            Substitute.transformResource(layoutSrcName, resFile, properties);
        }

        // transform kotlin files  These only need to have their package directive changed.
        String[] appSrcNames = new String[] { Constants.Templates.ANDROID_MAIN_APPLICATION,
                                              Constants.Templates.ANDROID_ENDPOINT_ACTIVITY,
                                              Constants.Templates.ANDROID_STRING_ADAPTER,
                                              Constants.Templates.ANDROID_ENDPOINT_FRAGMENT };
        for (String appSrcName : appSrcNames) {
            File appSrcDir = Substitute.pathFromPackage(OutputPaths.androidKotlinDir(outputDir), appSrcName, packageName);
            if (!appSrcDir.exists() && !appSrcDir.mkdirs()) {
                throw new Exception("failed to create directory " + appSrcDir.getAbsolutePath());
            }
            File appFile = new File(appSrcDir, Substitute.resourceTemplateToFilename(appSrcName));
            Substitute.transformResource(appSrcName, appFile, properties);
        }
   }
}
