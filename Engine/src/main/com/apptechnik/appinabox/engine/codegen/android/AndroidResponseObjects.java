package com.apptechnik.appinabox.engine.codegen.android;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.mixin.CreateMixin;
import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.codegen.query.ResponseObjects;
import com.apptechnik.appinabox.engine.exceptions.SyncException;
import com.apptechnik.appinabox.engine.parser.pojos.FieldReferenceNode;
import com.apptechnik.appinabox.engine.parser.pojos.QueryFieldsNode;
import com.apptechnik.appinabox.engine.parser.pojos.Tree;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Traversal function for SELECT response objects.
 */
public class AndroidResponseObjects implements Tree.ConsumerException<Stack<Tree<QueryFieldsNode>>> {
    private final CreateObjects createObjects;
    private final List<ImportStatement> endpointImportStatements;

    public AndroidResponseObjects(CreateObjects createObjects) {
        this.createObjects = createObjects;
        this.endpointImportStatements = new ArrayList<ImportStatement>();
    }

    @Override
    public void accept(Stack<Tree<QueryFieldsNode>> stack) throws Exception {
        CodeGen codegen = createObjects.getKotlinCodeGen();
        Tree<QueryFieldsNode> currentTreeNode = stack.peek();
        String packageName = createObjects.getQueryPackageName();

        if (!FieldReferenceNode.isSingleField(currentTreeNode)) {
            ResponseObjects responseObjects = new ResponseObjects(createObjects);

            // NOTE: how can this be correct if createObjects.getQueryPackageName() already contains Constants.Packages.QUERY?
            String importName = packageName + "." + currentTreeNode + "." + Constants.Packages.QUERY + "." + Constants.Packages.WILDCARD;
            endpointImportStatements.add(codegen.importStmt(importName));
            ClassDefinition classDef = responseObjects.generateObjectNoMethods(codegen, currentTreeNode);

            // Android Navigation requires Serializable to pass data.
            classDef.getClassDeclaration().getImplementsList().addType(new SimpleType(Constants.JavaTypes.SERIALIZABLE));

            // If the data is synchronized between server and client, then add a DAO annotation to the class,
            if (createObjects.getSqlFile().findTag(Constants.Directives.SYNC) != null) {
                classDef.getClassDeclaration().addAnnotation(currentTreeNode.get().createEntityAnnotation(codegen));
            }
            ClassFile responseClassFile = responseObjects.toKotlinClassFile(codegen, currentTreeNode, classDef);
            CreateMixin.writeKotlinMixinClass(codegen, createObjects, responseClassFile);
        }
    }
}
