package com.apptechnik.appinabox.engine.codegen.android;

public class AndroidUtil {

    /**
     * Return @id/${id}
     * @param id
     */
    public static String idRef(String id) {
        int ichSlash = id.indexOf('/');
        return (ichSlash >= 0) ? "@id/" + id.substring(ichSlash + 1) : "@id/" + id;
    }

    /**
     * Return @id/${id}
     * @param id
     */
    public static String addId(String id) {
        return "@+id/" + id;
    }
}
