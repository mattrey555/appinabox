package com.apptechnik.appinabox.engine.codegen.android;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.exceptions.PropertiesException;
import com.apptechnik.appinabox.engine.util.FileUtil;
import com.apptechnik.appinabox.engine.util.OutputPaths;
import com.apptechnik.appinabox.engine.util.Substitute;
import com.apptechnik.appinabox.engine.util.XMLUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.*;

public class AndroidWidgets {
    private final CreateObjectsAndroid createObjectsAndroid;

    public AndroidWidgets(CreateObjectsAndroid createObjectsAndroid) {
        this.createObjectsAndroid = createObjectsAndroid;
    }

    /**
     * Define the sqlTypeNames which can be mapped to elements for display and entry.
     * For example, a boolean value can be represented with a switch, a checkbox, a spinner,
     * or an EditText
     */
    private static class UIType {
        protected final String[] sqlTypeNames;                  // SQL Type name (VARCHAR, INTEGER, etc)
        protected final Element[] uiElementEntry;               // Android Control used for entry
        protected final Element[] uiElementDisplay;             // and display.

        public UIType(String[] sqlTypeNames, Element[] uiElementEntry, Element[] uiElementDisplay) {
            this.sqlTypeNames = sqlTypeNames;
            this.uiElementEntry = uiElementEntry;
            this.uiElementDisplay = uiElementDisplay;
        }
    }

    private static class Element {
        private final String name;          // Android Element/Control name
        private final String template;
        private final String[] imports;

        public Element(String name, String template, String[] imports) {
            this.name = name;
            this.template = template;
            this.imports = imports;
        }

        public Element(String name, String template, String importPackage) {
            this.name = name;
            this.template = template;
            this.imports = new String[] { importPackage };
        }

        public String getName() {
            return name;
        }

        public String getTemplate() {
            return template;
        }

        public String[] getImports() {
            return imports;
        }
    }

    /**
     * Some elements like TextView, can get/set single values through XML and be used for databinding:
     * android:text="${viewmodel.text}
     * Others, like datepicker, have multiple values:
     *      android:year="@{viewModel.year}"
     *      android:month="@{viewModel.month}"
     *      android:day="@{viewModel.day}"
     * which have to be mapped to/from an actual date through a custom function.
     * ex:
     * android:year="@{viewModel.year(viewModel.date)}
     * custom types like a generated ViewModel, used in fragments displaying scalar data:
     * <layout xmlns:android="http://schemas.android.com/apk/res/android">
     * <data>
     *      <variable
     *             name="viewModel"
     *             type="${PACKAGE}.${TYPE}ViewModel" />
     * </data>
     * or in a fragment:
     * <layout xmlns:android="http://schemas.android.com/apk/res/android">
     *   <data>
     *      <variable
     *          name="${TYPE}"
     *          type="${PACKAGE}.${TYPE}" />
     *   </data>
     *  ... XML layout...
     *  </layout>
     *  See https://developer.android.com/topic/libraries/data-binding/expressions
     */
    public static class ElementProperties {
        private final Map<String, String> properties;

        public ElementProperties() {
            this.properties = new HashMap<String, String>();
        }

        public Map<String, String> getProperties() {
            return properties;
        }

        public String getProperty(String name) {
            return properties.get(name);
        }

        public void addProperty(String name, String property) {
            properties.put(name, property);
        }
    }

    private static class ElementPropertiesMap {
        private final Map<String, ElementProperties> propertiesMap;

        public ElementPropertiesMap() {
            this.propertiesMap = new HashMap<String, ElementProperties>();
        }

        public void addProperties(String name, ElementProperties elementProperties) {
            propertiesMap.put(name, elementProperties);
        }

        public ElementProperties getProperties(String name) {
            return propertiesMap.get(name);
        }
    }

    /**
     * Because there are so many ways to build an expression to populate and extract values from
     * a control (DatePicker is an example), but that the widget and value are constant, it's best
     * to use a template in this case, even though the widget reference and value are generated
     * expressions, so flexibility overrides implementation consistency.  It would be possible
     * to have generated expressions for the to/from expressions instead of templates, but then,
     * we couldn't accept templates from JSON, and the long-term intent is to allow callers to
     * extend the converter library for custom widgets.
     * The general rule for a converter is that "to" takes an Android View as a parameter and
     * returns the expected type, and "from" takes a value and populates the Android View.
     * The problem with initialization in that you can initialize a widget with data from
     * several sources, for example, a dropdown could take:
     *   - Fixed list of primitives
     *   - Fixed list of primitives with descriptive strings
     *   - SQL query of locally synced data.
     *   - network request.
     * or a slider:
     *   - fixed min,max
     *   - SQL query of locally synced data.
     *   - network request.
     *  And these will be different for each SQL reference, but unlikely different in the
     *  application, meaning that you won't edit a phone number with a TextEdit and PhoneConverter
     *  on one screen, and a dropdown and the results of a SQL query in another.
     *  There's more complexity
     * "widget": "EditText",
     *       "converters": [
     *          {
     *             "int": {
     *                "to": "${VALUE} = Integer.parseInt(${WIDGET} as EditText).text.toString()",
     *                "from": "(${WIDGET} as EditText).setString(Integer.toString(${VALUE}))",
     *                "imports": [
     *                   "java.lang.Integer",
     *                   "android.widget.EditText"
     *                ]
     *             }
     *          },
     *
     */
    public static class Converters {
        private String widget;
        private List<TypeConverter> converters;
        private static List<Converters> CONVERTERS_INSTANCE = null;

        public Converters(String widget, List<TypeConverter> converters) {
            this.widget = widget;
            this.converters = converters;
        }

        public String getWidget() {
            return widget;
        }

        public void setWidget(String widget) {
            this.widget = widget;
        }

        public List<TypeConverter> getConverters() {
            return converters;
        }

        public void setConverters(List<TypeConverter> converters) {
            this.converters = converters;
        }

        public static List<Converters> getInstance() throws IOException {
            if (CONVERTERS_INSTANCE == null) {
                CONVERTERS_INSTANCE = readConverterList(Constants.Templates.WIDGET_CONVERTERS);
            }
            return CONVERTERS_INSTANCE;
        }

        public static List<TypeConverter> getTypeConverter(String widget) throws IOException {
            Converters converters = getInstance().stream()
                    .filter(c -> c.widget.equals(widget))
                    .findFirst()
                    .orElse(null);
            return (converters != null) ? converters.getConverters() : null;
        }

        public static TypeConverter getFromTypeConverter(String widget, String type) throws IOException {
            List<TypeConverter> typeConverters = getTypeConverter(widget);
            if (typeConverters != null) {
                return typeConverters.stream()
                        .filter(c -> c.type.equals(type))
                        .findFirst()
                        .orElse(null);
            }
            return null;
        }

        public static TypeConverter getToTypeConverter(String widget, String type) throws IOException {
            List<TypeConverter> typeConverters = getTypeConverter(widget);
            if (typeConverters != null) {
                return typeConverters.stream()
                        .filter(c -> c.type.equals(type))
                        .findFirst()
                        .orElse(null);
            }
            return null;
        }

        public static List<String> getTypeConverterImports(String widget, String type) throws IOException {
            TypeConverter converter = getFromTypeConverter(widget, type);
            if (converter != null) {
                return converter.getImports();
            }
            return null;
        }

        public static String getFromExpression(String widget, String type, String widgetExpr, String value)
        throws IOException, PropertiesException {
            TypeConverter typeConverter = getFromTypeConverter(widget, type);
            Properties properties = new Properties();
            properties.put(Constants.Substitutions.WIDGET, widgetExpr);
            properties.put(Constants.Substitutions.VALUE, value);
            return Substitute.transformString(typeConverter.from, properties);
        }

        public static String getToExpression(String widget, String type, String widgetExpr, String value)
                throws IOException, PropertiesException {
            TypeConverter typeConverter = getFromTypeConverter(widget, type);
            Properties properties = new Properties();
            properties.put(Constants.Substitutions.WIDGET, widgetExpr);
            properties.put(Constants.Substitutions.VALUE, value);
            return Substitute.transformString(typeConverter.to, properties);
        }

        /**
         * Read the list of converters from widget-converters.json
         * @param resourceName
         * @return
         * @throws IOException
         */
        public static List<Converters> readConverterList(String resourceName) throws IOException {
            String json = FileUtil.getResource(AndroidWidgets.class, resourceName);
            Gson gson = new Gson();

            Type userListType = new TypeToken<ArrayList<Converters>>(){}.getType();
            return gson.fromJson(json, userListType);
        }

    }

    private static class TypeConverter {
        private String type;
        private String to;
        private String from;
        private List<String> imports;

        public TypeConverter(String type, String to, String from, List<String> imports) {
            this.type = type;
            this.to = to;
            this.from = from;
            this.imports = imports;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getTo() {
            return to;
        }

        public void setTo(String to) {
            this.to = to;
        }

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public List<String> getImports() {
            return imports;
        }

        public void setImports(List<String> imports) {
            this.imports = imports;
        }
    }



    public static ElementPropertiesMap createElementPropertiesMap() {
        ElementPropertiesMap elementPropertiesMap = new ElementPropertiesMap();
        ElementProperties textProperties = new ElementProperties();
        textProperties.addProperty(Constants.UIProperties.VALUE, Constants.UIValues.TEXT);
        elementPropertiesMap.addProperties(Constants.UIElements.TEXT_VIEW, textProperties);

        ElementProperties editTextProperties = new ElementProperties();
        editTextProperties.addProperty(Constants.UIProperties.VALUE, Constants.UIValues.TEXT);
        elementPropertiesMap.addProperties(Constants.UIElements.EDIT_TEXT, editTextProperties);

        ElementProperties switchProperties = new ElementProperties();
        switchProperties.addProperty(Constants.UIProperties.VALUE, Constants.UIValues.CHECKED);
        elementPropertiesMap.addProperties(Constants.UIElements.SWITCH, switchProperties);
        return elementPropertiesMap;
    }

    private static ElementPropertiesMap UI_PROPERTIES_MAP = createElementPropertiesMap();

    public static ElementProperties getElementProperties(String name) {
        return UI_PROPERTIES_MAP.getProperties(name);
    }

    /**
     * Find the matching UI type for this SQL type.
     * @param sqlType VARCHAR, character varying, INTEGER, etc.
     * @return
     */
    private static UIType getUIType(String sqlType) {
        for (int i = 0; i < UI_TYPE_LIST.length; i++) {
            if (Arrays.stream(UI_TYPE_LIST[i].sqlTypeNames).anyMatch(sqlType::equalsIgnoreCase)) {
                return UI_TYPE_LIST[i];
            }
        }
        return null;
    }

    public static String[] getDisplay(String sqlType) {
        return Arrays.stream(getUIType(sqlType).uiElementDisplay).map(el -> el.getName()).toArray(String[]::new);
    }

    public static String[] getDisplayTemplate(String sqlType) {
        return Arrays.stream(getUIType(sqlType).uiElementDisplay).map(el -> el.getTemplate()).toArray(String[]::new);
    }

    public static String[] getEntry(String sqlType) {
        return Arrays.stream(getUIType(sqlType).uiElementEntry).map(el -> el.getName()).toArray(String[]::new);
    }

    public static String[] getEntryTemplate(String sqlType) {
        return Arrays.stream(getUIType(sqlType).uiElementEntry)
                .map(el -> el.getTemplate())
                .toArray(String[]::new);
    }

    public static String[] getEntryImports(String sqlType) {
        return Arrays.stream(getUIType(sqlType).uiElementEntry)
                .map(el -> el.getImports())
                .flatMap(imports -> Arrays.stream(imports))
                .toArray(String[]::new);
    }

    public static String[] getDisplayImports(String sqlType) {
        return Arrays.stream(getUIType(sqlType).uiElementDisplay)
                .map(el -> el.getImports())
                .flatMap(imports -> Arrays.stream(imports))
                .toArray(String[]::new);
    }

    /**
     *
     * Map of SQLTypes to allowed UIElement types.
     * Multiple SQLTypes can be mapped to element types, for example, INTEGER and LONG can both use EditText.
     */
    private static UIType[] UI_TYPE_LIST = new UIType[] {
            new UIType(new String[] { Constants.SQLTypes.BIT,
                                      Constants.SQLTypes.BOOLEAN },
                    new Element[] { new Element(Constants.UIElements.SWITCH,
                                                Constants.LayoutTemplates.SWITCH_XML,
                                                Constants.AndroidControlImports.SWITCH) },
                    new Element[] { new Element(Constants.UIElements.SWITCH,
                                                Constants.LayoutTemplates.SWITCH_XML,
                                                Constants.AndroidControlImports.SWITCH) }),
            new UIType(new String[] { Constants.SQLTypes.TINYINT,
                                      Constants.SQLTypes.SMALLINT,
                                      Constants.SQLTypes.INTEGER,
                                      Constants.SQLTypes.LONG,
                                      Constants.SQLTypes.SERIAL,
                                      Constants.SQLTypes.REAL,
                                      Constants.SQLTypes.FLOAT,
                                      Constants.SQLTypes.BIGINT,
                                      Constants.SQLTypes.DOUBLE,
                                      Constants.SQLTypes.NUMERIC,
                                      Constants.SQLTypes.DECIMAL,
                                      Constants.SQLTypes.CHAR,
                                      Constants.SQLTypes.NCHAR,
                                      Constants.SQLTypes.VARCHAR,
                                      Constants.SQLTypes.NVARCHAR,
                                      Constants.SQLTypes.TEXT,
                                      Constants.SQLTypes.CHARACTER_VARYING,
                                      Constants.SQLTypes.CHARACTER,
                                      Constants.SQLTypes.VARYING,
                                      Constants.SQLTypes.LONGVARCHAR,
                                      Constants.SQLTypes.NLONGVARCHAR,
                                      Constants.SQLTypes.BINARY,
                                      Constants.SQLTypes.VARBINARY,
                                      Constants.SQLTypes.LONGVARBINARY,
                                      Constants.SQLTypes.NULL,
                                      Constants.SQLTypes.OTHER,
                                      Constants.SQLTypes.JAVA_OBJECT,
                                      Constants.SQLTypes.DISTINCT,
                                      Constants.SQLTypes.STRUCT,
                                      Constants.SQLTypes.ARRAY,
                                      Constants.SQLTypes.BLOB,
                                      Constants.SQLTypes.CLOB,
                                      Constants.SQLTypes.NCLOB,
                                      Constants.SQLTypes.REF,
                                      Constants.SQLTypes.TSVECTOR,
                                      Constants.SQLTypes.TSQUERY,
                                      Constants.SQLTypes.STRING,
                                      Constants.SQLTypes.INTERVAL,
                                      Constants.SQLTypes.ANY,
                                      Constants.SQLTypes.ANY_NUMBER},
                    new Element[] { new Element(Constants.UIElements.EDIT_TEXT,
                            Constants.LayoutTemplates.EDIT_TEXT_XML,
                            Constants.AndroidControlImports.EDIT_TEXT) },
                    new Element[] { new Element(Constants.UIElements.TEXT_VIEW,
                                                Constants.LayoutTemplates.TEXT_VIEW_XML,
                                                Constants.AndroidControlImports.TEXT_VIEW) }),

        new UIType(new String[] { Constants.SQLTypes.DATE,
                                  Constants.SQLTypes.SMALLDATETIME,
                                  Constants.SQLTypes.DATETIME,
                                  Constants.SQLTypes.DATETIMEOFFSET,
                                  Constants.SQLTypes.TIMESTAMP},
                   new Element[] { new Element(Constants.UIElements.DATE_PICKER,
                                               Constants.LayoutTemplates.DATE_PICKER_XML,
                                               Constants.AndroidControlImports.DATE_PICKER) },
                   new Element[] { new Element(Constants.UIElements.TEXT_VIEW,
                                               Constants.LayoutTemplates.TEXT_VIEW_XML,
                                               Constants.AndroidControlImports.TEXT_VIEW)}),
        new UIType(new String[] { Constants.SQLTypes.TIME,
                                  Constants.SQLTypes.SMALLDATETIME},
                   new Element[] { new Element(Constants.UIElements.TIME_PICKER,
                                Constants.LayoutTemplates.TIME_PICKER_XML,
                                Constants.AndroidControlImports.TIME_PICKER) },
                   new Element[] { new Element(Constants.UIElements.TEXT_VIEW,
                                               Constants.LayoutTemplates.TEXT_VIEW_XML,
                                               Constants.AndroidControlImports.TEXT_VIEW)})
    };

    public static String templateFromUIElement(String uiElement) {
        return OutputPaths.androidLayoutResourceTemplate(uiElement);
    }

    /**
     * for a name and sqlType, generate the UI element for data entry.
     * @param doc XML document to add node.
     * @param properties  NOTE: this changes the "name" field in properties.
     * @param name parameter name.
     * @param sqlType SQL type.
     * @return XML Node
     * @throws Exception
     */
    public Node createUIEntry(Document doc, Properties properties, String name, String sqlType) throws Exception {
        String[] entryWidgetNames = AndroidWidgets.getEntryTemplate(sqlType);
        properties.put(Constants.Substitutions.NAME, name);

        // TODO: allow specification from .sql for widget type.
        String templateName = AndroidWidgets.templateFromUIElement(entryWidgetNames[0]);
        return XMLUtils.addChildFromTransformedResource(doc, Constants.UIElements.CONSTRAINT_LAYOUT,
                null, templateName, properties);
    }

    public Node createUIEntry(Document doc, String name, String sqlType) throws Exception {
        String[] entryWidgetNames = AndroidWidgets.getEntryTemplate(sqlType);
        Properties properties = new Properties();
        properties.put(Constants.Substitutions.NAME, name);

        // TODO: allow specification from .sql for widget type.
        String templateName = AndroidWidgets.templateFromUIElement(entryWidgetNames[0]);
        return XMLUtils.addChildFromTransformedResource(doc, Constants.UIElements.CONSTRAINT_LAYOUT,
                null, templateName, properties);
    }
    /**
     * Create a text label.
     * @param doc document to insert the text label into.
     * @param name actually widget ID
     * @param label widget label (name)
     * @return
     * @throws Exception
     */
    public Node createUILabel(Document doc, String name, String label) throws Exception {
        return createUIEntry(doc, Constants.LayoutTemplates.TEXT_VIEW_XML, name, label);
    }

    public Node createUIButton(Document doc, String name, String label) throws Exception {
        return createUIEntry(doc, Constants.LayoutTemplates.BUTTON_XML, name, label);
    }

    public Node createUIEntry(Document doc, String templateName, String name, String label) throws Exception {
        Properties properties = new Properties();
        properties.put(Constants.Substitutions.NAME, name + "_" + Constants.Suffixes.LABEL);
        properties.put(Constants.Substitutions.DESCRIPTION, label);

        createObjectsAndroid.getStringMap().addEntry(label, label);
        String template = OutputPaths.androidLayoutResourceTemplate(templateName);
        return XMLUtils.addChildFromTransformedResource(doc, Constants.UIElements.CONSTRAINT_LAYOUT,
                null, template, properties);
    }

}
