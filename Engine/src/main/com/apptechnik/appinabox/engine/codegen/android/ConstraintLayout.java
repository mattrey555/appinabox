package com.apptechnik.appinabox.engine.codegen.android;

import com.apptechnik.appinabox.engine.Constants;
import org.w3c.dom.Element;

/**
 * XML attributes for constraints in a constraint layout.
 */
public class ConstraintLayout {

    public static void applyLabelConstraints(Element prevLabelNode, Element labelNode) {
        labelNode.setAttribute(Constants.ConstraintLayout.START_TO_START_OF, Constants.ConstraintLayout.PARENT);
        if (prevLabelNode == null) {
            labelNode.setAttribute(Constants.ConstraintLayout.TOP_TO_TOP_OF, Constants.ConstraintLayout.PARENT);
        } else {
            String prevChildId = prevLabelNode.getAttribute(Constants.ConstraintLayout.ID);
            labelNode.setAttribute(Constants.ConstraintLayout.TOP_TO_BOTTOM_OF, AndroidUtil.idRef(prevChildId));
        }
    }
    public static void applyLabelConstraints(String prevLabelId, Element labelNode) {
        labelNode.setAttribute(Constants.ConstraintLayout.START_TO_START_OF, Constants.ConstraintLayout.PARENT);
        if (prevLabelId == null) {
            labelNode.setAttribute(Constants.ConstraintLayout.TOP_TO_TOP_OF, Constants.ConstraintLayout.PARENT);
        } else {
            labelNode.setAttribute(Constants.ConstraintLayout.TOP_TO_BOTTOM_OF, AndroidUtil.idRef(prevLabelId));
        }
    }

    public static void applyEditConstraints(Element prevChildNode, Element labelNode, Element childNode) {
        childNode.setAttribute(Constants.ConstraintLayout.END_TO_END_OF, Constants.ConstraintLayout.PARENT);
        String labelId = labelNode.getAttribute(Constants.ConstraintLayout.ID);
        childNode.setAttribute(Constants.ConstraintLayout.BASELINE_TO_BASELINE_OF, AndroidUtil.idRef(labelId));
        if (prevChildNode == null) {
            childNode.setAttribute(Constants.ConstraintLayout.TOP_TO_TOP_OF, Constants.ConstraintLayout.PARENT);
        } else {
            String prevChildId = prevChildNode.getAttribute(Constants.ConstraintLayout.ID);
            childNode.setAttribute(Constants.ConstraintLayout.TOP_TO_BOTTOM_OF, AndroidUtil.idRef(prevChildId));
        }
    }

    public static void applyButtonConstraints(Element prevChildNode, Element childNode) {
        if (prevChildNode == null) {
            childNode.setAttribute(Constants.ConstraintLayout.TOP_TO_TOP_OF, Constants.ConstraintLayout.PARENT);
        } else {
            String prevChildId = prevChildNode.getAttribute(Constants.ConstraintLayout.ID);
            childNode.setAttribute(Constants.ConstraintLayout.TOP_TO_BOTTOM_OF, AndroidUtil.idRef(prevChildId));
        }
        childNode.setAttribute(Constants.ConstraintLayout.END_TO_END_OF, Constants.ConstraintLayout.PARENT);
        childNode.setAttribute(Constants.ConstraintLayout.BOTTOM_TO_BOTTOM_OF, Constants.ConstraintLayout.PARENT);
    }
}
