package com.apptechnik.appinabox.engine.codegen.android;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;
import com.apptechnik.appinabox.engine.codegen.objects.TraverseQueryObjects;
import com.apptechnik.appinabox.engine.parser.ColumnInfo;
import com.apptechnik.appinabox.engine.parser.pojos.QueryFieldsNode;
import com.apptechnik.appinabox.engine.parser.pojos.Tree;
import com.apptechnik.appinabox.engine.util.ExtraProperties;
import com.apptechnik.appinabox.engine.util.OutputPaths;
import com.apptechnik.appinabox.engine.util.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.Properties;
import java.util.Stack;


/**
 * Create the layouts for adapter items.  Recurse the object hierarchy, generate a unique file name, and
 * create the variables and
 */
public class CreateAdapterItemLayoutXML extends TraverseQueryObjects {
    private final File outputDir;
    private final DocumentBuilder builder;

    public CreateAdapterItemLayoutXML(File outputDir, CreateObjectsAndroid createObjectsAndroid) throws Exception {
        super(createObjectsAndroid);
        this.outputDir = outputDir;
        this.builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    }

    /**
     * Create the data binding statements for this response node, maxing out at maxItems.
     * @param stack path to this node.
     */
    public void accept(Stack<Tree<QueryFieldsNode>> stack) throws Exception {
        QueryFieldsNode node = stack.peek().get();
        String layoutName = node.getItemLayoutName(getCreateObjects(), stack);
        int maxItems = Math.min(node.getColumnInfos().size(), ListItems.getMaxItems());
        ExtraProperties itemProperties = new ExtraProperties();
        itemProperties.put(Constants.Substitutions.NAME, node.getVarName());
        if (!node.isSingleField()) {
            itemProperties.put(Constants.Substitutions.VARIABLES,
                               fieldReference(node.getVarName(), getCreateObjects().getQueryPackageName(), node.getClassName()));
        } else {
            ColumnInfo colInfo = node.getColumnInfos().get(0);

            // CRITICAL NOTE: the JavaCodeGen is intentional.  data binding expects java types.
            itemProperties.put(Constants.Substitutions.VARIABLES, varReference(colInfo.getColumnName(true),
                                colInfo.getCodeType(getCreateObjects().getJavaCodeGen(), false).toString()));
        }
        // for databinding set <property>=${<value-reference>}
        for (int item = 0; item < maxItems; item++) {
            ColumnInfo columnInfo = node.getColumnInfos().get(item);
            bindElement(itemProperties, node, columnInfo, item);
        }
        String listElementName = ListItems.getListElements(maxItems)[0];
        Document itemDoc = XMLUtils.fromTransformedResource(builder, listElementName, itemProperties);
        XMLUtils.save(new File(OutputPaths.androidLayoutDir(outputDir), layoutName), itemDoc);
    }

    /**
     * @param itemProperties properties for substitution
     * @param responseNode node containing fields to display (used for binding reference @{var.field}
     * @param columnInfo SQL column information for field
     * @param item item index for uniqueness in properties.
     */
    private static void bindElement(ExtraProperties itemProperties,
                                    QueryFieldsNode responseNode,
                                    ColumnInfo columnInfo,
                                    int item) {
        String[] widgets = AndroidWidgets.getDisplay(columnInfo.getDataType());
        itemProperties.put(Constants.Substitutions.WIDGET + (item + 1), widgets[0]);
        itemProperties.put(Constants.Substitutions.VAR + (item + 1), columnInfo.getColumnName(true));

        // TODO: Add widget-specific properties later.
        // ACTUALLY NOW because TimePicker fails for getElementProperties
        // We select the first widget. Later, we'll take directives from the SQLFIle
        AndroidWidgets.ElementProperties elementProperties = AndroidWidgets.getElementProperties(widgets[0]);
        String bind;
        if (!responseNode.isSingleField()) {
            bind = bindVariable(elementProperties.getProperty(Constants.UIProperties.VALUE),
                                responseNode.getVarName(), columnInfo.getColumnName(true));
        } else {
            bind = bindSingleVariable(elementProperties.getProperty(Constants.UIProperties.VALUE),
                                       columnInfo.getColumnName());
        }
        itemProperties.put(Constants.Substitutions.WIDGET_PROPERTIES + (item + 1), bind);
    }

    /*
     * <variable
     *  name="${TYPE}"
     *  type="${PACKAGE}.${TYPE}" />
     */
    private static String fieldReference(String varName, String packageName, String typeName) {
        return String.format("<variable name=\"%s\" type=\"%s.%s\" />", varName, packageName, typeName);
    }

    /*
     * <variable
     *  name="${TYPE}"
     *  type="${PACKAGE}.${TYPE}" />
     */
    private static String varReference(String varName, String typeName) {
        return String.format("<variable name=\"%s\" type=\"%s\" />", varName, typeName);
    }

    /**
     * TODO: for more complex widgets this will have to refer to getter/setter methods
     * Generate the binding expression for android data binding:
     * ex:android:text="@{user.lastName}"
     * @param widgetValue
     * @param varName
     * @return
     */
    private static String bindVariable(String widgetValue, String objectName, String varName) {
        return String.format("android:%s=\"@{%s.%s}\"", widgetValue, objectName, varName);
    }


    /**
     * TODO: for more complex widgets this will have to refer to getter/setter methods
     * Generate the binding expression for android data binding:
     * ex:android:text="@{film_id}"
     * @param widgetValue
     * @param varName
     * @return
     */
    public static String bindSingleVariable(String widgetValue, String varName) {
        return String.format("android:%s=\"@{%s}\"", widgetValue, varName);
    }
}
