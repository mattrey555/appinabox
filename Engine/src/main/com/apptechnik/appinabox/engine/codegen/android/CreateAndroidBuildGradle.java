package com.apptechnik.appinabox.engine.codegen.android;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.gradle.CreateSettingsGradle;
import com.apptechnik.appinabox.engine.exceptions.PropertiesException;
import com.apptechnik.appinabox.engine.util.ExtraProperties;
import com.apptechnik.appinabox.engine.util.FileUtil;
import com.apptechnik.appinabox.engine.util.PropertiesUtil;
import com.apptechnik.appinabox.engine.util.Substitute;

import java.io.*;
import java.text.ParseException;
import java.util.Properties;

/**
 * Create the gradle file including the code generation for each servlet
 * The servlets are specified in SQL files. The name of the SQL file is the name of the outputted servlet.
 */
public class CreateAndroidBuildGradle {

	public static void main(String[] args)
	throws IOException, ParseException, PropertiesException {
		if (args.length < 4) {
			System.err.println("usage: output-directory sql-file-directory create-tables-file properties-file");
			System.exit(-1);
		}
		String outputDirName = args[0];
		String statementsFileDirName = args[1];
		String createTablesFile = args[2];
		String propertiesFileName = args[3];
		File statementsFileDir = FileUtil.checkDirectory("SQL statements directory", statementsFileDirName);
		File outputDir = FileUtil.checkDirectory("output directory", outputDirName);
		File androidOutputDir = FileUtil.checkDirectory("Android output directory",
														new File(outputDirName, Constants.Directories.ANDROID));
		File androidLibraryOutputDir = FileUtil.checkDirectory("Android library output directory",
																new File(outputDirName, Constants.Directories.ANDROID_LIBRARY));
		File androidAppOutputDir = FileUtil.checkDirectory("Android application output directory",
															new File(outputDirName, Constants.Directories.ANDROID_APP));

		File androidGradleFile = new File(androidOutputDir, Constants.Files.BUILD_GRADLE);
		File androidGradlePropertiesFile = new File(androidOutputDir, Constants.Files.GRADLE_PROPERTIES);
		File androidLibraryGradleFile = new File(androidLibraryOutputDir, Constants.Files.BUILD_GRADLE);
		File androidAppGradleFile = new File(androidAppOutputDir, Constants.Files.BUILD_GRADLE);
		File androidSettingsGradleFile = new File(androidOutputDir, Constants.Files.SETTINGS_GRADLE);

		File propertiesFile = new File(outputDir, propertiesFileName);

		System.out.println("output directory name = " + outputDirName + "\n" +
						   "statements file directory = " + statementsFileDirName + "\n" +
				           "Create Tables File = " + createTablesFile + "\n" +
						   "properties file name = " + propertiesFile);
		ExtraProperties propertyMap = new ExtraProperties(propertiesFile);
		propertyMap.setProperty(Constants.Substitutions.CREATE_TABLES, createTablesFile);
		propertyMap.setProperty(Constants.Substitutions.STATEMENT_FILES_DIR, statementsFileDirName);
		propertyMap.setProperty(Constants.Substitutions.PROPERTIES_FILE, propertiesFileName);
		propertyMap.setProperty(Constants.Substitutions.OUTPUT_DIR, outputDirName);
		Substitute.transformResource(Constants.Templates.ANDROID_LIBRARY_BUILD_GRADLE, androidLibraryGradleFile, propertyMap, false);
		Substitute.transformResource(Constants.Templates.ANDROID_APP_BUILD_GRADLE, androidAppGradleFile, propertyMap, false);
		Substitute.transformResource(Constants.Templates.ANDROID_BUILD_GRADLE, androidGradleFile, propertyMap, false);
		Substitute.transformResource(Constants.Templates.ANDROID_GRADLE_PROPERTIES, androidGradlePropertiesFile, propertyMap, false);
		Substitute.transformResource(Constants.Templates.ANDROID_SETTINGS_GRADLE, androidSettingsGradleFile, propertyMap, false);
		// write or modify the settings.gradle file in the parent directory to include this project.
		File settingsFile = new File(outputDir, Constants.Files.SETTINGS_GRADLE);
		CreateSettingsGradle.create(settingsFile, Constants.Directories.ANDROID);
		PropertiesUtil.mergeResourceToFile(new File(outputDir, Constants.Files.GRADLE_PROPERTIES),
											Constants.Templates.ANDROID_GRADLE_PROPERTIES);
	}
}
