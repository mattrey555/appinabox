package com.apptechnik.appinabox.engine.codegen.android;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;

import com.apptechnik.appinabox.engine.exceptions.*;
import com.apptechnik.appinabox.engine.log.Log;
import com.apptechnik.appinabox.engine.parser.create.StatementParser;
import com.apptechnik.appinabox.engine.util.*;


import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Create the Retrofit endpoint and objects for the Android Endpoint
 */
public class CreateAndroidLibrary {

    /**
     * TODO: check that the ordering is consistent with the other command-line applications.
     * create-table-file: file containing CREATE TABLE statements for the schema
     * properties-file: properties for this project (including package name, etc)
     * output-directory: top-level directory containing the android and server directories
     * sql-dir-name: directory containing SQL statements (traversed recursively)
     * name: name of the interface to generate.
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        if (args.length < 4) {
            System.err.println("usage: create-table-file properties-file output-directory sql-directory");
            System.exit(-1);
        }

        String createTableFileName = args[0];
        String propertiesFileName = args[1];
        String outputDirName = args[2];
        String sqlDirName = args[3];

        // library contains the endpoint, sync workers
        // application contains endpoint list fragment, generated layouts and fragments for queries and responses.
        File outputDir = FileUtil.checkDirectory("output directory", outputDirName);
        File endpointNameXMLFile = OutputPaths.androidEndpointsXml(outputDir);
        Log.debug("create table file = " + createTableFileName + "\n" +
                "SQL file = " + sqlDirName + "\n" +
                "properties file = " + propertiesFileName + "\n" +
                "output directory = " + outputDirName);
        File sqlDir = FileUtil.checkFile("SQL Statement file", sqlDirName);
        File createTableFile = FileUtil.checkFile("SQL create table", createTableFileName);
        File propertiesFile = FileUtil.checkFile("properties file", propertiesFileName);
        StatementParser statementParser = CreateObjects.parseSchema(createTableFile);
        ExtraProperties properties = new ExtraProperties(propertiesFile);
        CreateEndpoints.create(statementParser, createTableFile, propertiesFile, outputDir, sqlDir);
        createLibraryManifest(properties, OutputPaths.androidLibraryDir(outputDir));
        EndpointsMap endpointsMap = AndroidRepository.create(sqlDir, statementParser, outputDir, createTableFile, propertiesFile);
        AndroidViewModel.create(sqlDir, statementParser, outputDir, createTableFile, propertiesFile, endpointsMap);
    }

    /**
     * Create the library AndroidManifest.xml file in the src/main directory using the package from the properties file.
     * @param propertyMap
     * @param androidLibraryOutputDir
     * @throws IOException
     * @throws PropertiesException
     */
    private static void createLibraryManifest(Properties propertyMap, File androidLibraryOutputDir) throws IOException, PropertiesException {
        File androidMainDir = new File(androidLibraryOutputDir, Constants.Directories.ANDROID_MAIN);
        androidMainDir.mkdirs();
        File androidManifestFile = new File(androidMainDir, Constants.Files.ANDROID_MANIFEST);
        Substitute.transformResource(Constants.Templates.ANDROID_LIBRARY_MANIFEST, androidManifestFile, propertyMap, false);
    }
}
