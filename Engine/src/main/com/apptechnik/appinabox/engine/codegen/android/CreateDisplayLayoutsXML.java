package com.apptechnik.appinabox.engine.codegen.android;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.objects.TraverseQueryObjects;
import com.apptechnik.appinabox.engine.parser.ColumnInfo;
import com.apptechnik.appinabox.engine.parser.pojos.QueryFieldsNode;
import com.apptechnik.appinabox.engine.parser.pojos.Tree;
import com.apptechnik.appinabox.engine.util.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.Properties;
import java.util.Stack;


/**
 * NOTE: this doesn't appear to recurse properly.
 * Create the layouts for:
 * Scalar Fragments
 * List Fragments
 * List Items
 */
public class CreateDisplayLayoutsXML extends TraverseQueryObjects {
    private final AndroidWidgets androidWidgets;
    private final File outputDir;
    private final DocumentBuilder builder;

    public CreateDisplayLayoutsXML(File outputDir, CreateObjectsAndroid createObjectsAndroid) throws Exception {
        super(createObjectsAndroid);
        this.outputDir = outputDir;
        this.androidWidgets = new AndroidWidgets(createObjectsAndroid);
        this.builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    }

    private CreateObjectsAndroid getCreateAndroidObjects() {
        return (CreateObjectsAndroid) getCreateObjects();
    }

    public void createLayouts() throws Exception {
        String rootName = getCreateObjects().getVarName();
        if (getCreateObjects().isMultipleSelect()) {
            createFragmentLayouts(rootName, getCreateObjects().getJsonResponseTree());
        } else {
            createRecyclerLayouts(rootName, getCreateObjects().getJsonResponseTree());
        }
    }

    @Override
    public void accept(Stack<Tree<QueryFieldsNode>> stack) throws Exception {
        QueryFieldsNode node = stack.peek().get();
        createRecyclerLayouts(node.getListLayoutName(getCreateObjects(), stack), stack.peek());
    }

    public void createFragmentLayouts(String layoutName, Tree<QueryFieldsNode> responseTree) throws Exception {
        ExtraProperties properties = getCreateObjects().getProperties();
        Document fragmentDoc = XMLUtils.fromTransformedResource(builder, Constants.Templates.ANDROID_DISPLAY_FRAGMENT_XML, properties);
        EntryLayout entryLayout = new EntryLayout(fragmentDoc);
        for (ColumnInfo column : responseTree.get().getColumnInfos()) {
            entryLayout.addDisplay(column);
        }

        // scalar children (i.e. not lists, get put in a child recycler layouer
        createScalarLayouts(responseTree, entryLayout);
        for (Tree<QueryFieldsNode> child : responseTree.getChildren()) {
            if (!child.get().childUsesParentKeys(responseTree.get())) {
                String label = child.get().getVarName()+ "_" + Constants.Suffixes.LABEL;
                androidWidgets.createUIButton(fragmentDoc, label, label);
                createRecyclerLayouts(child.get().getVarName(), child);
            }
        }
        XMLUtils.save(new File(OutputPaths.androidLayoutDir(outputDir), layoutName + Constants.Extensions.FRAGMENT_XML), fragmentDoc);
    }

    /**
     * Recurse through descendants who use parent keys (i.e. scalar), and create a button and hidden layouts
     * which contains their values.
     * @param responseTree
     * @param entryLayout
     * @throws Exception
     */
    private void createScalarLayouts(Tree<QueryFieldsNode> responseTree, EntryLayout entryLayout) throws Exception{
        Properties childLayoutProperties = new Properties();
        childLayoutProperties.put(Constants.Substitutions.VISIBLE, Constants.AndroidAttributeValues.GONE);
        for (Tree<QueryFieldsNode> child : responseTree.getChildren()) {
            if (child.get().childUsesParentKeys(responseTree.get())) {
                entryLayout.addButton(child.get());
                EntryLayout childEntryLayout = entryLayout.addLayout(childLayoutProperties);
                createScalarLayouts(child, childEntryLayout);
            }
        }
    }

    /**
     * Replace WIDGET<X> with the widget for the column, VAR<X> with the variable, and
     * WIDGET_PROPERTIES<X> with appropriate widget properties.
     * The layouts are written into a flat directory mapped from a hierarchy of query nodes
     * @param responseTree
     * @throws Exception
     */
    public void createRecyclerLayouts(String layoutName, Tree<QueryFieldsNode> responseTree) throws Exception {
        QueryFieldsNode responseNode = responseTree.get();
        ExtraProperties properties = getCreateObjects().getProperties();
        Document fragmentDoc = XMLUtils.fromTransformedResource(builder, Constants.Templates.ANDROID_LIST_FRAGMENT_XML, properties);
        XMLUtils.save(new File(OutputPaths.androidLayoutDir(outputDir), layoutName + Constants.Extensions.FRAGMENT_XML), fragmentDoc);
        ListItems.populateListAdapter(getCreateObjects(), responseNode);
    }

    /**
     * Simplified constraint layout with the following rules:
     * add label and value display or edit.
     *    label on left-justified, value right-justified
     *    label and value are below previous node.
     * add button:
     *    button is left-justified, below previous node
     * constraint layout:
     *    creates a nested constraint layout match_parent width, wrap_content height.
     *    template has a visible flag.  Successive adds are added to this entry layout.
     *    The function returns a new entry layout, which objects are added to.
     */
    private class EntryLayout {
        private Node prevNode = null;
        private Document doc;

        public EntryLayout(Document doc) {
            this.prevNode = null;
            this.doc = doc;
        }

        public EntryLayout(Document doc, Node prevNode) {
            this.doc = doc;
            this.prevNode = prevNode;
        }

        public Node addDataEntry(ColumnInfo columnInfo) throws Exception {
            String label = columnInfo.getColumnName() + "_" + Constants.Suffixes.LABEL;
            String value = columnInfo.getColumnName() + "_" + Constants.Suffixes.VALUE;
            return addDataEntry(label, value, columnInfo.getDataType());
        }

        public Node addDataEntry(String label, String value, String valueSqlType) throws Exception {
            Node labelNode = androidWidgets.createUILabel(doc, label, label);
            ConstraintLayout.applyLabelConstraints((Element) prevNode, (Element) labelNode);
            getCreateAndroidObjects().getStringMap().addEntry(label, label);
            prevNode = labelNode;
            Node valueNode = androidWidgets.createUIEntry(doc, value, valueSqlType);
            ConstraintLayout.applyEditConstraints((Element) prevNode, (Element) labelNode, (Element) valueNode);
            prevNode = valueNode;
            return valueNode;
        }

        public Node addDisplay(ColumnInfo columnInfo) throws Exception {
            String label = columnInfo.getColumnName() + "_" + Constants.Suffixes.LABEL;
            String value = columnInfo.getColumnName() + "_" + Constants.Suffixes.VALUE;
            return addDisplay(label, value, columnInfo.getDataType());

        }

        public Node addDisplay(String label, String value, String valueSqlType) throws Exception {
            Node labelNode = androidWidgets.createUILabel(doc, label, label);
            ConstraintLayout.applyLabelConstraints((Element) prevNode, (Element) labelNode);
            getCreateAndroidObjects().getStringMap().addEntry(label, label);
            prevNode = labelNode;
            Node valueNode = androidWidgets.createUILabel(doc, value, valueSqlType);
            ConstraintLayout.applyEditConstraints((Element) prevNode, (Element) labelNode, (Element) valueNode);
            prevNode = valueNode;
            return valueNode;
        }

        public Node addButton(String label) throws Exception {
            Node buttonNode = androidWidgets.createUILabel(doc, label, label);
            ConstraintLayout.applyLabelConstraints((Element) prevNode, (Element) buttonNode);
            getCreateAndroidObjects().getStringMap().addEntry(label, label);
            prevNode = buttonNode;
            return prevNode;
        }

        public Node addButton(QueryFieldsNode node) throws Exception {
            return addButton(node.getVarName() + "_" + Constants.Suffixes.LABEL);
        }

        public EntryLayout addLayout(Properties properties) throws Exception {
            Node layoutNode = XMLUtils.addChildFromTransformedResource(doc, Constants.UIElements.CONSTRAINT_LAYOUT,
                                                      null, Constants.LayoutTemplates.CONSTRAINT_LAYOUT_XML, properties);
            return new EntryLayout(doc, layoutNode);
        }

        public Node getPrevNode() {
            return prevNode;
        }

        public Document getDoc() {
            return doc;
        }
    }
}
