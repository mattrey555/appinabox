package com.apptechnik.appinabox.engine.codegen.android;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.android.util.StringMap;
import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;
import com.apptechnik.appinabox.engine.codegen.objects.ProcessSQLFiles;
import com.apptechnik.appinabox.engine.codegen.pojos.ClassFile;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeGen;
import com.apptechnik.appinabox.engine.codegen.pojos.FunctionDeclaration;
import com.apptechnik.appinabox.engine.codegen.pojos.kotlin.KotlinCodeGen;
import com.apptechnik.appinabox.engine.codegen.query.CreateQueryObjects;
import com.apptechnik.appinabox.engine.exceptions.PropertiesException;
import com.apptechnik.appinabox.engine.log.Log;
import com.apptechnik.appinabox.engine.parser.create.StatementParser;
import com.apptechnik.appinabox.engine.parser.pojos.MergedFieldNode;
import com.apptechnik.appinabox.engine.parser.pojos.QueryFieldsNode;
import com.apptechnik.appinabox.engine.parser.pojos.Tree;
import com.apptechnik.appinabox.engine.util.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import net.sf.jsqlparser.statement.select.Select;
import java.io.File;
import java.io.IOException;
import java.util.*;

public class CreateEndpoints {

    /**
     * TODO: check that the ordering is consistent with the other command-line applications.
     * create-table-file: file containing CREATE TABLE statements for the schema
     * properties-file: properties for this project (including package name, etc)
     * output-directory: top-level directory containing the android and server directories
     * sql-dir-name: directory containing SQL statements (traversed recursively)
     * name: name of the interface to generate.
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        if (args.length < 4) {
            System.err.println("usage: create-table-file properties-file output-directory sql-directory");
            System.exit(-1);
        }

        String createTableFileName = args[0];
        String propertiesFileName = args[1];
        String outputDirName = args[2];
        String sqlDirName = args[3];

        // library contains the endpoint, sync workers
        // application contains endpoint list fragment, generated layouts and fragments for queries and responses.
        File outputDir = FileUtil.checkDirectory("output directory", outputDirName);
        Log.debug("create table file = " + createTableFileName + "\n" +
                "SQL file = " + sqlDirName + "\n" +
                "properties file = " + propertiesFileName + "\n" +
                "output directory = " + outputDirName);
        File sqlDir = FileUtil.checkFile("SQL Statement file", sqlDirName);
        File createTableFile = FileUtil.checkFile("SQL create table", createTableFileName);
        File propertiesFile = FileUtil.checkFile("properties file", propertiesFileName);
        StatementParser statementParser = CreateObjects.parseSchema(createTableFile);
        create(statementParser, createTableFile, propertiesFile, outputDir, sqlDir);
    }


    public static void create(StatementParser statementParser,
                              File createTableFile,
                              File propertiesFile,
                              File outputDir,
                              File sqlDir) throws Exception {
        File endpointNameXMLFile = OutputPaths.androidEndpointsXml(outputDir);

        // endpoint methods are the actual endpoint methods.
        // imports are the classes used in each query/update, endpoint names are from the .SQL file
        // (either the file name without extension, or name if specified in the .SQL file.
        // endpoint names is the map of URLs to endpoint names
        List<FunctionDeclaration> endpointMethods = new ArrayList<FunctionDeclaration>();
        List<String> importNames = new ArrayList<String>();
        Map<String, String> endpointNames = new HashMap<String, String>();

        // traverse the directory and generate an endpoint for each SQL file.
        ProcessFile processFile = new ProcessFile(statementParser, outputDir,
                                                  createTableFile, propertiesFile, endpointMethods,
                                                  importNames, endpointNames);
        CreateObjects.walkSQLDir(sqlDir, processFile);
        CodeGen codegen = new KotlinCodeGen();
        ExtraProperties properties = new ExtraProperties(propertiesFile);

        // create a class containing all of the endpoints, a string array for the endpoint names, the library manifest,
        // and the environment.xml file with the server URL
        RetrofitEndpoint.endpointClassFile(codegen, properties, endpointMethods).write(outputDir);
        createStringArray(endpointNameXMLFile, endpointNames.values());
        createLibraryManifest(properties, OutputPaths.androidLibraryDir(outputDir));
        String url = properties.getProp(Constants.Properties.SERVER_URL, "base URL name");
        File appEnvironmentXmlFile = OutputPaths.androidEnvironmentXml(outputDir);
        StringMap environmentMap = new StringMap(appEnvironmentXmlFile);
        environmentMap.addEntry(Constants.Properties.SERVER_URL, url);
        environmentMap.commit();
    }

    /**
     * File consumer to process .SQL files.
     */
    /*
    ProcessSQLFiles(StatementParser statementParser,
                    File outputDir,
                    File createTableFile,
                    File propertiesFile)
     */
    private static class ProcessFile extends ProcessSQLFiles {
        private final List<FunctionDeclaration> endpointMethods;    // list of method endpoints to add to.
        private final List<String> importNames;                     // fully-qualified classes to import in endpoint
        private final Map<String, String> endpointNames;            // mappings from endpoint URLs to descriptions (or to URLs if no description)

        public ProcessFile(StatementParser statementParser,
                           File outputDir,
                           File createTableFile,
                           File propertiesFile,
                           List<FunctionDeclaration> endpointMethods,
                           List<String> importNames,
                           Map<String, String> endpointNames) throws IOException {
            super(statementParser, outputDir, createTableFile, propertiesFile);
            this.endpointMethods = endpointMethods;
            this.importNames = importNames;
            this.endpointNames = endpointNames;
        }

        @Override
        public void accept(File sqlFile) throws Exception {
            CreateObjectsAndroid createObjectsAndroid = new CreateObjectsAndroid(sqlFile, propertiesFile, createTableFile,
                    statementParser, outputDir);
            if (createObjectsAndroid.getSqlFile().inDirectiveList(Constants.Directives.ENDPOINTS, Constants.Directives.ANDROID)) {
                createEndpoint(createObjectsAndroid);
                String url = createObjectsAndroid.getSqlFile().getTag(Constants.Directives.URL);
                endpointNames.put(url, createObjectsAndroid.getSqlFile().getDescription());
            }
        }

        /**
         * Create the endpoint and the associated object hierarchy for the request.
         * NOTE: just doing response tree endpoints, will do request tree.
         * @param createObjectsAndroid
         * @throws Exception
         */
        public void createEndpoint(CreateObjectsAndroid createObjectsAndroid) throws Exception {
            if (createObjectsAndroid.getJsonResponseTree() != null) {
                AndroidResponseObjects androidResponseObjects = new AndroidResponseObjects(createObjectsAndroid);
                executeOnSelects(createObjectsAndroid, endpointMethods);
                createObjectsAndroid.getJsonResponseTree().traverseTreeStack(androidResponseObjects);

                // Create the objects for the select return, in the same manner as we do for the server, where
                // if there are multiple select calls, create a top-level object to aggregate their results.
                if (createObjectsAndroid.getSelectStatementList().size() > 1) {
                    ClassFile multipleSelectObject =
                        CreateQueryObjects.createMultipleSelectObject(createObjectsAndroid.getKotlinCodeGen(), createObjectsAndroid,
                                                                      Constants.Templates.KOTLIN_OBJECT_IMPORTS,
                                                                      createObjectsAndroid.getJsonResponseTree());
                    importNames.add(multipleSelectObject.createImport());
                    multipleSelectObject.write(createObjectsAndroid.getOutputDir());
                } else {
                    Tree<QueryFieldsNode> rootNodeTree =  createObjectsAndroid.getJsonResponseTree();
                    if (!rootNodeTree.get().isSingleField()) {
                        importNames.add(createObjectsAndroid.getQueryPackageName());
                    }
                }
            }

            // if the SQL statement list only contains DELETE calls, then the request JSON may be unspecified.
            if (createObjectsAndroid.getJsonRequestTree() != null) {
                Tree<MergedFieldNode> rootNodeTree =  createObjectsAndroid.getJsonRequestTree();
                AndroidRequestObjects androidRequestObjects = new AndroidRequestObjects(createObjectsAndroid);
                rootNodeTree.traverseTreeStack(androidRequestObjects);
                if (!rootNodeTree.get().isSingleField()) {
                    importNames.add(OutputPackages.getUpdatePackageName(createObjectsAndroid.getPackageName()) + "." + rootNodeTree.get().getVarName());
                }
            }
        }
    }

    /**
     * For each select statement:
     *  map them to the tables they reference.
     *  find the node in the JSON tree they reference.
     *  recurse the SELECT statement to find the primary keys from each referenced table, and insert
     *  them into the SELECT items list during the recursion.
     *  Map JDBC NamedParameters -> types.
     *  apply fixed types from the type: directive.
     *  map the SELECT statement to the parameter map.
     *  Create the java objects created from the JSON applied to the SELECT statement, with fixed types applied.
     * @param createObjects common data for creating POJOs
     * @throws Exception general failure.
     */
    private static void executeOnSelects(CreateObjectsAndroid createObjects,
                                         List<FunctionDeclaration> endpointMethods) throws Exception {
        CreateQueryObjects.resolveSelects(createObjects, createObjects.getJsonResponseTree());
        List<Select> selectStatements = createObjects.getSelectStatementList();
        AndroidResponseObjects androidResponseObjects = new AndroidResponseObjects(createObjects);
        endpointMethods.add(RetrofitEndpoint.endpointFn(createObjects.getKotlinCodeGen(), createObjects));
        for (int i = 0; i < selectStatements.size(); i++) {
            Tree<QueryFieldsNode> jsonTree =
                    CreateQueryObjects.getAssociatedJsonTree(selectStatements, i, createObjects.getJsonResponseTree());
            jsonTree.traverseTreeStack(androidResponseObjects);
        }
    }

    /**
     * Create a string array resource from a template xml file and a list of strings.
     * @param xmlFile output XML file
     * @param strings strings to add to stringarray
     * @throws Exception if there was an error constructing the XML or saving the file.
     */
    public static void createStringArray(File xmlFile, Collection<String> strings)throws Exception {
        Document xmlDoc = XMLUtils.fromResource(Constants.Templates.STRING_ARRAY_XML);
        Element resourceElement = xmlDoc.getDocumentElement();
        Node stringArrayNode = resourceElement.getElementsByTagName(Constants.AndroidResources.STRING_ARRAY).item(0);
        for (String s : strings) {
            XMLUtils.addSingleTextElement(xmlDoc, stringArrayNode, Constants.AndroidResources.ITEM, s);
        }
        XMLUtils.save(xmlFile, xmlDoc);
    }

    /**
     * Create the library AndroidManifest.xml file in the src/main directory using the package from the properties file.
     * @param propertyMap
     * @param androidLibraryOutputDir
     * @throws IOException
     * @throws PropertiesException
     */
    private static void createLibraryManifest(Properties propertyMap, File androidLibraryOutputDir) throws IOException, PropertiesException {
        File androidMainDir = new File(androidLibraryOutputDir, Constants.Directories.ANDROID_MAIN);
        androidMainDir.mkdirs();
        File androidManifestFile = new File(androidMainDir, Constants.Files.ANDROID_MANIFEST);
        Substitute.transformResource(Constants.Templates.ANDROID_LIBRARY_MANIFEST, androidManifestFile, propertyMap, false);
    }
}
