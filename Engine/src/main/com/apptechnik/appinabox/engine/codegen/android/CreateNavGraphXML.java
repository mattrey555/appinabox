package com.apptechnik.appinabox.engine.codegen.android;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.android.util.AndroidXML;
import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;
import com.apptechnik.appinabox.engine.codegen.objects.ProcessSQLFiles;
import com.apptechnik.appinabox.engine.codegen.objects.ProcessSQLFilesAndroid;
import com.apptechnik.appinabox.engine.codegen.util.SQLFile;
import com.apptechnik.appinabox.engine.parser.create.StatementParser;
import com.apptechnik.appinabox.engine.parser.pojos.QueryFieldsNode;
import com.apptechnik.appinabox.engine.parser.pojos.Tree;
import com.apptechnik.appinabox.engine.util.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Stack;


/**
 * From the list of endpoints, create tne nav_graph from the endpoint list to each fragment
 * to enter the parameters for the query/update.
 *     <fragment
 *         android:id="@+id/${query_name}"
 *         android:name="${package}.${query_name|query_description}Fragment"
 *         android:label="@string/${query_name}"
 *         tools:layout="@layout/${query_name}_layout">
 */
public class CreateNavGraphXML {
    /**
     * create-table-file: file containing CREATE TABLE statements for the schema
     * properties-file: properties for this project (including package name, etc)
     * output-directory: top-level directory containing the android and server directories
     * sql-dir-name: directory containing SQL statements (traversed recursively)
     * name: name of the interface to generate.
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        if (args.length < 4) {
			System.err.println("usage: output-directory sql-file-directory create-tables-file properties-file");
			System.exit(-1);
		}
		String outputDirName = args[0];
		String sqlDirName = args[1];
		String createTablesFileName = args[2];
		String propertiesFileName = args[3];
		File sqlDir = FileUtil.checkDirectory("SQL statements directory", sqlDirName);
		File outputDir = FileUtil.checkDirectory("output directory", outputDirName);
		File propertiesFile = new File(outputDir, propertiesFileName);

		System.out.println("output directory name = " + outputDirName + "\n" +
						   "statements file directory = " + sqlDirName + "\n" +
				           "Create Tables File = " + createTablesFileName + "\n" +
						   "properties file name = " + propertiesFile);
        File createTablesFile = FileUtil.checkFile("SQL create table", createTablesFileName);

        StatementParser statementParser = CreateObjects.parseSchema(createTablesFile);
        NavGraph navGraph = new NavGraph(statementParser, propertiesFile, createTablesFile, outputDir);
        navGraph.create(sqlDir);
    }

    public static class NavGraph extends ProcessSQLFilesAndroid {
        private final DocumentBuilder builder;
        private final Document navXMLDoc;
        private final Node navigationNode;
        private final ExtraProperties properties;
        private final List<Node> fragmentNodes;
        private final List<Node> globalActionNodes;

        /**
         * Create the nav_graph.xml file, with fragments for  parameters for each query,
         *
         * @param propertiesFile  properties file for resource transformation (stuff like package nme)
         * @param createTableFile used by CreateObjects
         * @param statementParser used by CreateObjects
         * @param outputDir       used by CreateObjects
         * @throws Exception
         */
        public NavGraph(StatementParser statementParser,
                        File propertiesFile,
                        File createTableFile,
                        File outputDir) throws Exception {
            super(statementParser, outputDir, propertiesFile, createTableFile);
            this.properties = new ExtraProperties(propertiesFile);
            this.builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            this.navXMLDoc = XMLUtils.fromTransformedResource(builder, Constants.Templates.NAV_GRAPH_XML, properties);

            // Find the navigation node to put the global actions under
            this.navigationNode = XMLUtils.findElementByName(navXMLDoc.getDocumentElement(), Constants.AndroidThings.NAVIGATION);
            this.fragmentNodes = new ArrayList<Node>();
            this.globalActionNodes = new ArrayList<Node>();
        }

        @Override
        public void create(File sqlDir) throws Exception {
            super.create(sqlDir);
            File outputFile = OutputPaths.androidNavGraphXml(outputDir);
            fragmentNodes.forEach(node -> navigationNode.appendChild(node));
            globalActionNodes.forEach(node -> navigationNode.appendChild(node));
            XMLUtils.save(outputFile, navXMLDoc);
        }

        @Override
        public void accept(CreateObjectsAndroid createObjectsAndroid) throws Exception {
            if (createObjectsAndroid.getJsonResponseTree() != null) {
                addNavGraphQueryFragment(createObjectsAndroid);
                if (createObjectsAndroid.getJsonResponseTree() != null) {
                    createObjectsAndroid.getJsonResponseTree().traverseTreeStack(new ResultFragments(createObjectsAndroid));
                }
                createObjectsAndroid.getStringMap().addEntry(createObjectsAndroid.getVarName(),
                                                             createObjectsAndroid.getSqlFile().getDescription());
                createObjectsAndroid.getStringMap().commit();
            }
        }

        /**
         * Given a SQL file, use its name and description to create the fragment in the nav_graph.
         * Each SQLFile has a top-level fragment, which takes the parameters for the query, or data
         * entry for an insert/update.
         * SELECT: fragment contains data entry for parameters (Q: how are objects handled?) and execute button
         * Execute button runs retrofit query, and navigates to display fragment with results.
         * DELETE/INSERT/UPDATE: fragment contains data entry for parameters and execute button.   Execute runs retrofit
         * endpoint, and navigates to screen with result code.
         * If there are mutliple commands executed, and one of them is a SELECT, navigate to the display fragment
         * for the select data.
         *
         * @throws Exception
         */
        private void addNavGraphQueryFragment(CreateObjectsAndroid createObjectsAndroid) throws Exception {
            SQLFile sqlFile = createObjectsAndroid.getSqlFile();
            String descriptionTag = createObjectsAndroid.getVarName() + Constants.Suffixes.QUERY_PARAMS;
            String descriptionString = sqlFile.getDescription();
            String packageName = OutputPackages.androidFragments(createObjectsAndroid.getPackageName());
            String queryFragment = createObjectsAndroid.queryFragmentName();

            properties.setProperty(Constants.Substitutions.FRAGMENT_LABEL, descriptionTag);
            createObjectsAndroid.getStringMap().addEntry(descriptionTag, descriptionString);

            // should be queryparams layout name
            properties.setProperty(Constants.Substitutions.FRAGMENT_LAYOUT, createObjectsAndroid.getLayoutName());
            properties.setProperty(Constants.Substitutions.PACKAGE, packageName);
            properties.setProperty(Constants.Substitutions.FRAGMENT_NAME, queryFragment);
            // add the query params fragment.
            fragmentNodes.add(XMLUtils.transformResource(builder, navXMLDoc, Constants.Templates.NAV_GRAPH_QUERY_FRAGMENT_XML, properties));
         }

        /**
         * Traverser to create fragments to navigate the result hierarchy
         */
        public class ResultFragments implements Tree.ConsumerException<Stack<Tree<QueryFieldsNode>>> {
            private final CreateObjectsAndroid createObjectsAndroid;

            public ResultFragments(CreateObjectsAndroid createObjectsAndroid) {
                this.createObjectsAndroid = createObjectsAndroid;
            }

            public void accept(Stack<Tree<QueryFieldsNode>> stack) throws Exception {
                createResultFragment(createObjectsAndroid, stack.peek().get(), stack.size() == 1);
                addActions(createObjectsAndroid, stack.peek());
            }
        }

        /**
         * Top-level multiple-result: TextView for each List child which navigates to ChildList Fragment.
         * Object: RecyclerView of list and Fragment for detail.
         *
         * @param createObjectsAndroid
         * @param node
         * @throws Exception
         */

        private void createResultFragment(CreateObjectsAndroid createObjectsAndroid,
                                          QueryFieldsNode node,
                                          boolean isRoot) throws Exception {
            createResultFragment(createObjectsAndroid, node, false, Constants.Templates.NAV_GRAPH_RESULTS_FRAGMENT_XML);
            if (!isRoot || !createObjectsAndroid.isMultipleSelect()) {
                createResultFragment(createObjectsAndroid, node, true, Constants.Templates.NAV_GRAPH_RESULTS_LIST_FRAGMENT_XML);
            }
        }

        private void createResultFragment(CreateObjectsAndroid createObjectsAndroid,
                                          QueryFieldsNode node,
                                          boolean isList,
                                          String template) throws Exception {
            Properties nodeProperties = new Properties(properties);
            String varName = node.getVarName();

            // scalar fragment for all nodes (for details)
            String descriptionTag = createObjectsAndroid.fragmentDescriptionTag(varName);
            createObjectsAndroid.getStringMap().addEntry(descriptionTag, createObjectsAndroid.fragmentDescription(varName));
            properties.setProperty(Constants.Substitutions.FRAGMENT_LABEL, descriptionTag);
            properties.setProperty(Constants.Substitutions.PACKAGE, OutputPackages.androidFragments(createObjectsAndroid.getPackageName()));
            properties.setProperty(Constants.Substitutions.VAR, varName);
            nodeProperties.setProperty(Constants.Substitutions.TYPE, node.getClassName());
            nodeProperties.setProperty(Constants.Substitutions.FRAGMENT_LAYOUT, createObjectsAndroid.responseFragmentLayout(isList, varName));
            nodeProperties.setProperty(Constants.Substitutions.FRAGMENT_NAME, createObjectsAndroid.responseFragmentName(isList, varName));
            fragmentNodes.add(XMLUtils.transformResource(builder, navXMLDoc, template, nodeProperties));
        }

        /**
         * Replace FRAGMENT_ACTION and FRAGMENT_NAME in action.xml.txt
         *
         * @param createObjectsAndroid
         * @param node
         * @throws Exception
         */
        private void addActions(CreateObjectsAndroid createObjectsAndroid, Tree<QueryFieldsNode> node) throws Exception {
            for (Tree<QueryFieldsNode> child : node.getChildren()) {
                Properties nodeProperties = new Properties(properties);
                String childFragmentName = createObjectsAndroid.responseFragmentName(child.get().childUsesParentKeys(node.get()), child.get().getVarName());
                nodeProperties.setProperty(Constants.Substitutions.FRAGMENT_ACTION, globalActionName(childFragmentName));
                nodeProperties.setProperty(Constants.Substitutions.FRAGMENT_NAME, childFragmentName);
                globalActionNodes.add(XMLUtils.transformResource(builder, navXMLDoc, Constants.Templates.ACTION_XML, nodeProperties));
           }
        }
    }

    /**
     * transform insert_query_filmQueryParamsFragment and insert_query_filmResultsListFragment to
     * insert_query_filmQueryParams_to_insert_query_filmResultsList
     * action_<frag>
     * @param fragParent
     * @param fragChild
     * @return
     */
    public static String actionName(String fragParent, String fragChild) {
        return Constants.NavigationXML.ACTION + "_" + fragParent.replace(Constants.Suffixes.FRAGMENT, "") +
                "_to_" + fragChild.replace(Constants.Suffixes.FRAGMENT, "");
    }

    public static String globalActionName(String frag) {
        return Constants.NavigationXML.ACTION + "_" + frag.replace(Constants.Suffixes.FRAGMENT, "");
    }
}
