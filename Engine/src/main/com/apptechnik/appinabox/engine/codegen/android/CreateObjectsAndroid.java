package com.apptechnik.appinabox.engine.codegen.android;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.android.util.StringMap;
import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeGen;
import com.apptechnik.appinabox.engine.codegen.pojos.ParameterList;
import com.apptechnik.appinabox.engine.codegen.pojos.SimpleType;
import com.apptechnik.appinabox.engine.codegen.pojos.Type;
import com.apptechnik.appinabox.engine.codegen.util.CodegenUtil;
import com.apptechnik.appinabox.engine.exceptions.NameNotFoundException;
import com.apptechnik.appinabox.engine.exceptions.TypeNotFoundException;
import com.apptechnik.appinabox.engine.exceptions.UnsupportedParamTypeException;
import com.apptechnik.appinabox.engine.parser.create.StatementParser;
import com.apptechnik.appinabox.engine.parser.pojos.MergedFieldNode;
import com.apptechnik.appinabox.engine.parser.pojos.QueryFieldsNode;
import com.apptechnik.appinabox.engine.util.FileUtil;
import com.apptechnik.appinabox.engine.util.OutputPackages;
import com.apptechnik.appinabox.engine.util.OutputPaths;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

/**
 *
 */
public class CreateObjectsAndroid extends CreateObjects {
    private StringMap stringMap;                            // strings.xml (application)

    public CreateObjectsAndroid(File sourceFile,
                                 File propertiesFile,
                                 File createTableFile,
                                 StatementParser statementParser,
                                 File outputDir) throws Exception {
        super(sourceFile, propertiesFile, createTableFile, statementParser, outputDir);
        File appStringsXmlFile = OutputPaths.androidStringsXml(outputDir);
        stringMap = new StringMap(appStringsXmlFile);
    }

    public String getLayoutXmlFileName() {
        return getVarName() + "_" + Constants.Extensions.LAYOUT_XML;
    }

    public String getLayoutName() {
        return getVarName() + "_" + Constants.Suffixes.LAYOUT;
    }

    public StringMap getStringMap() {
        return stringMap;
    }

    public String queryFragmentName() {
        return getVarName() + Constants.Suffixes.QUERY_PARAMS_FRAGMENT;
    }

    /**
     * this is one of the fex exceptions where it's OK to pass control
     * @param isList list or scalar.
     * @param varName
     * @return
     */
    public String responseFragmentLayout(boolean isList, String varName) {
        if (isList) {
            return getVarName() + "_" + varName + Constants.Suffixes.LIST_LAYOUT;
        } else {
            return getVarName() + "_" + varName + Constants.Suffixes.LAYOUT;
        }
    }

    public String responseFragmentName(boolean isList, String varName) {
        if (isList) {
            return getVarName() + "_" + varName + Constants.Suffixes.RESULTS_LIST_FRAGMENT;
        } else {
            return getVarName() + "_" + varName + Constants.Suffixes.RESULTS_FRAGMENT;
        }
    }

    public String fragmentDescriptionTag(String varName) {
        return getVarName() + "_" + varName + "_" + Constants.Suffixes.DESCRIPTION;
    }

    public String fragmentDescription(String varName) {
        return getVarName() + "_" + varName + "_" + Constants.Suffixes.DESCRIPTION;
    }

    public Type retrofitCallReturnType(CodeGen codegen) throws TypeNotFoundException {
        if (getJsonResponseTree() != null) {
            Type responseType = codegen.listType(QueryFieldsNode.getNodeType(codegen, getQueryPackageName(), getJsonResponseTree()));
            return codegen.genericType(codegen.classType(Constants.KotlinTypes.CALL), responseType);
        } else {
            return new SimpleType(Constants.KotlinKeywords.UNIT);
        }
    }

    public Type retrofitCallRequestType(CodeGen codegen)
    throws TypeNotFoundException, UnsupportedParamTypeException, NameNotFoundException {
        // generate the parameter list from the query parameters, and if there is a request type, then add it.
        return codegen.listType(MergedFieldNode.getNodeType(codegen, OutputPackages.getUpdatePackageName(getPackageName()),
                                                            getJsonRequestTree(), getStatementParamMap()));
    }
}
