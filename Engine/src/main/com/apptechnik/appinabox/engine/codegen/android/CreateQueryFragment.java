package com.apptechnik.appinabox.engine.codegen.android;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.codegen.util.CodegenUtil;
import com.apptechnik.appinabox.engine.exceptions.PropertiesException;
import com.apptechnik.appinabox.engine.exceptions.TypeNotFoundException;
import com.apptechnik.appinabox.engine.parser.pojos.ParameterMap;
import com.apptechnik.appinabox.engine.util.OutputPackages;
import com.apptechnik.appinabox.engine.util.OutputPaths;
import com.apptechnik.appinabox.engine.util.StringUtil;
import com.apptechnik.appinabox.engine.util.Substitute;
import net.sf.jsqlparser.statement.create.table.ColDataType;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Transform QueryParamsFragment.kt.txt:
 *
 */
public class CreateQueryFragment {
    private final CreateObjectsAndroid createObjectsAndroid;
    private final AndroidWidgets androidWidgets;
    private final File outputDir;
    private final EndpointsMap endpointsMap;

    public CreateQueryFragment(File outputDir, CreateObjectsAndroid createObjectsAndroid, EndpointsMap endpointsMap) {
        this.createObjectsAndroid = createObjectsAndroid;
        this.outputDir = outputDir;
        this.androidWidgets = new AndroidWidgets(createObjectsAndroid);
        this.endpointsMap = endpointsMap;
        createObjectsAndroid.getProperties().put(Constants.Substitutions.NAME, createObjectsAndroid.getVarName());
    }

    /**
     * PACKAGE: root package name
     * NAME: name of source .SQL file
     * FRAGMENT_LAYOUT: layout generated for this fragment (see CreateQueryFragmentXML.createFragmentXML())
     * ASSIGN_VARIABLES: 
     * @throws Exception
     */
    public void createFragment() throws Exception {
        CodeGen codegen = createObjectsAndroid.getKotlinCodeGen();
        String fragmentPackageName = OutputPackages.androidFragments(createObjectsAndroid.getPackageName());
        File fragmentOutputDir = OutputPaths.androidKotlinDir(outputDir, StringUtil.packageToFilePath(fragmentPackageName));
        File fragmentOutputFile = new File(fragmentOutputDir, createObjectsAndroid.getVarName() + Constants.Suffixes.QUERY_PARAMS_FRAGMENT + Constants.Extensions.KOTLIN);
        Properties properties = createObjectsAndroid.getProperties();
        properties.put(Constants.Substitutions.WIDGET_IMPORTS, getWidgetImports());
        properties.put(Constants.Substitutions.ASSIGN_VARIABLES, getQueryParamsStatements(codegen).toString());
        properties.put(Constants.Substitutions.FRAGMENT_LAYOUT, createObjectsAndroid.getLayoutName());
        properties.put(Constants.Substitutions.ENDPOINT, createObjectsAndroid.getSqlFile().getName());
        properties.put(Constants.Substitutions.ENDPOINT_ARGS, endpointParams(codegen, createObjectsAndroid.getSqlFile().getName()).toString());
        properties.put(Constants.Substitutions.REPOSITORY_NAME, endpointsMap.getRepositoryForEndpoint(createObjectsAndroid.getSqlFile().getName()));
        Substitute.transformResource(Constants.Templates.ANDROID_QUERY_PARAMS_FRAGMENT, fragmentOutputFile, properties);
    }

    /**
     * Collect the imports used for the android entry widgets.
     * @return
     */
    public String getWidgetImports() {
        ParameterMap params = createObjectsAndroid.getQueryParams();
        Set<String> importSet = new HashSet<String>();
        for (String param : params.keySet()) {
            ParameterMap.ParamData paramData = params.getParamData(param);
            ColDataType colDataType = paramData.getDataType();
            String[] imports = AndroidWidgets.getEntryImports(colDataType.getDataType());
            importSet.addAll(Arrays.asList(imports));
        }
        return StringUtil.delim(importSet.stream()
                    .map(s -> Constants.KotlinKeywords.IMPORT + " " + s)
                    .collect(Collectors.toList()), "\n");

    }
    /**
     * val ${FIELD} = findViewById(R.id.${FIELD}).text
     * repository.{QUERY}(params)
      */

    public String getQueryParamsStatements(CodeGen codegen)
    throws TypeNotFoundException, PropertiesException, IOException {
        ParameterMap params = createObjectsAndroid.getQueryParams();
        List<String> statementList = new ArrayList<String>(params.keySet().size());
        for (String param: params.keySet()) {
            ParameterMap.ParamData paramData = params.getParamData(param);
            ColDataType colDataType = paramData.getDataType();
            String entryWidget = AndroidWidgets.getEntry(colDataType.getDataType())[0];
            AndroidWidgets.ElementProperties entryProperties = AndroidWidgets.getElementProperties(entryWidget);
            String entryProperty = entryProperties.getProperty(Constants.UIProperties.VALUE);

            // val ${$VALUE} = getView().findViewById(R.id.${VALUE}).text
            // val ${$VALUE} = (this@${FRAGMENT}.view?.findViewById(R.id.{$VALUE}) as {ENTRY}).text.toString()
            CodeExpression at = codegen.at(codegen.var(Constants.KotlinKeywords.THIS),
                                           codegen.var(createObjectsAndroid.getVarName() + Constants.Suffixes.QUERY_PARAMS_FRAGMENT));
            CodeExpression widgetRef =
                 codegen.dot(codegen.var(Constants.KotlinVariables.V),
                             codegen.methodCall(Constants.AndroidFns.FIND_VIEW_BY_ID,
                                                codegen.dot(codegen.var(Constants.AndroidThings.R_ID), codegen.var(param))));
            statementList.add(AndroidWidgets.Converters.getToExpression(entryWidget,
                              codegen.typeFromSQLType(colDataType.getDataType(), false).toString(),
                              widgetRef.toString(), param));
        }
        return StringUtil.delim(statementList, "\n");
    }

    public ArgumentList endpointParams(CodeGen codegen, String endpointName) {
        return codegen.argList(CodegenUtil.declarationsToParameters(endpointsMap.getEndpointForName(endpointName).getParameterList()));
    }
}
