package com.apptechnik.appinabox.engine.codegen.android;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.parser.pojos.ParameterMap;
import com.apptechnik.appinabox.engine.util.ExtraProperties;
import com.apptechnik.appinabox.engine.util.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;

/**
 * Create a fragment to edit the fields for a query.
 */
public class CreateQueryFragmentXML {
    private final CreateObjectsAndroid createObjectsAndroid;
    private final AndroidWidgets androidWidgets;

    public CreateQueryFragmentXML(CreateObjectsAndroid createObjectsAndroid) {
        this.createObjectsAndroid = createObjectsAndroid;
        androidWidgets = new AndroidWidgets(createObjectsAndroid);
    }

    /**
     * Create the layout for the fragment from the query parameters.
     * @param layoutXmlFile
     * @throws Exception
     */
    public void createFragmentXML(File layoutXmlFile) throws Exception {
        ExtraProperties properties = new ExtraProperties(createObjectsAndroid.getPropertiesFile());
        // ${NAME} in the query_fragment.xml is the SQL variable name.

        properties.put(Constants.Substitutions.NAME, createObjectsAndroid.getVarName());
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document fragmentDoc = XMLUtils.fromTransformedResource(builder, Constants.Templates.ANDROID_QUERY_FRAGMENT_XML, properties);
        Node prevLabelNode = null;
        Node prevEditNode = null;

        for (String param : createObjectsAndroid.getQueryParams().keySet()) {
            ParameterMap.ParamData paramData = createObjectsAndroid.getQueryParams().getParamData(param);
            String sqlType = paramData.getDataType().getDataType();

            Node labelNode = androidWidgets.createUILabel(fragmentDoc, param, param);
            ConstraintLayout.applyLabelConstraints((Element) prevLabelNode, (Element) labelNode);
            prevLabelNode = labelNode;
            Node editNode = androidWidgets.createUIEntry(fragmentDoc, properties, param, sqlType);
            ConstraintLayout.applyEditConstraints((Element) prevEditNode, (Element) labelNode, (Element) editNode);
            prevEditNode = editNode;
        }
        Node buttonNode = XMLUtils.addChildFromTransformedResource(fragmentDoc, Constants.UIElements.CONSTRAINT_LAYOUT,
                                                  null, Constants.Templates.EXECUTE_BUTTON, properties);
        createObjectsAndroid.getStringMap().addEntry(Constants.AndroidLabels.EXECUTE, Constants.AndroidLabels.EXECUTE);
        createObjectsAndroid.getStringMap().commit();
        ConstraintLayout.applyButtonConstraints((Element) prevEditNode, (Element) buttonNode);
        XMLUtils.save(layoutXmlFile, fragmentDoc);
    }
}
