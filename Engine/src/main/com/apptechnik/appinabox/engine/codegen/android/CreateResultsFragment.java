package com.apptechnik.appinabox.engine.codegen.android;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeGen;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeStatement;
import com.apptechnik.appinabox.engine.codegen.pojos.StatementList;
import com.apptechnik.appinabox.engine.exceptions.PropertiesException;
import com.apptechnik.appinabox.engine.parser.ColumnInfo;
import com.apptechnik.appinabox.engine.parser.pojos.QueryFieldsNode;
import com.apptechnik.appinabox.engine.parser.pojos.Tree;
import com.apptechnik.appinabox.engine.util.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Stack;

/**
 * Create scalar and list results fragments.
 */
public class CreateResultsFragment {
    private final CreateObjectsAndroid createObjectsAndroid;
    private final AndroidWidgets androidWidgets;
    private final File outputDir;

    /**
     *
     * @param outputDir Output Directory (root projet directory ("."))
     * @param createObjectsAndroid
     */
    public CreateResultsFragment(File outputDir, CreateObjectsAndroid createObjectsAndroid) {
        this.createObjectsAndroid = createObjectsAndroid;
        this.outputDir = outputDir;
        this.androidWidgets = new AndroidWidgets(createObjectsAndroid);
        createObjectsAndroid.getProperties().put(Constants.Substitutions.NAME, createObjectsAndroid.getVarName());
    }

    public void createFragments() throws Exception {
        if (createObjectsAndroid.getJsonResponseTree() != null) {
            createObjectsAndroid.getJsonResponseTree().traverseTreeStack(new ResultFragments(createObjectsAndroid));
        }
    }

    public class ResultFragments implements Tree.ConsumerException<Stack<Tree<QueryFieldsNode>>> {
        private final CreateObjectsAndroid createObjectsAndroid;

        public ResultFragments(CreateObjectsAndroid createObjectsAndroid) {
            this.createObjectsAndroid = createObjectsAndroid;
        }

        public void accept(Stack<Tree<QueryFieldsNode>> stack) throws Exception {
            Tree<QueryFieldsNode> tree = stack.peek();
            String varName = tree.get().getVarName();
            String fragmentPackageName = OutputPackages.androidFragments(createObjectsAndroid.getPackageName());
            File fragmentOutputDir = OutputPaths.androidKotlinDir(outputDir, StringUtil.packageToFilePath(fragmentPackageName));
            String fragmentName = createObjectsAndroid.responseFragmentName(false, varName);
            File fragmentOutputFile = new File(fragmentOutputDir, fragmentName + Constants.Extensions.KOTLIN);

            // List elements have a list results fragment and a results fragment.
            createResultsFragment(tree, fragmentName, fragmentOutputFile);
            if (!createObjectsAndroid.isScalarNode(stack)) {
                String listFragmentName = createObjectsAndroid.responseFragmentName(true, varName);
                File listFragmentOutputFile = new File(fragmentOutputDir, listFragmentName + Constants.Extensions.KOTLIN);
                createResultsListFragment(tree, listFragmentName, listFragmentOutputFile);
            }
        }
    }


    public void createResultsFragment(Tree<QueryFieldsNode> tree, String fragmentName, File fragmentOutputFile) throws Exception {
        Properties properties = new Properties(createObjectsAndroid.getProperties());
        properties.put(Constants.Substitutions.VAR_NAME, tree.get().getVarName());
        properties.put(Constants.Substitutions.FRAGMENT_NAME, fragmentName);
        properties.put(Constants.Substitutions.TYPE, tree.get().getClassName());
        properties.put(Constants.Substitutions.NAVIGATION_LISTENERS, navigateChildListeners(tree));
        Substitute.transformResource(Constants.Templates.RESULTS_FRAGMENT, fragmentOutputFile, properties);
    }

     public void createResultsListFragment(Tree<QueryFieldsNode> tree, String fragmentName, File fragmentOutputFile) throws Exception {
        Properties properties = new Properties(createObjectsAndroid.getProperties());
        properties.put(Constants.Substitutions.VAR_NAME, tree.get().getVarName());
        properties.put(Constants.Substitutions.FRAGMENT_NAME, fragmentName);
        properties.put(Constants.Substitutions.TYPE, tree.get().getClassName());
        properties.put(Constants.Substitutions.NAVIGATION_LISTENERS, navigateListItemListener(tree.get().getVarName()));
        Substitute.transformResource(Constants.Templates.RESULTS_LIST_FRAGMENT, fragmentOutputFile, properties);
    }

    /**
     * ListItem Listener goes from ${TYPE}ResultsList to ${TYPE}Results
     * @param varName
     * @throws Exception
     */
    private String navigateListItemListener(String varName) throws Exception {
        String fragmentName = createObjectsAndroid.responseFragmentName(true, varName);
        String childFragmentName = createObjectsAndroid.responseFragmentName(false, varName);
        Properties nodeProperties = new Properties(createObjectsAndroid.getProperties());
        nodeProperties.put(Constants.Substitutions.TYPE, varName);
        nodeProperties.put(Constants.Substitutions.FRAGMENT_ACTION, CreateNavGraphXML.actionName(fragmentName, childFragmentName));
        return Substitute.transformResource(Constants.Templates.NAVIGATE_LIST_ITEM_LISTENER, nodeProperties);
    }

    /**
     * In the scalar case, listeners either go to the child object (if it is scalar), or
     * a list of child objects.
     * @param tree
     * @return
     * @throws Exception
     */
    private String navigateChildListeners(Tree<QueryFieldsNode> tree) throws Exception {
        String navigateChildListeners = "\n";
        for (Tree<QueryFieldsNode> childNode : tree.getChildren()) {
            navigateChildListeners += navigateChildListener(childNode.get().childUsesParentKeys(tree.get()), tree.get(), childNode.get()) + "\n";
        }
        return navigateChildListeners;
    }

    /**
     * Transform NavigateChildListener.kt
     */
    private String navigateChildListener(boolean isList,
                                         QueryFieldsNode node,
                                         QueryFieldsNode childNode) throws IOException, PropertiesException {
        String fragmentName = createObjectsAndroid.responseFragmentName(isList, childNode.getVarName());
        String childFragmentName = createObjectsAndroid.responseFragmentName(isList, childNode.getVarName());
        Properties nodeProperties = new Properties(createObjectsAndroid.getProperties());
        nodeProperties.put(Constants.Substitutions.TYPE, childNode.getVarName());
        nodeProperties.put(Constants.Substitutions.FRAGMENT_ACTION, CreateNavGraphXML.actionName(fragmentName, childFragmentName));
        String templateName = isList ?
                Constants.Templates.NAVIGATE_CHILD_LISTENER :
                Constants.Templates.NAVIGATE_CHILD_LIST_LISTENER;
        return Substitute.transformResource(templateName, nodeProperties);
    }

    /**
     * binding.user = User("Test", "User")
     * @param responseTree
     * @return
     */
    public StatementList bindingStatements(Tree<QueryFieldsNode> responseTree) {
        CodeGen codegen = createObjectsAndroid.getKotlinCodeGen();
        List<CodeStatement> statementList = new ArrayList<CodeStatement>();
        for (ColumnInfo column : responseTree.get().getColumnInfos()) {
            String value = column.getColumnName() + "_" + Constants.Suffixes.VALUE;
        }
        return codegen.stmtList(statementList);
    }
}
