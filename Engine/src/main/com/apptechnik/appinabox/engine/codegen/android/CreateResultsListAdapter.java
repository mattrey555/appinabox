package com.apptechnik.appinabox.engine.codegen.android;

import com.apptechnik.appinabox.engine.Constants;

import com.apptechnik.appinabox.engine.codegen.pojos.CodeBinaryExpression;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeGen;
import com.apptechnik.appinabox.engine.codegen.query.CreateQueryObjects;
import com.apptechnik.appinabox.engine.parser.ColumnInfo;
import com.apptechnik.appinabox.engine.parser.pojos.*;
import com.apptechnik.appinabox.engine.util.*;
import net.sf.jsqlparser.statement.select.Select;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Create a list adapter for a type.
 * Requires an equals() function which compares only the displayed fields.
 * This needs a mapping from the source fields to the fields displayed in
 * the adapter.  The fields in the select clause subsetted by non-child fields,
 * with @Hidden as an annotation from the object, propagated from the .SQL file
 * which defines:
 * {@Code
 * hidden: <field>...
 * field : <ident>|<field>.<ident>
 * }
 * default: fields are ordered by their primary key
 * optional sort-key and filter. Filter defaults to sort-key, only string fields can be filtered.
 * If there are more fields than can be displayed in a view holder, the first (view holder max)
 * are chosen, ordered by position in the JSON reference.
 * Fields in the SELECT clause which are not mapped to JSON should be flagged.
 * Need to map field to adapter item resource.
 *     Default matching resource is table, which maps from #items -> resource.
 *     can be overridden in properties file, with dot syntax.  Reference is android resource file name.
 *     listitem: movies.cast: adapter_5b.xml
 *  problem is:
 *      list items are templates VARIABLES, WIDGET1, WIDGET_PROPERTIES.  Too high a burden to ask
 *      developers to use templates, especially, the{@Code} <data>{VARIABLES}</data>} template
 */
public class CreateResultsListAdapter {
    private final CreateObjectsAndroid createObjectsAndroid;
    private final List<Select> selectStatements;
    private final Map<Select, ColumnsAndKeys> map;

    public CreateResultsListAdapter(CreateObjectsAndroid createObjectsAndroid, Map<Select, ColumnsAndKeys> map) {
        this.createObjectsAndroid = createObjectsAndroid;
        this.map = map;
        this.selectStatements = createObjectsAndroid.getSelectStatementList();
    }

    public void createAdapters() throws Exception {
        if (createObjectsAndroid.getJsonResponseTree() != null) {
            for (int i = 0; i < selectStatements.size(); i++) {
                Tree<QueryFieldsNode> jsonTree = CreateQueryObjects.getAssociatedJsonTree(selectStatements, i, createObjectsAndroid.getJsonResponseTree());
                jsonTree.traverseTreeStack(new ResultAdapters(createObjectsAndroid, selectStatements.get(i), map.get(selectStatements.get(i))));
            }
        }
    }

    public class ResultAdapters implements Tree.ConsumerException<Stack<Tree<QueryFieldsNode>>> {
        private final CreateObjectsAndroid createObjectsAndroid;
        private final Select select;
        private final ColumnsAndKeys columnsAndKeys;

        public ResultAdapters(CreateObjectsAndroid createObjectsAndroid, Select select, ColumnsAndKeys columnsAndKeys) {
            this.createObjectsAndroid = createObjectsAndroid;
            this.select = select;
            this.columnsAndKeys = columnsAndKeys;
        }

        /**
         * substitute: TYPE, ITEM_BINDING, COMPARE_ID, COMPARE_CONTENTS
         * COMPARE_ID: compares primary keys between objects.
         * What about the onClickListener to show the fields for non-leaf items?
         * @param stack
         * @throws Exception
         */
        public void accept(Stack<Tree<QueryFieldsNode>> stack) throws Exception {
            Tree<QueryFieldsNode> tree = stack.peek();
            if (!createObjectsAndroid.isScalarNode(stack)) {
                List<ColumnInfo> visibleFields = getDisplayedFields(columnsAndKeys.getColumnInfoSet());
                String layoutXml = itemLayoutXML(createObjectsAndroid.getKotlinCodeGen(), stack, visibleFields);
                adapterCode(createObjectsAndroid.getKotlinCodeGen(), tree, visibleFields, layoutXml);
            }
        }

        /**
         * Substitute TYPE, ITEM_BINDING, COMPARE_ID, and COMPARE_CONTENTS in ListAdapter.kt.txt
         * @param codegen code geenerator
         * @param tree recursed tree node
         * @param displayedFields list of displayed fields
         * @param layoutXml
         * @throws Exception
         */
        private void adapterCode(CodeGen codegen,
                                 Tree<QueryFieldsNode> tree,
                                 List<ColumnInfo> displayedFields,
                                 String layoutXml) throws Exception {
            QueryFieldsNode node = tree.get();
            List<String> displayedFieldNames = displayedFields.stream().map(f -> f.getColumnName(true)).collect(Collectors.toList());
            String adapterPackageName = OutputPackages.androidAdapters(createObjectsAndroid.getPackageName());
            File adapterOutputDir = OutputPaths.androidKotlinDir(createObjectsAndroid.getOutputDir(),
                                                                 StringUtil.packageToFilePath(adapterPackageName));
            // since we create adapters for each list element, we have to take the current node name into account
            File adapterOutputFile = new File(adapterOutputDir, tree.get().getVarName() + Constants.Suffixes.ADAPTER + Constants.Extensions.KOTLIN);
            ExtraProperties properties = new ExtraProperties(createObjectsAndroid.getProperties());
            String typeName = node.getNodeType(codegen, tree).toString();
            properties.put(Constants.Substitutions.TYPE, typeName);
            properties.put(Constants.Substitutions.COMPARE_ID, itemCompare(codegen, tree.get().primaryKeyName(true)));
            properties.put(Constants.Substitutions.COMPARE_CONTENTS, compareContents(codegen, displayedFieldNames).toString());
            properties.put(Constants.Substitutions.ITEM_BINDING, columnBindings(displayedFields));
            Substitute.transformResource(Constants.Templates.ANDROID_LIST_ADAPTER, adapterOutputFile, properties);
        }

        private String itemLayoutXML(CodeGen codegen, Stack<Tree<QueryFieldsNode>> stack, List<ColumnInfo> displayedFields)
        throws Exception {
            String dotReference = QueryFieldsNode.toDotReference(stack);
            String layoutXml = getLayoutXml(dotReference, displayedFields.size());
            populateLayoutXML(codegen, stack, layoutXml, displayedFields);
            return layoutXml;
        }

        private String getLayoutXml(String dotReference, int numFields) throws IOException {
            String layoutXmlFile = createObjectsAndroid.getSqlFile().getLayoutName(dotReference, numFields);
            return FileUtil.getResource(CreateResultsListAdapter.class, layoutXmlFile);
        }

        /**
         * VARIABLES
         * for each variable:
         *      <variable name="${NAME}" type="${TYPE}"/>
         *
         * VAR{N}
         *      maps to column names (without table references)
         * WIDGET_PROPERTIES{N}
         *      android:text=${var.field}
         * @param layoutXml
         * @param displayedFields
         */
        private void populateLayoutXML(CodeGen codegen,
                                       Stack<Tree<QueryFieldsNode>> stack,
                                       String layoutXml,
                                       List<ColumnInfo> displayedFields)
        throws Exception {
            ExtraProperties properties = new ExtraProperties();
            QueryFieldsNode node = stack.peek().get();
            properties.put(Constants.Substitutions.NAME, node.getVarName());
            properties.put(Constants.Substitutions.VARIABLES,
                           String.format("<variable name=\"%s\" type=\"%s\"/>\n", node.getVarName(), node.getFullClassName(createObjectsAndroid)));
            for (int i = 0; i < displayedFields.size(); i++) {
                addDisplayedField(properties, node.getVarName(), displayedFields.get(i), i);
            }
            File outputLayoutFile = new File(OutputPaths.androidLayoutDir(createObjectsAndroid.getOutputDir()),
                                             layoutFileName(stack));
            Substitute.transformString(layoutXml, properties, outputLayoutFile);
        }

        /**
         * For a column, set the variable, display widgets and widget properties. VAR(i), WIDGET(i), WIDGET_PROPERTIES(i)
         * @param properties properties used for transformation.
         * @param varName variable name of node
         * @param columnInfo source column name and type
         * @param i index
         */
        private void addDisplayedField(Properties properties, String varName, ColumnInfo columnInfo, int i) {
            properties.put(Constants.Substitutions.VAR + (i + 1), columnInfo.getColumnName(true));

            // We select the first widget. Later, we'll take directives from the SQLFile
            String[] widgets = AndroidWidgets.getDisplay(columnInfo.getDataType());
            AndroidWidgets.ElementProperties elementProperties = AndroidWidgets.getElementProperties(widgets[0]);
            String bind = CreateAdapterItemLayoutXML.bindSingleVariable(elementProperties.getProperty(Constants.UIProperties.VALUE),
                                                                     varName + "." + columnInfo.getColumnName());
            properties.put(Constants.Substitutions.WIDGET + (i + 1), widgets[0]);
            properties.put(Constants.Substitutions.WIDGET_PROPERTIES + (i + 1), bind);
        }

        /**
         * ${COLUMN}.${PROPERTY} = item.${COLUMN}
         * @param columnInfo
         * @return
         */
        private String columnBinding(ColumnInfo columnInfo) {
            String[] widgets = AndroidWidgets.getDisplay(columnInfo.getDataType());

            // We select the first widget. Later, we'll take directives from the SQLFile
            AndroidWidgets.ElementProperties elementProperties = AndroidWidgets.getElementProperties(widgets[0]);
            String col = columnInfo.getColumnName(true);
            String property = elementProperties.getProperty(Constants.UIProperties.VALUE);
            return String.format("%s.%s = item.%s", col, property, col);
        }

        private String columnBindings(List<ColumnInfo> columnInfoList) {
            StringBuffer sb = new StringBuffer();
            int numItems = ListItems.getNumElements(columnInfoList.size());
            for (int i = 0; i < numItems; i++) {
                sb.append(columnBinding(columnInfoList.get(i)) + ";\n");
            }
            return sb.toString();
        }

        /**
         * The layout files have to be segregated for each subtype, meaning that if we have two queries with nodes that
         * contain "stores"
         * @param stack path to this node,
         * @return ${root}_${var}_.. path_to_node.xml
         */
        private String layoutFileName(Stack<Tree<QueryFieldsNode>> stack) {
            List<String> vars = new ArrayList<String>(stack.size() + 1);
            vars.add(createObjectsAndroid.getVarName());
            stack.stream().forEach(tree -> vars.add(tree.get().getVarName()));
            return StringUtil.concat(vars, "_") + Constants.Extensions.XML;
        }

        /**
         * Filter the columns by the not "hidden" tag
         * @param columns
         * @return
         * @throws Exception
         */
        private List<ColumnInfo> getDisplayedFields(Collection<ColumnInfo> columns) throws Exception {
            List<String> hiddenFields = createObjectsAndroid.getSqlFile().findTags(Constants.Directives.HIDDEN);
            return columns.stream()
                    .filter(candk -> !hiddenFields.contains(candk.getColumnName(true)))
                    .collect(Collectors.toList());
        }

        //private void populateLayou
        /**
         * The properties file can contain an item layout reference.  SQLFile maintains a map between
         * the layout resource files and the number of items displayed.
         * oldItem.${field} == newItem.${field} and ...
         */
        private CodeExpression compareContents(CodeGen codegen, List<String> visibleFields){
            if (visibleFields.isEmpty()) {
                return codegen.methodCall(Constants.KotlinFunctions.ARE_ITEMS_THE_SAME,
                                          codegen.var(Constants.KotlinVariables.OLD_ITEM),
                                          codegen.var(Constants.KotlinVariables.NEW_ITEM));
            }
            CodeExpression compareExpr = itemCompare(codegen, visibleFields.get(0));
            for (int i = 1; i < visibleFields.size(); i++) {
                compareExpr = codegen.binaryExpr(Constants.KotlinOperators.AND,
                                                 compareExpr, itemCompare(codegen, visibleFields.get(i)));
            }
            return compareExpr;
        }

        CodeBinaryExpression itemCompare(CodeGen codegen, String field) {
            return codegen.binaryExpr(Constants.KotlinOperators.EQUALS,
                                      codegen.dot(codegen.var(Constants.KotlinVariables.NEW_ITEM), codegen.var(field)),
                                      codegen.dot(codegen.var(Constants.KotlinVariables.OLD_ITEM), codegen.var(field)));
        }
    }
}
