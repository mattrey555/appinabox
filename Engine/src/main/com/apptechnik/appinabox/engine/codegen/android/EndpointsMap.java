package com.apptechnik.appinabox.engine.codegen.android;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;
import com.apptechnik.appinabox.engine.codegen.objects.ProcessSQLFiles;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.codegen.query.CreateQueryObjects;
import com.apptechnik.appinabox.engine.parser.TableInfoMap;
import com.apptechnik.appinabox.engine.parser.create.StatementParser;
import com.apptechnik.appinabox.engine.util.CheckedFunction;
import com.apptechnik.appinabox.engine.util.StatementUtil;
import com.apptechnik.appinabox.engine.util.StringUtil;
import net.sf.jsqlparser.statement.Statement;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class EndpointsMap extends HashMap<String, List<EndpointsMap.EndpointInfo>> {

    public static class EndpointInfo {
        private final File sqlfile;
        private final FunctionDeclaration fn;

        public EndpointInfo(File sqlfile, FunctionDeclaration fn) {
            this.sqlfile = sqlfile;
            this.fn = fn;
        }

        public File getSqlfile() {
            return sqlfile;
        }

        public FunctionDeclaration getFn() {
            return fn;
        }

        public Type endpointReturnBaseType() {
            return ((GenericType) getFn().getReturnType()).getItemList().get(0);
        }

        public ParameterList stripAnnotations(CodeGen codegen) {
            return codegen.paramList(fn.getParameterList().getParameterDeclarationList().stream()
                                        .map(decl -> codegen.paramDecl(decl.getType(), decl.getVariable()))
                                        .collect(Collectors.toList()));
        }

        public FunctionDeclaration toViewModelFn(CodeGen codegen) {
            return codegen.functionDecl(false, true, Constants.KotlinKeywords.PUBLIC, false,
                                        fn.getName(), null, stripAnnotations(codegen), null, null, null);
        }
    }

    /**
     * Recurse through the .SQL files and generate a map of unique repository names from the tables
     * used in the SQL queries
     * @param sqlDir
     * @param statementParser
     * @param outputDir
     * @param createTableFile
     * @param propertiesFile
     * @return
     * @throws IOException
     */
    public EndpointsMap(File sqlDir,
                        StatementParser statementParser,
                        File outputDir,
                        File createTableFile,
                        File propertiesFile) throws IOException {
        ProcessFile processFile = new ProcessFile(statementParser, outputDir, createTableFile, propertiesFile);
        CreateObjects.walkSQLDir(sqlDir, processFile);
    }

    private class ProcessFile extends ProcessSQLFiles {
        public ProcessFile(StatementParser statementParser,
                           File outputDir,
                           File createTableFile,
                           File propertiesFile) throws IOException {
            super(statementParser, outputDir, createTableFile, propertiesFile);
        }

        @Override
        public void accept(File sqlFile) throws Exception {
            CreateObjectsAndroid createObjects = new CreateObjectsAndroid(sqlFile, propertiesFile, createTableFile,
                                                                          statementParser, outputDir);
            if (createObjects.getJsonResponseTree() != null) {
                CreateQueryObjects.resolveSelects(createObjects, createObjects.getJsonResponseTree());
                FunctionDeclaration endpoint = RetrofitEndpoint.endpointFn(createObjects.getKotlinCodeGen(), createObjects);
                addEndpoint(createObjects, sqlFile, endpoint, createObjects.getTableInfoMap());
            }
        }

        private void addEndpoint(CreateObjectsAndroid createObjects,
                                 File sqlFile,
                                 FunctionDeclaration endpoint,
                                 TableInfoMap tableInfoMap) {
            String key = createObjects.getSqlFile().findTag(Constants.Directives.REPOSITORY);
            if (key == null) {
                // get the tables names used in the SQL queries in the statementList, map them to a set, then
                // sort them and generate the key.
                List<Statement> statementList = createObjects.getSQLStatementList();
                List<String> tableNames =
                        new ArrayList<String>(
                                statementList.stream()
                                        .map(CheckedFunction.wrap(s -> StatementUtil.getTables(s, tableInfoMap).keySet()))
                                        .flatMap(Collection::stream)
                                        .collect(Collectors.toSet()));
                tableNames.sort(new StringUtil.StringComparator());
                key = StringUtil.concat(
                        tableNames.stream()
                                .map(s -> StringUtil.underscoresToMixedCase(s, true))
                                .collect(Collectors.toList()), "");
            }
            addEndpointToMap(key, sqlFile, endpoint);
        }

        /**
         * Add the endpoint to the list of endpoints for the repository referenced by key.
         * @param key repository name
         * @param sqlFile source SQL file for endpoint
         * @param endpoint endpoint function declaration.
         */
        private void addEndpointToMap(String key, File sqlFile, FunctionDeclaration endpoint) {
            List<EndpointInfo> endpointList = get(key);
            if (endpointList == null) {
                endpointList = new ArrayList<EndpointInfo>();
                put(key, endpointList);
            }
            endpointList.add(new EndpointInfo(sqlFile, endpoint));
        }
    }

    /**
     * Given a a SQL file, search the tableEndpoints map to find the corresponding repository name
     * @param sqlFile
     * @return
     */
    public String getRepositoryForEndpoint(File sqlFile) {
        return keySet().stream()
                .filter(key -> hasSQLFile(get(key), sqlFile))
                .findFirst()
                .orElse(null);
    }

    public String getRepositoryForEndpoint(String endpointName) {
        return keySet().stream()
                .filter(key -> hasEndpoint(get(key), endpointName))
                .findFirst()
                .orElse(null);
    }

    /**
     * Return the function declaration for the named endpoint.
     * @param endpointName
     * @return
     */
    public FunctionDeclaration getEndpointForName(String endpointName) {
        return values().stream()
                .flatMap(List::stream)
                .map(e -> e.getFn())
                .filter(f -> f.getName().equals(endpointName))
                .findFirst().orElse(null);
    }

    public EndpointsMap.EndpointInfo getEndpointInfoForName(String endpointName) {
        return values().stream()
                .flatMap(List::stream)
                .filter(ei -> ei.sqlfile.getAbsolutePath().equals(endpointName))
                .findFirst().orElse(null);
    }

    private static boolean hasEndpoint(List<EndpointInfo> endpointInfoList, String endpointName) {
        return endpointInfoList.stream()
                .map(e -> e.getFn())
                .filter(fn -> fn.getName().equals(endpointName))
                .findAny().isPresent();
    }

    private static boolean hasSQLFile(List<EndpointInfo> endpointInfoList, File sqlFile) {
        return endpointInfoList.stream()
                .map(e -> e.getSqlfile())
                .filter(s -> s.equals(sqlFile))
                .findAny().isPresent();
    }
}
