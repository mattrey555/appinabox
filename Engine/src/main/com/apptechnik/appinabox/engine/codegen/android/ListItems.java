package com.apptechnik.appinabox.engine.codegen.android;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.exceptions.PrimaryKeyNotFoundException;
import com.apptechnik.appinabox.engine.exceptions.PropertiesException;
import com.apptechnik.appinabox.engine.parser.ColumnInfo;
import com.apptechnik.appinabox.engine.parser.pojos.QueryFieldsNode;
import com.apptechnik.appinabox.engine.util.OutputPackages;
import com.apptechnik.appinabox.engine.util.OutputPaths;
import com.apptechnik.appinabox.engine.util.StringUtil;
import com.apptechnik.appinabox.engine.util.Substitute;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * Methods for mapping from JSON to List Items for display.
 */
public class ListItems {

    private static class ListElementMap {
        private final int numItems;
        private final String[] layouts;

        public ListElementMap(int numItems, String[] layouts) {
            this.numItems = numItems;
            this.layouts = layouts;
        }
    }

    // TODO: read this from JSON.
    private static ListElementMap[] LIST_ELEMENT_MAP = {
        new ListElementMap(1, new String[] { Constants.LayoutTemplates.LIST_ELEMENT_1_XML }),
        new ListElementMap(2, new String[] { Constants.LayoutTemplates.LIST_ELEMENT_2A_XML,
                                                      Constants.LayoutTemplates.LIST_ELEMENT_2B_XML }),
        new ListElementMap(3, new String[] { Constants.LayoutTemplates.LIST_ELEMENT_3A_XML,
                                                      Constants.LayoutTemplates.LIST_ELEMENT_3B_XML,
                                                      Constants.LayoutTemplates.LIST_ELEMENT_3C_XML})
    };

    public static int getMaxItems() {
        return LIST_ELEMENT_MAP.length;
    }

    public static int getNumElements(int numFields) {
        return Math.min(numFields, LIST_ELEMENT_MAP.length);
    }

    public static String[] getListElements(int numItems) {
        ListElementMap map = Arrays.stream(LIST_ELEMENT_MAP)
                .filter(el -> el.numItems == numItems).findFirst().orElse(null);
        return (map != null) ? map.layouts : LIST_ELEMENT_MAP[LIST_ELEMENT_MAP.length - 1].layouts;
    }

    /**
     * Fill ListAdapter.kt.txt
     * BINDING name of data binding (map from film_query_limitlist_item to FilmQueryLimitListItemBinding)
     * COMPARE_CONTENTS compare view holder contents.
     * ITEM_BINDING
     * @param createObjects
     * @param node
     * @throws IOException
     * @throws PropertiesException
     */
    public static void populateListAdapter(CreateObjects createObjects,
                                           QueryFieldsNode node) throws Exception {
        String adapterPackageName = OutputPackages.androidAdapters(createObjects.getPackageName());
        File adapterOutputDir = OutputPaths.androidKotlinDir(createObjects.getOutputDir(),
                                                             StringUtil.packageToFilePath(adapterPackageName));
        File listAdapterFile = new File(adapterOutputDir, node.getVarName() + Constants.Suffixes.ADAPTER + Constants.Extensions.KOTLIN);

        CodeGen codegen = createObjects.getKotlinCodeGen();
        Properties properties = new Properties(createObjects.getProperties());
        properties.put(Constants.Substitutions.TYPE, node.getVarName());
        StatementList itemBindingStatements = itemBindingStatements(codegen, node);
        properties.put(Constants.Substitutions.ITEM_BINDING, itemBindingStatements.toString());
        properties.put(Constants.Substitutions.BINDING, bindingName(node));
        properties.put(Constants.Substitutions.COMPARE_ID, compareIdExpression(codegen, node).toString());
        properties.put(Constants.Substitutions.COMPARE_CONTENTS, compareContentsExpression(codegen, node).toString());
        Substitute.transformResource(Constants.Templates.ANDROID_LIST_ADAPTER, listAdapterFile, properties);
    }

    /**
     * BINDING name of data binding (map from film_list_item to filmListItemBinding)
     * @return
     */
    private static String bindingName(QueryFieldsNode node) {
        return StringUtil.underscoresToMixedCase(node.getVarName(), true) + Constants.AndroidThings.LIST_ITEM_BINDING;
    }

    /**
     * Generate the statement list to assign values in the view holder to our data. In the function:
     * {@code
     * fun bind(item: String) = with(binding) {
     *     text.text = item1
     *     title.text = item2
     * }
     * }
     * generate
     * {@code
     *     text.text = item1
     *     title.text = item2
     * }
     * @param codegen
     * @param node
     * @return
     */
    // NOTE: not generating correct statements.
    private static StatementList itemBindingStatements(CodeGen codegen, QueryFieldsNode node) {
        List<CodeStatement> itemBindingStatementList = new ArrayList<>();
        for (ColumnInfo columnInfo : node.getColumnInfos()) {
            String columnName = columnInfo.getColumnName(true);
            CodeExpression itemRef = codegen.dot(codegen.var(Constants.Variables.ITEM), codegen.var(columnName));

            // for the moment, we are just binding the text field with no formatting.
            // TODO: bind based on widget type: use SQLMapType to find the string conversion function
            CodeExpression itemBinding = codegen.dot(codegen.var(Constants.Variables.ITEM_BINDING),
                                                     codegen.dot(codegen.var(columnName),
                                                                 codegen.var(Constants.Variables.TEXT)));
            AssignmentStatement itemAssignment = codegen.assignStmt(itemBinding, itemRef);
            itemBindingStatementList.add(itemAssignment);
        }
        return codegen.stmtList(itemBindingStatementList);
    }

    /**
     * COMPARE_ID
     * {code
     * oldItem.${PRIMARY_KEY} == newItem.${PRIMARY_KEY}
     * }
     * @param codegen
     * @param node
     * @return
     */
    private static CodeExpression compareIdExpression(CodeGen codegen, QueryFieldsNode node)
    throws PrimaryKeyNotFoundException{
        ColumnInfo columnInfo = node.getColumnInfos().stream()
                                    .filter(col -> col.isPrimaryKey())
                                    .findFirst().orElse(null);

        // TODO: this requires that all sub-objects in a mapping have a primary key, so this
        // should be enforced earlier (Verify this)
        if (columnInfo != null) {
            return compareColumns(codegen, columnInfo);
        } else {
            throw new PrimaryKeyNotFoundException("failed to find primary key in " + node);
        }
    }

    /**
     * COMPARE_CONTENTS
     * {code
     * oldItem.${COLUMN} == newItem.${COLUMN} [&& ...]
     * }
     * @param codegen
     * @param node
     * @return
     */
    private static CodeExpression compareContentsExpression(CodeGen codegen, QueryFieldsNode node) {
        CodeExpression combinedExpression = null;

        // TODO: this isn't quite correct, because it compares all the fields in the select statement.  It
        // should only compare the fields which are actually displayed
        for (ColumnInfo columnInfo : node.getColumnInfos()) {
            CodeBinaryExpression comparisonExpression = compareColumns(codegen, columnInfo);
            if (combinedExpression == null) {
                combinedExpression = comparisonExpression;
            } else {
                combinedExpression = codegen.binaryExpr(Constants.KotlinOperators.AND, combinedExpression, comparisonExpression);
            }
        }
        return combinedExpression;
    }

    /**
     * {code
     * oldItem.${COLUMN} == newItem.${COLUMN} [&& ...]
     * }
     * @param codegen
     * @param columnInfo
     * @return
     */
    private static CodeBinaryExpression compareColumns(CodeGen codegen, ColumnInfo columnInfo) {
        Variable oldItemVar = codegen.var(Constants.Variables.OLD_ITEM);
        Variable newItemVar = codegen.var(Constants.Variables.NEW_ITEM);
        CodeExpression oldValue = codegen.dot(oldItemVar, codegen.var(columnInfo.getColumnName(true)));
        CodeExpression newValue = codegen.dot(newItemVar, codegen.var(columnInfo.getColumnName(true)));
        return codegen.binaryExpr(Constants.KotlinOperators.EQUALS, oldValue, newValue);
    }
}
