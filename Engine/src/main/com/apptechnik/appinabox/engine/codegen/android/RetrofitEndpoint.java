package com.apptechnik.appinabox.engine.codegen.android;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.codegen.util.CodegenUtil;
import com.apptechnik.appinabox.engine.exceptions.*;
import com.apptechnik.appinabox.engine.util.ExtraProperties;
import com.apptechnik.appinabox.engine.util.OutputPackages;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class RetrofitEndpoint {

    /**
     * Create a query endpoint of the form
     * {@Code
     * @[GET|POST|PUT|DELETE]("<url>")
     * fun get<name>([@Query("<var>") <var>| @Path("<var>") var| @Header("<var>"> var],...) : Call<object>
     * }
     * @param codegen kotlin code generator
     * @return Endpoint function declaration for interfce.
     * @throws TypeNotFoundException
     * @throws NameNotFoundException
     */
    public static FunctionDeclaration endpointFn(CodeGen codegen,
                                                 CreateObjectsAndroid createObjectsAndroid)
            throws TypeNotFoundException, DirectiveNotFoundException, UnsupportedParamTypeException, NameNotFoundException {
        // if there is a response, Call<List<node-type>> otherwise Unit
        Type returnCallType = createObjectsAndroid.retrofitCallReturnType(codegen);

        // generate the parameter list from the query parameters, and if there is a request type, then add it.
        ParameterList paramList = codegen.paramList(CodegenUtil.endpointParameterDeclarations(codegen, createObjectsAndroid.getQueryPackageName(),
                                                                                              createObjectsAndroid.getQueryParams()));
        if (createObjectsAndroid.getJsonRequestTree() != null) {
            Type requestType = createObjectsAndroid.retrofitCallRequestType(codegen);
            paramList.addParam(codegen.paramDecl(requestType, codegen.var(createObjectsAndroid.getJsonRequestTree().get().getVarName())));
        }
        // craete the interface declaration.
        String name = createObjectsAndroid.getVarName();
        String retrofitUrl = convertToRetrofitUrl(createObjectsAndroid.getSqlFile().getTag(Constants.Directives.URL));
        AnnotationList annotList = codegen.annotList(createObjectsAndroid.getServletHttpMethod().toUpperCase(), codegen.stringConst(retrofitUrl));
        return codegen.functionDecl(Constants.JavaKeywords.PUBLIC, false, name, paramList, returnCallType, null, annotList);
    }

    /**
     * Our URL spec uses ${var}, but retrofit uses {var}
     * @param url
     * @return
     */
    private static String convertToRetrofitUrl(String url) {
        return url.replaceAll("\\$\\{", "{");
    }

    /**
     * companion object {
     *         fun getInstance(network : Network) : dvdrentalApi {
     *             return network.createNetworkAdapter(dvdrentalApi::class.java)
     *         }
     *     }
     */
    private static FunctionDefinition getInstanceDef(CodeGen codegen, String endpointName) {
        ParameterDeclaration paramDecl = codegen.paramDecl(codegen.classType(Constants.KotlinTypes.NETWORK),
                Constants.KotlinVariables.NETWORK);
        FunctionDeclaration fnDecl = codegen.functionDecl(Constants.KotlinFunctions.GET_INSTANCE, true,
                                                          codegen.paramList(paramDecl), codegen.classType(endpointName));
        ReturnStatement returnStatement =
                codegen.returnStmt(codegen.dot(codegen.var(Constants.KotlinVariables.NETWORK),
                                               codegen.methodCall(Constants.KotlinFunctions.CREATE_NETWORK_ADAPTER,
                                                                  codegen.var(endpointName + Constants.Suffixes.CLASS_JAVA))));
        return codegen.functionDef(fnDecl, codegen.stmtList(returnStatement));
    }

    /**
     * Create the endpoint interface file:
     * {@Code}
     * {imports}
     * object RestServer {
     *      interface dvdrentalApi  {
     *          @POST("/insert_query_film")
     *          public fun insert_query_film(insert_film : List<com.appinabox.dvdrental.insert_query_film.update.insert_film>) : Call<List<Int>>
     *          ...
     *
     * }
     * @param codegen
     * @param properties to get package and endpoint name
     * @param endpointMethods list of endpoint methods
     * @return
     * @throws PropertiesException
     * @throws IOException
     */
    public static ClassFile endpointClassFile(CodeGen codegen,
                                              ExtraProperties properties,
                                              List<FunctionDeclaration> endpointMethods)
    throws PropertiesException, IOException {
        String packageName = properties.getProp(Constants.Properties.PACKAGE, "Package name, ex: com.company.project");
        String networkPackageName = OutputPackages.androidNetwork(packageName);
        String endpointName = properties.getProp(Constants.Properties.ENDPOINT_NAME, "Android Endpoint API name");
        StatementList importStatementList = codegen.importStatementList(Constants.Templates.ANDROID_ENDPOINT_IMPORTS);
        List<InterfaceDeclaration> endpointInterfaces = Collections.singletonList(codegen.interfaceDecl(endpointName, endpointMethods));
        List<FunctionDefinition> classFns = Collections.singletonList(getInstanceDef(codegen, endpointName));
        ClassDeclaration classDecl = codegen.classDecl(Constants.KotlinTypes.REST_SERVER);
        ClassDefinition classDef =
            codegen.classDef(classDecl, null, null, classFns, null, endpointInterfaces);
        return codegen.classFile(codegen.packageStmt(networkPackageName), importStatementList, classDef);
    }
}
