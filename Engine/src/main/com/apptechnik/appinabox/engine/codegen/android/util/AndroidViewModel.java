package com.apptechnik.appinabox.engine.codegen.android.util;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.android.CreateObjectsAndroid;
import com.apptechnik.appinabox.engine.codegen.android.EndpointsMap;
import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.codegen.pojos.kotlin.KotlinCodeGen;
import com.apptechnik.appinabox.engine.codegen.query.CreateQueryObjects;
import com.apptechnik.appinabox.engine.codegen.util.CodegenUtil;
import com.apptechnik.appinabox.engine.parser.create.StatementParser;
import com.apptechnik.appinabox.engine.parser.pojos.QueryFieldsNode;
import com.apptechnik.appinabox.engine.parser.pojos.Tree;
import com.apptechnik.appinabox.engine.util.*;


import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * substitute ViewModel.kt.txt
 * PACKAGE root package name
 * ENDPOINT_NAME function name of retrofit endpoint
 * REPOSITORY_NAME repository name (which groups retrofit endpoints
 * TYPE return type from endpoint
 * ARGS endpoint parameter declartions mapped to arguments.
 */
public class AndroidViewModel {
    public static void createViewModels(File sqlDir,
                                        StatementParser statementParser,
                                        File outputDir,
                                        File createTableFile,
                                        File propertiesFile,
                                        EndpointsMap endpointsMap) throws Exception {
        ProcessFile processFile = new ProcessFile(statementParser, outputDir, createTableFile, propertiesFile, endpointsMap);
        CreateObjects.walkSQLDir(sqlDir, processFile);
        createViewModelFactories(endpointsMap, propertiesFile, createTableFile, outputDir);
    }

    private static class ProcessFile implements CheckedConsumer<File> {
        private final StatementParser statementParser;              // PERFORMANCE: tableInfo parsed from schema.
        private final File outputDir;                               // .java output directory
        private final File createTableFile;                         // schema file
        private final File propertiesFile;                          // .properties file for template substitution
        private final EndpointsMap endpointsMap;

        public ProcessFile(StatementParser statementParser,
                           File outputDir,
                           File createTableFile,
                           File propertiesFile,
                           EndpointsMap endpointsMap) {
            this.statementParser = statementParser;
            this.outputDir = outputDir;
            this.createTableFile = createTableFile;
            this.propertiesFile = propertiesFile;
            this.endpointsMap = endpointsMap;
        }

        @Override
        public void accept(File sqlFile) throws Exception {
            CreateObjectsAndroid createObjects = new CreateObjectsAndroid(sqlFile, propertiesFile, createTableFile,
                                                                          statementParser, outputDir);
            if (createObjects.getJsonResponseTree() != null) {
                Tree<QueryFieldsNode> responseNodeTree = createObjects.getJsonResponseTree();
                CreateQueryObjects.resolveSelects(createObjects, responseNodeTree);
                EndpointsMap.EndpointInfo endpoint = endpointsMap.getEndpointInfoForName(sqlFile.getAbsolutePath());
                ExtraProperties properties = new ExtraProperties(createObjects.getProperties());
                viewModelProperties(properties, sqlFile, createObjects, endpoint);
                String packageName = properties.getProp(Constants.Properties.PACKAGE, "package name");
                String viewModelPackage = OutputPackages.androidViewModels(packageName);
                File viewModelOutputDir = OutputPaths.androidKotlinDir(outputDir, StringUtil.packageToFilePath(viewModelPackage));
                File viewModelOutputFile = new File(viewModelOutputDir, endpoint.getFn().getName() + Constants.Suffixes.VIEW_MODEL + Constants.Extensions.KOTLIN);
                Substitute.transformResource(Constants.Templates.VIEW_MODEL, viewModelOutputFile, properties, false);
            }
        }

        /**
         * substitute ViewModel.kt.txt
         * PACKAGE root package name
         * ENDPOINT_NAME function name of retrofit endpoint
         * REPOSITORY_NAME repository name (which groups retrofit endpoints
         * TYPE return type from endpoint
         * ARGS endpoint parameter declartions mapped to arguments.
         */
        private void viewModelProperties(ExtraProperties properties,
                                         File sqlFile,
                                         CreateObjectsAndroid createObjects,
                                         EndpointsMap.EndpointInfo endpoint) throws Exception {
            CodeGen codegen = createObjects.getKotlinCodeGen();
            Tree<QueryFieldsNode> responseNodeTree = createObjects.getJsonResponseTree();
            Type responseType = codegen.listType(QueryFieldsNode.getNodeType(codegen, createObjects.getQueryPackageName(), responseNodeTree));
            properties.put(Constants.Substitutions.TYPE, responseType.toString());
            String repositoryName = endpointsMap.getRepositoryForEndpoint(sqlFile);
            properties.put(Constants.Substitutions.REPOSITORY_NAME, repositoryName);
            MethodCall endpointCall = CodegenUtil.toMethodCall(codegen, endpoint.getFn());
            properties.put(Constants.Substitutions.ENDPOINT_CALL, endpointCall.toString());
            properties.put(Constants.Substitutions.ENDPOINT_DECL, endpoint.toViewModelFn(codegen).toString());
            properties.put(Constants.Substitutions.ENDPOINT_NAME, endpoint.getFn().getName());
            String packageName =  properties.getProp(Constants.Properties.PACKAGE, "package name");
            properties.put(Constants.Substitutions.PACKAGE, packageName);
        }
    }

    private static void createViewModelFactories(EndpointsMap endpointsMap,
                                                 File propertiesFile,
                                                 File createTableFile,
                                                 File outputDir) throws Exception {
        Properties properties = new ExtraProperties(propertiesFile);
        CodeGen codegen = new KotlinCodeGen();
        String packageName = properties.getProperty(Constants.Properties.PACKAGE, "package name");
        String viewModelPackage = OutputPackages.androidViewModels(packageName);
        String repoPackage = OutputPackages.androidRepositories(packageName);
        PackageStatement packageStatement = codegen.packageStmt(viewModelPackage);
        for (String repoName : endpointsMap.keySet()) {
            String fullRepoName = repoName + Constants.Suffixes.REPOSITORY;
            List<String> endpointNameList =
                    endpointsMap.get(repoName).stream()
                            .map(e -> e.getFn().getName())
                            .collect(Collectors.toList());
            StatementList importStatements = codegen.importStatementList(Constants.Templates.VIEW_MODEL_FACTORY_IMPORTS);
            importStatements.addStatement(codegen.importStmt(repoPackage + "." + fullRepoName));
            ClassDefinition classDef = viewModelFactoryDefinition(codegen, fullRepoName, endpointNameList);
            ClassFile classFile = codegen.classFile(packageStatement, importStatements, classDef);
            classFile.write(outputDir);
        }
    }
    /**
     * class ViewModelFactory(private val repository : ${REPOSITORY_NAME}) : ViewModelProvider.Factory {
     *
     *     override fun <T : ViewModel> create(modelClass: Class<T>): T {
     *         if (modelClass.isAssignableFrom(${ENDPOINT_NAME}ViewModel::class.java)) {
     *             @Suppress("UNCHECKED_CAST")
     *             return ${ENDPOINT_NAME}ViewModel(repository) as T
     *         } else if ...
     *         }
     *         throw IllegalArgumentException("Unknown ViewModel class")
     *     }
     * }
     */

    private static ClassDefinition viewModelFactoryDefinition(CodeGen codegen,
                                                              String repositoryName,
                                                              List<String> endpointNameList) {
        // TODO: generic?
        Type paramType = codegen.genericType(codegen.classType(Constants.KotlinTypes.CLASS), codegen.classType(Constants.KotlinTypes.T));
        ParameterList paramList = codegen.paramList(codegen.paramDecl(paramType, codegen.var(Constants.KotlinVariables.MODEL_CLASS)));
        // TODO: generic?
        Type returnType = codegen.classType(Constants.KotlinTypes.T);
        ConstructorDefinition constructorDef = viewModelConstructorDefinition(codegen, repositoryName);
        MemberDeclaration applicationMember = codegen.memberDecl(codegen.classType(Constants.KotlinTypes.APPLICATION),
                                                                 codegen.var(Constants.KotlinVariables.APPLICATION));
        MemberDeclaration repoMember = codegen.memberDecl(codegen.classType(repositoryName), codegen.var(Constants.KotlinVariables.REPOSITORY));
        List<MemberDeclaration> members = new ArrayList<MemberDeclaration>();
        members.add(applicationMember);
        members.add(repoMember);

        TypeList genericsList = codegen.typeList(codegen.extendsType(Constants.KotlinTypes.T,
                codegen.classType(Constants.KotlinTypes.VIEW_MODEL)));
        FunctionDeclaration createDecl = codegen.functionDecl(true, false, Constants.KotlinKeywords.PUBLIC, false,
                                                              Constants.KotlinFunctions.CREATE, genericsList, paramList, returnType,
                                                              null, null);
        List<CodeStatement> statementList =
                endpointNameList.stream().map(name -> viewModelStmt(codegen, name)).collect(Collectors.toList());
        statementList.add(codegen.throwStmt(codegen.alloc(codegen.classType(Constants.KotlinTypes.EXCEPTION),
                                                          codegen.argList(codegen.stringConst("class not found")))));
        FunctionDefinition fnDef = codegen.functionDef(createDecl, codegen.stmtList(statementList));
        return codegen.classDef(viewModelFactoryDeclaration(codegen, repositoryName),
                                members,
                                Collections.singletonList(constructorDef),
                                Collections.singletonList(fnDef));
    }

    /**
     * if (modelClass.isAssignableFrom(${ENDPOINT_NAME}ViewModel::class.java)) {
     *      @Suppress("UNCHECKED_CAST")
     *      return ${ENDPOINT_NAME}ViewModel(repository) as T
     * @param codegen
     * @param endpointName
     * @return
     */
    private static CodeStatement viewModelStmt(CodeGen codegen, String endpointName) {
        Variable classVar = codegen.var(endpointName + Constants.KotlinVariables.VIEW_MODEL_CLASS_JAVA);
        CodeExpression classExpr = codegen.dot(codegen.var(Constants.KotlinVariables.MODEL_CLASS),
                codegen.methodCall(Constants.KotlinFunctions.IS_ASSIGNABLE_FROM, classVar));

        // TODO: Generic?
        CodeExpression castExpr = codegen.castExpr(codegen.classType(Constants.KotlinTypes.T),
                codegen.alloc(codegen.classType(endpointName + Constants.Suffixes.VIEW_MODEL),
                        codegen.var(Constants.KotlinVariables.APPLICATION),
                        codegen.var(Constants.KotlinVariables.REPOSITORY)));
        Annotation suppressUnchecked = codegen.annotation(Constants.Annotations.SUPPRESS,
                                                          codegen.annotationArg(codegen.stringConst(Constants.Annotations.UNCHECKED_CAST)));
        ReturnStatement returnStatement = codegen.returnStmt(castExpr);
        return codegen.ifStmt(classExpr, suppressUnchecked, returnStatement);
    }

    /**
     * class ${REPOSITORY}ViewModelFactory(private val repository : ${REPOSITORY_NAME}) : ViewModelProvider.Factory
     */
    private static ClassDeclaration viewModelFactoryDeclaration(CodeGen codegen, String repositoryName) {
        TypeList implementsTypeList = codegen.typeList(codegen.classType(Constants.KotlinTypes.VIEW_MODEL_PROVIDER_FACTORY));
        // TODO: Generic?
        TypeList genericList = codegen.typeList(codegen.classType(Constants.KotlinTypes.T));
        return codegen.classDecl(repositoryName + Constants.Suffixes.VIEW_MODEL_FACTORY, genericList, null, implementsTypeList);
    }

    /**
     * {@Code
     * {repo-name}ViewModelFactory }
     */

    private static ConstructorDefinition viewModelConstructorDefinition(CodeGen codegen, String repositoryName) {
        ParameterDeclaration applicationParam = codegen.paramDecl(codegen.classType(Constants.KotlinTypes.APPLICATION),
                                                                  codegen.var(Constants.KotlinVariables.APPLICATION));
        ParameterDeclaration repoParam = codegen.paramDecl(codegen.classType(repositoryName),
                                                           codegen.var(Constants.KotlinVariables.REPOSITORY));
        ConstructorDeclaration decl = codegen.constructorDecl(repositoryName + Constants.Suffixes.VIEW_MODEL_FACTORY,
                                                              codegen.paramList(applicationParam, repoParam));
        AssignmentStatement repoAssignment = codegen.assignStmt(codegen.dot(codegen.var(Constants.KotlinKeywords.THIS),
                                                                            codegen.var(Constants.KotlinVariables.REPOSITORY)),
                                                                codegen.var(Constants.KotlinVariables.REPOSITORY));
        AssignmentStatement applicationAssignment = codegen.assignStmt(codegen.dot(codegen.var(Constants.KotlinKeywords.THIS),
                                                                                   codegen.var(Constants.KotlinVariables.APPLICATION)),
                                                                       codegen.var(Constants.KotlinVariables.APPLICATION));

        StatementList statementList = codegen.stmtList(applicationAssignment, repoAssignment);
        return codegen.constructorDef(decl, statementList);
    }
}
