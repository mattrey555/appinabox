package com.apptechnik.appinabox.engine.codegen.android.util;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.android.AndroidUtil;
import com.apptechnik.appinabox.engine.exceptions.XmlElementNotFoundException;
import com.apptechnik.appinabox.engine.util.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * XML utilities for Android-specific XML files, such as navigation, layouts, and AndroidManifest.xml
 */
public class AndroidXML {
    public static Element findView(Element element, String viewName, String viewId) {
        Map<String, String> viewProperties = new HashMap<>();
        viewProperties.put(Constants.AndroidAttributes.ID, viewId);
        List<Element> elements = XMLUtils.findElementsByNameAndAttributes(element, viewName, viewProperties);
        return !elements.isEmpty() ? elements.get(0) : null;
    }

    /**
     * Given a parent element, find
     * @param parentElement
     * @param id
     * @return
     * @throws XmlElementNotFoundException
     */
    public static Element findViewByAddedId(Element parentElement, String id)
    throws XmlElementNotFoundException {
        String addedId = AndroidUtil.addId(id);
        Element viewElement = AndroidXML.findView(parentElement, Constants.NavigationXML.FRAGMENT, addedId);
        if (viewElement == null) {
            throw new XmlElementNotFoundException("failed to find " + Constants.NavigationXML.FRAGMENT +
                    " in " + Constants.Templates.NAV_GRAPH_XML);
        }
        return viewElement;
    }
    /**
     * Create a string array resource from a template xml file and a list of strings.
     * @param xmlFile output XML file
     * @param strings strings to add to stringarray
     * @throws Exception if there was an error constructing the XML or saving the file.
     */
    public static void createStringArray(File xmlFile, Collection<String> strings)throws Exception {
        Document xmlDoc = XMLUtils.fromResource(Constants.Templates.STRING_ARRAY_XML);
        Element resourceElement = xmlDoc.getDocumentElement();
        Node stringArrayNode = resourceElement.getElementsByTagName(Constants.AndroidResources.STRING_ARRAY).item(0);
        for (String s : strings) {
            XMLUtils.addSingleTextElement(xmlDoc, stringArrayNode, Constants.AndroidResources.ITEM, s);
        }
        XMLUtils.save(xmlFile, xmlDoc);
    }

}
