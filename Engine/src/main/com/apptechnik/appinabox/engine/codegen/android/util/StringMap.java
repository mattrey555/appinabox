package com.apptechnik.appinabox.engine.codegen.android.util;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.util.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Map of string resources for Android strings.xml. Persisted between calls so string resources
 * can be added between build processes.
 */
public class StringMap {
    private File xmlFile;
    private Map<String, StringItem> stringMap;

    private class StringItem {
        String value;
        Map<String, String> attributes;

        public StringItem(Element element) {
            attributes = new HashMap<String, String>();
            value = element.getChildNodes().item(0).getNodeValue();
            for (int i = 0; i < element.getAttributes().getLength(); i++) {
                attributes.put(element.getAttributes().item(i).getNodeName(),
                               element.getAttributes().item(i).getNodeValue());
            }
        }

        public StringItem(String key, String value) {
            attributes = new HashMap<String, String>();
            attributes.put(Constants.AndroidResources.NAME, key);
            this.value = value;
        }

        public Element toXML(Document xmlDoc, Node parentNode) {
            Element newElement = XMLUtils.addSingleTextElement(xmlDoc, parentNode, Constants.AndroidResources.STRING, value);
            attributes.keySet().stream().forEach(key -> newElement.setAttribute(key, attributes.get(key)));
            return newElement;
        }

        public String getValue() {
            return value;
        }

        public Map<String, String> getAttributes() {
            return attributes;
        }
    }

    public StringMap(File xmlFile) throws Exception {
        stringMap = new HashMap<String, StringItem>();
        this.xmlFile = xmlFile;

        if (xmlFile.exists()) {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            Document doc = builder.parse(new InputSource(new FileReader(xmlFile)));
            List<Element> items = XMLUtils.findElementsByNameAndAttributes(doc, Constants.AndroidResources.STRING, null);
            items.stream().forEach(item -> stringMap.put(item.getAttribute(Constants.AndroidResources.NAME),
                                                         new StringItem(item)));
        }
    }

    public void commit() throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.newDocument();
        Node resourceNode = XMLUtils.addNode(doc, doc, Constants.AndroidResources.RESOURCES);
        for (String key : stringMap.keySet()) {
            stringMap.get(key).toXML(doc, resourceNode);
        }
        XMLUtils.save(xmlFile, doc);
    }

    public void addEntry(String key, String value) {
        stringMap.put(key, new StringItem(key, value));
    }

    public String getEntry(String key) {
        return stringMap.get(key).value;
    }

    public String getAttribute(String key, String attribute) {
        return stringMap.get(key).attributes.get(attribute);
    }
}
