package com.apptechnik.appinabox.engine.codegen.gradle;

import java.io.*;
import java.text.ParseException;
import java.util.Properties;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.exceptions.PropertiesException;
import com.apptechnik.appinabox.engine.util.ExtraProperties;
import com.apptechnik.appinabox.engine.util.FileUtil;
import com.apptechnik.appinabox.engine.util.Substitute;

/**
 * Create the gradle file including the code generation for each servlet
 * The servlets are specified in SQL files. The name of the SQL file is the name of the outputted servlet.
 */
public class CreateBuildGradle {

	public static void main(String[] args)
	throws IOException, ParseException, PropertiesException {
		if (args.length < 4) {
			System.err.println("usage: output-directory sql-file-directory create-tables-file properties-file");
			System.exit(-1);
		}
		String outputDirName = args[0];
		String statementsFileDirName = args[1];
		String createTablesFileName = args[2];
		String propertiesFileName = args[3];

		// check to make sure the directories exist
		File sqlDir = FileUtil.checkDirectory("SQL statements directory", statementsFileDirName);
		File outputDir = FileUtil.checkDirectory("output directory", outputDirName);
		File serverOutputDir = FileUtil.checkDirectory("server output directory",
														new File(outputDirName, Constants.Directories.SERVER));
		File serverGradleFile = new File(serverOutputDir, Constants.Files.BUILD_GRADLE);
		File propertiesFile = new File(outputDir, propertiesFileName);

		System.out.println("output directory name = " + outputDirName + "\n" +
						   "statements file directory = " + statementsFileDirName + "\n" +
				           "Create Tables File = " + createTablesFileName + "\n" +
						   "properties file name = " + propertiesFile);

		// start with the properties files, and add the substitutions
		String archiveName = outputDir.getName();
		ExtraProperties propertyMap = new ExtraProperties(propertiesFile);
		propertyMap.setProperty(Constants.Substitutions.CREATE_TABLES, createTablesFileName);
		propertyMap.setProperty(Constants.Substitutions.STATEMENT_FILES_DIR, statementsFileDirName);
		propertyMap.setProperty(Constants.Substitutions.PROPERTIES_FILE, propertiesFileName);
		propertyMap.setProperty(Constants.Substitutions.OUTPUT_DIR, outputDirName);
		propertyMap.setProperty(Constants.Substitutions.ARCHIVE_NAME, archiveName);

		Substitute.transformResource(Constants.Templates.SERVER_BUILD_GRADLE, serverGradleFile, propertyMap, false);

		// write or modify the settings.gradle file in the parent directory to include this project.
		File settingsFile = new File(outputDir, Constants.Files.SETTINGS_GRADLE);
		CreateSettingsGradle.create(settingsFile, Constants.Directories.SERVER);

		// if the top-level gradle file doesn't exist, then create it.
		File projectGradleFile = new File(outputDir, Constants.Files.BUILD_GRADLE);
		String gradleText = FileUtil.getResource(CreateBuildGradle.class, Constants.Templates.PROJECT_BUILD_GRADLE);
		if (!projectGradleFile.exists()) {
			FileUtil.writeString(projectGradleFile, gradleText);
		}
	}
}
