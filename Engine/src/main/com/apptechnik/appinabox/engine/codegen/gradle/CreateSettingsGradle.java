package com.apptechnik.appinabox.engine.codegen.gradle;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.util.FileUtil;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Create or modify settings.gradle to include the new project name.
 * If it exists, scan for lines containing "include"
 */
public class CreateSettingsGradle {
    public static void create(File file, String projectName) throws IOException {
        if (file.exists()) {
            addProjectToGradleSettings(file, projectName);
        } else {
            createGradleSettings(file, projectName);
        }
    }

    /**
     * The settings file is pretty simple, with a :include {project-name}
     * @param file File to write the settings to
     * @param projectName project-name
     * @throws IOException if the file can't be written for some reason.
     */
    private static void createGradleSettings(File file, String projectName) throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        bw.write(Constants.Gradle.INCLUDE + " ':" +  projectName + "'\n");
        bw.close();
    }

    /**
     *
     * @param file
     * @param projectName
     * @throws IOException
     */
    private static void addProjectToGradleSettings(File file, String projectName) throws IOException {
        List<String> lines = FileUtil.readStringList(file);

        // search for the include ":project-name" directives in the settings file.
        // or it could be include ":other-project", ":project-name" because gradle
        // loves alternate syntaxes.
        List<String> includes = lines.stream()
                .filter(line -> line.startsWith(Constants.Gradle.INCLUDE))
                .toList();
        boolean alreadyIncluded = false;
        String includeExpr = "include \":" + projectName + "\"";
        for (String inc : includes) {
            List<String> matches = StringUtil.matchingRegexes(inc, includeExpr);
            for (String match : matches) {
                String id = StringUtil.stripQuotes(match);
                if (id.equals(projectName) || id.equals(":" + projectName)) {
                    alreadyIncluded = true;
                }
            }
        }
        if (!alreadyIncluded) {
            lines.add(Constants.Gradle.INCLUDE + " ':" + projectName + "'");
        }

        // TODO: if there's any lines in the file that don't start with :include
        // they're going to get deleted.
        FileUtil.writeStringList(file, lines);
    }
}
