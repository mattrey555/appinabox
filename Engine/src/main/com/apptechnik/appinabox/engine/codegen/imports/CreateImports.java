package com.apptechnik.appinabox.engine.codegen.imports;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeGen;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeStatement;
import com.apptechnik.appinabox.engine.codegen.pojos.ImportStatement;
import com.apptechnik.appinabox.engine.codegen.pojos.StatementList;
import com.apptechnik.appinabox.engine.parser.pojos.FieldReferenceNode;
import com.apptechnik.appinabox.engine.util.OutputPackages;

import java.util.ArrayList;
import java.util.List;

public class CreateImports {
    private final String packageName;

    public CreateImports(String packageName) {
        this.packageName = packageName;
    }

    /**
     * Create the imports for INSERT/UPDATE statements
     * {@code
     * import <update-package-name>.*
     * import <update-package-name>.sql.*
     * }
     * @param hasPojos in the case where the object is a list of primitives, we don't insert any POJOs.
     *                 NOTE: I hate passing control like this.
     * @return ImportStatementList
     */
    public StatementList updateImports(CodeGen codegen, boolean hasPojos) {
        ImportStatement updateSQLPackageImport =
                codegen.importStmt(OutputPackages.getUpdateSQLPackageName(packageName) +
                                    "." + Constants.Packages.WILDCARD);
        if (hasPojos) {
            ImportStatement updatePojoPackageImport =
                    codegen.importStmt(OutputPackages.getUpdatePackageName(packageName) +
                                       "." + Constants.Packages.WILDCARD);
            return codegen.stmtList(updatePojoPackageImport, updateSQLPackageImport);
        } else {
            return codegen.stmtList(updateSQLPackageImport);
        }
    }

    public StatementList updateImports(CodeGen codegen,
                                       boolean hasPojos,
                                       boolean hasUpdates) {
        List<ImportStatement> list = new ArrayList<ImportStatement>();
        if (hasUpdates) {
            list.add(codegen.importStmt(OutputPackages.getUpdateSQLPackageName(packageName) + "." +
                    Constants.Packages.WILDCARD));
        }
        if (hasPojos) {
            list.add(codegen.importStmt(OutputPackages.getUpdatePackageName(packageName) + "." +
                    Constants.Packages.WILDCARD));
        }
        return codegen.stmtList(list);
    }
    /**
     * Imports for the JSON request objects, and associated SQL statements.
     * {@code
     * import ${IMPORT_QUERY_PACKAGE_NAME}.*;
     * import ${IMPORT_QUERY_PACKAGE_NAME}.${SQL_PACKAGE_NAME}.*;
     * }
     * @param codegen code generator
     * @param hasPojos add the import for the POJOs generated for the query.
     * @return list of import statements.
     */
    public StatementList queryImports(CodeGen codegen, boolean hasPojos) {
        ImportStatement querySQLPackageImport =
                codegen.importStmt(OutputPackages.getQuerySQLPackageName(packageName) + "." +
                                    Constants.Packages.WILDCARD);
        if (hasPojos) {
            ImportStatement queryPojoPackageImport =
                codegen.importStmt(OutputPackages.getQueryPackageName(packageName) + "." +
                                    Constants.Packages.WILDCARD);
            return codegen.stmtList(queryPojoPackageImport, querySQLPackageImport);
        } else {
            return codegen.stmtList(querySQLPackageImport);
        }
    }
}
