package com.apptechnik.appinabox.engine.codegen.main;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.imports.CreateImports;
import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.codegen.query.CreateQueryObjects;
import com.apptechnik.appinabox.engine.codegen.servlet.CreateServlet;
import com.apptechnik.appinabox.engine.codegen.update.UpdateSQLObjects;
import com.apptechnik.appinabox.engine.exceptions.NameNotFoundException;
import com.apptechnik.appinabox.engine.exceptions.PropertiesException;
import com.apptechnik.appinabox.engine.exceptions.TypeMismatchException;
import com.apptechnik.appinabox.engine.exceptions.TypeNotFoundException;
import com.apptechnik.appinabox.engine.parser.pojos.*;
import com.apptechnik.appinabox.engine.util.*;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.select.Select;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

public class CreateMain {
    private final CreateObjects createObjects;
    private final CreateQueryObjects createQueryObjects;

    public CreateMain(CreateObjects createObjects, CreateQueryObjects createQueryObjects) {
        this.createObjects = createObjects;
        this.createQueryObjects = createQueryObjects;
    }

    /**
     * Create the program which executes SQL statements with parameters from the command-line,
     * and returns the JSON from ONE select statement to standard output.
     * @param queryParamMap map of JDBC Named Paramters to type data for command-line parameters.
     * @param selectList List of SELECT statements (must be single element)
     * @param templateFile resoure template file
     * @param importsFile list of standard imports
     * @param nodeNames JSON node name for results from SELECT
     * @throws Exception
     */
    public void createSingleReturnMainClass(CodeGen codegen,
                                            ParameterMap queryParamMap,
                                             List<Select> selectList,
                                             List<Statement> updateList,
                                             List<String> nodeNames,
                                             String templateFile,
                                             String importsFile) throws Exception {
        StatementList importStatementList = codegen.importStatementList(importsFile);
        // set the default properties, then override with values read from the properties file.
        // 2 command-line arguments: properties and input json file.
        ExtraProperties properties = new ExtraProperties(createObjects.getPropertiesFile());
        setDefaultSubstitutions(codegen, properties, importStatementList.toString(), createObjects.getVarName(),
                                selectList, updateList);
        if (createObjects.getJsonResponseTree() != null) {
            properties.put(Constants.Substitutions.MAIN_WRITE_OUTPUT,
                           FileUtil.getResource(CreateServlet.class, Constants.Fragments.MAIN_WRITE_OUTPUT));
        }

        // we may have a request JSON without UPDATE/INSERT/DELETE statements if it's used as
        // a SELECT IN clause parameter. Need the update imports to access the request object.
        int argStart = 1;
        argStart = requestSubstitutions(codegen, properties, queryParamMap, updateList, argStart);

        // only add SELECT and format into JSON if there are any SELECT statements.
        if (!selectList.isEmpty()) {
            ParameterMap selectParameterMap = new ParameterMap(createObjects.getStatementParameterMapMap(), selectList);
            MethodCallStatement selectListAssignment =
                    CreateQueryObjects.selectListAssignment(codegen,
                                                            Constants.Variables.DB_CONNECTION_TEST,
                                                            createQueryObjects.getResponseName(),
                                                            selectParameterMap,
                                                            createObjects.getJsonParameterMap(),
                                                            createObjects.isSingleFieldRequest());
            properties.put(Constants.Substitutions.SELECT_LIST, selectListAssignment + ";\n");
            CreateImports createImports = new CreateImports(createObjects.getPackageName());


            boolean hasPojos = !createObjects.isSingleFieldResponse();
            StatementList queryImports = createObjects.createQueryImports(codegen);
            properties.put(Constants.Substitutions.QUERY_IMPORTS, queryImports.toString());
        }
        File javaMainFile = createObjects.javaFile(createObjects.getQuerySQLPackageName(),
                                           createObjects.getVarName() + Constants.Extensions.MAIN);
        Substitute.transformResource(templateFile, javaMainFile, properties);
    }

    /**
     * When we aggregate a set of select calls, we want to return an object which contains a list of
     * their results.
     * @param jsonResponseMapTree
     * @param selectList List of SELECT statements (must be single element)
     * @param queryParamMap map of JDBC Named Paramters to type data for command-line parameters.
     * @param nodeNames JSON node name for results from SELECT
     * @param templateFile resoure template file
     * @param importsFile list of standard imports
     * @throws Exception
     */
    public void createMultipleReturnMainClass(CodeGen codegen,
                                              Tree<QueryFieldsNode> jsonResponseMapTree,
                                               List<Select> selectList,
                                               List<Statement> updateList,
                                               ParameterMap queryParamMap,
                                               List<String> nodeNames,
                                               String templateFile,
                                               String importsFile) throws Exception {
        StatementList importStatementList = codegen.importStatementList(importsFile);

        // set the default properties, then override with values read from the properties file.
        // 2 command-line arguments: properties and input json file.
        ExtraProperties properties = new ExtraProperties();
        setDefaultSubstitutions(codegen, properties, importStatementList.toString(), createObjects.getVarName(), selectList, updateList);
        // we may have a request JSON without UPDATE/INSERT/DELETE statements if it's used as
        // a SELECT IN clause parameter. Need the update imports to access the request object.
        int argStart = 1;
        argStart = requestSubstitutions(codegen, properties, queryParamMap, updateList, argStart);
        StatementList argParamAssignments = queryParamMap.parametersFromArguments(codegen, argStart);

        // List<insert_film> input
        if (createObjects.getJsonParameterMap() != null) {
            StatementList requestJsonAssignments =
                createObjects.getJsonParameterMap().parametersFromRequestJson(codegen, createObjects.getRequestName(), Constants.Variables.INPUT);
            argParamAssignments.getStatementList().addAll(requestJsonAssignments.getStatementList());
        }
        properties.put(Constants.Substitutions.CONVERT_ARGS, argParamAssignments.toString());

        if (!selectList.isEmpty()) {
            ParameterMap selectParameterMap = new ParameterMap(createObjects.getStatementParameterMapMap(), selectList);
            StatementList selectListCalls =
                    CreateQueryObjects.selectListFieldAssignments(codegen, jsonResponseMapTree, selectList,
                                                                  Constants.Variables.DB_CONNECTION_TEST,
                                                                  Constants.Variables.RESULT, selectParameterMap,
                                                                  createObjects.getJsonParameterMap(), createObjects.isSingleFieldRequest());
            properties.put(Constants.Substitutions.SELECT_LISTS_INTO_FIELDS, selectListCalls + ";\n");
            StatementList queryImports = createObjects.createQueryImports(codegen);
            properties.put(Constants.Substitutions.QUERY_IMPORTS, queryImports.toString());
        }
        File javaMainFile = createObjects.javaFile(createObjects.getQuerySQLPackageName(), createObjects.getVarName() + Constants.Extensions.MAIN);
        Substitute.transformResource(templateFile, javaMainFile, properties);
    }

    /**
     * TODO: verify that we're actually supposed to pass queryParamMap here.
     * @param properties
     * @param queryParamMap
     * @param argStart
     * @return
     * @throws IOException
     * @throws PropertiesException
     */
    private int requestSubstitutions(CodeGen codegen,
                                     Properties properties,
                                     ParameterMap queryParamMap,
                                     List<Statement> updateList,
                                     int argStart) {
        // we may have a request JSON without UPDATE/INSERT/DELETE statements if it's used as
        // a SELECT IN clause parameter. Need the update imports to access the request object.
        if (createObjects.getJsonRequestTree() != null) {
            // args[1] is file containing JSON to insert.
            argStart++;
        }
        Type requestResponseType = codegen.classType(Constants.JavaTypes.REQUEST_RESPONSE);
        Variable requestResponseVar = codegen.var(Constants.Variables.REQUEST_RESPONSE);
        TypedAssignmentStatement newRequestResponse =
            codegen.typedAssignStmt(requestResponseType, requestResponseVar,
                                    codegen.alloc(codegen.classType(Constants.JavaTypes.REQUEST_RESPONSE),
                                                  codegen.var(Constants.Variables.ARGS)));
        properties.put(Constants.Substitutions.REQUEST_RESPONSE_ALLOCATION, newRequestResponse.toString() + ";\n");

        MethodCall assignRequestResponse =
            codegen.methodCall(requestResponseVar, Constants.Functions.SET_OUTPUT, codegen.var(Constants.Variables.RESULT));
        properties.put(Constants.Substitutions.REQUEST_RESPONSE_ASSIGNMENT, assignRequestResponse + ";\n");

        // only import update objects if there are update statements.
        if (!updateList.isEmpty()) {
            StatementList updateImports = createObjects.createUpdateImports(codegen);
            properties.put(Constants.Substitutions.UPDATE_IMPORTS, updateImports.toString());
        }
        if (!StatementUtil.filterUpdateDeleteInsert(createObjects.getSQLStatementList()).isEmpty()) {
            if (createObjects.getJsonRequestTree() != null) {
                MethodCall updateCall = UpdateSQLObjects.insertUpdateListCall(codegen, Constants.Variables.DB_CONNECTION_TEST,
                                                                              createObjects.getRequestName(),
                                                                              Constants.Variables.INPUT, queryParamMap);
                properties.put(Constants.Substitutions.INSERT_UPDATE_LIST, updateCall+ ";\n");
            } else {
                MethodCall updateCall = UpdateSQLObjects.insertUpdateCall(codegen, Constants.Variables.DB_CONNECTION_TEST,
                                                                          createObjects.getRequestName(), queryParamMap);
                properties.put(Constants.Substitutions.INSERT_UPDATE, updateCall+ ";\n");

            }
        }
        return argStart;
    }


    /**
     * Set the default substitutions for each template
     * @param properties
     * @param imports
     * @param className
     */
    public void setDefaultSubstitutions(CodeGen codegen,
                                        Properties properties,
                                        String imports,
                                        String className,
                                        List<Select> selectList,
                                        List<Statement> updateList)
    throws TypeMismatchException, TypeNotFoundException, NameNotFoundException {
        properties.put(Constants.Substitutions.PACKAGE, createObjects.getPackageName());
        properties.put(Constants.Substitutions.IMPORT_QUERY_PACKAGE_NAME, OutputPackages.getQueryPackageName(createObjects.getPackageName()));
        properties.put(Constants.Substitutions.IMPORT_UPDATE_PACKAGE_NAME, OutputPackages.getUpdatePackageName(createObjects.getPackageName()));
        properties.put(Constants.Substitutions.QUERY, Constants.Packages.QUERY);
        properties.put(Constants.Substitutions.UPDATE, Constants.Packages.UPDATE);
        properties.put(Constants.Substitutions.SQL_FNS, Constants.Extensions.SQLFNS);
        properties.put(Constants.Substitutions.SQL_PACKAGE_NAME, Constants.Packages.SQL);
        properties.put(Constants.Substitutions.IMPORTS, imports);
        properties.put(Constants.Substitutions.DATE_FORMAT, Constants.PropertyDefaults.DATE_FORMAT);
        properties.put(Constants.Substitutions.HTTP_METHOD, createObjects.getServletHttpMethod());
        if (className != null) {
            properties.put(Constants.Substitutions.CLASSNAME, className);
        }

        // if there is an update list, see if the root node of the JSON tree maps to any JDBC Parameter.
        // if there is a select list, see if the root node of the JSON tree maps to a JDBC Parameter in a SELECT in clause.
        if (createObjects.getJsonRequestTree() != null) {
            Type rootType = createObjects.getRequestType(codegen, selectList, updateList);
            if (rootType != null) {
                properties.put(Constants.Substitutions.CLASSNAME_IN, rootType.toString());
            } else {
                throw new TypeNotFoundException("unable to resolve root type for " +
                                                FieldReferenceNode.createString(createObjects.getJsonRequestTree()));
            }
        }
        if (createQueryObjects != null) {
            Type rootType = QueryFieldsNode.getNodeType(codegen, createObjects.getJsonResponseTree());
            properties.put(Constants.Substitutions.CLASSNAME_OUT, rootType.toString());
        }
    }
}
