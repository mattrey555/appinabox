package com.apptechnik.appinabox.engine.codegen.main;

import com.apptechnik.appinabox.engine.codegen.params.CreateRequestResponse;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.codegen.util.SQLFile;
import com.apptechnik.appinabox.engine.exceptions.*;
import com.apptechnik.appinabox.engine.codegen.update.UpdateSQLObjects;
import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;
import com.apptechnik.appinabox.engine.codegen.query.CreateQueryObjects;
import com.apptechnik.appinabox.engine.codegen.servlet.CreateServlet;
import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.log.Log;
import com.apptechnik.appinabox.engine.parser.TableInfoMap;
import com.apptechnik.appinabox.engine.parser.create.StatementParser;
import com.apptechnik.appinabox.engine.parser.pojos.*;
import com.apptechnik.appinabox.engine.util.*;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.select.Select;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Create complex query/update programs, which take JSON input for INSERT/UPDATE/DELETE, and return the
 * results from single or multiple SELECT calls.
 */
public class CreateSQLObjects {
    private CreateObjects createObjects;
    private CreateQueryObjects createQueryObjects;

    /**
     * Usage:
     * create-table-file: contains CREATE TABLE statements for schema
     * properties-file: properties files used for substitutions in templates
     * output-directory: directory to generate output
     * sql-file: SQL file or directory
     * sql-directory: SQL directory if file is specified (to get the sync tables)
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        if (args.length < 4) {
            System.err.println("usage: create-table-file properties-file output-directory sql-file-or-directory <sql-directory>");
            System.exit(-1);
        }

        String createTableFileName = args[0];
        String propertiesFileName = args[1];
        String outputDirName = args[2];
        String mainDirName = outputDirName + "/" + Constants.Directories.SERVER_GENERATED_MAIN;
        String testDirName = outputDirName + "/" + Constants.Directories.SERVER_GENERATED_TEST;
        String sqlFileNameOrDir = args[3];
        Log.debug("create table file = " + createTableFileName + "\n" +
                  "SQL file = " + sqlFileNameOrDir + "\n" +
                  "properties file = " + propertiesFileName + "\n" +
                  "server directory  = " + mainDirName + "\n" +
                  "server test directory  = " + testDirName);
        File serverDir = FileUtil.checkDirectory("generated server main code", mainDirName);
        File testOutputDir = FileUtil.checkDirectory("generated server test code", testDirName);
        File sqlFileOrDir = FileUtil.checkFile("SQL Statement file/directory", sqlFileNameOrDir);
        File createTableFile = FileUtil.checkFile("SQL create table", createTableFileName);
        File propertiesFile = FileUtil.checkFile("properties file", propertiesFileName);
        StatementParser statementParser = CreateObjects.parseSchema(createTableFile);

        // if the last argument is a single file, then process it, if it's a directory, recursively process
        // all of the contained .sql files
        Set<String> syncTables = new HashSet<String>();
        if (sqlFileOrDir.isFile()) {
            if (args.length < 5) {
                System.err.println("When processing a single file, you must specify the directory for sync directives to be processed");
                System.exit(-1);
            }
            File sqlDir = FileUtil.checkDirectory("SQL directory", args[4]);
            checkSyncTables(sqlDir, statementParser);
            CreateObjects createObjects = new CreateObjects(sqlFileOrDir, propertiesFile, createTableFile,
                                                            statementParser, serverDir);
            createObjects.initializeDirectories();
            CreateSQLObjects createSQLObjects = new CreateSQLObjects(createObjects);
            createSQLObjects.createObjects();
        } else if (sqlFileOrDir.isDirectory()) {
            ProcessFile processFile = new ProcessFile(statementParser, serverDir, createTableFile, propertiesFile);
            CreateObjects.walkSQLDir(sqlFileOrDir, processFile);
            checkSyncTables(sqlFileOrDir, statementParser);

        }
    }

    /**
     * Check the CREATE TABLE statements to ensure that they follow sync conventions.
     * @param sqlDir directory containing SQL files.
     * @param statementParser
     * @throws IOException
     */
    private static void checkSyncTables(File sqlDir, StatementParser statementParser) throws IOException {
        Set<String> syncTables = new HashSet<String>();
        GetSyncTables getSyncTables = new GetSyncTables(syncTables);
        Files.walk(Paths.get(sqlDir.toURI()))
                .filter(Files::isRegularFile)
                .filter(p -> p.getFileName().toString().endsWith(Constants.Extensions.SQL))
                .forEach(CheckedConsumer.wrap(getSyncTables));
        List<String> syncErrorMessages = statementParser.getTableInfoMap().checkSyncedTables(syncTables);

        CheckSyncTables checkConsumer =
            new CheckSyncTables(syncTables, statementParser.getTableInfoMap(), syncErrorMessages);
        Files.walk(Paths.get(sqlDir.toURI()))
                .filter(Files::isRegularFile)
                .filter(p -> p.getFileName().toString().endsWith(Constants.Extensions.SQL))
                .forEach(CheckedConsumer.wrap(checkConsumer));
        if (!syncErrorMessages.isEmpty()) {
            syncErrorMessages.stream().forEach(err -> System.out.println(err));
            System.exit(-1);
        }

    }
    /**
     * We have to get the sync tables in advance, because the sync directive can be specified in the query
     * file, but the table can be referenced in any other file, and we check the SQL statement to validate
     * that tables flagged for sync follow these conventions:
     * CREATE TABLE: must contain a DELETED boolean flag
     * SELECT: must use AND DELETED=FALSE in WHERE clause
     * UPDATE: must use AND DELETED=FALSE in WHERE clause
     * DELETE: must not use DELETE, must use UPDATE {@code <table>} SET DELETE=TRUE
     */
    private static class GetSyncTables implements CheckedConsumer<Path> {
        private final Set<String> syncTables;

        public GetSyncTables(Set<String> syncTables) {
            this.syncTables = syncTables;
        }

        @Override
        public void accept(Path path) throws Exception {
            SQLFile sqlFile = new SQLFile(path.toFile());
            String sync = sqlFile.findTag(Constants.Directives.SYNC);
            if (sync != null) {
                syncTables.addAll(StringUtil.splitAndTrimList(sync, "[, \t]"));
            }
        }
    }

    private static class CheckSyncTables implements CheckedConsumer<Path> {
        private final Set<String> syncTables;
        private final TableInfoMap createTableInfoMap;
        private List<String> errorMessages;

        public CheckSyncTables(Set<String> syncTables,
                               TableInfoMap createTableInfoMap,
                               List<String> errorMessages) {
            this.syncTables = syncTables;
            this.createTableInfoMap = createTableInfoMap;
            this.errorMessages = errorMessages;
        }

        @Override
        public void accept(Path path) throws Exception {
            SQLFile sqlFile = new SQLFile(path.toFile());
            List<String> fileErrors = sqlFile.validateSyncConventions(sqlFile.getFile().getAbsolutePath(),
                                                                      syncTables, createTableInfoMap);
            errorMessages.addAll(fileErrors);
        }
    }

    /**
     * File consumer to process .SQL files.
     */
    private static class ProcessFile implements CheckedConsumer<File> {
        private final StatementParser statementParser;              // PERFORMANCE: tableInfo parsed from schema.
        private final File outputDir;                               // .java output directory
        private final File createTableFile;                         // schema file
        private final File propertiesFile;                          // .properties file for template substitution
        private Map<String, SQLFile> processMap;                    // map of package names, to SQLFiles to validate uniqueness.

        public ProcessFile(StatementParser statementParser,
                           File outputDir,
                           File createTableFile,
                           File propertiesFile) {
            this.statementParser = statementParser;
            this.outputDir = outputDir;
            this.createTableFile = createTableFile;
            this.propertiesFile = propertiesFile;
            this.processMap = new HashMap<String, SQLFile>();
        }

        @Override
        public void accept(File sqlFile) throws Exception {
            CreateObjects createObjects = new CreateObjects(sqlFile, propertiesFile, createTableFile,
                                                            statementParser, outputDir);
            createObjects.initializeDirectories();
            String packageName = createObjects.getPackageName();
            if (processMap.containsKey(packageName)) {
                throw new DuplicatePackageException("Package name ", packageName,
                                                    createObjects.getSqlFile(), processMap.get(packageName));
            }
            processMap.put(createObjects.getPackageName(), createObjects.getSqlFile());
            CreateSQLObjects createSQLObjects = new CreateSQLObjects(createObjects);
            createSQLObjects.createObjects();
        }
    }

    public CreateSQLObjects(CreateObjects createObjects) throws ArgumentException {
        this.createObjects = createObjects;
    }

    public void createObjects() throws Exception {
        List<Select> selectStatements = createObjects.getSelectStatementList();
        List<Statement> updateStatements = createObjects.getUpdateStatementList();
        if (!selectStatements.isEmpty()) {
            createQueryObjects = new CreateQueryObjects(createObjects, selectStatements);
            createQueryObjects.createObjectFiles();
        }

        // we may have to create the request even if there are no INSERT/UPDATE statements, because
        // the SELECT call may extract its parameters from the request payload.
        if ((createObjects.getJsonRequestTree() == null) && !StatementUtil.filterUpdateInsert(updateStatements).isEmpty()) {
            throw new PropertiesException("in " + createObjects.getSqlFile().getFile().getAbsolutePath() +
                                          " has INSERT/UPDATE statements, and does not specify a request JSON map");
        }
        if (!updateStatements.isEmpty()) {
            UpdateSQLObjects updateSQLObjects = new UpdateSQLObjects(createObjects);
            updateSQLObjects.executeOnChanges(createObjects.getJavaCodeGen());
        }
        createTopLevelClasses(createObjects.getJavaCodeGen(), selectStatements, updateStatements);
    }

    /**
     * Create the command line program which executes the query from command-line arguments
     * and writes the resulting JSON to standard output.
     * @param selectList
     * @throws Exception
     */
    public void createTopLevelClasses(CodeGen codegen,
                                      List<Select> selectList,
                                      List<Statement> updateList) throws Exception {

        // get the package names generated from the JSON nodes in the response (IF THERE IS ONE)
        List<String> nodeNames = (createQueryObjects != null) ?
            FieldReferenceNode.getNodeNames(createObjects.getJsonResponseTree()) : new ArrayList<String>();
        CreateRequestResponse createRequestResponse = new CreateRequestResponse(createObjects);
        createRequestResponse.toClassFile().write(createObjects.getOutputDir());
        CreateServlet createServlet = new CreateServlet(this, nodeNames);
        CreateMain createMain = new CreateMain(createObjects, createQueryObjects);
        ParameterMap selectParameterMap = new ParameterMap(createObjects.getStatementParameterMapMap(), selectList);
        if (selectList.size() <= 1) {
            // single select also handles the case where there is no return JSON.
            createMain.createSingleReturnMainClass(codegen, createObjects.getQueryParams(),
                                                   selectList, updateList, nodeNames,
                                                   Constants.Templates.SQL_MAIN_SINGLE_RESPONSE,
                                                   Constants.Templates.MAIN_IMPORTS);
            createServlet.createSingleReturnServlet(codegen, selectList, updateList, selectParameterMap);
        } else {
            createMain.createMultipleReturnMainClass(codegen, createObjects.getJsonResponseTree(),
                                                     selectList, updateList, createObjects.getQueryParams(), nodeNames,
                                                     Constants.Templates.SQL_MAIN_MULTIPLE_RESPONSE,
                                                     Constants.Templates.MAIN_IMPORTS);
            CreateQueryObjects.createMultipleSelectObject(codegen, createObjects, Constants.Templates.SQLFNS_IMPORTS,
                                                          createObjects.getJsonResponseTree()).write(createObjects.getOutputDir());
            createServlet.createMultipleReturnServlet(codegen, createObjects.getJsonResponseTree(),
                                                      selectList, updateList, selectParameterMap);
        }
    }

    /**
     * Set the default substitutions for each template
     * @param properties
     * @param imports
     * @param className
     */
    public void setDefaultSubstitutions(CodeGen codegen,
                                        Properties properties,
                                        String imports,
                                        String className,
                                        List<Select> selectList,
                                        List<Statement> updateList)
    throws TypeMismatchException, TypeNotFoundException, NameNotFoundException {
        properties.put(Constants.Substitutions.PACKAGE, createObjects.getPackageName());
        properties.put(Constants.Substitutions.IMPORT_QUERY_PACKAGE_NAME, OutputPackages.getQueryPackageName(createObjects.getPackageName()));
        properties.put(Constants.Substitutions.IMPORT_UPDATE_PACKAGE_NAME, OutputPackages.getUpdatePackageName(createObjects.getPackageName()));
        properties.put(Constants.Substitutions.QUERY, Constants.Packages.QUERY);
        properties.put(Constants.Substitutions.UPDATE, Constants.Packages.UPDATE);
        properties.put(Constants.Substitutions.SQL_FNS, Constants.Extensions.SQLFNS);
        properties.put(Constants.Substitutions.SQL_PACKAGE_NAME, Constants.Packages.SQL);
        if (className != null) {
            properties.put(Constants.Substitutions.CLASSNAME, className);
        }

        // if there is an update list, see if the root node of the JSON tree maps to any JDBC Parameter.
        // if there is a select list, see if the root node of the JSON tree maps to a JDBC Parameter in a SELECT in clause.
        if (createObjects.getJsonRequestTree() != null) {
            Type rootType = createObjects.getRequestType(codegen, selectList, updateList);
            if (rootType != null) {
                properties.put(Constants.Substitutions.CLASSNAME_IN, rootType.toString());
            } else {
                throw new TypeNotFoundException("unable to resolve root type for " +
                                                FieldReferenceNode.createString(createObjects.getJsonRequestTree()));
            }
        }
        if (createQueryObjects != null) {
            Type rootType = QueryFieldsNode.getNodeType(codegen, createObjects.getJsonResponseTree());
            properties.put(Constants.Substitutions.CLASSNAME_OUT, rootType.toString());
        }
        properties.put(Constants.Substitutions.IMPORTS, imports);
        properties.put(Constants.Substitutions.DATE_FORMAT, Constants.PropertyDefaults.DATE_FORMAT);
        properties.put(Constants.Substitutions.HTTP_METHOD, createObjects.getServletHttpMethod());
    }


    public CreateObjects getCreateObjects() {
        return createObjects;
    }
}
