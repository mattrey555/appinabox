package com.apptechnik.appinabox.engine.codegen.mixin;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.codegen.pojos.kotlin.KotlinClassFile;
import com.apptechnik.appinabox.engine.codegen.util.CodegenUtil;
import com.apptechnik.appinabox.engine.exceptions.UnsupportedExpressionException;
import com.apptechnik.appinabox.engine.util.CheckedFunction;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.*;
import java.util.stream.Collectors;

// A VERY IMPORTANT RULE TO FOLLOW WITH THIS CLASS:
// DO NOT MODIFY THE CLASSES WHICH ARE PASSED TO IT, SINCE THEY'RE USED FOR GENERATING OUTPUT.
// TODO: why do we generate a mixin class.

/**
 * Create a Mixin Type from a ConstructorDefinition, so that classes can inherit from the Mixin class
 * and inherit from its sub objects.
 * so a class like this:
 * {@code
 * class A {
 *     int id;
 *     float sum;
 *     List<item> items;
 *     Customer customer;
 * }}
 * maps to MixinType:
 * class AMixin<I extends item, C extends Customer> {
 *     int id;
 *     float sum;
 *     List<I> items;
 *     C customer;
 * }
 *
 * This allows classes to inherit from the mixin type, and inherit contained classes as well.  The return types and
 * arguments of the generated methods are extended as well.
 */
public class CreateMixin {
    private final ClassFile sourceClassFile;
    private final String newClassName;

    public CreateMixin(ClassFile sourceClassFile, String newClassName) {
        this.sourceClassFile = sourceClassFile;
        this.newClassName = newClassName;
    }

    /**
     * Given a class, if it has complex members (i.e. non-atomic), then create a "Mixin" class which uses
     * generics, and remap the types of the class to implement the generics.
     * @param codegen code generator
     * @param classFile class file to remap.
     * @return
     * @throws Exception
     */
    public static ClassFile createAndRemapToMixinClass(CodeGen codegen, ClassFile classFile) {
        ClassDefinition classDef = classFile.getClassDefinition();
        String mixinName = classDef.getClassDeclaration().getClassName() + Constants.Substitutions.MIXIN;
        CreateMixin createMixin = new CreateMixin(classFile, mixinName);
        ClassDefinition mixinDef = createMixin.createFrom(codegen, classDef);
        return codegen.classFile(classFile.getPackageStatement(), classFile.getImportStatements(),
                CreateMixin.remapClass(codegen, classDef, mixinDef));
    }

    /**
     * Create a class file from the original class that will inherit from this Mixin class.
     * Use the same package and imports, but a remapped class definition.
     * @param codegen code generator
     * @return ClassFile for Mixin class.
     * @throws Exception
     */
    public ClassFile toClassFile(CodeGen codegen) throws Exception {
        return codegen.classFile(sourceClassFile.getPackageStatement(), sourceClassFile.getImportStatements(),
                                 createFrom(codegen, sourceClassFile.getClassDefinition()));
    }

    /**
     * Given an existing class declaration with concrete types, create a new class which implements generics
     * for each complex type in the class, so the complex objects (or lists of complex objects) can be extended.
     * @param classDef source class definition.
     * @return ClassDefinition for mixin class.
     * @throws UnsupportedExpressionException
     */
    public ClassDefinition createFrom(CodeGen codegen, ClassDefinition classDef) {
        Map<Type, String> typeMap = new HashMap<Type, String>();
        List<Type> extendedTypeVars = extendedTypes(codegen, classDef.getMemberDeclarationList(), typeMap);
        ClassDeclaration mixinDecl =
            codegen.classDecl(false, false, false, newClassName, codegen.typeList(extendedTypeVars), null, null, null);
        List<MemberDeclaration> mixinMembers = extendMemberDeclarations(codegen, typeMap, classDef.getMemberDeclarationList());
        List<MemberDeclaration> scalarMembers = classDef.getMemberDeclarationList().stream()
                .filter(memberDeclaration -> memberDeclaration.getType() instanceof SimpleType).toList();
        ConstructorDefinition constructor = codegen.defaultConstructor(newClassName);
        List<FunctionDefinition> methods = CodegenUtil.accessorsAndSetters(codegen, mixinMembers);

        return codegen.classDef(mixinDecl, mixinMembers, List.of(constructor), methods);
    }


    /**
     * Given the list of members:
     * filter out simple types, and extend the types. A single object can actually map to multiple generics.
     * @param memberList
     * @param typeMap
     * @return
     */
    public static List<Type> extendedTypes(CodeGen codegen,
                                           List<MemberDeclaration> memberList,
                                           Map<Type, String> typeMap) {
        return memberList.stream()
                        .filter(m -> !codegen.isSimpleType(m.getType()))
                        .map(CheckedFunction.wrap(m -> extendType(codegen, typeMap, m.getType())))
                        .flatMap(Collection::stream)
                        .collect(Collectors.toList());
    }

    /**
     * Create an extended type of the form &lt;T extends type&gt; from type. If the type is a generic, recurse through
     * the item list.  Only elements in the typeMap are remapped.  Other elements are left alone.
     * @param typeMap map of types to generic type names.
     * @param t type to remap
     * @return remapped type
     * @throws UnsupportedExpressionException type was not a SimpleType or generic Type
     */
    public static List<Type> extendType(CodeGen codegen, Map<Type, String> typeMap, Type t) throws UnsupportedExpressionException {
        // TODO: this should only be ClassType.
        if ((t instanceof SimpleType) || (t instanceof ClassType)) {
            String genericName = StringUtil.uniqueVarName(typeMap.values(), t.getName().substring(0, 1).toUpperCase());
            typeMap.put(t, genericName);
            return Collections.singletonList(codegen.extendsType(genericName, t));
        } else if (t instanceof GenericType) {
            return ((GenericType) t).getItemList().stream()
                    .map(CheckedFunction.wrap(el -> extendType(codegen, typeMap, el)))
                    .flatMap(Collection::stream)
                    .toList();
        } else {
            throw new UnsupportedExpressionException(t + " could not be recognized to extend");
        }
    }

    /**
     * from the list of member declarations, convert the subobjects to extended types:
     * @code type var} to {@code T var}
     *      * and
     *      * {@code generic<type> var} to {@code generic<T> var}
     * @param typeMap map of types to type parameters
     * @param memberList list of member declarations
     * @return remapped list of member declarations
     */
    public static List<MemberDeclaration> extendMemberDeclarations(CodeGen codegen,
                                                                   Map<Type, String> typeMap,
                                                                   List<MemberDeclaration> memberList) {

        // Change the type to a generic that extends the based type amd the accessor to protected
        return memberList.stream()
                    .filter(m -> !codegen.isSimpleType(m.getType()))
                    .map(CheckedFunction.wrap(m ->
                            codegen.memberDecl(Constants.JavaKeywords.PROTECTED,
                                               remapType(codegen, typeMap, m.getType()),
                                               m.getVariable())))
                    .toList();
    }

    /**
     * Does this class have any non-primitive members (lists or classes)?
     * @param codegen code generator
     * @param def class definition.
     * @return true if there's a complex member.
     */
    public static boolean hasComplexMembers(CodeGen codegen, ClassDefinition def) {
        return def.getMemberDeclarationList().stream().anyMatch(m -> !codegen.isSimpleType(m.getType()));
    }

    /**
     * Recursively map concrete types to a generic types.
     * @param codegen Code Generator
     * @param typeMap map of old types to new types.
     * @param t type to map.
     * @return remapped type.
     * @throws UnsupportedExpressionException
     */
    public static Type remapType(CodeGen codegen, Map<Type, String> typeMap, Type t)
            throws UnsupportedExpressionException {
        // TODO: should only be class type.
        // TODO: use predicate on type, not instanceof.
        if ((t instanceof SimpleType) || (t instanceof ClassType)) {
            return typeMap.containsKey(t) ? codegen.withNullable(codegen.typeVariable(typeMap.get(t)), true) : t;
        } else if (t instanceof GenericType) {
            GenericType g = (GenericType) t;
            List<Type> remappedTypes = g.getItemList().stream()
                    .map(CheckedFunction.wrap(el -> remapType(codegen, typeMap, el)))
                    .toList();
            return codegen.genericType(remapType(codegen, typeMap, ((GenericType) t).getContainer()), remappedTypes);
        } else {
            throw new UnsupportedExpressionException(t + " could not be recognized to remap");
        }
    }

    /**
     * Remap a parameter declaration to the generic from the type map.
     * @param codegen code generator
     * @param typeMap map of concrete types to generics.
     * @param p parameter decleration
     * @return generic parameter declaration.
     * @throws UnsupportedExpressionException
     */
    private static ParameterDeclaration remapParam(CodeGen codegen,
                                                   Map<Type, String> typeMap,
                                                   ParameterDeclaration p)
    throws UnsupportedExpressionException {
        return codegen.paramDecl(remapType(codegen, typeMap, p.getType()), p.getVariable());
    }

    /**
     * remap the accessors and setters for complex types to reference the generics:
     * {@code
     * <type> get<var-name>() { return <var-name> ) }
     * }
     * maps to:
     * {@code
     * <generic-type> get<var-name>() { return <var-name> ) }
     * }
     * @param typeMap
     * @param methods
     * @return
     * @throws UnsupportedExpressionException
     */
    public static List<FunctionDefinition> extendSettersAndAccessors(CodeGen codegen,
                                                                     Map<Type, String> typeMap,
                                                                     List<FunctionDefinition> methods) {
        if (methods == null) {
            return null;
        } else {
            return methods.stream()
                    .map(CheckedFunction.wrap(m -> remapMethod(codegen, typeMap, m)))
                    .collect(Collectors.toList());
        }
    }

    /**
     * Remap a method from concrete types to generic types.
     * @param codegen code generator
     * @param typeMap map of concrete types to generic types.
     * @param method method with concrete type parameters and arguments.
     * @return method with generic types substituted from the type map.
     * @throws UnsupportedExpressionException
     */
    private static FunctionDefinition remapMethod(CodeGen codegen,
                                                  Map<Type, String> typeMap,
                                                  FunctionDefinition method)
    throws UnsupportedExpressionException {
        FunctionDeclaration decl = method.getFunctionDeclaration();
        ParameterList paramDecls = decl.getParameterList();
        ParameterList remappedParams = (paramDecls != null) ?
                codegen.paramList(paramDecls.getParameterDeclarationList().stream()
                        .map(CheckedFunction.wrap(p -> remapParam(codegen, typeMap, p)))
                        .toList()) :
                codegen.paramList();
        Type remappedReturnType = remapType(codegen, typeMap, decl.getReturnType());
        FunctionDeclaration remappedDecl =
                codegen.functionDecl(decl.getAccessModifier(), decl.isStatic(), decl.getName(),
                                     remappedParams, remappedReturnType);
        return codegen.functionDef(remappedDecl, method.getStatementlist());
    }

    /**
     * Given a class, and a parent Mixin class, remove all the members and accessors, and extend the mixin class
     * so:
     * {@code
     * class info {
     *     int field;
     *     Name name;
     *     Address address;
     *     Contact contact
     * }
     * }
     * maps to:
     * {@code
     * class infoMixin<N extends Name, A extends Address, Contact extends Contact> {
     *     int field;
     *     N name;
     *     A address;
     *     C contact;
     * }
     * class info extends infoMixin<Name, Address, Contact> {
     * }
     * }
     * @param classDef
     * @param mixinDef
     */
    public static ClassDefinition remapClass(CodeGen codegen, ClassDefinition classDef, ClassDefinition mixinDef) {
        // get the types from the Mixin class.
        List<Type> typeList = mixinDef.getClassDeclaration().getGenericsList().getTypeList().stream()
                .map(t -> ((ExtendsType) t).getType())
                .toList();
        ClassDeclaration decl = classDef.getClassDeclaration();
        List<MemberDeclaration> scalarMembers = classDef.getMemberDeclarationList().stream()
                .filter(memberDeclaration -> memberDeclaration.getType() instanceof SimpleType)
                .toList();
        List<FunctionDefinition> scalarMethods = CodegenUtil.accessorsAndSetters(codegen, scalarMembers);
        GenericType extendsType = codegen.genericType(codegen.classType(mixinDef.getClassDeclaration().getClassName()), typeList);
        ClassDeclaration remappedDecl = codegen.classDecl(decl.isStatic(), decl.isFinal(), decl.isData(),
                                                          decl.getClassName(), null, extendsType,
                                                          decl.getImplementsList(), decl.getAnnotationList());
        return codegen.classDef(remappedDecl, scalarMembers, classDef.getConstructorDefinitionList(), scalarMethods);
    }


    // TODO: this looks like a duplicate.
    // TODO: review this when Android/Kotlin is operational.
    public static boolean writeKotlinMixinClass(CodeGen codegen, CreateObjects createObjects, ClassFile classFile)
    throws Exception {
        ClassDefinition classDef = classFile.getClassDefinition();
        boolean mixinGenerated = false;
        if (CreateMixin.hasComplexMembers(codegen, classDef)) {
            String mixinName = classDef.getClassDeclaration().getClassName() + Constants.Substitutions.MIXIN;
            CreateMixin createMixin = new CreateMixin(classFile, mixinName);
            ClassDefinition mixinDef = createMixin.createFrom(codegen, classDef);
            createMixin.toClassFile(codegen).write(createObjects.getOutputDir());
            classFile.setClassDefinition(CreateMixin.remapClass(codegen, classDef, mixinDef));
            ((KotlinClassFile) classFile).write(createObjects.getOutputDir(), mixinDef);
            mixinGenerated = true;
        } else {
            classFile.write(createObjects.getOutputDir());
        }
        return mixinGenerated;
    }
}
