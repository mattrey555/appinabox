package com.apptechnik.appinabox.engine.codegen.objects;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.imports.CreateImports;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeGen;
import com.apptechnik.appinabox.engine.codegen.pojos.StatementList;
import com.apptechnik.appinabox.engine.codegen.pojos.Type;
import com.apptechnik.appinabox.engine.codegen.pojos.java.JavaCodeGen;
import com.apptechnik.appinabox.engine.codegen.pojos.kotlin.KotlinCodeGen;
import com.apptechnik.appinabox.engine.codegen.util.SQLFile;
import com.apptechnik.appinabox.engine.exceptions.TypeNotFoundException;
import com.apptechnik.appinabox.engine.parser.pojos.*;
import com.apptechnik.appinabox.engine.util.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Properties which are common when generating code from a set of .SQL files.
 */
public class CreateCommon {
    protected final String packageName;                                     // name of the package to generate files for
    protected final String varName;                                         // SQL file name w/o extension is object name
    protected final File sourceFile;                                        // file containing SQL statements + JSON Mapping.
    protected final SQLFile sqlFile;                                        // SQL file with basic processing.
    protected final String responseName;                                    // root of JSON response and used in package name
    protected final Tree<QueryFieldsNode> jsonResponseTree;                 // JSON result mapping.
    protected final String requestName;                                     // root of JSON request and used in package name
    protected final Tree<MergedFieldNode> jsonRequestTree;                  // JSON request mapping.
    private final File outputDir;                                         // output directory
    private final CodeGen javaCodeGen;                                    // java code generator
    private final CodeGen kotlinCodeGen;                                  // Kotlin code generator.
    private final ExtraProperties properties;

    /**
     * Create the objects for the request and response JSON, common properties, and SQL statements.
     * @param propertiesFile properties file (is this used?)
     * @param outputDir  root output directory for generated java files (created if needed)
     * @throws Exception if the source SQL  file can't be read, failed to parse the JSON
     * or failed to parse SQL
     */

    public CreateCommon(File propertiesFile,
                        File outputDir,
                        File sourceFile) throws Exception {

        // POJOS and SQL function classes inhabit different directories and packages.
        this.sourceFile = sourceFile;
        sqlFile = new SQLFile(sourceFile);
        this.outputDir = outputDir;
        this.properties = new ExtraProperties(propertiesFile);
        this.javaCodeGen = new JavaCodeGen();
        this.kotlinCodeGen = new KotlinCodeGen();
        this.packageName = sqlFile.findTag(Constants.Directives.PACKAGE);
        // parse and validate the response and request trees.
        this.varName = StringUtil.removeExtension(sqlFile.getName());
        this.requestName = StringUtil.findTag(getComments(), Constants.Directives.NAME_REQUEST);
        this.jsonRequestTree = sqlFile.createJSONTree(getVarName(),
                                                        Constants.Directives.NAME_REQUEST,
                                                        Constants.Directives.JSON_REQUEST,
                                                        new MergedFieldNode.Factory());
        this.responseName = StringUtil.findTag(getComments(), Constants.Directives.NAME_RESPONSE);
        this.jsonResponseTree = sqlFile.createJSONTree(getVarName(),
                                                        Constants.Directives.NAME_RESPONSE,
                                                        Constants.Directives.JSON_RESPONSE,
                                                        new QueryFieldsNode.Factory());
        // parse and validate the response and request trees.
        if (this.jsonResponseTree != null) {
            this.jsonResponseTree.get().setName(this.responseName);
            FieldReferenceNode.validateJsonTree(this.jsonResponseTree);
        }
    }

    public File getOutputDir() {
        return outputDir;
    }

    public File getPropertiesFile() {
        return properties.getFile();
    }

    public ExtraProperties getProperties() {
        return properties;
    }

    public CodeGen getJavaCodeGen() {
        return javaCodeGen;
    }

    public CodeGen getKotlinCodeGen() {
        return kotlinCodeGen;
    }

    /**
     * A cheap way to open/a/java/file from.a.package.name
     * @param packageName java package.
     * @param fileName file name (no extension
     * @return IndentingFileWriter to write the java file to
     * @throws IOException if the file couldn't be opened.
     */
    public IndentingFileWriter openJavaFile(String packageName, String fileName) throws IOException {
        return new IndentingFileWriter(new FileWriter(javaFile(packageName, fileName)), 0);
    }

    // TODO: throw an exception if the directories cannot be created.
    public File javaFile(String packageName, String fileName)  {
        File dir = new File(getOutputDir(), StringUtil.packageToFilePath(packageName));
        dir.mkdirs();
        return new File(dir, fileName + Constants.Extensions.JAVA);
    }

    public IndentingFileWriter openKotlinFile(String packageName, String fileName) throws Exception {
        return new IndentingFileWriter(new FileWriter(kotlinFile(packageName, fileName)), 0);
    }

    // TODO: wrong output directory.
    public File kotlinFile(String packageName, String fileName)  throws Exception {
        return new File(OutputPaths.androidKotlinDir(getOutputDir(), packageName), fileName + Constants.Extensions.KOTLIN);
    }

    /**
     * Is the request (UPDATE/INSERT) a single field, so we don't generate POJOs for it.
     * @return true if the request only has a single field.
     */
    public boolean isSingleFieldRequest() {
        return (getJsonRequestTree() != null) && FieldReferenceNode.isSingleField(getJsonRequestTree());
    }

    /**
     * Is the reqponse (UPDATE/INSERT) a single field, so we don't generate POJOs for it.
     * @return true if the response only has a single field.
     */
    public boolean isSingleFieldResponse() {
        return (getJsonResponseTree() != null) && FieldReferenceNode.isSingleField(getJsonResponseTree());
    }

    public String getPackageName() {
        return packageName;
    }

    public String getVarName() {
        return varName;
    }

    public String getObjectsName() {
        return varName;
    }

    public SQLFile getSqlFile() {
        return sqlFile;
    }

    public File getSourceFile() {
        return sourceFile;
    }

    public Tree<QueryFieldsNode> getJsonResponseTree() {
        return jsonResponseTree;
    }

    public Tree<MergedFieldNode> getJsonRequestTree() {
        return jsonRequestTree;
    }

    public String getResponseName() {
        return responseName != null ? responseName : varName;
    }

    /**
     * The JSON request might be null, but there may be DELETE statements which take command-line/URL parameters.
     * @return
     */
    public String getRequestName() {
        return requestName != null ? requestName : varName;
    }

    public Type getResponseClass(CodeGen codegen) throws TypeNotFoundException {
        return QueryFieldsNode.getNodeType(codegen, getJsonResponseTree());
    }

    public List<String> getComments() {
        return getSqlFile().getComments();
    }


    public String getQueryPackageName() {
        return packageName + "." + Constants.Packages.QUERY;
    }
}
