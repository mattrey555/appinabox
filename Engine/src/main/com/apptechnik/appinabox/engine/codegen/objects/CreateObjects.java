package com.apptechnik.appinabox.engine.codegen.objects;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.imports.CreateImports;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeGen;
import com.apptechnik.appinabox.engine.codegen.pojos.StatementList;
import com.apptechnik.appinabox.engine.codegen.pojos.Type;
import com.apptechnik.appinabox.engine.exceptions.*;
import com.apptechnik.appinabox.engine.log.Log;
import com.apptechnik.appinabox.engine.parser.*;
import com.apptechnik.appinabox.engine.parser.create.StatementParser;
import com.apptechnik.appinabox.engine.parser.pojos.*;
import com.apptechnik.appinabox.engine.parser.util.JsonParameterMap;
import com.apptechnik.appinabox.engine.util.*;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.delete.Delete;
import net.sf.jsqlparser.statement.insert.Insert;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.update.Update;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * notes:
 * named parameters can come from incoming JSON, but can also be explicitly specified, either from command-line
 * parameters, or from headers/path components/query parameters from the URL.
 * @param <T> QueryFieldsNode or MergedFieldNode
 */


public class CreateObjects<T extends FieldReferenceNode> extends CreateCommon {
    private final File createTableFile;                                   // Create Table statements referred to in SQL call.
    private final TableInfoMap createTableInfoMap;                        // tables parsed form createTableFile
    private final TypeMap fixedParamTypeMap;                              // parameter types specified in SQL file comments
    private final ParameterMap queryParams;                               // params from URL path, query params, HTTP headers.
    private final JsonParameterMap jsonParameterMap;                      // map of REQUEST JSON to parameters in SELECT statements .
    protected final StatementParamMap statementParamMap;                  // statement->parameter map;
    protected String httpMethod;                                          // explicit or derived HTTP method (POST/GET/etc)

    /**
     * Create the objects for the request and response JSON, common properties, and SQL statements.
     * @param sourceFile SQL statements with comments for directives.
     * @param propertiesFile properties file (is this used?)
     * @param createTableFile file containing CREATE TABLE statements and other SQL statements for schema
     * @param statementParser parsed CREATE TABLE and other statements into schema
     * @param outputDir  root output directory for generated java files (created if needed)
     * @throws Exception if the source SQL  file can't be read, failed to parse the JSON
     * or failed to parse SQL
     */

    public CreateObjects(File sourceFile,
                         File propertiesFile,
                         File createTableFile,
                         StatementParser statementParser,
                         File outputDir)
    throws Exception {
        super(propertiesFile, outputDir, sourceFile);
        System.out.println("generating objects for " + sourceFile.getAbsolutePath());
        List<String> syncErrorMessages = new ArrayList<String>();
        getSqlFile().markSyncTables(statementParser.getTableInfoMap(), syncErrorMessages);

        // read the source of truth, and get the comments, where we extract the directives.
        validateNotNullableFormat(getPackageName(), Constants.Regexes.JAVA_PACKAGE, " improperly formatted java package name");
        validateNotNullableFormat(getObjectsName(), Constants.Regexes.IDENTIFIER, " improperly formatted name");
        this.createTableFile = createTableFile;
        this.createTableInfoMap = statementParser.getTableInfoMap();
        this.queryParams = new ParameterMap(this.getComments());

        // apply the fixed types to override inference.
        this.fixedParamTypeMap = new TypeMap(getSqlFile());

        statementParamMap = new StatementParamMap(getSqlFile().getStatementList(), createTableInfoMap,
                                                  getSqlFile().getJdbcDotParamMaps(), fixedParamTypeMap, queryParams);
        if (this.getJsonRequestTree() != null) {
            FieldReferenceNode.validateJsonTree(this.getJsonRequestTree());
            MapUpdateTree.assignSQLStatements(getJsonRequestTree(), queryParams, getSqlFile().getStatementList(),
                                              statementParamMap, createTableInfoMap);
        }
        jsonParameterMap = new JsonParameterMap(getSqlFile().getStatementList(), this.getQueryParams(),
                                                this.statementParamMap, this.getJsonRequestTree());
        List<ParameterMap.ParamError> paramErrorList = statementParamMap.validateParameters(this.getQueryParams());
        ParameterMap.ParamError.writeLog(paramErrorList);
        if (ParameterMap.ParamError.anyFatalError(paramErrorList)) {
            throw new ParamTypeMismatchException("failed to validate parameters");
        }

        this.httpMethod = getSpecifiedHttpMethod();
        String derivedHttpMethod = deriveHTTPMethod();
        if (this.httpMethod == null) {
            this.httpMethod = derivedHttpMethod;
        } else if (!this.httpMethod.equals(derivedHttpMethod)) {
            Log.warn("HTTP method " + this.httpMethod + " specified in " + sourceFile.getAbsolutePath() +
                     " does not match the HTTP method derived from SQL statements " + derivedHttpMethod);
        }
    }

    /**
     * Creating directories is a side-effect of construction, and constructors should not
     * have side-effects.
     * @throws IOException if the directories or files cannot be created.
     */
    public void initializeDirectories() throws IOException {

        // POJOS and SQL function classes inhabit different directories and packages.
        File objectsDir = new File(getOutputDir(), StringUtil.packageToFilePath(this.getPackageName()));
        FileUtil.checkDirectory("object file output directory: " + this.getPackageName(), objectsDir);
        File sqlFnsDir = new File(getOutputDir(), StringUtil.packageToFilePath(this.getPackageName() + "." + Constants.Packages.SQL));
        FileUtil.checkDirectory("SQL Functions output directory: " + this.getPackageName() + "." + Constants.Packages.SQL, sqlFnsDir);
    }


    /**
     * Parse the CREATE TABLE files, our source of truth for SQL types, and he list of explicit types
     * for parameters.
     * @param createTableFile source File
     * @return statementParser (wrapper for tableInfoMap.
     * @throws Exception if there's a parse failure
     */
    public static StatementParser parseSchema(File createTableFile) throws Exception {
        StatementParser statementParser = new StatementParser();
        String createTableText = FileUtil.readFileToString(createTableFile);
        try {
            statementParser.parse(createTableText);
        } catch (Exception ex) {
            throw new Exception("threw exception while parsing " + createTableFile.getAbsolutePath(), ex);
        }
        return statementParser;
    }

    /**
     * An HTTP method can be specified by either the path to the file, with the SQL files divided
     * into directories for POST/PUT/GET/DELETE, or by the method: directive in the file.
     * @return the specified HTTP method
     * @throws PropertiesException if the directive is not a valid HTTP method, or is specified
     * in both the path and method directive, but they don't match.
     */
    public String getSpecifiedHttpMethod() throws PropertiesException {
        String pathMethodName = null;
        String filePath = getSourceFile().getAbsolutePath();
        if (filePath.toUpperCase().contains(Constants.HttpMethods.POST)) {
            pathMethodName = Constants.HttpMethods.POST;
        }
        if (filePath.toUpperCase().contains(Constants.HttpMethods.PUT)) {
            pathMethodName = Constants.HttpMethods.PUT;
        }
        if (filePath.toUpperCase().contains(Constants.HttpMethods.GET)) {
            pathMethodName = Constants.HttpMethods.GET;
        }
        if (filePath.toUpperCase().contains(Constants.HttpMethods.DELETE)) {
            pathMethodName = Constants.HttpMethods.DELETE;
        }
        String directiveMethodName = StringUtil.findTag(getComments(), Constants.Directives.METHOD);
        if (directiveMethodName != null) {
            if (!StringUtil.in(Constants.HttpMethods.METHODS, directiveMethodName)) {
                throw new PropertiesException("HTTP method was specified as " + directiveMethodName +
                        " in " + filePath + " is not a supported HTTP method " +
                        StringUtil.concatArray(", ", Constants.HttpMethods.METHODS));
            }
            if ((pathMethodName != null) && !pathMethodName.equals(directiveMethodName)) {
                throw new PropertiesException("HTTP method " + pathMethodName + " from path " + filePath +
                        " specified as " + pathMethodName +
                        " does not match " + directiveMethodName);
            }
            return directiveMethodName;
        } else {
            return pathMethodName;
        }
    }

    /**
     * Recurse the sqlDir and execute the consumer on each file which ends with a .sql extension.
     * @param sqlDir directory containing SQL files.
     * @param consumer function to apply to each SQL file.
     * @throws IOException if any of the files or directories can't be created.
     */
    public static void walkSQLDir(File sqlDir, CheckedConsumer<File> consumer) throws IOException {
        FileUtil.walkFiles(sqlDir, Constants.Extensions.SQL, consumer);
    }

    /**
     * Derive the HTTP method by the following rules:
     * POST: If there are any INSERT statements
     * PUT: If there are any UPDATE statements
     * DELETE: If there are any DELETE statements
     * GET: otherwise.
     * @return HTTP method from Constants.HttpMethods.
     */
    public String deriveHTTPMethod() {
        boolean anyInsert = false;
        boolean anyUpdate = false;
        boolean anyDelete = false;
        for (Statement statement : getSQLStatementList()) {
            anyInsert |= (statement instanceof Insert);
            anyUpdate |= (statement instanceof Update);
            anyDelete |= (statement instanceof Delete);
        }
        if (anyInsert) {
            return Constants.HttpMethods.POST;
        }
        if (anyUpdate) {
            return Constants.HttpMethods.PUT;
        }
        if (anyDelete) {
            return Constants.HttpMethods.DELETE;
        }
        return Constants.HttpMethods.GET;
    }

    public static boolean validateFormat(String s, String regex, String msg) throws ArgumentException {
        if (s.matches(regex)) {
            return true;
        } else {
            throw new ArgumentException("argument: " + s + " " + msg);
        }
    }

    public static boolean validateNullableFormat(String s, String regex, String msg) throws ArgumentException {
        if (s == null) {
            return true;
        } else if (s.matches(regex)) {
            return true;
        } else {
            throw new ArgumentException("argument: " + s + " " + msg);
        }
    }

    /**
     * Used for validation of parameters from input file against regular expressions. Throws
     * an exception on null.
     * @param s string to test
     * @param regex regular expression to test against.
     * @param msg error message to display
     * @return true if argument matches regex, otherwise exception
     * @throws ArgumentException argument was null or did not match regular expression.
     */
    public static boolean validateNotNullableFormat(String s, String regex, String msg) throws ArgumentException {
        if (s == null) {
            throw new ArgumentException("null argument " + msg);
        } else if (s.matches(regex)) {
            return true;
        } else {
            throw new ArgumentException("argument: " + s + " " + msg);
        }
    }

    public String getServletPackageName() {
        return getPackageName() + "." + Constants.Packages.SERVLET;
    }

    public String getServletName() {
        return getVarName() + Constants.Extensions.SERVLET;
    }

    public String getQuerySQLPackageName() {
        return getPackageName() + "." + Constants.Packages.QUERY + "." + Constants.Packages.SQL;
    }

    public String getUpdatePackageName() {
        return getPackageName() + "." + Constants.Packages.UPDATE;
    }

    public String getUpdateSQLPackageName() {
        return getPackageName() + "." + Constants.Packages.UPDATE + "." + Constants.Packages.SQL;
    }

    public String getSQLFunctionsClassName() {
        return getObjectsName() + Constants.Suffixes.SQL_FNS;
    }

    public List<Statement> getSQLStatementList() {
        return getSqlFile().getStatementList();
    }

    public List<Select> getSelectStatementList() {
        return StatementUtil.filterSelects(getSQLStatementList());
    }

    public boolean isMultipleSelect() {
        return getSelectStatementList().size() > 1;
    }

    /**
     * If the object is top-level, and the result of multiple SELECT calls, or it is a scalar
     * child, then it is a scalar.
     * @param stack stack of query fields nodes back to root.
     * @return true if the object is scalar.
     */
    public boolean isScalarNode(Stack<Tree<QueryFieldsNode>> stack) {
        if (stack.size() > 1) {
            QueryFieldsNode parent = stack.get(1).get();
            QueryFieldsNode node = stack.get(0).get();
            return node.childUsesParentKeys(parent);
        } else {
            return isMultipleSelect();
        }
    }

    public  List<Statement> getUpdateStatementList() {
        return StatementUtil.filterUpdateDeleteInsert(getSQLStatementList());
    }

    public Map<Statement, Map<String, String>> getJdbcDotParamMaps() {
        return getSqlFile().getJdbcDotParamMaps();
    }

    public Map<String, String> getJdbcDotParamMap(Statement statement) {
        return getSqlFile().getJdbcDotParamMaps().get(statement);
    }

    public TableInfoMap getTableInfoMap() {
        return createTableInfoMap;
    }

    public TypeMap getFixedParamTypeMap() {
        return fixedParamTypeMap;
    }

    public ParameterMap getQueryParams() {
        return queryParams;
    }

    public ParameterMap getStatementParamMap(Statement statement) {
        return statementParamMap.getParamMap(statement);
    }

    public StatementParamMap getStatementParamMap() {
        return statementParamMap;
    }

    public String getServletHttpMethod() {
        return StringUtil.capitalizeFirstChar(httpMethod);
    }

    public File getCreateTableFile() {
        return createTableFile;
    }

    public StatementParamMap getUpdateParameters() {
        return new StatementParamMap(statementParamMap, getUpdateStatementList());
    }

    /**
     * The JSON request might be null, but there may be DELETE statements which take command-line/URL parameters.
     * @return
     */
    public Type getRequestType(CodeGen codegen, ParameterMap parameterMap)
    throws NameNotFoundException, TypeNotFoundException {
        if (isSingleFieldRequest()) {
            return MergedFieldNode.getNodeType(codegen, getJsonRequestTree(), parameterMap);
        } else {
            return codegen.classType(getRequestName());
        }
    }

    public JsonParameterMap getJsonParameterMap() {
        return jsonParameterMap;
    }

    public Type getRequestType(CodeGen codegen,
                               List<Statement> selectList,
                               List<Statement> updateList)
    throws TypeMismatchException, TypeNotFoundException, NameNotFoundException {
        // if there is an update list, see if the root node of the JSON tree maps to any JDBC Parameter.
        // if there is a select list, see if the root node of the JSON tree maps to a JDBC Parameter in a SELECT in clause.
        Type rootType = null;
        if (getJsonRequestTree() != null) {
            if (!updateList.isEmpty()) {
                ParameterMap updateParameterMap = new ParameterMap(getStatementParameterMapMap(), updateList);
                rootType = MergedFieldNode.getNodeType(codegen, getJsonRequestTree(), updateParameterMap);
            }
            if ((rootType == null) && !selectList.isEmpty()) {
                ParameterMap selectParameterMap = new ParameterMap(getStatementParameterMapMap(), selectList);
                rootType = MergedFieldNode.getNodeType(codegen, getJsonRequestTree(), selectParameterMap);
            }
        }
        return rootType;
    }

    public Map<Statement, ParameterMap> getStatementParameterMapMap() {
        return statementParamMap.getStatementParamMap();
    }

    public Type getResponseType(CodeGen codegen) throws TypeNotFoundException {
         // if there are multiple selects, the top level object contains the lists of their results.
        if (StatementUtil.filterSelects(getSQLStatementList()).size() > 1) {
            return getResponseClass(codegen);
        } else {
            return codegen.listType(getResponseClass(codegen));
        }
    }

    /**
     * Create the imports for the response POJOs and SQL functions if there is a non-scalar response.
     * @param codegen code generator.
     * @return list of (possibly empty) import statements.
     */
    public StatementList createQueryImports(CodeGen codegen) {
        CreateImports createImports = new CreateImports(packageName);
        boolean hasResponsePojos = (getJsonResponseTree() != null) && !isSingleFieldResponse();
        return createImports.queryImports(codegen, hasResponsePojos);
    }

    /**
     * Create the imports for the request POJOs and SQL functions if there is a non-scalar request.
     * @param codegen code generator
     * @return list of (possibly empty) import statements.
     */
    public StatementList createUpdateImports(CodeGen codegen) {
        CreateImports createImports = new CreateImports(packageName);
        boolean hasUpdatePojos = (getJsonRequestTree() != null) && !isSingleFieldRequest();
        boolean hasUpdates = !StatementUtil.filterUpdateInsert(getSQLStatementList()).isEmpty();
        return createImports.updateImports(codegen, hasUpdatePojos, hasUpdates);
    }
}
