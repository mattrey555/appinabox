package com.apptechnik.appinabox.engine.codegen.objects;

import com.apptechnik.appinabox.engine.codegen.android.CreateObjectsAndroid;
import com.apptechnik.appinabox.engine.parser.create.StatementParser;
import com.apptechnik.appinabox.engine.util.CheckedConsumer;
import com.apptechnik.appinabox.engine.util.ExtraProperties;
import com.apptechnik.appinabox.engine.util.OutputPaths;
import com.apptechnik.appinabox.engine.util.XMLUtils;

import java.io.File;
import java.io.IOException;

/**
 * Enforce a common interface for all SQLFile processing.
 */
public abstract class ProcessSQLFiles implements CheckedConsumer<File> {
    protected final StatementParser statementParser;              // PERFORMANCE: tableInfo parsed from schema.
    protected final File outputDir;                               // .java output directory
    protected final File createTableFile;                         // schema file
    protected final File propertiesFile;                          // .properties file for template substitution
    protected final ExtraProperties properties;

    /**
     * Create the nav_graph.xml file, with fragments for  parameters for each query,
     *
     * @param propertiesFile  properties file for resource transformation (stuff like package nme)
     * @param createTableFile used by CreateObjects
     * @param statementParser used by CreateObjects
     * @param outputDir       used by CreateObjects
     * @throws Exception
     */
    public ProcessSQLFiles(StatementParser statementParser,
                           File outputDir,
                           File createTableFile,
                           File propertiesFile) throws IOException {
        this.statementParser = statementParser;
        this.outputDir = outputDir;
        this.createTableFile = createTableFile;
        this.propertiesFile = propertiesFile;
        this.properties = new ExtraProperties(this.propertiesFile);
    }

    public void create(File sqlDir) throws Exception {
        CreateObjects.walkSQLDir(sqlDir, this);
    }
}
