package com.apptechnik.appinabox.engine.codegen.objects;

import com.apptechnik.appinabox.engine.codegen.android.CreateObjectsAndroid;
import com.apptechnik.appinabox.engine.parser.create.StatementParser;
import com.apptechnik.appinabox.engine.util.CheckedConsumer;
import com.apptechnik.appinabox.engine.util.ExtraProperties;

import java.io.File;
import java.io.IOException;

/**
 * Enforce a common interface for all SQLFile processing.
 */
public abstract class ProcessSQLFilesAndroid extends ProcessSQLFiles {
    public ProcessSQLFilesAndroid(StatementParser statementParser,
                                  File outputDir,
                                  File createTableFile,
                                  File propertiesFile) throws IOException {
        super(statementParser, outputDir, createTableFile, propertiesFile);
    }


    @Override
    public void accept(File file) throws Exception {
        CreateObjectsAndroid createObjectsAndroid =
                new CreateObjectsAndroid(file, propertiesFile, createTableFile, statementParser, outputDir);
        accept(createObjectsAndroid);
    }

    public abstract void accept(CreateObjectsAndroid createObjectsAndroid) throws Exception;
}
