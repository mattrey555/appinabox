package com.apptechnik.appinabox.engine.codegen.objects;

import com.apptechnik.appinabox.engine.codegen.android.CreateObjectsAndroid;
import com.apptechnik.appinabox.engine.codegen.android.CreateResultsListAdapter;
import com.apptechnik.appinabox.engine.codegen.query.CreateQueryObjects;
import com.apptechnik.appinabox.engine.parser.pojos.ColumnsAndKeys;
import com.apptechnik.appinabox.engine.parser.pojos.QueryFieldsNode;
import com.apptechnik.appinabox.engine.parser.pojos.Tree;
import net.sf.jsqlparser.statement.select.Select;

import java.util.List;
import java.util.Map;
import java.util.Stack;

/**
 * Iterate over the list of select statements, find the associated JSON results, and traverse them.
 */
public abstract class TraverseQueryObjects implements Tree.ConsumerException<Stack<Tree<QueryFieldsNode>>> {
    private final CreateObjects createObjects;

    public TraverseQueryObjects(CreateObjects createObjects) {
        this.createObjects = createObjects;
    }

    public void create() throws Exception {
        if (createObjects.getJsonResponseTree() != null) {
            for (int i = 0; i < createObjects.getSelectStatementList().size(); i++) {
                CreateQueryObjects.getAssociatedJsonTree(createObjects.getSelectStatementList(), i, createObjects.getJsonResponseTree())
                        .traverseTreeStack(this);
            }
        }
    }

    public CreateObjects getCreateObjects() {
        return createObjects;
    }
}

