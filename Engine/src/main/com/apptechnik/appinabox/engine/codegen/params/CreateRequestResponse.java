package com.apptechnik.appinabox.engine.codegen.params;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.codegen.util.CodegenUtil;
import com.apptechnik.appinabox.engine.exceptions.NameNotFoundException;
import com.apptechnik.appinabox.engine.exceptions.TypeMismatchException;
import com.apptechnik.appinabox.engine.exceptions.TypeNotFoundException;
import com.apptechnik.appinabox.engine.exceptions.UnsupportedParamTypeException;
import com.apptechnik.appinabox.engine.parser.pojos.ParameterMap;
import com.apptechnik.appinabox.engine.parser.types.SQLType;
import com.apptechnik.appinabox.engine.parser.util.ColDataTypeUtils;
import com.apptechnik.appinabox.engine.util.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * TODO: replace this with a template implementation.  This is more complex than neccessary.
 * Create a RequestResponse class which parses the parameters and JSON from the query, and saves the results
 * Create parsers for servlet and command line parameters.
 */
public class CreateRequestResponse {
    private CreateObjects createObjects;

    public CreateRequestResponse(CreateObjects createObjects) {
        this.createObjects = createObjects;
    }

    public ClassFile toClassFile() throws Exception {
        CodeGen codegen = createObjects.getJavaCodeGen();
        PackageStatement packageStmt = codegen.packageStmt(createObjects.getPackageName());
        StatementList importStmtList = createImports(codegen);
        ClassDefinition classDef = createClass(codegen,
                                               Constants.ServletVariables.SERVLET_REQUEST,
                                               Constants.ServletVariables.SERVLET_RESPONSE,
                                               Constants.Variables.ARGS);
        return codegen.classFile(packageStmt, importStmtList, classDef);
    }

    /**
     * Create the import list.
     * @return
     * @throws IOException
     */
    private StatementList createImports(CodeGen codegen) throws Exception {
        StatementList importStatementList = codegen.importStatementList(Constants.Templates.REQUEST_RESPONSE_IMPORTS);
        importStatementList.addStatements(createObjects.createUpdateImports(codegen));
        if (createObjects.getJsonResponseTree() != null) {
            importStatementList.addStatements(createObjects.createQueryImports(codegen));
        }
        return importStatementList;
    }

    /**
     * Parse the path, query, ahd header parameeters from the input file comments.
     * Generate parse from HTTP Servlet and command-line constructors.
     * @param httpRequestVarName variable name for HttpServletRequest
     * @param httpResponseVarName variable name for HttpServletResponse
     * @param cmdLineArgsVarName variable name for command-line arguments.
     * @return class definition including members, constructor, accessors and setters.
     */
    public ClassDefinition createClass(CodeGen codegen,
                                       String httpRequestVarName,
                                       String httpResponseVarName,
                                       String cmdLineArgsVarName)
    throws NameNotFoundException, TypeNotFoundException, UnsupportedParamTypeException, TypeMismatchException {
        ClassDeclaration classDecl = codegen.classDecl(Constants.JavaTypes.REQUEST_RESPONSE,
                                                       codegen.classAnnot(Constants.Annotations.GENERATED));
        ParameterMap queryParamMap = createObjects.getQueryParams();

        // NOTE: need to handle scalar input test
        ParameterMap mergedParamMap = new ParameterMap(createObjects.getStatementParameterMapMap(),
                                                       createObjects.getSQLStatementList());
        List<ConstructorDefinition> constructorList = new ArrayList<ConstructorDefinition>();
        constructorList.add(servletConstructor(codegen, httpRequestVarName, httpResponseVarName, queryParamMap, mergedParamMap));
        constructorList.add(commandLineConstructor(codegen, cmdLineArgsVarName, queryParamMap, mergedParamMap));
        List<MemberDeclaration> memberDeclarationList = memberDeclarations(codegen, mergedParamMap);
        return codegen.classDef(classDecl, memberDeclarationList,
                                constructorList, methodDefinitions(codegen, memberDeclarationList));
    }

    /**
     * Generate:
     * {@code
     * Type listType = new TypeToken<ArrayList<type>>(){}.getType();
     * <name> = gson.fromJson(<source>, listType);
     * }
     */
    private List<CodeStatement> parseInput(CodeGen codegen,
                                           String gsonName,
                                           CodeExpression source,
                                           ParameterMap mergedParamMap)
    throws TypeNotFoundException, NameNotFoundException {
        Type inputType = createObjects.getRequestType(codegen, mergedParamMap);
       // if there's more than one select, the top-level type is an object containing lists, rather
        // than a single list.
        List<CodeStatement> statementList = new ArrayList<CodeStatement>();

        // Type listType = new TypeToken<ArrayList<type>>(){}.getType();
        Type typeToken = codegen.genericType(codegen.classType(Constants.JavaTypes.TYPE_TOKEN),
                                             codegen.arrayListType(inputType));
        Allocation newTypeToken = codegen.alloc(typeToken, null, codegen.stmtList());
        CodeBinaryExpression tokenGetType = codegen.dot(newTypeToken, codegen.methodCall(Constants.Functions.GET_TYPE));
        Variable listTypeVar = codegen.var(Constants.Variables.LIST_TYPE);
        statementList.add(codegen.typedAssignStmt(codegen.classType(Constants.JavaTypes.TYPE), listTypeVar, tokenGetType));
        // <name> = gson.fromJson(<inputName>, listType);
        MethodCall parseJsonCall = codegen.methodCall(codegen.var(gsonName), Constants.Functions.FROM_JSON,
                                                      source, listTypeVar);
        statementList.add(codegen.assignStmt(codegen.var(Constants.Variables.INPUT), parseJsonCall));
        return statementList;
    }

    /**
     * {@code InputStreamReader reader = new InputStreamReader(request.getInputStream(), "UTF-8"); }
     * @return
     */
    private CodeStatement servletReaderCall(CodeGen codegen,
                                            Variable requestVar,
                                            Variable readerVar,
                                            String encoding) {
        MethodCall getInputStreamCall = codegen.methodCall(requestVar, Constants.Functions.GET_INPUT_STREAM);
        Type isReaderType = codegen.classType(Constants.JavaTypes.INPUT_STREAM_READER);
        Allocation newStreamReader = codegen.alloc(isReaderType, getInputStreamCall, codegen.stringConst(encoding));
        return codegen.typedAssignStmt(isReaderType, readerVar, newStreamReader);
    }

    /**
     * {@code }OutputStreamWriter writer = new OutputStreamWriter(response.getOutputStream(), "UTF-8");
     * @return
     */
    private CodeStatement servletWriterCall(CodeGen codegen, Variable requestVar, Variable writerVar, String encoding) {
        MethodCall getInputStreamCall = codegen.methodCall(requestVar, Constants.Functions.GET_OUTPUT_STREAM);
        Type osReaderType = codegen.classType(Constants.JavaTypes.OUTPUT_STREAM_WRITER);
        Allocation newStreamReader =  codegen.alloc(osReaderType, getInputStreamCall, codegen.stringConst(encoding));
        return codegen.typedAssignStmt(osReaderType, writerVar, newStreamReader);
    }

    // BufferedReader br = new BufferedReader(new FileReader(new File(args[1])));
    private CodeStatement commandLineReaderCall(CodeGen codegen, Variable readerVar, CodeExpression cmdLineArgsVar) {
        Allocation newFile = codegen.alloc(codegen.classType(Constants.JavaTypes.FILE), cmdLineArgsVar);
        Allocation newFileReader = codegen.alloc(codegen.classType(Constants.JavaTypes.FILE_READER), newFile);
        Allocation newBufferedReader = codegen.alloc(codegen.classType(Constants.JavaTypes.BUFFERED_READER), newFileReader);
        return codegen.typedAssignStmt(codegen.classType(Constants.JavaTypes.BUFFERED_READER),
                                       readerVar, newBufferedReader);
    }

    //Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ").create();
    private CodeStatement createGson(CodeGen codegen) {
        Allocation builderAllocation = codegen.alloc(codegen.classType(Constants.JavaTypes.GSON_BUILDER));
        MethodCall setDateFormat = codegen.methodCall(Constants.Functions.SET_DATE_FORMAT,
                                                      codegen.stringConst(Constants.Formats.ISO_8601_DATE_FORMAT));
        CodeExpression createGson = codegen.dot(codegen.dot(builderAllocation, setDateFormat),
                                                            codegen.methodCall(Constants.Functions.CREATE));
        return codegen.assignStmt(codegen.var(Constants.Variables.GSON), createGson);
    }

    /**
     * Servlet constructor takes request and response arguments. Errors are written to the response stream
     * @param httpRequestVarName request variable name.
     * @param httpResponseVarName response variable name.
     * @param queryParamMap parameters from URL and headers.
     * @param mergedParamMap parameters from JDBC Named Parameters
     * @return {@code RequestResponse(HttpServletRequest <httpRequestVarName>, HttpServletResponse <httpResponseVarName>)
     *               throws IOException, UnsupportedEncodingException {
     * ... assign and validate parameters ...
     * ... parse request payload ...
     * }
     * }
     * @throws NameNotFoundException variable not found.
     * @throws TypeNotFoundException type not recognized.
     * @throws UnsupportedParamTypeException parameter type not supported.
     */
    private ConstructorDefinition servletConstructor(CodeGen codegen,
                                                     String httpRequestVarName,
                                                     String httpResponseVarName,
                                                     ParameterMap queryParamMap,
                                                     ParameterMap mergedParamMap)
    throws NameNotFoundException, TypeNotFoundException, UnsupportedParamTypeException {
        Variable requestVar = codegen.var(httpRequestVarName);
        ParameterDeclaration requestArg =
            codegen.paramDecl(codegen.classType(Constants.JavaTypes.HTTP_SERVLET_REQUEST), requestVar);
        Variable responseVar = codegen.var(httpResponseVarName);
        ParameterDeclaration responseArg =
            codegen.paramDecl(codegen.classType(Constants.JavaTypes.HTTP_SERVLET_RESPONSE), responseVar);
        TypeList exceptionList = codegen.typeList(codegen.classType(Constants.JavaExceptions.IO_EXCEPTION),
                                                  codegen.classType(Constants.JavaExceptions.UNSUPPORTED_ENCODING_EXCEPTION));
        ConstructorDeclaration constructorDeclaration =
            codegen.constructorDecl(Constants.JavaTypes.REQUEST_RESPONSE, codegen.paramList(requestArg, responseArg),
                                   exceptionList);
        List<CodeStatement> statementList = new ArrayList<CodeStatement>();

        // if there are any parameters from the URL path, then assign the URL path here.
        if (!queryParamMap.getParamSet(ParameterMap.ParamSource.URL_PATH).isEmpty()) {
            statementList.add(assignRequestPath(codegen, httpRequestVarName));
        }

        // method calls and assignment statements to validate and parse the parameters.
        // nullable parameters are defined as optional
        // TODO: sort out handling nullable entries later.
        // NOTE: query parameters by definition are scalars, even if they are in a SELECT IN clause.
        Variable writerVar = codegen.var(Constants.Variables.WRITER);
        statementList.add(servletWriterCall(codegen, responseVar, writerVar, Constants.Encoding.UTF_8));
        ParseArgsServlet parseArgs = new ParseArgsServlet();
        for (String paramName : queryParamMap.getParamNames()) {
            MethodCall getStringArgCall = getParamCall(codegen, httpRequestVarName, paramName, queryParamMap.get(paramName));
            statementList.add(convertAndAssignParameter(codegen, queryParamMap, paramName, getStringArgCall, parseArgs));
            statementList.add(validateParameter(codegen, getStringArgCall, paramName, queryParamMap.get(paramName), parseArgs));
        }

        // only parse the incoming JSON if it exists.
        statementList.add(createGson(codegen));
        if (createObjects.getJsonRequestTree() != null) {
            Variable readerVar = codegen.var(Constants.Variables.READER);
            statementList.add(servletReaderCall(codegen, requestVar, readerVar, Constants.Encoding.UTF_8));

            // List<inputType> input = gson.fromJson(reader, listType);
            statementList.addAll(parseInput(codegen, Constants.Variables.GSON, readerVar, mergedParamMap));
        }
        return codegen.constructorDef(constructorDeclaration, codegen.stmtList(statementList));
    }

    /**
     * command-line constructor takes request and response arguments. Errors are written to the response stream
     * @param cmdLineArgsVarName command line args variable name.
     * @param queryParamMap parameters from URL and headers.
     * @param mergedParamMap parameters from JDBC Named Parameters
     * @return {@code RequestResponse(HttpServletRequest <httpRequestVarName>, HttpServletResponse <httpResponseVarName>)
     *               throws IOException, UnsupportedEncodingException {
     * ... assign and validate parameters ...
     * ... parse request payload ...
     * }
     * }
     * @throws NameNotFoundException variable not found.
     * @throws TypeNotFoundException type not recognized.
     * @throws UnsupportedParamTypeException parameter type not supported.
     */
    private ConstructorDefinition commandLineConstructor(CodeGen codegen,
                                                         String cmdLineArgsVarName,
                                                         ParameterMap queryParamMap,
                                                         ParameterMap mergedParamMap)
    throws NameNotFoundException, TypeNotFoundException, UnsupportedParamTypeException {
        Variable cmdLineArgsVar = codegen.var(cmdLineArgsVarName);
        ParameterDeclaration constructorArg =
            codegen.paramDecl(codegen.arrayType(codegen.simpleType(Constants.JavaTypes.STRING)), cmdLineArgsVar);
        TypeList exceptionList = codegen.typeList(codegen.classType(Constants.JavaExceptions.IO_EXCEPTION),
                                                  codegen.classType(Constants.JavaExceptions.UNSUPPORTED_ENCODING_EXCEPTION));
        ConstructorDeclaration constructorDecl =
            codegen.constructorDecl(Constants.JavaTypes.REQUEST_RESPONSE, codegen.paramList(constructorArg), exceptionList);
        List<CodeStatement> statementList = new ArrayList<CodeStatement>();

        // method calls and assignment statements to validate and parse the parameters.
        // first argument is properties file, so we start from 1
        int iArg = 1;
        // BufferedReader br = new BufferedReader(new FileReader(new File(args[1])));
        // Type listType = new TypeToken<ArrayList<${CLASSNAME_IN}>>(){}.getType();
        // List<${type}> input = gson.fromJson(br, listType);

        // only parse the incoming JSON if it exists.
        statementList.add(createGson(codegen));
        if (createObjects.getJsonRequestTree() != null) {
            Variable readerVar = codegen.var(Constants.Variables.READER);
            CodeExpression argsVar = codegen.arrayRef(codegen.var(cmdLineArgsVarName), codegen.integerConst(iArg++));
            statementList.add(commandLineReaderCall(codegen, readerVar, argsVar));

            // List<inputType> input = gson.fromJson(reader, listType);
            statementList.addAll(parseInput(codegen, Constants.Variables.GSON, readerVar, mergedParamMap));
        }
        ParseArgs parseArgs = new ParseArgsCommandLine();
        for (String paramName : queryParamMap.getParamNames()) {
            CodeExpression argExpr = codegen.arrayRef(codegen.var(cmdLineArgsVarName), codegen.integerConst(iArg++));
            statementList.add(convertAndAssignParameter(codegen, queryParamMap, paramName, argExpr, parseArgs));
            statementList.add(validateParameter(codegen, argExpr, paramName, queryParamMap.get(paramName), parseArgs));
        }
        return codegen.constructorDef(constructorDecl, codegen.stmtList(statementList));
    }

    /**
     * Generate the member declarations:
     * gson: GSON parser
     * input: request tree type if not null
     * output: response tree type if not null
     * parameters and types from query parameters.
     * @param mergedParamMap
     * @return
     * @throws TypeNotFoundException
     * @throws NameNotFoundException
     */
    private List<MemberDeclaration> memberDeclarations(CodeGen codegen,
                                                       ParameterMap mergedParamMap)
    throws TypeNotFoundException, NameNotFoundException {

        // member declarations
        List<MemberDeclaration> memberDeclarationList = new ArrayList<MemberDeclaration>();
        memberDeclarationList.add(codegen.memberDecl(codegen.classType(Constants.JavaTypes.GSON),
                                                     codegen.var(Constants.Variables.GSON)));
        ParameterMap paramMap = createObjects.getQueryParams();
        if (createObjects.getJsonRequestTree() != null) {
            Type requestListType = codegen.listType(createObjects.getRequestType(codegen, mergedParamMap));
            memberDeclarationList.add(codegen.memberDecl(requestListType, codegen.var(Constants.Variables.INPUT)));
        }
        if (createObjects.getJsonResponseTree() != null) {

            // if there are multiple selects, the top level object contains the lists of their results.
            memberDeclarationList.add(codegen.memberDecl(createObjects.getResponseType(codegen),
                                                         codegen.var(Constants.Variables.OUTPUT)));
        }
        paramMap.getParamNames()
            .forEach(CheckedConsumer.wrap(paramName -> memberDeclarationList.add(codegen.memberDecl(paramMap.getJavaType(codegen, paramName),
                                                                                                    codegen.var(paramName)))));
        return memberDeclarationList;
    }

    /**
     * Write accessors and setters for fields generated above.
     * @return
     * @throws TypeNotFoundException
     * @throws NameNotFoundException
     */
    public List<FunctionDefinition> methodDefinitions(CodeGen codegen, List<MemberDeclaration> memberDeclarationList) {
        List<FunctionDefinition> methods = CodegenUtil.accessorsAndSetters(codegen, memberDeclarationList);
        // add the output method if there's output to be output.
        if (getCreateObjects().getJsonResponseTree() != null) {
            methods.add(outputMethod(codegen));
        }
        return methods;
    }

    /**
     * Generate:
     * {@code  void writeOutput(OutputStreamWriter writer) throws IOException {
     *      gson.toJson(output, writer);
     *      writer.flush();
     * }
     * @return
     */
    private FunctionDefinition outputMethod(CodeGen codegen) {
        ParameterDeclaration arg = codegen.paramDecl(codegen.classType(Constants.JavaTypes.WRITER),
                                                     codegen.var(Constants.Variables.WRITER));
        FunctionDeclaration decl = codegen.functionDecl(Constants.Functions.WRITE_OUTPUT,
                                                        codegen.paramList(arg),
                                                        codegen.simpleType(Constants.JavaTypes.VOID),
                                                        codegen.typeList(codegen.classType(Constants.JavaExceptions.IO_EXCEPTION)));
        MethodCall toJsonCall = codegen.methodCall(codegen.var(Constants.Variables.GSON),
                                                   Constants.Functions.TO_JSON,
                                                   codegen.var(Constants.Variables.OUTPUT),
                                                   codegen.var(Constants.Variables.WRITER));
        MethodCall flushCall = codegen.methodCall(codegen.var(Constants.Variables.WRITER), Constants.Functions.FLUSH);
        StatementList statements = codegen.stmtList(codegen.methodCallStmt(toJsonCall),
                                                    codegen.methodCallStmt(flushCall));
        return codegen.functionDef(decl, statements);
    }

    private AssignmentStatement assignRequestPath(CodeGen codegen, String httpRequestVarName) {
        return codegen.typedAssignStmt(codegen.simpleType(Constants.JavaTypes.STRING),
                                       codegen.var(Constants.Variables.PATH),
                                       codegen.methodCall(codegen.var(httpRequestVarName),
                                                          Constants.ServletFunctions.GET_REQUEST_URI));
    }

    /**
     * Generate the method call to extract a parameter from the URL path, query, or headers.
     * @param codegen
     * @param httpRequestVarName
     * @param paramName
     * @param paramData
     * @return
     * @throws UnsupportedParamTypeException
     */
    private MethodCall getParamCall(CodeGen codegen,
                                    String httpRequestVarName,
                                    String paramName,
                                    ParameterMap.ParamData paramData) throws UnsupportedParamTypeException {
        // NOTE: due to the servlet-mapping mechanism, path variables (which people shouldn't use anyway)
        // can only be appended to the path.
        if (paramData.getSource().equals(ParameterMap.ParamSource.URL_PATH)) {
            int pathIndex = PathUtil.pathElementIndex(Constants.Variables.PATH, '/', StringUtil.decorateKey(paramName));

            // TODO: generate a constant rather than just an index for the path element.
            // TODO: THESE MUST ADD STRING CONVERSIONS TO THEIR ACTUAL TYPES.
            // ServletUtils.getDelimElement(String s, char delim, int index)
            return codegen.methodCall(codegen.var(Constants.ServletClasses.SERVLET_STRING_UTIL),
                                      Constants.ServletFunctions.GET_DELIM_ELEMENT,
                                      codegen.var(Constants.Variables.PATH),
                                      codegen.stringConst(Constants.URL.PATH_DELIM),
                                      codegen.integerConst(pathIndex));
        } else if (paramData.getSource().equals(ParameterMap.ParamSource.URL_QUERY)) {
            return codegen.methodCall(codegen.var(httpRequestVarName),
                                      Constants.ServletFunctions.GET_PARAMETER,
                                      codegen.stringConst(paramName));
        } else if (paramData.getSource().equals(ParameterMap.ParamSource.HTTP_HEADER)) {
            MethodCall getHeaderCall = codegen.methodCall(codegen.var(httpRequestVarName),
                                                          Constants.ServletFunctions.GET_HEADER,
                                                          codegen.stringConst(paramData.getTag()));
            // if header is formatted, then:
            // extractValue(String format, String var, request.getHeader(paramData.getTag())
            // otherwise it's just request.getHeader(paramData.getTag())
            if (paramData.getVarFormat() != null) {
                return codegen.methodCall(codegen.var(Constants.Packages.STRING_UTILS),
                                          Constants.ServletFunctions.EXTRACT_VALUE,
                                          codegen.stringConst(paramData.getVarFormat()),
                                          codegen.stringConst(paramData.getTag()),
                                          getHeaderCall);
            } else {
                return getHeaderCall;
            }
        } else {
            throw new UnsupportedParamTypeException(paramName + " has unsupported type " + paramData.getSource().name());
        }
    }

    /**
     * Test if the parameter is null it it's required, inverted from NOT NULL in CREATE TABLE.
     * I don't think that ParamData.isNullable is propogated correctly.
     * @param getParamExpr URL path, query param or HTTP header Variable.
     * @param paramName for error messaging.
     * @param paramData for default values if required, and error messaging.
     * @return generated null test.
     */
    public CodeStatement validateParameter(CodeGen codegen,
                                           CodeExpression getParamExpr,
                                           String paramName,
                                           ParameterMap.ParamData paramData,
                                           ParseArgs parseArgs)
            throws UnsupportedParamTypeException {
        if (paramData.isOptional()) {

            // This is serious cheating here.  String values need to be quoted, but numeric values are being
            // converted from a string back into a string, so they might as well be transferred literally.
            CodeExpression defaultValueExpression;
            if (ColDataTypeUtils.isStringType(paramData.getDataType())) {
                defaultValueExpression = codegen.stringConst(paramData.getDefaultValue());
            } else if (ColDataTypeUtils.isNumericType(paramData.getDataType())) {
                defaultValueExpression = codegen.literalConst(paramData.getDefaultValue());
            } else {
                throw new UnsupportedParamTypeException("parameter " + paramName + " of type " + paramData.getDataType().getDataType() +
                                                        ": only string and numeric parameters support default values");
            }
            return codegen.assignStmt(codegen.var(paramName), defaultValueExpression);
        } else {
            // TODO: Nullable parameters are by default optional, and must be explicitly required.
            CodeExpression nullTestExpr = codegen.binaryExpr(Constants.JavaOperators.EQUALS, getParamExpr, codegen.nullExpr());
            String errorMessage = "Required parameter " + paramName + " was not specified in " + paramData.getSource().name();
            MethodCallStatement writeError = parseArgs.writeError(codegen, errorMessage, null);
            return codegen.ifStmt(nullTestExpr, writeError);
        }
    }

    /**
     * Assign a parameter pulled from the URL path or query params, or HTTP header to the value
     * passed to NamedParameterStatement.
     * @param parameterMap map of parameter names to parameter types
     * @param paramName parameter name
     * @param getParamExpr call to get the parameter.
     * @param parseArgs servlet/command-line error messaging
     * @return
     * @throws TypeNotFoundException
     * @throws NameNotFoundException
     */
    private TryCatch convertAndAssignParameter(CodeGen codegen,
                                               ParameterMap parameterMap,
                                               String paramName,
                                               CodeExpression getParamExpr,
                                               ParseArgs parseArgs)
    throws TypeNotFoundException, NameNotFoundException {
        String paramType = parameterMap.getColDataType(paramName).getDataType();

        // parameters parsed from query and header parameters are scalar by definition.
        CodeExpression conversionExpr = SQLType.convertFromString(codegen, paramType, false, getParamExpr);
        AssignmentStatement element = codegen.assignStmt(codegen.var(paramName), conversionExpr);
        String errorMessage = "parameter " + paramName  + " is of type " + paramType + " and was passed ";
        MethodCallStatement writeErrorMessage = parseArgs.writeError(codegen, errorMessage, getParamExpr);
        return codegen.tryCatch(codegen.stmtList(element),
                                codegen.classType(Constants.JavaTypes.EXCEPTION),
                                codegen.var(Constants.ServletVariables.EX),
                                codegen.stmtList(writeErrorMessage, codegen.returnStmt()));
    }

    public CreateObjects getCreateObjects() {
        return createObjects;
    }
}
