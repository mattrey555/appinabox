package com.apptechnik.appinabox.engine.codegen.params;

import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeGen;
import com.apptechnik.appinabox.engine.codegen.pojos.MethodCallStatement;

public interface ParseArgs {
    MethodCallStatement writeError(CodeGen codegen, String errorString, CodeExpression expr);
}
