package com.apptechnik.appinabox.engine.codegen.params;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeGen;
import com.apptechnik.appinabox.engine.codegen.pojos.MethodCallStatement;

public class ParseArgsCommandLine implements ParseArgs {

    /**
     * Write an error message to the standard output in JSON form.
     * @param errorString error message.
     * @param expr expression which caused error.
     * @return method call to write the error to the servlet output writer.
     */
    public MethodCallStatement writeError(CodeGen codegen, String errorString, CodeExpression expr) {
        CodeExpression errorMessage = codegen.stringConst(errorString);
        if (expr != null) {
            errorMessage = codegen.binaryExpr(Constants.JavaOperators.PLUS, errorMessage, expr);
        }
        return codegen.methodCallStmt(codegen.methodCall(Constants.Functions.SYSTEM_ERR_PRINTLN, errorMessage));
    }
}
