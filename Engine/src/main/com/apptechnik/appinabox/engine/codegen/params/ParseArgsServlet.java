package com.apptechnik.appinabox.engine.codegen.params;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeGen;
import com.apptechnik.appinabox.engine.codegen.pojos.MethodCallStatement;

public class ParseArgsServlet implements ParseArgs {

    /**
     * Write an error message to the servlet output in JSON form.
     * @param errorString error message.
     * @param expr expression which caused error.
     * @return method call to write the error to the servlet output writer.
     */
    public MethodCallStatement writeError(CodeGen codegen, String errorString, CodeExpression expr) {
        CodeExpression errorMessage = codegen.stringConst(errorString);
        if (expr != null) {
            errorMessage = codegen.binaryExpr(Constants.JavaOperators.PLUS, errorMessage, expr);
        }
        CodeExpression badRequestCode =
                codegen.dot(codegen.var(Constants.JavaTypes.SERVLET_RESPONSE),
                            codegen.var(Constants.ServletErrorCodes.SC_BAD_REQUEST));
        return codegen.methodCallStmt(codegen.var(Constants.ServletVariables.SERVLET_RESPONSE),
                                      Constants.ServletFunctions.SEND_ERROR,
                                      badRequestCode, errorMessage);
    }

}
