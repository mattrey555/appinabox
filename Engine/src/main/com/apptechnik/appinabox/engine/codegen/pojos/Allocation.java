package com.apptechnik.appinabox.engine.codegen.pojos;


public abstract class Allocation implements CodeExpression {
    protected final Type type;
    protected final ArgumentList constructorArgs;
    protected final StatementList inlineStatements;

    public Allocation(Type type, ArgumentList constructorArgs, StatementList inlineStatements) {
        this.type = type;
        this.constructorArgs = constructorArgs;
        this.inlineStatements = inlineStatements;
    }

    public Type getType() {
        return type;
    }

    public ArgumentList getConstructorArgs() {
        return constructorArgs;
    }

    @Override
    public abstract String toString();
}
