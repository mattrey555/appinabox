package com.apptechnik.appinabox.engine.codegen.pojos;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.List;

/**
 * NOTE: ArgumentList is incorrect.  Annotations use a different syntax than methods.
 */
public class Annotation implements CodeStatement {
    protected final String name;
    protected final List<AnnotationArg>  args;

    public Annotation(String name) {
        this.name = name;
        this.args = null;
    }

    public Annotation(String name, List<AnnotationArg> args) {
        this.name = name;
        this.args = args;
    }

    public String getName() {
        return name;
    }

    public List<AnnotationArg>  getArgs() {
        return args;
    }

    public boolean requiresDelimiter() {
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("@" + name);
        if ((args != null) && !args.isEmpty()) {
            if ((args.size() == 1) && args.get(0).getName().equals(Constants.JavaKeywords.VALUE))
            sb.append("(" + StringUtil.concat(args, ",") + ")");
        }
        return sb.toString();
    }
}
