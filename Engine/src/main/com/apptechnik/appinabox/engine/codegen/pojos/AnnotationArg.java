package com.apptechnik.appinabox.engine.codegen.pojos;

import com.apptechnik.appinabox.engine.Constants;

public class AnnotationArg {
    private final String name;
    private final CodeExpression argument;

    public AnnotationArg(String name, CodeExpression argument) {
        this.name = name;
        this.argument = argument;
    }

    public String getName() {
        return name;
    }

    public CodeExpression getArgument() {
        return argument;
    }

    // syntax is the same in Kotlin as in Java (name = arg)
    @Override
    public String toString() {
        if (name.equals(Constants.JavaKeywords.VALUE)) {
            return argument.toString();
        } else {
            return name + " = " + argument;
        }
    }
}
