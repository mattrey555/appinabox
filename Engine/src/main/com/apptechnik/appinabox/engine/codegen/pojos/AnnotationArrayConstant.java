package com.apptechnik.appinabox.engine.codegen.pojos;

import java.util.List;

public abstract class AnnotationArrayConstant implements Constant, CodeExpression {
    protected final List<Constant> value;

    public AnnotationArrayConstant(List<Constant> value) {
        this.value = value;
    }

    @Override
    public abstract String toString();
}
