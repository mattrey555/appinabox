package com.apptechnik.appinabox.engine.codegen.pojos;

import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.List;

public class AnnotationList implements CodeExpression {
    protected List<Annotation> annotationList;

    public AnnotationList(List<Annotation> annotationList) {
        this.annotationList = annotationList;
    }

    public List<Annotation> getAnnotationList() {
        return annotationList;
    }

    @Override
    public String toString() {
        return StringUtil.delim(annotationList, "\n");
    }
}
