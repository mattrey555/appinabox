package com.apptechnik.appinabox.engine.codegen.pojos;

import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.Arrays;
import java.util.List;

public abstract class ArgumentList implements CodeExpression {
    protected final List<CodeExpression> argumentList;

    public ArgumentList(List<CodeExpression> argumentList) {
        this.argumentList = argumentList;
    }

    public List<CodeExpression> getArgumentList() {
        return argumentList;
    }

    @Override
    public abstract String toString();
}
