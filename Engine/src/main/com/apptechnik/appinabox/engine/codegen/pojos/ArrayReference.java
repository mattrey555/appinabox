package com.apptechnik.appinabox.engine.codegen.pojos;

import java.util.Arrays;
import java.util.List;

public abstract class ArrayReference implements CodeExpression {
    protected final CodeExpression expr;
    protected final List<CodeExpression> indexList;

    public ArrayReference(CodeExpression expr, List<CodeExpression> indexList) {
        this.expr = expr;
        this.indexList = indexList;
    }

    public ArrayReference(CodeExpression expr, CodeExpression... index) {
        this(expr, Arrays.asList(index));
    }

    public CodeExpression getExpr() {
        return expr;
    }

    public List<CodeExpression> getIndexList() {
        return indexList;
    }

    public abstract String toString();
}
