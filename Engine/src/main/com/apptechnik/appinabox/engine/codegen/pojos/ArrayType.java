package com.apptechnik.appinabox.engine.codegen.pojos;

import com.apptechnik.appinabox.library.util.StringUtils;

public class ArrayType implements Type {
    protected final Type item;
    protected final int dimensions;
    protected final boolean isNullable;

    public ArrayType(Type item, int dimensions, boolean isNullable) {
        this.item = item;
        this.dimensions = dimensions;
        this.isNullable = isNullable;
    }

    public ArrayType(Type item) {
        this(item, 1, false);
    }

    public String getName() {
        return item.getName();
    }

    public Type getItem() {
        return item;
    }

    public int getDimensions() {
        return dimensions;
    }

    @Override
    public boolean isNullable() {
        return isNullable;
    }

    @Override
    public String toString() {
        return item + StringUtils.repeat("[]", "", dimensions);
    }
}
