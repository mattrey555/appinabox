package com.apptechnik.appinabox.engine.codegen.pojos;

public abstract class AssignmentStatement implements CodeStatement {
    protected CodeExpression lval;
    protected CodeExpression expression;

    public AssignmentStatement(CodeExpression lval, CodeExpression expression) {
        this.lval = lval;
        this.expression = expression;
    }

    public CodeExpression getLVal() {
        return lval;
    }

    public CodeExpression getExpression() {
        return expression;
    }

    public abstract boolean requiresDelimiter();

    public abstract String toString();
}
