package com.apptechnik.appinabox.engine.codegen.pojos;

import com.apptechnik.appinabox.engine.exceptions.TypeNotFoundException;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public abstract class BaseCodeGen {
    public abstract Allocation alloc(Type type, ArgumentList constructorArgs, StatementList inlineStatements);

    public abstract ArgumentList argList(List<CodeExpression> argumentList);

    public abstract ArrayReference arrayRef(CodeExpression expr, List<CodeExpression> indexList);

    public abstract ArrayType arrayType(Type item, int dimensions, boolean isNullable);

    public abstract AssignmentStatement assignStmt(CodeExpression lval, CodeExpression expression);

    public abstract BooleanConstant booleanConst(boolean value);

    public abstract CastExpression castExpr(Type type, CodeExpression expr);

    public abstract Annotation classAnnot(String name);

    public abstract InterfaceDeclaration interfaceDecl(String interfaceName,
                                                       TypeList genericsList,
                                                       TypeList implementsList,
                                                       List<FunctionDeclaration> methodList,
                                                       List<Annotation> annotationList);

    public abstract InterfaceFile interfaceFile(PackageStatement packageStatement,
                                                StatementList importStatements,
                                                InterfaceDeclaration interfaceDeclaration);


    public abstract ClassDeclaration classDecl(boolean isStatic,
                                               boolean isFinal,
                                               boolean isData,
                                               String className,
                                               TypeList genericsList,
                                               Type extendsType,
                                               TypeList implementsList,
                                               List<Annotation> annotationList);

    public abstract ClassDefinition classDef(ClassDeclaration classDeclaration,
                                             List<MemberDeclaration> memberDeclarationList,
                                             List<ConstructorDefinition> constructorDefinitionList,
                                             List<FunctionDefinition> functionDefinitionList,
                                             List<ClassDefinition> innerClassList,
                                             List<InterfaceDeclaration> innerInterfaceList);


    public abstract ClassFile classFile(PackageStatement packageStatement,
                                    StatementList importStatements,
                                    ClassDefinition classDefinition);

    public abstract CodeBinaryExpression binaryExpr(String operator, CodeExpression left, CodeExpression right);

    public abstract CodeExpression parenExpr(CodeExpression expr);

    public abstract ConstructorDeclaration constructorDecl(String qualifier,
                                                           String name,
                                                           ParameterList parameterList,
                                                           TypeList exceptionList);

    public abstract ConstructorDefinition constructorDef(ConstructorDeclaration declaration, StatementList statementList);

    public abstract ExtendsType extendsType(String name, Type type, boolean isNullable);

    public abstract ForIteration forIter(Type itemType, Variable itemVar, Variable collection);

    public abstract ForLoop forLoop(ForIteration iteration, StatementList statementList);

    public abstract AnnotationList methodAnnotList(List<Annotation> methodAnnotationList);

    public abstract FunctionDeclaration functionDecl(boolean override,
                                                     boolean suspend,
                                                     String accessModifier,
                                                     boolean isStatic,
                                                     String name,
                                                     TypeList genericsList,
                                                     ParameterList parameterList,
                                                     Type returnType,
                                                     TypeList exceptionTypeList,
                                                     AnnotationList annotationList);

    public abstract FunctionDefinition functionDef(FunctionDeclaration functionDeclaration, StatementList statementlist);

    public abstract GenericType genericType(Type container, List<Type> itemList, boolean isNullable);

    public abstract GenericType listType(Type elementType);

    public abstract GenericType arrayListType(Type elementType);

    public abstract IfStatement ifStmt(CodeExpression condition, StatementList ifStatementList, StatementList elseStatementList);

    public abstract ImportStatement importStmt(String packageName);

    public abstract StatementList importStatementList(String resourceName) throws IOException;

    public  List<CodeStatement> importStatements(List<String> packageNames) {
        return packageNames.stream().map(p -> importStmt(p)).collect(Collectors.toList());
    }


    public abstract IntegerConstant integerConst(int value);

    public abstract LambdaExpression lambdaExpr(Variable var, CodeExpression expr);

    public abstract LiteralConstant literalConst(String name);

    /**
     * NOTE: Array constant syntax is context dependent:
     * Code:
     * { 1, 2, 3, 4 }
     * Annotation:
     * [ 1, 2, 3, 4 ]
     * and we don't use array constants in generated code yet, hence the special-case naming.
     * @param constantList
     * @return
     */
    public abstract AnnotationArrayConstant annotationArrayConstant(List<Constant> constantList);

    public abstract MemberDeclaration memberDecl(String accessModifier,
                                                 boolean isLateinit,
                                                 boolean isStatic,
                                                 boolean isConstant,
                                                 Type type,
                                                 Variable variable,
                                                 List<Annotation> annotationList,
                                                 String byProvider,
                                                 CodeExpression assignedValue);

    public abstract Annotation annotation(String name, List<AnnotationArg> args);

    public abstract MethodCall methodCall(CodeExpression caller, String methodName, ArgumentList argList);

    public abstract MethodCallStatement methodCallStmt(MethodCall call);

    public abstract NotExpression notExpr(CodeExpression codeExpression);

    public abstract PackageStatement packageStmt(String name);

    public abstract AnnotationArg annotationArg(String name, CodeExpression value);

    public abstract Annotation paramAnnot(String name, List<AnnotationArg> argumentList);

    public abstract ParameterDeclaration paramDecl(Type type, Variable variable, List<Annotation> annotationList);

    public abstract ParameterList paramList(List<ParameterDeclaration> parameterDeclarationList);

    public abstract ReturnStatement returnStmt(CodeExpression expression);

    public abstract ThrowStatement throwStmt(CodeExpression expression);

    public abstract boolean isSimpleType(Type type);

    public abstract SimpleType simpleType(String name, boolean isNullable);

    public abstract ClassType classType(String name, boolean isNullable);

    public abstract TypeVariable typeVariable(String name);

    public abstract <T extends CodeStatement> StatementList<T> stmtList(List<T> statementList);

    public abstract StringConstant stringConst(String name);

    public abstract TryCatch tryCatch(StatementList bodyList, Type exceptionType, Variable exceptionName, StatementList exceptionBodyList);

    public abstract TypeList typeList(List<Type> typeList);

    public abstract TypedAssignmentStatement typedAssignStmt(Type type, CodeExpression lval, CodeExpression expr);

    public abstract Variable var(String name);

    public abstract CodeExpression nullExpr();

    public abstract VariableDeclaration varDecl(Type type, Variable variable);

    public abstract WhileLoop whileLoop(CodeExpression condition,
                                        StatementList statementList);

    public abstract Type typeFromSQLType(String typeName, boolean isNullable) throws TypeNotFoundException;

    public abstract NullSafe nullSafe(CodeExpression codeExpr);

    public abstract LambdaClass lambdaClass(Type implementedType, List<FunctionDefinition> functionDefinitionList);

    public abstract CodeExpression defaultValue(Type type);
}
