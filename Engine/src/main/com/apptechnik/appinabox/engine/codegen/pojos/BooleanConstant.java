package com.apptechnik.appinabox.engine.codegen.pojos;

public abstract class BooleanConstant implements Constant, CodeExpression {
    protected final boolean value;

    public BooleanConstant(boolean value) {
        this.value = value;
    }

    public abstract String toString();
}
