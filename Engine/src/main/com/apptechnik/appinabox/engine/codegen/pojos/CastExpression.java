package com.apptechnik.appinabox.engine.codegen.pojos;

public abstract class CastExpression implements CodeExpression {
    protected final Type type;
    protected final CodeExpression expr;

    public CastExpression(Type type, CodeExpression expr) {
        this.type = type;
        this.expr = expr;
    }

    public Type getType() {
        return type;
    }

    public CodeExpression getExpr() {
        return expr;
    }

    public abstract String toString();
}
