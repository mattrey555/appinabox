package com.apptechnik.appinabox.engine.codegen.pojos;

import com.apptechnik.appinabox.engine.Constants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Generate an expression of the form:
 * {@code public class <className>[<generics-list>] [extends <extends-list>] [implements <implements-list>] }
 */
public abstract class ClassDeclaration implements Definition {
    protected final boolean isStatic;                       // static class
    protected final boolean isFinal;                        // final/open
    protected final boolean isData;                         // kotlin-specific: data class
    protected final String accessQualifier;                 // public, protected, private
    protected final String className;
    protected final TypeList genericsList;                  // class generic types <T extends Foo, S extends Bar>
    protected Type extendsType;                             // remapped in mixin: deliberately NOT final
    protected final TypeList implementsList;                // interface definitions implemented.
    protected List<Annotation> annotationList;              // class prefix annotations

    public ClassDeclaration(String className,
                            TypeList genericsList,
                            Type extendsType,
                            TypeList implementsList,
                            List<Annotation> annotationList) {
        this.isStatic = false;
        this.isFinal = false;
        this.isData = false;
        this.accessQualifier = Constants.JavaKeywords.PUBLIC;
        this.className = className;
        this.extendsType = extendsType;
        this.implementsList = implementsList;
        this.genericsList = genericsList;
        this.annotationList = annotationList;
    }

    public ClassDeclaration(boolean isStatic,
                            boolean isFinal,
                            boolean isData,
                            String className,
                            TypeList genericsList,
                            Type extendsType,
                            TypeList implementsList,
                            List<Annotation> annotationList) {
        this.isStatic = isStatic;
        this.isFinal = isFinal;
        this.isData = isData;
        this.accessQualifier = Constants.JavaKeywords.PUBLIC;
        this.className = className;
        this.extendsType = extendsType;
        this.implementsList = implementsList;
        this.genericsList = genericsList;
        this.annotationList = annotationList;
    }

    public boolean isStatic() {
        return isStatic;
    }

    public String getAccessQualifier() {
        return accessQualifier;
    }

    public String getClassName() {
        return className;
    }

    public Type getExtendsType() {
        return extendsType;
    }

    public void setExtendsType(Type extendsType) {
        this.extendsType = extendsType;
    }

    public TypeList getGenericsList() {
        return genericsList;
    }

    public TypeList getImplementsList() {
        return implementsList;
    }

    public boolean isFinal() {
        return isFinal;
    }

    public boolean isData() {
        return isData;
    }

    public List<Annotation> getAnnotationList() {
        return annotationList;
    }

    public void addAnnotation(Annotation annotation) {
        if (annotationList == null) {
            annotationList = new ArrayList<>();
        }
        annotationList.add(annotation);
    }

    @Override
    public abstract String toString();

    public abstract String toString(List<MemberDeclaration> memberDeclarationList);
}