package com.apptechnik.appinabox.engine.codegen.pojos;

import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.List;

/**
 * A class definition is:
 * {@code
 * <classDeclaration> {
 * <memberDeclarationList>
 * <constructorDefinitionList>
 * <methodDefinitionList>
 * }}
 */
public abstract class ClassDefinition implements Definition {
    protected final ClassDeclaration classDeclaration;
    protected final List<MemberDeclaration> memberDeclarationList;
    protected final List<ConstructorDefinition> constructorDefinitionList;
    protected final List<FunctionDefinition> methodDefinitionList;
    protected final List<ClassDefinition> innerClassList;
    protected final List<InterfaceDeclaration> innerInterfaceList;

    public ClassDefinition(ClassDeclaration classDeclaration,
                           List<MemberDeclaration> memberDeclarationList,
                           List<ConstructorDefinition> constructorDefinitionList,
                           List<FunctionDefinition> functionDefinitionList,
                           List<ClassDefinition> innerClassList,
                           List<InterfaceDeclaration> innerInterfaceList) {
        this.classDeclaration = classDeclaration;
        this.memberDeclarationList = memberDeclarationList;
        this.constructorDefinitionList = constructorDefinitionList;
        this.methodDefinitionList = functionDefinitionList;
        this.innerClassList = innerClassList;
        this.innerInterfaceList = innerInterfaceList;
    }

    public ClassDeclaration getClassDeclaration() {
        return classDeclaration;
    }

    public List<MemberDeclaration> getMemberDeclarationList() {
        return memberDeclarationList;
    }

    public List<ConstructorDefinition> getConstructorDefinitionList() {
        return constructorDefinitionList;
    }

    public List<FunctionDefinition> getMethodDefinitionList() {
        return methodDefinitionList;
    }

    public List<ClassDefinition> getInnerClassList() {
        return innerClassList;
    }

    public List<InterfaceDeclaration> getInnerInterfaceList() {
        return innerInterfaceList;
    }

    @Override
    public abstract String toString();
}
