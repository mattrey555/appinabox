package com.apptechnik.appinabox.engine.codegen.pojos;

import com.apptechnik.appinabox.engine.codegen.objects.CreateCommon;
import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;
import com.apptechnik.appinabox.engine.util.IndentingFileWriter;

import java.io.File;
import java.io.IOException;

public abstract class ClassFile {
    protected final PackageStatement packageStatement;
    protected final StatementList importStatements;
    protected ClassDefinition classDefinition;            // replaced in rare cases.

    public ClassFile(PackageStatement packageStatement,
                     StatementList importStatements,
                     ClassDefinition classDefinition) {
        this.packageStatement = packageStatement;
        this.importStatements = importStatements;
        this.classDefinition = classDefinition;
    }

    public PackageStatement getPackageStatement() {
        return packageStatement;
    }

    public StatementList getImportStatements() {
        return importStatements;
    }

    public String createImport() {
        return getPackageStatement().getName() + "." + getClassDefinition().getClassDeclaration().getClassName();
    }

    public ClassDefinition getClassDefinition() {
        return classDefinition;
    }

    public void setClassDefinition(ClassDefinition classDefinition) {
        this.classDefinition = classDefinition;
    }

    @Override
    public abstract String toString();

    public abstract void write(File outputDir) throws Exception;
}
