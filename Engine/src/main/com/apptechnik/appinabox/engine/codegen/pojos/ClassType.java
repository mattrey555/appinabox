package com.apptechnik.appinabox.engine.codegen.pojos;

/**
 * Type for classes (i.e. not containers and not simple types)
 */
public class ClassType implements Type {
    protected final String name;
    protected final boolean isNullable;

    public ClassType() {
        this.name = null;
        this.isNullable = false;
    }

    public ClassType(String name) {
        this.name = name;
        this.isNullable = false;
    }

    public ClassType(String name, boolean isNullable) {
        this.name = name;
        this.isNullable = isNullable;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean isNullable() {
        return isNullable;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof ClassType) {
            return ((ClassType) o).getName().equals(getName());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
