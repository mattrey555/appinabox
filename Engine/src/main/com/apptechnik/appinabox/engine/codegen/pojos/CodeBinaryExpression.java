package com.apptechnik.appinabox.engine.codegen.pojos;

/**
 * I'd call it Expression, but if conflicts with the SQL parser exception.
 */
public abstract class CodeBinaryExpression implements CodeExpression {
    protected final String operator;
    protected final CodeExpression left;
    protected final CodeExpression right;

    public CodeBinaryExpression(String operator, CodeExpression left, CodeExpression right) {
        this.operator = operator;
        this.left = left;
        this.right = right;
    }

    public String getOperator() {
        return operator;
    }

    public CodeExpression getLeft() {
        return left;
    }

    public CodeExpression getRight() {
        return right;
    }

    @Override
    public abstract String toString();
}
