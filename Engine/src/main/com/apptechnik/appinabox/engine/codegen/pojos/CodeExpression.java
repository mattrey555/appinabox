package com.apptechnik.appinabox.engine.codegen.pojos;

/**
 * I'd call it Expression, but if conflcits with the SQL parser exception.
 */
public interface CodeExpression {
    String toString();
}
