
package com.apptechnik.appinabox.engine.codegen.pojos;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.kotlin.KotlinParamList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * wrapper functions for the abstract methods (still abstract because we don't provide concrete implementations here)
 */
public abstract class CodeGen extends BaseCodeGen {
    public Allocation alloc(Type type, ArgumentList constructorArgs) {
        return alloc(type, constructorArgs, (StatementList) null);
    }

    public Allocation alloc(Type type, CodeExpression...args) {
        return alloc(type, argList(args), null);
    }

    public ArgumentList argList(CodeExpression... exprList) {
        return argList(Arrays.asList(exprList));
    }

    public ArrayReference arrayRef(CodeExpression expr, CodeExpression... index) {
        return arrayRef(expr, Arrays.asList(index));
    }

    public ArrayType arrayType(Type item) {
        return arrayType(item, 1, false);
    }

    public ArrayType arrayType(Type item, int dimensions) {
        return arrayType(item, dimensions, false);
    }

    public InterfaceDeclaration interfaceDecl(String interfaceName,
                                               TypeList genericsList,
                                               TypeList implementsList,
                                               List<FunctionDeclaration> methodList) {
        return interfaceDecl(interfaceName, genericsList, implementsList, methodList, null);
    }

    public InterfaceDeclaration interfaceDecl(String interfaceName,
                                              TypeList implementsList,
                                              List<FunctionDeclaration> methodList) {
        return interfaceDecl(interfaceName, null, implementsList, methodList, null);
    }

    public InterfaceDeclaration interfaceDecl(String interfaceName,  List<FunctionDeclaration> methodList) {
        return interfaceDecl(interfaceName, null, null, methodList, null);
    }


    public ClassDeclaration classDecl(String className,
                                       TypeList genericsList,
                                       Type extendsType,
                                       TypeList implementsList,
                                       List<Annotation> annotationList) {
        return classDecl(false, false, false, className, genericsList,
                         extendsType, implementsList, annotationList);
    }

    public ClassDeclaration classDecl(String className,
                                      TypeList genericsList,
                                      Type extendsType,
                                      TypeList implementsList) {
        return classDecl(className, genericsList, extendsType, implementsList, null);
    }

    public ClassDeclaration classDecl(String className,
                                      Type extendsType,
                                      TypeList implementsList) {
        return classDecl(className, null, extendsType, implementsList);
    }

    public ClassDeclaration classDecl(String className) {
        return classDecl(className, null, null);
    }

    public ClassDeclaration classDecl(String className, List<Annotation> annotationList) {
        return classDecl(className, null, null, null, annotationList);
    }

    public ClassDeclaration classDecl(String className, Annotation... annotations) {
        return classDecl(className, null, null, null, Arrays.asList(annotations));
    }

    public ClassDefinition classDef(ClassDeclaration classDeclaration,
                                     List<MemberDeclaration> memberDeclarationList,
                                     List<ConstructorDefinition> constructorDefinitionList,
                                     List<FunctionDefinition> functionDefinitionList) {
        return classDef(classDeclaration, memberDeclarationList, constructorDefinitionList, functionDefinitionList, null, null);
    }

    public ClassDefinition classDef(ClassDeclaration classDeclaration,
                                     List<MemberDeclaration> memberDeclarationList,
                                     List<ConstructorDefinition> constructorDefinitionList,
                                     List<FunctionDefinition> functionDefinitionList,
                                     List<ClassDefinition> innerClassList) {
        return classDef(classDeclaration, memberDeclarationList, constructorDefinitionList, functionDefinitionList, innerClassList, null);
    }

    public CodeBinaryExpression dot(CodeExpression left, CodeExpression right) {
        return binaryExpr(Constants.JavaOperators.DOT, left, right);
    }

    public CodeBinaryExpression at(CodeExpression left, CodeExpression right) {
        return binaryExpr(Constants.KotlinOperators.AT, left, right);
    }

    public CodeBinaryExpression dotNullable(CodeExpression left, CodeExpression right) {
        return binaryExpr(Constants.KotlinOperators.DOT_NULLABLE, left, right);
    }

    public ConstructorDeclaration constructorDecl(String name,
						  ParameterList parameterList,
						  TypeList exceptionList) {
        return constructorDecl(Constants.JavaKeywords.PUBLIC, name, parameterList, exceptionList);
    }

    public ConstructorDeclaration constructorDecl(String name,
						  ParameterList parameterList) {
        return constructorDecl(Constants.JavaKeywords.PUBLIC, name, parameterList, null);
    }

    public ConstructorDeclaration constructorDecl(String name) {
        return constructorDecl(Constants.JavaKeywords.PUBLIC, name, null, null);
    }

    public ExtendsType extendsType(String name, Type type) {
        return extendsType(name, type, false);
    }

    public FunctionDeclaration functionDecl(String accessModifier,
                                             boolean isStatic,
                                             String name,
                                             ParameterList parameterList,
                                             Type returnType,
                                             TypeList exceptionTypeList,
                                             AnnotationList annotationList) {
        return functionDecl(false, false, accessModifier, isStatic, name, null, parameterList, returnType, exceptionTypeList, annotationList);
    }

    public FunctionDeclaration functionDecl(String accessModifier,
                                             boolean isStatic,
                                             String name,
                                             ParameterList parameterList,
                                             Type returnType,
                                             TypeList exceptionTypeList) {
        return functionDecl(accessModifier, isStatic, name, parameterList, returnType, exceptionTypeList, null);
    }

    public FunctionDeclaration functionDecl(boolean isStatic,
                                            String name,
                                            ParameterList parameterList,
                                            Type returnType,
                                            TypeList exceptionTypeList) {
        return functionDecl(Constants.JavaKeywords.PUBLIC, isStatic, name, parameterList, returnType, exceptionTypeList);
    }

    public FunctionDeclaration functionDecl(String accessModifier,
                                             boolean isStatic,
                                             String name,
                                             ParameterList parameterList,
                                             Type returnType) {
        return functionDecl(accessModifier, isStatic, name, parameterList, returnType, null);
    }

    public FunctionDeclaration functionDecl(String name,
                                            boolean isStatic,
                                            ParameterList parameterList,
                                            Type returnType) {
        return functionDecl(Constants.JavaKeywords.PUBLIC, isStatic, name, parameterList, returnType, null);
    }

    public FunctionDeclaration functionDecl(String name,
                                            ParameterList parameterList,
                                            Type returnType) {
        return functionDecl(Constants.JavaKeywords.PUBLIC, false, name, parameterList, returnType, null);
    }
    public FunctionDeclaration functionDecl(String name,
                                            ParameterList parameterList,
                                            Type returnType,
                                            TypeList exceptionTypeList) {
        return functionDecl(Constants.JavaKeywords.PUBLIC, false, name, parameterList, returnType, exceptionTypeList);
    }

    public FunctionDefinition functionDef(String name,
                                          ParameterList parameterList,
                                          Type returnType,
                                          StatementList statementlist) {
        return functionDef(functionDecl(name, parameterList, returnType), statementlist);
    }

    public Type withNullable(Type type, boolean isNullable) {
        if (type instanceof GenericType) {
            GenericType genericType = (GenericType) type;
            return genericType(genericType.getContainer(), genericType.getItemList(), isNullable);
        } else if (type instanceof SimpleType){
            SimpleType simpleType = (SimpleType) type;
            return simpleType(simpleType.getName(), isNullable);
        } else if (type instanceof ArrayType) {
            ArrayType arrayType = (ArrayType) type;
            return arrayType(arrayType.item, arrayType.getDimensions(), isNullable);
        } else {
            return type;
        }
    }

    public GenericType genericType(Type container, List<Type> itemList) {
        return genericType(container, itemList, false);
    }

    public GenericType genericType(Type container, Type... itemList) {
	    return genericType(container, Arrays.asList(itemList));
    }

    public GenericType genericType(Type container) {
        return genericType(container, (List<Type>) null);
    }

    public IfStatement ifStmt(CodeExpression condition, StatementList ifStatementList) {
        return ifStmt(condition, ifStatementList, null);
    }

    public IfStatement ifStmt(CodeExpression condition, CodeStatement...ifStatements) {
        return ifStmt(condition, stmtList(ifStatements), null);
    }


    public  List<CodeStatement> importStatements(List<String> packageNames) {
        return packageNames.stream().map(p -> importStmt(p)).collect(Collectors.toList());
    }

    public MemberDeclaration memberDecl(String accessModifier,
                                        boolean isStatic,
                                        boolean isConstant,
                                        Type type,
                                        Variable variable,
                                        CodeExpression assignedValue) {
        return memberDecl(accessModifier, false, isStatic, isConstant, type, variable, null, null, assignedValue);
    }

    public MemberDeclaration memberDecl(String accessModifier,
                                         boolean isStatic,
                                         boolean isConstant,
                                         Type type,
                                         Variable variable,
                                         List<Annotation> annotationList) {
        return memberDecl(accessModifier, false, isStatic, isConstant, type, variable, annotationList, null, null);
    }

    public MemberDeclaration memberDecl(String accessModifier,
                                         boolean isLateinit,
                                         boolean isStatic,
                                         boolean isConstant,
                                         Type type,
                                         Variable variable,
                                         List<Annotation> annotationList) {
        return memberDecl(accessModifier, isLateinit, isStatic, isConstant, type, variable, annotationList);
    }

    public MemberDeclaration memberDecl(String accessModifier,
                                        boolean isStatic,
                                        boolean isConstant,
                                        Type type,
                                        Variable variable) {
        return memberDecl(accessModifier, false, isStatic, isConstant, type, variable, null, null, null);
    }

    public MemberDeclaration memberDecl(String accessModifier, boolean isStatic, Type type, Variable variable) {
        return memberDecl(accessModifier, false, isStatic, false, type, variable, null, null, null);
    }

    public MemberDeclaration memberDecl(String accessModifier, Type type, Variable variable) {
        return memberDecl(accessModifier, false, false, false, type, variable, null, null, null);
    }

    public MemberDeclaration memberDecl(Type type, Variable variable) {
        return memberDecl(Constants.JavaKeywords.PRIVATE, false, false, false, type, variable, null, null, null);
    }

    public MemberDeclaration memberDecl(Type type, Variable variable, CodeExpression assignedValue) {
        return memberDecl(Constants.JavaKeywords.PRIVATE, false, false, false, type, variable, null, null, assignedValue);
    }

    public MemberDeclaration memberDecl(MemberDeclaration d) {
        return memberDecl(d.getAccessModifier(), d.isStatic(), d.getType(), d.getVariable());
    }

    public MemberDeclaration memberDecl(MemberDeclaration d, CodeExpression assignedValue) {
        return memberDecl(d.getAccessModifier(), d.isLateinit(), d.isStatic(), d.isConstant(), d.getType(), d.getVariable(), d.getAnnotationList(), d.getByProvider(), assignedValue);
    }

    public MethodCall methodCall(CodeExpression caller, String methodName, CodeExpression...args) {
        return methodCall(caller, methodName, argList(args));
    }

    public MethodCall methodCall(CodeExpression caller, String method) {
         return methodCall(caller, method, (ArgumentList) null);
    }

    public MethodCall methodCall(String method, ArgumentList args) {
        return methodCall(null, method, args);
    }

    public MethodCall methodCall(String method, CodeExpression...args) {
        return methodCall(null, method, argList(args));
    }

    public MethodCallStatement methodCallStmt(CodeExpression caller, String methodName, CodeExpression...args) {
        return methodCallStmt(methodCall(caller, methodName, args));
    }

    public Annotation annotation(String name, CodeExpression value) {
        return annotation(name, Collections.singletonList(annotationArg(value)));
    }

    public Annotation annotation(String name, AnnotationArg annotationArg) {
        return annotation(name, Collections.singletonList(annotationArg));
    }

    public AnnotationList annotList(String name, CodeExpression value) {
        return methodAnnotList(Collections.singletonList(annotation(name, value)));
    }

    public Annotation annotation(String name) {
        return annotation(name, (List<AnnotationArg>) null);
    }

    public AnnotationArg annotationArg(CodeExpression value) {
        return annotationArg(Constants.JavaKeywords.VALUE, value);
    }


    public  Annotation paramAnnot(String name) {
        return paramAnnot(name, null);
    }

    public List<Annotation> annotList(Annotation... annotationList) {
        return Arrays.asList(annotationList);
    }

    public ParameterDeclaration paramDecl(Type type, Variable variable, Annotation...annotations) {
        return paramDecl(type, variable, annotList(annotations));
    }

    public ParameterDeclaration paramDecl(Type type, String varName) {
        return paramDecl(type, var(varName));
    }

    public ParameterList paramList(ParameterDeclaration... parameterDeclarationArgs) {
        return paramList(Arrays.asList(parameterDeclarationArgs));
    }

    public ReturnStatement returnStmt() {
       	return returnStmt(null);
    }

    // TODO: Add validation that the type is a primive (i.e. int, float, Integer).
    // TODO: change to primitiveType
    public SimpleType simpleType(String name) {
        return simpleType(name, false);
    }

    public ClassType classType(String name) {
        return classType(name, false);
    }

    public <T extends CodeStatement> StatementList<T> stmtList(T... statementList) {
        return stmtList(Arrays.asList(statementList));
    }

    // TODO: generic?
    public TypeList typeList(Type... typeList) {
        return typeList(Arrays.asList(typeList));
    }

    // TODO: generic?
    public TypeList typeList(String simpleTypeName) {
        return typeList(simpleType(simpleTypeName));
    }

    public TypedAssignmentStatement typedAssignStmt(CodeExpression lval, CodeExpression expr) {
        return typedAssignStmt(null, lval, expr);
    }

    public TypedAssignmentStatement typedAssignStmt(Type type, AssignmentStatement assignmentStatement) {
        return typedAssignStmt(type, assignmentStatement.getLVal(), assignmentStatement.getExpression());
    }

    // TODO: change this to take a type, not a type name
    public TypedAssignmentStatement typedAssignStmt(String typeName, String varName, CodeExpression expr) {
        return typedAssignStmt(simpleType(typeName), var(varName), expr);
    }

    // TODO: remove this
    public VariableDeclaration varDecl(String typeName, String varName) {
        return varDecl(simpleType(typeName), var(varName));
    }

    /**
     * Transform the member list to the Kotlin Constructor arguments
     * @param memberDeclarationList
     * @return
     */
    public ParameterList constructorArgs(List<MemberDeclaration> memberDeclarationList) {
        return paramList(memberDeclarationList.stream()
                .map(member -> paramDecl(member.getType(), member.getVariable(), member.getAnnotationList()))
                .collect(Collectors.toList()));
    }

    public ConstructorDeclaration constructorDecl(String className, List<MemberDeclaration> memberDeclarationList) {
        return constructorDecl(className, constructorArgs(memberDeclarationList));
    }

    public ConstructorDefinition constructorDef(String className, List<MemberDeclaration> memberDeclarationList) {
        return constructorDef(constructorDecl(className, memberDeclarationList),
                               stmtList(memberDeclarationList.stream()
                                    .map(decl -> assignStmt(dot(var(Constants.JavaKeywords.THIS), decl.variable), decl.variable))
                                    .collect(Collectors.toList())));
    }

    public ConstructorDefinition defaultConstructor(String className) {
        return defaultConstructor(className, new ArrayList<MemberDeclaration>());
    }

    public ConstructorDefinition defaultConstructor(String className, List<MemberDeclaration> memberDeclarationList) {
        return constructorDef(constructorDecl(className),
                stmtList(memberDeclarationList.stream()
                        .map(decl -> assignStmt(dot(var(Constants.JavaKeywords.THIS), decl.variable), defaultValue(decl.type)))
                        .collect(Collectors.toList())));
    }

    /*
        public static AssignmentStatement listAllocAssignMember(CodeGen codegen, Variable listVar, Type elementType) {

     */

    /**
     * Constructor field assignment:
     * NOTE: Constants.KotlinKeywords.THIS needs to be changed to codegen.this()
     * {@Code
     * this.<field> = <field>
     * }
     * @param fieldName field to assign
     * @return assignment statement
     */
    public AssignmentStatement assignThisField(String fieldName) {
        return assignStmt(dot(var(Constants.KotlinKeywords.THIS), var(fieldName)), var(fieldName));
    }
}
