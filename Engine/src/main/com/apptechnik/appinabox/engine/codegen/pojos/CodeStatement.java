package com.apptechnik.appinabox.engine.codegen.pojos;

/**
 * I'd call it "statement", but there's naming conflicts all over the place with the SQL parser Statement class.
 */
public interface CodeStatement {
    String toString();
    boolean requiresDelimiter();
}
