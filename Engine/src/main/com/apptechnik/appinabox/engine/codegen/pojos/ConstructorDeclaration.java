package com.apptechnik.appinabox.engine.codegen.pojos;

import com.apptechnik.appinabox.engine.Constants;

public abstract class ConstructorDeclaration {
    protected final String qualifier;
    protected final String name;
    protected final ParameterList parameterList;
    protected final TypeList exceptionList;

    public ConstructorDeclaration(String qualifier,
                                  String name,
                                  ParameterList parameterList,
                                  TypeList exceptionList) {
        this.qualifier = qualifier;
        this.name = name;
        this.parameterList = parameterList;
        this.exceptionList = exceptionList;
    }

    public String getQualifier() {
        return qualifier;
    }
    public String getName() {
        return name;
    }

    public ParameterList getParameterList() {
        return parameterList;
    }

    public TypeList getExceptionList() {
        return exceptionList;
    }

    @Override
    public abstract String toString();
}

