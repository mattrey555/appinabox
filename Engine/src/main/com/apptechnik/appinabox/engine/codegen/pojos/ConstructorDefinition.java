package com.apptechnik.appinabox.engine.codegen.pojos;

public abstract class ConstructorDefinition implements Definition {
    protected final ConstructorDeclaration declaration;
    protected final StatementList statementList;

    public ConstructorDefinition(ConstructorDeclaration declaration, StatementList statementList) {
        this.declaration = declaration;
        this.statementList = statementList;
    }

    public ConstructorDeclaration getDeclaration() {
        return this.declaration;
    }

    public StatementList getStatementList() {
        return this.statementList;
    }

    public abstract String toString();
}

