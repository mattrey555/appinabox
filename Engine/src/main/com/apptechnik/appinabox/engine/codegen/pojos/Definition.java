package com.apptechnik.appinabox.engine.codegen.pojos;

public interface Definition {
    public String toString();
}
