package com.apptechnik.appinabox.engine.codegen.pojos;

import com.apptechnik.appinabox.engine.Constants;

public abstract class ExtendsType implements Type {
    protected final String name;
    protected final Type type;
    protected final boolean isNullable;

    public ExtendsType(String name, Type type, boolean isNullable) {
        this.name = name;
        this.type = type;
        this.isNullable = isNullable;
    }

    @Override
    public String getName() {
        return name;
    }

    public Type getType() {
        return type;
    }

    @Override
    public boolean isNullable() {
        return isNullable;
    }

    @Override
    public abstract String toString();
}
