package com.apptechnik.appinabox.engine.codegen.pojos;

import com.apptechnik.appinabox.engine.Constants;

// Type itemType, Variable itemVar, Variable collection
public abstract class ForIteration implements CodeStatement {
    protected final Type itemType;
    protected final Variable itemVar;
    protected final Variable collection;

    public ForIteration(Type itemType, Variable itemVar, Variable collection) {
        this.itemType = itemType;
        this.itemVar = itemVar;
        this.collection = collection;
    }

    public Type getItemType() {
        return itemType;
    }

    public Variable getItemVar() {
        return itemVar;
    }

    public Variable getCollection() {
        return collection;
    }

    @Override
    public boolean requiresDelimiter() {
        return true;
    }

    @Override
    public abstract String toString();
}
