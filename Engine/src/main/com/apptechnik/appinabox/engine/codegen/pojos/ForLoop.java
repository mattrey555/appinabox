package com.apptechnik.appinabox.engine.codegen.pojos;

// Type itemType, Variable itemVar, Variable collection
public abstract class ForLoop implements CodeStatement {
    protected final ForIteration iteration;
    protected final StatementList statementList;

    public ForLoop(ForIteration iteration, StatementList statementList) {
        this.iteration = iteration;
        this.statementList = statementList;
    }

    public ForIteration getIteration() {
        return iteration;
    }

    public StatementList getStatementList() {
        return statementList;
    }

    @Override
    public boolean requiresDelimiter() {
        return false;
    }

    @Override
    public abstract String toString();
}
