package com.apptechnik.appinabox.engine.codegen.pojos;

public abstract class FunctionDeclaration {
    protected final boolean override;
    protected final boolean suspend;            // Kotlin-specific
    protected final String accessModifier;
    protected final boolean isStatic;
    protected final String name;
    protected final TypeList genericsList;                  // class generic types <T extends Foo, S extends Bar>
    protected final ParameterList parameterList;
    protected final Type returnType;
    protected final TypeList exceptionTypeList;
    protected final AnnotationList annotationList;

    public FunctionDeclaration(boolean override,
                               boolean suspend,
                               String accessModifier,
                               boolean isStatic,
                               String name,
                               TypeList genericsList,
                               ParameterList parameterList,
                               Type returnType,
                               TypeList exceptionTypeList,
                               AnnotationList annotationList) {
        this.override = override;
        this.suspend = suspend;
        this.accessModifier = accessModifier;
        this.isStatic = isStatic;
        this.name = name;
        this.genericsList = genericsList;
        this.parameterList = parameterList;
        this.returnType = returnType;
        this.exceptionTypeList = exceptionTypeList;
        this.annotationList = annotationList;
    }

    public FunctionDeclaration(String accessModifier,
                               boolean isStatic,
                               String name,
                               ParameterList parameterList,
                               Type returnType,
                               TypeList exceptionTypeList,
                               AnnotationList annotationList) {
        this(false, false, accessModifier, isStatic, name, null, parameterList, returnType, exceptionTypeList, annotationList);
    }

    public String getAccessModifier() {
        return accessModifier;
    }

    public boolean isStatic() {
        return isStatic;
    }

    public String getName() {
        return name;
    }

    public ParameterList getParameterList() {
        return parameterList;
    }

    public Type getReturnType() {
        return returnType;
    }

    public boolean isSuspend() {
        return suspend;
    }

    public TypeList getGenericsList() {
        return genericsList;
    }

    public TypeList getExceptionTypeList() {
        return exceptionTypeList;
    }

    public AnnotationList getAnnotationList() {
        return annotationList;
    }

    @Override
    public abstract String toString();
}
