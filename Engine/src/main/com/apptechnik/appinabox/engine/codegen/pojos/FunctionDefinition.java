package com.apptechnik.appinabox.engine.codegen.pojos;

// NOTE: conflict between this and CodeStatement.

public abstract class FunctionDefinition implements Definition {
    protected final FunctionDeclaration functionDeclaration;
    protected final StatementList statementlist;

    public FunctionDefinition(FunctionDeclaration functionDeclaration, StatementList statementlist) {
        this.functionDeclaration = functionDeclaration;
        this.statementlist = statementlist;
    }

    public FunctionDeclaration getFunctionDeclaration() {
        return functionDeclaration;
    }

    public StatementList getStatementlist() {
        return statementlist;
    }

    @Override
    public abstract String toString();
}
