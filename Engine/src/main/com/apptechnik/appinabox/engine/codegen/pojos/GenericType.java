package com.apptechnik.appinabox.engine.codegen.pojos;

import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.Arrays;
import java.util.List;

/**
 * {@code <container<item, item, item>}
 */
public abstract class GenericType implements Type {
    protected final Type container;
    protected final List<Type> itemList;
    protected final boolean isNullable;

    public GenericType() {
        this.container = null;
        this.itemList = null;
        this.isNullable = false;
    }

    public GenericType(Type container, List<Type> itemList) {
        this.container = container;
        this.itemList = itemList;
        this.isNullable = false;
    }

    public GenericType(Type container, List<Type> itemList, boolean isNullable){
        this.container = container;
        this.itemList = itemList;
        this.isNullable = isNullable;
    }

    @Override
    public String getName() {
        return container.getName();
    }

    @Override
    public boolean isNullable() {
        return isNullable;
    }

    public Type getContainer() {
        return container;
    }

    public List<Type> getItemList() {
        return itemList;
    }

    @Override
    public abstract String toString();

    @Override
    public boolean equals(Object o) {
        if (o instanceof GenericType) {
            GenericType g = (GenericType) o;
            if (g.getContainer().equals(container)) {
                if ((g.getItemList() == null) != (getItemList() == null)) {
                    return false;
                }
                if ((g.getItemList() == null) && (getItemList() == null)) {
                    return true;
                }
                return g.getItemList().equals(getItemList());
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hashCode = container.hashCode();
        for (Type t : getItemList()) {
            hashCode ^= t.hashCode();
        }
        return hashCode;
    }
}
