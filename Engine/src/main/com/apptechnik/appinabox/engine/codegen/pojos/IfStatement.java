package com.apptechnik.appinabox.engine.codegen.pojos;

import com.apptechnik.appinabox.engine.Constants;

public abstract class IfStatement implements CodeStatement {
    protected final CodeExpression condition;
    protected final StatementList ifStatementList;
    protected final StatementList elseStatementList;

    public IfStatement(CodeExpression condition, StatementList ifStatementList, StatementList elseStatementList) {
        this.condition = condition;
        this.ifStatementList = ifStatementList;
        this.elseStatementList = elseStatementList;
    }

    public CodeExpression getCondition() {
        return condition;
    }

    public StatementList getIfStatementList() {
        return ifStatementList;
    }

    public StatementList getElseStatementList() {
        return elseStatementList;
    }

    @Override
    public boolean requiresDelimiter() {
        return false;
    }

    @Override
    public abstract String toString();
}
