package com.apptechnik.appinabox.engine.codegen.pojos;

import com.apptechnik.appinabox.engine.Constants;

import java.util.List;

/**
 * I'd call it "import", but there's 1,202,572 packages with the same name.
 */
public abstract class ImportStatement implements CodeStatement {
    protected final String packageName;

    public ImportStatement(String packageName) {
        this.packageName = packageName;
    }

    public String getPackageName() {
        return packageName;
    }

    @Override
    public boolean requiresDelimiter() {
        return false;
    }

    @Override
    public abstract String toString();
 }
