package com.apptechnik.appinabox.engine.codegen.pojos;

public abstract class IntegerConstant implements Constant, CodeExpression {
    protected final int value;

    public IntegerConstant(int value) {
        this.value = value;
    }

    @Override
    public abstract String toString();
}
