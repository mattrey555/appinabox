package com.apptechnik.appinabox.engine.codegen.pojos;

import java.util.List;

/**
 * Generate an expression of the form:
 * {@code public interface <className> [<generics-list>][implements <implements-list>] }
 */
public abstract class InterfaceDeclaration implements Definition {
    protected final String interfaceName;
    protected final TypeList genericsList;
    protected final TypeList implementsList;
    protected final List<FunctionDeclaration> methodDeclarationList;
    protected final List<Annotation> annotationList;

    public InterfaceDeclaration(String interfaceName,
                                TypeList genericsList,
                                TypeList implementsList,
                                List<FunctionDeclaration> methodDeclarationList,
                                List<Annotation> annotationList) {
        this.interfaceName = interfaceName;
        this.implementsList = implementsList;
        this.genericsList = genericsList;
        this.methodDeclarationList = methodDeclarationList;
        this.annotationList = annotationList;
    }

    public String getInterfaceName() {
        return interfaceName;
    }

    public TypeList getGenericsList() {
        return genericsList;
    }

    public TypeList getImplementsList() {
        return implementsList;
    }

    public List<FunctionDeclaration> getMethodDeclarationList() {
        return methodDeclarationList;
    }

    public List<Annotation> getAnnotationList() {
        return annotationList;
    }

    @Override
    public abstract String toString();
}