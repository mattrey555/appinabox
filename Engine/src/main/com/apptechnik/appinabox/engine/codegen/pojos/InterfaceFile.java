package com.apptechnik.appinabox.engine.codegen.pojos;

import com.apptechnik.appinabox.engine.codegen.objects.CreateCommon;
import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;

import java.io.IOException;

public abstract class InterfaceFile {
    protected final PackageStatement packageStatement;
    protected final StatementList importStatements;
    protected final InterfaceDeclaration interfaceDeclaration;

    public InterfaceFile(PackageStatement packageStatement,
                         StatementList importStatements,
                         InterfaceDeclaration interfaceDeclaration) {
        this.packageStatement = packageStatement;
        this.importStatements = importStatements;
        this.interfaceDeclaration = interfaceDeclaration;
    }

    public PackageStatement getPackageStatement() {
        return packageStatement;
    }

    public StatementList getImportStatements() {
        return importStatements;
    }

    public InterfaceDeclaration getInterfaceDeclaration() {
        return interfaceDeclaration;
    }

    @Override
    public abstract String toString();

    public abstract void write(CreateCommon createCommon) throws Exception;
}
