package com.apptechnik.appinabox.engine.codegen.pojos;

import java.util.List;

public abstract class LambdaClass implements CodeExpression {
    protected final Type implementedType;
    protected final List<FunctionDefinition> functionDefinitionList;

    public LambdaClass(Type implementedType, List<FunctionDefinition> functionDefinitionList) {
        this.implementedType = implementedType;
        this.functionDefinitionList = functionDefinitionList;
    }

    public Type getImplementedType() {
        return implementedType;
    }

    public List<FunctionDefinition> getFunctionDefinitionList() {
        return functionDefinitionList;
    }

    @Override
    public abstract String toString();

}
