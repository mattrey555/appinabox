package com.apptechnik.appinabox.engine.codegen.pojos;

/**
 * Generate a lambda expression of the form:
 * {@code var -> expression}
 */
public abstract class LambdaExpression implements CodeExpression {
    protected final Variable var;
    protected final CodeExpression expr;

    public LambdaExpression(Variable var, CodeExpression expr) {
        this.var = var;
        this.expr = expr;
    }

    @Override
    public abstract String toString();
}
