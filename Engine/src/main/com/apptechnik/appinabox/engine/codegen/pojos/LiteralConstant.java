package com.apptechnik.appinabox.engine.codegen.pojos;

public abstract class LiteralConstant implements Constant, CodeExpression {
    protected final String name;

    public LiteralConstant(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public abstract String toString();
}
