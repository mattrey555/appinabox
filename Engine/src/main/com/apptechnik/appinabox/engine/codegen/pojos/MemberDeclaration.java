package com.apptechnik.appinabox.engine.codegen.pojos;

import java.util.ArrayList;
import java.util.List;

/**
 * Generate a Member Declaration of the form:
 * {@code <access-modifier> [static] <type> <variable>}
 */
public abstract class MemberDeclaration extends VariableDeclaration {
    protected final String accessModifier;
    protected final boolean isLateinit;             // kotlin-specific
    protected final boolean isStatic;
    protected final boolean isConstant;
    protected List<Annotation> annotationList;
    protected final String byProvider;              // kotlin-specific (but java can use annotations)
    protected final CodeExpression assignedValue;

    public MemberDeclaration(String accessModifier,
                             boolean isLateinit,
                             boolean isStatic,
                             boolean isConstant,
                             Type type,
                             Variable variable,
                             List<Annotation> annotationList,
                             String byProvider,
                             CodeExpression assignedValue) {
        super(type, variable);
        this.accessModifier = accessModifier;
        this.isLateinit = isLateinit;
        this.isStatic = isStatic;
        this.isConstant = isConstant;
        this.annotationList = annotationList;
        this.byProvider = byProvider;
        this.assignedValue = assignedValue;
    }

    public MemberDeclaration(MemberDeclaration a) {
        super(a.getType(), a.getVariable());
        this.accessModifier = a.getAccessModifier();
        this.isStatic = a.isStatic();
        this.isConstant = a.isConstant();
        this.isLateinit = a.isLateinit();
        this.annotationList = new ArrayList<>(a.getAnnotationList());
        this.byProvider = a.getByProvider();
        this.assignedValue = a.getAssignedValue();
    }

    public String getAccessModifier() {
        return accessModifier;
    }

    public boolean isStatic() {
        return isStatic;
    }

    public boolean isConstant() {
        return isConstant;
    }

    public String getByProvider() {
        return byProvider;
    }

    public CodeExpression getAssignedValue() {
        return assignedValue;
    }

    public boolean isLateinit() {
        return isLateinit;
    }

    public List<Annotation> getAnnotationList() {
        return annotationList;
    }

    public void addAnnotation(Annotation annotation) {
        if (annotationList == null) {
            annotationList = new ArrayList<Annotation>();
        }
        annotationList.add(annotation);
    }

    @Override
    public abstract String toString();
}
