package com.apptechnik.appinabox.engine.codegen.pojos;

public abstract class MethodCall implements CodeExpression {
    protected final CodeExpression caller;
    protected final String methodName;
    protected final ArgumentList argList;


    public MethodCall(CodeExpression caller, String methodName, ArgumentList argList) {
        this.caller = caller;
        this.methodName = methodName;
        this.argList = argList;
    }

    public CodeExpression getCaller() {
        return caller;
    }

    public String getMethod() {
        return methodName;
    }

    public ArgumentList getArgs() {
        return argList;
    }

    @Override
    public abstract String toString();
}
