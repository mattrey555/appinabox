package com.apptechnik.appinabox.engine.codegen.pojos;

public abstract class MethodCallStatement implements CodeStatement {
    protected final MethodCall call;

    public MethodCallStatement(MethodCall call) {
        this.call = call;
    }

    @Override
    public boolean requiresDelimiter() {
        return true;
    }

    @Override
    public abstract String toString();
}
