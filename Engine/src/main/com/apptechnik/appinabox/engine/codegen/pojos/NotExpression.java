package com.apptechnik.appinabox.engine.codegen.pojos;

public abstract class NotExpression implements CodeExpression {
    protected final CodeExpression codeExpression;

    public NotExpression(CodeExpression codeExpression) {
        this.codeExpression = codeExpression;
    }

    public CodeExpression getJavaExpression() {
        return codeExpression;
    }

    @Override
    public abstract String toString();
}
