package com.apptechnik.appinabox.engine.codegen.pojos;

import com.apptechnik.appinabox.engine.Constants;

public abstract class NullExpression implements CodeExpression {
    @Override
    public abstract String toString();
}
