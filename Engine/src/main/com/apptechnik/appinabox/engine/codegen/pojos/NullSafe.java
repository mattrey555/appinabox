package com.apptechnik.appinabox.engine.codegen.pojos;

public abstract class NullSafe implements CodeExpression {
    protected final CodeExpression expr;

    public NullSafe(CodeExpression expr) {
        this.expr = expr;
    }

    public CodeExpression getExpr() {
        return expr;
    }

    @Override
    public abstract String toString();

}
