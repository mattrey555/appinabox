package com.apptechnik.appinabox.engine.codegen.pojos;

import com.apptechnik.appinabox.engine.Constants;

public abstract class PackageStatement implements CodeStatement {
    protected final String name;

    public String getName() {
        return name;
    }

    public PackageStatement(String name) {
        this.name = name;
    }

    @Override
    public boolean requiresDelimiter() {
        return false;
    }

    @Override
    public abstract String toString();
}
