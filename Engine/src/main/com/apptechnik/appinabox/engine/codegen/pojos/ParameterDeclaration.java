package com.apptechnik.appinabox.engine.codegen.pojos;

import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.List;

public abstract class ParameterDeclaration {
    protected final Type type;
    protected final Variable variable;
    protected List<Annotation> annotationList;
    protected final String memberPrefix;                // kotlin-specific (var/val) used in constructors. Ignored in Java

    public ParameterDeclaration(String memberPrefix, Type type, Variable variable, List<Annotation> annotationList) {
        this.memberPrefix = memberPrefix;
        this.type = type;
        this.variable = variable;
        this.annotationList = annotationList;
    }

    public ParameterDeclaration(Type type, Variable variable, List<Annotation> annotationList) {
        this(null, type, variable, annotationList);
    }

    public Type getType() {
        return type;
    }

    public Variable getVariable() {
        return variable;
    }

    public List<Annotation> getAnnotationList() {
        return annotationList;
    }

    public String getMemberPrefix() {
        return memberPrefix;
    }

    @Override
    public abstract String toString();
}
