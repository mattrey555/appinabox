package com.apptechnik.appinabox.engine.codegen.pojos;

import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.Arrays;
import java.util.List;

public abstract class ParameterList implements CodeExpression {
    protected final List<ParameterDeclaration> parameterDeclarationList;

    public ParameterList(List<ParameterDeclaration> parameterDeclarationList) {
        this.parameterDeclarationList = parameterDeclarationList;
    }

    public List<ParameterDeclaration> getParameterDeclarationList() {
        return parameterDeclarationList;
    }

    public void addParam(ParameterDeclaration paramDecl) {
        this.parameterDeclarationList.add(paramDecl);
    }

    @Override
    public abstract String toString();
}
