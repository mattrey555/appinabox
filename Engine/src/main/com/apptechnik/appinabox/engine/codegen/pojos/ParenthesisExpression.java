package com.apptechnik.appinabox.engine.codegen.pojos;

/**
 * I'd call it Expression, but if conflcits with the SQL parser exception.
 */
public abstract class ParenthesisExpression implements CodeExpression {
    protected final CodeExpression expr;

    public ParenthesisExpression(CodeExpression expr) {
        this.expr = expr;
    }

    public CodeExpression getExpr() {
        return expr;
    }
}
