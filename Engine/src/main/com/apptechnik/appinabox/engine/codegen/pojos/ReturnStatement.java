package com.apptechnik.appinabox.engine.codegen.pojos;

import com.apptechnik.appinabox.engine.Constants;

public abstract class ReturnStatement implements CodeStatement {
    protected CodeExpression expression;

    public ReturnStatement(CodeExpression expression) {
        this.expression = expression;
    }

    public CodeExpression getExpression() {
        return expression;
    }

    @Override
    public boolean requiresDelimiter() {
        return false;
    }

    @Override
    public abstract String toString();
}
