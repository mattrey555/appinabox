package com.apptechnik.appinabox.engine.codegen.pojos;

public class SimpleType implements Type {
    protected final String name;
    protected final boolean isNullable;

    public SimpleType() {
        this.name = null;
        this.isNullable = false;
    }

    public SimpleType(String name) {
        this.name = name;
        this.isNullable = false;
    }

    public SimpleType(String name, boolean isNullable) {
        this.name = name;
        this.isNullable = isNullable;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean isNullable() {
        return isNullable;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof SimpleType) {
            return ((SimpleType) o).getName().equals(getName());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
