package com.apptechnik.appinabox.engine.codegen.pojos;

import java.util.List;

// TODO really should have "code-block" which adds curly braces.
public abstract class StatementList<T extends CodeStatement> implements CodeExpression {
    protected final List<T> statementList;

    public StatementList(List<T> statementList) {
        this.statementList = statementList;
    }

    public List<T> getStatementList() {
        return statementList;
    }

    public void addStatement(T statement) {
        statementList.add(statement);
    }

    public void addStatements(List<T> statements) {
        statementList.addAll(statements);
    }

    public void addStatements(StatementList<T> statements) {
        statementList.addAll(statements.getStatementList());
    }

    @Override
    public abstract String toString();
}
