package com.apptechnik.appinabox.engine.codegen.pojos;

public abstract class StringConstant implements Constant, CodeExpression {
    protected final String name;

    public StringConstant(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public abstract String toString();
}
