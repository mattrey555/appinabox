package com.apptechnik.appinabox.engine.codegen.pojos;

public abstract class ThrowStatement implements CodeStatement {
    protected CodeExpression expression;

    public ThrowStatement(CodeExpression expression) {
        this.expression = expression;
    }

    public CodeExpression getExpression() {
        return expression;
    }

    @Override
    public boolean requiresDelimiter() {
        return false;
    }

    @Override
    public abstract String toString();
}
