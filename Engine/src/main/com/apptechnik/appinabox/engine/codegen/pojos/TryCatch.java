package com.apptechnik.appinabox.engine.codegen.pojos;

import com.apptechnik.appinabox.engine.Constants;

public abstract class TryCatch implements CodeStatement {
    protected StatementList bodyList;
    protected Type exceptionType;
    protected Variable exceptionName;
    protected StatementList exceptionBodyList;

    public TryCatch(StatementList bodyList, Type exceptionType, Variable exceptionName, StatementList exceptionBodyList) {
        this.bodyList = bodyList;
        this.exceptionType = exceptionType;
        this.exceptionName = exceptionName;
        this.exceptionBodyList = exceptionBodyList;
    }

    public StatementList getBodyList() {
        return bodyList;
    }

    public Type getExceptionType() {
        return exceptionType;
    }

    public Variable getExceptionName() {
        return exceptionName;
    }

    public StatementList getExceptionBodyList() {
        return exceptionBodyList;
    }

    @Override
    public boolean requiresDelimiter() {
        return false;
    }

    @Override
    public abstract String toString();
}
