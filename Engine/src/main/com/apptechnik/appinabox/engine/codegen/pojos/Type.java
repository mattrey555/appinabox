package com.apptechnik.appinabox.engine.codegen.pojos;

// TODO: naming conflict with java.util.Type.

public interface Type {
    String getName();
    boolean isNullable();

    @Override
    String toString();
}
