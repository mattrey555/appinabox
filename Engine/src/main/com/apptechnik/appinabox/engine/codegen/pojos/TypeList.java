package com.apptechnik.appinabox.engine.codegen.pojos;

import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TypeList  {
    protected final List<Type> typeList;

    public TypeList() {
        typeList = new ArrayList<>();
    }

    public TypeList(List<Type> typeList) {
        this.typeList = typeList;
    }

    public List<Type> getTypeList() {
        return typeList;
    }

    public void addType(Type type) { typeList.add(type); }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Type type : typeList) {
            sb.append(type + " ");
        }
        return sb.toString();
    }
}
