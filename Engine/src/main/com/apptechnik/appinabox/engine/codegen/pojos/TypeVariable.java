package com.apptechnik.appinabox.engine.codegen.pojos;

public class TypeVariable extends SimpleType {

    public TypeVariable(String name) {
        super(name);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof TypeVariable) {
            return ((TypeVariable) o).getName().equals(getName());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
