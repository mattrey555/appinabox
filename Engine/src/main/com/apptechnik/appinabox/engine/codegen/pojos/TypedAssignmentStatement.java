package com.apptechnik.appinabox.engine.codegen.pojos;

public abstract class TypedAssignmentStatement extends AssignmentStatement {
    protected final Type type;

    public TypedAssignmentStatement(Type type, CodeExpression lval, CodeExpression expr) {
        super(lval, expr);
        this.type = type;
    }

    /**
     * For implicitly typed languages like Kotlin
     * @param lval
     * @param expr
     */
    public TypedAssignmentStatement(CodeExpression lval, CodeExpression expr) {
        super(lval, expr);
        this.type = null;
    }

    public Type getType() {
        return type;
    }

    @Override
    public boolean requiresDelimiter() {
        return false;
    }

    @Override
    public abstract String toString();
}
