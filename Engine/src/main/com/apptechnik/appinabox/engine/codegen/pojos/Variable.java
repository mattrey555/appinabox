package com.apptechnik.appinabox.engine.codegen.pojos;

public abstract class Variable implements CodeExpression {
    protected final String name;

    public Variable(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public abstract String toString();
}
