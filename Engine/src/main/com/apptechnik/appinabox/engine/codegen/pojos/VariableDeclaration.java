package com.apptechnik.appinabox.engine.codegen.pojos;

public abstract class VariableDeclaration implements CodeStatement {
    protected final Type type;
    protected final Variable variable;

    public VariableDeclaration(Type type, Variable variable) {
        this.type = type;
        this.variable = variable;
    }

    public Type getType() {
        return type;
    }

    public Variable getVariable() {
        return variable;
    }

    @Override
    public boolean requiresDelimiter() {
        return true;
    }

    @Override
    public abstract String toString();
}
