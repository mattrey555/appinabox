package com.apptechnik.appinabox.engine.codegen.pojos;

public abstract class WhileLoop implements CodeStatement {
    protected final CodeExpression condition;
    protected final StatementList statementList;

    public WhileLoop(CodeExpression condition,
                     StatementList statementList) {
        this.condition = condition;
        this.statementList = statementList;
    }

    public CodeExpression getCondition() {
        return condition;
    }

    public StatementList getStatementList() {
        return statementList;
    }

    @Override
    public boolean requiresDelimiter() {
        return false;
    }

    @Override
    public abstract String toString();
}
