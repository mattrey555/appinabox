package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.Allocation;
import com.apptechnik.appinabox.engine.codegen.pojos.ArgumentList;
import com.apptechnik.appinabox.engine.codegen.pojos.StatementList;
import com.apptechnik.appinabox.engine.codegen.pojos.Type;

class JavaAllocation extends Allocation {

    public JavaAllocation(Type type, ArgumentList constructorArgs, StatementList inlineStatements) {
        super(type, constructorArgs, inlineStatements);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Constants.JavaKeywords.NEW + " " + getType());
        if (constructorArgs != null) {
            sb.append("(" + constructorArgs + ")");
        } else {
            sb.append("()");
        }
        if (inlineStatements != null) {
            sb.append("{");
            sb.append(inlineStatements);
            sb.append("}");
        }
        return sb.toString();
    }
}
