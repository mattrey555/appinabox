package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.AnnotationArg;
import com.apptechnik.appinabox.engine.codegen.pojos.ArgumentList;
import com.apptechnik.appinabox.engine.codegen.pojos.Annotation;

import java.util.List;

class JavaAnnotation extends Annotation {
    public JavaAnnotation(String name, List<AnnotationArg> argList) {
        super(name, argList);
    }
}
