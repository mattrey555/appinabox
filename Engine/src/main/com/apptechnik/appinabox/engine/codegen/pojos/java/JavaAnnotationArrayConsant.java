package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.AnnotationArrayConstant;
import com.apptechnik.appinabox.engine.codegen.pojos.Constant;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.List;

public class JavaAnnotationArrayConsant extends AnnotationArrayConstant {

    public JavaAnnotationArrayConsant(List<Constant> value) {
        super(value);
    }

    @Override
    public String toString() {
        return "[" + StringUtil.delim(value, ", ") + "]";
    }
}
