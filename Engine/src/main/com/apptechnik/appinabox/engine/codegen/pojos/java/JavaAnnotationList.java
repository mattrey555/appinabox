package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.Annotation;
import com.apptechnik.appinabox.engine.codegen.pojos.AnnotationList;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.List;

public class JavaAnnotationList extends AnnotationList {
    public JavaAnnotationList(List<Annotation> methodAnnotationList) {
        super(methodAnnotationList);
    }

    @Override
    public String toString() {
        return StringUtil.delim(annotationList, "\n");
    }
}

