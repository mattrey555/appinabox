package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.ArgumentList;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.Arrays;
import java.util.List;

class JavaArgumentList extends ArgumentList {

    public JavaArgumentList(List<CodeExpression> argumentList) {
        super(argumentList);
    }

    @Override
    public String toString() {
        return StringUtil.concat(argumentList, ", ");
    }
}
