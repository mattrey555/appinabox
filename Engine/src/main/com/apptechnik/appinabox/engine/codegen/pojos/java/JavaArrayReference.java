package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.ArrayReference;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;

import java.util.Arrays;
import java.util.List;

class JavaArrayReference extends ArrayReference {

    public JavaArrayReference(CodeExpression expr, List<CodeExpression> indexList) {
        super(expr, indexList);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(expr);
        for (CodeExpression index : indexList) {
            sb.append("[" + index +"]");
        }
        return sb.toString();
    }
}
