package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.ArrayType;
import com.apptechnik.appinabox.engine.codegen.pojos.Type;
import com.apptechnik.appinabox.library.util.StringUtils;

class JavaArrayType extends ArrayType {

    public JavaArrayType(Type item, int dimensions, boolean isNullable) {
        super(item, dimensions, isNullable);
    }

    @Override
    public String toString() {
        return item + StringUtils.repeat("[]", "", dimensions);
    }
}
