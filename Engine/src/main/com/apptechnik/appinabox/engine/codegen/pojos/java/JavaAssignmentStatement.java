package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.AssignmentStatement;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;

class JavaAssignmentStatement extends AssignmentStatement {

    public JavaAssignmentStatement(CodeExpression lval, CodeExpression expression) {
        super(lval, expression);
    }

    @Override
    public boolean requiresDelimiter() {
        return true;
    }

    @Override
    public String toString() {
        return lval + " = " + expression;
    }
}
