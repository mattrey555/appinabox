package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.CodeBinaryExpression;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;

/**
 * I'd call it Expression, but if conflicts with the SQL parser exception.
 */
class JavaBinaryExpression extends CodeBinaryExpression {

    public JavaBinaryExpression(String operator, CodeExpression left, CodeExpression right) {
        super(operator, left, right);
    }

    @Override
    public String toString() {
        return left.toString() + operator + right.toString();
    }
}
