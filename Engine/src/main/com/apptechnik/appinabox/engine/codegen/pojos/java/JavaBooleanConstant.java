package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.BooleanConstant;

class JavaBooleanConstant extends BooleanConstant {

    public JavaBooleanConstant(boolean value) {
        super(value);
    }

    @Override
    public String toString() {
        return Boolean.toString(value);
    }
}
