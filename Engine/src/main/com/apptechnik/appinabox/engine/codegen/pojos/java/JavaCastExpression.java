package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.CastExpression;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;
import com.apptechnik.appinabox.engine.codegen.pojos.Type;

class JavaCastExpression extends CastExpression {

    public JavaCastExpression(Type type, CodeExpression expr) {
        super(type, expr);
    }

    @Override
    public String toString() {
        return "(" + type + ") " + expr;
    }
}
