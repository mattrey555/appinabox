package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.Collections;
import java.util.List;

/**
 * Generate an expression of the form:
 * {@code public class <className> [extends <extends-list>] [implements <implements-list>] }
 */
class JavaClassDeclaration extends ClassDeclaration {
    public JavaClassDeclaration(String className,
                                TypeList genericsList,
                                Type extendsType,
                                TypeList implementsList,
                                List<Annotation> annotationList) {
        super(className, genericsList, extendsType, implementsList, annotationList);
    }

    public JavaClassDeclaration(boolean isStatic,
                                boolean isFinal,
                                boolean isData,
                                String className,
                                TypeList genericsList,
                                Type extendsType,
                                TypeList implementsList,
                                List<Annotation> annotationList) {
        super(isStatic, isFinal, isData, className, genericsList, extendsType, implementsList, annotationList);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (annotationList != null) {
            sb.append(StringUtil.delim(annotationList, "\n"));
        }
        sb.append(this.accessQualifier + " ");
        if (isFinal) {
           sb.append(Constants.JavaKeywords.FINAL + " ");
        }
        if (isStatic) {
            sb.append(Constants.JavaKeywords.STATIC + " ");
        }
        sb.append(Constants.JavaKeywords.CLASS + " " + this.className + " ");
        if (genericsList != null) {
            sb.append("<");
            sb.append(genericsList);
            sb.append(">");
        }
        if (extendsType != null) {
            sb.append(" " + Constants.JavaKeywords.EXTENDS + " ");
            sb.append(extendsType);
        }
        if (implementsList != null) {
            sb.append(" " + Constants.JavaKeywords.IMPLEMENTS + " ");
            sb.append(implementsList);
        }
        return sb.toString();
    }

    @Override
    public String toString(List<MemberDeclaration> memberDeclarationList) {
        return toString();
    }
}
