package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.List;

/**
 * A class definition is:
 * {@code
 * <classDeclaration> {
 * <memberDeclarationList>
 * <constructorDefinitionList>
 * <methodDefinitionList>
 * }}
 */
class JavaClassDefinition extends ClassDefinition {

    public JavaClassDefinition(ClassDeclaration classDeclaration,
                               List<MemberDeclaration> memberDeclarationList,
                               List<ConstructorDefinition> constructorDefinitionList,
                               List<FunctionDefinition> functionDefinitionList,
                               List<ClassDefinition> innerClassList,
                               List<InterfaceDeclaration> innerInterfaceList) {
        super(classDeclaration, memberDeclarationList, constructorDefinitionList, functionDefinitionList, innerClassList,
              innerInterfaceList);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(classDeclaration + " {\n");
        if (memberDeclarationList != null) {
            sb.append(StringUtil.delim(memberDeclarationList, ";\n"));
        }
        sb.append("\n");
        if (constructorDefinitionList != null) {
            sb.append(StringUtil.delim(constructorDefinitionList, "\n"));
        }
        if (methodDefinitionList != null) {
            sb.append(StringUtil.delim(methodDefinitionList, "\n"));
        }
        if (innerClassList != null) {
            sb.append(StringUtil.delim(innerClassList, "\n"));
        }
        if (innerInterfaceList != null) {
            sb.append(StringUtil.delim(innerInterfaceList, "\n"));
        }
        sb.append("}\n");
        return sb.toString();
    }
}
