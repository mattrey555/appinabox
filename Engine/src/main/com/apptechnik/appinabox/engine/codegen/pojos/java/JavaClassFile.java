package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.ClassDefinition;
import com.apptechnik.appinabox.engine.codegen.pojos.ClassFile;
import com.apptechnik.appinabox.engine.codegen.pojos.PackageStatement;
import com.apptechnik.appinabox.engine.codegen.pojos.StatementList;
import com.apptechnik.appinabox.engine.util.IndentingFileWriter;
import com.apptechnik.appinabox.engine.util.OutputPaths;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

class JavaClassFile extends ClassFile {

    public JavaClassFile(PackageStatement packageStatement,
                         StatementList importStatements,
                         ClassDefinition classDefinition) {
        super(packageStatement, importStatements, classDefinition);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(packageStatement + ";\n");
        sb.append(importStatements + "\n");
        sb.append(classDefinition + "\n");
        return sb.toString();
    }

    /**
     * Used the package name as a directory, and the class name as the file name to create a file
     * to write this class + package + imports to.
     * @param outputDir base directory references to write objects to.
     * @throws IOException on any kind of file open/write exception.
     */
    @Override
    public void write(File outputDir) throws Exception {
        String packageName = packageStatement.getName();
        String className = classDefinition.getClassDeclaration().getClassName();
        String fileName = className + Constants.Extensions.JAVA;
        File outputFile = new File(OutputPaths.javaDir(outputDir, packageName), fileName);
        IndentingFileWriter ifwSqlFns = new IndentingFileWriter(new FileWriter(outputFile));
        ifwSqlFns.write(this.toString());
        ifwSqlFns.close();
    }

}
