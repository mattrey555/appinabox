package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.ClassType;

class JavaClassType extends ClassType {
    public JavaClassType(String name, boolean isNullable) {
        super(name, isNullable);
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof JavaClassType) {
            return ((JavaClassType) o).getName().equals(getName());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
