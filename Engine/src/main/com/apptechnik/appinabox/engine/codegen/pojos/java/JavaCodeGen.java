
package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.codegen.pojos.kotlin.KotlinAnnotationArrayConsant;
import com.apptechnik.appinabox.engine.exceptions.TypeNotFoundException;
import com.apptechnik.appinabox.engine.parser.types.SQLMapType;
import com.apptechnik.appinabox.engine.util.FileUtil;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class JavaCodeGen extends CodeGen {
    public Allocation alloc(Type type, ArgumentList constructorArgs, StatementList inlineStatements) {
        return new JavaAllocation(type, constructorArgs, inlineStatements);
    }

    public ArgumentList argList(List<CodeExpression> argumentList) {
        return new JavaArgumentList(argumentList);
    }

    public ArrayReference arrayRef(CodeExpression expr, List<CodeExpression> indexList) {
        return new JavaArrayReference(expr, indexList);
    }

    public ArrayType arrayType(Type item, int dimensions, boolean isNullable) {
        return new JavaArrayType(item, dimensions, isNullable);
    }

    public AssignmentStatement assignStmt(CodeExpression lval, CodeExpression expression) {
        return new JavaAssignmentStatement(lval, expression);
    }

    public BooleanConstant booleanConst(boolean value) {
        return new JavaBooleanConstant(value);
    }

    public CastExpression castExpr(Type type, CodeExpression expr) {
        return new JavaCastExpression(type, expr);
    }

    public Annotation classAnnot(String name) {
        return new JavaAnnotation(name, null);
    }

    public InterfaceDeclaration interfaceDecl(String interfaceName,
                                               TypeList genericsList,
                                               TypeList implementsList,
                                               List<FunctionDeclaration> methodDeclarations,
                                               List<Annotation> annotationList) {
        return new JavaInterfaceDeclaration(interfaceName, genericsList, implementsList, methodDeclarations, annotationList);
    }

    public ClassDeclaration classDecl(boolean isStatic,
                                       boolean isFinal,
                                       boolean isData,
                                       String className,
                                       TypeList genericsList,
                                       Type extendsType,
                                       TypeList implementsList,
                                       List<Annotation> annotationList) {
        return new JavaClassDeclaration(isStatic, isFinal, isData, className, genericsList, extendsType, implementsList, annotationList);
    }

    public ClassDefinition classDef(ClassDeclaration classDeclaration,
                                    List<MemberDeclaration> memberDeclarationList,
                                    List<ConstructorDefinition> constructorDefinitionList,
                                    List<FunctionDefinition> functionDefinitionList,
                                    List<ClassDefinition> innertClassList,
                                    List<InterfaceDeclaration> innerInterfaceList) {
        return new JavaClassDefinition(classDeclaration, memberDeclarationList,
                                       constructorDefinitionList, functionDefinitionList, innertClassList, innerInterfaceList);
    }

    public InterfaceFile interfaceFile(PackageStatement packageStatement,
                                       StatementList importStatements,
                                       InterfaceDeclaration interfaceDeclaration) {
        return new JavaInterfaceFile(packageStatement, importStatements, interfaceDeclaration);
    }

    public ClassFile classFile(PackageStatement packageStatement,
                                StatementList importStatements,
                                ClassDefinition classDefinition) {
        return new JavaClassFile(packageStatement, importStatements, classDefinition);
    }

    public CodeBinaryExpression binaryExpr(String operator, CodeExpression left, CodeExpression right) {
        return new JavaBinaryExpression(operator, left, right);
    }

    public CodeExpression parenExpr(CodeExpression expr) {
        return new JavaParenthesisExpression(expr);
    }

    public ConstructorDeclaration constructorDecl(String qualifier,
                                                  String name,
                                                  ParameterList parameterList,
                                                  TypeList exceptionList) {
        return new JavaConstructorDeclaration(qualifier, name, parameterList, exceptionList);
    }

    public ConstructorDefinition constructorDef(ConstructorDeclaration declaration, StatementList statementList) {
        return new JavaConstructorDefinition(declaration, statementList);
    }


    public ExtendsType extendsType(String name, Type type, boolean isNullable) {
        return new JavaExtendsType(name, type, isNullable);
    }

    public ForIteration forIter(Type itemType, Variable itemVar, Variable collection) {
        return new JavaForIteration(itemType, itemVar, collection);
    }

    public ForLoop forLoop(ForIteration iteration, StatementList statementList) {
        return new JavaForLoop(iteration, statementList);
    }

    public FunctionDeclaration functionDecl(boolean override,
                                            boolean suspend,
                                            String accessModifier,
                                             boolean isStatic,
                                             String name,
                                             TypeList genericsList,
                                             ParameterList parameterList,
                                             Type returnType,
                                             TypeList exceptionTypeList,
                                             AnnotationList methodAnnotationList) {
        return new JavaFunctionDeclaration(override, suspend, accessModifier, isStatic, name, genericsList, parameterList,
                                           returnType, exceptionTypeList, methodAnnotationList);
    }

    public FunctionDefinition functionDef(FunctionDeclaration functionDeclaration, StatementList statementlist) {
        return new JavaFunctionDefinition(functionDeclaration, statementlist);
    }

    public GenericType genericType(Type container, List<Type> itemList, boolean isNullable) {
        return new JavaGenericType(container, itemList, isNullable);
    }

    public GenericType listType(Type elementType) {
        return genericType(classType(Constants.JavaTypes.LIST), elementType);
    }

    public GenericType arrayListType(Type elementType) {
        return genericType(classType(Constants.JavaTypes.ARRAY_LIST), elementType);
    }

    public IfStatement ifStmt(CodeExpression condition, StatementList ifStatementList, StatementList elseStatementList) {
        return new JavaIfStatement(condition, ifStatementList, elseStatementList);
    }

    public ImportStatement importStmt(String packageName) {
        return new JavaImportStatement(packageName);
    }

    public StatementList importStatementList(String resourceName) throws IOException {
        String[] importList = FileUtil.getResourceStrings(JavaCodeGen.class, resourceName);
        return new JavaStatementList(Arrays.stream(importList)
                .map(s -> new JavaImportStatement(s))
                .collect(Collectors.toList()));
    }

    public IntegerConstant integerConst(int value) {
        return new JavaIntegerConstant(value);
    }

    public LambdaExpression lambdaExpr(Variable var, CodeExpression expr) {
        return new JavaLambdaExpression(var, expr);
    }

    public LiteralConstant literalConst(String name) {
        return new JavaLiteralConstant(name);
    }


    public MemberDeclaration memberDecl(String accessModifier,
                                        boolean isLateinit,
                                        boolean isStatic,
                                        boolean isConstant,
                                        Type type,
                                        Variable variable,
                                        List<Annotation> annotationList,
                                        String byProvider,
                                        CodeExpression assignedValue) {
        return new JavaMemberDeclaration(accessModifier, isStatic, isConstant, type, variable, annotationList, byProvider, assignedValue);
    }

    public Annotation annotation(String name, List<AnnotationArg> args) {
        return new JavaAnnotation(name, args);
    }

    public AnnotationArrayConstant annotationArrayConstant(List<Constant> list) {
        return new KotlinAnnotationArrayConsant(list);
    }

    public AnnotationList methodAnnotList(List<Annotation> methodAnnotationList) {
        return new JavaAnnotationList(methodAnnotationList);
    }

    public MethodCall methodCall(CodeExpression caller, String methodName, ArgumentList argList) {
        return new JavaMethodCall(caller, methodName, argList);
    }

    public MethodCallStatement methodCallStmt(MethodCall call) {
        return new JavaMethodCallStatement(call);
    }

    public NotExpression notExpr(CodeExpression codeExpression) {
        return new JavaNotExpression(codeExpression);
    }

    public PackageStatement packageStmt(String name) {
        return new JavaPackageStatement(name);
    }

    public AnnotationArg annotationArg(String name, CodeExpression value) {
        return new AnnotationArg(name, value);
    }

    public Annotation paramAnnot(String name, List<AnnotationArg> argumentList) {
        return new JavaAnnotation(name, argumentList);
    }

    public ParameterDeclaration paramDecl(Type type, Variable variable, List<Annotation> annotationList) {
        return new JavaParameterDeclaration(type, variable, annotationList);
    }

    public ParameterList paramList(List<ParameterDeclaration> parameterDeclarationList) {
        return new JavaParameterList(parameterDeclarationList);
    }

    public ReturnStatement returnStmt(CodeExpression expression) {
        return new JavaReturnStatement(expression);
    }

    public ThrowStatement throwStmt(CodeExpression expression) { return new JavaThrowStatement(expression); }

    public SimpleType simpleType(String name, boolean isNullable) {
        return new JavaSimpleType(name, isNullable);
    }

    public ClassType classType(String name, boolean isNullable) {
        return new JavaClassType(name, isNullable);
    }
    public TypeVariable typeVariable(String name) {
        return new JavaTypeVariable(name);
    }

    /**
     * Is Type primitive or a generic of boxed primitives?
     * We only support generics with one argument.
     * @param type
     * @return
     */
    public boolean isSimpleType(Type type) {
        if (StringUtil.in(Constants.PRIMITIVE_JAVA_TYPES, type.getName())) {
            return true;
        } else if (type instanceof GenericType) {
            GenericType genericType = (GenericType) type;
            return genericType.getContainer().getName().equals(Constants.JavaTypes.LIST) &&
                    (genericType.getItemList().size() == 1) && isSimpleType(genericType.getItemList().get(0));
        }
        return false;
    }

    public StatementList stmtList(List statementList) {
        return new JavaStatementList(statementList);
    }

    public StringConstant stringConst(String name) {
        return new JavaStringConstant(name);
    }

    public TryCatch tryCatch(StatementList bodyList,
                             Type exceptionType,
                             Variable exceptionName,
                             StatementList exceptionBodyList) {
        return new JavaTryCatch(bodyList, exceptionType, exceptionName, exceptionBodyList);
    }

    public TypeList typeList(List<Type> typeList) {
        return new JavaTypeList(typeList);
    }

    public TypedAssignmentStatement typedAssignStmt(Type type, CodeExpression lval, CodeExpression expr) {
        return new JavaTypedAssignmentStatement(type, lval, expr);
    }

    public Variable var(String name) {
        return new JavaVariable(name);
    }

    public CodeExpression nullExpr() {
        return new JavaNullExpression();
    }
    public VariableDeclaration varDecl(Type type, Variable variable) {
        return new JavaVariableDeclaration(type, variable);
    }

    public WhileLoop whileLoop(CodeExpression condition, StatementList statementList) {
        return new JavaWhileLoop(condition, statementList);
    }

    public Type typeFromSQLType(String sqlTypeName, boolean isNullable)
    throws TypeNotFoundException {
        return SQLMapType.getSQLMapType(sqlTypeName).getJavaType(this, isNullable);
    }

    public NullSafe nullSafe(CodeExpression codeExpr) {
        return new JavaNullSafe(codeExpr);
    }

    public LambdaClass lambdaClass(Type implementedType, List<FunctionDefinition> functionDefinitionList) {
        return new JavaLambdaClass(implementedType, functionDefinitionList);
    }

    public CodeExpression defaultValue(Type type) {
        if (type instanceof SimpleType) {
            return new JavaVariable(Constants.JAVA_DEFAULTS.get(((SimpleType) type).getName()));
        } else {
            String allocatedGenericTypeName = Constants.JAVA_CONTAINER_DEFAULTS.get(((GenericType) type).getName());
            List<Type> itemList = ((GenericType) type).getItemList();
            return alloc(new JavaGenericType(classType(allocatedGenericTypeName), itemList, false));
        }
    }
}
