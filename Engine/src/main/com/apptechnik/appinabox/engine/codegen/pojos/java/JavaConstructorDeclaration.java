package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.ConstructorDeclaration;
import com.apptechnik.appinabox.engine.codegen.pojos.ParameterList;
import com.apptechnik.appinabox.engine.codegen.pojos.TypeList;

class JavaConstructorDeclaration extends ConstructorDeclaration  {

    public JavaConstructorDeclaration(String qualifier,
                                      String name,
                                      ParameterList parameterList,
                                      TypeList exceptionList) {
        super(qualifier, name, parameterList, exceptionList);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.qualifier + " ");
        sb.append(this.name + "(");
        if (this.parameterList != null) {
            sb.append(this.parameterList);
        }
        sb.append(")");
        if (exceptionList != null) {
            sb.append(" " + Constants.JavaKeywords.THROWS + " " + exceptionList + " ");
        }
        return sb.toString();
    }
}

