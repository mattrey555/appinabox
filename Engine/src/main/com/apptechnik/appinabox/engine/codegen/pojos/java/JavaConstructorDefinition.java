package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.ConstructorDeclaration;
import com.apptechnik.appinabox.engine.codegen.pojos.ConstructorDefinition;
import com.apptechnik.appinabox.engine.codegen.pojos.StatementList;

class JavaConstructorDefinition extends ConstructorDefinition {

    public JavaConstructorDefinition(ConstructorDeclaration declaration, StatementList statementList) {
        super(declaration, statementList);
    }

    @Override
    public String toString() {
        return declaration + "{\n" + statementList + "}\n";
    }
}

