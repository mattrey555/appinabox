package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.ExtendsType;
import com.apptechnik.appinabox.engine.codegen.pojos.Type;

class JavaExtendsType extends ExtendsType {

    public JavaExtendsType(String name, Type type, boolean isNullable) {
        super(name, type, isNullable);
    }

    @Override
    public String toString() {
        return name + " " + Constants.JavaKeywords.EXTENDS + " " + type.toString();
    }
}
