package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.ForIteration;
import com.apptechnik.appinabox.engine.codegen.pojos.Type;
import com.apptechnik.appinabox.engine.codegen.pojos.Variable;

// Type itemType, Variable itemVar, Variable collection
class JavaForIteration extends ForIteration {

    public JavaForIteration(Type itemType, Variable itemVar, Variable collection) {
        super(itemType, itemVar, collection);
    }

    @Override
    public boolean requiresDelimiter() {
        return true;
    }

    @Override
    public String toString() {
        return Constants.JavaKeywords.FOR + " (" + itemType + " " + itemVar + " : " + collection + ") ";
    }
}
