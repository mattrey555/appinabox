package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.ForIteration;
import com.apptechnik.appinabox.engine.codegen.pojos.ForLoop;
import com.apptechnik.appinabox.engine.codegen.pojos.StatementList;

// Type itemType, Variable itemVar, Variable collection
class JavaForLoop extends ForLoop {

    public JavaForLoop(ForIteration iteration, StatementList statementList) {
        super(iteration, statementList);
    }

    @Override
    public boolean requiresDelimiter() {
        return false;
    }

    @Override
    public String toString() {
        return iteration + " {\n" + statementList + "}\n";
    }
}
