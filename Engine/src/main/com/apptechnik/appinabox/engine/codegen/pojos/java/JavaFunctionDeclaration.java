package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.*;

class JavaFunctionDeclaration extends FunctionDeclaration {

    public JavaFunctionDeclaration(boolean override,
                                   boolean suspend,
                                   String accessModifier,
                                   boolean isStatic,
                                   String name,
                                   TypeList genericsList,
                                   ParameterList parameterList,
                                   Type returnType,
                                   TypeList exceptionTypeList,
                                   AnnotationList annotationList) {
        super(override, suspend, accessModifier, isStatic, name, genericsList, parameterList, returnType, exceptionTypeList, annotationList);
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (annotationList != null) {
            sb.append(annotationList);
        }
        sb.append(accessModifier + " ");
        if (isStatic) {
            sb.append(Constants.JavaKeywords.STATIC + " ");
        }
        sb.append(returnType + " " + name + "(");
        if (parameterList != null) {
            sb.append(parameterList);
        }
        sb.append(")");
        if (exceptionTypeList != null) {
            sb.append(" " + Constants.JavaKeywords.THROWS + " " + exceptionTypeList);
        }
        return sb.toString();
    }
}
