package com.apptechnik.appinabox.engine.codegen.pojos.java;
import com.apptechnik.appinabox.engine.codegen.pojos.FunctionDeclaration;
import com.apptechnik.appinabox.engine.codegen.pojos.FunctionDefinition;
import com.apptechnik.appinabox.engine.codegen.pojos.StatementList;

class JavaFunctionDefinition extends FunctionDefinition {

    public JavaFunctionDefinition(FunctionDeclaration functionDeclaration, StatementList statementlist) {
        super(functionDeclaration, statementlist);
    }

    @Override
    public String toString() {
        return functionDeclaration + " {\n" + statementlist + "}";
    }
}
