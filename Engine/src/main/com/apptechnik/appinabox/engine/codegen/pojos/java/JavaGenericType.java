package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.GenericType;
import com.apptechnik.appinabox.engine.codegen.pojos.Type;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.Arrays;
import java.util.List;

/**
 * TODO: This should be renamed to JavaGenericContainerType
 * {@code <container<item, item, item>}
 */
class JavaGenericType extends GenericType {

    public JavaGenericType(Type container, List<Type> itemList, boolean isNullable) {
        super(container, itemList, isNullable);
    }

    @Override
    public String toString() {
        return container + "<" + StringUtil.concat(itemList, ",") + ">";
    }
}
