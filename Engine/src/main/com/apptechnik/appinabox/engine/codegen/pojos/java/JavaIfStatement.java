package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;
import com.apptechnik.appinabox.engine.codegen.pojos.IfStatement;
import com.apptechnik.appinabox.engine.codegen.pojos.StatementList;

class JavaIfStatement extends IfStatement {

    public JavaIfStatement(CodeExpression condition,
                           StatementList ifStatementList,
                           StatementList elseStatementList) {
        super(condition, ifStatementList, elseStatementList);
    }

    @Override
    public boolean requiresDelimiter() {
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Constants.JavaKeywords.IF + " (" + condition + ") {\n");
        sb.append(ifStatementList);
        sb.append("}");
        if (elseStatementList != null) {
            sb.append(" " + Constants.JavaKeywords.ELSE + " {\n");
            sb.append(elseStatementList);
            sb.append("}");
        }
        sb.append("\n");
        return sb.toString();

    }
}
