package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.ImportStatement;

/**
 * I'd call it "import", but there's 1,202,572 packages with the same name.
 */
class JavaImportStatement extends ImportStatement {

    public JavaImportStatement(String packageName) {
        super(packageName);
    }

    @Override
    public boolean requiresDelimiter() {
        return true;
    }

    @Override
    public String toString() {
        return Constants.JavaKeywords.IMPORT + " " + packageName;
    }
}
