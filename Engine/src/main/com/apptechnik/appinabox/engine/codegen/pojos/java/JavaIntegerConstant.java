package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.IntegerConstant;

class JavaIntegerConstant extends IntegerConstant {
    public JavaIntegerConstant(int value) {
        super(value);
    }

    @Override
    public String toString() {
        return Integer.toString(value);
    }
}
