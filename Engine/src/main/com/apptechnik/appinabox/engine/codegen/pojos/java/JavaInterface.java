package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.FunctionDeclaration;
import com.apptechnik.appinabox.engine.codegen.pojos.kotlin.Interface;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.List;

class JavaInterface extends Interface {

    public JavaInterface(List<FunctionDeclaration> interfaceList) {
        super(interfaceList);
    }

    @Override
    public String toString() {
        return StringUtil.delim(interfaceList, ";\n");
    }
}
