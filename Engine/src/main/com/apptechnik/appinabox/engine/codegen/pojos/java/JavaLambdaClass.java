package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.FunctionDefinition;
import com.apptechnik.appinabox.engine.codegen.pojos.LambdaClass;
import com.apptechnik.appinabox.engine.codegen.pojos.Type;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.List;

public class JavaLambdaClass extends LambdaClass {

    public JavaLambdaClass(Type implementedType, List<FunctionDefinition> functionDefinitionList) {
        super(implementedType, functionDefinitionList);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Constants.JavaKeywords.NEW + " : " + implementedType.toString() + "() {\n");
        sb.append(StringUtil.delim(functionDefinitionList, "\n"));
        sb.append("\n}\n");
        return sb.toString();
    }
}
