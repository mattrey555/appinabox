package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.LiteralConstant;

class JavaLiteralConstant extends LiteralConstant {

    public JavaLiteralConstant(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return name;
    }
}
