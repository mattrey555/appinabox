package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.*;

import java.util.List;

/**
 * Generate a Member Declaration of the form:
 * {@code <access-modifier> [static] <type> <variable>}
 */
class JavaMemberDeclaration extends MemberDeclaration {

    public JavaMemberDeclaration(String accessModifier,
                                 boolean isStatic,
                                 boolean isConstant,
                                 Type type,
                                 Variable variable,
                                 List<Annotation> annotationList,
                                 String byProvider,
                                 CodeExpression assignedValue) {
        super(accessModifier, false, isStatic, isConstant, type, variable, annotationList, byProvider, assignedValue);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (annotationList != null) {
            for (Annotation annotation : annotationList) {
                sb.append(annotation + "\n");
            }
        }
        sb.append(accessModifier + " ");
        if (isConstant) {
            sb.append(Constants.JavaKeywords.FINAL + " ");
        }
        if (isStatic) {
            sb.append(Constants.JavaKeywords.STATIC + " ");
        }
        sb.append(type + " " + variable);
        if (assignedValue != null) {
            sb.append(" = " + assignedValue);
        }
        return sb.toString();
    }
}
