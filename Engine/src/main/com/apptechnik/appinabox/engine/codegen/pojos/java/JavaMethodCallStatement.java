package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.MethodCall;
import com.apptechnik.appinabox.engine.codegen.pojos.MethodCallStatement;

class JavaMethodCallStatement extends MethodCallStatement {

    public JavaMethodCallStatement(MethodCall call) {
        super(call);
    }

    @Override
    public boolean requiresDelimiter() {
        return true;
    }

    @Override
    public String toString() {
       return call.toString();
    }
}
