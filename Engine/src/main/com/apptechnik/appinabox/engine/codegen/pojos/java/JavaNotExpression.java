package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;
import com.apptechnik.appinabox.engine.codegen.pojos.NotExpression;

class JavaNotExpression extends NotExpression {

    public JavaNotExpression(CodeExpression codeExpression) {
        super(codeExpression);
    }

    @Override
    public String toString() {
        return "!" + codeExpression.toString();
    }
}
