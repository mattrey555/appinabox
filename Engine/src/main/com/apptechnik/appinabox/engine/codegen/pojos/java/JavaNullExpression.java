package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.NullExpression;

class JavaNullExpression extends NullExpression {
    @Override
    public String toString() {
        return Constants.JavaKeywords.NULL;
    }
}
