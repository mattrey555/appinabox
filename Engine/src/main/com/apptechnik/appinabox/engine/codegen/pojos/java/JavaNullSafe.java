package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;
import com.apptechnik.appinabox.engine.codegen.pojos.NullSafe;

public class JavaNullSafe extends NullSafe {
    public JavaNullSafe(CodeExpression expr) {
        super(expr);
    }
    @Override
    public String toString() {
        return expr.toString();
    }
}
