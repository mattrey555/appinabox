package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.List;

class JavaParameterDeclaration extends ParameterDeclaration {
    public JavaParameterDeclaration(Type type, Variable variable, List<Annotation> annotationList) {
        super(type, variable, annotationList);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (annotationList != null) {
            sb.append(StringUtil.delim(annotationList, "\n"));
        }
        sb.append(type + " " + variable);
        return sb.toString();
    }
}
