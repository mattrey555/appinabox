package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.ParameterDeclaration;
import com.apptechnik.appinabox.engine.codegen.pojos.ParameterList;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.Arrays;
import java.util.List;

public class JavaParameterList extends ParameterList {

    public JavaParameterList(List<ParameterDeclaration> parameterDeclarationList) {
        super(parameterDeclarationList);
    }

    @Override
    public String toString() {
        return StringUtil.concat(parameterDeclarationList, ", ");
    }
}
