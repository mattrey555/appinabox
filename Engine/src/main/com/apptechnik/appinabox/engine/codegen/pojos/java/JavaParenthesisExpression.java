package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;
import com.apptechnik.appinabox.engine.codegen.pojos.ParenthesisExpression;

public class JavaParenthesisExpression extends ParenthesisExpression {

    public JavaParenthesisExpression(CodeExpression expr) {
        super(expr);
    }

    @Override
    public String toString() {
        return "(" + expr + ")";
    }

}
