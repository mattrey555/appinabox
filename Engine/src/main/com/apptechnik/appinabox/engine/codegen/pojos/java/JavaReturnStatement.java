package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;
import com.apptechnik.appinabox.engine.codegen.pojos.ReturnStatement;

public class JavaReturnStatement extends ReturnStatement {

    public JavaReturnStatement(CodeExpression expression) {
        super(expression);
    }

    @Override
    public boolean requiresDelimiter() {
        return true;
    }

    @Override
    public String toString() {
        if (expression != null) {
            return Constants.JavaKeywords.RETURN + " " + expression;
        } else {
            return Constants.JavaKeywords.RETURN;
        }
    }
}
