package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.SimpleType;

class JavaSimpleType extends SimpleType {
    public JavaSimpleType(String name, boolean isNullable) {
        super(name, isNullable);
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof JavaSimpleType) {
            return ((JavaSimpleType) o).getName().equals(getName());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
