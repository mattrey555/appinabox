package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.CodeStatement;
import com.apptechnik.appinabox.engine.codegen.pojos.StatementList;
import com.apptechnik.appinabox.engine.util.FileUtil;

import java.io.IOException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

// TODO really should have "code-block" which adds curly braces.
class JavaStatementList<T extends CodeStatement> extends StatementList<T> {
    public JavaStatementList(List<T> statementList) {
        super(statementList);
    }

    public static JavaStatementList importStatementList(String resourceName) throws IOException {
        String[] importList = FileUtil.getResourceStrings(JavaStatementList.class, resourceName);
        return new JavaStatementList(Arrays.stream(importList)
                                    .map(s -> new JavaImportStatement(s))
                                    .collect(Collectors.toList()));
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        // TODO: change this to requiresDelimiter
        for (CodeStatement codeStatement : statementList) {
            sb.append(codeStatement);
            if (codeStatement.requiresDelimiter()) {
                sb.append(";");
            }
            sb.append("\n");
        }
        return sb.toString();
    }
}
