package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.StringConstant;

class JavaStringConstant extends StringConstant {

    public JavaStringConstant(String name) {
        super(name);
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "\"" + name + "\"";
    }
}
