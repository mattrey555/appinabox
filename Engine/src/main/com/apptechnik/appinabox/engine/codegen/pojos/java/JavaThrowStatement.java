package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;
import com.apptechnik.appinabox.engine.codegen.pojos.ReturnStatement;
import com.apptechnik.appinabox.engine.codegen.pojos.ThrowStatement;

public class JavaThrowStatement extends ThrowStatement {

    public JavaThrowStatement(CodeExpression expression) {
        super(expression);
    }

    @Override
    public boolean requiresDelimiter() {
        return true;
    }

    @Override
    public String toString() {
        if (expression != null) {
            return Constants.JavaKeywords.THROW + " " + expression;
        } else {
            return Constants.JavaKeywords.THROW;
        }
    }
}
