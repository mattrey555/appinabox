package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.StatementList;
import com.apptechnik.appinabox.engine.codegen.pojos.TryCatch;
import com.apptechnik.appinabox.engine.codegen.pojos.Type;
import com.apptechnik.appinabox.engine.codegen.pojos.Variable;

class JavaTryCatch extends TryCatch {

    public JavaTryCatch(StatementList bodyList,
                        Type exceptionType,
                        Variable exceptionName,
                        StatementList exceptionBodyList) {
        super(bodyList, exceptionType, exceptionName, exceptionBodyList);
    }

    @Override
    public boolean requiresDelimiter() {
        return false;
    }

    @Override
    public String toString() {
        return Constants.JavaKeywords.TRY + " {\n" + bodyList + "} " +
               Constants.JavaKeywords.CATCH + "(" + exceptionType + " " + exceptionName + ") {\n" +
               exceptionBodyList + "}\n";
    }
}
