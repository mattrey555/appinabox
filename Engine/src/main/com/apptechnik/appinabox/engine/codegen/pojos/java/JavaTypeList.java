package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.Type;
import com.apptechnik.appinabox.engine.codegen.pojos.TypeList;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.Arrays;
import java.util.List;

public class JavaTypeList extends TypeList {

    public JavaTypeList(List<Type> typeList) {
        super(typeList);
    }

    @Override
    public String toString() {
        return StringUtil.concat(typeList, ",");
    }
}
