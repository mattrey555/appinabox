package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.SimpleType;
import com.apptechnik.appinabox.engine.codegen.pojos.TypeVariable;

class JavaTypeVariable extends TypeVariable {
    public JavaTypeVariable(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof JavaTypeVariable) {
            return ((JavaTypeVariable) o).getName().equals(getName());
        }
        return false;
    }
}
