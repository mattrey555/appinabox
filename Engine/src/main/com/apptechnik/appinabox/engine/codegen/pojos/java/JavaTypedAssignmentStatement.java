package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;
import com.apptechnik.appinabox.engine.codegen.pojos.Type;
import com.apptechnik.appinabox.engine.codegen.pojos.TypedAssignmentStatement;

class JavaTypedAssignmentStatement extends TypedAssignmentStatement {

    public JavaTypedAssignmentStatement(Type type, CodeExpression lval, CodeExpression expr) {
        super(type, lval, expr);
    }

    @Override
    public boolean requiresDelimiter() {
        return true;
    }

    @Override
    public String toString() { return type + " " + lval + " = " + expression; }
}
