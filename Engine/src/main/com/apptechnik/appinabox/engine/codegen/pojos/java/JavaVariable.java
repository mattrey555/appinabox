package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.Variable;

class JavaVariable extends Variable {
    public JavaVariable(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return name;
    }
}
