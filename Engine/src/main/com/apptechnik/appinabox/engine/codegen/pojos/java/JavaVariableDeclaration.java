package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.codegen.pojos.Type;
import com.apptechnik.appinabox.engine.codegen.pojos.Variable;
import com.apptechnik.appinabox.engine.codegen.pojos.VariableDeclaration;

public class JavaVariableDeclaration extends VariableDeclaration {

    public JavaVariableDeclaration(Type type, Variable variable) {
        super(type, variable);
    }

    @Override
    public boolean requiresDelimiter() {
        return true;
    }

    @Override
    public String toString() {
        return type + " " + variable;
    }
}
