package com.apptechnik.appinabox.engine.codegen.pojos.java;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;
import com.apptechnik.appinabox.engine.codegen.pojos.StatementList;
import com.apptechnik.appinabox.engine.codegen.pojos.WhileLoop;

public class JavaWhileLoop extends WhileLoop {

    public JavaWhileLoop(CodeExpression condition,
                         StatementList statementList) {
        super(condition, statementList);
    }

    @Override
    public boolean requiresDelimiter() {
        return false;
    }

    @Override
    public String toString() {
        return Constants.JavaKeywords.WHILE + " (" + condition.toString() + ") {\n" +
                statementList.toString() + "}\n";
    }
}
