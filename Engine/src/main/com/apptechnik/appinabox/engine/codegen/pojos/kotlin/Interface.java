package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.pojos.FunctionDeclaration;

import java.util.List;

public abstract class Interface {
    protected final List<FunctionDeclaration> interfaceList;

    public Interface(List<FunctionDeclaration> interfaceList) {
        this.interfaceList = interfaceList;
    }

    public List<FunctionDeclaration> getInterfaceList() {
        return interfaceList;
    }

    public abstract String toString();
}
