package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.*;

class KotlinAllocation extends Allocation {

    public KotlinAllocation(Type type, ArgumentList constructorArgs, StatementList inlineStatements) {
        super(type, constructorArgs, inlineStatements);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getType());
        if (constructorArgs != null) {
            sb.append("(" + constructorArgs + ")");
        } else {
            sb.append("()");
        }
        if (inlineStatements != null) {
            sb.append("{");
            sb.append(inlineStatements);
            sb.append("}");
        }
        return sb.toString();
    }
}
