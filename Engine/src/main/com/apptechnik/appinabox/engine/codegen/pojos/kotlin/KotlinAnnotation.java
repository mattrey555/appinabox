package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.pojos.AnnotationArg;
import com.apptechnik.appinabox.engine.codegen.pojos.ArgumentList;
import com.apptechnik.appinabox.engine.codegen.pojos.Annotation;

import java.util.List;

class KotlinAnnotation extends Annotation {
    public KotlinAnnotation(String name, List<AnnotationArg> argList) {
        super(name, argList);
    }

    public boolean requiresDelimiter() {
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("@" + name);
        if ((args != null) && !args.isEmpty()) {
            sb.append("(" + args + ")");
        }
        return sb.toString();
    }
}
