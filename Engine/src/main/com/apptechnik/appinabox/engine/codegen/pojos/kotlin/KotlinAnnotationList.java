package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.pojos.Annotation;
import com.apptechnik.appinabox.engine.codegen.pojos.AnnotationList;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.List;

public class KotlinAnnotationList extends AnnotationList {

    public KotlinAnnotationList(List<Annotation> methodAnnotationList) {
        super(methodAnnotationList);
    }

    @Override
    public String toString() {
        return StringUtil.delim(annotationList, "\n");
    }
}

