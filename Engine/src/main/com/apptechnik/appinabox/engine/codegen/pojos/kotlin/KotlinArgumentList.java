package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.pojos.ArgumentList;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.List;

class KotlinArgumentList extends ArgumentList {

    public KotlinArgumentList(List<CodeExpression> argumentList) {
        super(argumentList);
    }

    @Override
    public String toString() {
        return StringUtil.concat(argumentList, ", ");
    }
}
