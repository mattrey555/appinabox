package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.pojos.ArrayReference;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;

import java.util.List;

class KotlinArrayReference extends ArrayReference {

    public KotlinArrayReference(CodeExpression expr, List<CodeExpression> indexList) {
        super(expr, indexList);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(expr);
        for (CodeExpression index : indexList) {
            sb.append("[" + index +"]");
        }
        return sb.toString();
    }
}
