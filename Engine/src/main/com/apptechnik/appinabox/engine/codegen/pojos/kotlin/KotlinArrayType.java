package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.pojos.ArrayType;
import com.apptechnik.appinabox.engine.codegen.pojos.Type;
import com.apptechnik.appinabox.library.util.StringUtils;

// TODO: this is not correct Kotlin Array type definition.
class KotlinArrayType extends ArrayType {

    public KotlinArrayType(Type item, int dimensions, boolean isNullable) {
        super(item, dimensions, isNullable);
    }

    @Override
    public String toString() {
        return item + StringUtils.repeat("[]", "", dimensions);
    }
}
