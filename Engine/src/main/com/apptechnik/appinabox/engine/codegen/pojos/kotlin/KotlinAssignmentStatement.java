package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.pojos.AssignmentStatement;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;

class KotlinAssignmentStatement extends AssignmentStatement {

    public KotlinAssignmentStatement(CodeExpression lval, CodeExpression expression) {
        super(lval, expression);
    }

    @Override
    public boolean requiresDelimiter() {
        return false;
    }

    @Override
    public String toString() {
        return lval + " = " + expression;
    }
}
