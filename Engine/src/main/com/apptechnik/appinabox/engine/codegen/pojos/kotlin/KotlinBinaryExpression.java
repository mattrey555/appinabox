package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.pojos.CodeBinaryExpression;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;

/**
 * I'd call it Expression, but if conflicts with the SQL parser exception.
 */
class KotlinBinaryExpression extends CodeBinaryExpression {

    public KotlinBinaryExpression(String operator, CodeExpression left, CodeExpression right) {
        super(operator, left, right);
    }

    @Override
    public String toString() {
        return left.toString() + operator + right.toString();
    }
}
