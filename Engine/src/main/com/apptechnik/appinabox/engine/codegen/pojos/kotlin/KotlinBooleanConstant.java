package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.pojos.BooleanConstant;

class KotlinBooleanConstant extends BooleanConstant {

    public KotlinBooleanConstant(boolean value) {
        super(value);
    }

    @Override
    public String toString() {
        return Boolean.toString(value);
    }
}
