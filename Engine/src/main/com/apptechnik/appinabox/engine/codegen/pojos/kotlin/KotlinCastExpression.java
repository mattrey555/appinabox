package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.CastExpression;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;
import com.apptechnik.appinabox.engine.codegen.pojos.Type;

class KotlinCastExpression extends CastExpression {

    public KotlinCastExpression(Type type, CodeExpression expr) {
        super(type, expr);
    }

    @Override
    public String toString() {
        return expr + " " + Constants.KotlinKeywords.AS + " " + type;
    }
}
