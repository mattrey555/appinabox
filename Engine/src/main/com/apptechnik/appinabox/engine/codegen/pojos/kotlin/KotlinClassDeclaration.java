package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.codegen.util.CodegenUtil;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Generate an expression of the form:
 * {@code public class <className> [extends <extends-list>] [implements <implements-list>] }
 */
class KotlinClassDeclaration extends ClassDeclaration {

    public KotlinClassDeclaration(boolean isStatic,
                                  boolean isFinal,
                                  boolean isData,
                                  String className,
                                  TypeList genericsList,
                                  Type extendsType,
                                  TypeList implementsList,
                                  List<Annotation> annotationList) {
        super(isStatic, isFinal, isData, className, genericsList, extendsType, implementsList, annotationList);
    }

    private String prefixes() {
        StringBuilder sb = new StringBuilder();
        if (annotationList != null) {
            sb.append(StringUtil.delim(annotationList, "\n"));
        }
        sb.append(this.accessQualifier + " ");

        if (!isFinal) {
            sb.append(Constants.KotlinKeywords.OPEN + " ");
        }
        if (isStatic) {
            sb.append(Constants.KotlinKeywords.STATIC + " ");
        }
        if (isData) {
            sb.append(Constants.KotlinKeywords.DATA + " ");
        }
        sb.append(Constants.JavaKeywords.CLASS + " " + this.className);
        if (genericsList != null) {
            sb.append("<");
            sb.append(genericsList);
            sb.append(">");
        }
        return sb.toString();
    }

    private String suffix() {
        StringBuilder sb = new StringBuilder();
        if (implementsList != null) {
            if (extendsType != null) {
                sb.append(", ");
            } else {
                sb.append(" : ");
            }
            sb.append(implementsList);
        }
        return sb.toString();
    }

    /**
     * The class declaration does not have the member declarations for the constructor, which is moved from the
     * class definition to the constructor.
     * @return
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(prefixes());
        if (extendsType != null) {
            sb.append(" : ");
            sb.append(extendsType);
        }
        sb.append(suffix());
        return sb.toString();
    }

    private String inlineConstructors(ClassDefinition parentClass, List<MemberDeclaration> memberDeclarationList) {
        StringBuilder sb = new StringBuilder();
        // TODO: remap the generic type names to the concrete types used in this class from extendsType and the
        // parent class generics list.
        List<MemberDeclaration> memberParams = new ArrayList<MemberDeclaration>(parentClass.getMemberDeclarationList());
        if (memberDeclarationList != null) {
            memberParams.addAll(memberDeclarationList);
        }
        ParameterList constructorParamList = remapGenerics(constructorArgs(memberParams), extendsType);
        sb.append("(");
        sb.append(constructorParamList);
        sb.append(")");
        if (parentClass.getMemberDeclarationList() != null) {
            ParameterList parentConstructorParamList = constructorArgs(parentClass.getMemberDeclarationList());
            List<CodeExpression> parentConstructorArgList = CodegenUtil.declarationsToParameters(parentConstructorParamList.getParameterDeclarationList());
            sb.append(" : ");
            sb.append(extendsType);
            sb.append("(");
            sb.append(StringUtil.concat(parentConstructorArgList, ", "));
            sb.append(")");
        }
        return sb.toString();
    }

    /**
     * Generate the Kotlin Class definition.
     * @param parentClass
     *
     * @return
     */
    public String toString(ClassDefinition parentClass, List<MemberDeclaration> memberDeclarationList) {
        StringBuilder sb = new StringBuilder();
        sb.append(prefixes());
        sb.append(inlineConstructors(parentClass, memberDeclarationList));
        sb.append(suffix());
        return sb.toString();
    }

    /**
     * Kotlin-specific where the constructor is declared in the class declaration:
     * class Foo(val bar : Int, val baz : Float)
     * @param memberDeclarationList list of members initialized in constructor.
     * @return
     */
    public String toString(List<MemberDeclaration> memberDeclarationList) {
        StringBuilder sb = new StringBuilder();
        sb.append(prefixes());
        sb.append("(");
        if (memberDeclarationList != null) {
            sb.append(constructorArgs(memberDeclarationList));
        }
        sb.append(")");
        if (extendsType != null) {
            sb.append(": ");
            sb.append(extendsType);
        }
        sb.append(suffix());
        return sb.toString();
    }

    /**
     * Transform the member list to the Kotlin Constructor arguments
     * @param memberDeclarationList
     * @return
     */
    public ParameterList constructorArgs(List<MemberDeclaration> memberDeclarationList) {
        return new KotlinParamList(memberDeclarationList.stream()
                .map(member -> new KotlinParamDeclaration(Constants.KotlinKeywords.VAL, member.getType(), member.getVariable(), member.getAnnotationList()))
                .collect(Collectors.toList()));
    }

    /**
     * change:
     * val inventory_id : Int?, val title : String?, val release_year : Short, val last_update : D
     * and
     * film_inventoryMixin<D: Date?>
     * to:
     * val inventory_id : Int?, val title : String?, val release_year : Short, val last_update : Date?
     * @param paramList
     * @param extendsType
     * @return
     */
    private ParameterList remapGenerics(ParameterList paramList, Type extendsType) {
        if ((extendsType == null) || (!(extendsType instanceof GenericType))) {
            return paramList;
        }
        GenericType genericExtendsType = (GenericType) extendsType;
        List<ParameterDeclaration> remappedDeclarationList = new ArrayList<ParameterDeclaration>();

        // list of concrete types mapped to generics in parent class.
        List<Type> typeList = genericExtendsType.getItemList();
        int concreteTypeIndex = 0;
        for (ParameterDeclaration decl : paramList.getParameterDeclarationList()) {
            if (decl.getType() instanceof TypeVariable) {
                Type concreteType = typeList.get(concreteTypeIndex);
                remappedDeclarationList.add(new KotlinParamDeclaration(concreteType, decl.getVariable(), decl.getAnnotationList()));
                concreteTypeIndex++;
            } else if (decl.getType() instanceof GenericType) {

                // NOTE: this only handles a single TypeVariable, which is all we generate, but it will break in
                // the generic case.
                GenericType genericType = (GenericType) decl.getType();
                if (genericType.getItemList().get(0) instanceof TypeVariable) {
                    Type concreteType = typeList.get(concreteTypeIndex++);
                    remappedDeclarationList.add(remapGenericParam(concreteType, decl));
                } else {
                    remappedDeclarationList.add(new KotlinParamDeclaration(decl.getType(), decl.getVariable(), decl.getAnnotationList()));
                }
            } else {
                remappedDeclarationList.add(new KotlinParamDeclaration(decl.getType(), decl.getVariable(), decl.getAnnotationList()));
            }
        }
        return new KotlinParamList(remappedDeclarationList);
    }

    KotlinParamDeclaration remapGenericParam(Type concreteType, ParameterDeclaration decl) {
        Type container = ((GenericType) decl.getType()).getContainer();
        KotlinGenericType remappedType = new KotlinGenericType(container, Collections.singletonList(concreteType), container.isNullable());
        return new KotlinParamDeclaration(remappedType, decl.getVariable(), decl.getAnnotationList());
    }
}
