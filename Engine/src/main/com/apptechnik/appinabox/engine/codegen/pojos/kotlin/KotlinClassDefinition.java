package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * A class definition is:
 * TODO: data class definition constructor.
 * {@code
 * <classDeclaration> {
 * <memberDeclarationList>
 * <constructorDefinitionList>
 * <methodDefinitionList>
 * }}
 */
class KotlinClassDefinition extends ClassDefinition {

    public KotlinClassDefinition(ClassDeclaration classDeclaration,
                                 List<MemberDeclaration> memberDeclarationList,
                                 List<ConstructorDefinition> constructorDefinitionList,
                                 List<FunctionDefinition> functionDefinitionList,
                                 List<ClassDefinition> innerClassList,
                                 List<InterfaceDeclaration> innerInterfaceList) {
        super(classDeclaration, memberDeclarationList, constructorDefinitionList, functionDefinitionList,
              innerClassList, innerInterfaceList);
     }

    /**
     * Filter the list of static members for the companion object.
     * @return
     */
     private List<MemberDeclaration> staticMembers() {
        return (memberDeclarationList != null) ?
                memberDeclarationList.stream()
                    .filter(m -> m.isStatic())
                    .collect(Collectors.toList()) :
                new ArrayList<MemberDeclaration>();
     }

    /**
     * Filter list of non-static members for class.
     * @return
     */
     private List<MemberDeclaration> nonStaticMembers() {
         return (memberDeclarationList != null) ?
                    memberDeclarationList.stream()
                        .filter(m -> !m.isStatic())
                        .collect(Collectors.toList()) :
                 new ArrayList<MemberDeclaration>();
     }

    /**
     * Filter list of static methods for companion object.
     * @return
     */
     private List<FunctionDefinition> staticMethods() {
        return (methodDefinitionList != null) ?
                methodDefinitionList.stream()
                    .filter(m -> m.getFunctionDeclaration().isStatic())
                    .collect(Collectors.toList()) :
                new ArrayList<FunctionDefinition>();
     }

    /**
     * Filter list of non-static methods for class.
     * @return
     */
    private List<FunctionDefinition> nonStaticMethods() {
        return (methodDefinitionList != null) ?
                methodDefinitionList.stream()
                    .filter(m -> !m.getFunctionDeclaration().isStatic())
                    .collect(Collectors.toList()) :
                new ArrayList<FunctionDefinition>();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (classDeclaration.isData()) {
            sb.append(getClassDeclaration().toString(memberDeclarationList));
        } else {
            sb.append(classDeclaration);
        }
        sb.append(bodyToString());
        return sb.toString();
    }

    /**
     * An utterly horrifying exception case, where we have to reference the parent class in order to create the
     * parent constructor.
     * @param parentClass
     * @return
     */
    public String toString(ClassDefinition parentClass) {
        StringBuilder sb = new StringBuilder();
        if (classDeclaration.isData()) {
            KotlinClassDeclaration classDecl = (KotlinClassDeclaration) getClassDeclaration();
            sb.append(classDecl.toString(parentClass, memberDeclarationList));
        } else {
            sb.append(classDeclaration);
        }
        sb.append(bodyToString());
        return sb.toString();
    }

    public String bodyToString() {
        StringBuilder sb = new StringBuilder();
        sb.append(" {\n");
        List<MemberDeclaration> staticMembers = staticMembers();
        List<FunctionDefinition> staticMethods = staticMethods();
        if (!staticMembers.isEmpty() || !staticMethods.isEmpty()) {
            sb.append(Constants.KotlinKeywords.COMPANION + " " + Constants.KotlinKeywords.OBJECT + " {\n");
            sb.append(StringUtil.delim(staticMembers, "\n"));
            sb.append(StringUtil.delim(staticMethods, "\n"));
            sb.append("\n}\n");
        }
        // data classes have members declared in their constructor, and only have the generated constructor.
        if (!classDeclaration.isData()) {
            List<MemberDeclaration> nonStaticMembers = nonStaticMembers();
            if (nonStaticMembers != null) {
                sb.append(StringUtil.delim(nonStaticMembers, "\n"));
            }
            if (constructorDefinitionList != null) {
                sb.append(StringUtil.delim(constructorDefinitionList, "\n"));
            }
        }
        List<FunctionDefinition> nonStaticMethods = nonStaticMethods();
        if (nonStaticMethods != null) {
            sb.append(StringUtil.delim(nonStaticMethods, "\n"));
        }
        if (innerClassList != null) {
            sb.append(StringUtil.delim(innerClassList, "\n"));
        }
        if (innerInterfaceList != null) {
            sb.append(StringUtil.delim(innerInterfaceList, "\n"));
        }
        sb.append("}\n");
        return sb.toString();
    }
}
