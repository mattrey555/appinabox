package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.objects.CreateCommon;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.util.IndentingFileWriter;
import com.apptechnik.appinabox.engine.util.OutputPaths;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Yes, this class is meant to be public and visible outside of the Kotlin Code Generator.
 * There is a Kotlin-specific case, where the parent class is used
 */
public class KotlinClassFile extends ClassFile {

    public KotlinClassFile(PackageStatement packageStatement,
                           StatementList importStatements,
                           ClassDefinition classDefinition) {
        super(packageStatement, importStatements, classDefinition);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(packageStatement + "\n");
        sb.append(importStatements + "\n");
        sb.append(classDefinition + "\n");
        return sb.toString();
    }

    /**
     * Please, forgive me for this exception case.
     * NOTE: comment as to why this was done this way.
     * @param parentClass
     * @return string
     */
    public String toString(ClassDefinition parentClass) {
        StringBuilder sb = new StringBuilder();
        sb.append(packageStatement + "\n");
        sb.append(importStatements + "\n");
        sb.append(((KotlinClassDefinition) classDefinition).toString(parentClass) + "\n");
        return sb.toString();
    }

    @Override
    public void write(File outputDir) throws Exception {
        String packageName = packageStatement.getName();
        String className = classDefinition.getClassDeclaration().getClassName();
         File outputFile = new File(OutputPaths.androidKotlinDir(outputDir, packageName), className + Constants.Extensions.KOTLIN);
        IndentingFileWriter ifwSqlFns = new IndentingFileWriter(new FileWriter(outputFile));
        ifwSqlFns.write(this.toString());
        ifwSqlFns.close();
    }

    /**
     * Horrifying exception case for Kotlin classes where the parent class constructor is part
     * of the class declaration.
     * @param outputDir base output directory
     * @param parentClass
     * @throws IOException on any kind of file open/write exception.
     */
    public void write(File outputDir, ClassDefinition parentClass) throws Exception {
        String packageName = packageStatement.getName();
        String className = classDefinition.getClassDeclaration().getClassName();
        File outputFile = new File(OutputPaths.androidKotlinDir(outputDir, packageName), className + Constants.Extensions.KOTLIN);
        IndentingFileWriter ifwSqlFns = new IndentingFileWriter(new FileWriter(outputFile));
        ifwSqlFns.write(this.toString(parentClass));
        ifwSqlFns.close();
    }
}
