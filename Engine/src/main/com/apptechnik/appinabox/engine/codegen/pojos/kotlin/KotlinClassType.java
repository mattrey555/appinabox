package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.pojos.ClassType;

class KotlinClassType extends ClassType {
    public KotlinClassType(String name) {
        super(name);
    }
    public KotlinClassType(String name, boolean isNullable) {
        super(name, isNullable);
    }

    public KotlinClassType(ClassType simpleType) {

    }

    @Override
    public String toString() {
        return isNullable ? name + "?" : name;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof KotlinClassType) {
            return ((KotlinClassType) o).getName().equals(getName());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
