
package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.exceptions.TypeNotFoundException;
import com.apptechnik.appinabox.engine.parser.types.SQLMapType;
import com.apptechnik.appinabox.engine.util.FileUtil;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class KotlinCodeGen extends CodeGen {
    public Allocation alloc(Type type, ArgumentList constructorArgs, StatementList inlineStatements) {
        return new KotlinAllocation(type, constructorArgs, inlineStatements);
    }

    public ArgumentList argList(List<CodeExpression> argumentList) {
        return new KotlinArgumentList(argumentList);
    }

    public ArrayReference arrayRef(CodeExpression expr, List<CodeExpression> indexList) {
        return new KotlinArrayReference(expr, indexList);
    }

    public ArrayType arrayType(Type item, int dimensions, boolean isNullable) {
        return new KotlinArrayType(item, dimensions, isNullable);
    }

    public AssignmentStatement assignStmt(CodeExpression lval, CodeExpression expression) {
        return new KotlinAssignmentStatement(lval, expression);
    }

    public BooleanConstant booleanConst(boolean value) {
        return new KotlinBooleanConstant(value);
    }

    public CastExpression castExpr(Type type, CodeExpression expr) {
        return new KotlinCastExpression(type, expr);
    }

    public Annotation classAnnot(String name) {
        return new KotlinAnnotation(name, null);
    }

    public InterfaceDeclaration interfaceDecl(String interfaceName,
                                              TypeList genericsList,
                                              TypeList implementsList,
                                              List<FunctionDeclaration> methodList,
                                              List<Annotation> annotationList) {
        return new KotlinInterfaceDeclaration(interfaceName, genericsList, implementsList, methodList, annotationList);
    }

    public ClassDeclaration classDecl(boolean isStatic,
                                      boolean isFinal,
                                      boolean isData,
                                      String className,
                                      TypeList genericsList,
                                      Type extendsType,
                                      TypeList implementsList,
                                      List<Annotation> annotationList) {
        return new KotlinClassDeclaration(isStatic, isFinal, isData, className, genericsList, extendsType, implementsList, annotationList);
    }

    public ClassDefinition classDef(ClassDeclaration classDeclaration,
                                    List<MemberDeclaration> memberDeclarationList,
                                    List<ConstructorDefinition> constructorDefinitionList,
                                    List<FunctionDefinition> functionDefinitionList,
                                    List<ClassDefinition> innerClassList,
                                    List<InterfaceDeclaration> innerInterfaceList) {
        return new KotlinClassDefinition(classDeclaration, memberDeclarationList,
                                         constructorDefinitionList, functionDefinitionList, innerClassList, innerInterfaceList);
    }

    public InterfaceFile interfaceFile(PackageStatement packageStatement,
                                        StatementList importStatements,
                                        InterfaceDeclaration interfaceDeclaration) {
        return new KotlinInterfaceFile(packageStatement, importStatements, interfaceDeclaration);
    }

    public ClassFile classFile(PackageStatement packageStatement,
                                StatementList importStatements,
                                ClassDefinition classDefinition) {
        return new KotlinClassFile(packageStatement, importStatements, classDefinition);
    }

    public CodeBinaryExpression binaryExpr(String operator, CodeExpression left, CodeExpression right) {
        return new KotlinBinaryExpression(operator, left, right);
    }

    public CodeExpression parenExpr(CodeExpression expr) {
        return new KotlinParenthesisExpression(expr);
    }

    public ConstructorDeclaration constructorDecl(String qualifier,
                                                           String name,
                                                           ParameterList parameterList,
                                                           TypeList exceptionList) {
        return new KotlinConstructorDeclaration(qualifier, name, parameterList, exceptionList);
    }

    public ConstructorDefinition constructorDef(ConstructorDeclaration declaration, StatementList statementList) {
        return new KotlinConstructorDefinition(declaration, statementList);
    }

    public ExtendsType extendsType(String name, Type type, boolean isNullable) {
        return new KotlinExtendsType(name, type, isNullable);
    }

    public ForIteration forIter(Type itemType, Variable itemVar, Variable collection) {
        return new KotlinForIteration(itemType, itemVar, collection);
    }

    public ForLoop forLoop(ForIteration iteration, StatementList statementList) {
        return new KotlinForLoop(iteration, statementList);
    }

    public FunctionDeclaration functionDecl(boolean override,
                                            boolean suspend,
                                            String accessModifier,
                                            boolean isStatic,
                                            String name,
                                            TypeList genericsList,
                                            ParameterList parameterList,
                                            Type returnType,
                                            TypeList exceptionTypeList,
                                            AnnotationList methodAnnotationList) {
        return new KotlinFunctionDeclaration(override, suspend, accessModifier, isStatic, name, genericsList, parameterList,
                                             returnType, exceptionTypeList, methodAnnotationList);
    }

    public FunctionDefinition functionDef(FunctionDeclaration functionDeclaration, StatementList statementlist) {
        return new KotlinFunctionDefinition(functionDeclaration, statementlist);
    }

    public GenericType genericType(Type container, List<Type> itemList, boolean isNullable) {
        return new KotlinGenericType(container, itemList, isNullable);
    }

    public GenericType listType(Type elementType) {
        return genericType(classType(Constants.KotlinTypes.LIST), elementType);
    }

    public GenericType arrayListType(Type elementType) {
        return genericType(classType(Constants.KotlinFunctions.MUTABLE_LIST_OF), elementType);
    }

    public IfStatement ifStmt(CodeExpression condition, StatementList ifStatementList, StatementList elseStatementList) {
        return new KotlinIfStatement(condition, ifStatementList, elseStatementList);
    }

    public ImportStatement importStmt(String packageName) {
        return new KotlinImportStatement(packageName);
    }

    /**
     * Create a list of imports from a resource file
     *
     * @param resourceName
     * @return
     * @throws IOException
     */
    public StatementList importStatementList(String resourceName) throws IOException {
        String[] importList = FileUtil.getResourceStrings(KotlinCodeGen.class, resourceName);
        return new KotlinStatementList(Arrays.stream(importList)
                .map(s -> new KotlinImportStatement(s))
                .collect(Collectors.toList()));
    }

    public IntegerConstant integerConst(int value) {
        return new KotlinIntegerConstant(value);
    }

    public LambdaExpression lambdaExpr(Variable var, CodeExpression expr) {
        return new KotlinLambdaExpression(var, expr);
    }

    public LiteralConstant literalConst(String name) {
        return new KotlinLiteralConstant(name);
    }

    public MemberDeclaration memberDecl(String accessModifier,
                                        boolean isLateinit,
                                        boolean isStatic,
                                        boolean isConstant,
                                        Type type,
                                        Variable variable,
                                        List<Annotation> annotationList,
                                        String byProvider,
                                        CodeExpression assignedValue) {
        return new KotlinMemberDeclaration(accessModifier, isLateinit, isStatic, isConstant, type, variable, annotationList, byProvider, assignedValue);
    }

    public AnnotationList methodAnnotList(List<Annotation> methodAnnotationList) {
        return new KotlinAnnotationList(methodAnnotationList);
    }

    public Annotation annotation(String name, List<AnnotationArg> args) {
        return new Annotation(name, args);
    }

    public AnnotationArrayConstant annotationArrayConstant(List<Constant> list) {
        return new KotlinAnnotationArrayConsant(list);
    }

    public MethodCall methodCall(CodeExpression caller, String methodName, ArgumentList argList) {
        return new KotlinMethodCall(caller, methodName, argList);
    }

    public MethodCallStatement methodCallStmt(MethodCall call) {
        return new KotlinMethodCallStatement(call);
    }

    public NotExpression notExpr(CodeExpression codeExpression) {
        return new KotlinNotExpression(codeExpression);
    }

    public PackageStatement packageStmt(String name) {
        return new KotlinPackageStatement(name);
    }

    public AnnotationArg annotationArg(String name, CodeExpression value) {
        return new AnnotationArg(name, value);
    }

    public Annotation paramAnnot(String name, List<AnnotationArg> argList) {
        return new Annotation(name, argList);
    }

    public ParameterDeclaration paramDecl(Type type, Variable variable, List<Annotation> annotationList) {
        return new KotlinParamDeclaration(type, variable, annotationList);
    }

    public ParameterList paramList(List<ParameterDeclaration> parameterDeclarationList) {
        return new KotlinParamList(parameterDeclarationList);
    }

    public ReturnStatement returnStmt(CodeExpression expression) {
        return new KotlinReturnStatement(expression);
    }

    public ThrowStatement throwStmt(CodeExpression expression) {
        return new KotlinThrowStatement(expression);
    }

    public boolean isNullableType(String typeName) {
        return typeName.charAt(typeName.length() - 1) == '?';
    }

    public String nullableTypeName(String typeName) {
        return typeName.substring(0, typeName.length() - 1);
    }
    /**
     * Is Type primitive of a generic of boxed primitives?
     * We only support generics with one argument.
     * @param type
     * @return
     */
    public boolean isSimpleType(Type type) {
        String typeName = type.getName();
        if (type instanceof SimpleType) {
            if (isNullableType(typeName)) {
                return StringUtil.in(Constants.PRIMITIVE_KOTLIN_TYPES, nullableTypeName(typeName));
            } else {
                return StringUtil.in(Constants.PRIMITIVE_KOTLIN_TYPES, typeName);
            }
        } else if (type instanceof GenericType) {
            GenericType genericType = (GenericType) type;
            return genericType.getContainer().getName().equals(Constants.KotlinTypes.LIST) &&
                    (genericType.getItemList().size() == 1) && isSimpleType(genericType.getItemList().get(0));
        }
        return false;
    }

    public SimpleType simpleType(String name, boolean isNullable) {
        return new KotlinSimpleType(name, isNullable);
    }

    public ClassType classType(String name, boolean isNullable) {
        return new KotlinClassType(name, isNullable);
    }

    public TypeVariable typeVariable(String name) {
        return new KotlinTypeVariable(name);
    }

    public StatementList stmtList(List statementList) {
        return new KotlinStatementList(statementList);
    }

    public StringConstant stringConst(String name) {
        return new KotlinStringConstant(name);
    }

    public TryCatch tryCatch(StatementList bodyList,
                             Type exceptionType,
                             Variable exceptionName,
                             StatementList exceptionBodyList) {
        return new KotlinTryCatch(bodyList, exceptionType, exceptionName, exceptionBodyList);
    }

    public TypeList typeList(List<Type> typeList) {
        return new KotlinTypeList(typeList);
    }

    public TypedAssignmentStatement typedAssignStmt(Type type, CodeExpression lval, CodeExpression expr) {
        return new KotlinTypedAssignmentStatement(type, lval, expr);
    }

    public Variable var(String name) {
        return new KotlinVariable(name);
    }

    public CodeExpression nullExpr() {
        return new KotlinNullExpression();
    }
    public VariableDeclaration varDecl(Type type, Variable variable) {
        return new KotlinVariableDeclaration(type, variable);
    }

    public WhileLoop whileLoop(CodeExpression condition, StatementList statementList) {
        return new KotlinWhileLoop(condition, statementList);
    }

    public Type typeFromSQLType(String sqlTypeName, boolean isNullable) throws TypeNotFoundException {
        return SQLMapType.getSQLMapType(sqlTypeName).getKotlinType(this, isNullable);
    }

    public NullSafe nullSafe(CodeExpression codeExpr) {
        return new KotlinNullSafe(codeExpr);
    }

    public LambdaClass lambdaClass(Type implementedType, List<FunctionDefinition> functionDefinitionList) {
        return new KotlinLambdaClass(implementedType, functionDefinitionList);
    }

    public CodeExpression defaultValue(Type type) {
        if (type instanceof SimpleType) {
            return new KotlinVariable(Constants.KOTLIN_DEFAULTS.get(((SimpleType) type).getName()));
        } else if (type instanceof GenericType) {
            // special case for lists in Kotlin
            if (type.getName().equals(Constants.KotlinTypes.LIST)) {
                return methodCall(Constants.KotlinFunctions.MUTABLE_LIST_OF, (ArgumentList) null);
            }
        }
        return alloc(type);
    }
}
