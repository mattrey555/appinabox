package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.ConstructorDeclaration;
import com.apptechnik.appinabox.engine.codegen.pojos.ParameterList;
import com.apptechnik.appinabox.engine.codegen.pojos.TypeList;

class KotlinConstructorDeclaration extends ConstructorDeclaration  {

    public KotlinConstructorDeclaration(String qualifier,
                                        String name,
                                        ParameterList parameterList,
                                        TypeList exceptionList) {
        super(qualifier, name, parameterList, exceptionList);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Constants.KotlinKeywords.CONSTRUCTOR + "(");
        if (this.parameterList != null) {
            sb.append(this.parameterList);
        }
        sb.append(")");

        // TODO: review kotlin exception throw clauses.
        if (exceptionList != null) {
            sb.append(" " + Constants.JavaKeywords.THROWS + " " + exceptionList + " ");
        }
        return sb.toString();
    }
}

