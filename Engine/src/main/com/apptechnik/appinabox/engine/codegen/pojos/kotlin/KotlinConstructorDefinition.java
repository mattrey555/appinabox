package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.pojos.ConstructorDeclaration;
import com.apptechnik.appinabox.engine.codegen.pojos.ConstructorDefinition;
import com.apptechnik.appinabox.engine.codegen.pojos.StatementList;

class KotlinConstructorDefinition extends ConstructorDefinition {

    public KotlinConstructorDefinition(ConstructorDeclaration declaration, StatementList statementList) {
        super(declaration, statementList);
    }

    @Override
    public String toString() {
        return declaration + "{\n" + statementList + "}\n";
    }
}

