package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.ExtendsType;
import com.apptechnik.appinabox.engine.codegen.pojos.Type;

class KotlinExtendsType extends ExtendsType {

    public KotlinExtendsType(String name, Type type, boolean isNullable) {
        super(name, type, isNullable);
    }

    @Override
    public String toString() {
        if (isNullable) {
            return name + " : " + type.toString() + "?";
        } else {
            return name + " : " + type.toString();
        }
    }
}
