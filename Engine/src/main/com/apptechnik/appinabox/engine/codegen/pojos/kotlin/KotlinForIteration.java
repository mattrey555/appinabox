package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.ForIteration;
import com.apptechnik.appinabox.engine.codegen.pojos.Type;
import com.apptechnik.appinabox.engine.codegen.pojos.Variable;

// Type itemType, Variable itemVar, Variable collection
class KotlinForIteration extends ForIteration {

    public KotlinForIteration(Type itemType, Variable itemVar, Variable collection) {
        super(itemType, itemVar, collection);
    }

    @Override
    public boolean requiresDelimiter() {
        return true;
    }

    @Override
    public String toString() {
        return Constants.JavaKeywords.FOR +
                " (" + itemType + " " + itemVar + " " + Constants.KotlinKeywords.IN + " " + collection + ") ";
    }
}
