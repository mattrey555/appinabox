package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.pojos.ForIteration;
import com.apptechnik.appinabox.engine.codegen.pojos.ForLoop;
import com.apptechnik.appinabox.engine.codegen.pojos.StatementList;

// Type itemType, Variable itemVar, Variable collection
class KotlinForLoop extends ForLoop {

    public KotlinForLoop(ForIteration iteration, StatementList statementList) {
        super(iteration, statementList);
    }

    @Override
    public boolean requiresDelimiter() {
        return false;
    }

    @Override
    public String toString() {
        return iteration + " {\n" + statementList + "}\n";
    }
}
