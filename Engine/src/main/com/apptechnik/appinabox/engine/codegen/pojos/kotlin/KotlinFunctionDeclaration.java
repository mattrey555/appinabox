package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.*;

class KotlinFunctionDeclaration extends FunctionDeclaration {

    public KotlinFunctionDeclaration(boolean override,
                                     boolean suspend,
                                     String accessModifier,
                                     boolean isStatic,
                                     String name,
                                     TypeList genericsList,
                                     ParameterList parameterList,
                                     Type returnType,
                                     TypeList exceptionTypeList,
                                     AnnotationList annotationList) {
        super(override, suspend, accessModifier, isStatic, name, genericsList, parameterList, returnType, exceptionTypeList, annotationList);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (annotationList != null) {
            sb.append(annotationList);
        }
        if (override) {
            sb.append(Constants.KotlinKeywords.OVERRIDE + " ");
        }
        if (suspend) {
            sb.append(Constants.KotlinKeywords.SUSPEND + " ");
        }
        sb.append(accessModifier + " ");
        sb.append(Constants.KotlinKeywords.FUN + " ");
        if (genericsList != null) {
            sb.append(" <" + genericsList + "> ");
        }
        sb.append(name + "(");
        if (parameterList != null) {
            sb.append(parameterList);
        }
        sb.append(")");
        if ((returnType != null) && !returnType.getName().equals(Constants.JavaTypes.VOID)) {
            sb.append(" : " + returnType);
        }
        // TODO: review kotlin throws exception clause
        if (exceptionTypeList != null) {
            sb.append(" " + Constants.JavaKeywords.THROWS + " " + exceptionTypeList);
        }
        return sb.toString();
    }
}
