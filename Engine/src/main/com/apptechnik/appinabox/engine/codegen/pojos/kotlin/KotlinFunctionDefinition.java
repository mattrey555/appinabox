package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;
import com.apptechnik.appinabox.engine.codegen.pojos.FunctionDeclaration;
import com.apptechnik.appinabox.engine.codegen.pojos.FunctionDefinition;
import com.apptechnik.appinabox.engine.codegen.pojos.StatementList;

class KotlinFunctionDefinition extends FunctionDefinition {

    public KotlinFunctionDefinition(FunctionDeclaration functionDeclaration, StatementList statementlist) {
        super(functionDeclaration, statementlist);
    }

    @Override
    public String toString() {
        return functionDeclaration + " {\n" + statementlist + "}";
    }
}
