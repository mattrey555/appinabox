package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.pojos.GenericType;
import com.apptechnik.appinabox.engine.codegen.pojos.Type;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.List;

/**
 * {@code <container<item, item, item>}
 */
class KotlinGenericType extends GenericType {

    public KotlinGenericType(Type container, List<Type> itemList, boolean isNullable) {
        super(container, itemList, isNullable);
    }

    @Override
    public String toString() {
        if (isNullable) {
            return container + "<" + StringUtil.concat(itemList, ",") + ">?";
        } else {
            return container + "<" + StringUtil.concat(itemList, ",") + ">";
        }
    }
}
