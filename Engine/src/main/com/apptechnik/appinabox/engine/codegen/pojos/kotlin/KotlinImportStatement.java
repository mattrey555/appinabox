package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.ImportStatement;

/**
 * I'd call it "import", but there's 1,202,572 packages with the same name.
 */
class KotlinImportStatement extends ImportStatement {

    public KotlinImportStatement(String packageName) {
        super(packageName);
    }

    @Override
    public boolean requiresDelimiter() {
        return false;
    }

    @Override
    public String toString() {
        return Constants.JavaKeywords.IMPORT + " " + packageName;
    }
}
