package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.pojos.IntegerConstant;

class KotlinIntegerConstant extends IntegerConstant {
    public KotlinIntegerConstant(int value) {
        super(value);
    }

    @Override
    public String toString() {
        return Integer.toString(value);
    }
}
