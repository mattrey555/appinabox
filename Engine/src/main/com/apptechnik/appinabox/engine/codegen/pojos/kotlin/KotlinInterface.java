package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.pojos.FunctionDeclaration;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.List;

class KotlinInterface extends Interface {

    public KotlinInterface(List<FunctionDeclaration> interfaceList) {
        super(interfaceList);
    }

    @Override
    public String toString() {
        return StringUtil.concat(interfaceList, "\n");
    }
}
