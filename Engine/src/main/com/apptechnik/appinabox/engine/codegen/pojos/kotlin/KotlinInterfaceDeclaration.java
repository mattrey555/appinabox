package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.List;

/**
 * Generate an expression of the form:
 * {@code public interface <className> [<generics-list>][implements <implements-list>] }
 */
public class KotlinInterfaceDeclaration extends InterfaceDeclaration {

    public KotlinInterfaceDeclaration(String interfaceName,
                                      TypeList genericsList,
                                      TypeList implementsList,
                                      List<FunctionDeclaration> methodDeclarationList,
                                      List<Annotation> annotationList) {
        super(interfaceName, genericsList, implementsList, methodDeclarationList, annotationList);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (annotationList != null) {
            for (Annotation annotation : annotationList) {
                sb.append(annotation + "\n");
            }
        }
        sb.append(Constants.JavaKeywords.INTERFACE + " ");
        sb.append(interfaceName + " ");
        if (genericsList != null) {
            sb.append("<" + genericsList + "> ");
        }
        if (implementsList != null) {
            sb.append(implementsList + " ");
        }
        sb.append(" {\n");
        if (methodDeclarationList != null) {
            sb.append(StringUtil.delim(methodDeclarationList, "\n"));
        }
        sb.append("\n}\n");
        return sb.toString();
    }
}