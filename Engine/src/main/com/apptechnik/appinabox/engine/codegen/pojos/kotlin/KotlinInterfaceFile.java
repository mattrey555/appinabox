package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.objects.CreateCommon;
import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.util.IndentingFileWriter;

import java.io.IOException;

class KotlinInterfaceFile extends InterfaceFile {

    public KotlinInterfaceFile(PackageStatement packageStatement,
                               StatementList importStatements,
                               InterfaceDeclaration interfaceDeclaration) {
        super(packageStatement, importStatements, interfaceDeclaration);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(packageStatement + "\n");
        sb.append(importStatements + "\n");
        sb.append(interfaceDeclaration + "\n");
        return sb.toString();
    }

    /**
     * Used the package name as a directory, and the class name as the file name to create a file
     * to write this class + package + imports to.
     * @param createCommon base directory references to write objects to.
     * @throws IOException on any kind of file open/write exception.
     */
    public void write(CreateCommon createCommon) throws Exception {
        String packageName = packageStatement.getName();
        String interfaceName = interfaceDeclaration.getInterfaceName();
        IndentingFileWriter ifwSqlFns = createCommon.openKotlinFile(packageName, interfaceName);
        ifwSqlFns.write(this.toString());
        ifwSqlFns.close();
    }
}
