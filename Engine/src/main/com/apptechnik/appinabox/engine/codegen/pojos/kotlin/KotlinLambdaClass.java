package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;
import com.apptechnik.appinabox.engine.codegen.pojos.FunctionDefinition;
import com.apptechnik.appinabox.engine.codegen.pojos.LambdaClass;
import com.apptechnik.appinabox.engine.codegen.pojos.Type;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.List;

/**
 * Example:
 * object : Callback<T>> {
 *      override fun onResponse(call : Call<T>, response : Response<T> ) {
 *          listOfMovies.setValue(response.body());
 *      }
 *      override fun onFailure(call : Call<T>, t : Throwable) {
 *          errorMessage.postValue(t.message);
 *      }
 * }
 */
public class KotlinLambdaClass extends LambdaClass {

    public KotlinLambdaClass(Type implementedType, List<FunctionDefinition> functionDefinitionList) {
        super(implementedType, functionDefinitionList);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Constants.KotlinKeywords.OBJECT + " : " + implementedType.toString() + " {\n");
        sb.append(StringUtil.delim(functionDefinitionList, "\n"));
        sb.append("\n}\n");
        return sb.toString();
    }
}
