package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;
import com.apptechnik.appinabox.engine.codegen.pojos.LambdaExpression;
import com.apptechnik.appinabox.engine.codegen.pojos.Variable;

/**
 * Generate a lambda expression of the form:
 * {@code var -> expression}
 */
public class KotlinLambdaExpression extends LambdaExpression {

    public KotlinLambdaExpression(Variable var, CodeExpression expr) {
        super(var, expr);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(var.toString());
        sb.append(" -> ");
        sb.append(expr.toString());
        return sb.toString();
    }
}
