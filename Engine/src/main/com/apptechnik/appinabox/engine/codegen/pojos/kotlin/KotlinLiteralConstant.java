package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.pojos.LiteralConstant;

class KotlinLiteralConstant extends LiteralConstant {

    public KotlinLiteralConstant(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return name;
    }
}
