package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.List;

/**
 * Generate a Member Declaration of the form:
 * {@code <access-modifier> [static] <type> <variable>}
 */
class KotlinMemberDeclaration extends MemberDeclaration {

    public KotlinMemberDeclaration(String accessModifier,
                                   boolean isLateinit,
                                   boolean isStatic,
                                   boolean isConstant,
                                   Type type,
                                   Variable variable,
                                   List<Annotation> annotationList,
                                   String byProvider,
                                   CodeExpression assignedValue) {
        super(accessModifier, isLateinit, isStatic, isConstant, type, variable, annotationList, byProvider, assignedValue);
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (annotationList != null) {
            sb.append(StringUtil.delim(annotationList, "\n"));
        }
        sb.append(accessModifier + " ");
        if (isLateinit) {
            sb.append(Constants.KotlinKeywords.LATEINIT + " ");
        }
        sb.append(isConstant ? Constants.KotlinKeywords.VAL : Constants.KotlinKeywords.VAR);
        sb.append(" ");
        // kotlin uses companion objects for statics.

        sb.append(variable + " : " + type);
        if (byProvider != null) {
            sb.append(Constants.KotlinKeywords.BY + " " + byProvider);
        }
        if (assignedValue != null) {
            sb.append(" = " + assignedValue);
        }
        return sb.toString();
    }
}
