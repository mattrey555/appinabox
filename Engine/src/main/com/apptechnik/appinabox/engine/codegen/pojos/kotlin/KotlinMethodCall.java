package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.pojos.ArgumentList;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;
import com.apptechnik.appinabox.engine.codegen.pojos.MethodCall;

class KotlinMethodCall extends MethodCall {
    public KotlinMethodCall(CodeExpression caller, String methodName, ArgumentList argList) {
        super(caller, methodName, argList);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        if (caller != null) {
            sb.append(caller + ".");
        }
        sb.append(methodName + "(");
        if (argList != null) {
            sb.append(argList);
        }
        sb.append(")");
        return sb.toString();
    }
}
