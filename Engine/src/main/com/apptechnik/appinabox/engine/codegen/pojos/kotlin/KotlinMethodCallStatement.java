package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.pojos.MethodCall;
import com.apptechnik.appinabox.engine.codegen.pojos.MethodCallStatement;

class KotlinMethodCallStatement extends MethodCallStatement {

    public KotlinMethodCallStatement(MethodCall call) {
        super(call);
    }

    @Override
    public boolean requiresDelimiter() {
        return false;
    }

    @Override
    public String toString() {
       return call.toString();
    }
}
