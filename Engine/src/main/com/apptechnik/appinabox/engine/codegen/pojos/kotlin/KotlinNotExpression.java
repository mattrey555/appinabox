package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;
import com.apptechnik.appinabox.engine.codegen.pojos.NotExpression;

class KotlinNotExpression extends NotExpression {

    public KotlinNotExpression(CodeExpression codeExpression) {
        super(codeExpression);
    }

    @Override
    public String toString() {
        return "!" + codeExpression.toString();
    }
}
