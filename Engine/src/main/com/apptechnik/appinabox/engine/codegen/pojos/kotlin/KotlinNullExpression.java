package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.NullExpression;

class KotlinNullExpression extends NullExpression {
    @Override
    public String toString() {
        return Constants.JavaKeywords.NULL;
    }
}
