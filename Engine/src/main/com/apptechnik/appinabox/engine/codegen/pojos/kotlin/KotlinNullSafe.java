package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;
import com.apptechnik.appinabox.engine.codegen.pojos.NullSafe;

public class KotlinNullSafe extends NullSafe {
    public KotlinNullSafe(CodeExpression expr) {
        super(expr);
    }
    @Override
    public String toString() {
        return expr.toString() + "?";
    }
}
