package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.PackageStatement;

/**
 * I'd call it Expression, but if conflcits with the SQL parser exception.
 */
class KotlinPackageStatement extends PackageStatement {

    public KotlinPackageStatement(String name) {
        super(name);
    }

    @Override
    public boolean requiresDelimiter() {
        return false;
    }

    @Override
    public String toString() {
        return Constants.JavaKeywords.PACKAGE + " " + name;
    }
}
