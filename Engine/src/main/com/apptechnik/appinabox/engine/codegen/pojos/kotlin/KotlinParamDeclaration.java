package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.List;

class KotlinParamDeclaration extends ParameterDeclaration {
    public KotlinParamDeclaration(String memberPrefix, Type type, Variable variable, List<Annotation> annotationList) {
        super(memberPrefix, type, variable, annotationList);
    }

    public KotlinParamDeclaration(Type type, Variable variable, List<Annotation> annotationList) {
        this(null, type, variable, annotationList);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (annotationList != null) {
            sb.append(StringUtil.delim(annotationList, "\n"));
        }
        if (memberPrefix != null) {
            sb.append(memberPrefix + " ");
        }
        sb.append(variable + " : " + type);
        return sb.toString();
    }
}
