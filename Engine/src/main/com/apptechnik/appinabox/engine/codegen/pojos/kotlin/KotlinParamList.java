package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.pojos.ParameterDeclaration;
import com.apptechnik.appinabox.engine.codegen.pojos.ParameterList;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.List;

public class KotlinParamList extends ParameterList {

    public KotlinParamList(List<ParameterDeclaration> parameterDeclarationList) {
        super(parameterDeclarationList);
    }

    @Override
    public String toString() {
        return StringUtil.concat(parameterDeclarationList, ", ");
    }
}
