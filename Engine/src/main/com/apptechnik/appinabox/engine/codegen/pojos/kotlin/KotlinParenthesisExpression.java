package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;
import com.apptechnik.appinabox.engine.codegen.pojos.ParenthesisExpression;

public class KotlinParenthesisExpression extends ParenthesisExpression {

    public KotlinParenthesisExpression(CodeExpression expr) {
        super(expr);
    }

    @Override
    public String toString() {
        return "(" + expr + ")";
    }

}
