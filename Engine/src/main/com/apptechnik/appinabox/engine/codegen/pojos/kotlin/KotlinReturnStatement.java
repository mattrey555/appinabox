package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;
import com.apptechnik.appinabox.engine.codegen.pojos.ReturnStatement;

public class KotlinReturnStatement extends ReturnStatement {

    public KotlinReturnStatement(CodeExpression expression) {
        super(expression);
    }

    @Override
    public boolean requiresDelimiter() {
        return false;
    }

    @Override
    public String toString() {
        if (expression != null) {
            return Constants.JavaKeywords.RETURN + " " + expression;
        } else {
            return Constants.JavaKeywords.RETURN;
        }
    }
}
