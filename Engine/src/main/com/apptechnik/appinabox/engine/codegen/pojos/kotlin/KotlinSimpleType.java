package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.pojos.SimpleType;

class KotlinSimpleType extends SimpleType {
    public KotlinSimpleType(String name) {
        super(name);
    }
    public KotlinSimpleType(String name, boolean isNullable) {
        super(name, isNullable);
    }

    public KotlinSimpleType(SimpleType simpleType) {

    }

    @Override
    public String toString() {
        return isNullable ? name + "?" : name;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof KotlinSimpleType) {
            return ((KotlinSimpleType) o).getName().equals(getName());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
