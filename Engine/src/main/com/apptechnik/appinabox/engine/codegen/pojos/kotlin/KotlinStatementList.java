package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.pojos.CodeStatement;
import com.apptechnik.appinabox.engine.codegen.pojos.StatementList;
import com.apptechnik.appinabox.engine.util.FileUtil;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

// TODO really should have "code-block" which adds curly braces.
class KotlinStatementList<T extends CodeStatement> extends StatementList<T> {
    public KotlinStatementList(List<T> statementList) {
        super(statementList);
    }

    public static KotlinStatementList importStatementList(String resourceName) throws IOException {
        String[] importList = FileUtil.getResourceStrings(KotlinStatementList.class, resourceName);
        return new KotlinStatementList(Arrays.stream(importList)
                                    .map(s -> new KotlinImportStatement(s))
                                    .collect(Collectors.toList()));
    }

    @Override
    public String toString() {
        return StringUtil.delim(statementList, "\n");
    }
}
