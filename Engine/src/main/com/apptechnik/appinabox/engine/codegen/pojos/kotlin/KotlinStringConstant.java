package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.pojos.StringConstant;

class KotlinStringConstant extends StringConstant {

    public KotlinStringConstant(String name) {
        super(name);
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "\"" + name + "\"";
    }
}
