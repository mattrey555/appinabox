package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;
import com.apptechnik.appinabox.engine.codegen.pojos.ReturnStatement;
import com.apptechnik.appinabox.engine.codegen.pojos.ThrowStatement;

public class KotlinThrowStatement extends ThrowStatement {

    public KotlinThrowStatement(CodeExpression expression) {
        super(expression);
    }

    @Override
    public boolean requiresDelimiter() {
        return false;
    }

    @Override
    public String toString() {
        if (expression != null) {
            return Constants.KotlinKeywords.THROW + " " + expression;
        } else {
            return Constants.KotlinKeywords.THROW;
        }
    }
}
