package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.pojos.Type;
import com.apptechnik.appinabox.engine.codegen.pojos.TypeList;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.List;

public class KotlinTypeList extends TypeList {

    public KotlinTypeList(List<Type> typeList) {
        super(typeList);
    }

    @Override
    public String toString() {
        return StringUtil.concat(typeList, ",");
    }
}
