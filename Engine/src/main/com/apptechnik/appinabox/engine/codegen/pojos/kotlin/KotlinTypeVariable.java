package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.pojos.TypeVariable;

class KotlinTypeVariable extends TypeVariable {
    public KotlinTypeVariable(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof KotlinTypeVariable) {
            return ((KotlinTypeVariable) o).getName().equals(getName());
        }
        return false;
    }
}
