package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;
import com.apptechnik.appinabox.engine.codegen.pojos.Type;
import com.apptechnik.appinabox.engine.codegen.pojos.TypedAssignmentStatement;

class KotlinTypedAssignmentStatement extends TypedAssignmentStatement {

    public KotlinTypedAssignmentStatement(Type type, CodeExpression lval, CodeExpression expr) {
        super(type, lval, expr);
    }

    @Override
    public boolean requiresDelimiter() {
        return false;
    }

    @Override
    public String toString() {
        if (type != null) {
            return Constants.KotlinKeywords.VAL + " " + lval + " : " + type + " = " + expression;
        } else {
            return Constants.KotlinKeywords.VAL + " " + lval + " = " + expression;
        }
    }
}
