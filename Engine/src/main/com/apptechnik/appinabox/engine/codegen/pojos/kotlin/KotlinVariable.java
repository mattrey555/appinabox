package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.pojos.Variable;

class KotlinVariable extends Variable {
    public KotlinVariable(String name) {
        super(name);
    }

    @Override
    public String toString() {
        return name;
    }
}
