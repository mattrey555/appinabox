package com.apptechnik.appinabox.engine.codegen.pojos.kotlin;

import com.apptechnik.appinabox.engine.codegen.pojos.Type;
import com.apptechnik.appinabox.engine.codegen.pojos.Variable;
import com.apptechnik.appinabox.engine.codegen.pojos.VariableDeclaration;

public class KotlinVariableDeclaration extends VariableDeclaration {

    public KotlinVariableDeclaration(Type type, Variable variable) {
        super(type, variable);
    }

    @Override
    public boolean requiresDelimiter() {
        return false;
    }

    @Override
    public String toString() {
        return variable + " " + type;
    }
}
