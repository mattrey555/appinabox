package com.apptechnik.appinabox.engine.codegen.query;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.codegen.util.CodegenUtil;
import com.apptechnik.appinabox.engine.exceptions.PropertiesException;
import com.apptechnik.appinabox.engine.exceptions.UndefinedParameterException;
import com.apptechnik.appinabox.engine.parser.*;
import com.apptechnik.appinabox.engine.parser.pojos.*;
import com.apptechnik.appinabox.engine.parser.query.SelectTableUtils;
import com.apptechnik.appinabox.engine.parser.util.JsonParameterMap;
import com.apptechnik.appinabox.engine.parser.util.JsonPathMatcher;
import com.apptechnik.appinabox.engine.util.*;

import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Create two objects, one in the target directory, which inherits from the generated code
 * in the "generated" subdirectory. The programmer can edit the object in the target directory
 * which contains the @Assign and @RemoveFields annotations
 */
public class CreateQueryObjects {
    private final CreateObjects createObjects;                      // common properties
    private String responseName;
    private List<Select> selectStatements;

    public CreateQueryObjects(CreateObjects createObjects, List<Select> selectStatements) throws Exception {
        this.createObjects = createObjects;
        this.selectStatements = selectStatements;
        this.responseName = StringUtil.findTag(this.createObjects.getComments(), Constants.Directives.NAME_RESPONSE);
        if (createObjects.getJsonResponseTree() == null) {
            throw new PropertiesException("in " + createObjects.getSqlFile().getFile().getAbsolutePath() +
                                          " does not specify a response JSON map");
        }
    }

    /**
     * Parse the JSON response tree form the json-response directive.  The top level node uses "name".
     * Generate objects for each node in the JSON response tree.
     * @throws Exception
     */
    public void createObjectFiles() throws Exception {
        executeOnSelects(this.createObjects);
    }

    /**
     * For each select statement:
     *  map them to the tables they reference.
     *  find the node in the JSON tree they reference.
     *  recurse the SELECT statement to find the primary keys from each referenced table, and insert
     *  them into the SELECT items list during the recursion.
     *  Map JDBC NamedParameters -> types.
     *  apply fixed types from the type: directive.
     *  map the SELECT statement to the columns and keys
     * @param createObjects common data for creating POJOs
     * @param jsonResponseTree JSON Mapping for response
     * @return map of select statements to associated columns and keys.
     * @throws Exception general failure.
     */
    public static Map<Select, ColumnsAndKeys> resolveSelects(CreateObjects createObjects,
                                                             Tree<QueryFieldsNode> jsonResponseTree) throws Exception {
        Map<Select, ColumnsAndKeys> selectColumnsAndKeysMap = new HashMap<Select, ColumnsAndKeys>();
        List<Select> selectStatements = createObjects.getSelectStatementList();
        for (int i = 0; i < selectStatements.size(); i++) {
            Select select = selectStatements.get(i);
            PlainSelect plainSelect = (PlainSelect) select.getSelectBody();
            TableInfoMap selectTableInfoMap = SelectTableUtils.selectTables(select, createObjects.getTableInfoMap());
            Tree<QueryFieldsNode> jsonTree = getAssociatedJsonTree(selectStatements, i, jsonResponseTree);
            ParameterMap parameterMap = createObjects.getStatementParamMap(select);
            List<String> undefinedParameters = parameterMap.undefinedParams(createObjects.getQueryParams());
            undefinedParameters = createObjects.getJsonParameterMap().filterUndefinedParams(undefinedParameters);
            if (!undefinedParameters.isEmpty()) {
                throw new UndefinedParameterException("parameters " + StringUtil.concat(undefinedParameters, ",") +
                        " used in " + select + " not found in query parameters " +
                        StringUtil.concat(createObjects.getQueryParams().keySet(), ","));
            }
            ExpressionType.getNamedParameterTypes(select, selectTableInfoMap, createObjects.getTableInfoMap());
            selectColumnsAndKeysMap.put(select, SelectTableUtils.resolveColumns(plainSelect, jsonTree, parameterMap,
                                                                                selectTableInfoMap));
        }
        return selectColumnsAndKeysMap;
    }

    /**
     * For each select statement:
     *  map them to the tables they reference.
     *  find the node in the JSON tree they reference.
     *  recurse the SELECT statement to find the primary keys from each referenced table, and insert
     *  them into the SELECT items list during the recursion.
     *  Map JDBC NamedParameters -> types.
     *  apply fixed types from the type: directive.
     *  map the SELECT statement to the parameter map.
     *  Create the java objects created from the JSON applied to the SELECT statement, with fixed types applied.
     * @param createObjects common data for creating POJOs
     * @throws Exception general failure.
     */
    private void executeOnSelects(CreateObjects createObjects) throws Exception {
        Map<Select, ColumnsAndKeys> selectColumnsAndKeysMap =
                resolveSelects(createObjects, createObjects.getJsonResponseTree());
        for (int i = 0; i < selectStatements.size(); i++) {
            Select select = selectStatements.get(i);
            ColumnsAndKeys columnsAndKeys = selectColumnsAndKeysMap.get(select);
            Tree<QueryFieldsNode> jsonTree = getAssociatedJsonTree(selectStatements, i, createObjects.getJsonResponseTree());
            ParameterMap parameterMap = createObjects.getStatementParamMap(select);
            ResponseJavaObjects.createJavaObjects(createObjects, jsonTree, select, columnsAndKeys, parameterMap);
        }
    }

    /**
     * Create the list of assignments for the top-level object with multiple select list calls.
     * The top-level node of the JSON tree contains the names of the fields in the top-level object,
     * the type of the field, and its SQL functions class .
     * The parameter map contains the list of parameters for the SelectList calls.
     * @param jsonTree top-level node of the JSON tree (children are fields of interest.
     * @param selectList list of select calls 1:1 Ordered to children of JSON tree
     * @param dbConnectionName variable name for the database connection.
     * @param objectName variable name of the top-level object
     * @param selectParameterMap map of SQL statements to their parameter maps.
     * @param jsonParameterMap parameters extracted from request JSON
     * @return list of assignment statements of the form:
     * {@code <objectName>.<field> = <field>SQLFns.selectList(<dbconn>, <param>, <param>...)}
     */
    public static StatementList selectListFieldAssignments(CodeGen codegen,
                                                           Tree<QueryFieldsNode> jsonTree,
                                                           List<Select> selectList,
                                                           String dbConnectionName,
                                                           String objectName,
                                                           ParameterMap selectParameterMap,
                                                           JsonParameterMap jsonParameterMap,
                                                           boolean singleFieldRequest) {
        List<CodeStatement> statementList = new ArrayList<CodeStatement>();
        Variable var = codegen.var(objectName);
        for (int i = 0; i < selectList.size(); i++) {
            String fieldName = jsonTree.getChildAt(i).get().getVarName();
            MethodCall assignment = createSelectSetFieldCall(codegen, var, dbConnectionName, fieldName, selectParameterMap,
                                                             jsonParameterMap, singleFieldRequest);
            statementList.add(codegen.methodCallStmt(assignment));
        }
        return codegen.stmtList(statementList);
    }

    /**
     * Create an expression of the form:
     * <var> = <Object>SQLFns.selectList(<dbconn>, <param>, <param>...)
     * @param var variable to assign to.
     * @param dbConnectionName variable name for the database connection.
     * @param objectName variable name of the top-level object
     * @param parameterMap SQL statement parameter map from arguments
     * @param jsonParameterMap parameters from request JSON.
     * @return
     */
    private static MethodCall createSelectSetFieldCall(CodeGen codegen,
                                                       Variable var,
                                                       String dbConnectionName,
                                                       String objectName,
                                                       ParameterMap parameterMap,
                                                       JsonParameterMap jsonParameterMap,
                                                       boolean singleFieldRequest) {
        MethodCall selectListMethodCall = selectListCall(codegen, dbConnectionName, objectName, parameterMap,
                                                         jsonParameterMap, singleFieldRequest);
        return codegen.methodCall(var, CodegenUtil.setterNameFromField(objectName), selectListMethodCall);
    }

    /**
     * Create a select list assignment statement of the form:
     * {@code List<<objectName>> <listName> = <Object>SQLFns.selectList(<dbconn>, <param>, <param>...) }
     * @param dbConnectionName database connection variable name
     * @param objectName type name of list
     * @param parameterMap JDBC Named paramters from select statements
     * @param jsonParameterMap parameters extracted from request JSON
     * @return typed assignment statement
     */
    public static MethodCallStatement selectListAssignment(CodeGen codegen,
                                                           String dbConnectionName,
                                                           String objectName,
                                                           ParameterMap parameterMap,
                                                           JsonParameterMap jsonParameterMap,
                                                           boolean singleFieldRequest) {
        MethodCall selectListMethodCall = selectListCall(codegen, dbConnectionName, objectName, parameterMap, jsonParameterMap, singleFieldRequest);
        return codegen.methodCallStmt(codegen.methodCall(codegen.var(Constants.Variables.REQUEST_RESPONSE),
                                      Constants.Functions.SET_OUTPUT, selectListMethodCall));
    }

    /**
     * Create an expression of the form:
     * <Object>SQLFns.selectList(<dbconn>, <param>, <param>...)
     * @param dbConnectionName database connection variable name
     * @param objectName type name of list
     * @param parameterMap parameter map from query for arguments.
     * @param jsonParameterMap parameters extracted from request JSON
     */
    private static MethodCall selectListCall(CodeGen codegen,
                                             String dbConnectionName,
                                             String objectName,
                                             ParameterMap parameterMap,
                                             JsonParameterMap jsonParameterMap,
                                             boolean singleFieldRequest) {
        List<CodeExpression> expressionList = new ArrayList<CodeExpression>();
        expressionList.add(codegen.var(dbConnectionName));

        // filter out the non-scalar parameters that we read from the request JSON
        Variable requestResponseVar = codegen.var(Constants.Variables.REQUEST_RESPONSE);
        parameterMap.getParamSet().stream()
            .filter(CheckedPredicate.wrap(param -> !parameterMap.isExtract(param)))
            .forEach(param -> expressionList.add(CodegenUtil.accessorCall(codegen, requestResponseVar, param)));

        // so they can be extracted from the request JSON....
        jsonParameterMap.getParamMapSet().stream()
            .forEach(pathMatch -> expressionList.add(extractListCall(codegen, requestResponseVar, pathMatch, singleFieldRequest)));
        return codegen.methodCall(codegen.var(objectName + Constants.Suffixes.SQL_FNS),
                                  Constants.Functions.SELECT_LIST, codegen.argList(expressionList));
    }

    /**
     * Generate the call to extract a list of boxed primitives from a Request object and the path specification,
     * given the "trace" of nodes from traversing the hierarchy
     * @param codegen
     * @param requestResponseVar name of the RequestResponse Variable
     * @param pathMatch node trace
     * @param singleFieldRequest true: request/response is a list of boxed primitives,
     *                           false: request/response is a complex object, on which the extract method is called.
     * @return
     */
    private static MethodCall extractListCall(CodeGen codegen,
                                              Variable requestResponseVar,
                                              JsonPathMatcher.PathMatch<MergedFieldNode> pathMatch,
                                              boolean singleFieldRequest) {
        if (singleFieldRequest) {
            return CodegenUtil.accessorCall(codegen, requestResponseVar, Constants.Variables.INPUT);
        } else {
            // <root-class-name>.extract_film_id(requestResponse.getInput())
            return codegen.methodCall(codegen.var(pathMatch.getMatchNodes().get(0).get().getVarName()),
                                      pathMatch.getExtractMethodName(),
                                      CodegenUtil.accessorCall(codegen, requestResponseVar, Constants.Variables.INPUT));

        }
    }

    /**
     * When we have multiple selects, each matches a child of the root node, which we match
     * by index parallel to the select calls. If there's one select statement, it matches to the JSON root.
     * @param selectList list of select statements.
     * @param index child index. Ignored if selectList.size() == 1
     * @param jsonTree
     * @return
     */
    public static Tree<QueryFieldsNode> getAssociatedJsonTree(List<Select> selectList,
                                                              int index,
                                                              Tree<QueryFieldsNode> jsonTree) {
        if (selectList.size() == 1) {
            return jsonTree;
        } else {
            return jsonTree.getChildAt(index);
        }
    }


    /**
     * If there are multiple SELECT statements, rather than returning a list of data, we return a object containing
     * the lists of results from each SELECT call.
     * @param createObjects used to generate query package name for sublists returned from SELECT calls,
     *                      and get the package name.
     * @param importsFileName file containing standard imports for this objecct.
     * @param fieldRefTree to get name of current node and children to generate setters, accessors, and fields.
     * @return
     * @throws Exception
     */
    public static ClassFile createMultipleSelectObject(CodeGen codegen,
                                                       CreateObjects createObjects,
                                                       String importsFileName,
                                                       Tree<QueryFieldsNode> fieldRefTree) throws Exception {
        QueryFieldsNode node = fieldRefTree.get();
        PackageStatement packageStatement = codegen.packageStmt(createObjects.getQueryPackageName());
        StatementList importStatementList = codegen.importStatementList(importsFileName);
        importStatementList.addStatement(codegen.importStmt(createObjects.getQueryPackageName() + "." + Constants.Packages.WILDCARD));
        ConstructorDeclaration constructorDeclaration = codegen.constructorDecl(node.getVarName());
        List<CodeStatement> constructorStatements = fieldRefTree.getChildNodes().stream()
                .map(child -> child.childListAllocation(codegen))
                .collect(Collectors.toList());
        ConstructorDefinition constructorDef = codegen.constructorDef(constructorDeclaration, codegen.stmtList(constructorStatements));
        List<FunctionDefinition> accessorsAndSetters = FieldReferenceNode.childAccessorsAndSetters(codegen, fieldRefTree);
        ClassDeclaration classDecl = codegen.classDecl(node.getVarName(), codegen.classAnnot(Constants.Annotations.GENERATED));
        ClassDefinition classDef = codegen.classDef(classDecl,
                                                    ResponseObjects.rootChildFieldDeclarations(codegen, fieldRefTree, Constants.JavaKeywords.PRIVATE),
                                                    Collections.singletonList(constructorDef),
                                                    accessorsAndSetters);
        return codegen.classFile(packageStatement, importStatementList, classDef);
    }


    public String getResponseName() {
        return responseName;
    }
}
