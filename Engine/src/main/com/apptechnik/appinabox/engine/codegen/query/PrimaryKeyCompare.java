package com.apptechnik.appinabox.engine.codegen.query;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.exceptions.TypeNotFoundException;
import com.apptechnik.appinabox.engine.parser.ColumnInfo;
import com.apptechnik.appinabox.engine.parser.types.SQLType;
import com.apptechnik.appinabox.engine.codegen.util.CodegenUtil;
import com.apptechnik.appinabox.engine.util.CheckedConsumer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Generate the primary key comparison class, from the list of primary keys.
 * Before we read each subobject, we read the values from the result set which refer to the source columns
 * of the primary keys, compare them the previous values
 * Before we read each subobject, we read the values from the result set which refer to the source columns
 * of the primary keys, compare them the previous values (stored in an array), and set the parallel element
 * in a boolean if they've changed.
 */

public class PrimaryKeyCompare {
    private final String packageName;
    private final Collection<ColumnInfo> primaryKeys;

    public PrimaryKeyCompare(String packageName, Collection<ColumnInfo> primaryKeys) {
        this.packageName = packageName;
        this.primaryKeys = primaryKeys;
    }

    public ClassFile toClassFile(CodeGen codegen) throws Exception {
        return codegen.classFile(codegen.packageStmt(packageName),
                                 codegen.importStatementList(Constants.Templates.PRIMARY_KEY_IMPORTS),
                                 createClass(codegen, primaryKeys));
    }

    /**
     * Create the static inner class used by ReadListFromResultSet() to compare the primary keys to the previous row,
     * and create the sub-objects if the keys change.
     * @param codegen
     * @param primaryKeys
     * @return
     * @throws TypeNotFoundException
     */
    public static ClassDefinition createClass(CodeGen codegen, Collection<ColumnInfo> primaryKeys) throws TypeNotFoundException {
        List<MemberDeclaration> members = new ArrayList<MemberDeclaration>();

        // <java-type> <primary-key>;
        // <java-type> <primary-key>Previous;
        // boolean <primary-key>Changed = true;
        primaryKeys
                .forEach(CheckedConsumer.wrap(key -> { members.add(key.toFieldDeclaration(codegen));
                    members.add(key.toFieldDeclaration(codegen, Constants.Suffixes.PREVIOUS));
                    members.add(codegen.memberDecl(Constants.JavaKeywords.PUBLIC, false, false,
                                                         codegen.simpleType(Constants.JavaTypes.BOOLEAN),
                                                         codegen.var(key.getColumnName(true) + Constants.Suffixes.CHANGED),
                                                         codegen.booleanConst(true))); }));
        List<FunctionDefinition> methods = new ArrayList<FunctionDefinition>();
        methods.add(comparisonFunction(codegen, primaryKeys));
        primaryKeys
            .forEach(CheckedConsumer.wrap(key -> methods.add(CodegenUtil.accessorDefinition(codegen, codegen.simpleType(Constants.JavaTypes.BOOLEAN),
                                                                                            codegen.var(key.getColumnName(true) + Constants.Suffixes.CHANGED)))));
        ClassDeclaration classDecl = codegen.classDecl(true, false, false, Constants.JavaTypes.PRIMARY_KEY_SOURCE_COLUMNS,
        null, null, null, null);

        return codegen.classDef(classDecl,members, null, methods);
    }

    /**
     * for each primary-key:
     * <primary-key> = rs.get<sql-type>(<primary-key>);
     * <primary-key>Changed = !rs.wasNull() && </primary-key>(<primary-key> != <primary-key>Previous | !<primary-key>.equals(<primary-key>Previous) {
     * <primary-key>Previous = <primary-key>
     * @param primaryKeys
     * @throws IOException
     * @throws TypeNotFoundException
     */
    private static FunctionDefinition comparisonFunction(CodeGen codegen, Collection<ColumnInfo> primaryKeys)
    throws TypeNotFoundException {
        Variable resultSetVar = codegen.var(Constants.Variables.RS);
        ParameterDeclaration resultSetDecl =
            codegen.paramDecl(codegen.classType(Constants.JavaTypes.RESULT_SET), resultSetVar);
        FunctionDeclaration functionDeclaration =
            codegen.functionDecl(Constants.Functions.COMPARE_PRIMARY_KEYS,
                                 codegen.paramList(resultSetDecl),
                                 codegen.simpleType(Constants.JavaTypes.VOID),
                                 codegen.typeList(codegen.classType(Constants.JavaExceptions.SQL_EXCEPTION)));
        List<CodeStatement> statementList = new ArrayList<CodeStatement>();
        for (ColumnInfo primaryKey : primaryKeys) {
            String varName = primaryKey.getColumnName(true);
            Variable var = codegen.var(varName);
            Variable prev = codegen.var(varName + Constants.Suffixes.PREVIOUS);
            Variable changed = codegen.var(varName + Constants.Suffixes.CHANGED);
            NotExpression notRsWasNull = codegen.notExpr(codegen.methodCall(resultSetVar, Constants.Functions.WAS_NULL));
            CodeExpression comparison = SQLType.notComparison(codegen, primaryKey.getColumnSQLTypeString(), var, prev);
            CodeBinaryExpression condition = codegen.binaryExpr(Constants.JavaOperators.AND, notRsWasNull, comparison);
            MethodCall resultSetCall = SQLType.resultSetCallFromSqlType(codegen, Constants.Variables.RS,
                                                                        primaryKey.getColumnSQLTypeString(), varName);
            statementList.add(codegen.assignStmt(var, resultSetCall));
            statementList.add(codegen.assignStmt(changed, condition));
            statementList.add(codegen.assignStmt(prev, var));
        }
        return codegen.functionDef(functionDeclaration, codegen.stmtList(statementList));
    }
}
