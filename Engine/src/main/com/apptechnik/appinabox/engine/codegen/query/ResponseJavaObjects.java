package com.apptechnik.appinabox.engine.codegen.query;

import com.apptechnik.appinabox.engine.codegen.pojos.ClassDefinition;
import com.apptechnik.appinabox.engine.codegen.pojos.ClassFile;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeGen;
import com.apptechnik.appinabox.engine.codegen.mixin.CreateMixin;
import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;
import com.apptechnik.appinabox.engine.codegen.sql.QuerySQLObjects;
import com.apptechnik.appinabox.engine.parser.pojos.*;
import net.sf.jsqlparser.statement.select.Select;

import java.util.Stack;

public class ResponseJavaObjects implements Tree.ConsumerException<Stack<Tree<QueryFieldsNode>>> {
    private final QuerySQLObjects querySQLObjects;

    public ResponseJavaObjects(CreateObjects createObjects,
                               Select select,
                               ColumnsAndKeys columnsAndKeys,
                               ParameterMap parameterMap) {
        querySQLObjects = new QuerySQLObjects(createObjects, select, columnsAndKeys.getColumnInfoSet(),
                                              columnsAndKeys.getPrimaryKeyList(), parameterMap);
    }

    /**
     * Given a tress of QueryFieldsNode, the SELECT table map, the source select statement, and parameters,
     * generate the java file which will convert a ResultSet to a hierarchical POJO
     * @param createObjects parameters parsed from the input SQL file
     * @param jsonTree mapping of resultSet fields to hierarchical data.
     * @param select source SELECT statement.  We modify this to include the primary key of each table referenced
     *               in the select item list.
     * @param columnsAndKeys  ist of columns, keys, and tables used in the query.
     * @param parameterMap  JDBC Named Parameter to type map
     * @throws Exception
     */
    public static void createJavaObjects(CreateObjects createObjects,
                                         Tree<QueryFieldsNode> jsonTree,
                                         Select select,
                                         ColumnsAndKeys columnsAndKeys,
                                         ParameterMap parameterMap) throws Exception {
       ResponseJavaObjects responseJavaObjects =
            new ResponseJavaObjects(createObjects, select, columnsAndKeys, parameterMap);
        jsonTree.traverseTreeStack(responseJavaObjects);
    }

    public void accept(Stack<Tree<QueryFieldsNode>> stack) throws Exception {
        Tree<QueryFieldsNode> currentTreeNode = stack.peek();
        boolean isRoot = stack.size() == 1;

        // create the query objects, the SQL methods to read from the result set, and
        // the primary key comparison object.
        CreateObjects createObjects = querySQLObjects.getCreateObjects();
        CodeGen codegen = createObjects.getJavaCodeGen();
        String sqlPackageName = createObjects.getQuerySQLPackageName();
        String packageName = createObjects.getQueryPackageName();

        // don't need to generate objects for lists of primitives, but we may need to generate the top-level
        // SQL functions (selectList)
        if (!FieldReferenceNode.isSingleField(currentTreeNode)) {
            ResponseObjects responseObjects = new ResponseObjects(createObjects);
            ClassDefinition classDef = responseObjects.generateObject(codegen, currentTreeNode);
            ClassFile responseClassFile = responseObjects.toClassFile(codegen, currentTreeNode, classDef);

            // Create the Mixin class which transfers the complex members from the inherited class
            // and changes its constructor
            if (CreateMixin.hasComplexMembers(codegen, classDef)) {
                ClassFile mixinClassFile = CreateMixin.createAndRemapToMixinClass(codegen, responseClassFile);
                mixinClassFile.write(createObjects.getOutputDir());
            }
            responseClassFile.write(createObjects.getOutputDir());

        }
        querySQLObjects.create(codegen, stack.get(0).get(), packageName, sqlPackageName, currentTreeNode, isRoot).write(createObjects.getOutputDir());
    }
}
