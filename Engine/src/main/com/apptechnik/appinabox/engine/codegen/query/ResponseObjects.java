package com.apptechnik.appinabox.engine.codegen.query;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.codegen.util.CodegenUtil;
import com.apptechnik.appinabox.engine.exceptions.TypeNotFoundException;
import com.apptechnik.appinabox.engine.parser.pojos.*;
import com.apptechnik.appinabox.engine.util.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * with {@code Tree<SelectItemNode>} (parsed from the json: comment in the source SQL file,
 * the source SELECT statement, and the Map of Tables from CREATE TABLES,
 * Generate the converter from the SQL ResultSet to a hierarchical POJO.
 */
public class ResponseObjects {
    private final CreateObjects createObjects;

    public ResponseObjects(CreateObjects createObjects) {
        this.createObjects = createObjects;
    }

    /**
     * Add the package statement and imports to the class definition to create a class file.
     * Create the Mixin template from the POJO
     * @param classDefinition generated class definition.
     * @throws Exception
     */
    public ClassFile toClassFile(CodeGen codegen, Tree<QueryFieldsNode> fieldRefTree, ClassDefinition classDefinition) throws Exception {
        return toClassFile(codegen, fieldRefTree, classDefinition, Constants.Templates.OBJECT_IMPORTS);
    }

    public ClassFile toKotlinClassFile(CodeGen codegen, Tree<QueryFieldsNode> fieldRefTree, ClassDefinition classDefinition) throws Exception {
        return toClassFile(codegen, fieldRefTree, classDefinition, Constants.Templates.KOTLIN_OBJECT_IMPORTS);
    }

    /**
     * Add the package statement and imports to the class definition to create a class file.
     * Create the Mixin template from the POJO
     * @param classDefinition generated class definition.
     * @param importsTemplate imports template (java and kotlin endpoints have diffrent imports)
     * @throws Exception
     */
    public ClassFile toClassFile(CodeGen codegen,
                                 Tree<QueryFieldsNode> fieldRefTree,
                                 ClassDefinition classDefinition,
                                 String importsTemplate) throws Exception {
        String packageName = createObjects.getPackageName() + "." + Constants.Packages.QUERY;
        PackageStatement packageStatement = codegen.packageStmt(packageName);
        StatementList importList = codegen.importStatementList(importsTemplate);
        importList.addStatements(FieldReferenceNode.importsFromChildren(codegen, packageName, fieldRefTree));
        return codegen.classFile(packageStatement, importList, classDefinition);
    }

    /**
     * Create the class definition or this object.
     * Generate field declarations, accessors, and setters from the node's columns and children
     * @param codegen
     * @return
     * @throws TypeNotFoundException
     */
    public ClassDefinition generateObject(CodeGen codegen, Tree<QueryFieldsNode> fieldRefTree)
    throws TypeNotFoundException {
        QueryFieldsNode node = fieldRefTree.get();
        ClassDeclaration classDecl =
            codegen.classDecl(node.getVarName(), codegen.classAnnot(Constants.Annotations.GENERATED));
        List<MemberDeclaration> members = node.fieldDeclarationsFromColumns(codegen, Constants.JavaKeywords.PRIVATE);
        members.addAll(QueryFieldsNode.childFieldDeclarationList(codegen, fieldRefTree, Constants.JavaKeywords.PRIVATE));
        List<ConstructorDefinition> constructors = Collections.singletonList(constructor(codegen, fieldRefTree));
        List<FunctionDefinition> methods = CodegenUtil.accessorsAndSetters(codegen, members);
        return codegen.classDef(classDecl, members, constructors, methods);
    }

    /**
     * Create the class definition or this object.
     * Generate field declarations from the node's columns and children
     * @param codegen
     * @param fieldRefTree node and its children.
     * @return
     * @throws TypeNotFoundException
     */
    public ClassDefinition generateObjectNoMethods(CodeGen codegen, Tree<QueryFieldsNode> fieldRefTree)
            throws TypeNotFoundException {
        String varName = fieldRefTree.get().getVarName();
        ClassDeclaration classDecl =
                codegen.classDecl(false, false, false, varName, null, null, new TypeList(),
                                  new ArrayList<Annotation>(List.of(codegen.classAnnot(Constants.Annotations.GENERATED))));
        List<MemberDeclaration> members = QueryFieldsNode.fieldDeclarationList(codegen, fieldRefTree, Constants.JavaKeywords.PUBLIC);
        List<ConstructorDefinition> constructors = List.of(codegen.constructorDef(varName, members));
        List<FunctionDefinition> methods = new ArrayList<FunctionDefinition>();
        return codegen.classDef(classDecl, members, constructors, methods);
    }
    /**
     * public class <object-name> {
     *     public <object-name() {
     *        [for each child]
     *        <child-type> <child-name> = new ArrayList<child-type>());
     * }
     * Allocate the lists for each child
     * @param tree current JSON node and children
     * @return constructor call.
     */
    private static ConstructorDefinition constructor(CodeGen codegen, Tree<QueryFieldsNode> tree) {
        ConstructorDeclaration declaration = codegen.constructorDecl(tree.get().getVarName());
        List<CodeStatement> childListAllocations =
            tree.getChildren().stream()
                    .map(CheckedFunction.wrap(child -> QueryFieldsNode.childListAllocation(codegen, tree, child)))
                    .collect(Collectors.toList());
        return codegen.constructorDef(declaration, codegen.stmtList(childListAllocations));
    }

    /**
     * Exception for the multiple SELECT case, where the top level structure is ALWAYS a collection
     * of lists of the SELECT calls.
     * @param tree top level of the JSON mapping tree.
     * @param accessModifier public/private/protected
     * @return list of child field declarations.
     * @throws TypeNotFoundException type was not found
     */
    public static List<MemberDeclaration> rootChildFieldDeclarations(CodeGen codegen,
                                                                     Tree<QueryFieldsNode> tree,
                                                                     String accessModifier) {
        return tree.getChildren().stream()
                .map(CheckedFunction.wrap(child -> QueryFieldsNode.childListFieldDeclaration(codegen, child, accessModifier)))
                .collect(Collectors.toList());
    }

    /**
     * Generate the imports from children which are not primitive types (single leaf nodes)
     * @param packageName
     * @param fieldRefTree
     * @return
     */
    public static StatementList importsFromChildren(CodeGen codegen,
                                                    String packageName,
                                                    Tree<? extends FieldReferenceNode> fieldRefTree) {
        List<CodeStatement> importList = fieldRefTree.getChildren().stream()
            .filter(child -> !FieldReferenceNode.isSingleField(child))
            .map(child -> codegen.importStmt(packageName + "." + child.get().getVarName()))
            .collect(Collectors.toList());
        return codegen.stmtList(importList);
    }
}
