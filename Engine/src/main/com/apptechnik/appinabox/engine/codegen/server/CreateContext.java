package com.apptechnik.appinabox.engine.codegen.server;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.server.CreateWebXML;
import com.apptechnik.appinabox.engine.util.ExtraProperties;
import com.apptechnik.appinabox.engine.util.FileUtil;
import com.apptechnik.appinabox.engine.util.Substitute;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/** transform properties and
 * url=jdbc:postgresql://localhost
 * database=dvdrental
 * user=postgres
 * password=FiatX1/9
 */

public class CreateContext {

    public static void main(String[] args) throws Exception {
        if (args.length < 2) {
            System.out.println("usage: CreateContext properties context.xml");
            System.exit(-1);
        }
        InputStream isResource = CreateWebXML.class.getClassLoader().getResourceAsStream(Constants.Templates.CONTEXT_DATASOURCE);
        if (isResource == null) {
            throw new FileNotFoundException("failed to open resource " + Constants.Templates.CONTEXT_DATASOURCE);
        }
        File propertiesFile = FileUtil.checkFile("properties file", args[0]);
        ExtraProperties properties = new ExtraProperties(propertiesFile);
        File contextFile = new File(args[1]);
        Substitute.transformStreamDefaults(isResource, contextFile, properties);
    }
}
