package com.apptechnik.appinabox.engine.codegen.server;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;
import com.apptechnik.appinabox.engine.util.CheckedConsumer;
import com.apptechnik.appinabox.engine.util.FileUtil;
import com.apptechnik.appinabox.engine.codegen.util.SQLFile;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CreateLog4j2 {
	private CreateLog4j2() {
    }
 
    /**
     * from the directory of Servlet Files, generate the header:
     * {@code
	 * <?xml version="1.0" encoding="UTF-8"?>
	 * <Configuration status="DEBUG">
	 * <Appenders>
	 * 		<Console name="Console" target="SYSTEM_OUT">
	 * 			<PatternLayout pattern="%d{HH:mm:ss.SSS} [%t] %-5level %logger{36} - %msg%n"/>
	 * 		</Console>
	 * </Appenders>
	 * <Loggers>
	 * 	<Logger name="com.mycompany.myapp.InsertServlet" level="debug">
	 * 			<AppenderRef ref="Console"/>
	 * 	</Logger>
	 * 	<Root level="debug">
	 * 		<AppenderRef ref="Console"/>
	 * 	</Root>
	 * </Loggers>
	 * </Configuration>
     *}
     */

	public static void main(String[] args) 
	throws Exception {
		boolean debug = true;
		if (args.length < 3) {
			System.out.println("usage: CreateLog4j2 output-file sql-directory properties");
			System.exit(-1);
		}
		// access the package directory from the parent directory and transformed package name, and read the
		// names of the class files from it.
		File outputFile = FileUtil.checkCreateFile("Log4j2.xml", args[0]);
		File sqlDirectory = FileUtil.checkDirectory("SQL Files Directory", args[1]);
		File propertiesFile = FileUtil.checkFile("properties file ", args[2]);

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		InputStream isTemplate = CreateLog4j2.class.getClassLoader().getResourceAsStream(Constants.Templates.LOG4J2);
		Document templateDoc = builder.parse(isTemplate);
		// iterate over each class file, strip the extension, prepend the last qualifier of the package path
		List<ServletDefinition> servletDefinitionList = new ArrayList<>();
		CreateObjects.walkSQLDir(sqlDirectory, file -> servletDefinitionList.add(new ServletDefinition(new SQLFile(file))));
		PopulateLog4j2.createLog4j2(outputFile, servletDefinitionList, templateDoc, propertiesFile, debug);
	}
}
