package com.apptechnik.appinabox.engine.codegen.server;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;
import com.apptechnik.appinabox.engine.codegen.util.SQLFile;
import com.apptechnik.appinabox.engine.exceptions.DirectiveNotFoundException;
import com.apptechnik.appinabox.engine.exceptions.NameNotFoundException;
import com.apptechnik.appinabox.engine.exceptions.ParameterFormatException;
import com.apptechnik.appinabox.engine.exceptions.PropertiesException;
import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.util.*;
import net.sf.jsqlparser.JSQLParserException;
import org.w3c.dom.Element;
import org.w3c.dom.Document;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.ArrayList;
import java.util.Properties;
import java.util.function.Consumer;

public class CreateWebXML {

    private CreateWebXML() {
    }
 
    /**
  	 * given the directory of servlets, generate the WebXML file. example:
     * {@code
	 * <web-app xmlns="http://java.sun.com/xml/ns/j2ee"
     * xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    	     xsi:schemaLocation="http://java.sun.com/xml/ns/j2ee http://java.sun.com/xml/ns/j2ee/web-app_2_4.xsd"
     * version="2.4">
	 * <resource-ref>
	 * <description>postgreSQL Datasource</description>
	 * 		<res-ref-name>jdbc/postgres</res-ref-name>
	 * 		<res-type>javax.sql.DataSource</res-type>
	 * 		<res-auth>Container</res-auth>
	 * </resource-ref>
     * Add the servlet entries for:
     * <servlet>
     *    <servlet-name>ServletName</servlet-name>
     *    <servlet-class>packageName.ServletClass</servlet-class>
	 *	  <init-param>
 	 *		<param-name>ParamName</param-name>
 	 *		<param-value>ParamValue</param-value>
 	 *	  </init-param>
	 *		...
     * </servlet>
     * <servlet-mapping>
     *    <servlet-name>ServletName</servlet-name>
     *    <url-pattern>urlPattern</servlet-name>
     * </servlet-mapping>
     *}
     */

	public static void main(String[] args) 
	throws Exception {
		boolean debug = true;

		if (args.length < 3) {
			System.out.println("usage: CreateWebXML output-file sql-directory properties");
			System.exit(-1);
		}
		File webXMLFile = FileUtil.checkCreateFile("Output web.xml file", args[0]);
		File sqlDirectory = FileUtil.checkDirectory("SQL files directory", args[1]);
		File propertiesFile = FileUtil.checkFile("properties file", args[2]);
		InputStream isResource = CreateWebXML.class.getClassLoader().getResourceAsStream(Constants.Templates.WEB_XML);
		if (isResource == null) {
			throw new FileNotFoundException("failed to open resource " + Constants.Templates.WEB_XML);
		}
		Properties properties = new Properties();
		properties.load(new FileInputStream(propertiesFile));

		// HACK: this is a terrible, and I'll admit it.  We want to use the same naming in the properties file
		// that we do in web.xml, but in context.xml it's name="jdbc/${database}" and in properties it's
		// just ${database}, so we have to prefix it with 'jdbc/' here, so it matches in the servlet.
		// now, I could have made the property name jdbc and stripped it at the other places, but there's
		// too much code dependent on that convention, so I'll change it later.
		String databaseName = properties.getProperty(Constants.Properties.DATABASE);
		if (databaseName != null) {
			String jdbcDatabaseName = Constants.PropertyHacks.JDBC_PREFIX + databaseName;
			properties.setProperty(Constants.Properties.DATABASE, jdbcDatabaseName);
		} else {
			throw new PropertiesException(propertiesFile, Constants.Properties.DATABASE);
		}
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document webXMLDoc = builder.parse(isResource);

		// create the web-app root node and set the schema attributes
		Element webAppElement = webXMLDoc.getDocumentElement();
		addParamsToContextParams(propertiesFile, properties, Constants.WebXML.INIT_PARAM_VALUES, webXMLDoc, webAppElement);
		addDatasource(webXMLDoc, webAppElement, databaseName);
		addSecurityConstraint(webXMLDoc, webAppElement);

		/**
		 * Recurse through the SQL files, pull the name, package, and URL directives, and add them
		 * as the servlet-name and servlet-mapping entries in the web.xml file.
		 */
		CreateObjects.walkSQLDir(sqlDirectory, file -> insertSQLFileToWebXML(file, webXMLDoc, webAppElement));
		XMLUtils.save(webXMLFile, webXMLDoc);
	}

	/**
	 *
	 * <init-param>
	 * <param-name>db-database</param-name>
	 * <param-value>store</param-value>
	 * </init-param> ...
	 * @param propertiesFile source properties file (used for error messaging).
	 * @param properties properties read from propertiesFile.
	 * @param propertyNames properties filter.
	 * @param webXMLDoc
	 */
	private static void addParamsToContextParams(File propertiesFile,
												 Properties properties,
												 String[] propertyNames,
												 Document webXMLDoc,
												 Element webAppElement) throws IOException, PropertiesException {
		for (String propertyName : propertyNames) {
			String value = properties.getProperty(propertyName);
			if (value == null) {
				throw new PropertiesException(propertiesFile, propertyName);
			}
			Element contextElement = webXMLDoc.createElement(Constants.WebXML.CONTEXT_PARAM);
			webAppElement.appendChild(contextElement);
			XMLUtils.addSingleTextElement(webXMLDoc, contextElement, Constants.WebXML.PARAM_NAME, propertyName);
			XMLUtils.addSingleTextElement(webXMLDoc, contextElement, Constants.WebXML.PARAM_VALUE, value);
		}
	}

	/**
	 * Create the entries:
	 * <servlet>
	 * 	<servlet-name>UpdateServlet</servlet-name>
	 * 	<servlet-class>com.mycompany.myapp.UpdateServlet</servlet-class>
	 * </servlet>
	 * <servlet-mapping>
	 *  <servlet-name>servlet2</servlet-name>
	 *  <url-pattern>servlet2-query</url-pattern>
	 * </servlet-mapping>
	 * @param file contains url, packageName, and name directives.
	 * @param webXMLDoc destination document
	 * @param webAppElement <web-app> element
	 */

	private static void insertSQLFileToWebXML(File file, Document webXMLDoc, Element webAppElement)
	throws IOException, DirectiveNotFoundException, JSQLParserException, NameNotFoundException, ParameterFormatException {
		SQLFile sqlFile = new SQLFile(file, false);
		ServletDefinition servletDefinition = new ServletDefinition(sqlFile);
		String url = sqlFile.getTag(Constants.Directives.URL);

		// Servlets are generated in the servlet subdirectory (see CreateObjects.getServletPackageName())
		String name = sqlFile.getName() + "." + Constants.Packages.SERVLET;
		String servletClass = servletDefinition.getClassName();
		String urlPath = UrlUtil.getPath(url);
		Element nameElement = addServletName(webXMLDoc, webAppElement, name, servletClass);
		Element mappingElement = addServletMapping(webXMLDoc, webAppElement, name, urlPath);
	}

	private static Element addServletName(Document webXMLDoc, Element webAppElement, String name, String servletPackage) {
		Element servletElement = XMLUtils.addNode(webXMLDoc, webAppElement, Constants.WebXML.SERVLET);
		XMLUtils.addSingleTextElement(webXMLDoc, servletElement, Constants.WebXML.SERVLET_NAME, name);
		XMLUtils.addSingleTextElement(webXMLDoc, servletElement, Constants.WebXML.SERVLET_CLASS, servletPackage);
		return servletElement;
	}

	/*
	 	<servlet-mapping>
    		<servlet-name>servlet2</servlet-name>
    		<url-pattern>servlet2-query</url-pattern>
  		</servlet-mapping>
	 */
	private static Element addServletMapping(Document webXMLDoc, Element webAppElement, String name, String urlPath) {
		Element servletMappingElement = XMLUtils.addNode(webXMLDoc, webAppElement, Constants.WebXML.SERVLET_MAPPING);
		XMLUtils.addSingleTextElement(webXMLDoc, servletMappingElement, Constants.WebXML.SERVLET_NAME, name);
		XMLUtils.addSingleTextElement(webXMLDoc, servletMappingElement, Constants.WebXML.URL_PATTERN, urlPath);
		return servletMappingElement;
	}

	/**
	 *
	 * <resource-ref>
	 *  <res-ref-name>jdbc/${database}</res-ref-name>
	 *  <res-type>javax.sql.DataSource</res-type>
	 *  <res-auth>Container</res-auth>
	 * </resource-ref>
	 * @param webXMLDoc
	 * @param parentElement
	 * @param databaseName
	 * @return
	 */
	private static Element addDatasource(Document webXMLDoc, Element parentElement, String databaseName) {
		Element resourceNode = XMLUtils.addNode(webXMLDoc, parentElement, Constants.WebXML.RESOURCE_REF);
		XMLUtils.addSingleTextElement(webXMLDoc, resourceNode, Constants.WebXML.RES_REF_NAME, Constants.WebXML.JDBC + "/" + databaseName);
		XMLUtils.addSingleTextElement(webXMLDoc, resourceNode, Constants.WebXML.RES_TYPE, Constants.WebXML.JAVAX_SQL_DATASOURCE);
		XMLUtils.addSingleTextElement(webXMLDoc, resourceNode, Constants.WebXML.RES_AUTH, Constants.WebXML.CONTAINER);
		return resourceNode;
	}

	/**
	 * Generate the security constraint for the web.xml file, for example:
	 * {@code
	 *     <security-constraint>
	 *         <web-resource-collection>
	 *             <web-resource-name>Secure resources</web-resource-name>
	 *             <url-pattern>/*</url-pattern>
	 *         </web-resource-collection>
	 *         <user-data-constraint>
	 *             <transport-guarantee>CONFIDENTIAL</transport-guarantee>
	 *         </user-data-constraint>
	 *     </security-constraint>
	 * }
	 * @param webXMLDoc
	 * @param webAppElement
	 */
	public static void addSecurityConstraint(Document webXMLDoc,
											 Element webAppElement) {
		Element securityConstraintElement = XMLUtils.addNode(webXMLDoc, webAppElement, Constants.WebXML.SECURITY_CONSTRAINT);
		Element webResourceCollectionElement = XMLUtils.addNode(webXMLDoc, securityConstraintElement, Constants.WebXML.WEB_RESOURCE_COLLECTION);
		XMLUtils.addSingleTextElement(webXMLDoc, webResourceCollectionElement,
							 		  Constants.WebXML.WEB_RESOURCE_NAME,Constants.WebXML.SECURE_RESOURCES);
		XMLUtils.addSingleTextElement(webXMLDoc, webResourceCollectionElement,
							 		  Constants.WebXML.URL_PATTERN,Constants.WebXML.WILDCARD_PATTERN);
		Element userDataConstraint = XMLUtils.addNode(webXMLDoc, securityConstraintElement, Constants.WebXML.USER_DATA_CONSTRAINT);
		XMLUtils.addSingleTextElement(webXMLDoc, userDataConstraint,
							 		  Constants.WebXML.TRANSPORT_GUARANTEE,Constants.WebXML.CONFIDENTIAL);
	}
}
