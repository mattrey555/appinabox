package com.apptechnik.appinabox.engine.codegen.server;

import com.apptechnik.appinabox.engine.util.ExtraProperties;
import com.apptechnik.appinabox.engine.util.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathExpressionException;
import java.io.*;
import java.util.List;
import java.util.Properties;


public class PopulateLog4j2 {
    private static final String LOGGERS = "Loggers";
    private static final String LOGGER = "Logger";
    private static final String NAME = "name";
	private static final String TARGET = "target";
	private static final String PATTERN_LAYOUT = "PatternLayout";
    private static final String LEVEL = "level";
    private static final String PATTERN = "pattern";
    private static final String APPENDERS = "Appenders";
    private static final String APPENDER_REF = "AppenderRef";
    private static final String REF = "ref";
    private static final String ROOT = "root";
	private static final String DEBUG = "debug";
	private static final String CONSOLE = "Console";


    private PopulateLog4j2() {
    }

 	/**
  	 * @param outputFile WEB-INF/web.xml
     * @param servletDefinitionList list of servlet definitions.
	 * @param templateDoc log4j2.xml file to merge with output.
	 * @param propertiesFile properties file
	 * @param debug debug flag
     */

 	public static void createLog4j2(File outputFile,
									List<ServletDefinition> servletDefinitionList,
									Document templateDoc,
									File propertiesFile,
									boolean debug)  
		throws IOException, XPathExpressionException, TransformerException {

		// properties file which controls which sections to output.
		ExtraProperties properties = new ExtraProperties(propertiesFile);

		// create the document.
		// create the context params, servlet names, and servlet mappings.
		createLoggers(templateDoc, servletDefinitionList, properties, debug);
		XMLUtils.save(outputFile, templateDoc);
	}
	
	/**
	 * From the contextParams properties map and the optionanl mergeDoc,
	 * create the context-param entries
	 * {@code
	 * 	<Logger name="com.mycompany.myapp.InsertServlet" level="debug">
	 * 			<AppenderRef ref="Console"/>
	 * 	</Logger>
	 * 	}
	 * @param document destination document
	 */

	public static void createLoggers(Document document,
									 List<ServletDefinition> servletDefinitionList,
									 Properties properties,
								     boolean debug) throws XPathExpressionException {

		NodeList loggersNodeList = document.getElementsByTagName(LOGGERS);
		Node loggersNode = loggersNodeList.item(0);
		for (ServletDefinition servletDefinition : servletDefinitionList) {
			addLogger(document, loggersNode, servletDefinition);
		}
	}

	public static void addLogger(Document document,
								 Node loggersNode,
								 ServletDefinition servletDefinition) {

		Element loggerElement = document.createElement(LOGGER);
		loggerElement.setAttribute(NAME, servletDefinition.getClassName());
		loggerElement.setAttribute(LEVEL, DEBUG);
		Element appenderRefElement = document.createElement(APPENDER_REF);
		appenderRefElement.setAttribute(REF, CONSOLE);
		loggerElement.appendChild(appenderRefElement);
		loggersNode.appendChild(loggerElement);
		document.importNode(loggerElement, true);
	}
}
