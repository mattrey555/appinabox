package com.apptechnik.appinabox.engine.codegen.server;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import com.apptechnik.appinabox.engine.util.ExtraProperties;
import com.apptechnik.appinabox.engine.util.FileUtil;
import com.apptechnik.appinabox.engine.util.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import javax.xml.xpath.XPathExpressionException;
import java.io.*;
import java.util.List;


public class PopulateWebXML {

	// NOTE: move these to Constants.
    private static final String WEB_APP = "web-app";
    private static final String USE_HTTPS = "use-https";
    private static final String SERVLET = "servlet";
    private static final String SERVLET_NAME = "servlet-name";
    private static final String SERVLET_CLASS = "servlet-class";
    private static final String CONTEXT_PARAM = "context-param";
    private static final String INIT_PARAM = "init-param";
    private static final String PARAM_NAME = "param-name";
    private static final String PARAM_VALUE = "param-value";
    private static final String URL_PATTERN = "url-pattern";
    private static final String SERVLET_MAPPING = "servlet-mapping";
    private static final String SECURITY_CONSTRAINT = "security-constraint";
    private static final String WEB_RESOURCE_COLLECTION = "web-resource-collection";
    private static final String WEB_RESOURCE_NAME = "web-resource-name";
    private static final String USER_DATA_CONSTRAINT = "user-data-constraint";
    private static final String TRANSPORT_GUARANTEE = "transport-guarantee";
    private static final String CONFIDENTIAL = "CONFIDENTIAL";
    private static final String SECURE_RESOURCES = "Secure Resources";
	private static final String WILDCARD_PATTERN = "/*";

    private PopulateWebXML() {
    }

    /**
     * Add the servlet entries for the servlets, for example:
     * {@code
     * <servlet>
     *    <servlet-name>ServletName</servlet-name>
     *    <servlet-class>packageName.ServletClass</servlet-class>
	  *	  <context-param>
 	  *		<param-name>ParamName</param-name>
 	  *		<param-value>ParamValue</param-value>
 	  *	  </context-param>
	  *	  <init-param>
 	  *		<param-name>ParamName</param-name>
 	  *		<param-value>ParamValue</param-value>
 	  *	  </init-param>
	  *		...
     * </servlet>
     * <servlet-mapping>
     *    <servlet-name>ServletName</servlet-name>
     *    <url-pattern>urlPattern</servlet-name>
     * </servlet-mapping>
 	  * }
	  * @param schemaAttributeFile properties file with schema attributes, xmlns, xsi.
     * @param controlPropertyFile properties (use-https: true|false) used for security-constraints
	  *                            in the web.xml file. Likely to be extended for othe properties.
	  * @param outputFile WEB-INF/web.xml
     * @param packageName packageName for the servlets com.corp.app
     * @param contextParamsFile context-params parameters for connecting to postgres, such as the
	  *                          database url and password.
     * @param servletDefinitionList list of servlet definitions.
	  * @param mergeDoc external web.xml file to merge with output.
	  * @param debug output debug info to console.
     */
  
    public static void createWebXML(File schemaAttributeFile,
									File controlPropertyFile,
									File outputFile,
									String packageName, 
									File contextParamsFile,
									List<ServletDefinition> servletDefinitionList,
									Document mergeDoc,
									boolean debug)  
		throws ParserConfigurationException, TransformerException, IOException, XPathExpressionException {

		// properties file which controls which sections to output.
		ExtraProperties controlProperties = new ExtraProperties(controlPropertyFile);

		// create the document.
		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
		Document document = documentBuilder.newDocument();

		// create the web-app root node and set the schema attributes
		Element webAppElement = document.createElement(WEB_APP);
		document.appendChild(webAppElement);
		createSchemaAttributes(webAppElement, schemaAttributeFile);

		// create the context params, servlet names, and servlet mappings.
		ExtraProperties contextParams = new ExtraProperties(contextParamsFile);
		createContextParams(document, webAppElement, contextParams, mergeDoc, debug);
		createServletNames(document, webAppElement, packageName, servletDefinitionList, mergeDoc, debug);
		createServletMappings(document, webAppElement, packageName, servletDefinitionList, mergeDoc, debug);

		String useHTTPS = controlProperties.getProperty(USE_HTTPS);
		if (Boolean.getBoolean(useHTTPS)) {
	    	createSecurityConstraint(document, webAppElement);
		}
		XMLUtils.save(outputFile, document);
	}
	
	/**
	 * From the contextParams properties map and the optionanl mergeDoc,
	 * create the context-param entries
	 * {@code
	 *   <context-param>
     *		<param-name>client-id</param-name>
     *		<param-value>client-id-test</param-value>
  	 *	</context-param>
	 *}
	 * @param document destination document
	 * @param webAppElement &lt;web-app&gt; element
	 * @param contextParams properties tag-value map of context params
	 * @param mergeDoc web.xml file to merge.
	 */

	public static void createContextParams(Document document,
										   Element webAppElement,
										   ExtraProperties contextParams,
										   Document mergeDoc,
										   boolean debug) throws XPathExpressionException {
		if (mergeDoc != null) {
			XMLUtils.importNodeList(document, webAppElement, mergeDoc, CONTEXT_PARAM, debug);
		}

		for (String name : contextParams.stringPropertyNames()) {
			String value = contextParams.getProperty(name);
			addContextParam(document, webAppElement, name, value);
		}
	}

	/**
	 * Create the list of entries
	 * {@code
	 * <servlet>
     * 	<servlet-name>UpdateServlet</servlet-name>
     * 	<servlet-class>com.mycompany.myapp.UpdateServlet</servlet-class>
     * </servlet>
	 * }
	 * @param document destination document
	 * @param webAppElement &lt;web-app&gt; element
	 * @param packageName ex. com.mycompany.myapp java package name.
	 * @param servletDefinitionList list of servlet definitions.
	 * @param mergeDoc parsed web.xml file to merge.
	 */
    public static void createServletNames(Document document,
										  Element webAppElement,
										  String packageName, 
										  List<ServletDefinition> servletDefinitionList,
										  Document mergeDoc,
										  boolean debug) throws XPathExpressionException {

		if (mergeDoc != null) {
			XMLUtils.importNodeList(document, webAppElement, mergeDoc, SERVLET, debug);
		}

		// first create the servlet-name/servlet-class list
		for (ServletDefinition servletDefinition : servletDefinitionList) {
			Element servletElement = document.createElement(SERVLET);
			webAppElement.appendChild(servletElement);
			createServletName(document, servletElement, packageName, servletDefinition);
			createServletInitParams(document, servletElement, packageName, servletDefinition);
		}
	}

	/**
	 * Create the entry:
	 * {@code
	 * <servlet>
     * 	<servlet-name>UpdateServlet</servlet-name>
     * 	<servlet-class>com.mycompany.myapp.UpdateServlet</servlet-class>
     * </servlet>
	 * }
	 * @param document destination document
	 * @param servletElement &lt;web-app&gt; element
	 * @param packageName ex. com.mycompany.myapp java package name.
	 * @param servletDefinition servlet definition.
	 */
	public static void createServletName(Document document,
										 Element servletElement,
										 String packageName, 
										 ServletDefinition servletDefinition) {
		Element servletNameElement = document.createElement(SERVLET_NAME);
		servletElement.appendChild(servletNameElement);
		servletNameElement.appendChild(document.createTextNode(servletDefinition.getName()));

		Element servletClassElement = document.createElement(SERVLET_CLASS);
		servletElement.appendChild(servletClassElement);
		servletClassElement.appendChild(document.createTextNode(servletDefinition.getClassName()));
	}


	/**
	 * Create the list of entries
	 * {@code
	 *  <servlet>
     *		<servlet-name>UpdateServlet</servlet-name>
     *		<servlet-class>com.mycompany.myapp.UpdateServlet</servlet-class>
  	 * </servlet>
	 * }
	 * @param document destination document
	 * @param webAppElement &lt;web-app&gt; element
	 * @param packageName ex. com.mycompany.myapp java package name.
	 * @param servletDefinitionList list of servlet definitions.
	 * @param mergeDoc parsed web.xml file to merge.
	 */
    public static void createServletMappings(Document document,
										     Element webAppElement,
										     String packageName, 
										     List<ServletDefinition> servletDefinitionList,
											 Document mergeDoc,
											 boolean debug) throws XPathExpressionException {

		if (mergeDoc != null) {
			XMLUtils.importNodeList(document, webAppElement, mergeDoc, SERVLET_MAPPING, debug);
		}
		// first create the servlet-name/servlet-class list
		for (ServletDefinition servletDefinition : servletDefinitionList) {
			createServletMapping(document, webAppElement, packageName, servletDefinition);
		}
	}

	public static void createServletMapping(Document document,
										    Element webAppElement,
										    String packageName, 
										    ServletDefinition servletDefinition) {
		Element servletMappingElement = document.createElement(SERVLET_MAPPING);
		webAppElement.appendChild(servletMappingElement);

		Element servletNameElement = document.createElement(SERVLET_NAME);
		servletMappingElement.appendChild(servletNameElement);
		servletNameElement.appendChild(document.createTextNode(servletDefinition.getName()));

		Element servletUrlElement = document.createElement(URL_PATTERN);
		servletMappingElement.appendChild(servletUrlElement);
		servletUrlElement.appendChild(document.createTextNode(servletDefinition.getUrlPattern()));
	}

	public static void createServletInitParams(Document document,
											   Element servletElement,
											   String packageName, 
											   ServletDefinition servletDefinition) {
		for (String name : servletDefinition.getParamKeys()) {
			String value = servletDefinition.getParam(name);
			addInitParam(document, servletElement, name, value);
		}
	}
	/**
	 * Generate the init-param section:
	 * {@code
	 *	  <init-param>
 	 *		<param-name>ParamName</param-name>
 	 *		<param-value>ParamValue</param-value>
 	 *	  </init-param>
	 * }
	 */

	public static void addInitParam(Document document, 
								    Element servletElement,
									String name, 
									String value) {
		addParam(document, servletElement, INIT_PARAM, name, value);
	}

	public static void addContextParam(Document document, 
								       Element servletElement,
									   String name, 
									   String value) {
		addParam(document, servletElement, CONTEXT_PARAM, name, value);
	}

	public static void addParam(Document document, 
								Element parentElement,
								String paramTag,
								String name, 
								String value) {
		Element initParamElement = document.createElement(paramTag);
		parentElement.appendChild(initParamElement);

		Element paramNameElement = document.createElement(PARAM_NAME);
		initParamElement.appendChild(paramNameElement);
		paramNameElement.appendChild(document.createTextNode(name));

		Element paramValueElement = document.createElement(PARAM_VALUE);
		initParamElement.appendChild(paramValueElement);
		paramValueElement.appendChild(document.createTextNode(value));
	}

	/**
	 * Generate the security-constraint:
	 * {@code
	 *     <security-constraint>
	 *         <web-resource-collection>
	 *             <web-resource-name>Secure resources</web-resource-name>
	 *             <url-pattern>/*</url-pattern>
	 *         </web-resource-collection>
	 *         <user-data-constraint>
	 *             <transport-guarantee>CONFIDENTIAL</transport-guarantee>
	 *         </user-data-constraint>
	 *     </security-constraint>
	 * }
	 * @param document web.xml document
	 * @param webAppElement &lt;web-app&gt; element
	 */
	public static void createSecurityConstraint(Document document,
												Element webAppElement) {
		Element securityConstraintElement = document.createElement(SECURITY_CONSTRAINT);
		webAppElement.appendChild(securityConstraintElement);
		Element webResourceCollectionElement = document.createElement(WEB_RESOURCE_COLLECTION);

		securityConstraintElement.appendChild(webResourceCollectionElement);

		Element webResourceNameElement = document.createElement(WEB_RESOURCE_NAME);
		webResourceCollectionElement.appendChild(webResourceNameElement);
		webResourceNameElement.appendChild(document.createTextNode(SECURE_RESOURCES));

		Element urlPatternElement = document.createElement(URL_PATTERN);
		webResourceNameElement.appendChild(urlPatternElement); 
		urlPatternElement.appendChild(document.createTextNode(WILDCARD_PATTERN));

		Element userDataConstraintElement = document.createElement(USER_DATA_CONSTRAINT);
		securityConstraintElement.appendChild(userDataConstraintElement);
		
		Element transportGuaranteeElement = document.createElement(TRANSPORT_GUARANTEE);
		userDataConstraintElement.appendChild(transportGuaranteeElement); 
		transportGuaranteeElement.appendChild(document.createTextNode(CONFIDENTIAL));
	} 

	/**
	 * The schema attributes file is a properties file, and we write it into the schema section
	 * in the XML file
	 * @param webAppElement &lt;web-app&gt; element
	 * @param schemaAttributesFile properties file of the form prop:value
	 */
	public static void createSchemaAttributes(Element webAppElement, File schemaAttributesFile) 
	throws IOException {

		// can't use properties because of the funny syntax (with colons) of XML header attributes.
		List<String> schemaAttributes = FileUtil.readStringList(schemaAttributesFile);
		for (String attribute : schemaAttributes) {
			String[] attributePair = attribute.split("=");
			webAppElement.setAttribute(attributePair[0], attributePair[1]);
		}
	}
}
