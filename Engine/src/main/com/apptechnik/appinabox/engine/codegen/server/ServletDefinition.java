package com.apptechnik.appinabox.engine.codegen.server;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.exceptions.DirectiveNotFoundException;
import com.apptechnik.appinabox.engine.exceptions.NameNotFoundException;
import com.apptechnik.appinabox.engine.codegen.util.SQLFile;
import com.apptechnik.appinabox.engine.util.UrlUtil;
import net.sf.jsqlparser.JSQLParserException;

import java.io.IOException;
import java.util.*;

public class ServletDefinition {
    private String servletName;
    private String servletClass;
    private String urlPattern;
	private Map<String, String> initParams;

    public ServletDefinition(String servletName, String servletClass, String urlPattern) {
     	this.servletName = servletName;
		this.servletClass = servletClass;
		this.urlPattern = urlPattern;
		this.initParams = new HashMap<String, String>();
    }

	/**
	 * Create servlet definition from package name and source java file.
	 * @param sqlFile .sql file with package, and name directives.
	 */
	public ServletDefinition(SQLFile sqlFile)
	throws IOException, JSQLParserException, NameNotFoundException, DirectiveNotFoundException {
		String packageName = sqlFile.getTag(Constants.Directives.PACKAGE);
		String servletPackageName = packageName + "." + Constants.Packages.SERVLET;
		this.servletName = sqlFile.getName() + Constants.Extensions.SERVLET;
		this.servletClass = servletPackageName + "." + servletName;
		this.urlPattern = UrlUtil.getPath(sqlFile.findTag(Constants.Directives.URL));
		this.initParams = new HashMap<String, String>();
	}

	public String getName(String packageName) {
		return packageName + "." + servletName;
	}

	public String getName() {
    	return servletName;
    }

    public String getClassName() {
    	return servletClass;
    }

    public String getUrlPattern() {
    	return urlPattern;
    }

	public Map<String, String> getParamMap() {
		return this.initParams;
	}

	public Set<String> getParamKeys() {
		return this.initParams.keySet();
	}

	public String getParam(String key) {
		return this.initParams.get(key);
	}

	public void addParam(String name, String value) {
		this.initParams.put(name, value);
	}
}

