package com.apptechnik.appinabox.engine.codegen.servlet;

import com.apptechnik.appinabox.engine.codegen.imports.CreateImports;
import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeGen;
import com.apptechnik.appinabox.engine.codegen.pojos.MethodCall;
import com.apptechnik.appinabox.engine.codegen.pojos.MethodCallStatement;
import com.apptechnik.appinabox.engine.codegen.pojos.StatementList;
import com.apptechnik.appinabox.engine.codegen.update.UpdateSQLObjects;
import com.apptechnik.appinabox.engine.codegen.query.CreateQueryObjects;
import com.apptechnik.appinabox.engine.codegen.main.CreateSQLObjects;
import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.exceptions.*;
import com.apptechnik.appinabox.engine.parser.pojos.*;
import com.apptechnik.appinabox.engine.util.FileUtil;
import com.apptechnik.appinabox.engine.util.Substitute;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.select.Select;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

/**
 * Create the servlet for servlets which aggregate the results of multiple SELECT statements.
 */
public class CreateServlet  {
    private final CreateSQLObjects createSQLObjects;
    private final List<String> nodeNames;

    public CreateServlet(CreateSQLObjects createSQLObjects, List<String> nodeNames) throws Exception {
        this.createSQLObjects = createSQLObjects;
        this.nodeNames = nodeNames;
    }

    public void createMultipleReturnServlet(CodeGen codegen,
                                            Tree<QueryFieldsNode> jsonResponseMapTree,
                                            List<Select> selectList,
                                            List<Statement> updateList,
                                            ParameterMap selectParameterMap)
    throws Exception {
        Properties properties = new Properties();
        commonServletSubstitions(codegen, properties, selectList, updateList);

        // substitute the SELECT statements if there are any.
        if (!selectList.isEmpty()) {
            StatementList selectListCalls =
                CreateQueryObjects.selectListFieldAssignments(codegen, jsonResponseMapTree, selectList, Constants.ServletVariables.DB_CONNECTION,
                                                              Constants.Variables.RESULT, selectParameterMap,
                                                              getCreateObjects().getJsonParameterMap(), getCreateObjects().isSingleFieldRequest());
            properties.put(Constants.Substitutions.SELECT_LISTS_INTO_FIELDS, selectListCalls.toString() + ";\n");
            StatementList queryImports = getCreateObjects().createQueryImports(codegen);
            properties.put(Constants.Substitutions.QUERY_IMPORTS, queryImports.toString());
        }
        File servletFile = getCreateObjects().javaFile(getCreateObjects().getServletPackageName(),
                                                       getCreateObjects().getServletName());
        Substitute.transformResource(Constants.ServletTemplates.POST_MULTIPLE_SERVLET, servletFile, properties);
    }

    /**
     * Create the servlet for GET or POST requests which execute a single SELECT call for their response.
     */
    public void createSingleReturnServlet(CodeGen codegen,
                                          List<Select> selectList,
                                          List<Statement> updateList,
                                          ParameterMap selectParamMap) throws Exception {
        Properties properties = new Properties();
        commonServletSubstitions(codegen, properties, selectList, updateList);

        // any select statements?  add their substitutions.
        if (!selectList.isEmpty()) {
            String responseName = getCreateObjects().getResponseName();
            MethodCallStatement selectListAssignment =
                    CreateQueryObjects.selectListAssignment(codegen, Constants.ServletVariables.DB_CONNECTION,
                                                            responseName, selectParamMap,
                                                            getCreateObjects().getJsonParameterMap(),
                                                            getCreateObjects().isSingleFieldRequest());
            properties.put(Constants.Substitutions.SELECT_LIST, selectListAssignment.toString() + ";");
            StatementList queryImports = getCreateObjects().createQueryImports(codegen);
            properties.put(Constants.Substitutions.QUERY_IMPORTS, queryImports.toString());
        }
        File servletFile = getCreateObjects().javaFile(getCreateObjects().getServletPackageName(),
                                                       getCreateObjects().getServletName());
        Substitute.transformResource(Constants.ServletTemplates.POST_SINGLE_SERVLET, servletFile, properties);
    }

    /**
     * Substitutions which are common to single and multiple return servlets: parse the query if there is one,
     * call the extract functions if they pass parameters to the SELECT calls.  Add the inserts and updates if there are any
     * @param properties
     * @throws IOException
     * @throws PropertiesException
     * @throws TypeNotFoundException
     * @throws NameNotFoundException
     * @throws UnsupportedParamTypeException
     */
    public void commonServletSubstitions(CodeGen codegen,
                                         Properties properties,
                                         List<Select> selectList,
                                         List<Statement> updateList)
    throws Exception {
        StatementList importStmtList = codegen.importStatementList(Constants.Templates.SERVLET_IMPORTS);
        createSQLObjects.setDefaultSubstitutions(codegen, properties, importStmtList.toString(),
                                                 getCreateObjects().getVarName(), selectList, updateList);
        if (getCreateObjects().getJsonResponseTree() != null) {
            properties.put(Constants.Substitutions.SERVLET_WRITE_OUTPUT,
                    FileUtil.getResource(CreateServlet.class, Constants.Fragments.SERVLET_WRITE_OUTPUT));
        }
        // create the assignment statement from the SQL SelectList call wrapper to the resulting list of objects.
        // read the list of imports and convert them to imports.  Create the list of statements to parse the query parameters.
        if (getCreateObjects().getJsonRequestTree() != null) {
            StatementList requestJsonAssignments =
                    getCreateObjects().getJsonParameterMap().parametersFromRequestJson(codegen, getCreateObjects().getRequestName(),
                            Constants.Variables.INPUT);
            properties.put(Constants.Substitutions.EXTRACT_LISTS, requestJsonAssignments.toString());
            if (!updateList.isEmpty()) {
                StatementList updateImports = getCreateObjects().createUpdateImports(codegen);
                properties.put(Constants.Substitutions.UPDATE_IMPORTS, updateImports.toString());
            }

            // substitute the INSERT/UPDATE statements if there are any
            if (!updateList.isEmpty()) {
                addInsertUpdateList(codegen, properties);
            }
        }
        List<Statement> unassignedStmtList =
            MergedFieldNode.filterUnassignedStatements(getCreateObjects().getJsonRequestTree(), updateList);
        if (!unassignedStmtList.isEmpty()) {
            addInsertUpdate(codegen, properties);
        }
    }

    /**
     * Add the INSERT and UPDATE LIST calls
     * @param properties
     */
    public void addInsertUpdateList(CodeGen codegen, Properties properties) {
        String requestName = getCreateObjects().getRequestName();
        ParameterMap paramMap = getCreateObjects().getQueryParams();
        String input = (getCreateObjects().getJsonRequestTree() != null) ? Constants.Variables.INPUT : Constants.JavaKeywords.NULL;
        MethodCall updateCall = UpdateSQLObjects.insertUpdateListCall(codegen, Constants.ServletVariables.DB_CONNECTION,
                                                                      requestName, input, paramMap);
        properties.put(Constants.Substitutions.INSERT_UPDATE_LIST, updateCall + ";\n");
    }

    /**
     * Add the INSERT and UPDATE calls
     * @param properties
     */
    public void addInsertUpdate(CodeGen codegen, Properties properties) {
        String requestName = getCreateObjects().getRequestName();
        ParameterMap paramMap = getCreateObjects().getQueryParams();
        MethodCall updateCall =
            UpdateSQLObjects.insertUpdateCall(codegen, Constants.ServletVariables.DB_CONNECTION, requestName, paramMap);
        properties.put(Constants.Substitutions.INSERT_UPDATE, updateCall + ";\n");
    }

    private CreateObjects getCreateObjects() {
        return createSQLObjects.getCreateObjects();
    }
}
