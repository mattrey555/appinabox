package com.apptechnik.appinabox.engine.codegen.sql;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.exceptions.NameNotFoundException;
import com.apptechnik.appinabox.engine.exceptions.TypeNotFoundException;
import com.apptechnik.appinabox.engine.parser.ColumnInfo;
import com.apptechnik.appinabox.engine.parser.types.SQLType;
import com.apptechnik.appinabox.engine.parser.pojos.*;
import com.apptechnik.appinabox.engine.codegen.util.CodegenUtil;
import com.apptechnik.appinabox.engine.util.ListUtil;
import com.apptechnik.appinabox.engine.util.OutputPackages;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.create.table.ColDataType;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Multiple INSERT, UPDATE. DELETE, and SELECT INTO staements can be combined, mapped from incoming JSON data in depth-first order.
 * A single SQL statement can access JSON values from the current subnode to the root node.  Multiple SQL statements can be
 * mapped to a single subnode.  The table maps to the JSON name of the current node. The top-level SQL statement
 * may apply to a subnode of the incoming JSON.  Not all incoming JSON tags must map to named parameters, but
 * named parameters must either map to a JSON tag, or the result of a previous SQL statement RETURNS clause.
 * The JSON values map to named parameters in the SQL statements.
 */
public class ChangeSQLObjects {
    private final CreateObjects createObjects;

    /**
     * Create the classes with methods for inserting/updating objects into the database
     * @param createObjects common data, such as the SQLFile, variables, properties, etc.
     */
    public ChangeSQLObjects(CreateObjects createObjects) {
        this.createObjects = createObjects;
    }

    /**
     * Called from the tree traversal to create a SQL file associated with a JSON mapping node.
     * @param stack stack of JSON mapping nodes from depth-first traversal
     * @param queryParams JDBC Named Parameter map.
     * @throws Exception
     */
    public ClassFile createSQLObject(CodeGen codegen,
                                     Stack<Tree<MergedFieldNode>> stack,
                                     ParameterMap queryParams) throws Exception {
        Tree<MergedFieldNode> currentNode = stack.peek();
        String objectName = currentNode.get().getVarName();
        String sqlFnsName = objectName + Constants.Suffixes.SQL_FNS;
        String sqlPackageName = OutputPackages.getUpdateSQLPackageName(createObjects.getPackageName());
        PackageStatement packageStatement = codegen.packageStmt(sqlPackageName);
        StatementList importStatementList = codegen.importStatementList(Constants.Templates.SQLFNS_IMPORTS);
        importStatementList.addStatements(createObjects.createUpdateImports(codegen));
        ClassDefinition classDef = sqlFnsClass(codegen, sqlFnsName, stack, queryParams);
        return codegen.classFile(packageStatement, importStatementList, classDef);
    }

    /**
     * Called from the top-level for INSERT/UPDATE/DELETS calls with parameters only from URL query/headers
     * (i.e. scalar)
     * @param statementList list of SQL statements.
     * @param queryParams JDBC Named Parameter map.
     * @throws Exception
     */
    public ClassFile createSQLObject(CodeGen codegen,
                                     List<Statement> statementList,
                                     ParameterMap queryParams)
    throws Exception {
        String sqlFnsName = createObjects.getRequestName() + Constants.Suffixes.SQL_FNS;
        String sqlPackageName = OutputPackages.getUpdateSQLPackageName(createObjects.getPackageName());
        StatementList importStatementList = codegen.importStatementList(Constants.Templates.SQLFNS_IMPORTS);
        importStatementList.addStatements(createObjects.createUpdateImports(codegen));
        ClassDefinition classDef = insertUpdateClass(codegen, sqlFnsName, queryParams, statementList);
        return codegen.classFile(codegen.packageStmt(sqlPackageName), importStatementList, classDef);
    }

    /**
     * Write the class with SQL methods.
     * @param className name of the class
     * @param nodeStack depth-first JSON mapping traversal to this point
     * @param queryParams map of JDBC Named Parameter to type information
     * @throws Exception
     */
    private ClassDefinition sqlFnsClass(CodeGen codegen,
                                        String className,
                                        Stack<Tree<MergedFieldNode>> nodeStack,
                                        ParameterMap queryParams) throws Exception {
        MergedFieldNode node = nodeStack.peek().get();
        ClassDeclaration classDecl = codegen.classDecl(className, codegen.classAnnot(Constants.Annotations.GENERATED));
        List<MemberDeclaration> sqlStatementVariables = CodegenUtil.sqlStatementVariables(codegen, node);
        List<FunctionDefinition> functions = Arrays.asList(
            createInsertsUpdates(codegen, nodeStack, createObjects.getStatementParamMap(), queryParams),
            createInsertUpdateList(codegen, nodeStack, queryParams, createObjects.getStatementParamMap()));
        return codegen.classDef(classDecl, sqlStatementVariables, null, functions);
    }

    /**
     * Write INSERT/UPDATE/DELETE function which ONLY takes parameters from NamedParameters
     * @param codegen code generator
     * @param className name of the class to generate
     * @param queryParams query parameters for UPDATE
     * @param statementList list of statements for settings SQL Named Paraemters.
     * @throws Exception
     */
    public ClassDefinition insertUpdateClass(CodeGen codegen,
                                             String className,
                                             ParameterMap queryParams,
                                             List<Statement> statementList) throws Exception {
        ClassDeclaration classDecl = codegen.classDecl(className, codegen.classAnnot(Constants.Annotations.GENERATED));
        List<MemberDeclaration> sqlVars = CodegenUtil.sqlStatementVariables(codegen, statementList);
        FunctionDefinition insertsUpdates = createInsertsUpdates(codegen, statementList, createObjects.getStatementParamMap(),
                                                                 queryParams);
        return codegen.classDef(classDecl, sqlVars, null, Collections.singletonList(insertsUpdates));
    }

    /**
     * Create the calls to populate the prepared statements from the depth-first stack of the JSON tree and the
     * query parameters.
     * @param treeNodeStack depth-first stack of the JSON tree.
     * @param statementParamMap map of JSON parameters to each statement.
     * @param queryParams parameter map from URL path + query + HTTP headers.
     * @return
     * @throws Exception
     */
    private static FunctionDefinition createInsertsUpdates(CodeGen codegen,
                                                           Stack<Tree<MergedFieldNode>> treeNodeStack,
                                                           StatementParamMap statementParamMap,
                                                           ParameterMap queryParams) throws Exception {
        Tree<MergedFieldNode> treeNode = treeNodeStack.peek();
        MergedFieldNode node = treeNode.get();
        List<MergedFieldNode> nodePath = treeNodeStack.stream().map(t -> t.get()).collect(Collectors.toList());
        // parameters: db connection, objects from nodePath, query parameters
        List<ParameterDeclaration> parameterDeclarations = new ArrayList<ParameterDeclaration>();
        parameterDeclarations.add(0, CodegenUtil.dbConnectionParameterDeclaration(codegen, Constants.Variables.DB_CONNECTION));
        parameterDeclarations.addAll(createParameterDeclarations(codegen, treeNodeStack, queryParams, statementParamMap));
        FunctionDeclaration functionDeclaration = CodegenUtil.updateInsertDeclaration(codegen, codegen.paramList(parameterDeclarations));
        List<CodeStatement> statementList = new ArrayList<CodeStatement>();
        // for each parameter in the parameter map, find the JSON node with a field reference
        // which matches the parameter.
        for (Statement statement : node.getUpdateStatements()) {
            ParameterMap parameterMap = statementParamMap.getParamMap(statement);
            statementList.add(createExecuteSQLStatement(codegen, node, statement));
            String statementVarName = node.getStatementVariableName(statement);

            // write setters for the current node, then traverse through ancestors.  It creates
            // a cleaner output and ensures bottom-most matching.  If the node is not found in the field list
            // check the command-line parameters.
            for (String jdbcParam : parameterMap.getParamSet()) {
                if (!parameterMap.isReturning(jdbcParam)) {
                    String dotParam = parameterMap.dotParamName(jdbcParam);
                    MethodCall setNamedParamCall = createSetNamedParamCall(codegen, statementVarName, treeNodeStack, dotParam,
                                                                           parameterMap, statementParamMap, queryParams, jdbcParam);
                    statementList.add(codegen.methodCallStmt(setNamedParamCall));
                }
            }
            // <statement>.execute()
            statementList.add(codegen.methodCallStmt(codegen.methodCall(codegen.var(statementVarName), Constants.Functions.EXECUTE)));

            // if the statement had a RETURNING call, then get the values from the result set and assign them to THIS object.
            // ResultSet rs<StatementVar> = stmt.getResultSet();
            // rs<StatementVar>.next();
            // <ObjectVar>.last_updated_person.get<JavaType>("<additionalColumName>"); ...
            if (node.hasAdditionalColumns(statement)) {
                statementList.addAll(readReturnedColumns(codegen, node, statement));
            }
        }
        // when calling insertUpdateList() for each child, the parameters are defined as:
        // 0: database connection
        // 1: child object referenced through accessor from current object <parent>.get<child>()
        // 2: list of objects from depth first traversal to this object, which were passed as arguments to this method.
        // 3: parameters from command-line.

        for (Tree<MergedFieldNode> childTree : treeNode.getChildren()) {
            statementList.add(insertUpdateListChild(codegen, treeNode, childTree, nodePath, queryParams));
        }
        return codegen.functionDef(functionDeclaration, codegen.stmtList(statementList));
    }

    /**
     * INSERT and UPDATE return columns in a RETURNING clause. These columns can be passed as JDBC Named Parameters
     * to other SQL calls.
     * ResultSet <statement-var>ResultSet = <statement-var></statement-var>
     * <statement-var>ResultSet.next()
     * for each column in the RETURNING clause:
     * <var>.set<column-name>(rs.get<type(column)>)
     * @param node current
     * @param statement
     * @return
     * @throws TypeNotFoundException
     */
    private static List<CodeStatement> readReturnedColumns(CodeGen codegen,
                                                           MergedFieldNode node,
                                                           Statement statement) throws TypeNotFoundException {
        List<CodeStatement> statementList = new ArrayList<CodeStatement>();
        MethodCall resultSetCall = codegen.methodCall(codegen.var(node.getStatementVariableName(statement)),
                                                      Constants.Functions.GET_RESULT_SET);
        String resultSetVarName = node.getResultSetVariableName(statement);
        Variable resultSetVar = codegen.var(resultSetVarName);
        statementList.add(codegen.typedAssignStmt(codegen.classType(Constants.JavaTypes.RESULT_SET), resultSetVar, resultSetCall));
        MethodCall resultSetNextCall = codegen.methodCall(resultSetVar, Constants.Functions.NEXT);
        statementList.add(codegen.methodCallStmt(resultSetNextCall));
        for (ColumnInfo returnedColumn : node.getAdditionalColumns(statement)) {
            MethodCall setObjectValueCall =
                CodegenUtil.getResultSetValue(codegen, resultSetVarName, node.getVarName(), returnedColumn);
            statementList.add(codegen.methodCallStmt(setObjectValueCall));
        }
        return statementList;
    }

    private static MethodCallStatement insertUpdateListChild(CodeGen codegen,
                                                             Tree<MergedFieldNode> treeNode,
                                                             Tree<MergedFieldNode> childTree,
                                                             List<MergedFieldNode> nodePath,
                                                             ParameterMap queryParams) {
        List<CodeExpression> exprList = nodePath.stream()
                .map(n -> codegen.var(n.getVarName()))
                .collect(Collectors.toList());
        exprList.add(0, codegen.var(Constants.Variables.DB_CONNECTION));
        exprList.add(1, CodegenUtil.accessorCall(codegen, codegen.var(treeNode.get().getVarName()),
                                                       childTree.get().getVarName()));
        exprList.addAll(queryParams.getParamNames().stream().map(param -> codegen.var(param)).collect(Collectors.toList()));
        ArgumentList argList = codegen.argList(exprList);
        Variable childSQLFns = codegen.var(childTree.get().getVarName() + Constants.Suffixes.SQL_FNS);
        MethodCall childInsertUpdateListCall = codegen.methodCall(childSQLFns, Constants.Functions.INSERT_UPDATE_LIST, argList);
        return codegen.methodCallStmt(childInsertUpdateListCall);
    }

    /**
     * Function which loops over insertsUpdates()
     * @param treeNodeStack stack of objects to this point
     * @param queryParams query parameters from URL
     * @return
     * @throws Exception
     */
    private static FunctionDefinition createInsertUpdateList(CodeGen codegen,
                                                             Stack<Tree<MergedFieldNode>> treeNodeStack,
                                                             ParameterMap queryParams,
                                                             StatementParamMap statementParamMap)
    throws TypeNotFoundException, NameNotFoundException {
        MergedFieldNode node = treeNodeStack.peek().get();
        Type itemType = MergedFieldNode.getNodeType(codegen,  treeNodeStack.peek(), statementParamMap);
        Variable itemVariable = codegen.var(node.getVarName());

        // top of the stack is end of the list. prepend List<simpleType(this)> to parameters.
        List<ParameterDeclaration> parameterDeclarations = new ArrayList<ParameterDeclaration>();

        // top-level list insert/update takes list of top-level object. Children take list of object, and
        // individual reference to parents.
        // REPLACE THE NODE DECLARATION WITH A LIST OF THAT NODE.
        // just after the database connection, and before the other parameters.
        // database connection is first parameter.
        parameterDeclarations.add(CodegenUtil.dbConnectionParameterDeclaration(codegen, Constants.Variables.DB_CONNECTION));
        parameterDeclarations.add(CodegenUtil.listParameterDeclaration(codegen, itemType, node.getVarName()));
        parameterDeclarations.addAll(createParameterDeclarations(codegen, ListUtil.exceptLast(treeNodeStack),
                                                                 queryParams, statementParamMap));
        FunctionDeclaration declaration = CodegenUtil.updateInsertListDeclaration(codegen, codegen.paramList(parameterDeclarations));
        ForIteration forIterator = codegen.forIter(itemType, itemVariable, codegen.var(node.getVarName() + Constants.Suffixes.LIST));

        // the parameter list is db,list<object>, param1, param2, param3
        // change it to db,<object>, param1, param2, param3
        List<CodeExpression> argumentList = CodegenUtil.declarationsToParameters(parameterDeclarations);
        // itemVar is object calling method, so remove it from parameter list. The first parameter
        // is the database connection.
        argumentList.set(1, itemVariable);
        Variable itemSQLFunctions = codegen.var(node.getVarName() + Constants.Suffixes.SQL_FNS);
        MethodCall elementInsertUpdateCall = codegen.methodCall(itemSQLFunctions, Constants.Functions.INSERT_UPDATE,
                                                                codegen.argList(argumentList));
        ForLoop forLoop = codegen.forLoop(forIterator, codegen.stmtList(codegen.methodCallStmt(elementInsertUpdateCall)));
        MethodCall commitCall = codegen.methodCall(codegen.var(Constants.Variables.DB_CONNECTION), Constants.Functions.COMMIT);
        return codegen.functionDef(declaration, codegen.stmtList(forLoop, codegen.methodCallStmt(commitCall)));
    }

    /**
     * Create the NamedParamStatement call
     * NamedParamStatement <statement-var> statement = new NamedParamStatement(conn, "<sql-string-constant>");
     * @param node mergedFieldNode JSON mapping node for this object.
     * @param statement SQL UPDATE/INSERT/DELETE statement
     * @return typed assignment statement.
     */
    private static TypedAssignmentStatement createExecuteSQLStatement(CodeGen codegen, MergedFieldNode node, Statement statement) {
        return createExecuteSQLStatement(codegen, node.getStatementVariableName(statement),
                                         node.getSQLConstantName(statement));
    }

    private static TypedAssignmentStatement createExecuteSQLStatement(CodeGen codegen,
                                                                      String statementVarName,
                                                                      String sqlConstantName) {
        Variable statementVar = codegen.var(statementVarName);
        Variable sqlStringVar = codegen.var(sqlConstantName);
        Variable connVar = codegen.var(Constants.Variables.DB_CONNECTION);
        ArgumentList argumentList = codegen.argList(connVar, sqlStringVar);
        ClassType statementType = codegen.classType(Constants.JavaTypes.NAMED_PARAM_STATEMENT);
        Allocation allocation = codegen.alloc(statementType, argumentList);
        return codegen.typedAssignStmt(statementType, statementVar, allocation);
    }

    /**
     * Create the parameter declarations for insertUpdate().  Also used by insertUpdateList, but the SECOND parameter
     * (not the database connection) is replaced with a list.
     * @param treePath stack of JSON field reference nodes with children
     * @param queryParams map of JDBC Named parameters to types
     * @return ParameterList declaration.
     * @throws TypeNotFoundException
     */
    private static List<ParameterDeclaration> createParameterDeclarations(CodeGen codegen,
                                                                          Collection<Tree<MergedFieldNode>> treePath,
                                                                          ParameterMap queryParams,
                                                                          StatementParamMap statementParamMap)
    throws TypeNotFoundException, NameNotFoundException {
        List<ParameterDeclaration> params = new ArrayList<ParameterDeclaration>();
        for (Tree<MergedFieldNode> tree : treePath) {
            params.add(codegen.paramDecl(MergedFieldNode.getNodeType(codegen, tree, statementParamMap),
                                         codegen.var(tree.get().getVarName())));
        }

        // we reverse the list, because it's actually a stack, and the top of the stack is the end of the list
        // and we want the parameters to be ordered by child, parent, grandparent, etc.
        Collections.reverse(params);
        params.addAll(CodegenUtil.parameterMapToDeclarations(codegen, queryParams));
        return params;
    }

    /**
     * Create the MethodCall which sets a typed JDBCNamedParameter in a NamedParamStatement from a reference
     * in the JSON tree, which may be a dot.reference, or the return value of the SQL call from an ancestor.
     * @param statementVarName NamedParamStatement variable name
     * @param nodeTreePath list of JSON nodes from this level with optional dot.reference and ancestor SQL statement return values.
     * @param dotParam name of field to set (scan JSON nodes and query parameters)
     * @param parameterMap map of names to inferred or explicit types
     * @param queryParams parameters from URL header/query strings.
     * @param jdbcParam parameter being set (may be dot.parameter)
     * @return MethodCall to set the named parameter by specified type.
     * @throws Exception
     */
    private static MethodCall createSetNamedParamCall(CodeGen codegen,
                                                      String statementVarName,
                                                      List<Tree<MergedFieldNode>> nodeTreePath,
                                                      String dotParam,
                                                      ParameterMap parameterMap,
                                                      StatementParamMap statementParamMap,
                                                      ParameterMap queryParams,
                                                      String jdbcParam) throws Exception {
         Variable statementVar = codegen.var(statementVarName);

         // Find the node in the path
         Tree<MergedFieldNode> matchingNodeTree = MergedFieldNode.matchNodeTree(nodeTreePath, dotParam);

         // search in this node to see if there is a field reference which matches this JDBC Named parameter
         if (matchingNodeTree != null) {
             JsonFieldReference fieldRef = matchingNodeTree.get().matchingFieldReference(dotParam);
             ColDataType colDataType = parameterMap.getColDataType(jdbcParam);
             Variable var = codegen.var(matchingNodeTree.get().getVarName());
             ColumnInfo colInfo = matchingNodeTree.get().matchAdditionalColumn(dotParam);

             // match additional columns first, since they may have a different data type.
             if (colInfo != null) {
                 MethodCall accessorCall = CodegenUtil.accessorCall(codegen, var, jdbcParam);
                 return SQLType.setNamedParamFromSqlType(codegen, colDataType.getDataType(), colInfo.getDataType(),
                                                         statementVar, codegen.stringConst(jdbcParam), accessorCall);
             } else if (fieldRef != null) {

                 // get all of the statements associated with nodes up to this path, and find the "most promoted type"
                 // as the source type. The destination type is
                 List<Statement> associatedStatements = MergedFieldNode.getAssociatedStatements(nodeTreePath);
                 ParameterMap.ParamData paramData = statementParamMap.getBestMatch(associatedStatements, jdbcParam);
                 CodeExpression valueExpr =
                     FieldReferenceNode.isSingleField(matchingNodeTree) ? var : CodegenUtil.accessorCall(codegen, var, jdbcParam);
                 return SQLType.setNamedParamFromSqlType(codegen, colDataType.getDataType(), paramData.getDataType().getDataType(), statementVar,
                                                         codegen.stringConst(jdbcParam), valueExpr);
             }
         }
         return createSetNamedParamCall(codegen, statementVarName, dotParam, queryParams, jdbcParam);
     }

    /**
     * Simpler method which just scans the query paramter map, not the incoming JSON.
     * @param statementVarName NamedParamStatement variable name
     * @param dotParam name of field to set (scan JSON nodes and query parameters)
     * @param jdbcParam parameter being set (may be dot.parameter)
     * @return MethodCall to set the named parameter by specified type.
     * @throws TypeNotFoundException
     */
    private static MethodCall createSetNamedParamCall(CodeGen codegen,
                                                      String statementVarName,
                                                      String dotParam,
                                                      ParameterMap queryParams,
                                                      String jdbcParam)
    throws TypeNotFoundException, NameNotFoundException {
        Variable statementVar = codegen.var(statementVarName);
        ColDataType paramType = queryParams.getColDataType(dotParam);
        return SQLType.setNamedParamFromSqlType(codegen, paramType.getDataType(), paramType.getDataType(),
                                                statementVar, codegen.stringConst(jdbcParam), codegen.var(jdbcParam));
    }

    /**
     * Variant of createInsertsUpdates (usually for DELETE statements) where NONE of the parameters are from
     * the request JSON (often if the request JSON is null).  All parameters must come from queryParams.
     * @param statementList list of SQL statements.
     * @param statementParamMap map of SQL statements to Named parameters.
     * @param queryParams URL query/header params
     * @return
     * @throws TypeNotFoundException
     * @throws NameNotFoundException
     */
    private static FunctionDefinition createInsertsUpdates(CodeGen codegen,
                                                           List<Statement> statementList,
                                                           StatementParamMap statementParamMap,
                                                           ParameterMap queryParams)
            throws TypeNotFoundException, NameNotFoundException {
        // parameters: db connection, objects from nodePath, query parameters
        List<ParameterDeclaration> parameterDecls = new ArrayList<ParameterDeclaration>();
        parameterDecls.add(0, CodegenUtil.dbConnectionParameterDeclaration(codegen, Constants.Variables.DB_CONNECTION));
        parameterDecls.addAll(CodegenUtil.parameterMapToDeclarations(codegen, queryParams));
        FunctionDeclaration functionDecl = CodegenUtil.updateInsertDeclaration(codegen, codegen.paramList(parameterDecls));
        // for each parameter in the parameter map, find the JSON node with a field reference
        // which matches the parameter.
        String statementVarPrefix = Constants.Variables.STATEMENT;
        String sqlConstantPrefix = Constants.Variables.SQL;
        int varIndex = 1;
        List<CodeStatement> codeStatementList = new ArrayList<CodeStatement>();
        for (Statement statement : statementList) {
            String statementVarName = statementVarPrefix + varIndex;
            String sqlConstant = sqlConstantPrefix + varIndex;
            varIndex++;
            ParameterMap parameterMap = statementParamMap.getParamMap(statement);
            codeStatementList.add(createExecuteSQLStatement(codegen, statementVarName, sqlConstant));

            // write setters for the current node, then traverse through ancestors.  It creates
            // a cleaner output and ensures bottom-most matching.  If the node is not found in the field list
            // check the command-line parameters.
            for (String jdbcParam : parameterMap.getParamSet()) {
                String dotParam = parameterMap.dotParamName(jdbcParam);
                if (!parameterMap.isReturning(dotParam)) {
                    MethodCall setNamedParamCall = createSetNamedParamCall(codegen, statementVarName, dotParam, queryParams, jdbcParam);
                    codeStatementList.add(codegen.methodCallStmt(setNamedParamCall));
                }
            }
            // <statement>.execute()
            codeStatementList.add(codegen.methodCallStmt(codegen.methodCall(codegen.var(statementVarName),
                                                                            Constants.Functions.EXECUTE)));
        }
        return codegen.functionDef(functionDecl, codegen.stmtList(codeStatementList));
    }

    public CreateObjects getCreateObjects() {
        return createObjects;
    }
}

