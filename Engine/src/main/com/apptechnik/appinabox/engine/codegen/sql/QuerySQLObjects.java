package com.apptechnik.appinabox.engine.codegen.sql;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.codegen.query.PrimaryKeyCompare;
import com.apptechnik.appinabox.engine.codegen.query.ResponseObjects;
import com.apptechnik.appinabox.engine.exceptions.NameNotFoundException;
import com.apptechnik.appinabox.engine.exceptions.TypeNotFoundException;
import com.apptechnik.appinabox.engine.parser.ColumnInfo;
import com.apptechnik.appinabox.engine.parser.types.SQLType;
import com.apptechnik.appinabox.engine.parser.pojos.*;
import com.apptechnik.appinabox.engine.codegen.util.CodegenUtil;
import net.sf.jsqlparser.statement.select.Select;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Create the java files for SQL methods
 */
public class QuerySQLObjects {
    private final CreateObjects createObjects;                                          // grab-bag from the source file
    private final Select select;                                                        // source SELECT statement
    private final Collection<ColumnInfo> columnInfoList;                                // columns and types from SELECT items
    private final Collection<ColumnInfo> primaryKeyList;                                // primary keys used to create PrimaryKeySelectColumns
    private final ParameterMap parameterMap;                                            // parameters from this select statement.


    public QuerySQLObjects(CreateObjects createObjects,
                           Select select,
                           Collection<ColumnInfo> columnInfoList,
                           Collection<ColumnInfo> primaryKeyList,
                           ParameterMap parameterMap) {
        this.createObjects = createObjects;
        this.select = select;
        this.columnInfoList = columnInfoList;
        this.primaryKeyList = primaryKeyList;
        this.parameterMap = parameterMap;
    }

    /**
     * Create the SQL functions for an object.
     * @param rootNode root node contains inner declaration of PrimaryKeySourceColumns.
     * @param packageName package name for POJOs
     * @param sqlPackageName package name for SQL fns.
     * @param node current node in JSON mappping
     * @param isRoot this is the root node, so it gets extra functions
     * @return class file
     * @throws Exception
     */
    public ClassFile create(CodeGen codegen,
                            QueryFieldsNode rootNode,
                            String packageName,
                            String sqlPackageName,
                            Tree<QueryFieldsNode> node,
                            boolean isRoot) throws Exception {
        PackageStatement packageStatement = codegen.packageStmt(sqlPackageName);
        StatementList importStatementList = codegen.importStatementList(Constants.Templates.MAIN_IMPORTS);

        // import the objects referenced by the SQL functions (if we're not a primitive field)
        if (!FieldReferenceNode.isSingleField(node)) {
            importStatementList.addStatement(codegen.importStmt(packageName + ".*"));
            importStatementList.addStatement(codegen.importStmt(sqlPackageName + ".*"));
            importStatementList.addStatements(ResponseObjects.importsFromChildren(codegen, packageName, node));
            importStatementList.addStatements(importSqlFunctionsFromChildren(codegen, sqlPackageName, node));
        }
        ClassDefinition classDef = generateSQLFunctionsClass(codegen, rootNode, isRoot, node);
        return codegen.classFile(packageStatement, importStatementList, classDef);
    }

    /**
     * Import the classes of SQL functions for children of this object.
     * @param sqlPackageName SQL package name.
     * @param selectItemTree field reference node and childern
     * @return list of import statements.
     */
    private static StatementList importSqlFunctionsFromChildren(CodeGen codegen,
                                                                String sqlPackageName,
                                                                Tree<? extends FieldReferenceNode> selectItemTree) {
        return codegen.stmtList(selectItemTree.getChildNodes().stream()
            .map(child -> codegen.importStmt(sqlPackageName + "." + child.getVarName() + Constants.Suffixes.SQL_FNS))
            .collect(Collectors.toList()));
    }

    /**
     * Generate the class of SQL functions for reading this object from a result set.
     * @param rootNode root node contains inner declaration of PrimaryKeySourceColumns.
     * @param root flag: root objects get ReadListFromResultSet and SelectList methods
     * @param jsonMap JSON mapping
     * @return class definition.
     * @throws Exception
     */
    public ClassDefinition generateSQLFunctionsClass(CodeGen codegen,
                                                     QueryFieldsNode rootNode,
                                                     boolean root,
                                                     Tree<QueryFieldsNode> jsonMap) throws Exception {
        QueryFieldsNode node = jsonMap.get();
        ClassDeclaration classDecl = codegen.classDecl(node.getSQLClassName());
        List<FunctionDefinition> functions = new ArrayList<FunctionDefinition>();
        boolean isSingleField = FieldReferenceNode.isSingleField(jsonMap);
        Type nodeType = QueryFieldsNode.getNodeType(codegen, jsonMap);
        if (!isSingleField) {
            functions.add(readFromResultSet(codegen, rootNode, nodeType, jsonMap, columnInfoList));
        }
        List<ClassDefinition> innerClasses = null;
        if (root) {
            functions.add(readListFromResultSet(codegen, node, nodeType, isSingleField));
            functions.add(selectList(codegen, select, nodeType, node.getSQLClassName(), parameterMap));
            innerClasses = Collections.singletonList(PrimaryKeyCompare.createClass(codegen, primaryKeyList));
        }
        return codegen.classDef(classDecl, null, null, functions, innerClasses);
    }

    /**
     * Generate:
     * public static void readFromResultSet(ResultSet rs,<object-name> o,boolean readFlag,PrimaryKeySourceColumns pksc) throws SQLException
     * @param objectType
     * @return
     */
    private static FunctionDeclaration readFromResultSetDeclaration(CodeGen codegen, QueryFieldsNode rootNode, Type objectType) {
        // TODO: should be a type constructor instead of string composition.
        Type pkscType = codegen.classType(rootNode.getSQLClassName() + "." + Constants.JavaTypes.PRIMARY_KEY_SOURCE_COLUMNS);
        ParameterList parameterList =
            codegen.paramList(codegen.paramDecl(codegen.classType(Constants.JavaTypes.RESULT_SET),
                                                Constants.Variables.RS),
                              codegen.paramDecl(objectType, codegen.var(Constants.Variables.OBJECT_NAME)),
                              codegen.paramDecl(codegen.simpleType(Constants.JavaTypes.BOOLEAN),
                                                Constants.Variables.READ_FLAG),
                              codegen.paramDecl(pkscType, codegen.var(Constants.Variables.PKSC)));
        TypeList exceptions = codegen.typeList(codegen.classType(Constants.JavaExceptions.SQL_EXCEPTION));
        return codegen.functionDecl(true, Constants.Functions.READ_FROM_RESULT_SET,
                                    parameterList, codegen.simpleType(Constants.JavaTypes.VOID), exceptions);
    }

    /**
     * if (readFlag) {
     *      <javaType><varName>Temp = rs.get<javaType>(<sqlVarName>);
     *          if (!rs.wasNull()) {
     *              <objectName>.set<varName>(<varName>Temp);
     *          }
     * }
     * @param rootNode root node contains inner declaration of PrimaryKeySourceColumns.
     * @param queryTree
     * @param columnInfoList
     * @return
     * @throws Exception
     */
    private FunctionDefinition readFromResultSet(CodeGen codegen,
                                                 QueryFieldsNode rootNode,
                                                 Type nodeType,
                                                 Tree<QueryFieldsNode> queryTree,
                                                 Collection<ColumnInfo> columnInfoList) throws Exception {
        // For each column in the select list referred to by the list of field references, generate:
        // <javaType><varName>Temp = rs.get<javaType>(<sqlVarName>);
        // if (!rs.wasNull()) {
        //      <objectName>.set<varName>(<varName>Temp);
        // }
        List<CodeStatement> ifReadFlagStats = new ArrayList<CodeStatement>();
        for (ColumnInfo columnInfo : queryTree.get().getColumnInfos()) {
            ifReadFlagStats.addAll(applySetterToResultSet(codegen, Constants.Variables.OBJECT_NAME, columnInfo));
        }
        // if (readFlag) { all the stuff above }
        List<CodeStatement> stmtList = new ArrayList<CodeStatement>();
        stmtList.add(codegen.ifStmt(codegen.var(Constants.Variables.READ_FLAG), codegen.stmtList(ifReadFlagStats)));

        // for each child,
        for (Tree<QueryFieldsNode> child : queryTree.getChildren()) {
            stmtList.addAll(readResultSetChildRecursions(codegen, queryTree, child, columnInfoList));
        }
        return codegen.functionDef(readFromResultSetDeclaration(codegen, rootNode, nodeType), codegen.stmtList(stmtList));
    }

    /**
     *
     * <child-type-name> new<child-var-name>;
     * boolean <readFlagVarName> = <readFlagVarName> || isChanged(pksc, <root-class-name>.<PRIMARY_KEY>) [ || ...]
     * @param parent
     * @param child
     * @param columnInfoList
     * @return
     * @throws Exception
     */
    private List<CodeStatement> readResultSetChildRecursions(CodeGen codegen,
                                                             Tree<QueryFieldsNode> parent,
                                                             Tree<QueryFieldsNode> child,
                                                             Collection<ColumnInfo> columnInfoList)
            throws Exception {
        List<CodeStatement> statementList = new ArrayList<CodeStatement>();
        Type childType  = QueryFieldsNode.getNodeType(codegen, child);
        String childVarName = child.get().getVarName();
        String newChildVarName = Constants.JavaKeywords.NEW + childVarName;

        // boolean <child-name>ReadFlag = readFlag || pksc.is<child-var-key>Changed() [ || ...]
        String childReadFlagName = childVarName + Constants.Variables.READ_FLAG;
        CodeExpression condition = primaryKeyConditionExpr(codegen, Constants.Variables.READ_FLAG, child.get().getKeys());
        statementList.add(codegen.typedAssignStmt(Constants.JavaTypes.BOOLEAN, childReadFlagName, condition));

        // if child's keys are same as parent's keys
        // <child-name> = new <child-name>();
        // <child-name>SQNFns.readFromResultSet(rs, <child-name>, pksc);
        // if child has a single column:
        // <child-type> <child-var>Temp = rs.get<JavaSQLType>("<column-name>")
        // if (!rs.wasNull()) {
        //   o.get<child-var>().add(<child-var>Temp)
        // }
        // if it has multiple columns:
        // new<child-name> = new <child-name>();
        // o.get<Child-name>.add(new<child-name>)
        // <child-name>SQNFns.readFromResultSet(rs, new<child-name>, pksc);
        if (child.get().childUsesParentKeys(parent.get())) {
            statementList.add(recurseChild(codegen, child.get()));
        } else {
            // <child-type-name> new<child-var-name>;
            statementList.add(codegen.varDecl(childType, codegen.var(newChildVarName)));
            List<CodeStatement> ifStatementList = QueryFieldsNode.isSingleField(child) ?
                                                        recurseSingleFieldList(codegen, child.get(), childType, columnInfoList) :
                                                        recurseChildList(codegen, child.get(), childType);
            statementList.add(codegen.ifStmt(codegen.var(childReadFlagName), codegen.stmtList(ifStatementList)));
        }
        return statementList;
    }

    private static CodeStatement recurseChild(CodeGen codegen, FieldReferenceNode child) {
        String childVarName = child.getVarName();
        String childReadFlagName = childVarName + Constants.Variables.READ_FLAG;
        MethodCall childAccessorCall = CodegenUtil.accessorCall(codegen, Constants.Variables.OBJECT_NAME, childVarName);
        // the child is already allocated in the constructor, like the lists are, but because it's
        // scalar, we don't need to allocate it before reading.
        // NOTE: readFromResultSet is a static call on each class, and takes the object as an argument.
        // <child-name>SQNFns.readFromResultSet(rs, new<child-name>, pksc);
        return codegen.methodCallStmt(readFromResultSetCall(codegen, child.getClassName(), childAccessorCall, childReadFlagName));
    }

    /**
     * When a node is a single column of a primitive type, our classes and associated SQL calls change.
     * <child-type> <child-var>Temp = rs.get<JavaSQLType>("<column-name>")
     * if (!rs.wasNull()) {
     *     o.get<child-var>().add(<child-var>Temp)
     * }
     * @param child
     * @param childType
     * @param columnInfoList
     * @return
     * @throws TypeNotFoundException
     */
    private static List<CodeStatement> recurseSingleFieldList(CodeGen codegen,
                                                              QueryFieldsNode child,
                                                              Type childType,
                                                              Collection<ColumnInfo> columnInfoList)
            throws TypeNotFoundException {
        List<CodeStatement> statementList = new ArrayList<CodeStatement>();
        Variable childVar = codegen.var(child.getVarName());
        Variable tempChildVar = codegen.var(child.getVarName() + Constants.Variables.TEMP);
        String sqlTypeName = child.getSingleLeafNodeSQLType();
        String sqlVarName = child.getColumnInfos().get(0).getColumnName(true);

        // <child-type> <child-var>Temp = rs.get<JavaSQLType>("<column-name>")
        MethodCall resultSetCall = SQLType.resultSetCallFromSqlType(codegen, Constants.Variables.RS, sqlTypeName, sqlVarName);
        statementList.add(codegen.typedAssignStmt(childType, tempChildVar, resultSetCall));

        // if (!rs.wasNull()) {
        //   o.get<child-var>().add(<child-var>Temp)
        // }
        MethodCall childListAccessorRef = CodegenUtil.accessorCall(codegen, Constants.Variables.OBJECT_NAME, childVar.getName());
        MethodCall setResultCall = codegen.methodCall(childListAccessorRef, Constants.Functions.ADD, tempChildVar);
        CodeExpression booleanExpr = codegen.notExpr(codegen.methodCall(codegen.var(Constants.Variables.RS), Constants.Functions.WAS_NULL));
        statementList.add(codegen.ifStmt(booleanExpr, codegen.stmtList(codegen.methodCallStmt(setResultCall))));
        return statementList;
    }

    /**
     * TODO: see if this can call recurseChild()
     * Recurse into a child with an object (i.e. non-primitive) type
     * new<child-name> = new <child-name>();
     * o.get<Child-name>.add(new<child-name>)
     * <child-name>SQNFns.readFromResultSet(rs, new<child-name>, pksc);
     * @param child
     * @param childType
     * @return
     * @throws TypeNotFoundException
     */
    private static List<CodeStatement> recurseChildList(CodeGen codegen, FieldReferenceNode child, Type childType) {
        String childVarName = child.getVarName();
        String childReadFlagName = childVarName + Constants.Variables.READ_FLAG;
        Variable newChildVar = codegen.var(Constants.JavaKeywords.NEW + child.getVarName());
        List<CodeStatement> statementList = new ArrayList<CodeStatement>();
        // complex simpleType, either allocate a new node, or use the last one.
        // new<child-name> = new <child-name>();
        statementList.add(codegen.assignStmt(newChildVar, codegen.alloc(childType)));

        // o.get<Child-name>.add(new<child-name>)
        MethodCall childAccessorCall = CodegenUtil.accessorCall(codegen, Constants.Variables.OBJECT_NAME, childVarName);
        MethodCall addListCall = codegen.methodCall(childAccessorCall, Constants.Functions.ADD, codegen.argList(newChildVar));
        statementList.add(codegen.methodCallStmt(addListCall));
        // NOTE: readFromResultSet is a static call on each class, and takes the object as an argument.
        // <child-name>SQNFns.readFromResultSet(rs, new<child-name>, pksc);
        statementList.add(codegen.methodCallStmt(readFromResultSetCall(codegen, child.getClassName(), newChildVar, childReadFlagName)));
        return statementList;
    }

    /**
     * Generate:
     * public static List<object-name> readListFromResultSet(ResultSet rs) throws SQLException
     * ReadListFromResultSet should only be generated for the top-level object.
     * @param node top-level select item node for this SQL->JSON mapping object
     * @param nodeType node type (may be a boxed primitive)
     * @param isSingleField apply to list of boxed primitives rather than object.
     * @return
     */
    private static FunctionDefinition readListFromResultSet(CodeGen codegen,
                                                            QueryFieldsNode node,
                                                            Type nodeType,
                                                            boolean isSingleField)
    throws TypeNotFoundException {
        String varName = node.getVarName();
        String listName = varName + Constants.JavaTypes.LIST;
        GenericType listType = codegen.listType(nodeType);
        ParameterDeclaration resultSetParam = codegen.paramDecl(codegen.classType(Constants.JavaTypes.RESULT_SET),
                Constants.Variables.RS);
        TypeList exceptions = codegen.typeList(codegen.classType(Constants.JavaTypes.SQL_EXCEPTION));

        // public static List<<object-name>> readListFromResultSet(ResultSet rs) throws SQLException
        FunctionDeclaration functionDecl =
            codegen.functionDecl(true, Constants.Functions.READ_LIST_FROM_RESULT_SET,
                                 codegen.paramList(resultSetParam), listType, exceptions);
        List<CodeStatement> statementList = new ArrayList<CodeStatement>();

        // PrimaryKeySourceColumns pksc = new PrimaryKeySourceColumns();
        // NOTE: in the single field case, this is allocated and passed, but not actually used.
        statementList.add(allocatePrimaryKeySourceColumns(codegen));

        // List<<object-name>> <object-name>List = new ArrayList<<object-name>>();
        statementList.add(CodegenUtil.listAllocAssignVar(codegen, codegen.var(listName), nodeType));

        // boolean readFlag = true
        statementList.add(codegen.typedAssignStmt(codegen.simpleType(Constants.JavaTypes.BOOLEAN),
                                                  codegen.var(Constants.Variables.READ_FLAG),
                                                  codegen.booleanConst(true)));

        // while (rs.next()) {
        List<CodeStatement> whileStatementList = isSingleField ?
            primitiveReadStatements(codegen, codegen.var(listName), nodeType, node.getColumnInfos().get(0)) :
            complexReadStatements(codegen, node, nodeType);
        Variable rsVar = codegen.var(Constants.Variables.RS);
        MethodCall rsNext = codegen.methodCall(rsVar, Constants.Functions.NEXT);
        statementList.add(codegen.whileLoop(rsNext, codegen.stmtList(whileStatementList)));
        statementList.add(codegen.returnStmt(codegen.var(listName)));
        return codegen.functionDef(functionDecl, codegen.stmtList(statementList));
    }

    private static List<CodeStatement> primitiveReadStatements(CodeGen codegen,
                                                               Variable listVar,
                                                               Type nodeType,
                                                               ColumnInfo columnInfo)
    throws TypeNotFoundException {
        List<CodeStatement> list = new ArrayList<CodeStatement>();
        list.add(codegen.typedAssignStmt(nodeType, codegen.var(Constants.Variables.OBJECT_NAME),
                                         resultSetGetValueCall(codegen, columnInfo)));
        list.add(codegen.methodCallStmt(codegen.methodCall(listVar, Constants.Functions.ADD,
                                                           codegen.var(Constants.Variables.OBJECT_NAME))));
        return list;
    }

    /**
     *
     * @param node
     * @param nodeType
     * @return
     */
    private static List<CodeStatement> complexReadStatements(CodeGen codegen, QueryFieldsNode node, Type nodeType) {
        List<CodeStatement> list = new ArrayList<CodeStatement>();
        Variable rsVar = codegen.var(Constants.Variables.RS);
        String listName = node.getVarName() + Constants.JavaTypes.LIST;

        // <nodeType> <objectName>;
        Variable objectVar = codegen.var(Constants.Variables.OBJECT_NAME);
        list.add(codegen.varDecl(nodeType, objectVar));

        // If this table has no primary key, then we ALWAYS create a new list element for each row, so we only
        // use the PKSC if there's a primary key to compare.
        // pksc.comparePrimaryKeys(rs);
        // if this node references primary keys:
        // boolean readFlag = readFlag || pksc.is<primary-key>Changed...
        Collection<ColumnInfo> referencedPrimaryKeys = node.getKeys();
        if (!referencedPrimaryKeys.isEmpty()) {
            MethodCall comparePksc = codegen.methodCall(codegen.var(Constants.Variables.PKSC),
                                                        Constants.Functions.COMPARE_PRIMARY_KEYS, rsVar);
            list.add(codegen.methodCallStmt(comparePksc));

            // boolean readFlag = readFlag || pksc.is<primary-key>Changed...
            list.add(codegen.assignStmt(codegen.var(Constants.Variables.READ_FLAG),
                                         primaryKeyConditionExpr(codegen, referencedPrimaryKeys)));
        }

        // if (readFlag) {
        List<CodeStatement> ifStatementList = new ArrayList<CodeStatement>();

        // if any of the primary keys in the root object have changed, create a new object and add it to the
        // end of the list, otherwise, just reference the last object in the list.
        // NOTE: Boxed primitives cannot be allocated before reading.
        // o = new <object-name>();
        ifStatementList.add(codegen.assignStmt(objectVar, codegen.alloc(nodeType)));

        // <list-name>.add(o);
        ifStatementList.add(codegen.methodCallStmt(codegen.var(listName), Constants.Functions.ADD, objectVar));
        List<CodeStatement> elseStatementList = new ArrayList<CodeStatement>();

        // o = <list-name>.get(<list-name>.size() - 1);
        elseStatementList.add(codegen.assignStmt(codegen.var(Constants.Variables.OBJECT_NAME),
                                                 lastListElement(codegen, listName)));
        list.add(codegen.ifStmt(codegen.var(Constants.Variables.READ_FLAG),
                                codegen.stmtList(ifStatementList), codegen.stmtList(elseStatementList)));
       // <object-name>SQLFns.readFromResultSet(rs, o, readFlag, pksc);
        list.add(codegen.methodCallStmt(readFromResultSetCall(codegen, node.getClassName(), objectVar, Constants.Variables.READ_FLAG)));
        return list;
    }

    private static TypedAssignmentStatement allocatePrimaryKeySourceColumns(CodeGen codegen) {
        // PrimaryKeySourceColumns pksc = new PrimaryKeySourceColumns();
        Allocation allocation = codegen.alloc(codegen.classType(Constants.JavaTypes.PRIMARY_KEY_SOURCE_COLUMNS));
        return codegen.typedAssignStmt(Constants.JavaTypes.PRIMARY_KEY_SOURCE_COLUMNS,
                                       Constants.Variables.PKSC, allocation);
    }

    /**
     * When reading the resultSet, if the primary key values for tables containing fields referenced by this object
     * (a "level" in our JSON tree) change, then we start a new node for this object and read it from the result set.
     *      pksc.is<Var>Changed() [ || ...]
     * @param readFlagVarName first for read-list, read-flag for child recursions (NOTE: move this to parent)
     * @param referencedPrimaryKeys primary keys reference by this object/JSON tree level.
     * @return boolean expression containing primary key references.
     */
    private static CodeExpression primaryKeyConditionExpr(CodeGen codegen,
                                                          String readFlagVarName,
                                                          Collection<ColumnInfo> referencedPrimaryKeys) {
        return codegen.binaryExpr(Constants.JavaOperators.OR,
                                  codegen.var(readFlagVarName), primaryKeyConditionExpr(codegen, referencedPrimaryKeys));
    }

    /**
     * Generate:
     * {@code <object-class-name>SQLFns.readFromResultSet(rs, o, readFlag, pksc); }
     * @param objectClassName object Class name (not variable name)
     * @return
     */
    private static MethodCall readFromResultSetCall(CodeGen codegen,
                                                    String objectClassName,
                                                    CodeExpression objectReference,
                                                    String childReadFlagName) {
        return codegen.methodCall(codegen.var(objectClassName +  Constants.Suffixes.SQL_FNS),
                                  Constants.Functions.READ_FROM_RESULT_SET,
                                  codegen.var(Constants.Variables.RS),
                                  objectReference,
                                  codegen.var(childReadFlagName),
                                  codegen.var(Constants.Variables.PKSC));
    }

    /**
     * generate:
     *      pksc.is<Var>Changed() [ || ...]
     * @param referencedPrimaryKeys primary keys reference by this object/JSON tree level.
     * @return boolean expression containing primary key references.
     */
    private static CodeExpression primaryKeyConditionExpr(CodeGen codegen, Collection<ColumnInfo> referencedPrimaryKeys) {
        if (referencedPrimaryKeys.isEmpty()) {
            return codegen.booleanConst(true);
        } else {
            Variable pkscVar = codegen.var(Constants.Variables.PKSC);
            CodeExpression returnedExpression = null;
            for (ColumnInfo primaryKey : referencedPrimaryKeys) {
                String changedFieldName = primaryKey.getColumnName(true) + Constants.Suffixes.CHANGED;
                MethodCall pkscChangedCall = codegen.methodCall(pkscVar, CodegenUtil.accessorNameFromField(changedFieldName));
                if (returnedExpression == null) {
                    returnedExpression = pkscChangedCall;
                } else {
                    returnedExpression = codegen.binaryExpr(Constants.JavaOperators.OR, returnedExpression, pkscChangedCall);
                }
            }
            return returnedExpression;
        }
    }

    /**
     * <listVarName>.get(<listVarName>.size() - 1)
     * @param listVarName
     * @return last list element call.
     */
    private static MethodCall lastListElement(CodeGen codegen, String listVarName) {
        MethodCall sizeCall = codegen.methodCall(codegen.var(listVarName), Constants.Functions.SIZE);
        CodeExpression lastElementIndexExpr =
            codegen.binaryExpr(Constants.JavaOperators.MINUS, sizeCall, codegen.integerConst(1));
        return codegen.methodCall(codegen.var(listVarName), Constants.Functions.GET, lastElementIndexExpr);
    }

    /**
     * In readFromResultSet(), with a field reference, generate the code:
     * {@code }<javaType><varName>Temp = rs.get<javaType>(<sqlVarName>);
     * if (!rs.wasNull()) {
     *   <objectName>.set<varName>(<varName>Temp);
     * }
     * }
     * @param objectName "o" (parameter from readFromResultSet)
     * @param columnInfo column info for variable name and type.
     * @return list of code statements described above.
     * @throws Exception
     */
    private static List<CodeStatement> applySetterToResultSet(CodeGen codegen,
                                                              String objectName,
                                                              ColumnInfo columnInfo) throws TypeNotFoundException {

        // need to get the alias of the column info...
        String varName = columnInfo.getColumnName(true);
        String tempVarName = varName + Constants.Variables.TEMP;
        List<CodeStatement> statementList = new ArrayList<CodeStatement>();

        // assign it to a temporary variable, and only call the setter if the value wasn't null.
        statementList.add(assignVarFromResultSet(codegen, columnInfo, tempVarName));
        MethodCall setValue = codegen.methodCall(codegen.var(objectName),
                                                 CodegenUtil.setterNameFromField(varName),
                                                 codegen.var(tempVarName));
        NotExpression notNullExpr = codegen.notExpr(codegen.methodCall(codegen.var(Constants.Variables.RS),
                                                    Constants.Functions.WAS_NULL));
        statementList.add(codegen.ifStmt(notNullExpr, codegen.methodCallStmt(setValue)));
        return statementList;
    }

    /**
     * {@code <javaType> <varName> = rs.get<javaType>(<columnName>); }
     * @param columnInfo
     * @param varName
     * @return
     * @throws TypeNotFoundException
     */
    private static CodeStatement assignVarFromResultSet(CodeGen codegen, ColumnInfo columnInfo, String varName)
    throws TypeNotFoundException {
        Type codeType = columnInfo.getCodeType(codegen, !columnInfo.isNotNull());
        return codegen.typedAssignStmt(codeType, codegen.var(varName), resultSetGetValueCall(codegen, columnInfo));
    }

    // NOTE: this should take rsVar to be parallel construction.
    private static MethodCall resultSetGetValueCall(CodeGen codegen, ColumnInfo columnInfo)
    throws TypeNotFoundException {
        String columnName = columnInfo.getColumnName(true);
        String sqlTypeName = columnInfo.getColumnSQLTypeString();
        return SQLType.resultSetCallFromSqlType(codegen, Constants.Variables.RS, sqlTypeName, columnName);
    }

    /**
     * Generate the selectList method:
     * public static List<<type>> selectList(Connection conn,(<param-type> <param>),...) throws SQLException {
     *     NamedParamStatement nps = new NamedParamStatement(conn, "<SQL>");
     *     nps.set<param-type>("<param>",param); ...
     *     ResultSet rs = nps.executeQuery();
     *     return <objectName>.readListFromResultSet(rs);
     * }
     * @param select select statement
     * @param objectType element type returned by select call.
     * @param sqlFnsClassName name of SQLFns class.
     * @param paramMap parameter map.
     * @return select list function definition.
     * @throws Exception
     */
    private static FunctionDefinition selectList(CodeGen codegen,
                                                 Select select,
                                                 Type objectType,
                                                 String sqlFnsClassName,
                                                 ParameterMap paramMap) throws Exception {

        // public static List<<type>> selectList(Connection conn,(<param-type> <param>),...) throws SQLException
        GenericType listType = codegen.listType(objectType);
        ParameterList argList = selectListParameterList(codegen, paramMap);
        FunctionDeclaration functionDecl =
            codegen.functionDecl(true, Constants.Functions.SELECT_LIST, argList,
                                 listType, codegen.typeList(Constants.JavaExceptions.SQL_EXCEPTION));
        List<CodeStatement> statementList = new ArrayList<CodeStatement>();

        // nps.set<param-type>("<param>",param);
        Allocation namedParamStmt = codegen.alloc(codegen.classType(Constants.JavaTypes.NAMED_PARAM_STATEMENT),
                                                  codegen.argList(codegen.var(Constants.Variables.CONN),
                                                  codegen.stringConst(select.toString())));
        TypedAssignmentStatement createNamedParamStmt =
            codegen.typedAssignStmt(Constants.JavaTypes.NAMED_PARAM_STATEMENT, Constants.Variables.NPS, namedParamStmt);
        statementList.add(createNamedParamStmt);

        // nps.set<param-type>("<param>",param);...
        for (String namedParam : paramMap.getParamSet()) {
            statementList.add(codegen.methodCallStmt(namedParamToNpsSetMethod(codegen, paramMap, namedParam)));
        }

        // ResultSet rs = nps.executeQuery();
        statementList.add(codegen.typedAssignStmt(codegen.classType(Constants.JavaTypes.RESULT_SET),
                          codegen.var(Constants.Variables.RS),
                          codegen.methodCall(codegen.var(Constants.Variables.NPS), Constants.Functions.EXECUTE_QUERY)));

        // return <objectName>.readListFromResultSet(rs);
        statementList.add(codegen.returnStmt(codegen.methodCall(codegen.var(sqlFnsClassName),
                                                                Constants.Functions.READ_LIST_FROM_RESULT_SET,
                                                                codegen.var(Constants.Variables.RS))));
        return codegen.functionDef(functionDecl, codegen.stmtList(statementList));
    }

    /**
     * Create the argument list for the SelectList call. The first argument is the database connection,
     * the rest of the arguments are generated from the named parameter set.
     * @param paramMap parameter map for this SELECT statement.
     * @return parameter list for select call: either scalar values for Named Parameters, or Lists of Boxed Primitives
     * for IN clause parameters.
     * @throws NameNotFoundException the named parameter was not found when resolving the type
     * @throws TypeNotFoundException the type of the named parameter could not be resolved.
     */
    private static ParameterList selectListParameterList(CodeGen codegen, ParameterMap paramMap)
    throws NameNotFoundException, TypeNotFoundException {
        List<ParameterDeclaration> argList = new ArrayList<ParameterDeclaration>(paramMap.getParamSet().size() + 1);
        argList.add(codegen.paramDecl(codegen.classType(Constants.JavaTypes.CONNECTION), Constants.Variables.CONN));
        for (String namedParam : paramMap.getParamSet()) {
            boolean isJsonRequestParam = paramMap.getParamSource(namedParam).equals(ParameterMap.ParamSource.REQUEST_JSON);
            Type type = isJsonRequestParam ? codegen.listType(paramMap.getBoxedJavaType(codegen, namedParam)) :
                                             paramMap.getJavaType(codegen, namedParam);
            argList.add(codegen.paramDecl(type, codegen.var(namedParam)));
        }
        return codegen.paramList(argList);
    }

    /**
     * Generate the NamedParameterStatement.set() call
     * {@code nps.set<Type>("namedParam", namedParam)}
     * @param paramMap
     * @param namedParam
     * @return
     * @throws NameNotFoundException
     * @throws TypeNotFoundException
     */
    public static MethodCall namedParamToNpsSetMethod(CodeGen codegen, ParameterMap paramMap, String namedParam)
    throws NameNotFoundException, TypeNotFoundException {
        String sqlTypeName = paramMap.getColDataType(namedParam).getDataType();
        boolean isList = paramMap.isInClause(namedParam) &&
                         paramMap.getParamSource(namedParam).equals(ParameterMap.ParamSource.REQUEST_JSON);
        return SQLType.setNamedParamFromSqlType(codegen, sqlTypeName, isList, Constants.Variables.NPS, namedParam, namedParam);
    }

    public CreateObjects getCreateObjects() {
        return createObjects;
    }

    public Collection<ColumnInfo> getPrimaryKeyList() {
        return primaryKeyList;
    }
}
