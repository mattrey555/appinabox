package com.apptechnik.appinabox.engine.codegen.test;

import com.apptechnik.appinabox.engine.util.ExtraProperties;
import com.apptechnik.appinabox.engine.util.StringUtil;
import com.apptechnik.appinabox.engine.util.FileUtil;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class CreateTestSelectJavaObjects {

    public static void main(String[] args) throws Exception {
        String packageName = args[0];
        String scriptFileName = args[1];
        String createTableFileName = args[2];
        String propertiesFileName = args[3];
        String outputDirName = args[4];

        File scriptFile = FileUtil.checkFile("script file", scriptFileName);
        String fullPackageName = packageName + "." + StringUtil.removeExtension(scriptFile.getName());
        File createTableFile = FileUtil.checkFile("SQL create table", createTableFileName);
        File outputDir = FileUtil.checkDirectory("Output Directory", outputDirName);
        File propertiesFile = FileUtil.checkFile("Properties File", propertiesFileName);
        File javaDir = StringUtil.directoryFromPackage(outputDir, fullPackageName);
        FileUtil.checkDirectory("output directory", javaDir);
    }
}
