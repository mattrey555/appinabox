package com.apptechnik.appinabox.engine.codegen.test;

import com.apptechnik.appinabox.engine.exceptions.TypeNotFoundException;
import com.apptechnik.appinabox.engine.exceptions.UnsupportedTypeException;
import com.apptechnik.appinabox.engine.parser.*;
import com.apptechnik.appinabox.engine.parser.types.SQLType;
import com.apptechnik.appinabox.engine.parser.create.StatementParser;
import com.apptechnik.appinabox.engine.util.ExtraProperties;
import com.apptechnik.appinabox.library.sql.NamedParamStatement;
import com.apptechnik.appinabox.library.sql.SelectUtils;
import com.apptechnik.appinabox.engine.util.FileUtil;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.JdbcNamedParameter;
import net.sf.jsqlparser.expression.operators.relational.ExpressionList;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.insert.Insert;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.sql.Date;
import java.util.*;

public class PopulateTables {
    public static final String PROPERTY_USER = "user";
    public static final String PROPERTY_PASSWORD = "password";
    public static final String PROPERTY_SSL = "ssl";

    public static void main(String[] args) throws Exception {
        System.out.println("create tables: " + args[0] +
                           "\ntable sizes: " + args[1] +
                            "\nDB Url: " + args[2] +
                            "\ndatabase: " + args[3] +
                            "\nproperties file: " + args[4]);

        File createTableFile = FileUtil.checkFile("create SQL tables", args[0]);
        File tableSizeFile = FileUtil.checkFile("table sizes file", args[1]);
        String dbUrl = args[2];
        String database = args[3];
        String propertiesFileName = args[4];
        ExtraProperties properties =  new ExtraProperties(new File(propertiesFileName));
        System.out.println("properties: " + properties.toString());
        String user = properties.getProperty("user");
        String password = properties.getProperty("password");
        Connection conn = DriverManager.getConnection(dbUrl + "/" + database, user, password);
        List<TableSize> tableSizeList = TableSize.parseTableSizes(tableSizeFile);
        String createTablesSQL = FileUtil.readFileToString(createTableFile);
        StatementParser statementParser = new StatementParser();
        statementParser.parse(createTablesSQL);
        TableInfoMap tableInfoMap = statementParser.getTableInfoMap();
        tableInfoMap.resolveAllChildReferences();
        populateTables(conn, tableInfoMap, tableSizeList);
    }

    public static void populateTables(Connection conn, TableInfoMap tableInfoMap, List<TableSize> tableSizeList) throws Exception {
        System.out.println("Table Map:\n" + tableInfoMap);
        List<TableInfo> tableInfoList = new LinkedList<TableInfo>(tableInfoMap.getValues());
        List<TableInfo> orderedTableInfoList = new ArrayList<TableInfo>(tableSizeList.size());
        while (!tableSizeList.isEmpty()) {
            TableInfo removeTableInfo = null;
            for (TableInfo tableInfo : tableInfoList) {
                if (!hasParent(tableInfo, tableInfoList)) {
                    TableSize tableSize = TableSize.getTableSize(tableSizeList, tableInfo.getTable().getName());
                    if (tableSize != null) {
                        orderedTableInfoList.add(tableInfo);
                    }
                    removeTableInfo = tableInfo;
                    break;
                }
            }
            if (removeTableInfo != null) {
                tableInfoList.remove(removeTableInfo);
            } else {
                break;
            }
        }
        for (int i = orderedTableInfoList.size() - 1; i >= 0; i--) {
            TableInfo tableInfo = orderedTableInfoList.get(i);
            System.out.println("deleting table " + tableInfo.getTable().getName());
            clearTable(conn, tableInfo);
        }
        for (int i = 0; i < orderedTableInfoList.size(); i++) {
            TableInfo tableInfo = orderedTableInfoList.get(i);
            System.out.println("inserting table " + tableInfo.getTable().getName());
            String insertSQL = createInsert(tableInfo);
            System.out.println("insertSQL: " + insertSQL);
            TableSize tableSize = TableSize.getTableSize(tableSizeList, tableInfo.getTable().getName());
            insertValues(conn, tableInfo, tableSize);

        }
    }

    private static boolean hasParent(TableInfo tableInfo, List<TableInfo> tableInfoList) {
        for (TableInfo candParentTableInfo : tableInfoList) {
            if (candParentTableInfo.inChildReferenceList(tableInfo.getTable())) {
                return true;
            }
        }
        return false;
    }

    private static void clearTable(Connection conn, TableInfo tableInfo) throws SQLException {
        conn.prepareCall("DELETE from " + tableInfo.getTable().getName()).execute();
    }

    private static void insertValues(Connection conn, TableInfo tableInfo, TableSize tableSize) throws Exception {
        NamedParamStatement nps = new NamedParamStatement(conn, createInsert(tableInfo));
        Map<Column, List<Integer>> foreignKeyValueListMap = new HashMap<Column, List<Integer>>();
        for (ColumnInfo columnInfo : tableInfo.getColumnInfoSet()) {
            Column foreignColumn = columnInfo.getForeignKey();
            if (foreignColumn != null) {
                List<Integer> intList = SelectUtils.selectIntList(conn, foreignColumn.getTable().getName(), foreignColumn.getColumnName());
                foreignKeyValueListMap.put(foreignColumn, intList);
            }
        }
        for (int row = 0; row < tableSize.size; row++) {
            populateInsert(foreignKeyValueListMap, tableInfo, nps, row).execute();
        }
    }

    private static String createInsert(TableInfo tableInfo) {
        Insert insert = new Insert();
        insert.setTable(tableInfo.getTable());
        List<Expression> expressions = new ArrayList<Expression>();
        List<Column> columnList = new ArrayList<Column>();
        for (ColumnInfo columnInfo : tableInfo.getColumnInfoSet()) {
            if (!columnInfo.isAutogenerate()) {
                Column col = new Column(columnInfo.getColumnName());
                columnList.add(col);
                System.out.println("column: " + col.getColumnName());
                JdbcNamedParameter namedParam = new JdbcNamedParameter(col.getColumnName());
                expressions.add(namedParam);
            }
        }
        insert.setColumns(columnList);
        ExpressionList expressionList = new ExpressionList();
        expressionList.setExpressions(expressions);
        insert.setItemsList(expressionList);
        return insert.toString();
    }

    private static NamedParamStatement populateInsert(Map<Column, List<Integer>> foreignKeyValueListMap,
                                                      TableInfo tableInfo,
                                                      NamedParamStatement insertStatement,
                                                      int index)
    throws TypeNotFoundException, UnsupportedTypeException, SQLException {
        for (ColumnInfo columnInfo : tableInfo.getColumnInfoSet()) {
            if (!columnInfo.isAutogenerate()) {
                String colName = columnInfo.getColumnName();
                String dataType = columnInfo.getDataType();
                int dataTypeEnum = SQLType.getJavaSqlTypeFromString(dataType);
                Column foreignColumn = columnInfo.getForeignKey();
                if (foreignColumn != null) {
                    List<Integer> valueList = foreignKeyValueListMap.get(foreignColumn);
                    int value = valueList.get(index % valueList.size());
                    insertStatement.setInt(colName, value);
                    if (dataTypeEnum == Types.INTEGER) {
                        insertStatement.setInt(colName, index);
                    } else if (dataTypeEnum == Types.SMALLINT) {
                        insertStatement.setShort(colName, (short) index);
                    } else if (dataTypeEnum == Types.TINYINT) {
                        insertStatement.setShort(colName, (short) index);
                    } else if (dataTypeEnum == Types.BIGINT) {
                        insertStatement.setLong(colName, (long) index);
                    } else {
                        throw new UnsupportedTypeException("in column: " + colName + " dataType: " + dataTypeEnum + " is not supported");
                    }
                } else {
                    if (dataTypeEnum == Types.INTEGER) {
                        insertStatement.setInt(colName, index);
                    } else if (dataTypeEnum == Types.SMALLINT) {
                        insertStatement.setShort(colName, (short) index);
                    } else if (dataTypeEnum == Types.TINYINT) {
                        insertStatement.setShort(colName, (short) index);
                    } else if (dataTypeEnum == Types.BIGINT) {
                        insertStatement.setLong(colName, (long) index);
                    } else if (dataTypeEnum == Types.DECIMAL) {
                        insertStatement.setFloat(colName, index);
                    } else if (dataTypeEnum == Types.VARCHAR) {
                        insertStatement.setString(colName, colName + index);
                    } else if ((dataTypeEnum == Types.DATE) || (dataTypeEnum == Types.TIMESTAMP)) {
                        Date date = new Date(System.currentTimeMillis() + index * 1000 * 60);
                        insertStatement.setDate(colName, date);
                    } else {
                        throw new UnsupportedTypeException("in column: " + colName + " dataType: " + dataTypeEnum + " is not supported");
                    }
                }
            }
        }
        return insertStatement;
    }

    private static class TableSize {
        public final String tableName;
        public final int size;

        public TableSize(String tableName, int size) {
            this.tableName = tableName;
            this.size = size;
        }

        public static List<TableSize> parseTableSizes(File file) throws IOException {
            List<TableSize> tableSizeList = new ArrayList<TableSize>();
            BufferedReader br = new BufferedReader(new FileReader(file));
            do {
                String line = br.readLine();
                if (line != null) {
                    String[] values = line.split(":");
                    TableSize tableSize = new TableSize(values[0], Integer.parseInt(values[1].trim()));
                    tableSizeList.add(tableSize);
                } else {
                    break;
                }
            } while (true);
            return tableSizeList;
        }

        public static TableSize getTableSize(List<TableSize> tableSizes, String tableName) {
            for (TableSize tableSize : tableSizes) {
                if (tableSize.tableName.equals(tableName)) {
                    return tableSize;
                }
            }
            return null;
        }
    }
}
