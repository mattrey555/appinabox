package com.apptechnik.appinabox.engine.codegen.test;


import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


/**
 * Grab-bag of miscellaneous test utilities.
 */

public class TestUtil {
    private static final String TAG = TestUtil.class.getSimpleName();

    private TestUtil() {
    }
    /*
     * @param a
     * @param b
     * @return
     * @throws Exception
     */
    public static List<String> diff(Object a, Object b) throws Exception {
        List<String> misses = new ArrayList<String>();
        diff(a.getClass().getSimpleName(), a, b, misses);
        return misses;
    }

    /**
     * Diff two objects. Current is the current expression, diff a and b.  Misses is the list
     * of expressions referring to where the objects are different.
     *
     * @param current
     * @param a
     * @param b
     * @param misses
     * @throws Exception
     */

    public static void diff(String current, Object a, Object b, List<String> misses) throws Exception {

        // only compare if they're not null.
        if ((a != null) && (b != null)) {
            String ref = current + "." + a.getClass().getSimpleName();

            // test if the classes are the same
            if (a.getClass() != b.getClass()) {

                String miss = String.format("%s != %s.%s", ref, current, b.getClass().getSimpleName());
                misses.add(miss);
            }

            // quick test if they're both strings.
            if ((a instanceof String) && (b instanceof String)) {
                if (!((String) a).equals((String) b)) {
                    String miss = String.format("%s (%s) != (%s)", ref, (String) a, (String) b);
                    misses.add(miss);
                }
            } else {


                // iterate over the non-static fields for each class.
                for (Field f :  a.getClass().getDeclaredFields()) {
                    f.setAccessible(true);

                    // compare scalars case.
                    if (isScalar(f)) {
                        if (!comparePrimitive(a, b, f)) {
                            String miss = String.format("%s.%s", ref, f.getName());
                            misses.add(miss);
                        }

                        // compare the existence, size, and elements in each list.
                    } else if (List.class.isAssignableFrom(f.getType())) {
                        List<Object> listA = (List<Object>) f.get(a);
                        List<Object> listB = (List<Object>) f.get(b);
                        if ((listA != null) && (listB != null)) {
                            if (listA.size() != listB.size()) {
                                String miss = String.format(Locale.US, "%s.%s size %d != %d", ref, f.getName(), listA.size(), listB.size());
                                misses.add(miss);
                            } else {
                                for (int i = 0; i < listA.size(); i++) {
                                    Object elementA = listA.get(i);
                                    Object elementB = listB.get(i);

                                    diff(String.format(Locale.US, "%s.%s[%d]", current, f.getName(), i), elementA, elementB, misses);
                                }
                            }
                        } else if ((listA == null) != (listB == null)) {
                            String miss = String.format("%s.%s list null mismatch", ref, f.getName());
                            misses.add(miss);
                        }
                    } else {
                        if (!((Class) f.getType()).isSynthetic()) {
                            diff(current + "." + f.getName(), f.get(a), f.get(b), misses);
                        }
                    }
                }
            }

            // fail if either is null but not both.
        } else if ((b == null) && (a != null)) {
            String ref = current + "." + a.getClass().getSimpleName();
            String miss = String.format("%s == null", ref);
            misses.add(miss);
        } else if ((a == null) && (b != null)) {
            String ref = current + "." + b.getClass().getSimpleName();
            String miss = String.format("%s == null", ref);
            misses.add(miss);
        }
    }


    // compare the field from two objects which refers to a primitive simpleType.
    public static boolean comparePrimitive(Object a, Object b, Field f) throws Exception {
        Class c = f.getType();
        if (c == boolean.class) {
            return f.getBoolean(a) == f.getBoolean(b);
        } else if (c == int.class) {
            return f.getInt(a) == f.getInt(b);
        } else if (c == long.class) {
            return f.getLong(a) == f.getLong(b);
        } else if (c == short.class) {
            return f.getShort(a) == f.getShort(b);
        } else if (c == String.class) {
            String sa = (String) f.get(a);
            String sb = (String) f.get(b);
            if ((sa == null) && (sb == null)) {
                return true;
            }
            if ((sa == null) || (sb == null)) {
                return false;
            }
            boolean test = sa.equals(sb);
            return test;
        } else if (c == double.class) {
            return (Double.isNaN(f.getDouble(a)) && Double.isNaN(f.getDouble(b))) || (f.getDouble(a) == f.getDouble(b));
        } else if (c == float.class) {
            return (Float.isNaN(f.getFloat(a)) && Float.isNaN(f.getFloat(b))) || (f.getFloat(a) == f.getFloat(b));
        } else if (c == Date.class) {
            Date aDate = (Date) f.get(a);
            Date bDate = (Date) f.get(b);
            if ((aDate != null) && (bDate != null)) {
                return ((Date) f.get(a)).equals((Date) f.get(b));
            } else {
                return (aDate == null) == (bDate == null);
            }
        } else if (c == byte[].class) {
            byte[] abyteArray = (byte[]) f.get(a);
            byte[] bbyteArray = (byte[]) f.get(b);
            if ((abyteArray != null) && (bbyteArray != null)) {
                if (abyteArray.length == bbyteArray.length) {
                    for (int i = 0; i < abyteArray.length; i++) {
                        if (abyteArray[i] != bbyteArray[i]) {
                            return false;
                        }
                    }
                    return true;
                }
            }
            return (a == null) == (b == null);
        } else {
            return false;
        }
    }


    /**
     * Checks if a class is of Boxed primitive like Integer or Boolean
     *
     * @param c class
     * @return
     */
    private static boolean isBoxedPrimitive(Class c) {
        if (c == Boolean.class || c == Integer.class || c == Long.class || c == Short.class || c == Double.class || c == Float.class) {
            return true;
        }
        return false;
    }

    /**
     * Test if a field is scalar (i.e. not an object or a list)
     * @param field field to test.
     * @return true if the field is a scalar class.
     */
    public static boolean isScalar(Field field) {
        Type t = field.getType();
        return t.equals(boolean.class) || t.equals(short.class)|| t.equals(int.class) || t.equals(long.class) ||
                t.equals(double.class) || t.equals(float.class) || t.equals(String.class) || t.equals(Date.class) ||
                t.equals(byte[].class) || t.equals(char[].class) || t.equals(int[].class) ||
                t.equals(long[].class) || t.equals(short[].class);
    }

    public static Class getListElementType(Field field) {
        ParameterizedType genericType = (ParameterizedType) field.getGenericType();
        return (Class) genericType.getActualTypeArguments()[0];
    }


    /**
     * Randomly returns a boxed primitive value for given class
     *
     * @param c class
     * @return
     */
    private static Object randomBoxedPrimitiveValue(Class c) {
        Object o = null;
        if (c == Boolean.class) {
            o = Math.random() > 0.5;
        } else if (c == Integer.class) {
            o = Integer.valueOf((int) (Math.random() * 100));
        } else if (c == Long.class) {
            o = Long.valueOf((long) (Math.random() * 1000));
        } else if (c == Short.class) {
            o = Short.valueOf((short) (Math.random() * 100));
        } else if (c == Double.class) {
            o = Math.random() * 100.0;
        } else if (c == Float.class) {
            o = Float.valueOf((float) (Math.random() * 100.0));
        }else if (c == Double.class) {
            o = Double.valueOf(Math.random() * 100.0);
        }
        return o;
    }


    /**
     * Randomly populate an object.
     * @param a       object.
     * @throws Exception reflection error.
     */
    private static void randomlyPopulate(Object a) throws Exception {
        for (Field f : a.getClass().getDeclaredFields()) {
            f.setAccessible(true);

            // compare scalars case.
            if (isScalar(f)) {
                randomlyAssignScalar(a, f);
            } else if (List.class.isAssignableFrom(f.getType())) {

                boolean isNull = Math.random() > 0.1;
                if (isNull) {
                    f.set(a, null);
                } else {
                    int length = (int) (Math.random() * 10);
                    Class clsChild = getListElementType(f);
                    List<Object> list = new ArrayList<>(length);
                    for (int i = 0; i < length; i++) {
                        Object child;
                        if (isBoxedPrimitive(clsChild)) {
                            child = randomBoxedPrimitiveValue(clsChild);
                        } else {
                            child = clsChild.newInstance();
                            randomlyPopulate(child);
                        }
                        list.add(child);
                    }
                    f.set(a, list);
                }
            } else if (Object.class.isAssignableFrom(f.getType())) {
                Class clsChild = (Class) f.getType();
                Object child = clsChild.newInstance();
                f.set(a, child);
            } else {
                throw new Exception(f.getName() + " is an unsupported field simpleType in " + a.getClass().getName());
            }
        }
    }

    /**
     * Randomly assign a scalar value.
     *
     * @param a object.
     * @param f field.
     * @throws IllegalAccessException reflection error.
     */
    private static void randomlyAssignScalar(Object a, Field f) throws IllegalAccessException {
        Class c = f.getType();
        if (c == boolean.class) {
            f.setBoolean(a, Math.random() > 0.5);
        } else if (c == int.class) {
            f.setInt(a, (int) (Math.random() * 100));
        } else if (c == long.class) {
            f.setLong(a, (long) (Math.random() * 100));
        } else if (c == short.class) {
            f.setShort(a, (short) (Math.random() * 100));
        } else if (c == String.class) {
            int length = (int) (Math.random() * 100);
            StringBuffer sb = new StringBuffer(length);
            for (int i = 0; i < length; i++) {
                char ch = (char) ('a' + (char) (Math.random() * 26));
                sb.append(ch);
            }
            f.set(a, sb.toString());
        } else if (c == double.class) {
            f.setDouble(a, Math.random() * 100.0);
        } else if (c == float.class) {
            f.setFloat(a, (float) (Math.random() * 100.0));
        } else if (c == Date.class) {
            long time = System.currentTimeMillis();
            Date date = new Date(time);
            f.set(a, date);
        } else if (c == byte[].class) {
            int length = (int) (Math.random() * 100);
            byte[] byteArray = new byte[length];
            for (int i = 0; i < length; i++) {
                byteArray[i] = (byte) (Math.random() * 100);
            }
            f.set(a, byteArray);
        }
    }
}