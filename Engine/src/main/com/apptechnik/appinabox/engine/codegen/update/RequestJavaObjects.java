package com.apptechnik.appinabox.engine.codegen.update;

import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeGen;
import com.apptechnik.appinabox.engine.codegen.sql.ChangeSQLObjects;
import com.apptechnik.appinabox.engine.parser.pojos.*;

import java.util.*;

/**
 * Multiple INSERT, UPDATE, DELETE, and SELECT INTO statements can be combined, mapped from incoming JSON data in depth-first order.
 * A single SQL statement can access JSON values from the current subnode to the root node.  Multiple SQL statements can be
 * mapped to a single subnode.  The table maps to the JSON name of the current node. The top-level SQL statement
 * may apply to a subnode of the incoming JSON.  Not all incoming JSON tags must map to named parameters, but
 * named parameters must either map to a JSON tag, or the result of a previous SQL statement RETURNS clause.
 * The JSON values map to named parameters in the SQL statements.
 */
public class RequestJavaObjects {
    private ChangeSQLObjects changeSQLObjects;
    private RequestObjects requestObjects;

    // TODO: don't need to duplicate parameterMap
    public RequestJavaObjects(CreateObjects createObjects) {
        changeSQLObjects = new ChangeSQLObjects(createObjects);
        requestObjects = new RequestObjects(createObjects);
    }

    /**
     * From a list of inserts and CREATE TABLE statements, write a set of java objects to a file
     * which represents the hierarchy which would be read from JSON, then populated into the database
     * The tables referenced by the INSERT calls have to have a foreign key relationship which
     * defines the parent-child relationship.  Each parent contains a list of their child objects
     * from the insertions.
     * @param jsonTree JSON map tree
     * @throws Exception could be any number of problems, such as unsupported expressions, unreferenced variables,
     * unsuported types or ambiguous references.
     */
    public void createObjects(CodeGen codegen,
                              Tree<MergedFieldNode> jsonTree) throws Exception {
        CreateJavaObjects createJavaObjects = new CreateJavaObjects(codegen);
        jsonTree.traverseTreeStack(createJavaObjects);
    }

    public void createSQLObjects(CodeGen codegen,
                                 Tree<MergedFieldNode> jsonTree,
                                 ParameterMap queryParams) throws Exception {
        CreateRequestSQLObjects createSQLObjects = new CreateRequestSQLObjects(codegen, queryParams);
        jsonTree.traverseTreeStack(createSQLObjects);
    }

    private class CreateRequestSQLObjects implements Tree.ConsumerException<Stack<Tree<MergedFieldNode>>> {
        private final CodeGen codegen;
        private final ParameterMap queryParams;

        public CreateRequestSQLObjects(CodeGen codegen,
                                       ParameterMap queryParams) {
            this.codegen = codegen;
            this.queryParams = queryParams;
        }

        @Override
        public void accept(Stack<Tree<MergedFieldNode>> stack) throws Exception {
            changeSQLObjects.createSQLObject(codegen, stack, queryParams)
                    .write(changeSQLObjects.getCreateObjects().getOutputDir());
        }
    }

    private class CreateJavaObjects implements Tree.ConsumerException<Stack<Tree<MergedFieldNode>>> {
        private final CodeGen codegen;

        public CreateJavaObjects(CodeGen codegen) {
            this.codegen = codegen;
        }

        @Override
        public void accept(Stack<Tree<MergedFieldNode>> stack) throws Exception {
            // for a single field list of a primitive type, we don't need to create an object
            // parse the JSON into, but we do need an insertUpdateList method to insert the list.
            if (!FieldReferenceNode.isSingleField(stack.peek())) {
                requestObjects.toClassFile(codegen, stack.peek(), stack.size() == 1)
                        .write(requestObjects.getCreateObjects().getOutputDir());
            }
        }
    }
}
