package com.apptechnik.appinabox.engine.codegen.update;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.exceptions.NameNotFoundException;
import com.apptechnik.appinabox.engine.exceptions.TypeNotFoundException;
import com.apptechnik.appinabox.engine.parser.ColumnInfo;
import com.apptechnik.appinabox.engine.parser.pojos.*;
import com.apptechnik.appinabox.engine.codegen.util.CodegenUtil;
import com.apptechnik.appinabox.engine.parser.util.JsonPathMatcher;
import com.apptechnik.appinabox.engine.util.CheckedFunction;
import com.apptechnik.appinabox.engine.util.OutputPackages;
import net.sf.jsqlparser.statement.Statement;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Multiple INSERT, UPDATE, DELETE, and SELECT INTO staements can be combined, mapped from incoming JSON data in depth-first order.
 * A single SQL statement can access JSON values from the current subnode to the root node.  Multiple SQL statements can be
 * mapped to a single subnode.  The table maps to the JSON name of the current node. The top-level SQL statement
 * may apply to a subnode of the incoming JSON.  Not all incoming JSON tags must map to named parameters, but
 * named parameters must either map to a JSON tag, or the result of a previous SQL statement RETURNS clause.
 * The JSON values map to named parameters in the SQL statements.
 */
public class RequestObjects {
    private final CreateObjects createObjects;

    public RequestObjects(CreateObjects createObjects) {
        this.createObjects = createObjects;
    }


    /**
     * Create call: open the file, insert the package and imports, then create the object.
     * @param tree current tree node.
     * @param isRoot is a root object (we generate the extraction functions in this case)
     * @throws Exception general failure creating objects.
     */
    public ClassFile toClassFile(CodeGen codegen, Tree<MergedFieldNode> tree, boolean isRoot) throws Exception {
        String packageName = OutputPackages.getUpdatePackageName(createObjects.getPackageName());
        ClassDefinition classDef = generateObject(codegen, tree, isRoot);
        return codegen.classFile(codegen.packageStmt(packageName),
                                 codegen.importStatementList(Constants.Templates.OBJECT_IMPORTS), classDef);
    }

    public ClassFile toKotlinClassFile(CodeGen codegen, Tree<MergedFieldNode> tree) throws Exception {
        String packageName =OutputPackages.getUpdatePackageName(createObjects.getPackageName());
        ClassDefinition classDef = generateObjectNoMethods(codegen, tree);
        return codegen.classFile(codegen.packageStmt(packageName),
                codegen.importStatementList(Constants.Templates.KOTLIN_OBJECT_IMPORTS), classDef);
    }

    /**
     * write the object
     * @param tree JSON tree node to generate from
     * @param isRoot top-level, create the extract functions.
     * @throws Exception
     */
    private ClassDefinition generateObject(CodeGen codegen,
                                           Tree<MergedFieldNode> tree,
                                           boolean isRoot)
    throws TypeNotFoundException, NameNotFoundException {
        MergedFieldNode node = tree.get();

        // annotation list has to be mutable, because we can add annotations
        ClassDeclaration classDecl =
                codegen.classDecl(false, false, true, node.getVarName(), null, null, null,
                                   Arrays.asList(codegen.classAnnot(Constants.Annotations.GENERATED)));
        // fields are the unique set of SQL named parameters between statements, minus the
        // set of named parameters defined by parent objects.
        // the SQL statements may have overlapping parameter names, and some of their parameters may come
        // from parent nodes.
        List<MemberDeclaration> members = fieldsFromStatementVariables(codegen, tree, Constants.JavaKeywords.PRIVATE);
        members.addAll(MergedFieldNode.fieldsFromChildren(codegen, tree, createObjects.getStatementParamMap(), Constants.JavaKeywords.PRIVATE));
        List<FunctionDefinition> methods = CodegenUtil.accessorsAndSetters(codegen, members);
        if (isRoot) {
            Collection<JsonPathMatcher.PathMatch<MergedFieldNode>> pathSet = createObjects.getJsonParameterMap().getParamMapSet();
            for (JsonPathMatcher.PathMatch<MergedFieldNode> pathMatch : pathSet) {
                methods.add(JsonPathMatcher.extractListFromPath(codegen, pathMatch));
            }
        }
        // request objects don't need constructors, since they're parsed from JSON
        return codegen.classDef(classDecl, members, null, methods);
    }

    /**
     * Used in Kotlin code generation, where setters and accessors aren't necessary.
     * @param tree JSON tree node to generate from
     * @throws Exception
     */
    public ClassDefinition generateObjectNoMethods(CodeGen codegen, Tree<MergedFieldNode> tree)
    throws TypeNotFoundException, NameNotFoundException {
        MergedFieldNode node = tree.get();

        // annotation list has to be mutable, since we can add annotations.
        ClassDeclaration classDecl =
                codegen.classDecl(false, false, false, node.getVarName(), null, null, null,
                                  new ArrayList<Annotation>(Arrays.asList(codegen.classAnnot(Constants.Annotations.GENERATED))));
        // Fields are the unique set of SQL named parameters between statements, minus the
        // set of named parameters defined by parent objects.
        // The SQL statements may have overlapping parameter names, and some of their parameters may come
        // from parent nodes.
        List<MemberDeclaration> members = fieldsFromStatementVariables(codegen, tree, Constants.KotlinKeywords.PUBLIC);
        members.addAll(MergedFieldNode.fieldsFromChildren(codegen, tree, createObjects.getStatementParamMap(), Constants.JavaKeywords.PUBLIC));
        List<ConstructorDefinition> constructorList = List.of(codegen.constructorDef(node.getVarName(), members),
                                                              codegen.defaultConstructor(node.getVarName(), members));
        // request objects don't need constructors, since they're parsed from JSON
        return codegen.classDef(classDecl, members, constructorList, null);
    }

    /**
     * Generate the fields from JDBCNamedParameters from the source statements (INSERT/UPDATE/DELETE, and
     * SELECT calls with IN clauses.
     * @param tree JSON mapping node.
     * @param accessModifier public for Kotlin, private for Java
     * @return list of member declarations.
     * @throws NameNotFoundException
     * @throws TypeNotFoundException
     */
    private  List<MemberDeclaration> fieldsFromStatementVariables(CodeGen codegen,
                                                                  Tree<MergedFieldNode> tree,
                                                                  String accessModifier)
    throws NameNotFoundException, TypeNotFoundException {
        MergedFieldNode node = tree.get();
        List<MemberDeclaration> declarations = new ArrayList<>();

        for (JsonParamReference param : node.getFieldReferenceList()) {
            boolean matched = false;
            for (Statement statement : node.getUpdateStatements()) {
                ParameterMap paramMap = createObjects.getStatementParamMap(statement);
                // NOTE: I'm not sure that we need to check the dot-syntax here
                String jdbcParam = paramMap.jdbcParamName(param.toString());
                if (!paramMap.hasParam(jdbcParam)) {
                    jdbcParam = paramMap.jdbcParamName(node.getVarName() + "." + param);
                }
                if (paramMap.hasParam(jdbcParam)) {
                    try {
                        String sqlType = paramMap.getColDataType(jdbcParam).getDataType();
                        boolean isNullable = paramMap.isNullable(jdbcParam);
                        MemberDeclaration decl = CodegenUtil.memberDeclarationFromSQLType(codegen, jdbcParam, sqlType,
                                                                                          !isNullable, accessModifier);
                        declarations.add(decl);
                        matched = true;
                    } catch (TypeNotFoundException tnfex) {
                        throw new TypeNotFoundException("while creating field from " + jdbcParam + " used in expression " +
                                paramMap.getExpression(jdbcParam) +
                                " please set an explicit type for " + jdbcParam, tnfex);
                    }
                }
            }
            if (!matched) {
                throw new NameNotFoundException("name not found while creating field from " + param);
            }
        }

        // add the INSERT/UPDATE RETURNING values, if any.
        declarations.addAll(node.getAllAdditionalColumns().stream()
                .map(CheckedFunction.wrap(c -> CodegenUtil.memberDeclarationFromColumn(codegen, c, Constants.JavaKeywords.PRIVATE)))
                .collect(Collectors.toList()));
        return declarations;
    }

    public CreateObjects getCreateObjects() {
        return createObjects;
    }
}
