package com.apptechnik.appinabox.engine.codegen.update;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeExpression;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeGen;
import com.apptechnik.appinabox.engine.codegen.pojos.MethodCall;
import com.apptechnik.appinabox.engine.codegen.pojos.Variable;
import com.apptechnik.appinabox.engine.codegen.sql.ChangeSQLObjects;
import com.apptechnik.appinabox.engine.codegen.util.CodegenUtil;
import com.apptechnik.appinabox.engine.exceptions.NameNotFoundException;
import com.apptechnik.appinabox.engine.exceptions.TypeNotFoundException;
import com.apptechnik.appinabox.engine.parser.*;
import com.apptechnik.appinabox.engine.parser.pojos.*;
import com.apptechnik.appinabox.engine.util.*;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;


import java.util.*;
import java.util.function.Consumer;

/**
 * Multiple INSERT, UPDATE, DELETE, and SELECT INTO staements can be combined, mapped from incoming JSON data in depth-first order.
 * A single SQL statement can access JSON values from the current subnode to the root node.  Multiple SQL statements can be
 * mapped to a single subnode.  The table maps to the JSON name of the current node. The top-level SQL statement
 * may apply to a subnode of the incoming JSON.  Not all incoming JSON tags must map to named parameters, but
 * named parameters must either map to a JSON tag, or the result of a previous SQL statement RETURNS clause.
 * The JSON values map to named parameters in the SQL statements.
 */
public class UpdateSQLObjects {
    private final CreateObjects createObjects;

    /**
     * named parameters can come from incoming JSON, but can also be explicitly specified, either from command-line
     * parameters, or from headers/path components/query parameters from the URL.
     * @param createObjects paramter grab-bag used to generate objects.
     */
    public UpdateSQLObjects(CreateObjects createObjects)  {
        this.createObjects = createObjects;
    }

    /**
     * Generate the map of statements to JDBC Named Parameters, apply the parameter types from the source comments,
     * verify that parameter types are set or inferred, create the objects and SQL Function classes, and the Main
     * class which takes a JSON input file and executes the SQL statements.
     * @throws Exception general failure creating the object files.
     */

    public void executeOnChanges(CodeGen codegen) throws Exception {

        // get the map of statements to parameter maps with types inferred from SQL statements,
        // then apply the fixed types, and apply the type from the parameters to the query parameters.
        List<Statement> updateList = createObjects.getUpdateStatementList();
        StatementParamMap statementParamMap = createObjects.getUpdateParameters();
        statementParamMap.checkParameterTypes();

        // if the SQL statement list only contains DELETE calls, then the request JSON may be unspecified.
        if (createObjects.getJsonRequestTree() != null) {
            RequestJavaObjects requestJavaObjects = new RequestJavaObjects(createObjects);
            requestJavaObjects.createObjects(codegen, createObjects.getJsonRequestTree());
            requestJavaObjects.createSQLObjects(codegen,  createObjects.getJsonRequestTree(), createObjects.getQueryParams());
        }

        // Change statements might not be assigned in JSON, such as DELETE statements, which may take query parameters.
        List<Statement> unassignedStatementList =
            MergedFieldNode.filterUnassignedStatements(createObjects.getJsonRequestTree(), updateList);
        if (!unassignedStatementList.isEmpty()) {
            // TODO: what's the sense of this parameter division between the constructor and arguments?
            ChangeSQLObjects changeSQLObjects = new ChangeSQLObjects(createObjects);
            changeSQLObjects.createSQLObject(codegen, unassignedStatementList, createObjects.getQueryParams())
                .write(createObjects.getOutputDir());
        }
    }

     /**
     * Create an MethodCall of the form:
     * {@code <Object>SQLFns.insertUpdateList(<dbconn>, <param>, <param>...) }
      * Used at the top-level update and child list updates.
      * @param dbConnectionName name of the SQL datbase connection
      * @param objectName Object type which contains the SQL_FNS (suffix added)
      * @param listName variable name of the
      * @param parameterMap for the JDBC Parameters to pass to the insertUpdateList call
      * @return MethodCall described aboce.
     */
    public static MethodCall insertUpdateListCall(CodeGen codegen,
                                                  String dbConnectionName,
                                                  String objectName,
                                                  String listName,
                                                  ParameterMap parameterMap) {
        List<CodeExpression> expressionList = new ArrayList<CodeExpression>();
        expressionList.add(codegen.var(dbConnectionName));
        Variable requestResponseVar = codegen.var(Constants.Variables.REQUEST_RESPONSE);
        expressionList.add(CodegenUtil.accessorCall(codegen, requestResponseVar, Constants.Variables.INPUT));

        // returning values are not INSERT/UPDATE values.
        parameterMap.getParamSet().stream()
            .forEach(param -> expressionList.add(CodegenUtil.accessorCall(codegen, requestResponseVar, param)));

        // TODO : don't expose the suffix.
        return codegen.methodCall(codegen.var(objectName + Constants.Suffixes.SQL_FNS),
                                  Constants.Functions.INSERT_UPDATE_LIST, codegen.argList(expressionList));
    }

    /**
     * Create an MethodCall of the form:
     * {@code <Object>SQLFns.insertUpdate(<dbconn>, <param>, <param>...) }
     * used to in
     * @param dbConnectionName name of the SQL datbase connection
     * @param objectName Object type which contains the SQL_FNS (suffix added)
     * @param parameterMap for the JDBC Parameters to pass to the insertUpdateList call
     * @return MethodCall described aboce.
     */
    public static MethodCall insertUpdateCall(CodeGen codegen,
                                              String dbConnectionName,
                                              String objectName,
                                              ParameterMap parameterMap) {
        List<CodeExpression> expressionList = new ArrayList<CodeExpression>();
        expressionList.add(codegen.var(dbConnectionName));
        Variable requestResponseVar = codegen.var(Constants.Variables.REQUEST_RESPONSE);
        parameterMap.getParamSet().stream()
                .forEach(param -> expressionList.add(CodegenUtil.accessorCall(codegen, requestResponseVar, param)));

        // TODO : don't expose the SQL_FNS suffix.
        return codegen.methodCall(codegen.var(objectName + Constants.Suffixes.SQL_FNS),
                                  Constants.Functions.INSERT_UPDATE, codegen.argList(expressionList));
    }
}
