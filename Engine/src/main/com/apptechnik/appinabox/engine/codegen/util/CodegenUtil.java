package com.apptechnik.appinabox.engine.codegen.util;

import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.exceptions.TypeNotFoundException;
import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.exceptions.UnsupportedExpressionException;
import com.apptechnik.appinabox.engine.exceptions.UnsupportedParamTypeException;
import com.apptechnik.appinabox.engine.parser.ColumnInfo;
import com.apptechnik.appinabox.engine.parser.types.SQLType;
import com.apptechnik.appinabox.engine.parser.pojos.MergedFieldNode;
import com.apptechnik.appinabox.engine.parser.pojos.ParameterMap;
import net.sf.jsqlparser.statement.Statement;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Code generation utilities.
 */
public class CodegenUtil {
    /**
     * call an accessor on an object field: {@code <objectVar>.get<VarName>() }
     * @param objectVar
     * @param varName
     * @return
     */
    public static MethodCall accessorCall(CodeGen codegen, Variable objectVar, String varName) {
        return codegen.methodCall(objectVar, accessorNameFromField(varName));
    }

    public static MethodCall accessorCall(CodeGen codegen, String objectVarName, String varName) {
        return codegen.methodCall(codegen.var(objectVarName), accessorNameFromField(varName));
    }

    public static MethodCall accessorCall(CodeGen codegen, String varName) {
        return codegen.methodCall(CodegenUtil.accessorNameFromField(varName));
    }

    /**
     * Generate:
     * {@code
     * collect(Collectors.toList())
     * }
     * @param codegen
     * @return
     */
    public static MethodCall collectToList(CodeGen codegen) {
        MethodCall collectorsToListCall = codegen.methodCall(codegen.var(Constants.JavaTypes.COLLECTORS),
                                                             Constants.Functions.TO_LIST);
        return codegen.methodCall(Constants.Functions.COLLECT, collectorsToListCall);
    }

    /**
     * Generate:
     * {@code stream().map(<lambda-expression>) }
     * @param codegen
     * @param lambdaExpr
     * @return
     */
    public static CodeExpression streamMap(CodeGen codegen, LambdaExpression lambdaExpr) {
        return codegen.dot(codegen.methodCall(Constants.Functions.STREAM), codegen.methodCall(Constants.Functions.MAP, lambdaExpr));
    }

    /**
     * Generate
     * {@code flatMap(Collection::stream) }
     */
    public static MethodCall flatMap(CodeGen codegen) {
        return codegen.methodCall(Constants.Functions.FLAT_MAP, codegen.var(Constants.Functions.COLLECTION_STREAM));
    }
    /**
     * {@code List<<object-name>> <object-name>List = new ArrayList<<object-name>>(); }
     * @param elementType
     * @param listVar
     * @return
     */
    public static TypedAssignmentStatement listAllocAssignVar(CodeGen codegen, Variable listVar, Type elementType) {
        GenericType listType = codegen.listType(elementType);
        Allocation listAllocation = codegen.alloc(codegen.arrayListType(elementType));
        return codegen.typedAssignStmt(listType, listVar, listAllocation);
    }

    /**
     * {@code List<<object-name>> <object-name>List = new ArrayList<<object-name>>(); }
     * @param elementType
     * @param listVar
     * @return
     */
    public static AssignmentStatement listAllocAssignMember(CodeGen codegen, Variable listVar, Type elementType) {
        Allocation listAllocation = codegen.alloc(codegen.arrayListType(elementType));
        return codegen.assignStmt(listVar, listAllocation);
    }

    /**
     * Create an accessor of the form:
     * {@code public <type> get<Field>() {
     *     return field;
     * }}
     * @param type
     * @param field
     * @return
     */
    public static FunctionDefinition accessorDefinition(CodeGen codegen, Type type, Variable field) {
        String functionName = accessorNameFromField(field.getName());
        FunctionDeclaration decl = codegen.functionDecl(functionName, null, type);
        return codegen.functionDef(decl,  codegen.stmtList(codegen.returnStmt(field)));
    }

    /**
     * create a setter of the form:
     * {@code public void set<Field>(<type> <field>) {
     *     this.<field> = <field>;
     * }
     * }
     * @param type field/argument type
     * @param field field/argument name
     * @return Function Definition (see above)
     */
    public static FunctionDefinition setterDefinition(CodeGen codegen, Type type, Variable field) {
        Type voidType = codegen.simpleType(Constants.JavaTypes.VOID);
        String functionName = setterNameFromField(field.getName());
        ParameterDeclaration parameterDeclaration = codegen.paramDecl(type, field);
        ParameterList parameterList = codegen.paramList(Collections.singletonList(parameterDeclaration));
        FunctionDeclaration decl = codegen.functionDecl(functionName, parameterList, voidType);
        CodeExpression variableField = codegen.dot(codegen.var(Constants.JavaKeywords.THIS),
                                                   codegen.var(field.getName()));
        return codegen.functionDef(decl, codegen.stmtList(codegen.assignStmt(variableField, field)));
    }

    public static List<FunctionDefinition> accessorAndSetterDefinition(CodeGen codegen, Type type, Variable field) {
        List<FunctionDefinition> list = new ArrayList<FunctionDefinition>();
        list.add(accessorDefinition(codegen, type, field));
        list.add(setterDefinition(codegen, type, field));
        return list;
    }

    /**
     * Wrapper for memberDeclarationFromSQLType
     * @param codegen
     * @param columnInfo
     * @param accessModifier
     * @return
     * @throws TypeNotFoundException
     */
    public static MemberDeclaration memberDeclarationFromColumn(CodeGen codegen,
                                                                ColumnInfo columnInfo,
                                                                String accessModifier)
    throws TypeNotFoundException{
        String sqlType = columnInfo.getColDataType().getDataType();
        boolean isNullable = !columnInfo.isNotNull();
        return CodegenUtil.memberDeclarationFromSQLType(codegen, columnInfo.getColumnName(true),
                                                        sqlType, !isNullable, accessModifier);

    }
    /**
     * Map a column name and SQL type to a target member declaration
     * @param codegen
     * @param name column name
     * @param sqlTypeStr SQL type
     * @param isNotNull isNotNull qualifier from SQL
     * @param accessModifier
     * @return
     * @throws TypeNotFoundException
     */
    public static MemberDeclaration memberDeclarationFromSQLType(CodeGen codegen,
                                                                 String name,
                                                                 String sqlTypeStr,
                                                                 boolean isNotNull,
                                                                 String accessModifier)
    throws TypeNotFoundException {
        Type type = codegen.typeFromSQLType(sqlTypeStr, !isNotNull);
        return codegen.memberDecl(accessModifier, false, type, codegen.var(name));
    }

    public static Type nullableType(CodeGen codegen, Type type, boolean isNullable) throws TypeNotFoundException {
        if (type instanceof SimpleType) {
            return codegen.simpleType(type.getName(), isNullable);
        } else if (type instanceof GenericType) {
            GenericType genericType = (GenericType) type;
            return codegen.genericType(genericType.getContainer(), genericType.getItemList(), isNullable);
        } else {
            throw new TypeNotFoundException("type " + type + " was not found");
        }
    }

    /**
     * create a setter from the SQL type:
     * {@code public void set<Name>(<type> name) {
     *     this.name = name;
     * }
     * }
     * @param name variable name
     * @param sqlTypeStr SQL Type as string.
     * @param isNullable type can be null
     * @return
     * @throws TypeNotFoundException
     */
    public static FunctionDefinition setterDefinition(CodeGen codegen,
                                                      String name,
                                                      String sqlTypeStr,
                                                      boolean isNullable)
    throws TypeNotFoundException {
        return CodegenUtil.setterDefinition(codegen, codegen.typeFromSQLType(sqlTypeStr, isNullable), codegen.var(name));
    }

    public static FunctionDefinition setterDefinition(CodeGen codegen, MemberDeclaration memberDecl) {
        return CodegenUtil.setterDefinition(codegen, memberDecl.getType(), memberDecl.getVariable());
    }


    public static FunctionDefinition accessorDefinition(CodeGen codegen, MemberDeclaration memberDecl) {
        return CodegenUtil.accessorDefinition(codegen, memberDecl.getType(), memberDecl.getVariable());
    }

    /**
     * Create the list of setters and accessors from the member list.
     * @param codegen
     * @param memberDeclList
     * @return
     */
    public static List<FunctionDefinition> accessorsAndSetters(CodeGen codegen, List<MemberDeclaration> memberDeclList) {
        List<FunctionDefinition> methods = memberDeclList.stream()
                .map(m -> CodegenUtil.accessorDefinition(codegen, m))
                .collect(Collectors.toList());
        methods.addAll(memberDeclList.stream()
                .map(m -> CodegenUtil.setterDefinition(codegen, m))
                .collect(Collectors.toList()));
        return methods;
    }

    /**
     * create an accessor from the SQL type
     * {@code public <SQLType> get<Name>() {
     *     return name;
     * }
     * }
     * @param name
     * @param sqlTypeStr
     * @param isNullable
     * @return Function Defintion (see above)
     * @throws TypeNotFoundException
     */
    public static FunctionDefinition accessorDeclarationFromSQLType(CodeGen codegen,
                                                                    String name,
                                                                    String sqlTypeStr,
                                                                    boolean isNullable)
    throws TypeNotFoundException {
        return CodegenUtil.accessorDefinition(codegen, codegen.typeFromSQLType(sqlTypeStr, isNullable), codegen.var(name));
    }

    /**
     * Generate the statement which assigns the SQL string to a variable.
     * {@code String <name> = "<sqlStr>"}
     * @param sqlStr SQL statement (probably statement.toString())
     * @param name variable name to use.
     * @return
     */
    public static MemberDeclaration statementStringDefinition(CodeGen codegen,
                                                              String sqlStr,
                                                              String name) {
        StringConstant sqlConstant = codegen.stringConst(sqlStr);
        Variable var = codegen.var(name);
        SimpleType stringType = codegen.simpleType(Constants.JavaTypes.STRING);
        MemberDeclaration memberDecl = codegen.memberDecl(Constants.JavaKeywords.PRIVATE, true, stringType, var);
        return codegen.memberDecl(memberDecl, sqlConstant);
    }

    /**
     * Given the list of SQL statements associated with this node, create the variable assignments
     * @param node
     * @return
     * @throws UnsupportedExpressionException
     */
    // TODO: move this to MergedFieldNode
    public static List<MemberDeclaration> sqlStatementVariables(CodeGen codegen,
                                                                MergedFieldNode node) throws UnsupportedExpressionException {
        // multiline-escape the SQL statements associated with this node.
        return node.createStatementVariableNames().keySet().stream()
                .map(stmt -> CodegenUtil.statementStringDefinition(codegen, stmt.toString().replace("\n", "\" + \n\""),
                                                                   node.getSQLConstantName(stmt)))
                .collect(Collectors.toList());
    }

    public static List<MemberDeclaration> sqlStatementVariables(CodeGen codegen,
                                                                List<Statement> statementList) {
        List<MemberDeclaration> declarationList = new ArrayList<MemberDeclaration>();
        int varIndex = 1;
        for (Statement statement: statementList) {
            String statementString = statement.toString().replace("\n", "\" + \n\"");
            String sqlStringConstant = Constants.Variables.SQL + varIndex;
            varIndex++;
            declarationList.add(CodegenUtil.statementStringDefinition(codegen, statementString, sqlStringConstant));
        }
        return declarationList;
    }


    /**
     * Create the parameter declarations for a parameter map.  We return a list rather than a
     * ParmeterList, because there may be other parameters passed to the function.
     * @param paramMap map of JDBC Named parameters to types.
     * @return list of parameter declarations
     * @throws TypeNotFoundException
     */
    public static List<ParameterDeclaration> parameterMapToDeclarations(CodeGen codegen,
                                                                        ParameterMap paramMap) throws TypeNotFoundException {
        List<ParameterDeclaration> declarations = new ArrayList<ParameterDeclaration>();
        for (String paramName : paramMap.getParamNames()) {
            try {
                ParameterMap.ParamData paramData = paramMap.get(paramName);
                Type type = codegen.typeFromSQLType(paramData.getDataType().getDataType(), paramData.isNullable());
                declarations.add(codegen.paramDecl(type, codegen.var(paramName)));
            } catch (TypeNotFoundException tnfex) {
                throw new TypeNotFoundException("for parameter " + paramName, tnfex);
            }
        }
        return declarations;
    }

    /**
     * Create the parameter declarations for a parameter map.  We return a list rather than a
     * ParmeterList, because there may be other parameters passed to the function.
     * @param paramMap map of JDBC Named parameters to types.
     * @return list of parameter declarations
     * @throws TypeNotFoundException
     */
    public static List<ParameterDeclaration> endpointParameterDeclarations(CodeGen codegen,
                                                                           String queryPackageName,
                                                                           ParameterMap paramMap)
    throws TypeNotFoundException, UnsupportedParamTypeException {
        List<ParameterDeclaration> declarations = new ArrayList<ParameterDeclaration>();
        for (String paramName : paramMap.getParamNames()) {
            try {
                ParameterMap.ParamData paramData = paramMap.get(paramName);
                String fullyQualifiedParamName = queryPackageName + "." + paramName;
                Type type = codegen.typeFromSQLType(paramData.getDataType().getDataType(), paramData.isNullable());
                Annotation paramAnnot = ParameterMap.ParamSource.paramAnnotation(codegen, fullyQualifiedParamName, paramData.getSource());
                declarations.add(codegen.paramDecl(type, codegen.var(paramName), paramAnnot));
            } catch (TypeNotFoundException tnfex) {
                throw new TypeNotFoundException("for parameter " + paramName, tnfex);
            }
        }
        return declarations;
    }

    public static ArgumentList endpointArgumentList(CodeGen codegen,
                                                    ParameterMap paramMap) {
        return codegen.argList(paramMap.getParamNames().stream().map(n -> codegen.var(n)).collect(Collectors.toList()));
    }

    public static  List<ParameterDeclaration> repositoryParameterDeclarations(CodeGen codegen,
                                                                              ParameterMap paramMap)
            throws TypeNotFoundException {
        List<ParameterDeclaration> declarations = new ArrayList<ParameterDeclaration>();
        for (String paramName : paramMap.getParamNames()) {
            try {
                ParameterMap.ParamData paramData = paramMap.get(paramName);
                Type type = codegen.typeFromSQLType(paramData.getDataType().getDataType(), paramData.isNullable());
                declarations.add(codegen.paramDecl(type, codegen.var(paramName)));
            } catch (TypeNotFoundException tnfex) {
                throw new TypeNotFoundException("for parameter " + paramName, tnfex);
            }
        }
        return declarations;
    }

    /**
     * Given a node with name "node-name", generate a ParameterDeclaration of the form:
     * {@code List<name> <name>List}
     * @param type list item type
     * @param name parameter name
     * @return parameter declaration described above
     */
    public static ParameterDeclaration listParameterDeclaration(CodeGen codegen, Type type, String name) {
        GenericType thisTypeList = codegen.listType(type);
        Variable thisListVar = codegen.var(name + Constants.Suffixes.LIST);
        return codegen.paramDecl(thisTypeList, thisListVar);
    }

    /**
     * create Parameter declaration for the database connection of the form:
     * {@code Connection <name>}
     * @param name
     * @return
     */
    public static ParameterDeclaration dbConnectionParameterDeclaration(CodeGen codegen, String name) {
        return codegen.paramDecl(codegen.classType(Constants.JavaTypes.CONNECTION), codegen.var(name));
    }

    /**
     * Strip the types from the declaration to make a parameter list.
     * @param paramList list of parameter declarations
     * @return list of parameters/arguments
     */
    public static List<CodeExpression> declarationsToParameters(List<ParameterDeclaration> paramList) {
        return paramList.stream()
                .map(parameterDeclaration -> parameterDeclaration.getVariable()).collect(Collectors.toList());
    }

    public static List<CodeExpression> declarationsToParameters(ParameterList paramList) {
        return paramList.getParameterDeclarationList().stream()
                .map(parameterDeclaration -> parameterDeclaration.getVariable()).collect(Collectors.toList());
    }

    public static MethodCall toMethodCall(CodeGen codegen, FunctionDeclaration fnDecl) {
        return codegen.methodCall(fnDecl.getName(), codegen.argList(CodegenUtil.declarationsToParameters(fnDecl.getParameterList())));
    }

    /**
     * Utility to create the function declaration:
     * {@code public static void insertUpdate(<parameter-list>) throws SQLException }
     * @param params list of parameter declerations:
     * {@code Connection <conn>, <type(<param>)> <param>, ... }
     * @return Function Declaration (see above)
     */
    public static FunctionDeclaration updateInsertDeclaration(CodeGen codegen, ParameterList params) {
        return codegen.functionDecl(true, Constants.Functions.INSERT_UPDATE,
                                    params, codegen.simpleType(Constants.JavaTypes.VOID),
                                    codegen.typeList(codegen.classType(Constants.JavaExceptions.SQL_EXCEPTION)));
    }

    /**
     * Utility to create the function declaration:
     * {@code public static void insertUpdateList(<parameter-list>) throws SQLException}
     * @param params list of parameter declerations:
     * {@code Connection <conn>, <type(<param>)> <param>, ... }
     * @return
     */
    public static FunctionDeclaration updateInsertListDeclaration(CodeGen codegen, ParameterList params) {
        return codegen.functionDecl(true, Constants.Functions.INSERT_UPDATE_LIST,
                                    params, codegen.simpleType(Constants.JavaTypes.VOID),
                                    codegen.typeList(codegen.classType(Constants.JavaExceptions.SQL_EXCEPTION)));

    }

    /**
     * Generete the ResultSet.getXXX() expression of the form:
     * {@code <result-set-var-name>.getXXX() }
     * @param resultSetVarName
     * @param objectName
     * @param columnInfo
     * @return
     * @throws TypeNotFoundException
     */
    public static MethodCall getResultSetValue(CodeGen codegen,
                                               String resultSetVarName,
                                               String objectName,
                                               ColumnInfo columnInfo)
    throws TypeNotFoundException {
        MethodCall rsGetValueCall =
            SQLType.resultSetCallFromSqlType(codegen, resultSetVarName, columnInfo.getColumnSQLTypeString(),
                                             columnInfo.getColumnName(true));
        return codegen.methodCall(codegen.var(objectName),
                                  setterNameFromField(columnInfo.getColumnName(true)), rsGetValueCall);
    }

    /**
     * Generate the getter name for a field
     *  {@code <var> -> get<Var> }
     * @param var
     * @return
     */
    public static String accessorNameFromField(String var) {
        StringBuffer sb = new StringBuffer();
        sb.append("get");
        sb.append(Character.toUpperCase(var.charAt(0)));
        sb.append(var.substring(1));
        return sb.toString();
    }

    /**
     * Generate the setter name for a field
     * {@code <var> -> set<Var> }
     * @param var
     * @return
     */
    public static String setterNameFromField(String var) {
        StringBuffer sb = new StringBuffer();
        sb.append("set");
        sb.append(Character.toUpperCase(var.charAt(0)));
        sb.append(var.substring(1));
        return sb.toString();
    }

    /**
     * Constructor field assignment:
     * NOTE: Constants.KotlinKeywords.THIS needs to be changed to codegen.this()
     * {@Code
     * this.<field> = <field>
     * }
     * @param codegen code generator
     * @param fieldName field to assign
     * @return assignment statement
     */
    public static AssignmentStatement assignThisField(CodeGen codegen, String fieldName) {
       return codegen.assignStmt(codegen.dot(codegen.var(Constants.KotlinKeywords.THIS), codegen.var(fieldName)),
                                 codegen.var(fieldName));
    }
}
