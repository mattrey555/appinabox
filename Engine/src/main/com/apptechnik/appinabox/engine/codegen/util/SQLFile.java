package com.apptechnik.appinabox.engine.codegen.util;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.android.ListItems;
import com.apptechnik.appinabox.engine.exceptions.DirectiveNotFoundException;
import com.apptechnik.appinabox.engine.exceptions.ParameterFormatException;
import com.apptechnik.appinabox.engine.exceptions.PropertiesException;
import com.apptechnik.appinabox.engine.exceptions.TokenException;
import com.apptechnik.appinabox.engine.parser.TableInfo;
import com.apptechnik.appinabox.engine.parser.TableInfoMap;
import com.apptechnik.appinabox.engine.parser.json.ParseJSONMapExpression;
import com.apptechnik.appinabox.engine.parser.pojos.FieldReferenceNode;
import com.apptechnik.appinabox.engine.parser.pojos.JsonFieldReference;
import com.apptechnik.appinabox.engine.parser.pojos.ParameterMap;
import com.apptechnik.appinabox.engine.parser.pojos.Tree;
import com.apptechnik.appinabox.engine.parser.query.SelectTableUtils;
import com.apptechnik.appinabox.engine.parser.sync.ValidateSync;
import com.apptechnik.appinabox.engine.util.*;
import com.apptechnik.appinabox.library.sql.NamedParamStatement;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserManager;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Basic parse of the SQL file, just getting the comments and statements, not finding parameters or types or anything like that.
 * Stuff like type inference, fixed types, json mapping for requests and responses are deferred to the caller.
 */
public class SQLFile {
    private final File file;
    private List<Statement> statementList;
    private Map<Statement, Map<String, String>> jdbcDotParamMaps;   // map of JDBC parameters to preprocessed "dot" parameters.
    private List<String> comments;

    /**
     * NOTE: If there are multiple statements in a file, they must be separated by semicolons.
     * Multi-line statements should be allowed.
     * WHile parsing the statements,
     * @param file file containing SQL and comments
     * @param parseSQL true; Parse SQL into statements, false: just get comments.
     * @throws IOException failed to read the file
     * @throws JSQLParserException SQL parse failed.
     */
    public SQLFile(File file, boolean parseSQL) throws IOException, JSQLParserException, ParameterFormatException {
        this.file = file;
        init(FileUtil.readFileToString(file), parseSQL);
    }

    /**
     * Same, but takes string (usually a resource used for testing).
     * @param sourceText source text, probably read from a file or resource
     * @param parseSQL directive to actually parse the SQL in the file.
     * @throws JSQLParserException SQL parse failed
     */
    public SQLFile(String sourceText, boolean parseSQL) throws JSQLParserException {
        this.file = new File("literal text");
        init(sourceText, parseSQL);
    }

    public void init(String sourceText, boolean parseSQL) throws JSQLParserException {
        if (parseSQL) {
            // find all the text which is not in comments, and assume that it is SQL statements.  Multiple SQL statements
            // MUST be delimted with ';'
            String[] sourceLines = StringUtil.stripEmptyLines(PreprocessorUtil.stripStandardComments(sourceText)).split("\n");
            String source = StringUtil.concatArray(" ", sourceLines);
            String[] delimitedSourceLines = StringUtil.splitQuotedEscaped(source, ";", "'", "\\");

            // we pre-process the SQL to allow dot-format variables.
            CCJSqlParserManager parseManager = new CCJSqlParserManager();
            this.statementList = new ArrayList<Statement>();
            this.jdbcDotParamMaps = new HashMap<Statement, Map<String, String>>();
            for (String delimitedSourceLine : delimitedSourceLines) {
                Map<String, String> dotParamMap = ParameterMap.mapDotParameters(delimitedSourceLine);
                delimitedSourceLine = NamedParamStatement.replaceMapStrings(delimitedSourceLine, dotParamMap);
                Statement statement = parseManager.parse(new StringReader(delimitedSourceLine));
                this.statementList.add(statement);
                this.jdbcDotParamMaps.put(statement, dotParamMap);
            }
        } else {
            this.statementList = null;
            this.jdbcDotParamMaps = null;
        }
        this.comments = PreprocessorUtil.textInStandardComments(sourceText);
    }

    public SQLFile(File file) throws IOException, JSQLParserException, ParameterFormatException {
        this(file, true);
    }

    public List<String> filterTag(String tagName) {
        return StringUtil.filterTag(this.getComments(), tagName);
    }

    /**
     * Find the first match for a tag. Hopefully there is only one
     * @param tagName tag to search for (should be in Constants.Directives)
     * @return resulting tag.
     */
    public String findTag(String tagName) {
        return StringUtil.findTag(this.getComments(), tagName);
    }

    /**
     * Find the first match for a tag. Hopefully there is only one.  If there are none, throw an exception
     * @param tagName tag to search for (should be in Constants.Directives)
     * @return resulting tag.
     */
    public String getTag(String tagName) throws DirectiveNotFoundException {
        String s = StringUtil.findTag(this.getComments(), tagName);
        if (s == null) {
            throw new DirectiveNotFoundException(this.file, tagName);
        }
        return s;
    }

    public List<String> findTags(String tagName) {
        return StringUtil.findTags(this.getComments(), tagName);
    }

    /**
     * Read the type map for fixed types.
     * form is type {@code 1<variable> <type> }
     * @return map of variable names to fixed types.
     * @throws PropertiesException incorrect type directive syntax
     */
    public Map<String, String> readFixedParamTypeMap() throws PropertiesException {
        List<String> typeSpecifiers = StringUtil.filterTag(this.getComments(), Constants.Directives.TYPE);
        Map<String, String> fixedParamTypeMap = new HashMap<String, String>();
        for (String line : typeSpecifiers) {
            line = line.trim();
            int ichDelim = StringUtil.firstWhiteSpace(line);
            if (ichDelim < 0) {
                throw new PropertiesException("type directive " + line +
                                              " should have 3 components: type: <param-name> <type>");
            }
            String variable = line.substring(0, ichDelim);
            String type = line.substring(ichDelim + 1);
            fixedParamTypeMap.put(variable, type);
        }
        return fixedParamTypeMap;
    }

    /**
     * Parse mapping from incoming JSON to Insert statements.
     * @return JSON tree or null if json: tag not found in comments.
     * @param rootName root name to apply to JSON mapping (probably from the SQL File Name with the extension stripped)
     * @param nameTag name tag which overrides the rootName
     * @param jsonTag tag for the JSON to parse.
     * @param nodeFactory factory for creating MergedFieldNodes or QueryFieldsNode
     * @throws ParseException JSON parsing failed
     * @throws IOException token reading failed
     * @throws TokenException tokenizing failed
     */
    public <T extends FieldReferenceNode, S extends JsonFieldReference> Tree<T> createJSONTree(String rootName,
                                                                                               String nameTag,
                                                                                               String jsonTag,
                                                                                               ParseJSONMapExpression.Factory<T, S> nodeFactory)
    throws IOException, ParseException, TokenException {
        String jsonFormat = findTag(jsonTag);
        if (jsonFormat == null) {
            return null;
        }

        // override the name, which defaults to the passed rootName (probably the file wihout extension
        String jsonName = findTag(nameTag);
        if (jsonName != null) {
            rootName = jsonName;
        }
        if (jsonFormat != null) {
            Tree<T> jsonTree = ParseJSONMapExpression.parse(jsonFormat, nodeFactory);
            jsonTree.get().setName(rootName);
            return jsonTree;
        } else {
            return null;
        }
    }


    public List<Statement> getStatementList() {
        return statementList;
    }

    public Map<Statement, Map<String, String>> getJdbcDotParamMaps() {
        return jdbcDotParamMaps;
    }

    public List<String> getComments() {
        return comments;
    }

    public File getFile() {
        return file;
    }

    /**
     * Get either the name from the name tag, or this filename with the extension stripped.
     * @return
     */
    public String getName() {
        String name = findTag(Constants.Directives.NAME);
        if (name == null) {
            name = StringUtil.removeExtension(getFile().getName());
        }
        return name;
    }

    /**
     * Get the SQLFile description
     * @return
     */
    public String getDescription() {
        String description = findTag(Constants.Directives.DESCRIPTION);
        return (description != null) ? description : getName();
    }

    /**
     * Given a directive, is an element in its space-delimited list.
     * ex endpoints: android ios
     * @param directive tag name
     * @param element name to search for.
     * @return
     */
    public boolean inDirectiveList(String directive, String element) {
        String endpoints = findTag(directive);
        if (endpoints != null) {
            String[] endpointNames = endpoints.split(" ");
            return StringUtil.in(endpointNames, element);
        }
        return false;
    }

    /**
     * Validate that tables marked for sync have MODIFIED_SINCE TIMESTAMP and DELETED BOOLEAN fields.
     * @param syncTables list of tables with sync directive in SQLFIle
     * @param createTableInfoMap CREATE TABLE statements from parser.
     * @return
     * @throws Exception
     */
    public List<String> validateSyncConventions(String sourceFile,
                                                Collection<String> syncTables,
                                                TableInfoMap createTableInfoMap) throws Exception {
        List<String> errorList = new ArrayList<String>();
        for (Statement statement : statementList) {
            errorList.addAll(ValidateSync.validateSyncConventions(sourceFile, statement, createTableInfoMap, syncTables));
            if (statement instanceof PlainSelect) {
                errorList.addAll(ValidateSync.validateSelectTableReferences(file.getAbsolutePath(),
                                                                            (Select)  statement, createTableInfoMap, syncTables));
            }
        }
        return errorList;
    }

    // syncTables.addAll(StringUtil.splitAndTrimList(sync, "[, \t]"));
    // TODO: boneyard this until we have synchronization well defined.
    public void markSyncTables(TableInfoMap createTableInfoMap, List<String> errorMessages) {
        String syncExpression = findTag(Constants.Directives.SYNC);
        if (syncExpression != null) {
            List<String> syncTableNames = StringUtil.splitAndTrimList(syncExpression, "[, \t]");

            // deleted and timestamp columns must be in "table.column" form.
            List<String> deletedColumnNames = StringUtil.splitAndTrimList(findTag(Constants.Directives.DELETED), ",");
            List<String> timestampColumnNames = StringUtil.splitAndTrimList(findTag(Constants.Directives.TIMESTAMP), ",");
            for (String syncTableName : syncTableNames) {
                String deletedColumnName = deletedColumnNames.stream()
                        .filter(c -> StringUtil.getTableName(c).equals(syncTableName))
                        .findFirst().orElse(Constants.SQLColumns.DELETED);
                String timestampColumnName = timestampColumnNames.stream()
                        .filter(c -> StringUtil.getTableName(c).equals(syncTableName))
                        .findFirst().orElse(Constants.SQLColumns.MODIFIED_DATE);
                TableInfo syncTable = createTableInfoMap.getTableInfo(syncTableName);
                syncTable.setSync(true, StringUtil.getColumnName(deletedColumnName),
                                  StringUtil.getColumnName(timestampColumnName));
            }
        }
    }

    /**
     * Get the layout associated with JSON dot reference, for list elements.
     * @param dotReference
     * @param defaultCount
     * @return
     */
    public String getLayoutName(String dotReference, int defaultCount) {
        String layout = findTag(Constants.Directives.LAYOUT + "." + dotReference);
        if (layout == null) {
            // NOTE: we will need to select which list item to use as a template.
            return ListItems.getListElements(defaultCount)[0];
        } else {
            return layout;
        }
    }

    /**
     * Return the list of variables (VAR1, VAR2...) in the layout XML.
     * @param layout
     * @return
     */
    public List<String> layoutFields(String layout) {
        return Substitute.getParams(layout).stream()
                .filter(var -> var.startsWith(Constants.Substitutions.VAR))
                .collect(Collectors.toList());
    }
}
