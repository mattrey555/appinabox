package com.apptechnik.appinabox.engine.exceptions;

import com.apptechnik.appinabox.engine.parser.TableInfo;

public class AmbiguousColumnException extends AppInABoxException {
	public AmbiguousColumnException(String message) {
		super(message);
	}

	public AmbiguousColumnException(String columnName, TableInfo table1, TableInfo table2) {
		this(columnName + " is ambiguous between " + table1.getTable().getName() +
			 " and " + table2.getTable().getName());
	}
}
		
