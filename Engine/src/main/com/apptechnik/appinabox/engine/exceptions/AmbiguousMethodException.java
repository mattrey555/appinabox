package com.apptechnik.appinabox.engine.exceptions;


import java.lang.reflect.Method;
import java.util.Set;

public class AmbiguousMethodException extends AppInABoxException {
	public AmbiguousMethodException(String message) {
		super(message);
	}

	public AmbiguousMethodException(String message, String methodName, Set<Method> methodSet) {
		super(message + " " + methodName + " " + methodListToString(methodSet));
	}

	private static String methodListToString(Set<Method> methodSet) {
		StringBuffer sb = new StringBuffer();
		for (Method method : methodSet) {
			sb.append(method.toString() + "\n");
		}
		return sb.toString();
	}
}
		
