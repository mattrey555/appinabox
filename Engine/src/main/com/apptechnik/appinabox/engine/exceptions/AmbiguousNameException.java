package com.apptechnik.appinabox.engine.exceptions;

import com.apptechnik.appinabox.engine.parser.TableInfo;

public class AmbiguousNameException extends AppInABoxException {
	public AmbiguousNameException(String name) {
		super(name + " is ambiguous");
	}
}
		
