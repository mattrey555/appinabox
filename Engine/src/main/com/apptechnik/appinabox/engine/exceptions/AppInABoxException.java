package com.apptechnik.appinabox.engine.exceptions;

public class AppInABoxException extends Exception {
    public AppInABoxException(String message) {
        super(message);
    }

    public AppInABoxException(String message, Throwable cause) {
        super(message, cause);
    }

}
