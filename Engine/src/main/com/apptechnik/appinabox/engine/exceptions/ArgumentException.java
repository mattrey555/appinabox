package com.apptechnik.appinabox.engine.exceptions;

public class ArgumentException extends AppInABoxException {
	public ArgumentException(String message) {
		super(message);
	}
}
		
