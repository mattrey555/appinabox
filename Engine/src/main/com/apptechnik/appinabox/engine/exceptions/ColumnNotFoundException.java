package com.apptechnik.appinabox.engine.exceptions;

public class ColumnNotFoundException extends AppInABoxException {
	public ColumnNotFoundException(String message) {
		super(message);
	}
}
		
