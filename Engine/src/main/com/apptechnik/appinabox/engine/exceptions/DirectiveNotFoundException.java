package com.apptechnik.appinabox.engine.exceptions;

import java.io.File;

public class DirectiveNotFoundException extends AppInABoxException {
	public DirectiveNotFoundException(File file, String tag) {
		super("Failed to find required tag " + tag + " in file " + file.getAbsolutePath());
	}
}
		
