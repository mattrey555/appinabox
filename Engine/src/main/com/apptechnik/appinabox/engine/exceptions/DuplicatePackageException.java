package com.apptechnik.appinabox.engine.exceptions;

import com.apptechnik.appinabox.engine.codegen.util.SQLFile;

public class DuplicatePackageException extends AppInABoxException {
	public DuplicatePackageException(String description, String name, SQLFile file1, SQLFile file2) {
		super(description + " " + name + " is ambiguous between " + file1.getFile().getAbsolutePath() +
			  " and " + file2.getFile().getAbsolutePath());
	}
}
		
