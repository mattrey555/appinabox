package com.apptechnik.appinabox.engine.exceptions;

import net.sf.jsqlparser.schema.Table;

import java.util.function.Function;

public class FunctionNotFoundException extends AppInABoxException {
	public FunctionNotFoundException(String message) {
		super(message);
	}
	public FunctionNotFoundException(Function function, String message) {
		super(function.toString() + " not found " + message);
	}
}
		
