package com.apptechnik.appinabox.engine.exceptions;

import net.sf.jsqlparser.expression.JdbcNamedParameter;
import net.sf.jsqlparser.statement.create.table.ColDataType;

/**
 * Thrown when parsing the header: directive
 */
public class HeaderFormatException extends Exception {
    public HeaderFormatException(String msg) {
        super(msg);
    }
}
