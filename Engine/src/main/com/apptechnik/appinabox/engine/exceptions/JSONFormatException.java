package com.apptechnik.appinabox.engine.exceptions;

public class JSONFormatException extends Exception {
    public JSONFormatException(String message) {
        super(message);
    }
}
