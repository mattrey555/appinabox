package com.apptechnik.appinabox.engine.exceptions;


import net.sf.jsqlparser.expression.JdbcNamedParameter;
import net.sf.jsqlparser.statement.Statement;

public class JdbcParamNotFoundException extends AppInABoxException {
	public JdbcParamNotFoundException(String message) {
		super(message);
	}
	public JdbcParamNotFoundException(Statement statement, JdbcNamedParameter param) {
		super("JDBC parameter " + param.getName() + " not found in " + statement);
	}
}
		
