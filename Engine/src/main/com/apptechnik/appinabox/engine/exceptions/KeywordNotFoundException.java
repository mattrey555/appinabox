package com.apptechnik.appinabox.engine.exceptions;

public class KeywordNotFoundException extends AppInABoxException {
	public KeywordNotFoundException(String message) {
		super(message);
	}
}
		
