package com.apptechnik.appinabox.engine.exceptions;


public class MethodNotFoundException extends AppInABoxException {
	public MethodNotFoundException(String message) {
		super(message);
	}
}
		
