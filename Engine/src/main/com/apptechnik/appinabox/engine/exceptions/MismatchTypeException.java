package com.apptechnik.appinabox.engine.exceptions;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.Type;
import java.util.List;

public class MismatchTypeException extends AppInABoxException {
	public MismatchTypeException(String message) {
		super(message);
	}
	public MismatchTypeException(String fieldName, Class c, Type type) {
		this("field " + fieldName +
			" in class " + c +
			" has unsupported type " + type.getTypeName());
	}

	public MismatchTypeException(Field field, Type type) {
		this(field.getName(), field.getDeclaringClass(), type);
	}

	public MismatchTypeException(String fieldName, Type type, Class c, Method method, Parameter param) {
		this("Field " + fieldName + " of type " + type.getTypeName() + " in class " + c.getName() +
				" does not match method " + method.getName() + " parameter " + param.getName() +
				" of type " + param.getType());

	}

	public MismatchTypeException(Method method, List<Class> argListTypes) {
		this("Method " + method.getName() + " in class " + method.getDeclaringClass() +
			 " cannot support multiple arguments " + argListToString(argListTypes));
	}

	private static String argListToString(List<Class> argList) {
		StringBuffer sb = new StringBuffer();
		for (Class c : argList) {
			sb.append(c.getName() + " ");
		}
		return sb.toString();
	}
}
		
