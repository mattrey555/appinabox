package com.apptechnik.appinabox.engine.exceptions;

public class MissingRequirementException extends Exception {
    public MissingRequirementException(String message) {
        super(message);
    }
}
