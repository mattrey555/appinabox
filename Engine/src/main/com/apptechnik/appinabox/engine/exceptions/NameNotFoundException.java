package com.apptechnik.appinabox.engine.exceptions;

public class NameNotFoundException extends AppInABoxException {
	public NameNotFoundException(String message) {
		super(message);
	}
}
		
