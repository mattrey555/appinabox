package com.apptechnik.appinabox.engine.exceptions;

import net.sf.jsqlparser.expression.JdbcNamedParameter;
import net.sf.jsqlparser.statement.create.table.ColDataType;

/**
 * Thrown when merging parameter maps, and two columns with the same name
 * map to different types.
 */
public class ParamTypeMismatchException extends Exception {
    public ParamTypeMismatchException(String msg) {
        super(msg);
    }

    public ParamTypeMismatchException(JdbcNamedParameter param, ColDataType typeA, ColDataType typeB) {
        super(param.getName() + " has different types " + typeA.getDataType() +
                " and " + typeB.getDataType() + " in different expressions");
    }

    public ParamTypeMismatchException(String param, ColDataType typeA, ColDataType typeB) {
        super(param + " has different types " + typeA.getDataType() +
                " and " + typeB.getDataType() + " in different expressions");
    }
}
