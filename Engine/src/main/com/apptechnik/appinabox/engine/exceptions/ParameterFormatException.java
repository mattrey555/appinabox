package com.apptechnik.appinabox.engine.exceptions;

public class ParameterFormatException extends AppInABoxException{
    public ParameterFormatException(String message) {
        super(message);
    }

}
