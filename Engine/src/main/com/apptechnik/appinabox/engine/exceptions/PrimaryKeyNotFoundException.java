package com.apptechnik.appinabox.engine.exceptions;

public class PrimaryKeyNotFoundException extends AppInABoxException{
    public PrimaryKeyNotFoundException(String message) {
        super(message);
    }

}
