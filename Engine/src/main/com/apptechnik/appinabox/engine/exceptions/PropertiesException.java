package com.apptechnik.appinabox.engine.exceptions;

import java.io.File;

public class PropertiesException extends Exception {
    public PropertiesException(String message) {
        super(message);
    }
    public PropertiesException(File propertiesFile, String property) {
        super("property " + property + " not found in " + propertiesFile.getAbsolutePath());
    }

}
