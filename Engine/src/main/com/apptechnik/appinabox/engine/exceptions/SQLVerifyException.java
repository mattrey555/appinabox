package com.apptechnik.appinabox.engine.exceptions;

public class SQLVerifyException extends AppInABoxException {
	public SQLVerifyException(String message) {
		super(message);
	}
}
		
