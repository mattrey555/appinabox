package com.apptechnik.appinabox.engine.exceptions;

public class SyncException extends Exception {
    public SyncException(String message) {
        super(message);
    }
}
