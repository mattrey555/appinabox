package com.apptechnik.appinabox.engine.exceptions;

import net.sf.jsqlparser.schema.Table;

public class TableNotFoundException extends AppInABoxException {
	public TableNotFoundException(String message) {
		super(message);
	}
	public TableNotFoundException(Table table, String message) {
		super(table.toString() + " not found " + message);
	}
}
		
