package com.apptechnik.appinabox.engine.exceptions;

public class TokenException extends Exception {
    public TokenException(String message) {
        super(message);
    }
}
