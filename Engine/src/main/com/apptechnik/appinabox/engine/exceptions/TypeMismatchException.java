package com.apptechnik.appinabox.engine.exceptions;
import net.sf.jsqlparser.statement.create.table.ColDataType;

public class TypeMismatchException extends AppInABoxException {
	public TypeMismatchException(String message) {
		super(message);
	}

	public TypeMismatchException(String message, ColDataType a, ColDataType b) {
		super(message + " " + a.toString() + " != " + b.toString());
	}
}
		
