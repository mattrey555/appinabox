package com.apptechnik.appinabox.engine.exceptions;

public class TypeNotFoundException extends AppInABoxException {
	public TypeNotFoundException(String message) {
		super(message);
	}
	public TypeNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
}
		
