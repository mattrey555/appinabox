package com.apptechnik.appinabox.engine.exceptions;

import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.Statement;


public class UndefinedParameterException extends AppInABoxException {
	public UndefinedParameterException(String message) {
		super(message);
	}
}
		
