package com.apptechnik.appinabox.engine.exceptions;

import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.Statement;


// TODO: GET RID OF THIS.
public class UndefinedTableException extends TableNotFoundException {
	public UndefinedTableException(String message) {
		super(message);
	}

	public UndefinedTableException(Table table, Statement statement) {
		super("table " + table.toString() + " in " + statement.toString() + " not found");
	}

	public UndefinedTableException(Table table, String message) {
		super("table " + table.toString() + " not found: " + message);
	}
}
		
