package com.apptechnik.appinabox.engine.exceptions;

import java.util.jar.JarFile;

public class UnresolvedClassException extends AppInABoxException {
	public UnresolvedClassException(String message) {
		super(message);
	}

	public UnresolvedClassException(ClassNotFoundException cnfex, String entryName, JarFile jarFile) {
		this("threw a class not found exception, " + cnfex.getMessage() +
				"while trying to load class: " + entryName +
				"from jar file: " + jarFile.getName());
	}

	public UnresolvedClassException(Exception ex, String entryName, JarFile jarFile) {
		this("threw a class not found exception, " + ex.getMessage() +
				"while trying to load class: " + entryName +
				"from jar file: " + jarFile.getName());
	}
}
		
