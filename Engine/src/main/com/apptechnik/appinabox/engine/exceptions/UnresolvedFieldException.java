package com.apptechnik.appinabox.engine.exceptions;

import java.lang.reflect.Field;

public class UnresolvedFieldException extends AppInABoxException {
	public UnresolvedFieldException(String message) {
		super(message);
	}
	public UnresolvedFieldException(Class c, String fieldName) {
		this("field " + fieldName +
				" in class " + c.getName() +
				" is not public and does not have a public setter");
	}
}
		
