package com.apptechnik.appinabox.engine.exceptions;

import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.operators.relational.ItemsList;
import net.sf.jsqlparser.statement.Statement;


public class UnsupportedExpressionException extends AppInABoxException {
	public UnsupportedExpressionException(String message) {
		super(message);
	}

	public UnsupportedExpressionException(String message, Statement statement) {
		super(message + ": " + statement != null ? statement.toString() : "null");
	}

	public UnsupportedExpressionException(String message, Expression expression) {
		super(message + ": " + expression != null ? expression.toString() : "null");
	}

	public UnsupportedExpressionException(String message, ItemsList itemsList) {
		super(message + ": " + itemsList != null ? itemsList.toString() : "null");
	}
}
		
