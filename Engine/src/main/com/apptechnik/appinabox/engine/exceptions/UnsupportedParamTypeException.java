package com.apptechnik.appinabox.engine.exceptions;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.Type;
import java.util.List;

public class UnsupportedParamTypeException extends AppInABoxException {
	public UnsupportedParamTypeException(String message) {
		super(message);
	}
}
		
