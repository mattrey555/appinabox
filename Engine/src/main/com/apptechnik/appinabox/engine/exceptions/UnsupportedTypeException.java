package com.apptechnik.appinabox.engine.exceptions;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.Type;
import java.util.List;

public class UnsupportedTypeException extends AppInABoxException {
	public UnsupportedTypeException(String message) {
		super(message);
	}
	public UnsupportedTypeException(String fieldName, Class c, Type type) {
		this("field " + fieldName +
			" in class " + c +
			" has unsupported type " + type.getTypeName());
	}

	public UnsupportedTypeException(Field field, Type type) {
		this(field.getName(), field.getDeclaringClass(), type);
	}

	public UnsupportedTypeException(Method method, List<Class> argListTypes) {
		this("Method " + method.getName() + " in class " + method.getDeclaringClass() +
			 " cannot support multiple arguments " + argListToString(argListTypes));
	}

	public UnsupportedTypeException(Method method, Parameter param, List<Class> argListTypes) {
		this("Parameter " + param.getName() + " of type " + param.getType() +
			" in method " + method.getName() + " in class " + method.getDeclaringClass() +
			" cannot support multiple type arguments " + argListToString(argListTypes));
	}

	public UnsupportedTypeException(Class unsupportedType) {
		this("types of " + unsupportedType.getName() +  " are not supported");
	}

	public UnsupportedTypeException(Type unsupportedType) {
		this("types of " + unsupportedType.getTypeName() +  " are not supported");
	}
	private static String argListToString(List<Class> argList) {
		StringBuffer sb = new StringBuffer();
		for (Class c : argList) {
			sb.append(c.getName() + " ");
		}
		return sb.toString();
	}
}
		
