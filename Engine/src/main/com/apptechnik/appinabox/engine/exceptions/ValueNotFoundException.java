package com.apptechnik.appinabox.engine.exceptions;

public class ValueNotFoundException extends AppInABoxException {
	public ValueNotFoundException(String message) {
		super(message);
	}
}
		
