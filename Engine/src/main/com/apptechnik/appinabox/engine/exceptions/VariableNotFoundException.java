package com.apptechnik.appinabox.engine.exceptions;

public class VariableNotFoundException extends AppInABoxException {
	public VariableNotFoundException(String message) {
		super(message);
	}
}
		
