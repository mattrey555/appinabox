package com.apptechnik.appinabox.engine.exceptions;

public class XmlElementNotFoundException extends AppInABoxException {
	public XmlElementNotFoundException(String message) {
		super(message);
	}
}
		
