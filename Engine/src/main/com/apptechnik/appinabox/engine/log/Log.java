package com.apptechnik.appinabox.engine.log;

public class Log {
    public static final int ERROR = 0;
    public static final int DEBUG = 1;
    public static final int INFO = 2;
    public static final int WARN = 3;
    public static final int VERBOSE = 4;

    private static int level = DEBUG;

    public static void error(String s) {
        System.err.println(s);
    }

    public static void debug(String s) {
        if (level >= DEBUG) {
            System.out.println(s);
        }
    }

    public static void info(String s) {
        if (level >= INFO) {
            System.out.println(s);
        }
    }

    public static void warn(String s) {
        if (level >= WARN) {
            System.out.println(s);
        }
    }

    public static void verbose(String s) {
        if (level >= VERBOSE) {
            System.out.println(s);
        }
    }
}
