package com.apptechnik.appinabox.engine.parser;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeGen;
import com.apptechnik.appinabox.engine.codegen.pojos.FunctionDefinition;
import com.apptechnik.appinabox.engine.codegen.pojos.MemberDeclaration;
import com.apptechnik.appinabox.engine.codegen.pojos.Type;
import com.apptechnik.appinabox.engine.codegen.util.CodegenUtil;
import com.apptechnik.appinabox.engine.exceptions.AmbiguousColumnException;
import com.apptechnik.appinabox.engine.exceptions.ColumnNotFoundException;
import com.apptechnik.appinabox.engine.exceptions.TypeNotFoundException;
import com.apptechnik.appinabox.engine.exceptions.UnsupportedExpressionException;
import com.apptechnik.appinabox.engine.parser.pojos.ParameterMap;
import com.apptechnik.appinabox.engine.parser.query.SelectItemUtils;
import com.apptechnik.appinabox.engine.parser.types.SQLType;
import com.apptechnik.appinabox.engine.util.StatementUtil;
import com.apptechnik.appinabox.engine.util.StringUtil;
import net.sf.jsqlparser.expression.Alias;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.alter.AlterExpression;
import net.sf.jsqlparser.statement.create.table.ColDataType;
import net.sf.jsqlparser.statement.create.table.ColumnDefinition;
import net.sf.jsqlparser.statement.create.table.Index;
import net.sf.jsqlparser.statement.select.AllColumns;
import net.sf.jsqlparser.statement.select.OrderByElement;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;
import net.sf.jsqlparser.statement.select.SelectItem;

import java.util.*;
import java.util.stream.Collectors;

/**
 * PostGres specific: parse the column specification strings for NOT NULL and REFERENCES expressions
 */
public class ColumnInfo {
    private Alias alias;                            // alias
    private boolean isAliasSynthetic;               // alias was generated to differentiate columns
    private TableInfo containingTableInfo;          // table info back reference.
    private boolean notNull;
    private boolean autogenerate;
    private boolean primaryKey;
    private Column foreignKey;
    private ColumnDefinition columnDefinition;
    private List<Column> childReferenceList;        // list of child references. must have table and column specified
    private SelectItem selectItem;                  // SELECT item which refers to this column

    public ColumnInfo(TableInfo containingTableInfo, ColumnDefinition columnDefinition) {
        init(containingTableInfo, columnDefinition);
    }

    /**
     * Constructor used from ALTER TABLE expression
     * @param tableInfo
     * @param columnName
     * @param columnDataType
     */
    public ColumnInfo(TableInfo tableInfo, String columnName, AlterExpression.ColumnDataType columnDataType) {
        ColumnDefinition colDef =
            new ColumnDefinition(columnName, columnDataType.getColDataType(), columnDataType.getColumnSpecs());
        init(tableInfo, colDef);

    }

    public ColumnInfo(TableInfoMap tableInfoMap, Column column) throws AmbiguousColumnException, ColumnNotFoundException {
        TableInfo tableInfo = tableInfoMap.getTableInfo(column);
        ColumnDefinition colDef = tableInfo.getColumnDefinition(column.getName(true));
        init(tableInfo, colDef);
    }

    public void init(TableInfo containingTableInfo, ColumnDefinition columnDefinition) {
        //WORKAROUND: sometimes the column name can be quoted, because it conflicts with a SQL keyword,
        //so we strip the quotes here
        columnDefinition.setColumnName(StringUtil.stripQuotes(columnDefinition.getColumnName()));
        this.containingTableInfo = containingTableInfo;
        this.childReferenceList = new ArrayList<Column>();
        this.columnDefinition = columnDefinition;
        this.autogenerate = false;
        this.isAliasSynthetic = false;
        this.alias = null;
        massageColumnDefinition(this.columnDefinition);
        if (columnDefinition.getColumnSpecs() == null) {
            notNull = false;
            foreignKey = null;
            primaryKey = false;
        } else {
            processColumnSpecStrings(columnDefinition);
        }
    }

    /**
     * Copy a columnInfo, in case we have to alias it or something.
     * @param c columnInfo to copy from
     */
    public ColumnInfo(ColumnInfo c) {
        this.primaryKey = c.primaryKey;
        this.containingTableInfo = c.containingTableInfo;
        this.childReferenceList = c.childReferenceList;
        this.columnDefinition = c.columnDefinition;
        this.autogenerate = c.autogenerate;
        this.foreignKey = copyColumn(c.foreignKey);
        this.columnDefinition = copyColumnDefintion(c.columnDefinition);
        this.selectItem = c.selectItem;
        this.notNull = c.notNull;
        this.alias = copyAlias(c.getAlias());
        this.isAliasSynthetic = c.isAliasSynthetic;
    }

    /**
     * Copy a list of column infos, so we can do stuff like assign their tables to a table with an
     * alias.
     * @param columnInfoList list of columnInfo to copy.
     * @return
     */
    public static List<ColumnInfo> copyList(Collection<ColumnInfo> columnInfoList) {
        return columnInfoList.stream().map(c -> new ColumnInfo(c)).collect(Collectors.toList());
    }

    /**
     * Copy a column definition
     * @param c column definition to copy
     * @return copied column definition.
     */
    public ColumnDefinition copyColumnDefintion(ColumnDefinition c) {
        ColumnDefinition t = new ColumnDefinition();
        t.setColDataType(c.getColDataType());
        t.setColumnName(c.getColumnName());
        if (c.getColumnSpecs() != null) {
            t.setColumnSpecs(new ArrayList<String>(c.getColumnSpecs()));
        }
        return t;
    }

    /**
     * Copy a column to another column.
     * @param c column to copy name and table from.
     * @return column with name and table references.
     */
    private Column copyColumn(Column c) {
        if (c == null) {
            return null;
        }
        Column t = new Column();
        t.setColumnName(c.getColumnName());
        t.setTable(copyTable(c.getTable()));
        return t;
    }

    private Table copyTable(Table c) {
        Table t = new Table();
        t.setName(c.getName());
        t.setAlias(copyAlias(c.getAlias()));
        return t;
    }

    /**
     * Copy an alias (may be null)
     * @param c alias or null
     * @return alias copy or null
     */
    public Alias copyAlias(Alias c) {
        if (c == null) {
            return null;
        } else {
            Alias t = new Alias(c.getName());
            t.setUseAs(c.isUseAs());
            return t;
        }
    }

    /**
     * Create a column from a table name and a column name.
     * @param tableName table name.
     * @param columnName column name.
     * @return column.
     */
    public static Column createColumn(String tableName, String columnName) {
        Column column = new Column();
        column.setColumnName(columnName);
        Table table = new Table();
        table.setName(tableName);
        column.setTable(table);
        return column;
    }

    /**
     * process the SQL type qualifiers, suck as PRIMARY KEY,  NOT NULL, and REFERENCES
     * TODO: Handle all the type qualifiers
     * @param columnDefinition JSQLParser column definition from CREATE TABLE.
     */
    private void processColumnSpecStrings(ColumnDefinition columnDefinition) {

        // search for NOT NULL and REFERENCES column qualifiers.
        for (int i = 0; i < columnDefinition.getColumnSpecs().size(); i++) {
            String s = columnDefinition.getColumnSpecs().get(i);
            if (s.equals(Constants.SQLKeywords.TAG_NOT)) {
                if (i < columnDefinition.getColumnSpecs().size() - 1) {
                    String s2 = columnDefinition.getColumnSpecs().get(i + 1);
                    notNull = s2.equals(Constants.SQLKeywords.TAG_NULL);
                }
            }
            if (s.equals(Constants.SQLKeywords.TAG_PRIMARY)) {
                if (i < columnDefinition.getColumnSpecs().size() - 1) {
                    String s2 = columnDefinition.getColumnSpecs().get(i + 1);
                    primaryKey = s2.equals(Constants.SQLKeywords.TAG_KEY);
                }
            }

            // Postgres references of the form REFERENCES customers(customer_id)
            if (s.equals(Constants.SQLKeywords.TAG_REFERENCES)) {
                if (i < columnDefinition.getColumnSpecs().size() - 2) {
                    String referredTableName = columnDefinition.getColumnSpecs().get(i + 1);
                    String columnName = StringUtil.stripParentheses(columnDefinition.getColumnSpecs().get(i + 2));
                    foreignKey = createColumn(referredTableName, columnName);
                }
            }
        }
    }

    /**
     * Convert PostGres specific types to standard SQL types.
     */
    private void massageColumnDefinition(ColumnDefinition columnDefinition) {
        ColDataType colDataType = columnDefinition.getColDataType();
        if (colDataType.getDataType().equals(Constants.SQLKeywords.POSTGRES_SERIAL)) {
            colDataType.setDataType(Constants.SQLKeywords.SQLTYPE_INTEGER);
            autogenerate = true;
        }
        if (colDataType.getDataType().equals(Constants.SQLKeywords.SQLTYPE_TIMESTAMP)) {
            autogenerate = true;
        }
    }


    /**
     * We want to know the tables which have foreign key references to this table.
     * When we insert hierarchical table, we place all the values which we parse for
     * this table which might be referenced by other INSERT statements, and pre-populate
     * those insertions with the values which were read for this table, so if they don't
     * appear in the JSON, they're "inherited" from te parent table
     */
    public void resolveChildReferences(Table table, TableInfo childTableInfo) {
        for (ColumnInfo childColumnInfo : childTableInfo.getColumnInfoSet()) {
            Column foreignKey = childColumnInfo.getForeignKey();
            if (foreignKey != null) {
                if (foreignKey.getTable().getName().equals(table.getName()) &&
                    foreignKey.getColumnName().equals(columnDefinition.getColumnName())) {

                    // the foreign key references this table, but as a child reference
                    // we want it to reference the child table and column (reverse)
                    Column childKey = createColumn(childTableInfo.getTable().getName(),
                                                   childColumnInfo.getColumnName());
                    childReferenceList.add(childKey);
                }
            }
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("column: " + columnDefinition.getColumnName());
        if (alias != null) {
            sb.append(" as: " + alias.getName());
        }
        if (containingTableInfo != null) {
            sb.append(" table: " + containingTableInfo.getName());
        } else {
            sb.append(" unknown table");
        }
        sb.append(" simpleType: " + columnDefinition.getColDataType().getDataType());
        if ((columnDefinition.getColumnSpecs() != null) && !columnDefinition.getColumnSpecs().isEmpty()) {
            sb.append(" spec strings:");
            sb.append(StringUtil.concat(columnDefinition.getColumnSpecs(), " "));
        }
        sb.append(notNull ? " (not nullable) " : " (allows null) ");
        if (foreignKey != null) {
            sb.append("foreign key: " + foreignKey);
        }
        if (!childReferenceList.isEmpty()) {
            sb.append(" child references");
            for (Column childColumn : childReferenceList) {
                sb.append(childColumn + "\n");
            }
        }
        return sb.toString();
    }

    /**
     * Convert a list of column definitions to a list of column info
     *
     * @param columnDefinitionList list of column definitions.
     * @param tableInfo tableInfo used as backreference in ColumnInfo
     * @param columnDefinitionList
     * @return list of column info
     */
    public static List<ColumnInfo> createColumnInfoList(TableInfo tableInfo,
                                                        List<ColumnDefinition> columnDefinitionList) {
        return columnDefinitionList.stream()
                .map(columnDefinition -> new ColumnInfo(tableInfo, columnDefinition))
                .collect(Collectors.toList());
    }

    /**
     * Convert to a SelectExpressionItem, suitable for adding to the select item list.
     *
     * @param tableInfo table containing this columnInfo.
     * @return
     */
    public SelectExpressionItem toSelectExpressionItem(TableInfo tableInfo) {
        SelectExpressionItem newItem = new SelectExpressionItem();
        Column column = new Column();
        column.setColumnName(getColumnName());
        column.setTable(tableInfo.getTable());
        newItem.setExpression(column);
        return newItem;
    }

    /**
     * For a column which refers to an item in a SELECT call, return true if it is referenced in
     * its GROUP BY expression
     * @return true if it's a select item and referred to in the GROUP BY expression
     * @throws Exception thrown by the traverser.
     */
    public boolean isReferredToByAggregationFunction() throws Exception {
        if ((selectItem != null) && (selectItem instanceof SelectExpressionItem)) {
            Expression expression = ((SelectExpressionItem) selectItem).getExpression();
            SelectItemUtils.HasAggregationFunction aggregatedColumns = new SelectItemUtils.HasAggregationFunction();
            Traverse.traverseExpression(expression, aggregatedColumns);
            return aggregatedColumns.isHasAggregationFunction();
        }
        return false;
    }

    // TODO: this is unused.
    /**
     * convert a selectExpressionItem to a list of column infos.  Intended for use with
     * Postgres-specific INSERT/UPDATE RETURNING clause.
     * @param selectExpressionItem SELECT item (with optional alias.
     * @param tableInfoMap map of table names/aliases to table info
     * @param parameterMap used for getting the type of an expression when it references a NAMED_PARAMETER
     * @return list of columns in the select item expression
     * @throws Exception failed to find a column.
     */
    public static List<ColumnInfo> fromSelectExpressionItem(SelectExpressionItem selectExpressionItem,
                                                            Statement statement,
                                                            ParameterMap parameterMap,
                                                            TableInfoMap tableInfoMap) throws Exception {
        Alias alias = selectExpressionItem.getAlias();
        if (selectExpressionItem.getExpression() instanceof Column) {
            return Collections.singletonList(tableInfoMap.getColumnInfo((Column) selectExpressionItem.getExpression()));
        } else if (selectExpressionItem.getExpression() instanceof AllColumns) {
            return tableInfoMap.getColumnInfoList(StatementUtil.getColumns(statement));
        } else if (alias != null) {
            String aliasName = alias.getName();
            TableInfo tableInfo = tableInfoMap.getTableInfo(aliasName);
            ColDataType colDataType = ExpressionType.getType(selectExpressionItem.getExpression(), parameterMap, tableInfoMap);
            ColumnDefinition colDef = new ColumnDefinition();
            colDef.setColumnName(aliasName);
            colDef.setColDataType(colDataType);
            return Collections.singletonList(new ColumnInfo(tableInfo, colDef));
        } else {
            throw new UnsupportedExpressionException("converting SelectExpressionItem to columnInfoList", statement);
        }
    }

    /**
     * From a table map and a parameter map, create a column from a select expression item.
     * Used in subqueries when we create synthetic tables.
     * @param tableInfoMap map of statements to referenced tables.
     * @param parameterMap map of statements to named parameters.
     * @param selectExpressionItem select expression to find the type from
     * @return column info.
     * @throws Exception
     */
    public static ColumnInfo fromSelectExpressionItem(TableInfoMap tableInfoMap,
                                                      ParameterMap parameterMap,
                                                      SelectExpressionItem selectExpressionItem)
            throws Exception {
        Expression expr = selectExpressionItem.getExpression();
        ColDataType colDataType = ExpressionType.getType(expr, parameterMap, tableInfoMap);
        if (colDataType == null) {
            throw new UnsupportedExpressionException(expr.toString() + " could not be resolved to a type");
        }
        List<ColumnInfo> columnInfoList = ColumnInfo.resolveColumns(expr, tableInfoMap);
        if (selectExpressionItem.getAlias() != null) {
            ColumnDefinition colDef = new ColumnDefinition();
            colDef.setColumnName(selectExpressionItem.getAlias().getName());
            colDef.setColDataType(colDataType);
            TableInfo tableInfo = columnInfoList.get(0).getContainingTableInfo();
            return new ColumnInfo(tableInfo, colDef);
        } else if (columnInfoList.size() == 1) {
            return columnInfoList.get(0);
        } else {
            throw new UnsupportedExpressionException(selectExpressionItem + " has multiple columns and no alias");
        }
    }

    /**
     * Traverse an expression and resolve it to the list of columns
     */
    public static class ColumnTraverse {
        protected List<ColumnInfo> columnInfos;
        protected final TableInfoMap subqueryTableInfoMap;

        public ColumnTraverse(TableInfoMap subqueryTableInfoMap) {
            this.columnInfos = new ArrayList<ColumnInfo>();
            this.subqueryTableInfoMap = subqueryTableInfoMap;
        }

        public List<ColumnInfo> getColumnInfos() {
            return columnInfos;
        }
    }

    public static class ResolveColumns extends ColumnTraverse implements Traverse.StackFunction<Expression, Object> {
        public ResolveColumns(TableInfoMap subqueryTableInfoMap) {
            super(subqueryTableInfoMap);
        }

        /**
         * Consumer method which finds the columnInfo associated with this column.
         * @param stack stack of expressions recursed to this point.
         * @param expr expression at this recursion point
         * @return expr or null;
         */
        public Object apply(Stack<Expression> stack, Expression expr) throws Exception {
            if (expr instanceof Column) {
                ColumnInfo columnInfo = findColumnInfo(subqueryTableInfoMap, (Column) expr);
                if (columnInfo != null) {
                    columnInfos.add(columnInfo);
                    return expr;
                } else {
                    throw new ColumnNotFoundException("column " + expr + " not found in sub query map");
                }
            }
            return null;
        }

    }
    /**
     * Exposed method to return the list of ColumnInfo referred to by this expression.
     * @param expr
     * @param tableInfoMap
     * @return
     * @throws Exception
     */
    public static List<ColumnInfo> resolveColumns(Expression expr,
                                                  TableInfoMap tableInfoMap) throws Exception {
        ResolveColumns resolveColumns = new ResolveColumns(tableInfoMap);
        Traverse.traverseExpression(expr, resolveColumns);
        return resolveColumns.getColumnInfos();
    }

    /**
     * Retrieve the columnInfo associated with this column in the map of tables.
     * @param tableInfoMap map of statements to tables.
     * @param column column to search for
     * @return columnInfo or null.
     * @throws ColumnNotFoundException column could not be found or was ambiguous
     * @throws AmbiguousColumnException or was ambiguous
     */
    public static ColumnInfo findColumnInfo(TableInfoMap tableInfoMap, Column column)
    throws AmbiguousColumnException, ColumnNotFoundException {
        TableInfo tableInfo = tableInfoMap.getTableInfo(column);
        if (tableInfo != null) {
            return tableInfo.getColumnInfo((column).getColumnName());
        }
        return null;
    }

    /**
     * Create the java field declaration for a select item and associated SQL column.
     * @return java field declaration as string,
     * @throws TypeNotFoundException the type specified in the columnInfo couldn't be found.
     */
    public MemberDeclaration fieldDeclaration(CodeGen codegen, String accessModifier) throws TypeNotFoundException {
        return CodegenUtil.memberDeclarationFromSQLType(codegen, getColumnName(true),
                                                        getDataType(), !isNotNull(), accessModifier);
    }

    // TODO: Unused.
    public FunctionDefinition setterDefinition(CodeGen codegen) throws TypeNotFoundException {
        return CodegenUtil.setterDefinition(codegen, getColumnName(true),
                                                        getDataType(), !isNotNull());
    }

    public FunctionDefinition accessorDefinition(CodeGen codegen) throws TypeNotFoundException  {
        return CodegenUtil.accessorDeclarationFromSQLType(codegen, getColumnName(true),
                                                          getDataType(), !isNotNull());
    }

    /**
     * does name match a column the column list either by colum name or alias
     * @param name
     * @param columnInfoList
     */
    public static ColumnInfo matchColumn(String name, List<ColumnInfo> columnInfoList) {
        return columnInfoList.stream()
                .filter(col -> col.getColumnName(true).equals(name))
                .findFirst()
                .orElse(null);
    }

    /**
     * Filter the list of columns in this table.
     * @param columnInfoList list of columns
     * @param tableInfo tableInfo to filter on.
     * @return
     */
    public static boolean inTable(List<ColumnInfo> columnInfoList, TableInfo tableInfo) {
        return columnInfoList.stream()
                .anyMatch(columnInfo -> columnInfo.getContainingTableInfo().equals(tableInfo));
    }

    /**
     * Retreive the indices associate with these columns.
     * 1) get the unique set of tables containing the columns
     * 2) filter them by non-null indexLists
     * 3) concatenate the index lists.
     * @param columnInfoList
     * @return
     */
    public static List<Index> getIndexList(Collection<ColumnInfo> columnInfoList) {
        return columnInfoList.stream()
                .map(col -> col.getContainingTableInfo())
                .collect(Collectors.toSet()).stream()
                .filter(table -> table.getIndexList() != null)
                .map(table -> table.getIndexList())
                .flatMap(Collection::stream)
                .toList();
    }

    /**
     * Is the table referenced in any of the columnInfos in this list? (used for finding if we already
     * have a sort key, and don't need to add a primary key on that table.
     * @param tableInfo
     * @param columnInfoList
     * @return
     */
    public static boolean tableInList(TableInfo tableInfo, List<ColumnInfo> columnInfoList) {
        return columnInfoList.stream().anyMatch(col -> col.getContainingTableInfo().equals(tableInfo));
    }

    /**
     * Filter the list of column info
     * @param tableInfo
     * @param columnInfoList
     * @return
     */
    public static List<ColumnInfo> filterByTableInfo(TableInfo tableInfo, List<ColumnInfo> columnInfoList) {
        return columnInfoList.stream()
                .filter(col -> col.getContainingTableInfo().equals(tableInfo))
                .collect(Collectors.toList());
    }

    public Alias getAlias() {
        return alias;
    }

    public String getAliasName() { return (alias != null) ? alias.getName() : null; }

    public void setAlias(Alias alias) {
        this.alias = alias;
    }

    // TODO: unused.
    /**
     * We often set the alias from a selectExpressionItem alias.
     * @param selectItem
     */
    public boolean setIfAlias(SelectItem selectItem) {
        if (selectItem instanceof SelectExpressionItem) {
            SelectExpressionItem selectExpressionItem = (SelectExpressionItem) selectItem;
            if (selectExpressionItem.getAlias() != null) {
                setAlias(selectExpressionItem.getAlias());
                return true;
            }
        }
        return false;
    }

    public boolean sameTable(ColumnInfo columnInfo) {
        return containingTableInfo.equals(columnInfo.getContainingTableInfo());
    }
    /**
     * Convert to a Column
     *
     * @return
     */
    public Column toColumn() {
        return new Column(new Table(getTableName()), getColumnName());
    }

    public Column toColumn(boolean allowAlias) {
        if (allowAlias && alias != null) {
            return new Column(alias.getName());
        } else {
            return toColumn();
        }
    }

    public SelectExpressionItem toSelectExpressionItem() {
        SelectExpressionItem selectExpressionItem = new SelectExpressionItem();
        selectExpressionItem.setExpression(toColumn(false));
        if (alias != null) {
            selectExpressionItem.setAlias(alias);
        }
        return selectExpressionItem;
    }

    /**
     * Use the alias here, since the tables will be aliased.
     * @return
     */
    public OrderByElement toOrderByElement() {
        OrderByElement o = new OrderByElement();
        o.setExpression(toColumn(true));
        return o;
    }

    public boolean equalsColumn(Column col) {
        if ((col.getTable() != null) && (getTableName() != null)) {
            if (col.getTable().getAlias() != null) {
                if (getTableName().equals(col.getTable().getAlias().getName())) {
                    return false;
                }
            }
            if (getTableName().equals(col.getTable().getName())) {
                return false;
            }
        }
        if (alias != null) {
            return alias.getName().equals(col.getName(true));
        } else {
            return columnDefinition.getColumnName().equals(col.getName(true));
        }
    }

    /**
     * Does this match a single-column expression?
     * @param a hopefully a column
     * @return if matches a as a column, false otherwise.
     */
    public boolean equalsColumn(Expression a) {
        if (a instanceof Column) {
            return equalsColumn((Column) a);
        }
        return false;
    }

    public boolean nameConflict(Collection<ColumnInfo> columnInfoList) {
        return columnInfoList.stream().anyMatch(col -> this.nameConflict(col));
    }

    public ColumnInfo getNameConflict(Collection<ColumnInfo> columnInfoList) {
        return columnInfoList.stream().filter(col -> this.nameConflict(col)).findFirst().orElse(null);
    }

    public static boolean nameConflict(String name, Collection<ColumnInfo> columnInfoList) {
        return columnInfoList.stream().anyMatch(col -> col.nameConflict(name));
    }


    /**
     * Does this column conflict with b by column name or alias?
     * @param b
     * @return true if there is conflict. false if there is peace.
     */
    public boolean nameConflict(ColumnInfo b) {
        return nameConflict(b.getColumnName(true));
    }

    public boolean nameConflict(String name) {
        return getColumnName(true).equals(name);
    }

    public MemberDeclaration toFieldDeclaration(CodeGen codegen) throws TypeNotFoundException {
        return toFieldDeclaration(codegen, null);
    }

    /**
     * Generate a field declaration from the column type.
     * @param suffix suffixed used for disambiguation;
     * @return {@code public <type> <column-name[suffix]>}
     * @throws TypeNotFoundException
     */
    public MemberDeclaration toFieldDeclaration(CodeGen codegen, String suffix) throws TypeNotFoundException {
        String varName = (suffix != null) ? getColumnName(true) + suffix : getColumnName(true);
        return codegen.memberDecl(Constants.JavaKeywords.PUBLIC, false,
                                  codegen.typeFromSQLType(getDataType(), !isNotNull()), codegen.var(varName));
    }

    public String getDataType() {
        return getColumnDefinition().getColDataType().getDataType();
    }

    public boolean inChildReferenceList(Table table) {
        return childReferenceList.stream()
                .anyMatch(candColumn -> candColumn.getTable().getName().equals(table.getName()));
    }

    public ColDataType getColDataType() {
        return getColumnDefinition().getColDataType();
    }

    public boolean isNotNull() {
        return notNull;
    }

    public Column getForeignKey() {
        return foreignKey;
    }

    public ColumnDefinition getColumnDefinition() {
        return columnDefinition;
    }

    /**
     * Get the Java type (int/float/etc) for this column's type.
     * @param isNullable return boxed type.
     * @return Java Type
     * @throws TypeNotFoundException
     */
    public Type getCodeType(CodeGen codegen, boolean isNullable) throws TypeNotFoundException {
        return codegen.typeFromSQLType(getColumnDefinition().getColDataType().getDataType(), isNullable);
    }

    public String getColumnName() {
        return columnDefinition.getColumnName();
    }

    public String getTablePrefixedName() {
        return getTableName() + "_" + getColumnName(true);
    }

    /**
     * EXPLICITLY DO NOT RETURN THE TABLE QUALIFIER.  This is intended to be used in code generation,
     * where the resultSet field name does not allow table qualifiers.  Use elsewhere at your peril.
     * @param allowAlias
     * @return
     */
    public String getColumnName(boolean allowAlias) {
        return (allowAlias && alias != null) ? alias.getName() : columnDefinition.getColumnName();
    }

    /**
     * Used to rename order-by keys, which are columns, but are not aliased, when we apply
     * an alias to a corresponding select item.
     * @param name
     */
    public void setColumnName(String name) {
        columnDefinition.setColumnName(name);
    }

    public TableInfo getContainingTableInfo() {
        return containingTableInfo;
    }

    public String getTableName() {
        return containingTableInfo.getName();
    }

    public String getColumnSQLTypeString() {
        return columnDefinition.getColDataType().getDataType();
    }

    /**
     * Used in setNamedParameter() to get the parameter type.
     * @return
     * @throws TypeNotFoundException
     */
    public int getColumnSQLType() throws TypeNotFoundException {
        return SQLType.getJavaSqlTypeFromString(getColumnSQLTypeString());
    }

    public boolean isAutogenerate() {
        return autogenerate;
    }

    public boolean isPrimaryKey() {
        return primaryKey;
    }

    public void setContainingTableInfo(TableInfo containingTableInfo) {
        this.containingTableInfo = containingTableInfo;
    }

    public SelectItem getSelectItem() {
        return selectItem;
    }

    public void setSelectItem(SelectItem selectItem) {
        this.selectItem = selectItem;
    }

    public void setNotNull(boolean notNull) {
        this.notNull = notNull;
    }

    public void setAutogenerate(boolean autogenerate) {
        this.autogenerate = autogenerate;
    }

    public void setPrimaryKey(boolean primaryKey) {
        this.primaryKey = primaryKey;
    }

    public void setForeignKey(Column foreignKey) {
        this.foreignKey = foreignKey;
    }

    public void setColumnDefinition(ColumnDefinition columnDefinition) {
        this.columnDefinition = columnDefinition;
    }

    public void setChildReferenceList(List<Column> childReferenceList) {
        this.childReferenceList = childReferenceList;
    }

    public boolean isAliasSynthetic() {
        return isAliasSynthetic;
    }

    public void setAliasSynthetic(boolean aliasSynthetic) {
        isAliasSynthetic = aliasSynthetic;
    }


    @Override
    public boolean equals(Object o) {
        if (o instanceof ColumnInfo) {
            ColumnInfo a = (ColumnInfo) o;
            if ((alias != null) && (a.alias != null)) {
                return a.alias.getName().equals(alias.getName());
            }
            if ((alias == null) && (a.alias == null)) {
                if (!TableInfo.sameTableInfo(containingTableInfo, a.containingTableInfo)) {
                    return false;
                }
                return getColumnName().equals(a.getColumnName());
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hashCode = 1;
        if (alias != null) {
            hashCode += alias.getName().hashCode();
        }
        hashCode += containingTableInfo.getTable().getName().hashCode();
        hashCode += getColumnName().hashCode();
        return hashCode;
    }
}
