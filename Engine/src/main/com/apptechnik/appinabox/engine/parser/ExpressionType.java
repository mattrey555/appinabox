package com.apptechnik.appinabox.engine.parser;

import java.util.List;
import java.util.ArrayList;
import java.util.Stack;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.log.Log;
import com.apptechnik.appinabox.engine.parser.query.SelectTableUtils;
import com.apptechnik.appinabox.engine.parser.util.FunctionType;
import com.apptechnik.appinabox.engine.exceptions.*;
import com.apptechnik.appinabox.engine.parser.util.ColDataTypeUtils;
import com.apptechnik.appinabox.engine.parser.pojos.ParameterMap;
import com.apptechnik.appinabox.engine.util.StringUtil;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.expression.*;
import net.sf.jsqlparser.expression.operators.arithmetic.*;
import net.sf.jsqlparser.expression.operators.relational.*;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.create.table.ColDataType;
import net.sf.jsqlparser.statement.delete.Delete;
import net.sf.jsqlparser.statement.insert.Insert;
import net.sf.jsqlparser.statement.select.*;
import net.sf.jsqlparser.statement.update.Update;

/**
 * TYPE INFERENCE FOR SQL.
 */
public class ExpressionType {
	
	public ExpressionType() {
	}

	/**
	 * Traverse a SELECT statement, and generate a map of JDBC Named Parameters to their inferred tyoes.
	 * @param select select statemen (must be PlainSelect)
	 * @param selectTableInfoMap tables filtered by SELECT call
	 * @param createTableInfoMap tables from CREATE TABLE statements
	 * @return
	 * @throws Exception
	 */
	public static ParameterMap getNamedParameterTypes(Select select,
													  TableInfoMap selectTableInfoMap,
													  TableInfoMap createTableInfoMap) throws Exception {
		ParameterMap paramMap = new ParameterMap();
		if (select.getSelectBody() instanceof PlainSelect) {
			PlainSelect plainSelect = (PlainSelect) select.getSelectBody();
			ExpressionType.getNamedParameterTypes(plainSelect, paramMap, selectTableInfoMap, createTableInfoMap);
		} else {
			throw new UnsupportedExpressionException("trying to get named parameters", select);
		}
		return paramMap;
	}
	/**
	 * Given an Insert, Update, Delete, or Select statement, create the map of parameters to the expressions,
	 * columns, and types that they are associated with.  For example:
	 * Select * from t where customer_id = :id
	 * will create a parameter map where "id" is mapped to the column customer_id with its type, and table t.
	 * @param statement  Insert, Update, Delete, or Select statement
	 * @param statementTableInfoMap map of table names to table information.
	 * @return parameter map.
	 * @throws Exception
	 */
	public static ParameterMap getNamedParameterTypes(Statement statement,
													  TableInfoMap statementTableInfoMap,
													  TableInfoMap createTableInfoMap) throws Exception {
		ParameterMap paramMap = new ParameterMap();
		if (statement instanceof Update) {
			Update update = (Update) statement;
			TableInfo tableInfo = statementTableInfoMap.getTableInfo(update.getTable());
			ExpressionType.getNamedParameterTypes(update.getColumns(), update.getExpressions(), paramMap, tableInfo);
			ExpressionType.getNamedParameterTypes(ParameterMap.ParamSource.QUERY, update.getWhere(), paramMap, tableInfo, ColDataTypeUtils.BOOLEAN);
		} else if (statement instanceof Insert) {
			Insert insert = (Insert) statement;
			Table insertTable = insert.getTable();
			TableInfo tableInfo = statementTableInfoMap.getTableInfo(insertTable);
			if (insert.getColumns() == null) {
				Log.warn("the INSERT statement: " + insert + " contained no VALUES");
			} else {
				ExpressionType.getNamedParameterTypes(insert.getColumns(), ((ExpressionList) insert.getItemsList()).getExpressions(),
													  paramMap, tableInfo);
			}
		} else if (statement instanceof Delete) {
			Delete delete = (Delete) statement;
			Table deleteTable = delete.getTable();
			TableInfo tableInfo = statementTableInfoMap.getTableInfo(deleteTable);
			ExpressionType.getNamedParameterTypes(ParameterMap.ParamSource.QUERY, delete.getWhere(), paramMap, tableInfo, ColDataTypeUtils.BOOLEAN);
		} else if (statement instanceof Select) {
			Select select = (Select) statement;
			if (select.getSelectBody() instanceof PlainSelect) {
				PlainSelect plainSelect = (PlainSelect) select.getSelectBody();
				ExpressionType.getNamedParameterTypes(plainSelect, paramMap, statementTableInfoMap, createTableInfoMap);
			} else {
				throw new UnsupportedExpressionException("trying to get named parameters", statement);
			}
		} else {
			throw new UnsupportedExpressionException("trying to get named parameters", statement);
		}
		return paramMap;
	}

	/**
	 * In a SQL expression of the form:
	 * UPDATE table SET column1 = expression1, column2 = expression2 WHERE expression
	 * get the named parameter types from the SET expressions, use the table name
	 * from the table info to set the column table name.
	 * @param columnList list of columns to get the types from
	 * @param expressionList list of expressions to traverse for JBDC Named Paramters.
	 * @param paramMap parameter map to populate
	 * @param tableInfo source table info
	 */
	public static void getNamedParameterTypes(List<Column> columnList, 
											  List<Expression> expressionList, 
											  ParameterMap paramMap,
											  TableInfo tableInfo) throws Exception {
		if (columnList.size() != expressionList.size()) {
			throw new SQLVerifyException("column list " + StringUtil.concat(columnList, ", ") +
									     " mismatch number of elements with " + StringUtil.concat(expressionList, ", "));
		}

		for (int i = 0; i < columnList.size(); i++) {
			Column column = columnList.get(i);
			Expression expression = expressionList.get(i);

			// corresponding column is read by name from the table info.
			// NOTE: should this be aliased?
			ColDataType colDataType = tableInfo.getColDataType(column.getName(false));
			if (colDataType == null) {
				throw new NameNotFoundException("unable to find  column: " + column.getName(false) +
												" in table: " + tableInfo);
			}
			ExpressionType.getNamedParameterTypes(ParameterMap.ParamSource.VALUE, expression, paramMap, tableInfo, colDataType);
		}
	}

	/**
	 * Get the parameter map for a Plain Select statement.
	 * Recurse through the FROM item, JOINs, LIMIT and WHERE expressions
	 * @param paramMap resulting parameter map
	 * @param selectTableInfoMap map of table names to table column definitions filtered for THIS select statement
	 */
	public static void getNamedParameterTypes(PlainSelect plainSelect,
											  ParameterMap paramMap,
										      TableInfoMap selectTableInfoMap,
											  TableInfoMap createTableInfoMap)
		throws Exception {
		getNamedParameterTypes(plainSelect.getFromItem(), paramMap, selectTableInfoMap, createTableInfoMap);
		if (plainSelect.getJoins() != null) {
			for (Join join : plainSelect.getJoins()) {
				getNamedParameterTypes(join.getRightItem(), paramMap, selectTableInfoMap, createTableInfoMap);
			}
		}
		if (plainSelect.getLimit() != null) {
			if (plainSelect.getLimit().getOffset() instanceof JdbcNamedParameter) {
				JdbcNamedParameter param = (JdbcNamedParameter) plainSelect.getLimit().getOffset();
				paramMap.put(ParameterMap.ParamSource.QUERY, param, param, ColDataTypeUtils.INTEGER);
			}
			if (plainSelect.getLimit().getRowCount() instanceof JdbcNamedParameter) {
				JdbcNamedParameter param = (JdbcNamedParameter) plainSelect.getLimit().getRowCount();
				paramMap.put(ParameterMap.ParamSource.QUERY, param, param, ColDataTypeUtils.INTEGER);
			}
		}

		// where expressions must result in boolean type.
		getNamedParameterTypes(ParameterMap.ParamSource.QUERY, plainSelect.getWhere(), paramMap, selectTableInfoMap, ColDataTypeUtils.BOOLEAN);
	}


	/**
	 * Get the parameter map for the FROM clause in a SELECT statement
	 */
	private static void getNamedParameterTypes(FromItem fromItem, 
											   ParameterMap paramMap,
											   TableInfoMap selectTableInfoMap,
											   TableInfoMap createTableInfoMap)
		throws Exception {
		if (fromItem instanceof SubSelect) {
			SubSelect subSelect = (SubSelect) fromItem;
			SelectBody selectBody = subSelect.getSelectBody();
			if (selectBody instanceof PlainSelect) {
				PlainSelect plainSelect = (PlainSelect) selectBody;
				TableInfoMap subSelectTableInfoMap = SelectTableUtils.selectTables(plainSelect, createTableInfoMap);
				getNamedParameterTypes(plainSelect, paramMap, subSelectTableInfoMap, createTableInfoMap);
			} else {
				throw new UnsupportedExpressionException("expression " + selectBody + " is not a plain select statement");
			}
		} else if (fromItem instanceof SubJoin) {
			SubJoin subJoin = (SubJoin) fromItem;
			getNamedParameterTypes(fromItem, paramMap, selectTableInfoMap, createTableInfoMap);
			for (Join join : subJoin.getJoinList()) {
				getNamedParameterTypes(join.getRightItem(), paramMap, selectTableInfoMap, createTableInfoMap);
			}
		}
	}

	/** we want to derive the types for the named Jdbc parameters in an expression
	 * TODO: clean this up
	 * by determining the type of the containing expression.
	 * so for example:
	 * (:namedVar &lt; 1.0), namedVar is a double
	 * (:namedVar like 'name%') namedVar is a string.
	 * @param expr
	 */
	public static void getNamedParameterTypes(ParameterMap.ParamSource source,
											  Expression expr,
											  ParameterMap paramMap,
											  TableInfoMap tableInfoMap,
											  ColDataType expectedColDataType) throws Exception {
		Traverse.traverseExpression(expr, expectedColDataType, new NamedParameterTypeFunction(source, paramMap, tableInfoMap));
	}


	public static void getNamedParameterTypes(ParameterMap.ParamSource source,
											  Expression expr,
											  ParameterMap paramMap,
											  TableInfo tableInfo,
											  ColDataType expectedColDataType) throws Exception {
		TableInfoMap tableInfoMap = new TableInfoMap(tableInfo);
		Traverse.traverseExpression(expr, expectedColDataType, new NamedParameterTypeFunction(source, paramMap, tableInfoMap));
	}


	/**
	 * We try to get the type of each of the parameters.  When we encounter an expression, the values of the
	 * variables and constants dictate at least a set of values for other variables and constants, also, the
	 * allowed types for each SQL operator and function, for example, SUBSTRING takes a strinng and two integers
	 * for its arguments.  + make take any numeric type (int, long, short, etc).
	 * There are functions, such as CAST, or the :: operator which the variable type can be anything, so it
	 * has to be defaulted as a string, with an optionally specified override.
	 * This can be executed as part of a query, or in a VALUES clause in an INSERT or UPDATE statement.
	 * In the INSERT/UPDATE case, the constraint type is the corresponding column, in the SELECT/WHERE
	 * cause, it's bool.
	 * We can return a set of candidate types when we don't know, and specify which named parameter needs to
	 * have its type defined.
	 */


	private static class NamedParameterTypeFunction implements Traverse.StackFunctionParam<Expression, ColDataType, ColDataType> {
		private ParameterMap parameterMap;
		private TableInfoMap tableInfoMap;
		private ParameterMap.ParamSource source;
		private List<ExpressionTypeFailure> failedSubExpressionList;

		public NamedParameterTypeFunction(ParameterMap.ParamSource source,
										  ParameterMap parameterMap,
										  TableInfoMap tableInfoMap) {
			this.parameterMap = parameterMap;
			this.tableInfoMap = tableInfoMap;
			this.source = source;
			this.failedSubExpressionList = new ArrayList<ExpressionTypeFailure>();
		}

		private void addFail(Expression expr,
							 Expression offender,
							 ColDataType expectedColDataType,
							 ColDataType resultingColDataType) {
			this.failedSubExpressionList.add(new ExpressionTypeFailure(expr, offender, expectedColDataType, resultingColDataType));
		}

		/**
		 * The type of a SQL parameter can be inferred by the containing expression  a SQL parameter
		 *(and optionally, the expected result type of that expression
		 * @param callStack
		 * @param expr
		 * @param expectedColDataType
		 * @return
		 * @throws Exception
		 */
		@Override
		public ColDataType apply(Stack<Expression> callStack,
								 Expression expr,
								 ColDataType expectedColDataType) throws Exception {
			if (expr instanceof JdbcNamedParameter) {
				JdbcNamedParameter namedParam = (JdbcNamedParameter) expr;

				// don't try to resolve it if it's already been resolved.
				// TODO: actually resolve it, check type compatibility, and promote the registered type if necessary
				ColDataType paramDataType = parameterMap.findColDataType(namedParam.getName());
				if ((paramDataType == null) || paramDataType.getDataType().equals(Constants.SQLTypes.NAMED_PARAMETER)) {
					// if the expression is just named parameter, the returned type is the expected type.
					if (callStack.isEmpty()) {
						parameterMap.put(this.source, namedParam, expr, expectedColDataType);
					} else {

						// the relationship to the parent expression relative to the expected data type is the return type.
						Expression parentExpr = callStack.peek();
						if (ColDataTypeUtils.isBinaryOperator(parentExpr)) {
							ColDataType resultingDataType = resolveBinaryExpression((BinaryExpression) parentExpr, parameterMap, expectedColDataType);
							parameterMap.put(this.source, namedParam, parentExpr, resultingDataType);
						} else if (parentExpr instanceof InExpression) {
							resolveInExpression((InExpression) parentExpr, namedParam, parameterMap, tableInfoMap);
						} else if (parentExpr instanceof Parenthesis) {
							parameterMap.put(this.source, namedParam, expr, expectedColDataType);
							return expectedColDataType;
						} else if (parentExpr instanceof CastExpression) {
							// cast expression does not apply any restrictions on input type, so we leave the type as
							// NAMED_PARAMETER
							parameterMap.put(this.source, namedParam, parentExpr, ColDataTypeUtils.NAMED_PARAMETER);
						} else if (parentExpr instanceof Function) {
							Function function = (Function) parentExpr;
						} else {
							throw new UnsupportedExpressionException("parent expression: " + parentExpr + " not supported");
						}
					}
				}
			} else if (expr instanceof Column) {
				return tableInfoMap.getColumnInfo((Column) expr).getColDataType();
			}
			return null;
		}

		private ColDataType resolveBinaryExpression(BinaryExpression binExpr,
												    ParameterMap parameterMap,
												    ColDataType expectedColDataType) throws Exception {

			ColDataType leftType = getType(binExpr.getLeftExpression(), parameterMap, tableInfoMap);
			ColDataType rightType = getType(binExpr.getRightExpression(), parameterMap, tableInfoMap);
			return ColDataTypeUtils.inferBinaryOperatorArgumentType(binExpr, leftType, rightType);
		}

		/**
		 * SQL expression <expr> IN (<expr>,<expr>,...>
		 * An IN expression returns a boolean type, but sets no constraints on its left and right types, other than
		 * that the resulting type from the left hand expression must be compatible with the types in the right hand
		 * expression list.  Although the JSQL parser returns a list of expressions for the left-hand compoonent,
		 * we treat it as a single expression, and throw an exception otherwise.
		 * @param inExpr
		 * @return
		 * @throws Exception
		 */
		private boolean resolveInExpression(InExpression inExpr,
										 	JdbcNamedParameter param,
										 	ParameterMap parameterMap,
										 	TableInfoMap tableInfoMap)
			throws Exception {

			if (!(inExpr.getLeftExpression() instanceof Expression)) {
				throw new UnsupportedExpressionException("left side of IN expression must be an expression ", inExpr.getLeftExpression());
			}
			ColDataType leftExpressionType = getType(inExpr.getLeftExpression(), parameterMap, tableInfoMap);

			if (!(inExpr.getRightItemsList() instanceof ExpressionList)) {
				throw new UnsupportedExpressionException("right side of IN expression must be an expression list ", inExpr.getRightItemsList());
			}
			ExpressionList rightExpressionList = (ExpressionList) inExpr.getRightItemsList();
			ColDataType rightExpressionType = null;

			// hopefully, we can find an expression which can be fully resolved.
			for (Expression rightExpression : rightExpressionList.getExpressions()) {
				if (rightExpression != param) {
					ColDataType exprType = getType(rightExpression, parameterMap, tableInfoMap);
					if (!exprType.getDataType().equals(Constants.SQLTypes.NAMED_PARAMETER)) {
						if (rightExpressionType != null) {
							rightExpressionType = ColDataTypeUtils.promoteDataType(rightExpressionType, exprType);
						} else {
							rightExpressionType = exprType;
						}
					}
				}
			}
			if ((leftExpressionType != null) && !leftExpressionType.getDataType().equals(Constants.SQLTypes.NAMED_PARAMETER)) {
				this.parameterMap.put(source, param, inExpr, leftExpressionType, true);
				return true;
			} else if ((rightExpressionType != null) && !rightExpressionType.getDataType().equals(Constants.SQLTypes.NAMED_PARAMETER)) {
				this.parameterMap.put(source, param, inExpr, rightExpressionType, true);
				return true;
			}
			return false;
		}
	}

	private static class ExpressionTypeFailure {
		private final Expression expr;
		private final Expression offender;
		private final ColDataType expectedType;
		private final ColDataType returnedType;

		public ExpressionTypeFailure(Expression expr, Expression offender, ColDataType expectedType, ColDataType returnedType) {
			this.expr = expr;
			this.offender = offender;
			this.expectedType = expectedType;
			this.returnedType = returnedType;
		}

		public Expression getExpr() {
			return expr;
		}

		public Expression getOffender() {
			return offender;
		}

		public ColDataType getExpectedType() {
			return expectedType;
		}

		public ColDataType getReturnedType() {
			return returnedType;
		}
	}


	public static ColDataType getType(List<Expression> exprList, ParameterMap parameterMap, TableInfoMap tableInfoMap)
		throws TypeMismatchException, AmbiguousColumnException, UndefinedTableException,
			   ColumnNotFoundException, UnsupportedExpressionException, FunctionNotFoundException {
		if (!exprList.isEmpty()) {
			ColDataType colDataType = getType(exprList.get(0), parameterMap, tableInfoMap);
			for (int i = 1; i < exprList.size(); i++) {
				ColDataType otherColDataType = getType(exprList.get(i), parameterMap, tableInfoMap);
				colDataType = ColDataTypeUtils.promoteDataType(colDataType, otherColDataType);
			}
			return colDataType;
		} else {
			return null;
		}
	}
				
	/**
	 * if we have an expression of the form: "SELECT field FROM table", then tableName is set to "table"
	 * in the case: "SELECT field from (SELECT field1 as field from table where field1 like 'foo%')
	 * The whole select statement becomes a table, with the field expression becoming the 
	 * columns
	 */
	public static ColDataType getType(Expression expr, ParameterMap parameterMap, TableInfoMap tableInfoMap)
		throws AmbiguousColumnException, UndefinedTableException, ColumnNotFoundException,
			UnsupportedExpressionException, FunctionNotFoundException, TypeMismatchException {
		if (expr instanceof JdbcNamedParameter) {
			ColDataType colDataType = parameterMap.findColDataType(((JdbcNamedParameter) expr).getName());
			return (colDataType != null) ? colDataType : ColDataTypeUtils.NAMED_PARAMETER;
		} else if (expr instanceof LongValue) {
			return ColDataTypeUtils.INTEGER;
		} else if (expr instanceof DoubleValue) {
			return ColDataTypeUtils.DOUBLE;
		} else if (expr instanceof DateValue) {
			return ColDataTypeUtils.DATE;
		} else if (expr instanceof TimestampValue) {
			return ColDataTypeUtils.TIMESTAMP;
		} else if (expr instanceof TimeValue) {
			return ColDataTypeUtils.TIME;
		} else if (expr instanceof StringValue) {
			return ColDataTypeUtils.VARCHAR;
		} else if (ColDataTypeUtils.isArithmeticExpression(expr)) {
			ColDataType leftType = getType(((BinaryExpression) expr).getLeftExpression(), parameterMap, tableInfoMap);
			ColDataType rightType = getType(((BinaryExpression) expr).getRightExpression(), parameterMap, tableInfoMap);
			return ColDataTypeUtils.promoteDataType(leftType, rightType);
		} else if ((expr instanceof BitwiseLeftShift) || (expr instanceof BitwiseRightShift)) {
			ColDataType leftType = getType(((BinaryExpression) expr).getLeftExpression(), parameterMap, tableInfoMap);
			return leftType;
		} else if (expr instanceof Modulo) {
			ColDataType leftType = getType(((BinaryExpression) expr).getLeftExpression(), parameterMap, tableInfoMap);
			return leftType;
		} else if (ColDataTypeUtils.isBooleanExpression(expr)) {
			return ColDataTypeUtils.BOOLEAN;
		} else if (expr instanceof CaseExpression) {
			List<WhenClause> whenClauseList = ((CaseExpression) expr).getWhenClauses();
			return getType(whenClauseList.get(0).getThenExpression(), parameterMap, tableInfoMap);
		} else if (expr instanceof CastExpression) {
			return ((CastExpression) expr).getType();
		} else if (expr instanceof DateTimeLiteralExpression) {
			return ColDataTypeUtils.DATE;
		} else if (expr instanceof DoubleValue) {
			return ColDataTypeUtils.DOUBLE;
		} else if (expr instanceof LongValue) {
			return ColDataTypeUtils.INTEGER;
		} else if (expr instanceof HexValue) {
			return ColDataTypeUtils.INTEGER;
		} else if (expr instanceof TimestampValue) {
			return ColDataTypeUtils.TIMESTAMP;
		} else if (expr instanceof NullValue) {
			return ColDataTypeUtils.NULL;
		} else if (expr instanceof Column) {
			return tableInfoMap.getColumnType((Column) expr);
		} else if (expr instanceof Parenthesis) {
			return getType(((Parenthesis) expr).getExpression(), parameterMap, tableInfoMap);
		} else if (expr instanceof SignedExpression) {
			return getType(((SignedExpression) expr).getExpression(), parameterMap, tableInfoMap);
		// for some reason, 4.2 no longer supports AllComparisonExpression.
		} else if (expr instanceof AnyComparisonExpression) {
			if (((AnyComparisonExpression) expr).getSubSelect().getSelectBody() instanceof PlainSelect) {
				PlainSelect plainSelect = (PlainSelect) ((AnyComparisonExpression) expr).getSubSelect().getSelectBody();
				return getSingleColumnPlainSelectReturnType(plainSelect, tableInfoMap);
			} else {
				throw new UnsupportedExpressionException("ANY clause can only use a single select", expr);
			}
		} else if (expr instanceof AnalyticExpression) {
			throw new UnsupportedExpressionException("analytic expressions are not supported", expr);
		} else if (expr instanceof UserVariable) {

			// TODO
			throw new UnsupportedExpressionException("TODO: User variables are not supported", expr);
		} else if (expr instanceof Function) {
			return resolveFunction((Function) expr, parameterMap, tableInfoMap);
		}
		return null;
	}

	private static ColDataType resolveFunction(Function function, ParameterMap parameterMap, TableInfoMap tableInfoMap)
	throws AmbiguousColumnException, UndefinedTableException, ColumnNotFoundException,
		   UnsupportedExpressionException, FunctionNotFoundException, TypeMismatchException {
		ExpressionList parameters = function.getParameters();
		FunctionType functionType = FunctionType.getFunctionType(function.getName());
		if (functionType == null) {
			throw new FunctionNotFoundException("the function " + function.getName() +
					" was not found in the list of known functions");
		}
		if (functionType.getArgTypes().length == 0) {
			return functionType.getResultType();
		} else if ((functionType.isSameTypeAsArg() != -1) || functionType.getResultType().equals(ColDataTypeUtils.SAME)) {
			if (parameters.getExpressions().isEmpty()) {
				throw new UnsupportedExpressionException("Function " + function +
						" has no arguments, but " + function.getName() +
						" returns the same type as its arguments");
			}
			int referenceIndex = functionType.isSameTypeAsArg();
			if ((referenceIndex < 0) || (referenceIndex >= parameters.getExpressions().size())) {
				throw new UnsupportedExpressionException("function " + function + " maps to " + functionType +
														 " which is supposed to return the same type as parameter " +
														 referenceIndex + " but the index is out of range");
			}
			return getType(parameters.getExpressions().get(referenceIndex), parameterMap, tableInfoMap);
		} else {
			return functionType.getResultType();
		}
	}


	// TODO
	private static ColDataType getSingleColumnPlainSelectReturnType(PlainSelect plainSelect, TableInfoMap tableInfoMap)
	throws UnsupportedExpressionException {
		throw new UnsupportedExpressionException("Any Comparison Expressions  are not supported");
	}
}
