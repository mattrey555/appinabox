package com.apptechnik.appinabox.engine.parser;

import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.schema.Column;

import java.util.*;
import java.util.stream.Collectors;

public class GetColumns {

    /**
     * StackFunction to return the list of columns in an Expression.
     */
    private static class GetColumnsFunction implements Traverse.StackFunction<Expression, Boolean> {
        private List<Column> columnList;

        public GetColumnsFunction() {
            columnList = new ArrayList<Column>();
        }

        public List<Column> getColumnList() {
            return columnList;
        }

        public Boolean apply(Stack<Expression> callStack, Expression expr) throws Exception {
            if (expr instanceof Column) {
                columnList.add((Column) expr);
            }
            return null;
        }
    }
    /**
     * Given an expression, return the unique set of ColumnInfo it references.
     * @param expr expression to traverse
     * @return set of columnInfo referenced by columns in the expression.
     */
    public static List<Column> getColumns(Expression expr) throws Exception {
        GetColumnsFunction getColumnsFunction = new GetColumnsFunction();
        Traverse.traverseExpression(expr, getColumnsFunction);
        return getColumnsFunction.getColumnList();
    }
}
