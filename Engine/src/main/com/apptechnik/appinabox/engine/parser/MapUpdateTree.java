package com.apptechnik.appinabox.engine.parser;

import com.apptechnik.appinabox.engine.parser.pojos.MergedFieldNode;
import com.apptechnik.appinabox.engine.parser.pojos.ParameterMap;
import com.apptechnik.appinabox.engine.parser.pojos.StatementParamMap;
import com.apptechnik.appinabox.engine.parser.pojos.Tree;
import com.apptechnik.appinabox.engine.util.CheckedFunction;
import com.apptechnik.appinabox.engine.util.StatementUtil;
import com.apptechnik.appinabox.engine.util.StringUtil;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Map the UPDATE/INSERT statements to the MergedFieldNode tree.  Add the expressions from the RETURNING
 * clause to MergedFieldNode.additionalColumns.
 */
public class MapUpdateTree {
    /**
     * Traverse the JSON tree and find which statements are assigned to each node. Return a map of
     * SQL statements to their parameter maps/
     * @param requestJsonTree JSON mapping tree.
     * @param queryParams query parameters from URL
     * @param statementList list of UPDATE/INSERT/DELETE statements
     * @param statementParamMap map of statements to ParameterMaps and dot-syntax names to JDBC parameters.
     * @param createTableInfoMap
     * @return map of SQL statements to their named parameters.
     * @throws Exception
     */
    public static void assignSQLStatements(Tree<MergedFieldNode> requestJsonTree,
                                            ParameterMap queryParams,
                                            List<Statement> statementList,
                                            StatementParamMap statementParamMap,
                                            TableInfoMap createTableInfoMap) throws Exception {

        // if the SQL statement list is only DELETE calls, the JSON request map may be null.
        AssignStatement assignStatement = new AssignStatement(statementList, queryParams, statementParamMap,
                                                              createTableInfoMap);
        requestJsonTree.traverseTreeStack(assignStatement);
        if (!assignStatement.getUnmatchedStatementList().isEmpty()) {
            // NOTE: show which variables could not be matched.
            throw new Exception("while assigning SQL statements to JSON nodes, the following statement(s) could not be matched:\n" +
                                StringUtil.concat(assignStatement.getUnmatchedStatementList(), "\n"));
        }
    }

    /*
     * The tree is parsed from a JSON expression, but there are  SQL statements which return values which
     * we refer to in successive SQL statements. We add the return values to the tree as columns.
     * Code generation will create and assign members from the SQL statement return values.
     */
    private static class AssignStatement implements Tree.ConsumerException<Stack<Tree<MergedFieldNode>>> {
        private List<Statement> statementList;
        private final ParameterMap queryParams;
        private final StatementParamMap statementParamMap;
        private final TableInfoMap tableInfoMap;

        /**
         * Tree Consumer to determine if the fields for a SQL statement can be populated by
         * a node and its ancestors.  Add matching statements to the node, and add their
         * returning values to their additional columns
         * @param statementList SQL statements.
         * @param queryParams URL path + query params + HTTP headers.
         * @param statementParamMap map of statements to ParameterMaps and dot-syntax names to JDBC parameters.
         * @param tableInfoMap map of tables to columns/column types.
         */
        public AssignStatement(List<Statement> statementList,
                               ParameterMap queryParams,
                               StatementParamMap statementParamMap,
                               TableInfoMap tableInfoMap) {

            // copy because we are destructive.
            this.statementList = new ArrayList<Statement>(statementList);
            this.queryParams = queryParams;
            this.statementParamMap = statementParamMap;
            this.tableInfoMap = tableInfoMap;
        }

        /**
         * A node might match multiple statements, so we loop over them util they don't match
         * @param nodeTreeStack path in the JSON map to this node.
         */
        @Override
        public void accept(Stack<Tree<MergedFieldNode>> nodeTreeStack) throws Exception {
            List<Statement> matchingStatements = new ArrayList<Statement>();
            for (Statement statement : statementList) {
                ParameterMap parameterMap = statementParamMap.getParamMap(statement);

                // Are all the JDBC named parameters in this statement populated by this object and its ancestors?
                // if so, the statement and its returning columns are associated with this node.
                if (parametersComplete(nodeTreeStack, parameterMap, queryParams)) {
                    parameterMap.values().forEach(paramData -> paramData.setSource(ParameterMap.ParamSource.REQUEST_JSON));
                    MergedFieldNode node = nodeTreeStack.peek().get();
                    node.addAssociatedStatement(statement);
                    List<SelectExpressionItem> returningExpressionList = StatementUtil.getReturningExpressionList(statement);
                    if (returningExpressionList != null) {
                        TableInfoMap statementTableInfoMap = tableInfoMap.filterTable(StatementUtil.getTable(statement).getName());
                        List<ColumnInfo> expressionList =
                            createReturningExpressionList(statement, parameterMap, statementTableInfoMap, returningExpressionList);
                        node.addAdditionalColumns(statement, expressionList);
                        parameterMap.addReturningExpressions(statement, statementTableInfoMap);
                    }
                    matchingStatements.add(statement);
                }
            }
            statementList.removeAll(matchingStatements);
        }

        public List<Statement> getUnmatchedStatementList() {
            return statementList;
        }
    }

    /**
     * Given a statement with a RETURNING expression (is this PostGres-specific?), create the list of columns
     * @param statement INSERT/UPDATE statement with optional RETURNING clause.
     * @param parameterMap used for types for named parameters
     * @param tableInfoMap map of table names/alias to tables
     * @return list of ColumnInfo returned or null.
     * @throws Exception if the returning columns mismatched the source exression.
     */
    public static List<ColumnInfo> createReturningExpressionList(Statement statement,
                                                                 ParameterMap parameterMap,
                                                                 TableInfoMap tableInfoMap,
                                                                 List<SelectExpressionItem> returnedList) throws Exception {
        return returnedList.stream()
                .map(CheckedFunction.wrap(item -> ColumnInfo.fromSelectExpressionItem(tableInfoMap, parameterMap, item)))
                .collect(Collectors.toList());
    }

    /**
     * Are all of the JDBC parameters satisfied by this list of nodes from a
     * depth-first traversal of the JSON tree. Nodes are populated
     * from the JSON mapping and by RETURNING clauses in INSERT/SELECT statements.
     * The "dot syntax" use in the named parameters does NOT match a dot-syntax
     * in the JSON expression (the way it does in a select statement), but rather
     * the names of each JSON node.  The dot-syntax can be relative to a sub-node
     * but *must* be unique across the entire tree.
     * @param nodeTreeStack stack of JSON/SQL mapping nodes via depth-first recursion (lowest node is top of stack)
     * @param parameterMap parameter list from SQL statement.
     * @param queryParams URL path + query + HTTP header parameters
     * @return
     */
    private static boolean parametersComplete(Stack<Tree<MergedFieldNode>> nodeTreeStack,
                                              ParameterMap parameterMap,
                                              ParameterMap queryParams) {
        // iterate over all the parameters.
        for (String jdbcParam : parameterMap.getParamSet()) {
            String dotParamName = parameterMap.dotParamName(jdbcParam);
            MergedFieldNode matchingNode = MergedFieldNode.matchNode(nodeTreeStack, dotParamName);
            if ((matchingNode == null) && !queryParams.hasParam(dotParamName)) {
                return false;
            }
        }
        return true;
    }
}
