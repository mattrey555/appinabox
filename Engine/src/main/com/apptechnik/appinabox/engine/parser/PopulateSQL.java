package com.apptechnik.appinabox.engine.parser;

import com.apptechnik.appinabox.engine.exceptions.TypeNotFoundException;
import com.apptechnik.appinabox.engine.exceptions.UndefinedTableException;
import com.apptechnik.appinabox.engine.exceptions.ValueNotFoundException;
import com.apptechnik.appinabox.engine.parser.create.StatementParser;
import com.apptechnik.appinabox.library.sql.NamedParamStatement;
import net.sf.jsqlparser.schema.Column;

import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

// In progress class to populate the SQL database
public class PopulateSQL {
    private Connection dbConnection;
    private TableInfoMap tableInfoMap;


    public PopulateSQL(Connection dbConnection, File createTableFile) throws Exception {
        StatementParser statementParser = new StatementParser();
        statementParser.parse(createTableFile);
        this.dbConnection = dbConnection;
        tableInfoMap = statementParser.getTableInfoMap();
        tableInfoMap.resolveAllChildReferences();
    }

    public void execute(int numValues) throws Exception {
        Collection<TableInfo> tableInfoList = tableInfoMap.getValues();
        List<TableInfo> populatedList = new ArrayList<>();
        while (!tableInfoList.isEmpty()) {
            List<TableInfo> removeList = new ArrayList<>();
            for (TableInfo tableInfo : tableInfoList) {
                if (allReferencesResolved(tableInfo, populatedList)) {
                    populateTable(dbConnection, tableInfo, numValues);
                    removeList.add(tableInfo);
                    populatedList.add(tableInfo);
                }
            }
            if (removeList.isEmpty()) {
                throw new UndefinedTableException("failed to resolve all tables");
            }
            tableInfoList.removeAll(removeList);
            numValues *= 2;
        }
    }

    public boolean allReferencesResolved(TableInfo tableInfo, List<TableInfo> tableInfoList) {
        for (ColumnInfo columnInfo : tableInfo.getColumnInfoSet()) {
            Column foreignKey = columnInfo.getForeignKey();
            if (foreignKey != null) {
                boolean found = false;
                for (TableInfo parentTableInfo : tableInfoList) {
                    if (foreignKey.getTable().getName().equals(parentTableInfo.getTable().getName())) {
                        found = true;
                    }
                }
                if (!found) {
                    return false;
                }
            }
        }
        return true;
    }

    public void populateTable(Connection connection, TableInfo tableInfo, int numValues)
            throws TypeNotFoundException, SQLException, ValueNotFoundException {
        String insertStatement = createInsertStatement(tableInfo);
        NamedParamStatement nps = new NamedParamStatement(connection, insertStatement);
        for (int i = 0; i < numValues; i++) {
            populateInsertStatement(connection, nps, tableInfo, i);
            try {
                nps.execute();
            } catch (SQLException sqlex) {
                System.out.println("oops");
            }
        }
    }

    public String createInsertStatement(TableInfo tableInfo) {
        StringBuffer sbVars = new StringBuffer();
        StringBuffer sbVals = new StringBuffer();
        boolean first = true;

        for (ColumnInfo columnInfo : tableInfo.getColumnInfoSet()) {
            if (!columnInfo.isAutogenerate()) {
                if (!first) {
                    sbVars.append(", ");
                    sbVals.append(", ");
                }
                sbVars.append(columnInfo.getColumnName());
                sbVals.append(':' + columnInfo.getColumnName());
                first = false;
            }
        }
        return "INSERT INTO " + tableInfo.getTable().getName() + " (" + sbVars + ")" +
                " VALUES (" + sbVals + ")";
    }

    void populateInsertStatement(Connection connection, NamedParamStatement statement, TableInfo tableInfo, int index)
            throws TypeNotFoundException, SQLException,  ValueNotFoundException {
        for (ColumnInfo columnInfo : tableInfo.getColumnInfoSet()) {
            if (columnInfo.getForeignKey() != null) {
                setNamedParamFromForeignKey(connection, statement, columnInfo);
            } else if (!columnInfo.isAutogenerate()) {
                setNamedParamFromSqlType(statement, columnInfo.getColumnSQLType(), columnInfo.getColumnName(), index);
            }
        }
    }

    public static void setNamedParamFromForeignKey(Connection connection,
                                                   NamedParamStatement statement,
                                                   ColumnInfo columnInfo)
            throws SQLException, TypeNotFoundException, ValueNotFoundException {
        String statementString = foreignKeySelectStatement(columnInfo);
        PreparedStatement query = connection.prepareStatement(statementString);
        ResultSet resultSet = query.executeQuery();
        try {
            if (resultSet.next()) {
                setNamedParamFromValue(statement, resultSet, 1, columnInfo.getColumnSQLType(), columnInfo.getColumnName());
            } else {
                throw new ValueNotFoundException("no results for " + statementString);
            }
        } finally {
            resultSet.close();
            query.close();
        }
    }


    public static String foreignKeySelectStatement(ColumnInfo columnInfo) {
        return "SELECT " + columnInfo.getForeignKey().getColumnName() + " FROM " + columnInfo.getForeignKey().getTable().getName();
    }

    public static void setNamedParamFromValue(NamedParamStatement statement,
                                              ResultSet resultSet,
                                              int columnIndex,
                                              int simpleType,
                                              String columnName)
            throws TypeNotFoundException, SQLException {
        if (simpleType == Types.BIT) { statement.setBoolean(columnName, resultSet.getBoolean(columnIndex)); }
        else if (simpleType == Types.TINYINT) { statement.setInt(columnName, resultSet.getByte(columnIndex)); }
        else if (simpleType == Types.SMALLINT) { statement.setShort(columnName, resultSet.getShort(columnIndex)); }
        else if (simpleType == Types.INTEGER) { statement.setInt(columnName, resultSet.getInt(columnIndex)); }
        else if (simpleType == Types.BIGINT) { statement.setLong(columnName, resultSet.getLong(columnIndex)); }
        else if (simpleType == Types.FLOAT) { statement.setFloat(columnName, resultSet.getFloat(columnIndex)); }
        else if (simpleType == Types.REAL) { statement.setDouble(columnName, resultSet.getDouble(columnIndex)); }
        else if (simpleType == Types.DOUBLE) { statement.setDouble(columnName, resultSet.getDouble(columnIndex)); }
        else if (simpleType == Types.NUMERIC) { statement.setDouble(columnName, resultSet.getDouble(columnIndex)); }
        else if (simpleType == Types.DECIMAL) { statement.setDouble(columnName, resultSet.getDouble(columnIndex)); }
        else if (simpleType == Types.CHAR) { statement.setInt(columnName, resultSet.getByte(columnIndex)); }
        else if (simpleType == Types.VARCHAR) { statement.setString(columnName, resultSet.getString(columnIndex)); }
        else if (simpleType == Types.LONGVARCHAR) { statement.setString(columnName, resultSet.getString(columnIndex)); }
        else if (simpleType == Types.DATE) { statement.setDate(columnName, resultSet.getDate(columnIndex)); }
        else if (simpleType == Types.TIME) { statement.setLong(columnName, resultSet.getLong(columnIndex)); }
        else if (simpleType == Types.TIMESTAMP) { statement.setLong(columnName, resultSet.getLong(columnIndex)); }
        else if (simpleType == Types.BINARY) { statement.setBytes(columnName, resultSet.getBytes(columnIndex)); }
        else if (simpleType == Types.VARBINARY) { statement.setBytes(columnName, resultSet.getBytes(columnIndex)); }
        else if (simpleType == Types.LONGVARBINARY) { statement.setBytes(columnName, resultSet.getBytes(columnIndex)); }
        else if (simpleType == Types.JAVA_OBJECT) { statement.setBytes(columnName, resultSet.getBytes(columnIndex)); }
        else if (simpleType == Types.DISTINCT) { statement.setInt(columnName, resultSet.getInt(columnIndex)); }
        else if (simpleType == Types.STRUCT) { statement.setBytes(columnName, resultSet.getBytes(columnIndex)); }
        else if (simpleType == Types.ARRAY) { statement.setBytes(columnName, resultSet.getBytes(columnIndex)); }
        else if (simpleType == Types.BLOB) { statement.setBytes(columnName, resultSet.getBytes(columnIndex)); }
        else if (simpleType == Types.CLOB) { statement.setBytes(columnName, resultSet.getBytes(columnIndex)); }
        else if (simpleType == Types.REF) { statement.setBytes(columnName, resultSet.getBytes(columnIndex)); }
        else { throw new TypeNotFoundException("simpleType " + simpleType + " not supported"); }
    }


    public static void setNamedParamFromSqlType(NamedParamStatement statement, int simpleType, String columnName, int index)
            throws TypeNotFoundException, SQLException {
        if (simpleType == Types.BIT) { statement.setBoolean(columnName, ((index % 2) == 0)); }
        else if (simpleType == Types.TINYINT) { statement.setInt(columnName, index % 256); }
        else if (simpleType == Types.SMALLINT) { statement.setShort(columnName, (short) index); }
        else if (simpleType == Types.INTEGER) { statement.setInt(columnName, index); }
        else if (simpleType == Types.BIGINT) { statement.setLong(columnName, index); }
        else if (simpleType == Types.FLOAT) { statement.setFloat(columnName, index); }
        else if (simpleType == Types.REAL) { statement.setDouble(columnName, index); }
        else if (simpleType == Types.DOUBLE) { statement.setDouble(columnName, index); }
        else if (simpleType == Types.NUMERIC) { statement.setDouble(columnName, index); }
        else if (simpleType == Types.DECIMAL) { statement.setDouble(columnName, index); }
        else if (simpleType == Types.CHAR) { statement.setInt(columnName, index % 256); }
        else if (simpleType == Types.VARCHAR) { statement.setString(columnName, columnName + index); }
        else if (simpleType == Types.LONGVARCHAR) { statement.setString(columnName, columnName + index); }
        else if (simpleType == Types.DATE) { statement.setDate(columnName, new Date(System.currentTimeMillis())); }
        else if (simpleType == Types.TIME) { statement.setLong(columnName, System.currentTimeMillis()); }
        else if (simpleType == Types.TIMESTAMP) { statement.setLong(columnName, System.currentTimeMillis()); }
        else if (simpleType == Types.BINARY) { statement.setBytes(columnName, toBytes(columnName, index)); }
        else if (simpleType == Types.VARBINARY) { statement.setBytes(columnName, toBytes(columnName, index)); }
        else if (simpleType == Types.LONGVARBINARY) { statement.setBytes(columnName, toBytes(columnName, index)); }
        else if (simpleType == Types.JAVA_OBJECT) { statement.setBytes(columnName, toBytes(columnName, index)); }
        else if (simpleType == Types.DISTINCT) { statement.setInt(columnName, index); }
        else if (simpleType == Types.STRUCT) { statement.setBytes(columnName, toBytes(columnName, index)); }
        else if (simpleType == Types.ARRAY) { statement.setBytes(columnName, toBytes(columnName, index)); }
        else if (simpleType == Types.BLOB) { statement.setBytes(columnName, toBytes(columnName, index)); }
        else if (simpleType == Types.CLOB) { statement.setBytes(columnName, toBytes(columnName, index)); }
        else if (simpleType == Types.REF) { statement.setBytes(columnName, toBytes(columnName, index)); }
        else { throw new TypeNotFoundException("simpleType " + simpleType + " not supported"); }
    }

    private static byte[] toBytes(String columnName, int index) {
        String var = columnName + index;
        return var.getBytes();
    }

}
