package com.apptechnik.appinabox.engine.parser;

import java.util.*;
import java.util.stream.Collectors;

import com.apptechnik.appinabox.engine.util.StringUtil;
import com.apptechnik.appinabox.engine.exceptions.*;

import net.sf.jsqlparser.expression.Alias;
import net.sf.jsqlparser.statement.create.table.ColDataType;
import net.sf.jsqlparser.statement.create.table.ColumnDefinition;
import net.sf.jsqlparser.statement.create.table.CreateTable;
import net.sf.jsqlparser.statement.create.table.Index;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.schema.Column;

/**
 * Info about SQL tables (list of columns and types, associated indices, whether it is a synthetic
 * table from a nested select.
 */
public class TableInfo {
	private Table table;								// SQL parser Table backreference.
	private Collection<ColumnInfo> columnInfoSet;		// list of column information.
	private List<Index> indexList;						// indices (from create table script)
	private boolean synthetic;							// added from a sub-select call.
	private String alias;								// parent Map key backreference.
	private boolean clientSync;                         // SQLFile directive to sync with clients.
	private String deletedColumnName;                   // SQLFile directive soft delete column name (default: Constants.SQLColumns.DELETED)
	private String modifiedTimestampColumnName;         // SQLFile directive timestamp (default: Constants.SQLColumns.MODIFIED_SINCE)

	public TableInfo(Table table, Collection<ColumnInfo> columnInfoSet, List<Index> indexList, boolean synthetic) {
		this.table = table;
		this.columnInfoSet = columnInfoSet;

		// we may be re-assigning the columns to this TableInfo, so reset their TableInfo to this.
		this.columnInfoSet.stream().forEach(columnInfo -> columnInfo.setContainingTableInfo(this));
		this.indexList = indexList;
		this.synthetic = synthetic;
		this.clientSync = false;
		this.deletedColumnName = null;
		this.modifiedTimestampColumnName = null;
	}

	/**
	 * Copy constructor which copies the columns, since aliases may be applied to them, and we don't
	 * want to change the original columns
	 * @param t source table
	 * @param synthetic since we're copying a table, this beter be true.
	 */
	public TableInfo(TableInfo t, boolean synthetic, Alias alias) {
		this(t.table, ColumnInfo.copyList(t.columnInfoSet), t.indexList, synthetic);
		if (alias != null) {
			setAlias(alias.getName());
		} else if (t.getAlias() != null) {
			setAlias(t.getAlias());
		}
		this.clientSync = t.clientSync;
		this.deletedColumnName = t.deletedColumnName;
		this.modifiedTimestampColumnName = t.modifiedTimestampColumnName;
	}

	/**
	 * Copy constructor.
	 * @param createTable
	 */
	public TableInfo(CreateTable createTable) {
		this.table = createTable.getTable();
		this.columnInfoSet = ColumnInfo.createColumnInfoList(this, createTable.getColumnDefinitions());
		// why do we set this to false explicitly?
		this.synthetic = false;
	}

	/**
	 * Remove the named column from this table info.
	 * @param columnName name of column to remove
	 * @return true if removed, false if it wasn't found in the table.
	 */
	public boolean removeColumn(String columnName) {
		Optional<ColumnInfo> matchColumnInfo =
			this.columnInfoSet.stream()
				.filter(columnInfo -> columnInfo.getColumnName().equals(columnName)).findFirst();
		if (matchColumnInfo.isPresent()) {
			columnInfoSet.remove(matchColumnInfo.get());
			return true;
		}
		return false;
	}

	/**
	 * Rename a column in this table info.
	 * @param oldColumnName old name
	 * @param newColumnName new name
	 * @return true if old name was found and changed, false otherwise.
	 */
	public boolean renameColumn(String oldColumnName, String newColumnName) {
		Optional<ColumnInfo> matchColumnInfo =
				this.columnInfoSet.stream()
						.filter(columnInfo -> columnInfo.getColumnName().equals(oldColumnName)).findFirst();
		if (matchColumnInfo.isPresent()) {
			matchColumnInfo.get().setColumnName(newColumnName);
			return true;
		}
		return false;
	}

	/**
	 * Drop an index from a DROP INDEX statement.
	 * @param index
	 */
	public void addIndex(Index index) {
		if (indexList == null) {
			indexList = new ArrayList<Index>();
		}
		indexList.add(index);
	}

	public boolean dropIndex(String indexName) {
		Index matchIndex = getIndex(indexName);
		if (matchIndex != null) {

			// if we turned on "primary key" for this columnn with an index constraint
			// turn it off, because we removed the index.
			matchIndex.getColumnsNames().stream().forEach(name -> getColumnInfo(name).setPrimaryKey(false));
			indexList.remove(matchIndex);
			return true;
		}
		return false;
	}

	public Index getIndex(String name) {
		if (indexList == null) {
			return null;
		}
		return indexList.stream().filter(idx -> idx.getName().equals(name)).findFirst().orElse(null);
	}

	/**
	 * Given a column name, return the corresponding column definition by scanning the list
	 *
	 * @param columnName name of the column to find.
	 * @return ColumnDefinition matching ColumnDefinition
	 */
	public ColumnInfo getColumnInfo(String columnName)  {
		return this.columnInfoSet.stream().filter(col -> col.getColumnName().equals(columnName)).findFirst().orElse(null);
	}

	public ColumnDefinition getColumnDefinition(String columnName)  {
		ColumnInfo columnInfo = getColumnInfo(columnName);
		return (columnInfo != null) ? columnInfo.getColumnDefinition() : null;
	}

	public ColDataType getColDataType(String columnName) {
		ColumnInfo columnInfo = getColumnInfo(columnName);
		return (columnInfo != null) ? columnInfo.getColDataType() : null;
	}

	/**
	 * Is the column type equal to the type name?
	 * @param columnName column name
	 * @param typeName type name to match.
	 * @return
	 */
	public boolean isType(String columnName, String typeName) {
		ColDataType colDataType = getColDataType(columnName);
		return (colDataType != null) ? typeName.equalsIgnoreCase(colDataType.getDataType()) : false;
	}

	/**
	 * Given a column, with an optional table qualifier:
	 * if the column has a table name, check if it matches the table name or alias.
	 * Search the table for a matching column name.
	 * @param column
	 * @param tableAlias
	 * @return
	 */
	public boolean matchColumnInfo(Column column, String tableAlias) {
		if (column.getTable() != null) {
			String columnTableName = column.getTable().getName();
			if ((tableAlias != null) && !tableAlias.equals(columnTableName)) {
				return false;
			}
			if (!getTable().getName().equals(columnTableName)) {
				return false;
			}
		}
		return getColumnInfoSet().stream()
				.filter(colInfo -> colInfo.getColumnName().equals(column.getColumnName()))
				.findAny().isPresent();
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof TableInfo) {
			return ((TableInfo) o).getTable().getName().equals(getTable().getName());
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hashCode = 1;
		if (getTable().getAlias() != null) {
			hashCode += getTable().getAlias().getName().hashCode();
		}
		hashCode += getTable().getName().hashCode();
		return hashCode;
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("table " + table.getName());
		if (getAlias() != null) {
			sb.append(" as " + getAlias());
		}
		if (table.getAlias() != null) {
			sb.append("table alias " + table.getAlias().getName());
		}
		sb.append("\n");
		sb.append(StringUtil.concat(getColumnInfoSet(), "\n"));
		return sb.toString();
	}


	public boolean inChildReferenceList(Table table) {
		for (ColumnInfo colInfo : getColumnInfoSet()) {
			if (colInfo.inChildReferenceList(table)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Return the columns for the primary key of this table.
	 * The primary key can be specified in the CREATE TABLE call in the column specification, or
	 * in the CREATE INDEX
	 * NOTE: there can only be one primrary key, so just return a ColumnInfo, not a list.
	 * @return list of column definitions.
	 */
	public List<ColumnInfo> getPrimaryKeyColumns() {
		return getColumnInfoSet().stream().filter(c -> c.isPrimaryKey()).collect(Collectors.toList());
	}

	/**
	 * Scan the list of column info to find the primary key.
	 * @return
	 */
	public ColumnInfo getPrimaryKey() {
		return columnInfoSet.stream().filter(col -> col.isPrimaryKey()).findFirst().orElse(null);
	}


	/**
	 * Are two tables the same?
	 * 1) do they both have aliases which match?
	 * 2) do neither of them have aliases and their names match?
	 * 3) false
	 * @param a
	 * @param b
	 * @return
	 */
    public static boolean sameTableInfo(TableInfo a, TableInfo b) {
		if ((a.getAlias() != null) && (b.getAlias() != null)) {
			return a.getAlias().equals(b.getAlias());
		} else if ((a.getAlias() == null) && (b.getAlias() == null)) {
			return a.getName().equals(b.getName());
		} else {
			return false;
		}
	}


	/**
	 * Retrieve a list of columns from the ColumnDefinition list in TableInfo
	 */
	public  List<Column> getTableColumns() {
		return getColumnInfoSet().stream()
				.map(colInfo -> colInfo.toColumn())
				.collect(Collectors.toList());
	}


	/**
	 * return the table for this table info.
	 *
	 * @return SQL table.
	 */
	public Table getTable() {
		return this.table;
	}

	public String getName() {
		return (this.alias != null) ? this.alias: this.getTable().getName();
	}

	public String getAlias() {
		return alias;
	}

	// TODO: we should set the alias for the SQL table as well here, but let's get everything else stable first.
	public void setAlias(String alias) {
		this.alias = alias;
	}

	public void addColumn(ColumnInfo columnInfo) {
		this.columnInfoSet.add(columnInfo);
	}


	public boolean isSynthetic() {
		return synthetic;
	}

	public void setSynthetic(boolean synthetic) {
		this.synthetic = synthetic;
	}

	/**
	 * return the list of columnInfo for this table.
	 * @return column definition with parsed spec strings.
	 */
	public Collection<ColumnInfo> getColumnInfoSet() {
		return columnInfoSet;
	}

	public List<Index> getIndexList() {
		return indexList;
	}

	/**
	 * Is there a column in this table with the name "name"?
	 * @param name
	 * @return
	 */
	public boolean containsName(String name) {
		return columnInfoSet.stream()
				.filter(colInfo -> colInfo.getColumnName().equals(name))
				.findAny().isPresent();
	}


	public void resolveChildReferences(TableInfo childTableInfo) {
		for (ColumnInfo columnInfo : columnInfoSet) {
			columnInfo.resolveChildReferences(this.getTable(), childTableInfo);
		}
	}

	public void resolveChildReferences(Map<String, TableInfo> tableInfoMap) {
		for (String tableName : tableInfoMap.keySet()) {
			TableInfo childTableInfo = tableInfoMap.get(tableName);
			resolveChildReferences(childTableInfo);
		}
	}


	public void setSync(boolean sync, String deletedColumnName, String modifiedTimestampColumnName) {
		this.clientSync = sync;
		this.deletedColumnName = deletedColumnName;
		this.modifiedTimestampColumnName = modifiedTimestampColumnName;
	}

	public boolean isClientSync() {
		return clientSync;
	}

	public String getDeletedColumnName() {
		return deletedColumnName;
	}

	public String getModifiedTimestampColumnName() {
		return modifiedTimestampColumnName;
	}


}
