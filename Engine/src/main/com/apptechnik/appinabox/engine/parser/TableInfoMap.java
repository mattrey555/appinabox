package com.apptechnik.appinabox.engine.parser;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.exceptions.*;
import com.apptechnik.appinabox.engine.parser.create.StatementParser;
import com.apptechnik.appinabox.engine.parser.pojos.ParameterMap;
import com.apptechnik.appinabox.engine.util.CheckedFunction;
import com.apptechnik.appinabox.engine.util.StringUtil;
import net.sf.jsqlparser.expression.Alias;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.create.table.ColDataType;
import net.sf.jsqlparser.statement.create.table.ColumnDefinition;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Wrapper for map of table info names to table information
 * TODO: Add a flag to indicate whether tables can be referenced by alias (SELECT:yes vs CREATE:no)
 */
public class TableInfoMap {
    private Map<String, TableInfo> tableInfoMap;

    public TableInfoMap() {
        tableInfoMap = new HashMap<>();
    }

    public TableInfoMap(TableInfo tableInfo) {
        tableInfoMap = new HashMap<>();
        tableInfoMap.put(tableInfo.getTable().getName(), tableInfo);
    }

    /**
     * Create a table info map from a map.
     * @param tableInfoMap map of table names to table information.
     */
    public TableInfoMap(Map<String, TableInfo> tableInfoMap) {
        this.tableInfoMap = tableInfoMap;
    }

    public TableInfoMap(String createTables) throws Exception {
        // parse the create table statements
        StatementParser statementParser = new StatementParser();
        statementParser.parse(createTables);
        this.tableInfoMap = statementParser.getTableInfoMap().getTableInfoMap();
    }

    public void merge(TableInfoMap otherTableInfoMap) {
        otherTableInfoMap.keySet().stream()
            .forEach(tableAliasName -> tableInfoMap.put(tableAliasName, otherTableInfoMap.getTableInfo(tableAliasName)));
    }


    /**
     * Given a column and a list of tables to search, find the table which contains that column.
     * If there is not exact match, throw AmbiguousColumnException or ColumnNotFoundException
     * @param column SQL column to find the matching table for.
     * @return name of the matching table
     * @throws AmbiguousColumnException more than one table contained the column
     * @throws ColumnNotFoundException no tables contained the column
     */
    public Table getTable(Column column) throws AmbiguousColumnException, ColumnNotFoundException {
        TableInfo tableInfo = getTableInfo(column);
        if (tableInfo != null) {
            return tableInfo.getTable();
        } else {
            String tableNameList = StringUtil.concat(new ArrayList<String>(tableInfoMap.keySet()), ",");
            throw new ColumnNotFoundException("failed to find column " + column +
                                              " in table list: " + tableNameList);
        }
    }

    /**
     * Get the table information for the named table.
     * @param tableName unaliased table name
     * @return TableInfo or null.
     */
    public TableInfo getTableInfo(String tableName) {
        return tableInfoMap.get(tableName);
    }


    public TableInfo getTableInfo(Table table) {
        String tableName = (table.getAlias() != null) ? table.getAlias().getName() : table.getName();
        return tableInfoMap.get(tableName);
    }

     /**
     * Given a column and a list of tables to search, find the table info which contains that column.
     * If there is not exact match, throw AmbiguousColumnException or ColumnNotFoundException
     * @param column
     * @return name of the matching table
     * @throws AmbiguousColumnException more than one table contained the column
     * @throws ColumnNotFoundException no tables contained the column
     */
    public TableInfo getTableInfo(Column column) throws AmbiguousColumnException, ColumnNotFoundException {
        if (column.getTable() != null) {
            return tableInfoMap.get(column.getTable().getName());
        } else {
            return getTableInfoForColumnName(column.getColumnName());
        }
    }

    public void put(Table table, TableInfo tableInfo) {
        if (table.getAlias() != null) {
            tableInfoMap.put(table.getAlias().getName(), tableInfo);
        } else {
            tableInfoMap.put(table.getName(), tableInfo);
        }
    }

    /**
     * Add a table info to this map
     * @param name
     * @param tableInfo
     * @param isSynthetic
     */
    public void put(String name, TableInfo tableInfo, boolean isSynthetic) {
        Alias alias = new Alias(name);
        TableInfo tableInfoCopy = new TableInfo(tableInfo, isSynthetic, alias);
        tableInfoCopy.setAlias(name);
        tableInfoMap.put(name, tableInfoCopy);
    }

    public boolean dropTable(String tableName) {
        return tableInfoMap.remove(tableName) != null;
    }

    /**
     * For INSERT and UPDATE statments, filter the table so its just the entry for the table referred to
     * in the statement.
     */
    public TableInfoMap filterTable(String tableName) {
        Map<String, TableInfo> filteredTable = new HashMap<String, TableInfo>();
        filteredTable.put(tableName, tableInfoMap.get(tableName));
        return new TableInfoMap(filteredTable);
    }

    /**
     * Given a column, find the matching table and column definition and return the SQL simpleType string
     */
    public ColDataType getColumnType(Column column)
    throws AmbiguousColumnException, UndefinedTableException, ColumnNotFoundException {
        return getColumnInfo(column).getColDataType();
    }

    public ColumnInfo getColumnInfo(Column column)
    throws UndefinedTableException, AmbiguousColumnException, ColumnNotFoundException {
        String tableName = (column.getTable() != null) ? column.getTable().getName() : null;
        return getColumnInfo(tableName, column.getColumnName());
    }

    public List<ColumnInfo> getColumnInfoList(List<Column> columnList) {
        return columnList.stream().map(CheckedFunction.wrap(c -> getColumnInfo(c))).collect(Collectors.toList());
    }

    /**
     * Given a column name without a table discriminator, find the TableInfo which contains it.
     * Throw an exception if there is no match, or multiple matches.
     * @param columnName name of the column
     * @return
     * @throws AmbiguousColumnException
     * @throws ColumnNotFoundException
     */
    public TableInfo getTableInfoForColumnName(String columnName) throws AmbiguousColumnException, ColumnNotFoundException {
        TableInfo candTableInfo = null;
        for (String tableName : tableInfoMap.keySet()) {
            TableInfo tableInfo = tableInfoMap.get(tableName);
            for (ColumnInfo colInfo : tableInfo.getColumnInfoSet()) {
                if (colInfo.getColumnName().equals(columnName)) {
                    if (candTableInfo != null) {
                        throw new AmbiguousColumnException("ambiguous column " + columnName +
                                " between " + tableName +
                                " and " + candTableInfo.getTable().getName());
                    } else {
                        candTableInfo = tableInfo;
                    }
                }
            }
        }
        if (candTableInfo == null) {
            throw new ColumnNotFoundException("column " + columnName + " not found in tables: " + getTableNames());
        }
        return candTableInfo;
    }

    /**
     * Given a map of table names to ColumDefinitions,  a tableName (which may be null) and columnName,
     * find the matching tableInfo.
     *
     * @param tableName    table name (may be null)
     * @param columnName   column name
     */
    public ColumnInfo getColumnInfo(String tableName, String columnName)
            throws AmbiguousColumnException, UndefinedTableException, ColumnNotFoundException {
        TableInfo foundTable = null;
        if (tableName != null) {
            foundTable = tableInfoMap.get(tableName);
            if (foundTable == null) {
                throw new UndefinedTableException(tableName + " not found, valid tables are " + tableInfoNamesToString());
            }
        } else {
            foundTable = getTableInfoForColumnName(columnName);
        }
        ColumnInfo foundColumnInfo = foundTable.getColumnInfo(columnName);
        if (foundColumnInfo == null) {
            throw new ColumnNotFoundException("column: " + columnName + " not found in table: " + tableName);
        }
        return foundColumnInfo;
    }

    /**
     * Tables flagged for sync follow these conventions:
     * CREATE TABLE: must contain a DELETED boolean flag and MODIFIED_SINCE timestamp
     * SQL statements flagged for sync must follow these conventions:
     * SELECT: must use AND DELETED=FALSE and MODIFIEDSINCE > :modified_since in WHERE clause
     * UPDATE: must use AND DELETED=FALSE in WHERE clause
     * DELETE: must not use DELETE, must use UPDATE {@code <table>} SET DELETE=TRUE
     * @param syncedTables collection of synced table directives from the .sql files
     * @return list of errors from checking synced tables.
     */
    public List<String> checkSyncedTables(Collection<String> syncedTables) {
        List<String> errorMessages = new ArrayList<String>();
        for (String syncedTable : syncedTables) {
            TableInfo tableInfo = tableInfoMap.get(syncedTable);
            if (tableInfo == null) {
                errorMessages.add("The sync table directive for: " + syncedTable + " does not reference a table");
            } else {
                ColumnDefinition deletedColDef = tableInfo.getColumnDefinition(tableInfo.getDeletedColumnName());
                if (deletedColDef == null) {
                    errorMessages.add("The sync table " + syncedTable + " does not have a DELETED column");
                } else if (!deletedColDef.getColDataType().getDataType().equalsIgnoreCase(Constants.SQLTypes.BOOLEAN)) {
                    errorMessages.add("The sync table " + syncedTable + " has a DELETED column, but it's not BOOLEAN (" +
                                      deletedColDef.getColDataType().getDataType() + ")");
                }
                ColumnDefinition modifiedColDef = tableInfo.getColumnDefinition(tableInfo.getModifiedTimestampColumnName());
                if (modifiedColDef == null) {
                    errorMessages.add("The sync table " + syncedTable + " does not have a MODIFIED_DATE column");
                } else if (!modifiedColDef.getColDataType().getDataType().equalsIgnoreCase(Constants.SQLTypes.TIMESTAMP)) {
                    errorMessages.add("The sync table " + syncedTable + " has a MODIFIED_SINCE column, but it's not TIMESTAMP (" +
                                      modifiedColDef.getColDataType().getDataType() + ")");
                }
            }
        }
        return errorMessages;
    }

    /**
     * From a table map and a parameter map, create a column from a select expression item.
     * Used in subqueries when we create synthetic tables.
     * @param parameterMap map of statements to named parameters.
     * @param selectExpressionItem select expression to find the type from
     * @return column info.
     * @throws Exception
     */
    public ColumnInfo fromSelectExpressionItem(ParameterMap parameterMap,
                                               SelectExpressionItem selectExpressionItem) throws Exception {
        Expression expr = selectExpressionItem.getExpression();
        ColDataType colDataType = ExpressionType.getType(expr, parameterMap, this);
        if (colDataType == null) {
            throw new UnsupportedExpressionException(expr.toString() + " could not be resolved to a type");
        }
        List<ColumnInfo> columnInfoList = ColumnInfo.resolveColumns(expr, this);
        if (selectExpressionItem.getAlias() != null) {
            ColumnDefinition colDef = new ColumnDefinition();
            colDef.setColumnName(selectExpressionItem.getAlias().getName());
            colDef.setColDataType(colDataType);
            TableInfo tableInfo = columnInfoList.get(0).getContainingTableInfo();
            return new ColumnInfo(tableInfo, colDef);
        } else if (columnInfoList.size() == 1) {
            return columnInfoList.get(0);
        } else {
            throw new UnsupportedExpressionException(selectExpressionItem + " has multiple columns and no alias");
        }
    }

    /**
     * Extract the column information from this table.
     * @return list of column info
     */
    public List<ColumnInfo> getAllColumnInfo() {
        return tableInfoMap.values().stream()
                .map(tableInfo -> tableInfo.getColumnInfoSet())
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    public List<TableInfo> getTableInfoList() {
        return new ArrayList<TableInfo>(tableInfoMap.values());
    }

    /**
     * Apply DROP INDEX SQL statement.
     * @param indexName index name.
     * @return true if it was actually dropped, false otherwise.
     */
    public boolean dropIndex(String indexName) {
        boolean found = true;
        for (TableInfo tableInfo : tableInfoMap.values()) {
            found |= tableInfo.dropIndex(indexName);
        }
        return found;
    }


    /**
     * Table definitions can contain REFERENCES clauses, PostGres' version of foreign keys.  We
     * create the reverse link from the parent ColumnInfo to the child.
     **/
    public void resolveAllChildReferences() {
        for (String tableName : tableInfoMap.keySet()) {
            TableInfo tableInfo = tableInfoMap.get(tableName);
            tableInfo.resolveChildReferences(tableInfoMap);
        }
    }

    public Map<String, TableInfo> getTableInfoMap() {
        return tableInfoMap;
    }

    public String toString() {
        return StringUtil.concat(
                tableInfoMap.keySet().stream()
                .map(key -> key + " -> " + tableInfoMap.get(key))
                .collect(Collectors.toList()), "\n");
    }

    public String getTableNames() {
        return tableInfoMap.keySet().stream().collect(Collectors.joining(","));
    }

    public String tableInfoNamesToString() {
        return StringUtil.concat(new ArrayList<String>(tableInfoMap.keySet()), "\n");
    }

    public Collection<TableInfo> getValues() {
        return tableInfoMap.values();
    }

    public Set<String> keySet() {
        return tableInfoMap.keySet();
    }
}
