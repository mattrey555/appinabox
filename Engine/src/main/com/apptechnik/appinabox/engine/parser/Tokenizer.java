
import java.util.ArrayList;
import java.util.List;
import java.text.ParseException;

/**
 * tokenize an expression of the form:
 * function-call := <IDENT>(<param-list>)
 * param-list := <param> | <param>,<param-list>
 * param := <ident> | "<string>" | <integer> | <double> | <float> | <boolean>
 * ident := [A-Z][a-z][A-Za-z0-9]*
 * string := .*
 * integer := [0-9][0-9]*
 * float := [0-9]*\.[0-9][0-9]F
 * double := [0-9]*\.[0-9][0-9]
 * boolean := true\|false
 */

public class Tokenizer {
    private List<Token> tokenList;
    private StringBuilder sbToken;
    private TokenType tokenType;

    public Tokenizer() {
         tokenList = new ArrayList<Token>();
         sbToken = new StringBuilder();
         tokenType = TokenType.UNKNOWN;
    }
       
    public enum TokenType {
        IDENT,
        OPEN_PAREN,
        CLOSE_PAREN,
        COMMA,
        INTEGER,
        FLOAT,
        DOUBLE,
        BOOLEAN,
        UNKNOWN,
        QUOTED_STRING
    }


    public class Token {
        private TokenType type;
        private String token;

        public Token(TokenType type, String token) {
            this.type = type;
            this.token = token;
        }

        public String getToken() {
            return token;
        }

        public TokenType getType() {
            return type;
        }

    }

    private void addToken() {
        if (sbToken.length() > 0) {
            tokenList.add(new Token(tokenType, sbToken.toString()));
        }
        sbToken = new StringBuilder();
        tokenType = TokenType.UNKNOWN;
    }

    public List<Token> tokenize(String s) throws ParseException {
        int ich = 0; 
        while (ich < s.length()) {
            char ch = s.charAt(ich);
            if (ch == '"') {
                tokenType = TokenType.QUOTED_STRING;
                sbToken.append(ch);
                ich++;
                boolean inQuotes = true;
                boolean escaped = false;
                while (ich < s.length() && inQuotes) {
                    ch = s.charAt(ich);
                    sbToken.append(ch);
                    if (ch == '\\') {
                        escaped = true;
                    } else {
                        if (ch == '"') {
                            if (!escaped) {
                                inQuotes = false;
                            }
                        } 
                    }
                    ich++;
                }
                if (inQuotes) {
                    throw new ParseException("unclosed string literal: " + s, ich);
                }
                addToken();
            } else if (ch == '(') {
                tokenType = TokenType.OPEN_PAREN;
                sbToken.append(ch);
                addToken();
            } else if (ch == ')') {
                tokenType = TokenType.CLOSE_PAREN;
                sbToken.append(ch);
                addToken();
            } else if (ch == ',') {
                tokenType = TokenType.COMMA;
                sbToken.append(ch);
                addToken();
            } else if (Character.isDigit(ch)) {
                tokenType = TokenType.INTEGER;
                while (ich < s.length()) {
                    ch = s.charAt(ich);
                    if (ch == '.') {
                        tokenType = TokenType.DOUBLE;
                    } else if (ch == 'F') {
                        tokenType = TokenType.FLOAT;
                    } else if (!Character.isDigit(ch)) {
                        break;
                    }
                    sbToken.append(ch);
                    ich++;
                }
                addToken();
            } else if (Character.isLetter(ch)) {
                tokenType = TokenType.IDENT;
                while (ich < s.length()) {
                    ch = s.charAt(ich);
                    if (Character.isLetterOrDigit(ch)) {
                       sbToken.append(ch);
                       ich++;
                    } else {
                       break;
                    }
                }
            } else if (Character.isWhitespace(ch)) {
                ich++;
            } else {
                throw new ParseException("unrecognized character: " + ch + " at " + ich + " in " + s, ich);
            }
        }
        return tokenList;
    }
}
