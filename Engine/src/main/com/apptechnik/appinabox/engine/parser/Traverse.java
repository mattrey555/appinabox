package com.apptechnik.appinabox.engine.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.function.Consumer;

import com.apptechnik.appinabox.engine.exceptions.*;
import com.apptechnik.appinabox.engine.parser.query.SelectTableUtils;
import net.sf.jsqlparser.expression.*;
import net.sf.jsqlparser.expression.operators.relational.*;
import net.sf.jsqlparser.statement.select.*;
import net.sf.jsqlparser.util.cnfexpression.MultipleExpression;

/**
 * Recursive expression traverser.  Takes a StackFunction which is applied
 * to each expression as they are encountered.  A breadcrumb trail is maintained
 * in a stack.
 * I would love to use the node traversal in jsqlparser, but there are many expressions
 * (like select getWhere()), where it's null (as of version 4.2)
 */
public class Traverse {
	
	public Traverse() {
	}

	/**
	 * Traversal function which maintains a stack back to the expression root, since it's sometimes
	 * handy to know the context of a node.
	 * @param <S>
	 * @param <T>
	 */
	public interface StackFunction<S,T> {
		public T apply(Stack<S> callStack, S value) throws Exception;
	}

	public static class ConsumerWrapper implements StackFunction<Expression, Void> {
		private Consumer consumer;

		public ConsumerWrapper(Consumer consumer) {
			this.consumer = consumer;
		}

		public Void apply(Stack<Expression> callStack, Expression value) {
			consumer.accept(value);
			return null;
		}
	}

	public interface StackFunctionParam<S, T, U> {
		public U apply(Stack<S> callStack, S value, T param) throws Exception;
	}

	private static class StackFunctionParamNotReally<S,T> implements StackFunctionParam<S, Void, T> {
		private final StackFunction<S, T> realStackFunction;

		public StackFunctionParamNotReally(StackFunction<S, T> realStackFunction) {
			this.realStackFunction = realStackFunction;
		}

		@Override
		public T apply(Stack<S> callStack, S value, Void param) throws Exception {
			return this.realStackFunction.apply(callStack, value);
		}
	}

	public static <R> R traverseExpression(Expression expr, StackFunction<Expression,R> fn) throws Exception {
		StackFunctionParam wrapperFn = new StackFunctionParamNotReally(fn);
		return (R) traverseExpressionInternal(expr, new Stack<Expression>(), null, wrapperFn);
	}

	public static <T, S> T traverseExpression(Expression expr, T param, StackFunctionParam<Expression, T, S> fn) throws Exception {
		return (T) traverseExpressionInternal(expr, new Stack<Expression>(), param, fn);
	}

	/**
	 * Wrapper to traverse an expression with a conventional consumer.
	 * @param expr
	 * @param consumer
	 * @throws Exception
	 */
	public static void traverseExpression(Expression expr, Consumer consumer) throws Exception {
		ConsumerWrapper consumerWrapper = new ConsumerWrapper(consumer);
		traverseExpression(expr, consumerWrapper);
	}

	/**
	 * Internal function to traverse the expression. Exit early if the stack function returns null.
	 * TODO: remove the returns in here and in passed functions.
	 * @param expr expression somewhere in the expression tree.
	 * @param callStack backtrace to the root
	 * @param fn function to call on each node.
	 * @param <R>
	 * @return
	 * @throws Exception
	 */
	private static <T, R> R traverseExpressionInternal(Expression expr,
													   Stack<Expression> callStack,
													   T param,
													   StackFunctionParam<Expression, T, R> fn) throws Exception {
		R exprResult = fn.apply(callStack, expr, param);
		if (exprResult != null) {
			return exprResult;
		}
		callStack.push(expr);

		// The recursion is only on expressions which contain subexpressions.  Constants, Variables, etc, should terminate.
		try {
			if (expr instanceof BinaryExpression) {
				BinaryExpression binExpr = (BinaryExpression) expr;
				R leftResult = traverseExpressionInternal(binExpr.getLeftExpression(), callStack, param, fn);
				R rightResult = traverseExpressionInternal(binExpr.getRightExpression(), callStack, param, fn);
				if (leftResult != null) {
					return leftResult;
				} else if (rightResult != null) {
					return rightResult;
				}
			} else if (expr instanceof IsBooleanExpression) {
				IsBooleanExpression isExpr = (IsBooleanExpression) expr;
				R leftResult = traverseExpressionInternal(isExpr.getLeftExpression(), callStack, param, fn);
				return leftResult;
			} else if (expr instanceof AnalyticExpression) {
				AnalyticExpression analyticExpr = (AnalyticExpression) expr;
				R analyticResult = traverseExpressionInternal(analyticExpr.getExpression(), callStack, param, fn);
				if (analyticResult != null) {
					return analyticResult;
				}
			} else if (expr instanceof Between) {
				Between between = (Between) expr;
				R resultLeft = traverseExpressionInternal(between.getLeftExpression(), callStack, param, fn);
				R resultStart = traverseExpressionInternal(between.getBetweenExpressionStart(), callStack, param, fn);
				R resultEnd = traverseExpressionInternal(between.getBetweenExpressionStart(), callStack, param, fn);
			} else if (expr instanceof CaseExpression) {
				CaseExpression caseExpr = (CaseExpression) expr;
				R resultElse = traverseExpressionInternal(caseExpr.getElseExpression(), callStack, param, fn);
				R resultSwitch = traverseExpressionInternal(caseExpr.getSwitchExpression(), callStack, param, fn);
				List<WhenClause> whenClauseList = caseExpr.getWhenClauses();
				for (WhenClause whenClause : whenClauseList) {
					R resultWhen = traverseExpressionInternal(whenClause.getWhenExpression(), callStack, param, fn);
					R resultThen = traverseExpressionInternal(whenClause.getThenExpression(), callStack, param, fn);
				}
			} else if (expr instanceof CastExpression) {
				CastExpression castExpr = (CastExpression) expr;
				R resultLeft = traverseExpressionInternal(castExpr.getLeftExpression(), callStack, param, fn);
			} else if (expr instanceof ExistsExpression) {
				ExistsExpression existsExpr = (ExistsExpression) expr;
				R result = traverseExpressionInternal(existsExpr.getRightExpression(), callStack, param, fn);
			} else if (expr instanceof ExtractExpression) {
				ExtractExpression extractExpr = (ExtractExpression) expr;
				R result = traverseExpressionInternal(extractExpr.getExpression(), callStack, param, fn);
			} else if (expr instanceof net.sf.jsqlparser.expression.Function) {
				net.sf.jsqlparser.expression.Function function = (net.sf.jsqlparser.expression.Function) expr;
				ExpressionList parameters = function.getParameters();
				R paramResult = traverseExpressionListInternal(parameters.getExpressions(), callStack, param, fn);
			} else if (expr instanceof InExpression) {
				InExpression inExpr = (InExpression) expr;
				R leftResult = traverseExpressionInternal(inExpr.getLeftExpression(), callStack, param, fn);
				if (inExpr.getRightItemsList() instanceof ExpressionList) {
					ExpressionList rightItemsList = (ExpressionList) inExpr.getRightItemsList();
					R rightItemsResult = traverseExpressionListInternal(rightItemsList.getExpressions(), callStack, param, fn);
				}
			} else if (expr instanceof IsNullExpression) {
				IsNullExpression isNullExpr = (IsNullExpression) expr;
				R leftResult = traverseExpressionInternal(isNullExpr.getLeftExpression(), callStack, param, fn);
			} else if (expr instanceof KeepExpression) {
				KeepExpression keepExpr = (KeepExpression) expr;
				List<OrderByElement> orderByElementList = keepExpr.getOrderByElements();
				for (OrderByElement orderByElement : orderByElementList) {
					R orderByResult = traverseExpressionInternal(orderByElement.getExpression(), callStack, param, fn);
				}
			} else if (expr instanceof MultipleExpression) {
				MultipleExpression multiExpr = (MultipleExpression) expr;
				R exprListResult = traverseExpressionListInternal(multiExpr.getList(), callStack, param, fn);
			} else if (expr instanceof Parenthesis) {
				Parenthesis parenExpr = (Parenthesis) expr;
				R parenResult = traverseExpressionInternal(parenExpr.getExpression(), callStack, param, fn);
			} else if (expr instanceof RowConstructor) {
				RowConstructor rowConstructorExpr = (RowConstructor) expr;
				ExpressionList exprList = rowConstructorExpr.getExprList();
				R rowConstructorResult = traverseExpressionListInternal(exprList.getExpressions(), callStack, param, fn);
			} else if (expr instanceof SignedExpression) {
				SignedExpression signedExpr = (SignedExpression) expr;
				R signedResult = traverseExpressionInternal(signedExpr.getExpression(), callStack, param, fn);
			} else if (expr instanceof ValueListExpression) {
				ValueListExpression valueListExpr = (ValueListExpression) expr;
				ExpressionList exprList = valueListExpr.getExpressionList(); 
				R valueListResult = traverseExpressionListInternal(exprList.getExpressions(), callStack, param, fn);
			}
		} finally {
			callStack.pop();
		}
		return null;
	}

	public static <R> R traverseExpressionList(List<Expression> exprList, 
											   StackFunction<Expression,R> fn) throws Exception {
		StackFunctionParam wrapperFn = new StackFunctionParamNotReally(fn);
		return (R) traverseExpressionListInternal(exprList, new Stack<Expression>(), null, wrapperFn);
	}

	private static <T, R> R traverseExpressionListInternal(List<Expression> exprList,
													       Stack<Expression> callStack,
													       T param,
														   StackFunctionParam<Expression, T, R> fn) throws Exception {
		for (Expression expr : exprList) {
			R exprResult = traverseExpressionInternal(expr, callStack, param, fn);
			if (exprResult != null) {
				return exprResult;
			}
		}
		return null;
	}


	/**
	 * The most common operation for traversal is to find apply a function to an expression relative to the 
	 * opposite expression in a binary expression, i.e. a = b, we want to call fn(a,b), where we're looking
	 * for objects of type a, and want to execute some function on a and the other side of the expression,
	 * such as finding the table and column, or the expression type
	 */
	public static abstract class ResolveBinaryExpressionFunction<T> implements Traverse.StackFunction<Expression, T> {
			
		/**
		 * for an expression of the type A in (B,C,D), call the type matcher on B,C,D, and apply expression on
		 * (A,B), (A,C), (A,D)
		 */
        private void resolveInExpression(InExpression inExpr) throws Exception {
			filterApplyInExpression(inExpr.getLeftExpression(), ((ExpressionList) inExpr.getRightItemsList()).getExpressions());
        }

		private void filterApplyInExpression(Expression opposingExpression, List<Expression> expressionList) throws Exception {
			for (Expression expr : expressionList) {
				if (isExprType(expr)) {
					applyExpression(expr, opposingExpression);
				}
			}
		}

        private void resolveBinaryExpression(BinaryExpression binExpr, Expression expr) 
            throws AmbiguousColumnException, UndefinedTableException, ColumnNotFoundException, TypeNotFoundException, Exception {
            if (binExpr.getLeftExpression().equals(expr)) {
				applyExpression(expr, binExpr.getRightExpression());
            } else {
				applyExpression(expr, binExpr.getLeftExpression());
            }
        }

		public abstract boolean isExprType(Expression expr);
		public abstract void applyExpression(Expression targetExpression, Expression opposingExpression) throws Exception;

        @Override
        public T apply(Stack<Expression> callStack, Expression expr) throws Exception {
            if (isExprType(expr)) {
                Expression parentExpr = callStack.peek();
                if (parentExpr instanceof BinaryExpression) {
                    resolveBinaryExpression((BinaryExpression) parentExpr, expr);
                } else if (parentExpr instanceof InExpression) {
                    resolveInExpression((InExpression) parentExpr);
                } else if (parentExpr instanceof Parenthesis) {
                    BinaryExpression binExpr = findExpressionOfType(callStack, BinaryExpression.class);
                    if (binExpr != null) {
                        resolveBinaryExpression(binExpr, expr);
                    } else {
						throw new UnsupportedExpressionException("unable to find binary expression ancestor of " + expr);
					}
                } else {
                    throw new UnsupportedExpressionException("parent expression: " + parentExpr + " not supported");
                }
            }
            return null;
        }
	}

	/**
	 * Given a call stack, trace backwards to find an expression of the matching type.
	 */
    public static <T extends Expression> T findExpressionOfType(Stack<Expression> callStack, Class<T> exprClass) {
        for (Expression expr : callStack) {
            if (exprClass.isAssignableFrom(expr.getClass())) {
                return (T) expr;
            }
        }
        return null;
    }


	private static class FilterType<T> implements Consumer<Expression> {
		public List<T> resultList;
		public Class c;

		public FilterType(Class c, List<T> resultList) {
			this.c = c;
			this.resultList = resultList;
		}

		public void accept(Expression expr) {
			if (c.isAssignableFrom(expr.getClass())) {
				resultList.add((T) expr);
			}
		}
	}

	public static <T> List<T> recursivelyFilterByType(Expression expr, Class<T> c) throws Exception {
		List<T> list = new ArrayList<T>();
		Traverse.traverseExpression(expr, new FilterType(c, list));
		return list;
	}

}
