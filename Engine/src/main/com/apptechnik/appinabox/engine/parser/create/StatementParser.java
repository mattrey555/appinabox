package com.apptechnik.appinabox.engine.parser.create;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.exceptions.ColumnNotFoundException;
import com.apptechnik.appinabox.engine.exceptions.TableNotFoundException;
import com.apptechnik.appinabox.engine.exceptions.UnsupportedExpressionException;
import com.apptechnik.appinabox.engine.parser.ColumnInfo;
import com.apptechnik.appinabox.engine.parser.TableInfo;
import com.apptechnik.appinabox.engine.parser.TableInfoMap;
import com.apptechnik.appinabox.engine.parser.pojos.ParameterMap;
import com.apptechnik.appinabox.engine.parser.query.SelectTableUtils;
import com.apptechnik.appinabox.engine.util.FileUtil;
import net.sf.jsqlparser.statement.Statements;
import net.sf.jsqlparser.parser.StringProvider;
import net.sf.jsqlparser.parser.CCJSqlParser;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.ParseException;
import net.sf.jsqlparser.statement.alter.AlterExpression;
import net.sf.jsqlparser.statement.alter.AlterOperation;
import net.sf.jsqlparser.statement.create.index.CreateIndex;
import net.sf.jsqlparser.statement.create.table.ColumnDefinition;
import net.sf.jsqlparser.statement.create.table.CreateTable;
import net.sf.jsqlparser.statement.alter.Alter;
import net.sf.jsqlparser.statement.Statement;

import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.create.table.Index;
import net.sf.jsqlparser.statement.create.view.CreateView;
import net.sf.jsqlparser.statement.drop.Drop;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;

/**
 * Parse a series of CREATE TABLE statements, and create the tableInfoMap, which describes
 * the column definitions and constraints.
 */
public class StatementParser {
	private TableInfoMap tableInfoMap;

    public StatementParser() {
		tableInfoMap = new TableInfoMap();
	}

	/**
	 * Parse a string source into the tableInfoMap
	 * @param source SQL statements, hopefully containing CREATE TABLE statements.
	 * @throws IOException
	 * @throws JSQLParserException
	 * @throws ParseException
	 */
	public void parse(String source) throws Exception {
		CCJSqlParser parser = new CCJSqlParser(new StringProvider(source));
		Statements statements = parser.Statements();
		for (Statement statement : statements.getStatements()) {
			if (statement instanceof CreateTable) {
				createTable((CreateTable) statement);
			} else if (statement instanceof Alter) {
				alterTable((Alter) statement);
			} else if (statement instanceof CreateIndex) {
				createIndex((CreateIndex) statement);
			} else if (statement instanceof CreateView) {
				createView((CreateView) statement);
			} else if (statement instanceof Drop) {
				drop((Drop) statement);
			} else {
				System.err.println(Constants.WARNING + ": " + statement.toString() + " not processed");
			}
		}
    }

    private void createTable(CreateTable createTable) {
		TableInfo tableInfo = new TableInfo(createTable);
		tableInfoMap.put(createTable.getTable(), tableInfo);
	}

	/**
	 * Parse CREATE VIEW similar to how we would handle a SELECT statement, but add the
	 * resulting table to the CREATE TABLE table info map.
	 * @param createView
	 * @throws Exception
	 */
	public void createView(CreateView createView) throws Exception {
		Select select = createView.getSelect();
		if (select.getSelectBody() instanceof PlainSelect) {
			PlainSelect plainSelect = (PlainSelect) select.getSelectBody();
			TableInfoMap createViewTableInfoMap =
				SelectTableUtils.createSelectTableMap(plainSelect, new ParameterMap(), tableInfoMap);
			createViewTableInfoMap.getValues().stream()
				.forEach(tableInfo -> { tableInfoMap.put(tableInfo.getName(), tableInfo, true); });
		}
	}

	/**
	 * Process the various ALTER TABLE statements that are relevant to our processing.
	 * @param alter ALTER TABLE statement
	 * @throws TableNotFoundException
	 * @throws ColumnNotFoundException
	 * @throws UnsupportedExpressionException
	 */
	private void alterTable(Alter alter)
	throws TableNotFoundException, ColumnNotFoundException, UnsupportedExpressionException {
		Table table = alter.getTable();
		TableInfo tableInfo = tableInfoMap.getTableInfo(table);
		if (tableInfo == null) {
			throw new TableNotFoundException("table " + table + " in statement: " + alter +
											 " not found in previous CREATE TABLE statement");
		}
		List<AlterExpression> alterExpressionList = alter.getAlterExpressions();
		for (AlterExpression alterExpression : alterExpressionList) {
			AlterOperation op = alterExpression.getOperation();
			if (op == AlterOperation.ADD) {
				alterAddToTable(alterExpression, tableInfo);
			} else if (op == AlterOperation.DROP) {
				String columnName = alterExpression.getColumnName();
				if (columnName != null) {
					if (!tableInfo.removeColumn(columnName)) {
						throw new ColumnNotFoundException("in ALTER TABLE statement: " + alter +
								" the column " + columnName + " was not found in table: " + tableInfo);
					}
				} else {
					throw new ColumnNotFoundException("in ALTER TABLE statement: " + alter +
													  " the DROP expression has no column");
				}
			} else if ((op == AlterOperation.MODIFY) || (op == AlterOperation.ALTER)) {
				throw new UnsupportedExpressionException("ALTER statement " + alterExpression + " isn't supported");
			} else if (op == AlterOperation.RENAME) {
				String oldColumnName = alterExpression.getColumnOldName();
				String newColumnName = alterExpression.getColumnName();
				tableInfo.renameColumn(oldColumnName, newColumnName);
			}
		}
	}

	/**
	 * Apply the modifications from the ALTER TABLE ADD COLUMN statement to the tableInfo
	 * ADD INDEX: add the index to the index list.  Set it as the primary key if it's unique
	 * 			  NOTE: should we actually do that?
	 * PRIMARY KEY: reset the primary key.
	 *
	 * @param alterExpression
	 * @param tableInfo
	 */
	private void alterAddToTable(AlterExpression alterExpression, TableInfo tableInfo) {
		Index index = alterExpression.getIndex();
		if (index != null) {
			tableInfo.addIndex(index);
			if ((index.getType() != null) && index.getType().equals(Constants.SQLKeywords.UNIQUE)) {
				setPrimaryKeys(tableInfo, index.getColumnsNames());
			}
		} else if ((alterExpression.getPkColumns() != null) && !alterExpression.getPkColumns().isEmpty()) {
			setPrimaryKeys(tableInfo, alterExpression.getPkColumns());
		} else if (alterExpression.getColumnName() != null) {
			String columnName = alterExpression.getColumnName();
			if ((alterExpression.getColDataTypeList() != null) && !alterExpression.getColDataTypeList().isEmpty()) {
				AlterExpression.ColumnDataType columnDataType = alterExpression.getColDataTypeList().get(0);
				ColumnInfo columnInfo = new ColumnInfo(tableInfo, columnName, columnDataType);
				tableInfo.addColumn(columnInfo);
			}
		}
	}

	private void setPrimaryKeys(TableInfo tableInfo, List<String> columnNames) {
		columnNames.stream()
				.forEach(columnName -> {
					ColumnInfo columnInfo = tableInfo.getColumnInfo(columnName);
					if (columnInfo != null) {
						columnInfo.setPrimaryKey(true);
					}
				});
	}

	private void createIndex(CreateIndex createIndex) {
		TableInfo tableInfo = tableInfoMap.getTableInfo(createIndex.getTable());
		tableInfo.addIndex(createIndex.getIndex());
	}

	private void drop(Drop drop) throws TableNotFoundException {
		// often, we drop tables and indices IF EXISTS before creating them
		if (drop.getType().equalsIgnoreCase(Constants.SQLKeywords.INDEX)) {
			if (!drop.isIfExists()) {
				if (!tableInfoMap.dropIndex(drop.getName().getName())) {
					throw new TableNotFoundException("while executing " + drop + "failed to drop index " + drop.getName().getName());
				}
			}
		} else if (drop.getType().equalsIgnoreCase(Constants.SQLKeywords.TABLE)) {

			if (!drop.isIfExists()) {
				if (!tableInfoMap.dropTable(drop.getName().getName())) {
					throw new TableNotFoundException("while executing " + "failed to drop table " + drop.getName().getName());
				}
			}
		}
	}

	/**
	 * Same as above, but read from a file.
	 * @param file file containing SQL statements.
	 * @throws Exception can't read the file, or can't parse the SQL, or can't resolve the SQL
	 * @return TableInfoMap
	 */
    public TableInfoMap parse(File file) throws Exception {
		parse(FileUtil.readFileToString(file));
		return getTableInfoMap();
    }

	/**
	 * Return the populated tableInfoMap.
	 * @return
	 */
	public TableInfoMap getTableInfoMap() {
		return tableInfoMap;
	}
}
