package com.apptechnik.appinabox.engine.parser.format;

import java.io.OutputStream;
import java.sql.Array;

public class FormatArray implements Formatter {
    public void formatStream(Object arg, OutputStream os) throws Exception {
        if (arg instanceof Array) {
            Array sqlArray = (Array) arg;
            Object[] array = (Object[]) sqlArray.getArray();
            int sqlType = ((Array) arg).getBaseType();
            Formatter formatter = Formatter.getFormatter(sqlType);
            for (int i = 0; i < array.length; i++) {
                formatter.formatStream(array[i], os);
            }

        }


    }
}
