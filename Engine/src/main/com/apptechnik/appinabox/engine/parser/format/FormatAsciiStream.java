package com.apptechnik.appinabox.engine.parser.format;

import com.apptechnik.appinabox.engine.exceptions.TypeMismatchException;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Base64;

public class FormatAsciiStream implements Formatter {
    @Override
    public void formatStream(Object arg, OutputStream os) throws Exception {
        int size = 128;
        if (arg instanceof InputStream) {
            byte[] array = new byte[size];
            do {
                int nbytes = ((InputStream) arg).read(array);
                if (nbytes > 0) {
                    os.write(array, 0, nbytes);
                }
                if (nbytes < size) {
                    break;
                }
            } while (true);
        }
        throw new TypeMismatchException(arg.getClass().getName() + " is not an InputStream");
    }
}
