package com.apptechnik.appinabox.engine.parser.format;

import com.apptechnik.appinabox.engine.exceptions.TypeMismatchException;

import java.io.OutputStream;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;

public class FormatBigDecimal implements Formatter {
    @Override
    public void formatStream(Object arg, OutputStream os) throws Exception {
        if (arg instanceof BigDecimal) {
            String s = ((BigDecimal) arg).toString();
            os.write(s.getBytes(StandardCharsets.UTF_8));
        }
        throw new TypeMismatchException(arg.getClass().getName() + " is not a Double");
    }
}
