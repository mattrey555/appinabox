package com.apptechnik.appinabox.engine.parser.format;

import com.apptechnik.appinabox.engine.exceptions.TypeMismatchException;

import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;

public class FormatBigInt implements Formatter {
    @Override
    public void formatStream(Object arg, OutputStream os) throws Exception {
        if (arg instanceof BigInteger) {
            String s = ((BigInteger) arg).toString();
            os.write(s.getBytes(StandardCharsets.UTF_8));
        }
        throw new TypeMismatchException(arg.getClass().getName() + " is not a BigInteger");
    }
}
