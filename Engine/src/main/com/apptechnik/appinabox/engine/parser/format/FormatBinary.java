package com.apptechnik.appinabox.engine.parser.format;

import com.apptechnik.appinabox.engine.exceptions.TypeMismatchException;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Base64;

public class FormatBinary implements Formatter {
    @Override
    public void formatStream(Object arg, OutputStream os) throws Exception {
        int size = 128;
        int encodedSize = 4*((size + 4)/3);
        Base64.Encoder encoder = Base64.getEncoder();
        if (arg instanceof InputStream) {
            byte[] barray = new byte[size];
            byte[] encodedArray = new byte[encodedSize];
            do {
                int nbytes = ((InputStream) arg).read(barray);
                if (nbytes == size) {
                    int actualEncodedSize = encoder.encode(barray, encodedArray);
                    os.write(encodedArray, 0, actualEncodedSize);
                } else if (nbytes > 0) {
                    byte[] truncatedBArray = Arrays.copyOf(barray, nbytes);
                    int actualEncodedSize = encoder.encode(truncatedBArray, encodedArray);
                    os.write(encodedArray, 0, actualEncodedSize);
                } else {
                    break;
                }
            } while (true);
        }
        throw new TypeMismatchException(arg.getClass().getName() + " is not a BinaryStream");
    }
}
