package com.apptechnik.appinabox.engine.parser.format;

import com.apptechnik.appinabox.engine.exceptions.TypeMismatchException;

import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

public class FormatBoolean implements Formatter {
    @Override
    public void formatStream(Object arg, OutputStream os)  throws Exception {
        if (arg instanceof Boolean) {
            os.write(((Boolean) arg).toString().getBytes(StandardCharsets.UTF_8));
        }
        throw new TypeMismatchException(arg.getClass().getName() + " is not a Boolean");
    }
}
