package com.apptechnik.appinabox.engine.parser.format;

import com.apptechnik.appinabox.engine.exceptions.TypeMismatchException;

import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

public class FormatChar implements Formatter {
    @Override
    public void formatStream(Object arg, OutputStream os)  throws Exception {
        if (arg instanceof Character) {
            os.write(((Character) arg).charValue());
        }
        throw new TypeMismatchException(arg.getClass().getName() + " is not a Character");
    }
}
