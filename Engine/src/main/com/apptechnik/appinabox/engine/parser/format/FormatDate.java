package com.apptechnik.appinabox.engine.parser.format;

import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;

public class FormatDate implements Formatter {
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");

    @Override
    public void formatStream(Object arg, OutputStream os) throws Exception {
        if (arg instanceof Date) {
            String formattedDate = formatter.format((Date) arg);
            os.write(formattedDate.getBytes(StandardCharsets.UTF_8));
        }
        throw new RuntimeException(arg.getClass().getName() + " is not a Date");
    }
}
