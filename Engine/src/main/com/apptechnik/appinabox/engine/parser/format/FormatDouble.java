package com.apptechnik.appinabox.engine.parser.format;

import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

public class FormatDouble implements Formatter {
    @Override
    public void formatStream(Object arg, OutputStream os) throws Exception {
        if (arg instanceof Double) {
            os.write(((Double) arg).toString().getBytes(StandardCharsets.UTF_8));
        }
        throw new RuntimeException(arg.getClass().getName() + " is not a Double");
    }
}
