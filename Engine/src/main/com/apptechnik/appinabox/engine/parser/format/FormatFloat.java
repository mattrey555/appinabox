package com.apptechnik.appinabox.engine.parser.format;

import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

public class FormatFloat implements Formatter {
    @Override
    public void formatStream(Object arg, OutputStream os) throws Exception {
        if (arg instanceof Float) {
            os.write(((Float) arg).toString().getBytes(StandardCharsets.UTF_8));
        }
        throw new RuntimeException(arg.getClass().getName() + " is not a Float");
    }
}
