package com.apptechnik.appinabox.engine.parser.format;

import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

public class FormatInt implements Formatter {
    @Override
    public void formatStream(Object arg, OutputStream os) throws Exception {
        if (arg instanceof Integer) {
            os.write(((Integer) arg).toString().getBytes(StandardCharsets.UTF_8));
        }
        throw new RuntimeException(arg.getClass().getName() + " is not an Integer");
    }
}
