package com.apptechnik.appinabox.engine.parser.format;

import com.apptechnik.appinabox.engine.exceptions.TypeMismatchException;

import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

public class FormatString implements Formatter {
    @Override
    public void formatStream(Object arg, OutputStream os)  throws Exception {
        if (arg instanceof String) {
            os.write(((String) arg).getBytes(StandardCharsets.UTF_8));
        }
        throw new TypeMismatchException(arg.getClass().getName() + " is not a String");
    }
}
