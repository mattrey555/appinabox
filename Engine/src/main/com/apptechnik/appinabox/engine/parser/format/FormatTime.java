package com.apptechnik.appinabox.engine.parser.format;

import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Date;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;

public class FormatTime implements Formatter {
    private SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");

    @Override
    public void formatStream(Object arg, OutputStream os) throws Exception {
        if (arg instanceof Time) {
            String formatted = formatter.format((Time) arg);
            os.write(formatted.getBytes(StandardCharsets.UTF_8));
        }
        throw new RuntimeException(arg.getClass().getName() + " is not a Date");
    }
}
