package com.apptechnik.appinabox.engine.parser.format;

import java.io.OutputStream;
import java.sql.SQLException;
import java.sql.Types;

/**
 * Formatting methods write to an output stream to reduce the amount of memory for temporary
 * strings.
 */
public interface Formatter {
    void formatStream(Object arg, OutputStream os) throws Exception;


    public static Formatter getFormatter(int sqlType) throws SQLException {
        switch (sqlType) {
            case Types.ARRAY:
                return new FormatArray();
            case Types.BIGINT:
                return new FormatBigInt();
            case Types.BIT:
            case Types.BOOLEAN:
                return new FormatBoolean();
            case Types.BINARY:
            case Types.BLOB:
            case Types.SQLXML:
                return new FormatBinaryStream();
            case Types.CHAR:
                return new FormatChar();
            case Types.CLOB:
            case Types.NCLOB:
                return new FormatAsciiStream();
            case Types.DATALINK:
            case Types.NCHAR:
            case Types.LONGNVARCHAR:
            case Types.LONGVARCHAR:
            case Types.VARCHAR:
                return new FormatString();
            case Types.DATE:
                return new FormatDate();
            case Types.DECIMAL:
            case Types.DOUBLE:
                return new FormatDouble();
            case Types.DISTINCT:
            case Types.INTEGER:
            case Types.NUMERIC:
            case Types.ROWID:
            case Types.SMALLINT:
            case Types.TINYINT:
                return new FormatInt();
            case Types.FLOAT:
                return new FormatFloat();
            case Types.JAVA_OBJECT:
                throw new SQLException("Java Object Type is not supported");
                // TODO: investigate use of getBinaryStream()
            case Types.LONGVARBINARY:
            case Types.VARBINARY:
                return new FormatBinary();
            // TODO: temporary
            case Types.REF:
            case Types.REF_CURSOR:
                throw new SQLException("Ref Type is not supported");

            case Types.STRUCT:
                throw new SQLException("STRUCT types are not supported");
            case Types.TIME:
            case Types.TIME_WITH_TIMEZONE:
                return FormatTime();
            case Types.TIMESTAMP:
            case Types.TIMESTAMP_WITH_TIMEZONE:
                return FormatTimestamp();
            default:
                throw new SQLException("unsupported java type code " + sqlType);
        }

    }

}
