package com.apptechnik.appinabox.engine.parser.json;

import com.apptechnik.appinabox.engine.exceptions.TokenException;
import com.apptechnik.appinabox.engine.util.StringUtil;
import com.apptechnik.appinabox.library.util.StringUtils;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Tokens for JSON expressions
 */
public class JSONToken {
    private static final int READ_AHEAD_LIMIT = 32;
    private Reader reader;
    private int position;
    private int markPosition = 0;

    // supported token types: { } , : . [A-Za-z_][A-Za-z0-9_*] [space|tab|CR] *
    public static enum TokenType {
        OPEN_CURLY,
        CLOSE_CURLY,
        OPEN_SQUARE,
        CLOSE_SQUARE,
        COMMA,
        COLON,
        DOT,
        IDENT,
        WHITESPACE,
        STAR,
        END
    }

    public static class Token {
        public final TokenType type;
        public final String val;
        public final int position;

        public Token(TokenType type, String val, int position) {
            this.type = type;
            this.val = val;
            this.position = position;
        }

        public String toString() {
            return (val != null) ? val : "";
        }
    }

    /**
     * wrapper for list of tokens (actually kind of a stack)
     */
    public static class TokenList {
        private List<Token> tokenList;

        public TokenList(List<Token> tokenList) {
            this.tokenList = tokenList;
        }

        public Token peek() {
            return tokenList.get(0);
        }

        public Token pop() {
            Token t = tokenList.get(0);
            tokenList = tokenList.subList(1, tokenList.size());
            return t;
        }

        public String toString() {
            StringBuffer sb = new StringBuffer();
            tokenList.forEach(t ->sb.append(t.toString() + " "));
            return sb.toString();
        }
    }

    public JSONToken(Reader reader) {
        this.reader = reader;
        this.position = 0;
    }

    /**
     * Tokenize a string
     * @param s string to tokenize.
     * @return
     * @throws IOException
     * @throws TokenException
     */
    public static TokenList tokenList(String s) throws IOException, TokenException {
        JSONToken JSONToken = new JSONToken(new StringReader(s));
        List<Token> tokenList = new ArrayList<>();
        Token token;
        try {
            do {
                token = JSONToken.nextToken();
                tokenList.add(token);
            } while (token.type != TokenType.END);
        } catch (TokenException ex) {
            throw new TokenException("threw " + ex.getMessage() + " at " + tokenList.toString());
        }
        return new TokenList(tokenList);
    }

    /**
     * Tokenize from characters read from the reader, which we assume is a string reader,
     * or at least supports mark/reset.
     * @return Token
     * @throws IOException if the reader can't be read.
     * @throws TokenException if a character wasnt recognized.
     */
    public Token nextToken() throws IOException, TokenException {
        Token token = null;
        do {
             token = nextTokenWithWhitespace(reader);
        } while (token.type == TokenType.WHITESPACE);
        return token;
    }

    private int read() throws IOException {
        int c = reader.read();
        position++;
        return c;
    }

    private void mark() throws IOException {
        markPosition = position;
        reader.mark(READ_AHEAD_LIMIT);
    }

    private void reset() throws IOException {
        reader.reset();
        position = markPosition;
    }

    public Token nextTokenWithWhitespace(Reader reader) throws IOException, TokenException {
        int c =  read();
        if (c == -1) {
            return new Token(TokenType.END, null, position);
        } else {
            char ch = (char) c;
            if (Character.isWhitespace(ch)) {
                char[] chs = new char[]{ch};
                return new Token(TokenType.WHITESPACE, new String(chs), position);
            } else if (ch == '{') {
                return new Token(TokenType.OPEN_CURLY, "{", position);
            } else if (ch == '}') {
                return new Token(TokenType.CLOSE_CURLY, "}", position);
            } else if (ch == '[') {
                return new Token(TokenType.OPEN_SQUARE, "[", position);
            } else if (ch == ']') {
                return new Token(TokenType.CLOSE_SQUARE, "]", position);
            } else if (ch == ':') {
                return new Token(TokenType.COLON, ":", position);
            } else if (ch == ',') {
                return new Token(TokenType.COMMA, ",", position);
            } else if (ch == '.') {
                return new Token(TokenType.DOT, ".", position);
            } else if (ch == '*') {
                return new Token(TokenType.STAR, "*", position);
            } else if (Character.isLetter(ch) || (ch == '_')) {
                reader.mark(1);
                StringBuffer sb = new StringBuffer();
                while (Character.isLetterOrDigit(ch) || (ch == '_')) {
                    sb.append(ch);
                    reader.mark(1);
                    ch = (char) reader.read();
                }
                reset();
                return new Token(TokenType.IDENT, sb.toString(), position);
            } else {
                throw new TokenException("unrecognized character " + ch);
            }
        }
    }
}
