package com.apptechnik.appinabox.engine.parser.json;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.exceptions.TokenException;
import com.apptechnik.appinabox.engine.parser.pojos.JsonFieldReference;
import com.apptechnik.appinabox.engine.parser.pojos.FieldReferenceNode;
import com.apptechnik.appinabox.engine.parser.pojos.QueryFieldsNode;
import com.apptechnik.appinabox.engine.parser.pojos.Tree;

import java.io.IOException;
import java.text.ParseException;

/**
 * Take an expression of the form:
 * {@code 
 * { <expr-list></expr-list> }
 * expr-list : <expr> | <expr>,<expr-list>
 * expr: * | identifier | identifier.identifier | identifier.* | identifier : { <expr-list> } | identifier : [ expr ]
 * identifier:[_A-Za-z][A-Za-z0-9]*
 * }
 * FUTURE: allow provide remapping of field names identifier : expr
 */
public class ParseJSONMapExpression {

    public static Tree<QueryFieldsNode> parse(String s) throws Exception {
        QueryFieldsNode.Factory factory = new QueryFieldsNode.Factory();

        // Tokenize the string by our pseudo-json tokenizer
        JSONToken.TokenList tokenList = JSONToken.tokenList(s);
        QueryFieldsNode rootNode = factory.create();
        Tree<QueryFieldsNode> selectItemRoot = new Tree(rootNode);
        parseObject(tokenList, selectItemRoot, factory);
        return selectItemRoot;
    }

    /**
     * Factory to create objects which extend FieldReferenceNode
     * @param <T> class which extended FieldReferenceNode
     * @param <S>
     */
    public interface Factory<T extends FieldReferenceNode, S extends JsonFieldReference> {
        T create();
        S createRef(String column, String table);
    }

    public static <T extends FieldReferenceNode, S extends JsonFieldReference> Tree<T> parse(String s, Factory<T, S> f)
    throws IOException, ParseException, TokenException {
        JSONToken.TokenList tokenList = JSONToken.tokenList(s);
        Tree<T> selectItemRoot = new Tree(f.create());
        parseObject(tokenList, selectItemRoot, f);
        return selectItemRoot;
    }

    /**
     * Parse an object from a token list into a select item jsonTree.
     * @param tokenList tokenized list
     * @param jsonTree { jsonTree, of :{ select items }, grouped : {  by, curly, braces }}
     * @throws ParseException
     */
    private static<T extends FieldReferenceNode, S extends JsonFieldReference> void parseObject(JSONToken.TokenList tokenList,
                                                                                                Tree<T> jsonTree,
                                                                                                Factory<T, S> f) throws ParseException {
        JSONToken.Token open = tokenList.pop();
        if ((open.type != JSONToken.TokenType.OPEN_CURLY) && (open.type != JSONToken.TokenType.OPEN_SQUARE)) {
            throw new ParseException(open.val + " is not an open curly brace or square bracket ",  open.position);
        }
        S ref = null;
        Tree<T> selectItemChild = null;
        for (JSONToken.Token next = tokenList.peek(); next.type != JSONToken.TokenType.END; next = tokenList.pop()) {
            if (next.type == JSONToken.TokenType.IDENT) {
                ref = parseColumnReference(next, tokenList, f);
            } else if (next.type == JSONToken.TokenType.STAR) {
                ref = f.createRef(Constants.SQLKeywords.ALL_COLUMNS, null);
            } else if (next.type == JSONToken.TokenType.COMMA) {
                if ((ref == null) && (selectItemChild == null)) {
                    throw new ParseException("Expected an identifier or a child object before a comma ", next.position);
                }
                if (ref != null) {
                    jsonTree.get().addFieldReference(ref);
                }
                ref = null;
            } else if (next.type == JSONToken.TokenType.COLON) {
                if (ref == null) {
                    throw new ParseException("Expected an identifier before a colon ", next.position);
                }
                // NOTE: I should not have parsed this into a JSQL class before I knew what it is, so
                // I'm changing its semantics here.
                selectItemChild = new Tree<T>(f.create());
                selectItemChild.get().setName(ref.toString());
                jsonTree.addChild(selectItemChild);
                parseObject(tokenList, selectItemChild, f);
                ref = null;
            } else if (next.type == JSONToken.TokenType.CLOSE_CURLY) {
                if (ref != null) {
                    jsonTree.get().addFieldReference(ref);
                }
                return;
            } else if (next.type == JSONToken.TokenType.CLOSE_SQUARE) {
                if (ref != null) {
                    jsonTree.get().addFieldReference(ref);
                }
                if (jsonTree.get().getFieldReferenceCount() > 1) {
                    throw new ParseException("JSON Arrays can only have 1 element " + jsonTree.get(), next.position);
                } else {
                    jsonTree.get().setPrimitiveArray(true);
                }
                return;
            } else {
                throw new ParseException("unrecognized token: " + next.val + " in " + jsonTree.get(), next.position);
            }
        }
    }

    /**
     * A column reference in a nesting expression can be:
     * Column (which may refer to an alias)
     * Table.Column (where Table may refer to an alias)
     * Table.* (where Table may refer to an alias)
     * @param ident
     * @param tokenList
     * @return
     * @throws ParseException
     */
    private static <T extends FieldReferenceNode, S extends JsonFieldReference> S parseColumnReference(JSONToken.Token ident,
                                                                                                       JSONToken.TokenList tokenList,
                                                                                                       Factory<T, S> f) throws ParseException {
        if (ident.type != JSONToken.TokenType.IDENT) {
            throw new ParseException(ident.val + " is not an idenfifier ", ident.position);
        }
        JSONToken.Token next = tokenList.peek();
        if (next.type == JSONToken.TokenType.DOT) {
            JSONToken.Token dot = tokenList.pop();
            JSONToken.Token field = tokenList.pop();
            if (field.type == JSONToken.TokenType.IDENT) {
                return f.createRef(field.val, ident.val);
            } else if (field.type == JSONToken.TokenType.STAR) {
                return f.createRef(Constants.SQLKeywords.ALL_COLUMNS, ident.val);
            } else {
                throw new ParseException(field.val + " is not an idenfifier or all table columns", field.position);
            }
        } else {
            return f.createRef(ident.val, null);
        }
    }
}
