package com.apptechnik.appinabox.engine.parser.pojos;

import com.apptechnik.appinabox.engine.parser.ColumnInfo;
import com.apptechnik.appinabox.engine.parser.TableInfoMap;

import java.util.Collection;
import java.util.List;

/**
 * A return structure, basically, because you can't have multi-return in java.
 */
public class ColumnsAndKeys {
    private Collection<ColumnInfo> columnInfoSet;
    private Collection<ColumnInfo> primaryKeyList;
    private TableInfoMap selectTableInfoMap;

    public ColumnsAndKeys(Collection<ColumnInfo> columnInfoSet,
                          Collection<ColumnInfo> primaryKeyList,
                          TableInfoMap selectTableInfoMap) {
        this.columnInfoSet = columnInfoSet;
        this.primaryKeyList = primaryKeyList;
        this.selectTableInfoMap = selectTableInfoMap;
    }

    public Collection<ColumnInfo> getPrimaryKeyList() {
        return primaryKeyList;
    }

    public Collection<ColumnInfo> getColumnInfoSet() {
        return columnInfoSet;
    }

    public TableInfoMap getSelectTableInfoMap() {
        return selectTableInfoMap;
    }

    public void setColumnInfoSet(List<ColumnInfo> columnInfoSet) {
        this.columnInfoSet = columnInfoSet;
    }

    public void setPrimaryKeyList(List<ColumnInfo> primaryKeyList) {
        this.primaryKeyList = primaryKeyList;
    }

    public void setSelectTableInfoMap(TableInfoMap selectTableInfoMap) {
        this.selectTableInfoMap = selectTableInfoMap;
    }
}

