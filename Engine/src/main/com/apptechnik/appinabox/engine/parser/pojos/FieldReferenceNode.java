package com.apptechnik.appinabox.engine.parser.pojos;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.codegen.util.CodegenUtil;
import com.apptechnik.appinabox.engine.exceptions.AmbiguousNameException;
import com.apptechnik.appinabox.engine.exceptions.JSONFormatException;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Tree node of field references to the JSON mapping spec.
 */
public abstract class FieldReferenceNode<T extends JsonFieldReference> {
    private String name;                   // name of this node.
    private List<T> fieldReferenceList;    // list of field references.
    private boolean primitiveArray;        // represents an array of primitives (String/Integer/etc)

    public FieldReferenceNode(List<T> fieldReferenceList) {
        this.fieldReferenceList = fieldReferenceList;
        this.primitiveArray = false;
    }

    public FieldReferenceNode() {
        this(new ArrayList<>());
    }

    public FieldReferenceNode(String name) {
        this();
        setName(name);
    }

    public FieldReferenceNode(FieldReferenceNode node) {
        this.name = name;
        this.fieldReferenceList = fieldReferenceList;
        this.primitiveArray = primitiveArray;
    }

    public abstract boolean isSingleField();
    /**
     * The json may match multiple SELECT statements which generate package names from matching JSON
     * nodes. Scan the tree for the first level which has references.  The SELECT statements MUST
     * match nodes at the same level, either root or its children.
     * @param <T>
     */
    private static class GetRootNodes<T extends FieldReferenceNode> implements Tree.ParamConsumer<Stack<Tree<T>>, List<T>> {
        private int rootDepth;

        public GetRootNodes() {
            rootDepth = -1;
        }

        @Override
        public void applyResult(Stack<Tree<T>> stack, List<T> rootNodeList) {
            Tree<T> tree = stack.peek();
            if (!tree.get().getFieldReferenceList().isEmpty()) {
                if (rootDepth == -1) {
                    rootDepth = stack.size();
                }
                if (stack.size() == rootDepth) {
                    rootNodeList.add(tree.get());
                }
            }
        }
    }


    public static <T extends FieldReferenceNode> List<T> getRootNodes(Tree<T> tree) throws Exception {
        return tree.traverseTreeStack(new GetRootNodes(), new ArrayList<T>());
    }

    public static <T extends FieldReferenceNode> List<String> getNodeNames(Tree<T> tree) throws Exception {
        return FieldReferenceNode.getRootNodes(tree).stream()
                .map(node -> node.getVarName())
                .collect(Collectors.toList());
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVarName() {
        return this.name;
    }

    public String getSQLClassName() {
        return this.name + Constants.Suffixes.SQL_FNS;
    }

    public String getClassName() {
        return this.name;
    }

    public String getFullClassName(CreateObjects createObjects) {
        return createObjects.getQueryPackageName() + "." + getClassName();
    }

    public void setFieldReferenceList(List<T> fieldReferenceList) {
        this.fieldReferenceList = fieldReferenceList;
    }

    public List<T> getFieldReferenceList() {
        return fieldReferenceList;
    }

    public void addFieldReference(T fieldRef) {
        fieldReferenceList.add(fieldRef);
    }

    public int getFieldReferenceCount() {
        return fieldReferenceList.size();
    }

    public JsonFieldReference getFieldReferenceAt(int index) {
        return fieldReferenceList.get(index);
    }

    public boolean isPrimitiveArray() {
        return primitiveArray;
    }

    public void setPrimitiveArray(boolean primitiveArray) {
        this.primitiveArray = primitiveArray;
    }

    /**
     * Create an accessor of the form
     * {@code
     * public List<<typeName>> get<Var-nmee>() {
     *     return varName;
     * }
     * }
     * @return function definition described above.
     */
    public FunctionDefinition childListFieldAccessor(CodeGen codegen) {
        GenericType listType = codegen.listType(codegen.classType(getClassName()));
        return CodegenUtil.accessorDefinition(codegen, listType, codegen.var(getVarName()));
    }

    /**
     * Create a setter of the form:
     * {@code
     * public void set<Var-name>(<type> <var-name>) {
     *     this.<var-name> = <var-name>
     * }
     * }
     * @return  function definition described above
     */
    public FunctionDefinition childFieldListSetter(CodeGen codegen) {
        GenericType listType = codegen.listType(codegen.classType(getClassName()));
        return CodegenUtil.setterDefinition(codegen, listType, codegen.var(getVarName()));
    }

    /**
     * Create a AssignmentStatement of the form:
     * {@code <child> = new ArrayList<<child>>>(); }
     * @return
     */
    public AssignmentStatement childListAllocation(CodeGen codegen) {
        return CodegenUtil.listAllocAssignMember(codegen, codegen.var(getVarName()), codegen.classType(getClassName()));
    }

    /**
     * For a field whose primary keys are the same as a parent, it is a scalar, not a list.
     * @param codegen
     * @return
     */
    public MemberDeclaration childFieldDeclaration(CodeGen codegen, String accessModifier) {
        return codegen.memberDecl(accessModifier, codegen.classType(getVarName()), codegen.var(getClassName()));
    }

    public static List<FunctionDefinition> childAccessorsAndSetters(CodeGen codegen,
                                                                    Tree<? extends FieldReferenceNode> fieldRefTree) {
        List<FunctionDefinition> accessorsAndSetters = new ArrayList<FunctionDefinition>();
        for (FieldReferenceNode child : fieldRefTree.getChildNodes()) {
            accessorsAndSetters.add(child.childListFieldAccessor(codegen));
            accessorsAndSetters.add(child.childFieldListSetter(codegen));
        }
        return accessorsAndSetters;
    }

    /**
     * Fiend a field reference matching paramName
     * @param ref parameter name (probably a JDBC Named Parameter, optionally dot syntax (table.field || field)
     * @return field reference or null f not found.
     */
    public T matchingFieldReference(String ref) {
        String[] refParts = ref.split("\\.");
        String columnName = (refParts.length == 2) ? refParts[1] : refParts[0];
        String tableName = (refParts.length == 2) ? refParts[0] : null;
        return matchingFieldReference(tableName, columnName);
    }

    public T matchingFieldReference(List<String> ref) {
        String columnName = (ref.size() == 2) ? ref.get(1) : ref.get(0);
        String tableName = (ref.size() == 2) ? ref.get(0) : null;
        return matchingFieldReference(tableName, columnName);
    }

    public T matchingFieldReference(String tableName, String columnName) {
        T tableAndColumnMatch = getFieldReferenceList().stream()
                .filter(fieldRef -> fieldRef.matchTableAndColumn(tableName, columnName))
                .findFirst()
                .orElse(null);
        if (tableAndColumnMatch != null) {
            return tableAndColumnMatch;
        }
        if (getVarName().equals(tableName)) {
            T columnMatch = getFieldReferenceList().stream()
                    .filter(fieldRef -> fieldRef.matchTableAndColumn(null, columnName))
                    .findFirst()
                    .orElse(null);
            if (columnMatch != null) {
                return columnMatch;
            }
        }
        return null;
    }

    /**
     * Tree traversal method used in validation to accumulate the node names.
     * @param <T> extends FieldReferenceNode
     */

    private static class NodeNames<T extends FieldReferenceNode> implements Tree.ConsumerException<T> {
        private Set<String> nameSet;
        private AmbiguousNameException ex;

        public NodeNames() {
            nameSet = new HashSet<String>();
        }

        @Override
        public void accept(T node) {
            if (nameSet.contains(node.getVarName())) {
                ex = new AmbiguousNameException(node.getVarName());
            }
            nameSet.add(node.getVarName());
        }

        public Set<String> getNameSet() {
            return nameSet;
        }

        public AmbiguousNameException getException() {
            return ex;
        }
    }

    public static boolean validate(Tree<? extends FieldReferenceNode> tree) throws Exception {
        NodeNames nodeNames = new NodeNames();
        tree.traverseNode(nodeNames);
        return true;
    }

    /**
     * If there is a single select, then it's valid.
     * If there are multiple selects, then the top-level response JSON tree must have no field references
     * and the same number of children as there are select calls.
     * @param jsonTree
     */
    public static boolean validateJsonTree(Tree<? extends FieldReferenceNode> jsonTree)
            throws Exception {

        GetNodeList getNodeList = new GetNodeList();
        jsonTree.traverseNode(getNodeList);
        List<String> duplicateErrors = getNodeList.duplicateErrors();
        if (!duplicateErrors.isEmpty()) {
            throw new JSONFormatException("duplicate node names:\n" + StringUtil.concat(duplicateErrors, "\n"));
        }
        return true;
    }

    public static class GetNodeList<T extends FieldReferenceNode> implements Tree.ConsumerException<T> {
        private List<T> nodeList;

        public GetNodeList() {
            nodeList = new ArrayList<T>();
        }

        @Override
        public void accept(T treeNode) {
            nodeList.add(treeNode);
        }

        public List<T> getNodeList() {
            return nodeList;
        }

        public List<String> duplicateErrors() {
            List<String> errors = new ArrayList<String>();
            for (int i = 0; i < nodeList.size(); i++ ) {
                FieldReferenceNode node = nodeList.get(i);
                for (int j = i + 1; j < nodeList.size(); j++) {
                    FieldReferenceNode otherNode = nodeList.get(j);
                    if (node != otherNode) {
                        if (node.getVarName().equals(otherNode.getVarName())) {
                            errors.add(node.getVarName() + " is duplicated between " + node + " and " + otherNode);
                        }
                    }
                }
            }
            return errors;
        }
    }

    /**
     * This field represents an array of a primitive boxed type IFF:
     * 1) it has no children
     * 2) it only has one field
     * 3) is not a wildcard.
     * @param tree
     * @return
     */
    public static boolean isSingleField(Tree<? extends FieldReferenceNode> tree) {
        return (tree.getChildCount() == 0) && tree.get().isSingleField();
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof FieldReferenceNode) {
            FieldReferenceNode b = (FieldReferenceNode) o;
            if (b.name.equals(name)) {
                return fieldReferenceList.equals(b.fieldReferenceList);
            }
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (name != null) {
            sb.append(name);
            sb.append(" : ");
        }
        sb.append(StringUtil.concat(fieldReferenceList, ", "));
        return sb.toString();
    }

    /**
     * Recursively print a tree of objects which derive from FieldReferenceNodes
     * @param tree Tree of objects which derive from FieldReferenceNode
     * @param <S> reference type contained in FieldReferenceNode.
     * @param <T> actual class.
     * @return text of the field reference nodes.
     */
    public static <S extends JsonFieldReference, T extends FieldReferenceNode<S>> String createString(Tree<T> tree) {
        StringBuilder sb = new StringBuilder();
        sb.append(tree.get().getVarName() + " : ");
        sb.append("{ ");
        sb.append(StringUtil.concat(tree.get().getFieldReferenceList(), ", "));
        boolean first = tree.get().getFieldReferenceList().isEmpty();
        for (Tree<T> childTree : tree.getChildren()) {
            if (!first) {
                sb.append(", ");
            }
            first = false;
            sb.append(createString(childTree));
        }
        return sb.toString();
    }

    /**
     * Change this path of tree nodes to FieldRefences to a dot.reference.
     * @param path collection field reference nodes (probably traced by recursion)
     * @return dot.reference of the field reference node names.
     */
    public static <T extends FieldReferenceNode> String toDotReference(Collection<Tree<T>> path) {
        return StringUtil.concat(path.stream().map(tree -> tree.get().getVarName()).collect(Collectors.toList()), ",");
    }


    /**
     * Generate the imports from children which are not primitive types (single leaf nodes).
     * @param packageName prefix package name for imports.
     * @param fieldRefTree field reference tree node (since we're getting imports from children).
     * @return list of import statements.
     */
    public static StatementList importsFromChildren(CodeGen codegen,
                                                    String packageName,
                                                    Tree<? extends FieldReferenceNode> fieldRefTree) {
        List<CodeStatement> importList = fieldRefTree.getChildren().stream()
                .filter(child -> !FieldReferenceNode.isSingleField(child))
                .map(child -> codegen.importStmt(packageName + "." + child.get().getVarName()))
                .collect(Collectors.toList());
        return codegen.stmtList(importList);
    }

}
