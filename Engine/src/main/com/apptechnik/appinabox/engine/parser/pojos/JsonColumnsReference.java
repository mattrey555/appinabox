package com.apptechnik.appinabox.engine.parser.pojos;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.exceptions.AmbiguousColumnException;
import com.apptechnik.appinabox.engine.exceptions.ColumnNotFoundException;
import com.apptechnik.appinabox.engine.parser.ColumnInfo;
import com.apptechnik.appinabox.engine.parser.TableInfoMap;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.select.AllColumns;
import net.sf.jsqlparser.statement.select.AllTableColumns;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;
import net.sf.jsqlparser.statement.select.SelectItem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * For response JSON, which matches a set of columns from the SELECT items list.
 */
public class JsonColumnsReference extends JsonFieldReference {
    private List<ColumnInfo> matchingColumnInfo;            // list of columns which match this reference

    public JsonColumnsReference(String columnName, String tableName) {
        super(columnName, tableName);
        this.matchingColumnInfo = new ArrayList<ColumnInfo>();
    }

    public JsonColumnsReference(ColumnInfo columnInfo) {
        super(columnInfo);
        this.matchingColumnInfo = Collections.singletonList(columnInfo);
    }

    public JsonColumnsReference(String columnName) {
        this(columnName, null);
    }

    public List<ColumnInfo> matchColumnList(Collection<ColumnInfo> columnInfoList) {
        List<ColumnInfo> matchingColumns =
                columnInfoList.stream()
                        .filter(columnInfo -> matchColumnInfo(columnInfo)).collect(Collectors.toList());
        setMatchingColumnInfoList(matchingColumns);
        return matchingColumns;
    }

    /**
     * Filter a list of select items against this field reference.
     * @param selectItemList
     * @return
     */
    public List<SelectItem> filterSelectItemList(List<SelectItem> selectItemList, TableInfoMap tableInfoMap)
            throws AmbiguousColumnException, ColumnNotFoundException {
        List<SelectItem> filteredList = new ArrayList<SelectItem>();
        for (SelectItem item : selectItemList) {
            if (matchSelectItem(item, tableInfoMap)) {
                filteredList.add(item);
            }
        }
        return filteredList;
    }

    /**
     * does this field reference match the selectItem from the SELECT ITEM list.
     * @param selectItem select item
     * @param tableInfoMap to get the table associated with the column if the selectItem does not specify it.
     * @return
     */
    public boolean matchSelectItem(SelectItem selectItem, TableInfoMap tableInfoMap)
            throws ColumnNotFoundException, AmbiguousColumnException {
        // match against * and t.*
        if (selectItem instanceof AllColumns) {
            return true;
        } else if (selectItem instanceof AllTableColumns) {
            AllTableColumns allTableColumns = (AllTableColumns) selectItem;
            return allTableColumns.getTable().getName().equals(getTableName());
        } else if (selectItem instanceof SelectExpressionItem) {
            if (isAllColumns()) {
                return true;
            }
            SelectExpressionItem selectExpressionItem = (SelectExpressionItem) selectItem;
            if (selectExpressionItem.getAlias() != null) {
                return matchAlias(selectExpressionItem);
            } else if (selectExpressionItem.getExpression() instanceof Column) {
                return matchColumn((Column) selectExpressionItem.getExpression(), tableInfoMap);
            }
        }
        return false;
    }

    public boolean isAllColumns() {
        return (getTableName() == null) && (getColumnName().equals(Constants.SQLKeywords.ALL_COLUMNS));
    }

    public boolean isTableColumns() {
        return (getTableName() != null) && getColumnName().equals(Constants.SQLKeywords.ALL_COLUMNS);
    }

    /**
     * Match this field Reference against a column, using a tableInfoMap for disambiguation.
     * @param col column
     * @param tableInfoMap table to compare against.
     * @return
     */
    public boolean matchColumn(Column col, TableInfoMap tableInfoMap)
            throws AmbiguousColumnException, ColumnNotFoundException {
        if (isAllColumns()) {
            return true;
        } else if (isDotForm()) {
            return (col.getTable() != null) && getTableName().equals(col.getTable().getName()) && getColumnName().equals(col.getColumnName());
        } else if (isColumnOnly()) {
            if (getTableName() != null) {
                if (col.getTable() != null) {
                    if (!getTableName().equals(col.getTable().getName())) {
                        return false;
                    }
                } else {
                    Table table = tableInfoMap.getTable(col);
                    if (!getTableName().equals(table.getName())) {
                        return false;
                    }
                }
            }
            return getColumnName().equals(col.getColumnName());
        } else if (isTableColumns()) {
            return (col.getTable() != null) && getTableName().equals(col.getTable().getName());
        } else {
            return false;
        }
    }

    /**
     * Match the alias from a selectExpressionItem if it has one.
     * @param selectExpressionItem select expression hopefully with alias
     * @return true if the selectExpressionItem has an alias and it matches.
     */
    public boolean matchAlias(SelectExpressionItem selectExpressionItem) {
        return (selectExpressionItem.getAlias() != null) && selectExpressionItem.getAlias().getName().equals(getColumnName());
    }

    public List<ColumnInfo> getMatchingColumnInfo() {
        return matchingColumnInfo;
    }

    public void addColumnInfo(ColumnInfo columnInfo) {
        matchingColumnInfo.add(columnInfo);
    }

    public void setMatchingColumnInfoList(List<ColumnInfo> columnInfoList) {
        matchingColumnInfo = columnInfoList;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof JsonColumnsReference) {
            JsonColumnsReference b = (JsonColumnsReference) o;
            if (super.equals(b)) {
                return getMatchingColumnInfo().equals(b.getMatchingColumnInfo());
            }
        }
        return false;
    }
}
