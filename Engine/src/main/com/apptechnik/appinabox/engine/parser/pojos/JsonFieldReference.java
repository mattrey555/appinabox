package com.apptechnik.appinabox.engine.parser.pojos;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.parser.*;

/**
 * Notation for a field reference.  Can be of the form:
 * * - all columns from all tables in the SELECT FROM clause
 * {@code
 * <table>.* - all columns from table
 * <column> - unique column name across tables in the SELECT FROM clause, or an alias on a select item
 *            from <column> AS <alias>
 * <table>.<column>
 * }
 */
public class JsonFieldReference {
    private final String columnName;
    private final String tableName;

    public JsonFieldReference(String columnName, String tableName) {
        this.columnName = columnName;
        this.tableName = tableName;
    }

    public JsonFieldReference(String columnName) {
        this(columnName, null);
    }

    public JsonFieldReference(JsonFieldReference jsonFieldReference) {
        this.columnName = jsonFieldReference.getColumnName();
        this.tableName = jsonFieldReference.getTableName();
    }

    public JsonFieldReference(ColumnInfo columnInfo) {
        if (columnInfo.getAlias() != null) {
            this.columnName = columnInfo.getAlias().getName();
            this.tableName = null;
        } else {
            this.columnName = columnInfo.getColumnName();
            this.tableName = columnInfo.getTableName();
        }
    }

    /**
     * convert this field reference to a string.
     * @return
     */
    @Override
    public String toString() {
        if (tableName != null) {
            return tableName + "." + columnName;
        } else {
            return columnName;
        }
    }

    public boolean isAllColumns() {
        return (getTableName() == null) && (getColumnName().equals(Constants.SQLKeywords.ALL_COLUMNS));
    }

    public boolean isTableColumns() {
        return (getTableName() != null) && getColumnName().equals(Constants.SQLKeywords.ALL_COLUMNS);
    }

    public boolean isDotForm() {
        return (getTableName() != null) && (getColumnName() != null) &&
                !getColumnName().equals(Constants.SQLKeywords.ALL_COLUMNS);
    }

    public boolean isColumnOnly() {
        return (getTableName() == null) && (getColumnName() != null) &&
                !getColumnName().equals(Constants.SQLKeywords.ALL_COLUMNS);
    }

    public String getColumnName() {
        return columnName;
    }

    public String getTableName() {
        return tableName;
    }

    /**
     * Match field to field and table.field to table.field, but not table.field to field
     * or field to field.table
     * @param tableName tableName (may be null)
     * @param columnName
     * @return
     */
    public boolean matchTableAndColumn(String tableName, String columnName) {
        if (tableName != null) {
            if ((getTableName() == null) || !getTableName().equals(tableName)) {
                return false;
            }
        } else if (getTableName() != null) {
            return false;
        }
        return getColumnName().equals(columnName);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof JsonFieldReference) {
            JsonFieldReference b = (JsonFieldReference) o;
            return matchTableAndColumn(b.getTableName(), b.getColumnName());
        }
        return false;
    }

    /**
     * Match this field reference to a column info, by alias, column, and optional table.
     * THIS WILL NOT MATCH {@code <table>.<column>} to {@code <column>} or vice versa.
     * @param columnInfo column info to match.
     * @return
     */
    public boolean matchColumnInfo(ColumnInfo columnInfo) {
        if (getColumnName().equals(Constants.SQLKeywords.ALL_COLUMNS)) {
            return (getTableName() == null) ? true : getTableName().equals(columnInfo.getTableName());
        }

        // if the column alias is synethetic (i.e we generated it), the reference matches against
        // the *actual* table and column name, and the alias is ignored.
        if ((columnInfo.getAlias() != null) && !columnInfo.isAliasSynthetic()) {
            return columnInfo.getAlias().getName().equals(getColumnName());
        }
        if ((getTableName() != null) && (columnInfo.getTableName() != null)) {
            if (!getTableName().equals(columnInfo.getTableName())) {
                return false;
            }
        }
        return getColumnName().equals(columnInfo.getColumnName());
    }

    @Override
    public int hashCode() {
        int code = columnName.hashCode();
        return (tableName != null) ? code ^ tableName.hashCode() : code;
    }
}
