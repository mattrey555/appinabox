package com.apptechnik.appinabox.engine.parser.pojos;

import com.apptechnik.appinabox.engine.parser.ColumnInfo;

import java.util.ArrayList;

/**
 * For Request JSON, where the tag matches a JSON parameter, with optional dot.syntax
 * NOTE: "*" and "t.*" formats are not allowed.
 */
public class JsonParamReference extends JsonFieldReference {
    public JsonParamReference(String columnName, String tableName) {
        super(columnName, tableName);
    }

    public JsonParamReference(String columnName) {
        super(columnName);
    }

    public JsonParamReference(ColumnInfo columnInfo) { super(columnInfo.getColumnName(), columnInfo.getTableName()); }
}
