package com.apptechnik.appinabox.engine.parser.pojos;

import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.exceptions.NameNotFoundException;
import com.apptechnik.appinabox.engine.exceptions.TypeNotFoundException;
import com.apptechnik.appinabox.engine.exceptions.UnsupportedExpressionException;
import com.apptechnik.appinabox.engine.parser.ColumnInfo;
import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.parser.json.ParseJSONMapExpression;
import com.apptechnik.appinabox.engine.util.CheckedFunction;
import com.apptechnik.appinabox.engine.util.StatementUtil;
import com.apptechnik.appinabox.engine.util.StringUtil;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.Statement;

import java.util.*;
import java.util.stream.Collectors;

/**
 * When we parse and merge INSERT/UPDATE statements, values can be returned from INSERT/UPDATE
 * in the RETURNING clause (PostGres-specific).  Those values can be used in following SQL
 * statements, whose refer to elements in nodes at or below them in the JSON mapping.
 * They are listed as additional columns.
 */
public class MergedFieldNode extends FieldReferenceNode<JsonParamReference> {
    private Map<Statement, List<ColumnInfo>> additionalColumns;
    private List<Statement> associatedStatements;
    private Map<Statement, String> SQLConstantNames;

    MergedFieldNode() {
        additionalColumns = new HashMap<Statement, List<ColumnInfo>>();
        associatedStatements = new ArrayList<Statement>();
        SQLConstantNames = null;
    }

    public static class Factory implements ParseJSONMapExpression.Factory<MergedFieldNode, JsonParamReference> {
        public MergedFieldNode create() {
            return new MergedFieldNode();
        }
        public JsonParamReference createRef(String prefix, String ref) {
            return new JsonParamReference(prefix, ref);
        }
    }

    public List<ColumnInfo> getAdditionalColumns(Statement statement) {
        return additionalColumns.get(statement);
    }

    public List<ColumnInfo> getAllAdditionalColumns() {
        return additionalColumns.values().stream().flatMap(List::stream).collect(Collectors.toList());
    }


    public void addAdditionalColumns(Statement statement, List<ColumnInfo> additionalColumns) {
        this.additionalColumns.put(statement, additionalColumns);
    }

    public boolean hasAdditionalColumns(Statement statement) {
        return (additionalColumns.get(statement) != null) && !additionalColumns.get(statement).isEmpty();
    }

    public List<Statement> getAssociatedStatements() {
        return associatedStatements;
    }

    public List<Statement> getUpdateStatements() {
        return StatementUtil.filterUpdateDeleteInsert(associatedStatements);
    }

    public void setAssociatedStatements(List<Statement> associatedStatements) {
        this.associatedStatements = associatedStatements;
    }

    public void addAssociatedStatement(Statement statement) {
        this.associatedStatements.add(statement);
    }

    /**
     * Create a map of unique identifiers (with numeric suffixes) to statements for the string
     * assignments.
     * @return
     * @throws UnsupportedExpressionException
     */
    public Map<Statement, String> createStatementVariableNames() throws UnsupportedExpressionException {
        SQLConstantNames = new HashMap<Statement, String>();

        // only create statements for change SQL statements.
        for (Statement statement : StatementUtil.filterUpdateDeleteInsert(this.associatedStatements)) {
            Table table = StatementUtil.getTable(statement);
            String varName = table.getName();
            String uniqueVarName = varName;
            int index = 1;
            while (SQLConstantNames.containsValue(uniqueVarName)) {
                uniqueVarName = varName + index++;
            }
            SQLConstantNames.put(statement, uniqueVarName);
        }
        return SQLConstantNames;
    }

    public Map<Statement, String> getSQLConstantNames() {
        return SQLConstantNames;
    }

    public String getSQLConstantName(Statement statement) {
        return SQLConstantNames.get(statement) + Constants.Suffixes.SQL;
    }

    public String getStatementVariableName(Statement statement) {
        return SQLConstantNames.get(statement) + Constants.Suffixes.STATEMENT;
    }

    public String getResultSetVariableName(Statement statement) {
        return SQLConstantNames.get(statement) + Constants.Suffixes.RESULT_SET;
    }

    public ColumnInfo matchAdditionalColumn(String name) {
        return ColumnInfo.matchColumn(name, getAllAdditionalColumns());
    }

    /**
     * Generate member fields of the form:
     * {@code <type> <child-variable-name>}
     * where type is the child's type. NOTE: if the child is a single value no children,
     * its type is a List of Boxed Primitives.
     * @param fieldRefTree JSON mapping tree.
     * @param statementParamMap map of statements to parameter maps.
     * @param accessModifier public for Kotlin, private for java.
     * @return list of member declarations.
     * @throws TypeNotFoundException the child was a boxed primitive, but its type couldn't be resolved.
     */
    public static List<MemberDeclaration> fieldsFromChildren(CodeGen codegen,
                                                             Tree<MergedFieldNode> fieldRefTree,
                                                             StatementParamMap statementParamMap,
                                                             String accessModifier) {
        return fieldRefTree.getChildren().stream()
                .map(CheckedFunction.wrap(c -> MergedFieldNode.childFieldDeclaration(codegen, c, statementParamMap, accessModifier)))
                 .collect(Collectors.toList());
    }

    /**
     * Generate member fields of the form:
     * {@code <type> <child-variable-name>}
     * where type is the child's type. NOTE: if the child is a single value no children,
     * its type is a List of Boxed Primitives.
     * {@code List<(Boxed Type from field reference + columnInfoList)> <varName>}
     * @param childTree node and its child references.
     * @param statementParamMap parameter map used for type mapping of JDBC named parameter arguments.
     * @param accessModifier public for Kotlin, private for Java
     * @return MemberDeclaration of the form described above
     * @throws TypeNotFoundException
     */
    public static MemberDeclaration childFieldDeclaration(CodeGen codegen,
                                                          Tree<MergedFieldNode> childTree,
                                                          StatementParamMap statementParamMap,
                                                          String accessModifier)
    throws TypeNotFoundException, NameNotFoundException {
        Variable var = codegen.var(childTree.get().getVarName());

        // insert children are always lists.
        return codegen.memberDecl(accessModifier, codegen.listType(getNodeType(codegen, childTree, statementParamMap)), var);
    }

    /**
     * Find the parameter map associated with this field reference.
     * @param node
     * @param statementParamMap
     * @return
     * @throws NameNotFoundException
     */
    // TODO: needs to take the tree node in the cast of name : { field }
    public ParameterMap getParamMap(MergedFieldNode node,
                                    StatementParamMap statementParamMap)
    throws NameNotFoundException {
        String treeAndField = node.getVarName() + "." + node.getFieldReferenceList().get(0).toString();
        String field = node.getFieldReferenceList().get(0).toString();
        for (Statement statement : getAssociatedStatements()) {
            ParameterMap paramMap = statementParamMap.getParamMap(statement);
            String treeAndFieldName = paramMap.jdbcParamName(treeAndField);
            if (treeAndFieldName != null) {
                return paramMap;
            }
            String fieldName = paramMap.jdbcParamName(field);
            if (fieldName != null) {
                return paramMap;
            }
        }
        throw new NameNotFoundException(node + " not found in parameter maps for " + this);
    }

    /**
     * Get the type for this tree node.  If the node is a single field and has no children, thn
     *
     * @param codegen
     * @param tree
     * @param statementParamMap
     * @return
     * @throws TypeNotFoundException
     * @throws NameNotFoundException
     */
    public static Type getNodeType(CodeGen codegen,
                                   Tree<MergedFieldNode> tree,
                                   StatementParamMap statementParamMap)
    throws TypeNotFoundException, NameNotFoundException {
        if (FieldReferenceNode.isSingleField(tree)) {
            ParameterMap paramMap = tree.get().getParamMap(tree.get(), statementParamMap);
            return getNodeType(codegen, tree, paramMap);
        } else {
            return codegen.classType(tree.get().getClassName());
        }
    }

    /**
     * Same, except with a fully qualified package name as a prefix.
     * @param codegen
     * @param packageName package name prefix.
     * @param tree tree node to get the node type for.
     * @param statementParamMap map of SQL statements to parameters and types.
     * @return
     * @throws TypeNotFoundException
     * @throws NameNotFoundException
     */
    public static Type getNodeType(CodeGen codegen,
                                   String packageName,
                                   Tree<MergedFieldNode> tree,
                                   StatementParamMap statementParamMap)
            throws TypeNotFoundException, NameNotFoundException {
        if (FieldReferenceNode.isSingleField(tree)) {
            ParameterMap paramMap = tree.get().getParamMap(tree.get(), statementParamMap);
            return tree.get().getSingleLeafNodeType(codegen, paramMap);
        } else {
            return codegen.classType(packageName + "." + tree.get().getClassName());
        }
    }

    public static Type getNodeType(CodeGen codegen, Tree<MergedFieldNode> tree, ParameterMap paramMap)
    throws TypeNotFoundException, NameNotFoundException {
        if (FieldReferenceNode.isSingleField(tree)) {
            return tree.get().getSingleLeafNodeType(codegen, paramMap);
        } else {
            return codegen.classType(tree.get().getClassName());
        }
    }

    /**
     * Get the boxed primitive simpleType referred to by this leaf node with a single field reference (used in JSON Lists)
     * @return Boxed Type (since we're going to use it in a list)
     * @throws TypeNotFoundException if the type could not be found in the type map
     * @throws NameNotFoundException if the parameter couldn't be found
     */
    public Type getSingleLeafNodeType(CodeGen codegen, ParameterMap parameterMap)
    throws TypeNotFoundException, NameNotFoundException {
        Type type = findSingleLeafNodeType(codegen, parameterMap);
        if (type == null) {
             throw new NameNotFoundException("trying to get the type for " + getFieldReferenceAt(0) +
                                            " in request JSON " + this.toString());
        }
        return type;
    }

    public Type findSingleLeafNodeType(CodeGen codegen, ParameterMap parameterMap)
    throws TypeNotFoundException {
        // Check against the named parameters, then additional columns.  If the match is against
        // an additianal column, there may be only 1 statement with one returning column

        JsonFieldReference ref = getFieldReferenceList().get(0);
        ParameterMap.ParamData paramData = parameterMap.getParamData(parameterMap.jdbcParamName(ref.toString()));
        if (paramData != null) {
            return codegen.typeFromSQLType(paramData.getDataType().getDataType(), true);
        } else {
            paramData = parameterMap.getParamData(parameterMap.jdbcParamName(getVarName() + "." + ref));
            if (paramData != null) {
                return codegen.typeFromSQLType(paramData.getDataType().getDataType(), true);
            } else if ((additionalColumns != null) && (additionalColumns.size() == 1)) {
                List<ColumnInfo> returningColumns = additionalColumns.values().iterator().next();
                if (returningColumns.size() == 1) {
                    return codegen.typeFromSQLType(returningColumns.get(0).getDataType(), true);
                }
            }
        }
        return null;
    }

    /**
     * This field represents an array of a primitive boxed type IFF:
     * 1) it has no children
     * 2) it only has one field
     * 3) is not a wildcard.
     * @return
     */
    public boolean isSingleField() {
        if (getFieldReferenceCount() == 0) {
            return additionalColumns.size() == 1;
        } else {
            return (getFieldReferenceCount() == 1) &&
                    (getFieldReferenceAt(0).isDotForm() || getFieldReferenceAt(0).isColumnOnly());
        }
    }

    public static MergedFieldNode matchNode(List<Tree<MergedFieldNode>> nodeTreeStack, String param) {
        Tree<MergedFieldNode> foundNode = matchNodeTree(nodeTreeStack, param);
        return (foundNode != null) ? foundNode.get() : null;
    }

    public static Tree<MergedFieldNode> matchNodeTree(List<Tree<MergedFieldNode>> nodeTreeStack, String param) {
        return matchTreeNode(nodeTreeStack, param.split("\\."));
    }

    /**
     * Given a parameter of the form field, parent.field, parent.parent.field, given a list of nodes
     * from the depth-first traversal of the node tree, find the best matching node.
     * @param nodeTreeStack list of field reference nodes (the merged stuff isn't used here), top of the stack
     *                  is end of the list
     * @param paramComponents dot form identifier split into components.
     * @return
     */
    public static Tree<MergedFieldNode> matchTreeNode(List<Tree<MergedFieldNode>> nodeTreeStack,
                                                      String[] paramComponents) {

        // if only a single field name is specified, then it must match the bottom node.
        // top of the stack is end of the list.
        Tree<MergedFieldNode> nodeTree = nodeTreeStack.get(nodeTreeStack.size() - 1);
        MergedFieldNode node = nodeTree.get();
        String fieldName = paramComponents[paramComponents.length - 1];
        if (paramComponents.length == 1) {
            if ((node.matchingFieldReference(fieldName) != null) || (node.matchAdditionalColumn(fieldName) != null)) {
                return nodeTree;
            }
        }

        // walk backwards up the tree to find the first node whose NAME (not field ref) matches
        // the PARENT component of this expression, and see if there is an ancestral match up
        // the tree.  Since we pushed the nodes in the stack as we recursed, the list is
        // reversed.
        for (int iNode = 0; iNode < nodeTreeStack.size(); iNode++) {
            nodeTree = nodeTreeStack.get(iNode);
            node = nodeTree.get();
            if ((node.matchingFieldReference(fieldName) != null) || (node.matchAdditionalColumn(fieldName) != null)) {
                boolean match = true;
                for (int iNodeAncestor = iNode, iComponent = paramComponents.length - 2;
                     iNodeAncestor < nodeTreeStack.size() && iComponent >= 0; iNode++, iComponent--) {
                    Tree<MergedFieldNode> ancestorNodeTree = nodeTreeStack.get(iNodeAncestor);
                    String ancestorName = paramComponents[iComponent];
                    if (!ancestorNodeTree.get().getVarName().equals(ancestorName)) {
                        match = false;
                    }
                }
                if (match) {
                    return nodeTree;
                }
            }
        }
        return null;
    }

    /**
     * From the list of statements and the JSON mapping, find the list of statements which are not assigned to
     * in the JSON mapping.
     * @param tree
     * @param statementList
     * @return
     * @throws Exception
     */
    public static List<Statement> filterUnassignedStatements(Tree<MergedFieldNode> tree, List<Statement> statementList)
    throws Exception {
        if (tree == null) {
            return statementList;
        }
        Set<Statement> statementSet = new HashSet<Statement>();
        tree.traverseNode(new GetAssignedStatements(), statementSet);
        List<Statement> assignedStatements = new ArrayList<Statement>(statementList);
        assignedStatements.removeAll(statementSet);
        return assignedStatements;
    }

    private static class GetAssignedStatements implements Tree.ParamConsumer<MergedFieldNode, Set<Statement>> {

        @Override
        public void applyResult(MergedFieldNode node, Set<Statement> statementSet) {
            statementSet.addAll(node.getAssociatedStatements());
        }
   }

    /**
     * Get the list of statements associated with the nodes in this list of nodes.
     * @param nodeTreeList
     * @return
     */
   public static List<Statement> getAssociatedStatements(List<Tree<MergedFieldNode>> nodeTreeList) {
        return nodeTreeList.stream()
                .map(node -> node.get().getAssociatedStatements())
                .flatMap(List::stream)
                .collect(Collectors.toList());
   }

    @Override
    public JsonParamReference matchingFieldReference(String tableName, String columnName) {
        JsonParamReference node = super.matchingFieldReference(tableName, columnName);
        if (node != null) {
            return node;
        }
        ColumnInfo matchingColumn = additionalColumns.values().stream().flatMap(List::stream).filter(
            col -> col.getColumnName(true).equals(columnName) &&
                                     ((tableName == null) || col.getTableName().equals(tableName)))
                    .findFirst().orElse(null);
        if (matchingColumn != null) {
            return new JsonParamReference(matchingColumn);
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append(super.toString());
        for (Statement statement : additionalColumns.keySet()) {
            sb.append(statement);
            sb.append(StringUtil.delim(additionalColumns.get(statement), ", "));
            sb.append("\n");
        }
        return sb.toString();
    }
}
