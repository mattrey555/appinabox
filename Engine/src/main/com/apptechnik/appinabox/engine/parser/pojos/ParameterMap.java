package com.apptechnik.appinabox.engine.parser.pojos;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.exceptions.*;
import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.log.Log;
import com.apptechnik.appinabox.engine.parser.ExpressionType;
import com.apptechnik.appinabox.engine.parser.types.SQLType;
import com.apptechnik.appinabox.engine.parser.TableInfoMap;
import com.apptechnik.appinabox.engine.parser.util.ColDataTypeUtils;
import com.apptechnik.appinabox.engine.util.StatementUtil;
import com.apptechnik.appinabox.engine.util.StringUtil;
import com.apptechnik.appinabox.engine.util.Substitute;
import net.sf.jsqlparser.expression.JdbcNamedParameter;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.create.table.ColDataType;
import net.sf.jsqlparser.statement.insert.Insert;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;
import net.sf.jsqlparser.statement.update.Update;

/**
 * Parameter Map: Map JdbcNamedParameter to Parameter data, which has the source expression and
 * data simpleType.
 * url: specifies the '/' prefixed URL with type substitution parameters using ${var} syntax.
 * type: {@code <type> }specifies a fixed SQL type, such as varchar.
 * header: {@code <HTTP header tag> <param-name> } specifies an HTTP header tag
 * optional: {@code <identifier> [default <constant>]} identifies an optional parameter with optional default value.
 */
public class ParameterMap extends HashMap<String, ParameterMap.ParamData> {
	private Map<String, String> dotParamMap;		// map of JDBC parameters to dot-syntax parameters
	public ParameterMap() {
		super();
	}

	public ParameterMap(ParameterMap a) {
		super(a);
		this.dotParamMap = a.getDotParamMap();
	}

	/**
	 * Parse the parameter map from the directives in CreateObject, parsed from the comments in the SQL file.
	 * Note that query and header parameters do not have a dot-syntax->JDBC parameter map.
	 * @param comments comments parsed from the .SQL file
	 * @throws PropertiesException if an optional parameter was specified, but not found
	 * @throws HeaderFormatException an HTTP header parameter was not properly specified
	 */
	public ParameterMap(List<String> comments) throws PropertiesException, HeaderFormatException {
		parseParameterMap(comments);
		this.dotParamMap = new HashMap<String, String>();
	}

	/**
	 * Create a merged parameter map from a list of statements and the statement parameter map
	 * @param statementParameterMapMap map of SQL statements to map of JDBCNamedParameters to types
	 * @param statementList list of SQL statements.
	 * @throws TypeMismatchException
	 */
	public ParameterMap(Map<Statement, ParameterMap> statementParameterMapMap,
						List<? extends Statement> statementList) throws TypeMismatchException {
		this.dotParamMap = new HashMap<String, String>();
		for (Statement statement: statementList) {
			merge(statementParameterMapMap.get(statement));
		}
		List<ParamData> undefinedSources = checkSourcesValid();
		if (!undefinedSources.isEmpty()) {
			throw new TypeMismatchException("sources were undefined " + StringUtil.concat(undefinedSources, "\n"));
		}
	}


	/**
	 * Given a SQL expression which we used dot-syntax parameters, replace the dots with underscores
	 * and generate a mapping
	 * @param sql SQL statement string.
	 * @return map of dot-syntax parameters to JDBC parameters with underscores.
	 */
	public static Map<String, String> mapDotParameters(String sql)  {
		Map<String, String> varMap = new HashMap<String, String>();
		Pattern pattern = Pattern.compile(Constants.Regexes.NAMED_DOT_PARAM_REGEXP);
		Matcher matcher = pattern.matcher(sql);
		while (matcher.find()) {
			String identifier = matcher.group();
			dotsToUnderscores(identifier.substring(1), varMap); // remove the ":"
		}
		return varMap;
	}

	/**
	 * Dot parameters uniquely match JSON, but JDBC Named Parameters are single identifiers.
	 * Create a map from "dot_syntax" to "_" which is legal for JDBCNamedParameters.
	 * NOTE: "_" will break this. TODO: what does that comment mean?
	 * @param dotRef
	 * @param varMap
	 * @return
	 */
	private static String dotsToUnderscores(String dotRef, Map<String, String> varMap) {
		String underscoreVar = dotRef.replace('.', '_');
		varMap.put(underscoreVar, dotRef);
		return underscoreVar;
	}

	/**
	 * follows retrofit substitution conventions. variables are referenced by ${identifier}
	 * @param comments list of text extracted from comments in the .SQL file
	 * @throws PropertiesException if an optional parameter was specified, but not found
	 * @throws HeaderFormatException if the directive does not have parameter and header elements.
	 */
	private void parseParameterMap(List<String> comments) throws PropertiesException, HeaderFormatException {
		List<String> types = StringUtil.filterTag(comments, Constants.Directives.TYPE);

		TypeMap paramTypeMap = new TypeMap(types);
		String url = StringUtil.findTag(comments, Constants.Directives.URL);

		// TODO: use a proper URL parser for this.
		if (url != null) {
			int ichQuery = url.indexOf('?');
			String path = (ichQuery != -1) ? url.substring(0, ichQuery) : url;
			String query = (ichQuery != -1) ? url.substring(ichQuery + 1) : null;
			Collection<String> pathVariables = Substitute.getParams(path);
			Collection<String> queryVariables = Substitute.getParams(query);
			addParams(pathVariables, paramTypeMap, ParamSource.URL_PATH);
			addParams(queryVariables, paramTypeMap, ParamSource.URL_QUERY);
		}
		addHeaderParams(StringUtil.filterTag(comments, Constants.Directives.HEADER), paramTypeMap);
		setOptionalParameters(StringUtil.filterTag(comments, Constants.Directives.OPTIONAL));
	}

	/**
	 * header: {@code <HTTP header tag> <param-name>} specifies an HTTP header tag
	 * @param headers list of strings of the form {@code <param-name> <HTTP-Header>}
	 * @param paramTypeMap map of parameter names to parameter types, specified by the :type directive
	 * @throws HeaderFormatException if the directive does not have parameter and header elements.
	 */
	public void addHeaderParams(List<String> headers, TypeMap paramTypeMap)
	throws HeaderFormatException {
		for (String headerLine : headers) {
			int ichDelim1 = headerLine.indexOf(':');
			if (ichDelim1 == -1) {
				throw new HeaderFormatException("the header line: " + headerLine +
												 " must have a parameter name and an HTTP header name");
			}
			int ichDelim2 = ichDelim1 + 1;
			String header = headerLine.substring(0, ichDelim1).trim();
			String param = headerLine.substring(ichDelim2).trim();

			// check to see if there are any ${variable} format variables, otherwise it's a single variable.
			List<String> varList = StringUtil.matchingRegexes(param, Constants.Regexes.VARIABLE_REFERENCE);
			if (!varList.isEmpty()) {
				for (String decoratedVar : varList) {
					// strip ${var} to var
					String var = StringUtil.undecorateKey(decoratedVar);
					addHeaderParam(header, var, param, paramTypeMap);
				}
			} else {
				addHeaderParam(header, param, paramTypeMap);
			}
		}
	}

	public void addReturningExpressions(Statement statement,
									     TableInfoMap tableInfoMap) throws Exception {
		List<SelectExpressionItem> selectExprItemList;
		if (statement instanceof Update) {
			selectExprItemList = ((Update) statement).getReturningExpressionList();
		} else if (statement instanceof Insert) {
			selectExprItemList = ((Insert) statement).getReturningExpressionList();
		} else {
			return;
		}
		// Expression expr, ParameterMap parameterMap, TableInfoMap tableInfoMap
		for (SelectExpressionItem selectExprItem :  selectExprItemList) {
			ColDataType colDataType = ExpressionType.getType(selectExprItem.getExpression(), this, tableInfoMap);
			String name;
			if (selectExprItem.getAlias() != null) {
				name = selectExprItem.getAlias().getName();
			} else if (selectExprItem.getExpression() instanceof Column) {
				name = ((Column) selectExprItem.getExpression()).getColumnName();
			} else {
				throw new UnsupportedExpressionException("returning complex expressions must be aliases in statement: " +
						statement + " expression: " + selectExprItem);
			}
			ParameterMap.ParamData paramData = new ParameterMap.ParamData(name, ParameterMap.ParamSource.SQL_RETURNING, colDataType);
			put(name, paramData);
		}
	}

	/**
	 * Add a header param to the parameter map.
	 * @param header header tag for param data: NOTE: NOT THE KEY USED IN THE PARAMETER MAP.
	 * @param param parameter (i.e JDBC param) to ddd
	 * @param paramTypeMap fixed type map.
	 */
	private void addHeaderParam(String header, String param, TypeMap paramTypeMap) {
		addHeaderParam(header, param, null, paramTypeMap);
	}

	/**
	 * Add a header param to the parameter map.
	 * @param header header tag for param data: NOTE: NOT THE KEY USED IN THE PARAMETER MAP.
	 * @param param parameter (i.e JDBC param) to ddd
	 * @param varFormat variable ${format} if $[expressions} are used.
	 * @param paramTypeMap fixed type map.
	 */
	private void addHeaderParam(String header, String param, String varFormat, TypeMap paramTypeMap) {
		String typeName = (paramTypeMap.get(param) != null) ? paramTypeMap.get(param) : Constants.SQLTypes.NAMED_PARAMETER;
		ColDataType colDataType = new ColDataType(typeName);
		ParamData paramData = new ParamData(header, varFormat, ParamSource.HTTP_HEADER, null, colDataType, false);
		put(param, paramData);
	}

	/**
	 * Add parameters from the URL path or query components.
	 * @param params list of URL parameters from path/query (hopefully query)
	 * @param paramTypeMap non-onto map of Parameter names to parameter types
	 * @param source source (HEADER/URL_QUERY/etc)
	 */
	private void addParams(Collection<String> params, TypeMap paramTypeMap, ParamSource source) {
		params.stream().forEach(param -> {
			String typeName = paramTypeMap.get(param, Constants.SQLTypes.NAMED_PARAMETER);
			put(param, new ParamData(source, null, new ColDataType(typeName)));
		});
	}

	/**
	 * Parse the lines of the form optional: <param-name> <default-value>
	 * @param optionalDirectives list of comments prefixed by optional:
	 * @throws PropertiesException if an optional parameter was specified, but not found
	 */
	private void setOptionalParameters(List<String> optionalDirectives) throws PropertiesException {
		for (String directive : optionalDirectives) {
			int ichSpace = StringUtil.firstWhiteSpace(directive.trim());
			String paramName = directive.substring(0, ichSpace).trim();
			String defaultValue = StringUtil.stripQuotes(directive.substring(ichSpace + 1).trim());
			ParamData paramData = get(paramName);
			if (paramData == null) {
				throw new PropertiesException(paramName + " was specified as an optional parameter, but is not specified in the URL or headers");
			}
			paramData.setOptional(true, defaultValue);
		}
	}

	public List<ParamData> checkSourcesValid() {
		return values().stream().filter(paramData -> paramData.notSpecified()).collect(Collectors.toList());
	}

	/**
	 * Resolve the variable types between parameter maps  If there is a shared parameter
	 * whose type cannot be promoted throw a TypeMismatchException.
	 * The query parameter elements default to NAMED_PARAMETER type, which is wildcard.
	 * @param b parameter Map
	 * @throws TypeMismatchException if either parameter type cannot be promoted to the other.
	 */
	public void resolveTypes(ParameterMap b) throws TypeMismatchException {
		for (String aparam : getParamSet()) {
			ParamData pda = getParamData(aparam);
			ParamData pdb = b.getParamData(aparam);
			if (pdb != null) {
				// if the type is a named parameter, copy the data type from the concrete value,
				// otherwise, check that they're compatible.
				if (pda.getDataType().getDataType().equals(Constants.SQLTypes.NAMED_PARAMETER)) {
					pda.setDataType(pdb.getDataType());
					pda.setNullable(pdb.isNullable());
					pda.setIsList(pdb.isList());
				} else {
					// throws an exception if the types aren't compatible.
					// we don't have to worry about the list case, because list and scalar aren't associated.
					ColDataType promotedDataType = ColDataTypeUtils.promoteDataType(pda.getDataType(), pdb.getDataType());
					pda.setDataType(promotedDataType);
					pdb.setDataType(promotedDataType);
				}

				// reset the source to the best defined source (i.e. URL query is better than SQL query)
				ParamSource source = pda.resolveSource(pdb);
				pda.setSource(source);
				pdb.setSource(source);
			}
		}
	}

	/**
	 * Merge another parameter map into this parameter map, applying type promotion and data source promotion.
	 * @param b
	 * @throws TypeMismatchException
	 */
	public void merge(ParameterMap b) throws TypeMismatchException {
		for (String bparam : b.getParamSet()) {
			ParamData pda = getParamData(bparam);
			ParamData pdbCopy = new ParamData(b.getParamData(bparam));
			if (pda != null) {
				ColDataType promotedDataType = ColDataTypeUtils.promoteDataType(pda.getDataType(), pdbCopy.getDataType());
				pdbCopy.setDataType(promotedDataType);
				ParamSource source = pda.resolveSource(pdbCopy);
				pdbCopy.setSource(source);
			}
			put(bparam, pdbCopy);
		}

		// merge the dot-syntax maps.  Adding elements is OK, replacing elements with different parameter names is not.
		for (String bDotSyntax : b.getDotParamMap().keySet()) {
			String bJdbcParam = b.getDotParamMap().get(bDotSyntax);
			if (dotParamMap.get(bDotSyntax) != null) {
				if (!dotParamMap.get(bDotSyntax).equals(bJdbcParam)) {
					throw new TypeMismatchException(bDotSyntax + " maps to " + dotParamMap.get(bDotSyntax) + " and " + bJdbcParam);
				}
			} else {
				dotParamMap.put(bDotSyntax, bJdbcParam);
			}
		}
	}

	/**
	 * Do the same for the parameter generated from the list of SQL expressions.
	 * @param parameterMapMap parameter generated from the list of SQL expressions.
	 * @throws TypeMismatchException
	 */
	public void applyParameterTypes(Map<Statement, ParameterMap> parameterMapMap) throws TypeMismatchException {
		for (ParameterMap paramMap : parameterMapMap.values()) {
			resolveTypes(paramMap);
		}
	}

	/**
	 * Apply the fixed types to the parameter for unresolved parameters.
	 * @param fixedParamTypeMap map of parameter names to fixed types
	 * @return list of names of warnings
	 * @throws NameNotFoundException if the fixed type parameter name was not found in this Parameter Map.
	 */
	public List<String> applyFixedParamTypes(TypeMap fixedParamTypeMap)
	throws NameNotFoundException {
		List<String> warnings = new ArrayList<String>();
		for (String fixedParamName : fixedParamTypeMap.keySet()) {
			if (hasParam(fixedParamName)) {
				String fixedType = fixedParamTypeMap.get(fixedParamName);
				ColDataType paramType = getColDataType(fixedParamName);
				if (!paramType.getDataType().equals(Constants.SQLTypes.NAMED_PARAMETER)) {
					warnings.add("parameter " + fixedParamName + " was assigned fixedType " + fixedType +
								 " but type " + paramType.getDataType() + " was already inferred");
				}
				ParamData paramData = new ParamData(getParamData(fixedParamName));
				paramData.setDataType(new ColDataType(fixedType));
				put(fixedParamName, paramData);
			} else {
				warnings.add("the fixed type " + fixedParamName + " did not match any parameters");
			}
		}
		return warnings;
	}

	/**
	 * Given a parameter map which should contain all of the parameters in this parameter map,
	 * return the list of parameters in this map not in the containingMap
	 * i.e. difference(this, containingMap)
	 * @param containingMap
	 * @return list of undefined parameters.
	 */
	public List<String> undefinedParams(ParameterMap containingMap) {
		return keySet().stream().filter(key -> !containingMap.containsKey(key)).collect(Collectors.toList());
	}

	/**
	 * Return the names of the elements in the parameter whose types match typeName (typically NAMED_PARAMETER)
	 * @param typeName type name.
	 * @return list of parameters with the specified type
	 */
	public List<String> filterByType(String typeName)  {
		return entrySet().stream()
				.filter(entry -> entry.getValue().getDataType().getDataType().equals(typeName))
				.map(entry -> entry.getKey())
				.collect(Collectors.toList());
	}

	/**
	 * Get the parameter data associated with this parameter name.
	 * @param param
	 * @return
	 */
	public ParamData getParamData(String param) {
		return get(param);
	}

	private ParamData getWithException(String name) throws NameNotFoundException {
		ParamData paramData = get(name);
		if (paramData == null) {
			throw new NameNotFoundException("named parameter " + name + " was not found in the parameter table");
		}
		return paramData;
	}

	/**
	 * Return the data simpleType associated with this named parameter
	 */
	public ColDataType getColDataType(JdbcNamedParameter param) throws NameNotFoundException {
		return getWithException(param.getName()).getDataType();
	}

	/**
	 * Get the java simpleType associated with this JdbcNamedParameter.
	 * @param name JDBC named parameter.
	 * @return
	 * @throws NameNotFoundException
	 * @throws TypeNotFoundException
	 */
	public Type getJavaType(CodeGen codegen, String name) throws NameNotFoundException, TypeNotFoundException {
		ParamData paramData = getWithException(name);
		return SQLType.getJavaTypeFromSQLType(codegen, paramData.getDataType().getDataType(), paramData.isNullable());
	}

	public Type getBoxedJavaType(CodeGen codegen, String name) throws NameNotFoundException, TypeNotFoundException {
		ParamData paramData = getWithException(name);
		return SQLType.getJavaTypeFromSQLType(codegen, paramData.getDataType().getDataType(), true);
	}

	/**
	 * The Named Parameter is referred to in a SELECT IN clause
	 * @param name Named parameter
	 * @return true if it's in.
	 * @throws NameNotFoundException variable name not found
	 * @throws TypeNotFoundException couldn't resolve variable type
	 */
	public boolean isInClause(String name) throws NameNotFoundException, TypeNotFoundException {
		return getWithException(name).isList();
	}

	/**
	 * get the dataType associated with the name of JdbcNamedParameter
	 * @param paramName JdbcNamedParameter name
	 * @return datatype of the expression associated with the JdbcNamedParameter
	 */
	public ColDataType getColDataType(String paramName) throws NameNotFoundException {
		return getColDataType(getNamedParameter(paramName));
	}

	public boolean isNullable(String paramName) throws NameNotFoundException {
		return getParamData(paramName).isNullable();
	}

	public boolean isExtract(String paramName) throws NameNotFoundException {
		return getParamData(paramName).isExtract();
	}

	public boolean isReturning(String paramName) throws NameNotFoundException {
		return getParamData(paramName).isReturning();
	}

	/**
	 * get the parameter source associated with the name of JdbcNamedParameter
	 * @param paramName JdbcNamedParameter name
	 * @return paramter source (ex QUERY, VALUE)
	 */
	public ParamSource getParamSource(String paramName) throws NameNotFoundException {
		return getWithException(paramName).getSource();
	}

	/**
	 * get the dataType associated with the name of JdbcNamedParameter
	 * @param paramName JdbcNamedParameter name
	 * @return datatype of the expression associated with the JdbcNamedParameter
	 */
	public ColDataType findColDataType(String paramName)  {
		ParamData paramData = get(paramName);
		return (paramData != null) ? paramData.getDataType() : null;
	}

	public List<String> getParamNames() {
		return new ArrayList<String>(keySet());
	}

	public JdbcNamedParameter getNamedParameter(String paramName) throws NameNotFoundException {
		if (containsKey(paramName)) {
			return new JdbcNamedParameter(paramName);
		}
		throw new NameNotFoundException("named parameter " + paramName + " was not found in the parameter map");
	}

	/**
	 * Get the source expression (where its type is generated from) associated with this parameter
	 * @param paramName parameter name.
	 * @return SQL expression.
	 * @throws NameNotFoundException
	 */
	public Expression getExpression(String paramName) throws NameNotFoundException {
		Expression expr = get(paramName).getExpression();
		if (expr != null) {
			return expr;
		} else {
			throw new NameNotFoundException("named parameter " + paramName + " was not found in the parameter table");
		}
	}

	/**
	 * Add a parameter to the parameter map
	 * @param source QUERY/HEADER/etc
	 * @param param JDBC Named parameter.
	 * @param expr containing expression
	 * @param dataType inferred data type for the parameter, or NAMED_PARAMETER if it couldn't be inferred.
	 * @param isList if the named parameter is in an IN param, then the type is list (type)
	 */
	public void put(ParamSource source,
					JdbcNamedParameter param,
					Expression expr,
					ColDataType dataType,
					boolean isList) {
		put(param.getName(), new ParamData(param.getName(), source, expr, dataType, isList));
	}

	/**
	 * Add a parameter to the parameter map
	 * @param source QUERY/HEADER/etc
	 * @param param JDBC Named parameter.
	 * @param expr containing expression
	 * @param dataType inferred data type for the parameter, or NAMED_PARAMETER if it couldn't be inferred.
	 */
	public void put(ParamSource source, JdbcNamedParameter param, Expression expr, ColDataType dataType) {
		put(param.getName(), new ParamData(param.getName(), source, expr, dataType, false));
	}

	public Set<String> getParamSet() {
		return keySet();
	}

	public Map<String, String> getDotParamMap() {
		return dotParamMap;
	}

	public void setDotParamMap(Map<String, String> dotParamMap) {
		this.dotParamMap = dotParamMap;
	}

	/**
	 * We allow a "." syntax for JDBC parameters, for unambiuous mapping to the JSON mapping.
	 * SQL doesn't allow "." references in variables, so we replace them with underscores
	 * but we need to map them to the original "."- syntax variables.
	 * @param jdbcParamName parameter name
	 * @return JDBCNamedParam name
	 */
	public String dotParamName(String jdbcParamName) {
		return (dotParamMap.get(jdbcParamName) != null) ? dotParamMap.get(jdbcParamName) : jdbcParamName;
	}

	/**
	 * Inverse mapping, because sometimes we have to go from JDBC parameters to dot-syntax parameters.
	 * @param dotParamName
	 * @return
	 */
	public String jdbcParamName(String dotParamName) {
		if (dotParamName.contains(".")) {
			for (String jdbcParamName : dotParamMap.keySet()) {
				if (dotParamMap.get(jdbcParamName).equals(dotParamName)) {
					return jdbcParamName;
				}
			}
			return null;
		} else {
			return dotParamName;
		}
	}

	/**
	 * Get the parameter set filtered by source for a varargs of ParamSource
	 * @param sourceArgs list of param source
	 * @return
	 * @throws NameNotFoundException
	 */
	public Set<String> getParamSet(ParamSource... sourceArgs) throws NameNotFoundException {
		Set<String> matchingKeys = new HashSet<String>();
		List<ParamSource> sourceList = Arrays.asList(sourceArgs);
		for (String key : keySet()) {
			if (sourceList.contains(getParamSource(key))) {
				matchingKeys.add(key);
			}
		}
		return matchingKeys;
	}

	public boolean hasParam(String param) {
		return get(param) != null;
	}

	public String toString() {
		return StringUtil.delim(values(), "\n");
	}

	public enum ParamSource {
		VALUE("SQL value"),					// INSERT, UPDATE, SELECT values
		QUERY("SQL query"),					// SELECT, DELETE, UPDATE WHERE/LIMIT clauses
		URL_PATH("URL path"),					// /dont/use/path/components/as/$[variable}
		URL_QUERY("URL query"),				// ?param=${variable}
		HTTP_HEADER("HTTP header"),			// header: <HTTPHeader> <param-name>
		REQUEST_JSON("Request JSON"),			// request JSON
		SQL_RETURNING("SQL Returning");		// from INSERT/UPDATE/DELETE returning clause

		private String name;

		ParamSource(String name) {
			this.name = name;
		}

		public static Annotation paramAnnotation(CodeGen codegen, String paramName, ParamSource source)
		throws UnsupportedParamTypeException {
			List<AnnotationArg> argList =
				Collections.singletonList(codegen.annotationArg(Constants.JavaKeywords.VALUE,
										  codegen.stringConst(paramName)));
			if (source.equals(URL_PATH)) {
				return codegen.paramAnnot(Constants.Annotations.PATH, argList);
			} else if (source.equals(URL_QUERY)) {
				return codegen.paramAnnot(Constants.Annotations.QUERY, argList);
			} else if (source.equals(HTTP_HEADER)) {
				return codegen.paramAnnot(Constants.Annotations.HEADER, argList);
			} else if (source.equals(REQUEST_JSON)) {
				return codegen.paramAnnot(Constants.Annotations.BODY, argList);
			} else {
				throw new UnsupportedParamTypeException(source + " used in " + paramName +
													    " is not a supported endpoint parameter type");
			}
		}
	}

	/**
	 * Given a list of statements, and the CREATE TABLE map, create a map of SQL statements to
	 * JDBC Named Parameters with types.
	 * @param statementList List of SQL statements.
	 * @param createTableInfoMap map of table names to table definitions.
	 * @return map of SQL statements to the JDBC Named Parameters with types.
	 * @throws Exception
	 */
	public static Map<Statement, ParameterMap> applyTypes(List<Statement> statementList,
													      TableInfoMap createTableInfoMap) throws Exception {
		Map<Statement, ParameterMap> statementParameterMap = new HashMap<Statement, ParameterMap>();
		for (Statement statement : statementList) {
			TableInfoMap statementInfoMap = StatementUtil.getTables(statement, createTableInfoMap);
			ParameterMap parameterMap = ExpressionType.getNamedParameterTypes(statement, statementInfoMap, createTableInfoMap);
			statementParameterMap.put(statement, parameterMap);
		}
		return statementParameterMap;
	}


	/**
	 * Class used for reporting parameter errors.
	 */
	public static class ParamError {
		private final String paramName;
		private final ParamData paramData;
		private final int errorType;
		private final String errorMessage;

		public ParamError(String paramName, ParamData paramData, String errorMessage, int errorType) {
			this.paramName = paramName;
			this.paramData = paramData;
			this.errorMessage = errorMessage;
			this.errorType = errorType;
		}

		public String getParamName() {
			return paramName;
		}

		public ParamData getParamData() {
			return paramData;
		}

		public int getErrorType() {
			return errorType;
		}

		@Override
		public String toString() {
			return Constants.ErrorNames[errorType] + ": " + paramName + " " + errorMessage;
		}

		public static boolean anyFatalError(List<ParamError> paramErrorList) {
			return paramErrorList.stream()
					.filter(err -> err.getErrorType() == Constants.ErrorTypes.ERROR)
					.findFirst().isPresent();
		}

		public static void writeLog(List<ParamError> paramErrorList) {
			paramErrorList.stream()
					.forEach(err -> {
						if (err.getErrorType() == Constants.ErrorTypes.ERROR) {
							Log.error(err.toString());
						} else {
							Log.warn(err.toString());
						}
					});

		}
	}

	/**
	 * All of the parameters in the query must either match a type-resolved named parameter in the SQL expression,
	 * or be specified in the fixedParamTypeMap (which we should warn, since it's unused.
	 * @param statementParameterMapMap Parameter maps from JDBCNamedParameters
	 * @param queryParams Parameter map from URL and HTTP headers.
	 * @param fixedParamTypeMap fixed type mapping.
	 * @return
	 */
	public static List<ParamError> validateParameters(Map<Statement, ParameterMap> statementParameterMapMap,
													  ParameterMap queryParams,
													  Map<String, String> fixedParamTypeMap) {
		List<ParamError> paramErrorList = new ArrayList<ParamError>();
		for (String paramName : queryParams.keySet()) {
			List<ParameterMap> matchingStatementParams = statementParameterMapMap.values().stream()
					.filter(paramMap -> paramMap.hasParam(paramName))
					.collect(Collectors.toList());
			if (matchingStatementParams.isEmpty()) {
				if (fixedParamTypeMap.containsKey(paramName)) {
					paramErrorList.add(new ParamError(paramName, queryParams.getParamData(paramName),
							                          "not used as a named parameter in any SQL statement, but has a fixed type",
												      Constants.ErrorTypes.WARNING));
				} else {
					paramErrorList.add(new ParamError(paramName, queryParams.getParamData(paramName),
													  "not used as a named parameter in any SQL statement. Either use it in a statement, or assign it a fixed type",
													 Constants.ErrorTypes.ERROR));
				}
			} else if (matchingStatementParams.size() > 1) {
				for (ParameterMap paramMap : matchingStatementParams) {
					try {
						ColDataTypeUtils.promoteDataType(paramMap.findColDataType(paramName), matchingStatementParams.get(0).findColDataType(paramName));
					} catch (TypeMismatchException tmex) {
						paramErrorList.add(new ParamError(paramName, queryParams.getParamData(paramName),
														  tmex.getMessage(),Constants.ErrorTypes.ERROR));
					}
				}
			}
		}
		return paramErrorList;
	}
	/**
	 * Create a list of statements of the form:
	 * {@code <param-name> = <conversion-package>.<conversion-method>(args[i]) }
	 * for each element in the parameter map.
	 * @param argStart starting position of commond-line arguments to convert (may change depending on file type)
	 * @return StatementList
	 * @throws TypeNotFoundException
	 * @throws NameNotFoundException
	 */
	public StatementList parametersFromArguments(CodeGen codegen, int argStart)
	throws TypeNotFoundException, NameNotFoundException {
		List<CodeStatement> statementList = new ArrayList<CodeStatement>();
		int iArg = argStart;
		Variable argVar = codegen.var(Constants.Variables.ARGS);
		for (String paramName : getParamSet()) {
			Variable varParam = codegen.var(paramName);

			// if the parameter is in the request JSON and in a SELECT IN clause, then it is a list, not a scalar.
			// TODO: also support comma-separated values in the HTTP query.
			boolean isList = isInClause(paramName) &&
							 getParamSource(paramName).equals(ParameterMap.ParamSource.REQUEST_JSON);
			Type varType = isList ? codegen.listType(getBoxedJavaType(codegen, paramName)) : getJavaType(codegen, paramName);
			String sqlType = getColDataType(paramName).getDataType();
			ArrayReference argArrayReference = codegen.arrayRef(argVar, codegen.integerConst(iArg++));
			CodeExpression conversionExpr = SQLType.convertFromString(codegen, sqlType, isList, argArrayReference);
			statementList.add(codegen.typedAssignStmt(varType, varParam, conversionExpr));
		}
		return codegen.stmtList(statementList);
	}


	// TODO: might be handy if this included the source table.
	public static class ParamData {
		private String tag;						// tag used in the HTTP header case, where the header tag is mapped to the parameter.
		private String varFormat;				// variable format for ${format} expressions in headers.
		private ParamSource source;				// source type
		private ColDataType dataType;			// SQL data type.
		private Expression expr;				// source expression
		private boolean nullable;				// can be null (represent by java Boxed primitive.
		private boolean optional;				// param is in optional: directive
		private String defaultValue;			// optional statement has defaultValue.
		private boolean isList;					// used in an "in" SELECT clause

		/**
		 * ParamData constructor
		 * @param tag HTTP header tag.
		 * @param varFormat variable format.
		 * @param source source enumeration (select query, insert column value, etc)
		 * @param expr source expression
		 * @param dataType SQL dataType
		 */
		public ParamData(String tag,
						 String varFormat,
						 ParamSource source,
						 Expression expr,
						 ColDataType dataType,
						 boolean isList) {
			this.expr = expr;
			this.dataType = dataType;
			this.source = source;
			this.nullable = false;
			this.tag = tag;
			this.optional = false;
			this.defaultValue = null;
			this.isList = isList;
			this.varFormat = varFormat;
		}


		/**
		 * Same, but with tag from headers (why isn't this also for query params
		 * @param tag HTTP header tag.
		 * @param source source enumeration (select query, insert column value, etc)
		 * @param expr source expression
		 * @param dataType SQL dataType
		 */
		public ParamData(String tag,
						 ParamSource source,
						 Expression expr,
						 ColDataType dataType,
						 boolean isList) {
			this(tag, null, source, expr, dataType, isList);
		}

		/**
		 * Constructor from the URL path or query components.
		 * @param source source enumeration (select query, insert column value, etc)
		 * @param expr source expression non-null if from SQL expression
		 * @param dataType SQL dataType
		 */
		public ParamData(ParamSource source, Expression expr, ColDataType dataType) {
			this(null, source, expr, dataType, false);
		}


		public ParamData(String tag,
						 ParamSource source,
						 ColDataType dataType) {
			this(tag, source, null, dataType, false);
		}

		/**
		 * Copy constructor
		 * @param paramData
		 */
		public ParamData(ParamData paramData) {
			this.tag = paramData.tag;
			this.varFormat = paramData.varFormat;
			this.source = paramData.source;
			this.dataType = paramData.dataType;
			this.expr = paramData.expr;
			this.nullable = paramData.nullable;
			this.optional = paramData.optional;
			this.defaultValue = paramData.defaultValue;
			this.isList = paramData.isList;
		}

		public ColDataType getDataType() {
			return dataType;
		}

		public ParamSource getSource() {
			return source;
		}

		public Expression getExpression() {
			return expr;
		}

		public boolean isNullable() {
			return nullable;
		}

		public void setSource(ParamSource source) {
			this.source = source;
		}

		public void setDataType(ColDataType dataType) {
			this.dataType = dataType;
		}

		public void setExpr(Expression expr) {
			this.expr = expr;
		}

		public void setNullable(boolean nullable) {
			this.nullable = nullable;
		}

		public void setOptional(boolean optional, String defaultValue) {
			this.optional = optional;
			this.defaultValue = defaultValue;
		}

		public String getTag() {
			return tag;
		}

		public String getVarFormat() {
			return varFormat;
		}

		public boolean isOptional() {
			return optional;
		}

		public String getDefaultValue() {
			return defaultValue;
		}

		public boolean isList() {
			return isList;
		}

		public void setIsList(boolean isList) {
			this.isList = isList;
		}

		public ParamSource resolveSource(ParamData other) {

			// SQL query and SQL value are NOT valid sources.
			if (notSpecified() && !other.notSpecified()) {
				return other.getSource();
			} else if (other.notSpecified() && !notSpecified()) {
				return this.source;
			} else {
				return this.source;
			}
		}

		/**
		 * If a parameter has query or value from the source, its source hasn't been specified,
		 * since it needs to be mapped to a statement named parameter or returned column.
		 * @return
		 */
		public boolean notSpecified() {
			return getSource().equals(ParamSource.QUERY) || getSource().equals(ParamSource.VALUE);
		}

		/**
		 * The value is specified either in the request payload, or a returning clause,
		 * so it's a list, and should be extracted from the request.
		 * @return
		 */
		public boolean isExtract() {
			return getSource().equals(ParameterMap.ParamSource.REQUEST_JSON) ||
				   getSource().equals(ParameterMap.ParamSource.SQL_RETURNING);
		}

		public boolean isReturning() {
			return getSource().equals(ParameterMap.ParamSource.SQL_RETURNING);
		}



		@Override
		public String toString() {
			StringBuffer sb = new StringBuffer();
			if (source != null) {
				sb.append(source.name + ":");
			} else {
				sb.append("source unspecified: ");
			}
			if (tag != null) {
				sb.append(" tag: " + tag);
			}
			if (varFormat != null) {
				sb.append(" varFormat: " + varFormat);
			}
			sb.append(" " + getDataType() + ": ");
			if (getExpression() != null) {
				sb.append(getExpression() + " ");
			}
			return sb.toString();
		}
	}
}
