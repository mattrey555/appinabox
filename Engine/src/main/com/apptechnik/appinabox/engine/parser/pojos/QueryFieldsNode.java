package com.apptechnik.appinabox.engine.parser.pojos;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.codegen.util.CodegenUtil;
import com.apptechnik.appinabox.engine.exceptions.TypeNotFoundException;
import com.apptechnik.appinabox.engine.parser.*;
import com.apptechnik.appinabox.engine.parser.json.ParseJSONMapExpression;
import com.apptechnik.appinabox.engine.parser.query.SelectItemUtils;
import com.apptechnik.appinabox.engine.parser.query.SelectTableUtils;
import com.apptechnik.appinabox.engine.parser.types.SQLType;
import com.apptechnik.appinabox.engine.util.CheckedFunction;
import com.apptechnik.appinabox.engine.util.CheckedPredicate;
import com.apptechnik.appinabox.engine.util.StringUtil;
import net.sf.jsqlparser.expression.Alias;
import net.sf.jsqlparser.statement.select.SelectItem;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Map the EXPANDED columns to fields referenced in the json, and assign the keys.
 */
public class QueryFieldsNode extends FieldReferenceNode<JsonColumnsReference> {
    private List<ColumnInfo> keys;

    public QueryFieldsNode() {
        keys = new ArrayList<ColumnInfo>();
    }

    public QueryFieldsNode(QueryFieldsNode node) {
        super(node);
        this.keys = node.getKeys();
    }

    /**
     * Assign the SQL columns which map to each FieldReference, and associated primary keys
     * to its containing node.
     * @param tree to see if this node has children (for the single-element exception case)
     * @param selectItems SELECT item list
     * @param tableInfoMap maps of table names/aliases to tables.
     * @param columnInfoSet select items mapped to table columns
     * @param orderByKeys list of expressions from the ORDER BY clause.
     * @return list of JsonColimnsReference
     */
    public static List<JsonColumnsReference> assignColumnsAndKeys(Tree<QueryFieldsNode> tree,
                                                                  List<SelectItem> selectItems,
                                                                  TableInfoMap tableInfoMap,
                                                                  Collection<ColumnInfo> columnInfoSet,
                                                                  List<ColumnInfo> orderByKeys) throws Exception {
        AssignColumns assignColumns = new AssignColumns(columnInfoSet, orderByKeys);
        tree.traverseNode(assignColumns);
        AssignKeys assignKeys = new AssignKeys(orderByKeys, selectItems, tableInfoMap);
        tree.traverse(assignKeys);
        return assignColumns.getUnassignedFields();
    }

    /**
     * Perform a pre-order traversal of the tree, and write the set of column infos into a list
     * @param tree tree of QueryFieldsNodes
     * @return Set of ColumnInfo
     * @throws Exception if an exception is thrown during the traversal.
     */
    public static Collection<ColumnInfo> getOrderByKeys(Tree<QueryFieldsNode> tree) throws Exception {

        // ORDER OF THE KEYS FOLLOWING THE TREE TRAVERSAL (node, children left-to-right) is ESSENTIAL
        Collection<ColumnInfo> keys = new LinkedHashSet<ColumnInfo>();
        tree.traverseNode(new AccumulateKeys(), keys);
        return keys;
    }

    /**
     * Consumer to add ColumnInfos to the set.
     */
    private static class AccumulateKeys implements Tree.ParamConsumer<QueryFieldsNode, Collection<ColumnInfo>> {
        @Override
        public void applyResult(QueryFieldsNode node, Collection<ColumnInfo> keys) {
            keys.addAll(node.getKeys());
        }
    }

    public List<ColumnInfo> getKeys() {
        return keys;
    }

    public void setKeys(List<ColumnInfo> keys) {
        this.keys = keys;
    }

    public void addKey(ColumnInfo key) {
        this.keys.add(key);
        this.getColumnInfos().add(key);
        this.addFieldReference(new JsonColumnsReference(key));
    }

    /**
     * Assign the columns to their associated field references.
     * If the column name conflicts with other column names or aliases, a unique column alias is applied
     * If an alias is applied and there is a matching ORDER BY key, then it has to be changed too.
     */
    private static class AssignColumns implements Tree.ConsumerException<QueryFieldsNode> {
        private final Collection<ColumnInfo> columnInfoSet;
        private final List<ColumnInfo> orderByKeys;
        private final List<JsonColumnsReference> unassignedFields;

        public AssignColumns(Collection<ColumnInfo> columnInfoSet, List<ColumnInfo> orderByKeys) {
            this.columnInfoSet = columnInfoSet;
            this.orderByKeys = orderByKeys;
            this.unassignedFields = new ArrayList<JsonColumnsReference>();
        }

        public void accept(QueryFieldsNode node) {
            for (JsonColumnsReference ref : node.getFieldReferenceList()) {
                if (ref.matchColumnList(columnInfoSet).isEmpty()) {
                    unassignedFields.add(ref);
                }
                for (ColumnInfo col : ref.getMatchingColumnInfo()) {
                    // if there are any order by keys which match the select item, then they must be remapped.
                    ColumnInfo matchingOrderByKey = orderByKeys.stream()
                            .filter(key -> key.equals(col))
                            .findFirst()
                            .orElse(null);
                    boolean changed = SelectTableUtils.resolveConflict(col, columnInfoSet);
                    // order by keys DO NOT HAVE ALIASES.
                    if (changed && matchingOrderByKey != null) {
                        matchingOrderByKey.setColumnName(col.getAlias().getName());
                    }
                }
            }
        }

        public List<JsonColumnsReference> getUnassignedFields() {
            return unassignedFields;
        }
    }

    /**
     * filter out columns referred to by an aggregation function (GROUP BY expression)
     * @param columns input columns
     * @return filtered columns
     */
    private static List<ColumnInfo> filterOutAggregatedColumns(List<ColumnInfo> columns) {
        return columns.stream()
                .filter(CheckedPredicate.wrap(key -> !key.isReferredToByAggregationFunction()))
                .collect(Collectors.toList());
    }

    /**
     * Assign the keys which are associated with each JSON node.
     */
    private static class AssignKeys implements Tree.ConsumerException<Tree<QueryFieldsNode>> {
        private final List<ColumnInfo> orderByKeys;
        private final Set<ColumnInfo> primaryKeys;
        private final List<SelectItem> selectItems;
        private final TableInfoMap tableInfoMap;

        public AssignKeys(List<ColumnInfo> orderByKeys, List<SelectItem> selectItems, TableInfoMap tableInfoMap) {
            this.orderByKeys = orderByKeys;
            this.primaryKeys = new HashSet<ColumnInfo>();           // for tracking naming conflicts
            this.selectItems = selectItems;
            this.tableInfoMap = tableInfoMap;
        }

        public void accept(Tree<QueryFieldsNode> tree) throws Exception {
            if (!FieldReferenceNode.isSingleField(tree)) {
                QueryFieldsNode node = tree.get();
                // see if all the fields in this node are in the ORDER BY list, if so, then use those.
                List<ColumnInfo> matchingOrderByKeys =
                        node.getColumnInfos().stream()
                                .filter(col -> orderByKeys.contains(col))
                                .collect(Collectors.toList());
                if (matchingOrderByKeys.equals(node.getColumnInfos())) {
                    node.setKeys(matchingOrderByKeys);
                } else {
                    // some tables do not have primary keys, like synthetic tables from sub-selects.  Note that we
                    // create a set to remove duplicates.
                    Collection<ColumnInfo> matchingPrimaryKeys =
                            filterOutAggregatedColumns(node.getColumnInfos()).stream()
                                                              .map(col -> col.getContainingTableInfo().getPrimaryKey())
                                                              .filter(key -> key != null)
                                                              .collect(Collectors.toSet());

                    // Check to make sure it doesn't clash with an aliased column in the primary key list, the order by list
                    // and the select item list.  Give it a new alias if so.
                    // WE EXPLICITLY CHECK WITH THE TABLE NAME AS NULL, because java.sql.ResultSet
                    // cannot do get<Type>() on a table-qualified column (i.e. table.column)
                    for (ColumnInfo key : matchingPrimaryKeys) {
                        ColumnInfo copyKey = new ColumnInfo(key);
                        copyKey.setAlias(new Alias(copyKey.getTablePrefixedName()));
                        if (!primaryKeys.contains(key) && (key.nameConflict(primaryKeys) || key.nameConflict(orderByKeys))) {
                            String alias = SelectItemUtils.uniqueColumnName(selectItems, tableInfoMap, primaryKeys, orderByKeys, copyKey.getAliasName());
                            copyKey.setAlias(new Alias(alias));
                        }
                        // it's important to add the key after assigning it so keys in the same node don't conflict with each other.
                        primaryKeys.add(copyKey);
                        node.addKey(copyKey);
                    }
                }
            }
        }
    }

    public static class Factory implements ParseJSONMapExpression.Factory<QueryFieldsNode, JsonColumnsReference> {
        public QueryFieldsNode create() {
            return new QueryFieldsNode();
        }
        public JsonColumnsReference createRef(String prefix, String ref) {
            return new JsonColumnsReference(prefix, ref);
        }
    }

    /**
     * tables(child) is subset of tables(parent)
     * NOTE: we know this is true for parent-&gt;child, TODO: NOTE: is it true for ancestors?
     * @param parent parent field references.
     * @return true if the child uses the parent keys.
     */
    public boolean childUsesParentKeys(QueryFieldsNode parent) {
        return (parent.getKeys().size() == getKeys().size()) && parent.getKeys().containsAll(getKeys());
    }

    /**
     * Forced boxed simpleType.  NOTE: The child is named from the child node name, but
     * the ResultSet reference is from its field reference.
     * @return {@code List<<type>> } where type is a primitive boxed type.
     * @throws TypeNotFoundException
     */
    private Type boxedType(CodeGen codegen) throws TypeNotFoundException {
        ColumnInfo columnInfo = getColumnInfos().get(0);
        return codegen.typeFromSQLType(columnInfo.getDataType(), true);
    }

    /**
     * Get the type of a child (which may depend on the parent)
     * if the child and parent have the same primary keys, then the child is a a scalar, not a list
     * if the child has a single scalar value, then it is a list of boxed primitives
     * otherwise, it's a list of objects.
     * TODO: See if this should be removed.
     * @param parentTree parent (may be null)
     * @param childTree child to get the type for
     * @return type specified above.
     * @throws TypeNotFoundException
     */
    public static Type getChildListType(CodeGen codegen, Tree<QueryFieldsNode> parentTree, Tree<QueryFieldsNode> childTree)
    throws TypeNotFoundException {
        String childClassName = childTree.get().getClassName();
        if ((parentTree != null) && childTree.get().childUsesParentKeys(parentTree.get())) {
            return codegen.classType(childClassName);
        } else {
            return codegen.listType(QueryFieldsNode.getNodeType(codegen, childTree));
        }
    }

    /**
     * Get the node type for this node. If the node has no childen and only a single element, then
     * it is a boxed primitive, otherwise it's the node's name.
     * @param tree node (with children to check if it's a leaf/single.
     * @return Type of the node.
     * @throws TypeNotFoundException Boxed type of node's single field couldn't be found
     */
    public static Type getNodeType(CodeGen codegen, Tree<QueryFieldsNode> tree) throws TypeNotFoundException {
        return FieldReferenceNode.isSingleField(tree) ?
                tree.get().boxedType(codegen) :
                codegen.classType(tree.get().getClassName());
    }

    /**
     * Get the fully qualified node type for this node. If the node has no childen and only a single element, then
     * it is a boxed primitive, otherwise it's the node's name.
     * @param tree node (with children to check if it's a leaf/single.
     * @return Type of the node.
     * @throws TypeNotFoundException Boxed type of node's single field couldn't be found
     */
    public static Type getNodeType(CodeGen codegen, String packageName, Tree<QueryFieldsNode> tree)
    throws TypeNotFoundException {
        return FieldReferenceNode.isSingleField(tree) ?
                tree.get().boxedType(codegen) :
                codegen.classType(packageName + "." + tree.get().getClassName());
    }

    /**
     * This field represents an array of a primitive boxed type IFF:
     * 1) it has no children
     * 2) it only has one field
     * 3) is not a wildcard.
     * @return
     */
    public boolean isSingleField() {
        return (getFieldReferenceCount() == 1) &&
               (getFieldReferenceAt(0).isDotForm() || getFieldReferenceAt(0).isColumnOnly());
    }

    /**
     * Return the child type, which is a list of the child's type.
     * @param childTree child node (tree)
     * @return list of child's node type.
     * @throws TypeNotFoundException
     */
    public static Type getChildListType(CodeGen codegen, Tree<QueryFieldsNode> childTree) throws TypeNotFoundException {
        return codegen.listType(getNodeType(codegen, childTree));
    }

    public String getSingleLeafNodeSQLType() throws TypeNotFoundException {
        return SQLType.getJavaSqlTypeNameFromString(getColumnInfos().get(0).getDataType());
    }

    /**
     * Field declarations for variables to read SQL result set into.
     * @param codegen code generator
     * @param accessModifier private/public/protectde
     * @return list of member declarations
     */
    public List<MemberDeclaration> fieldDeclarationsFromColumns(CodeGen codegen, String accessModifier) {
        return getColumnInfos().stream()
                .map(CheckedFunction.wrap(c -> c.fieldDeclaration(codegen, accessModifier)))
                .collect(Collectors.toList());
    }

    /**
     * Generate
     * @Entity(primaryKeys = ["repoName", "repoOwner", "login"])
    */
    public Annotation createEntityAnnotation(CodeGen codegen) {
        List<Constant> primaryKeyNames = streamPrimaryKeys()
                .map(ci -> codegen.stringConst(ci.getColumnName(true)))
                .collect(Collectors.toList());
        return codegen.annotation(Constants.Annotations.ENTITY,
                                  codegen.annotationArg(Constants.Annotations.PRIMARY_KEY,
                                                        codegen.annotationArrayConstant(primaryKeyNames)));
    }

    /**
     * Add a PrimaryKey annotation to the member for Android Room
     * @param codegen code generator to create the annotation
     * @param members list of class members.
     * @return list of members which have the @PrimaryKey annotation adde.
     */
    public List<MemberDeclaration> applyPrimaryKeyAnnotation(CodeGen codegen, List<MemberDeclaration> members) {
        List<String> primaryKeyNames = getColumnInfos().stream()
                .filter(ci -> ci.isPrimaryKey())
                .map(ci -> ci.getColumnName(true))
                .collect(Collectors.toList());
        List<MemberDeclaration> primaryKeyMembers = members.stream()
                .filter(m -> primaryKeyNames.contains(m.getVariable().getName()))
                .collect(Collectors.toList());
        primaryKeyMembers.stream()
                .forEach(m -> m.addAnnotation(codegen.annotation(Constants.Annotations.PRIMARY_KEY)));
        return primaryKeyMembers;
    }

    /**
     * Get the list of column infos for this node by accumulating the column infos for each field reference.
     * @return
     */
    public List<ColumnInfo> getColumnInfos() {
        return getFieldReferenceList().stream()
                .map(ref -> ref.getMatchingColumnInfo())
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }


    /**
     * Generate member fields of the form:
     * {@code <type> <child-variable-name>}
     * where type is the child's type. NOTE: if the child is a single value no children,
     * its type is a List of Boxed Primitives.
     * {@code List<(Boxed Type from field reference + columnInfoList)> <varName>}
     * @param codegen code generator
     * @param childTree node and its child references.
     * @param accessModifier private/public/protected
     * @return MemberDeclaration of the form described above
     * @throws TypeNotFoundException
     */
    public static MemberDeclaration childListFieldDeclaration(CodeGen codegen,
                                                              Tree<QueryFieldsNode> childTree,
                                                              String accessModifier)
    throws TypeNotFoundException {
        return codegen.memberDecl(accessModifier, getChildListType(codegen, childTree), codegen.var(childTree.get().getVarName()));
    }

    /**
     * Generate a child list assignment of the form:
     * {@code <var> = new ArrayList<type>}
     * @param codegen
     * @param parentTree parent to check if primary keys are shared, which makes child a scalar, not a list
     * @param childTree node and its child references.
     * @return AssignmentStatement of the form described above.
     * @throws TypeNotFoundException
     */
    public static CodeStatement childListAllocation(CodeGen codegen,
                                                    Tree<QueryFieldsNode> parentTree,
                                                    Tree<QueryFieldsNode> childTree)
    throws TypeNotFoundException {
        Variable var = codegen.var(childTree.get().getVarName());
        if (childTree.get().childUsesParentKeys(parentTree.get())) {
            return codegen.assignStmt(var, codegen.alloc(QueryFieldsNode.getNodeType(codegen, childTree)));
        } else {
            return CodegenUtil.listAllocAssignMember(codegen, var, QueryFieldsNode.getNodeType(codegen, childTree));
        }

    }
    /**
     * Declare class member for each contained classes mapped from the Field References tree
     * if the child uses the same keys as its parent, then it's just a singleton, not a list
     * if the child is a single primitive value, then it's a list of primitives.
     * otherwise it's a list of children.
     * @param codegen code generator
     * @param tree tree to get children from (current node)
     * @param accessModifier private/public/protected
     * @return list of child field declarations
     */
    public static List<MemberDeclaration> childFieldDeclarationList(CodeGen codegen,
                                                                    Tree<QueryFieldsNode> tree,
                                                                    String accessModifier)
    throws TypeNotFoundException {
        List<MemberDeclaration> declarationList = new ArrayList<MemberDeclaration>();
        for (Tree<QueryFieldsNode> childTree : tree.getChildren()) {
            if (childTree.get().childUsesParentKeys(tree.get())) {
                declarationList.add(childTree.get().childFieldDeclaration(codegen, accessModifier));
            } else {
                declarationList.add(QueryFieldsNode.childListFieldDeclaration(codegen, childTree, accessModifier));
            }
        }
        return declarationList;
    }

    /**
     * Create the list of members from the columns and children referenced by this tree node.
     * @param codegen code generator
     * @param tree tree to generate code for.
     * @param accessModifier public/private/protected
     * @return
     * @throws TypeNotFoundException
     */
    public static List<MemberDeclaration> fieldDeclarationList(CodeGen codegen,
                                                               Tree<QueryFieldsNode> tree,
                                                               String accessModifier)
    throws TypeNotFoundException {
        List<MemberDeclaration> members = tree.get().fieldDeclarationsFromColumns(codegen, accessModifier);
        members.addAll(QueryFieldsNode.childFieldDeclarationList(codegen, tree, accessModifier));
        return members;
    }

    /**
     * Return the primary key column in this node.
     * @return
     */
    public ColumnInfo getPrimaryKey() {
        return getColumnInfos().stream()
                .filter(ci -> ci.isPrimaryKey())
                .findFirst()
                .orElse(null);
    }

    public Stream<ColumnInfo> streamPrimaryKeys() {
        return getColumnInfos().stream().filter(ci -> ci.isPrimaryKey());
    }

    /**
     * Need to get primary key for list, or first referenced field.
     * @param allowAlias
     * @return
     */
    public String primaryKeyName(boolean allowAlias) {
        return getPrimaryKey().getColumnName(allowAlias);
    }

    /**
     * NOTE: this only handles one level of nesting.
     * @param parent
     * @param allowAlias
     * @return
     */
    public String primaryKeyName(QueryFieldsNode parent, boolean allowAlias) {
        if (childUsesParentKeys(parent)) {
            return parent.primaryKeyName(allowAlias);
        } else {
            return primaryKeyName(allowAlias);
        }
    }

    public static String primaryKeyName(Stack<Tree<QueryFieldsNode>> stack) {
        for ( int index = 0; index < stack.size(); index++) {
            QueryFieldsNode node = stack.get(index).get();
            if (node.getPrimaryKey() != null) {
                return node.primaryKeyName(true);
            }
        }
        return null;
    }

    /**
     * Do all the table references from the other node match this table.
     * @param other other QueryFieldsNode.
     * @return true if they're all from the same table.
     */
    public boolean sameTable(QueryFieldsNode other) {
        for (ColumnInfo columnInfo: other.getColumnInfos()) {
            if (!columnInfo.sameTable(this.getColumnInfos().get(0))) {
                return false;
            }
        }
        return true;
    }

    /**
     * Given the set of select
     * @param selectItemList
     * @param tableInfoMap
     * @return
     * @throws Exception
     */
    public Collection<SelectItem> filterSelectItemList(List<SelectItem> selectItemList, TableInfoMap tableInfoMap)
            throws Exception {
        Set<SelectItem> filteredItems = new HashSet<SelectItem>();
        for (JsonColumnsReference ref : getFieldReferenceList()) {
            filteredItems.addAll(ref.filterSelectItemList(selectItemList, tableInfoMap));
        }
        return filteredItems;
    }

    /**
     * Generate list item layout unique file name.
     * @param createObjects
     * @param stack
     * @return
     */
    public String getItemLayoutName(CreateObjects createObjects, Stack<Tree<QueryFieldsNode>> stack) {
        return getLayoutName(createObjects, stack) + Constants.Extensions.LIST_ITEM_XML;
    }

    public String getListLayoutName(CreateObjects createObjects, Stack<Tree<QueryFieldsNode>> stack) {
        return getLayoutName(createObjects, stack) + Constants.Suffixes.LIST_LAYOUT;
    }

    public String getDetailLayoutName(CreateObjects createObjects, Stack<Tree<QueryFieldsNode>> stack) {
        return getLayoutName(createObjects, stack) + Constants.Suffixes.LIST_LAYOUT;
    }

    public String getLayoutName(CreateObjects createObjects, Stack<Tree<QueryFieldsNode>> stack) {
        StringBuilder sb = new StringBuilder();
        sb.append(StringUtil.mixedCaseToUnderscores(createObjects.getVarName()));
        for (Tree<QueryFieldsNode> tree : stack) {
            sb.append("_");
            sb.append(tree.get().getVarName());
        }
        return sb.toString();
    }

    /**
     * We can't use the normal "recurse-on-tree", because the syntax varies dependent
     * on the children:
     * { foo : bar, baz, child : { node }}
     * { foo : child { node }}
     * { foo : bar, baz }
     * Since we need to know if the node has fields and children to insert the comma
     * @param tree
     * @return
     */
    public static String toJsonString(Tree<QueryFieldsNode> tree) {
        StringBuilder sb = new StringBuilder();
        sb.append("{ ");
        QueryFieldsNode node = tree.get();
        sb.append(node.toString());

        // insert a "," if there are fields abd children.
        if ((node.getFieldReferenceCount() > 0) && (tree.getChildCount() > 0)) {
            sb.append(", ");
        }
        for (int iChild = 0; iChild < tree.getChildCount(); iChild++) {
            Tree<QueryFieldsNode> child = tree.getChildAt(iChild);
            sb.append(toJsonString(child));
            if (iChild < tree.getChildCount() - 1) {
                sb.append(", ");
            }
        }
        sb.append("}");
        return sb.toString();
    }
}
