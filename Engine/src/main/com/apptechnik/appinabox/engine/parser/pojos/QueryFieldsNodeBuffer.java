package com.apptechnik.appinabox.engine.parser.pojos;

import com.apptechnik.appinabox.engine.parser.format.Formatter;
import com.apptechnik.appinabox.engine.parser.util.ResultSetUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

public class QueryFieldsNodeBuffer extends QueryFieldsNode {
    private ByteArrayOutputStream boas;
    private int[] columnIndices;
    private int[] sqlTypes;
    private Formatter[] outputFormatters;

    public QueryFieldsNodeBuffer(QueryFieldsNode node, ResultSetMetaData metaData)
    throws SQLException {
        super(node);
        this.setKeys(node.getKeys());
        boas = new ByteArrayOutputStream();
        columnIndices = new int[node.getKeys().size()];
        assignColumnIndices(metaData);
        sqlTypes = new int[node.getKeys().size()];
        assignTypes(metaData);
        outputFormatters = new Formatter[node.getKeys().size()];
        assignFormatters();
    }

    private void assignColumnIndices(ResultSetMetaData metaData) throws SQLException {
        for (int iName = 0; iName < columnIndices.length; iName++) {
            for (int iCol = 0; iCol < metaData.getColumnCount(); iCol++) {
                String name = metaData.getColumnName(iCol);
                if (getKeys().get(iName).getColumnName().equals(name)) {
                    columnIndices[iName] = iCol;
                }
            }
        }
    }

    private void assignTypes(ResultSetMetaData metaData) throws SQLException {
        for (int iCol = 0; iCol < sqlTypes.length; iCol++) {
            sqlTypes[iCol] = metaData.getColumnType(columnIndices[iCol]);
        }
    }

    private void assignFormatters() throws SQLException {
        for (int iCol = 0; iCol < sqlTypes.length; iCol++) {
            outputFormatters[iCol] = Formatter.getFormatter(sqlTypes[iCol]);
        }
    }

    public static Tree<QueryFieldsNodeBuffer> create(
            Tree<QueryFieldsNode> tree,
            ResultSetMetaData metaData) throws Exception {
        return tree.traverseFn(node -> new QueryFieldsNodeBuffer(node, metaData));
    }

    public boolean changed(ResultSet rs, ResultSet rsPrev) throws SQLException, IOException {
        if (rsPrev == null) {
            return true;
        }
        for (int iCol = 0; iCol < getKeys().size(); iCol++) {
            if (!ResultSetUtils.sameValue(rs, rsPrev, sqlTypes[iCol], columnIndices[iCol])) {
                return true;
            }
        }
        return false;
    }

    /**
     * Commit the contents of this node's buffer to the stream.
     * @param os output stream.
     * @throws IOException
     */
    public void commit(OutputStream os) throws IOException {
        os.write(boas.toByteArray());
        boas.reset();
    }

    /**
     * Output the JSON for this result set if:
     * this node is a leaf node, which means duplicates are allowed.
     * the column values in this result set referred to by this node are
     * different from the previous result set.
     * @param rs result set
     * @param rsPrev previous result set to compare to
     * @param isParentChanged force output whether or not ResultSets are different.
     * @param isLeaf is a leaf node
     * @param isFirst is the first output from this node, otherwise prepend a comma.
     * @param os OutputStream
     * @throws SQLException
     * @throws IOException
     */
    public boolean output(ResultSet rs,
                          ResultSet rsPrev,
                          boolean isParentChanged,
                          boolean isLeaf,
                          boolean isFirst,
                          OutputStream os) throws Exception {
        if (isParentChanged || isLeaf || changed(rs, rsPrev)) {
            if (isFirst) {
                os.write(',');
            }
            for (int iCol = 0; iCol < getKeys().size(); iCol++) {
                os.write('\"');
                String columnName = getKeys().get(iCol).getColumnName(true);
                os.write(columnName.getBytes(StandardCharsets.UTF_8));
                os.write('\"');
                os.write(':');
                ResultSetUtils.outputColumnValue(rs, columnIndices[iCol], sqlTypes[iCol],
                        outputFormatters[iCol], os);
                if (iCol < getKeys().size() - 1) {
                    os.write(',');
                }
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Each node in the tree has an output stream, because only it only gets closed and written
     * out when its parent has changed.  As a future optimization, if the node is a single child,
     * it can be written out immediately when any of its values have change, or it is a leaf node.
     * If this is the first resultSet, write a '{' before writing the fields.
     * If the resultSet has changed since the previous resultSet:
     *    if the node is a leaf node, then write the fields
     *    if the node is not a leaf node, close and write all of its descendants, then write its
     *    data
     * If the resultSet has not changed since the previous resultSet:
     *     if the node is not a leaf node, do nothing.
     *     if the node is a leaf node, write its data.
     * @param tree
     * @param rs
     * @param rsPrev
     * @param isFirst
     * @param hasParentChanged
     * @param os
     */
    public static void writeOutput(Tree<QueryFieldsNodeBuffer> tree,
                                   ResultSet rs,
                                   ResultSet rsPrev,
                                   boolean isFirst,
                                   boolean hasParentChanged,
                                   OutputStream os) throws SQLException, IOException {
        boolean isLeaf = tree.getChildCount() == 0;
        QueryFieldsNodeBuffer node = tree.get();
        if (hasParentChanged) {
            node.commit(os);
            if (!isLeaf) {
                for (int iChild = 0; iChild < tree.getChildCount(); iChild++) {
                    Tree<QueryFieldsNodeBuffer> child = tree.getChildAt(iChild);
                    writeOutput(child, rs, rsPrev, false, true, os);
                }
            }
            os.write('}');
        } else if (isLeaf) {
            if (!isFirst) {
                os.write(',');
            }
            node.commit(os);
        }
    }
}
