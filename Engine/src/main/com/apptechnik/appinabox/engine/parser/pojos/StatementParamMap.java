package com.apptechnik.appinabox.engine.parser.pojos;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.exceptions.NameNotFoundException;
import com.apptechnik.appinabox.engine.exceptions.TypeMismatchException;
import com.apptechnik.appinabox.engine.exceptions.TypeNotFoundException;
import com.apptechnik.appinabox.engine.parser.ExpressionType;
import com.apptechnik.appinabox.engine.parser.TableInfoMap;
import com.apptechnik.appinabox.engine.parser.util.ColDataTypeUtils;
import com.apptechnik.appinabox.engine.util.StatementUtil;
import com.apptechnik.appinabox.engine.util.StringUtil;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.create.table.ColDataType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Map of statements to parameters, map of statements to map of dot-format parameters to JDBC parameters.
 * Map of fixed types to dot-format parameters.
 */
public class StatementParamMap {
    private Map<Statement, ParameterMap> statementParamMap;
    private TypeMap fixedParamTypeMap;

    public StatementParamMap(Map<Statement, ParameterMap> statementParamMap,
                             TypeMap fixedParamTypeMap) {
        this.statementParamMap = statementParamMap;
        this.fixedParamTypeMap = fixedParamTypeMap;
    }

    public StatementParamMap(StatementParamMap statementParamMap,
                             List<Statement> statementList) {
        this.statementParamMap = new HashMap<Statement, ParameterMap>();
        statementList.stream().forEach(statement -> this.statementParamMap.put(statement, statementParamMap.getParamMap(statement)));
        this.fixedParamTypeMap = statementParamMap.getFixedParamTypeMap();
    }

    /**
     * Given a list of statements, and the CREATE TABLE map, create a map of SQL statements to
     * JDBC Named Parameters with types.
     * @param statementList List of SQL statements.
     * @param createTableInfoMap map of table names to table definitions.
     * @param fixedParamTypeMap
     * @throws Exception
     */
    public StatementParamMap(List<Statement> statementList,
                             TableInfoMap createTableInfoMap,
                             Map<Statement, Map<String, String>> statementDotParamMap,
                             TypeMap fixedParamTypeMap,
                             ParameterMap queryParamMap) throws Exception {
        this.statementParamMap = new HashMap<Statement, ParameterMap>();
        this.fixedParamTypeMap = fixedParamTypeMap;
        for (Statement statement : statementList) {
            TableInfoMap statementInfoMap = StatementUtil.getTables(statement, createTableInfoMap);
            ParameterMap parameterMap = ExpressionType.getNamedParameterTypes(statement, statementInfoMap, createTableInfoMap);
            parameterMap.setDotParamMap(statementDotParamMap.get(statement));
            parameterMap.applyFixedParamTypes(fixedParamTypeMap);
            statementParamMap.put(statement, parameterMap);
        }
        queryParamMap.applyParameterTypes(statementParamMap);
    }

    public Map<Statement, ParameterMap> getStatementParamMap() {
        return statementParamMap;
    }

    public ParameterMap getParamMap(Statement statement) {
        return statementParamMap.get(statement);
    }

    public TypeMap getFixedParamTypeMap() {
        return fixedParamTypeMap;
    }


    /**
     * All of the parameters in the query must either match a type-resolved named parameter in the SQL expression,
     * or be specified in the fixedParamTypeMap (which we should warn, since it's unused.
     * @param queryParams Parameter map from URL and HTTP headers.
     * @return
     */
    public List<ParameterMap.ParamError> validateParameters(ParameterMap queryParams) {
        List<ParameterMap.ParamError> paramErrorList = new ArrayList<ParameterMap.ParamError>();
        for (String paramName : queryParams.keySet()) {
            List<ParameterMap> matchingStatementParams = statementParamMap.values().stream()
                    .filter(paramMap -> paramMap.hasParam(paramName))
                    .collect(Collectors.toList());
            if (matchingStatementParams.isEmpty()) {
                if (fixedParamTypeMap.contains(paramName)) {
                    paramErrorList.add(new ParameterMap.ParamError(paramName, queryParams.getParamData(paramName),
                            "not used as a named parameter in any SQL statement, but has a fixed type",
                            Constants.ErrorTypes.WARNING));
                } else {
                    paramErrorList.add(new ParameterMap.ParamError(paramName, queryParams.getParamData(paramName),
                            "not used as a named parameter in any SQL statement. Either use it in a statement, or assign it a fixed type",
                            Constants.ErrorTypes.ERROR));
                }
            } else if (matchingStatementParams.size() > 1) {
                for (ParameterMap paramMap : matchingStatementParams) {
                    try {
                        ColDataTypeUtils.promoteDataType(paramMap.findColDataType(paramName), matchingStatementParams.get(0).findColDataType(paramName));
                    } catch (TypeMismatchException tmex) {
                        paramErrorList.add(new ParameterMap.ParamError(paramName, queryParams.getParamData(paramName),
                                tmex.getMessage(),Constants.ErrorTypes.ERROR));
                    }
                }
            }
        }
        return paramErrorList;
    }
    /**
     * We can have multiple SQL statements which use the same parameter, but may have different types.
     * The types must be compatible.
     * @param statementList
     * @param paramName
     * @return
     * @throws TypeMismatchException
     */
    public ParameterMap.ParamData getBestMatch(List<Statement> statementList,
                                               String paramName) throws TypeMismatchException {
        ParameterMap.ParamData foundParam = null;
        for (Statement statement : statementList) {

            // statements may be select statements, so they may be in the node list, but not the statement->parameter map
            ParameterMap parameterMap = statementParamMap.get(statement);
            if (parameterMap != null) {
                ParameterMap.ParamData paramData = parameterMap.get(paramName);
                if (paramData != null) {
                    if (foundParam == null) {
                        foundParam = new ParameterMap.ParamData(paramData);
                    } else {
                        ColDataType bestType = ColDataTypeUtils.promoteDataType(paramData.getDataType(), foundParam.getDataType());
                        foundParam.setDataType(bestType);
                    }
                }
            }
        }
        return foundParam;
    }

    /**
     * Verify that all the parameter values have been inferred or set explicitly. Unset parameters
     * have the SQLType NAMED_PARAMTER.
     * @return map of statements of parameters of type NAMED_PARAMETER
     * @throws NameNotFoundException
     */
    public Map<Statement, List<String>> checkAllParametersInferred()  throws NameNotFoundException {
        Map<Statement, List<String>> unknownParamMap = new HashMap<Statement, List<String>>();
        for (Statement statement : statementParamMap.keySet()) {
            ParameterMap parameterMap = statementParamMap.get(statement);
            unknownParamMap.put(statement, parameterMap.filterByType(Constants.SQLTypes.NAMED_PARAMETER));
        }
        return unknownParamMap;
    }

    public void checkParameterTypes() throws NameNotFoundException, TypeNotFoundException {
        Map<Statement, List<String>> unknownParamMap = checkAllParametersInferred();
        boolean paramSetFailed = false;
        for (Statement statement : unknownParamMap.keySet()) {
            List<String> unspecifiedParams = unknownParamMap.get(statement);
            if (!unspecifiedParams.isEmpty()) {
                System.err.println("could not infer types for parameters in " + statement);
                paramSetFailed = true;
            }
            if (unknownParamMap.values().stream().flatMap(List::stream).findAny().isPresent()) {
                System.err.println("unreferenced parameters:" + StringUtil.concat(unknownParamMap.get(statement), ","));
            }
        }
        if (paramSetFailed) {
            throw new TypeNotFoundException("parameter types were unspecified, code generation failed");
        }
    }
}
