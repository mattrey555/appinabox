package com.apptechnik.appinabox.engine.parser.pojos;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Generic tree implementation, used for JSON mapping.
 * @param <T> whatever you want it to be. Could be a unicorn.
 */
public class Tree<T> {
    T data;
    private List<Tree<T>> children;

    /**
     * Empty constructor.  Initialize the child list.
     */
    public Tree() {
        children = new ArrayList<Tree<T>>();
    }

    /**
     * Root constructor, takes data for the root node.
     * @param data root node data.
     */
    public Tree(T data) {
        this();
        this.data = data;
    }

    /**
     * Return the data for this node.
     * @return the data for this node.
     */
    public T get() {
        return data;
    }

    /**
     * Set the data for a node.
     * @param data value to set.
     */
    public void set(T data) { this.data = data; }

    /**
     * Add a child to this node's list of children
     * @param item Tree node.
     */
    public void addChild(Tree<T> item) {
        children.add(item);
    }

    /**
     * Add a child to this node's list of children
     * @param item data (wrapped in tree node).
     */
    public Tree<T> addChild(T item) {
        Tree tree = new Tree(item);
        children.add(tree);
        return tree;
    }

    /**
     * Return the number of children of this node.
     * @return child cound.
     */
    public int getChildCount() {
        return children.size();
    }

    /**
     * Return true if this node has no children (i.e. it is a leaf node).
     * @return true if this node is childless.
     */
    public boolean isLeaf() {
        return (children == null) || (children.isEmpty());
    }

    /**
     * Return a child at index.
     * @param index index to get.
     * @return child at index or IndexOutOfBoundsException
     */
    public Tree<T> getChildAt(int index) {
        return children.get(index);
    }

    /**
     * Return this node's children.
     * @return
     */
    public List<Tree<T>> getChildren() {
        return children;
    }

    /**
     * Return the node values for this node's children.
     * @return list of child node values.
     */
    public List<T> getChildNodes() {
        return children.stream().map(t -> t.get()).collect(Collectors.toList());
    }

    /**
     * Traverse this tree with a Consumer that can throw an exception.
     * @param fn Consumer which can throw an exception.
     * @throws Exception thrown if the consumer throws an exception (on first exception)
     */
    public void traverse(ConsumerException<Tree<T>> fn) throws Exception {
        fn.accept(this);
        for (Tree<T> child : getChildren()) {
            child.traverse(fn);
        }
    }

    /**
     * Traverse a tree with a predicate, and return true if the predicate is true for all nodes.
     * @param fn predicate to apply.
     * @return true if the predicate returns true for all nodes.
     */
    public boolean traversePredicateNode(Predicate<T> fn) {
        if (fn.test(this.get())) {
            for (Tree<T> child : getChildren()) {
                if (!child.traversePredicateNode(fn)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Like traverse, except tha the consumer takes the node value, not the tree node.
     * @param fn exception-throwing consumer that takes a tree node value.
     * @throws Exception if the consumer threw an exception on any node.
     */
    public void traverseNode(ConsumerException<T> fn) throws Exception {
        fn.accept(this.get());
        for (Tree<T> child : getChildren()) {
            child.traverseNode(fn);
        }
    }

    /**
     * Sometimes when we recurse, we don't want to just visit each node, we want to get
     * a result set from its children and do something with it, so rather than write a
     * wrapper class, we embed it here.
     * @param <T> node type.
     * @param <U> result type
     */
    public interface ParamConsumer<T, U> {
        void applyResult(T node, U param) throws Exception;
    }

    /**
     * like a ParamConsumer, except returns true/false.
     * @param <T>
     * @param <U>
     */
    public interface ParamPredicate<T, U> {
        boolean test(T node, U param) throws Exception;
    }

    public interface ConsumerException<T> {
        void accept(T t) throws Exception;
    }

    public interface FunctionException<T, U> {
        U apply(T t) throws Exception;
    }

    /**
     * Traverse the tree, and apply the consumer to each each node, passing param.
     * @param fn consumer which takes an additional parameter.
     * @param param
     * @param <U>
     * @return
     * @throws Exception
     */
    public <U> U traverseNode(ParamConsumer<T, U> fn, U param) throws Exception {
        WrapConsumer wrapper = new WrapConsumer(fn, param);
        traverseNode(wrapper);
        return param;
    }

    /**
     * Travers the tree with a parameter consumer
     * @param fn consumer which takes an additional parameter.
     * @param param additional parameter.
     * @param <U> additional parameter type.
     * @return additional parameter.
     * @throws Exception
     */
    public <U> U traverse(ParamConsumer<T, U> fn, U param) throws Exception {
        WrapConsumer wrapper = new WrapConsumer(fn, param);
        traverse(wrapper);
        return param;
    }

    /**
     * Traverse the tree with a parameter consumer
     * @param fn consumer which takes an additional parameter.
     * @param param additional parameter.
     * @param <U> additional parameter type.
     * @return additional parameter.
     * @throws Exception
     */
    public <U> U traverseTreeStack(ParamConsumer<Stack<Tree<T>>, U> fn, U param) throws Exception {
        Stack<Tree<T>> stack = new Stack<Tree<T>>();
        WrapConsumer wrapper = new WrapConsumer(fn, param);
        Exception ex = traverseStackTreeRecursive(wrapper, stack);
        if (ex != null) {
            throw ex;
        }
        return param;
    }

    /**
     * Wrap a predicate consumer as a java Consumer
     * @param <T> tree node type
     * @param <U> additional parameter type.
     */
    private class WrapPredicate<T, U> implements Consumer<T> {
        private U param;
        private ParamPredicate<T, U> paramPredicate;
        private Exception exception;
        private boolean anyTrue;
        private boolean allTrue;

        public WrapPredicate(ParamPredicate paramPredicate, U param) {
            this.paramPredicate = paramPredicate;
            this.param = param;
            this.anyTrue = false;
            this.allTrue = true;
        }

        @Override
        public void accept(T node) {
            try {
                boolean result = paramPredicate.test(node, param);
                anyTrue |= result;
                allTrue &= result;
            } catch (Exception ex) {
                exception = ex;
            }
        }

        public boolean isAnyTrue() {
            return anyTrue;
        }

        public boolean isAllTrue() {
            return allTrue;
        }

        public Exception getException() {
            return exception;
        }
    }

    /**
     * Wrapper for java consumer, so we can pass a parameter.
     * @param <T> Tree node type
     * @param <U> parameter type
     */
    private class WrapConsumer<T, U> implements ConsumerException<T> {
        private U param;
        private ParamConsumer<T, U> paramConsumer;

        public WrapConsumer(ParamConsumer<T, U> paramConsumer, U param) {
            this.paramConsumer = paramConsumer;
            this.param = param;
        }

        public void accept(T value) throws Exception {
            paramConsumer.applyResult(value, param);
        }
    }

    /**
     * Create a tree which is the result of applying a function to a tree.
     * @param fn function
     * @param <U> function result
     * @return tree of U
     * @throws Exception
     */
    public <U> Tree<U> traverseFn(FunctionException<T, U> fn) throws Exception {
        Tree<U> tree = new Tree<U>(fn.apply(this.get()));
        for (Tree<T> t : this.children) {
            tree.addChild(t.traverseFn(fn));
        }
        return tree;
    }

    /**
     * Create a list which is the result of applying a function to a tree in-order traversal.
     * @param fn function
     * @param <U> function result
     * @return tree of U
     * @throws Exception
     */
    public <U> List<U> traverseFnToList(FunctionException<T, U> fn) throws Exception {
        List<U> list = new ArrayList<U>();
        traverseFnToListInner(this, fn, list);
        return list;
    }

    private <U> void traverseFnToListInner(Tree<T> tree,
                                           FunctionException<T, U> fn,
                                           List<U> list) throws Exception {
        list.add(fn.apply(tree.get()));
        for  (Tree<T> child : tree.getChildren()) {
            traverseFnToListInner(child, fn, list);
        }
    }

    public void traverseStack(ConsumerException<Stack<T>> fn) throws Exception {
        Stack<T> stack = new Stack<T>();
        Exception ex = traverseStackRecursive(fn, stack);
        if (ex != null) {
            throw ex;
        }
    }

    public Exception traverseStackRecursive(ConsumerException<Stack<T>> fn, Stack<T> stack) {
        stack.push(this.get());
        try {
            fn.accept(stack);
        } catch (Exception ex) {
            return ex;
        }
        for (Tree<T> child : getChildren()) {
            Exception ex = child.traverseStackRecursive(fn, stack);
            if (ex != null) {
                return ex;
            }
        }
        stack.pop();
        return null;
    }

    public void traverseTreeStack(ConsumerException<Stack<Tree<T>>> fn) throws Exception {
        Stack<Tree<T>> stack = new Stack<Tree<T>>();
        Exception ex = traverseStackTreeRecursive(fn, stack);
        if (ex != null) {
            throw ex;
        }
    }

    public Exception traverseStackTreeRecursive(ConsumerException<Stack<Tree<T>>> fn, Stack<Tree<T>> stack) {
        stack.push(this);
        try {
            fn.accept(stack);
        } catch (Exception ex) {
            return ex;
        }
        for (Tree<T> child : getChildren()) {
            Exception ex = child.traverseStackTreeRecursive(fn, stack);
            if (ex != null) {
                return ex;
            }
        }
        stack.pop();
        return null;
    }

    /**
     * Return the depth-first traversal of this tree as a list.
     * @return
     */
    public List<T> toList() {
        List<T> list = new ArrayList<T>();
        list.add(get());
        for (Tree<T> childTree : getChildren()) {
            list.addAll(childTree.toList());
        }
        return list;
    }

    public static <T> Tree<T> listToTree(List<T> list, ParentChild<T> fn) {
        T root = findElementWithChildrenAndNoParent(list, fn);
        if (root == null) {
            return null;
        }
        list.remove(root);
        return new Tree(root).listToTreeInternal(list, fn);
    }

    public static <T> T findElementWithChildrenAndNoParent(List<T> list, ParentChild<T> fn) {
        T root = null;
        for (T o : list) {

            // scan the list for a potential parent for this element.  If there is
            // none then scan again to see
            T parent = findParent(o, list, fn);
            if (parent == null) {
                root = o;
                boolean rootHasChild = false;
                for (T candChild : list) {
                    if (fn.isParent(root, candChild)) {
                        rootHasChild = true;
                        break;
                    }
                }
                if (rootHasChild) {
                    return root;
                }
            }
        }
        return null;
    }

    public Tree<T> listToTreeInternal(List<T> list, ParentChild<T> fn) {
        List<T> children = findChildren(get(), list, fn);
        list.removeAll(children);
        for (T child : children) {
            Tree<T> childTree = addChild(child);
            childTree.listToTreeInternal(list, fn);
        }
        return this;
    }

    public static <T> List<T> findChildren(T o, List<T> list, ParentChild<T> parentChild) {
        List<T> children = new ArrayList<T>();
        for (T child : list) {
            if (parentChild.isParent(o, child)) {
                children.add(child);
            }
        }
        return children;
    }

    public static <T> T findParent(T o, List<T> list, ParentChild<T> parentChild) {
        for (T parent : list) {
            if (parentChild.isParent(parent, o)) {
                return parent;
            }
        }
        return null;
    }

    /**
     * Create a String of the form
     * @return
     */
    public String toString() {
        return "{ " + toStringInner() + " }";
    }

    public String toStringInner() {
        StringBuilder sb = new StringBuilder();
        sb.append(data.toString());
        if (!children.isEmpty()) {
            sb.append(" { ");
            for (int i = 0; i < children.size(); i++) {
                sb.append(children.get(i).toStringInner());
                if (i != children.size() - 1) {
                    sb.append(",");
                }
            }
            sb.append(" }");
        }
        return sb.toString();
    }

    /**
     * Interface for methods that define a parent-child relationship. Used to
     * create trees from lists.
     * @param <T>
     */
    public interface ParentChild<T> {
        boolean isParent(T a, T b);
    }
}
