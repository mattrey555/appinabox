package com.apptechnik.appinabox.engine.parser.pojos;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.util.SQLFile;
import com.apptechnik.appinabox.engine.exceptions.PropertiesException;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Internal map of fixed type names in the SQL file.
 * TODO: move this to SQLFile
 * form is type: {@code 1<variable> <type> }
 */
public class TypeMap {
    private final Map<String, String> typeMap;            // parameter types specified in SQL file comments

    public TypeMap(SQLFile sqlFile) throws PropertiesException {
        this(sqlFile.filterTag(Constants.Directives.TYPE));
    }

    /**
     * Read the type map for fixed types.
     * form is type: {@code 1<variable> <type> }
     * &lt;type&gt; may be multiple identifiers.
     * @throws PropertiesException incorrect type directive syntax
     */
    public TypeMap(List<String> typeSpecifiers) throws PropertiesException {
        typeMap = new HashMap<String, String>();
        for (String line : typeSpecifiers) {
            line = line.trim();
            int ichDelim = StringUtil.firstWhiteSpace(line);
            if (ichDelim < 0) {
                throw new PropertiesException("type directive " + line +
                                              " should have at last 3 components: type: <param-name> <type>");
            }
            String variable = line.substring(0, ichDelim);
            String type = line.substring(ichDelim + 1);
            typeMap.put(variable, type);
        }
    }

    public boolean contains(String key) {
        return typeMap.containsKey(key);
    }

    public String get(String key) {
        return typeMap.get(key);
    }

    public String get(String key, String defaultValue) {
        return (get(key) != null) ? get(key) : defaultValue;
    }

    public Set<String> keySet() {
        return typeMap.keySet();
    }
}
