package com.apptechnik.appinabox.engine.parser.query;

import com.apptechnik.appinabox.engine.exceptions.UnsupportedExpressionException;
import net.sf.jsqlparser.expression.Alias;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.select.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Utilities for JOIN expressions to get the tables and columns used in SELECT JOINs
 */
public class JoinUtils {

    /**
     * Get the list of tables referenced in this select expression. Some expressions are unsupported.
     * @param plainSelect SELECT statement with some JOIN clauses
     * @return tables used in the JOIN clause
     * @throws UnsupportedExpressionException one of the JOINS used an unsupported expression wihtout an alias.
     */
    public static List<Table> getJoinTables(PlainSelect plainSelect) throws UnsupportedExpressionException {
        List<Table> tableList = new ArrayList<>();
        getSelectTables(plainSelect, tableList);
        return tableList;
    }

    /**
     * Inner method, which takes the returned table list as an argument
     * @param plainSelect SELECT statement with some JOIN clauses
     * @param tableList list of tables to add to.
     * @throws UnsupportedExpressionException one of the JOINS used an unsupported expression wihtout an alias.
     */
    private static void getSelectTables(PlainSelect plainSelect, List<Table> tableList) throws UnsupportedExpressionException {
        FromItem fromItem = plainSelect.getFromItem();
        if (fromItem instanceof Table) {
            tableList.add((Table) fromItem);
        } else if (fromItem instanceof SubJoin) {
            getJoinTables((SubJoin) fromItem, tableList);
        } else if (fromItem instanceof SubSelect) {
            Alias syntheticTableAlias = fromItem.getAlias();
            if (syntheticTableAlias == null) {
                throw new UnsupportedExpressionException("the sub-select " + plainSelect + " must be aliased");
            }
            tableList.add(new Table(syntheticTableAlias.getName()));
        }
        getJoinTables(plainSelect.getJoins(), tableList);
    }

    /**
     * Assume that the list of joins is a list of tables, with no sub-selects,
     * @param joinList list of joins
     * @param tableList list of tables to add to.
     * @throws UnsupportedExpressionException one of the JOINS used an unsupported expression wihtout an alias.
     */
    private static void getJoinTables(List<Join> joinList, List<Table> tableList) throws UnsupportedExpressionException {
        if (joinList != null) {
            for (Join join : joinList) {
                if (join.getRightItem() instanceof Table) {
                    tableList.add((Table) (Table) join.getRightItem());
                } else if (join.getRightItem() instanceof SubSelect) {
                    PlainSelect subSelect = (PlainSelect) ((SubSelect) join.getRightItem()).getSelectBody();
                    if (join.getRightItem().getAlias() == null) {
                        throw new UnsupportedExpressionException("the sub-select " + subSelect + " must be aliased");
                    }
                    tableList.add(new Table(join.getRightItem().getAlias().getName()));
               } else if (join.getRightItem() instanceof SubJoin) {

                    // TODO: this isn't right. Fix it.  Fix me.
                    List<Join> subJoinList = ((SubJoin) join.getRightItem()).getJoinList();
                    getJoinTables(subJoinList, tableList);
                } else {
                    throw new UnsupportedExpressionException("Joins only support simple tables " + join.getRightItem());
                }
            }
        }
    }

    private static void getJoinTables(SubJoin subJoin, List<Table> tableList) throws UnsupportedExpressionException {
        if (subJoin.getLeft() instanceof Table) {
            tableList.add((Table) subJoin.getLeft()) ;
        } else {
            throw new UnsupportedExpressionException("only tables are allowed in joins");
        }
        for (Join join : subJoin.getJoinList()) {
            if (join.getRightItem() instanceof Table) {
                tableList.add((Table) join.getRightItem());
            } else {
                throw new UnsupportedExpressionException("only tables are allowed in joins");
            }
        }
    }
}
