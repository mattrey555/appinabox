package com.apptechnik.appinabox.engine.parser.query;

import com.apptechnik.appinabox.engine.log.Log;
import com.apptechnik.appinabox.engine.parser.ColumnInfo;
import com.apptechnik.appinabox.engine.parser.TableInfo;
import com.apptechnik.appinabox.engine.parser.TableInfoMap;
import com.apptechnik.appinabox.engine.parser.query.SelectItemUtils;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.SelectItem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Methods used to modify the SELECT statement to add primary keys to the select items and ORDER BY clause
 */
public class ModifySQL {
    /**
     * Iterate through the rows of the result set and compare the primary key values from each
     * table to the previous record, and create a new (sub)object in the JSON hierarchy if the value
     * has changed.  For proper mapping, this means the results need to be ordered by primary key.
     * We add the primary keys to the ORDER BY clause of the SELECT statement, in the order which
     * their containing tables occur in the select item list.
     * @param plainSelect SELECT statement to add the keys to.
     * @param orderedPrimaryKeys primary keys to add.
     */
    public static void addPrimaryKeysToOrderBy(PlainSelect plainSelect,
                                               Collection<ColumnInfo> orderedPrimaryKeys) {
        plainSelect.setOrderByElements(orderedPrimaryKeys.stream()
                .map(key -> key.toOrderByElement())
                .collect(Collectors.toList()));
    }

    /**
     *  A SQL aggregation function usually requires a GROUP BY for the other columns in the SELECT item list.
     *  We modify the SELECT statement to add the table primary keys, so they have to be added to the GROUP BY
     *  clause.
     * @param plainSelect this may be a top-level select statement, or a sub-select.  The primary keys
     *                    are added to the GROUP BY clause if there are SQL aggregation functions in the
     *                    select list.
     * @param aggregatedColumnList column arguments to SQL aggregation functions.
     * @param primaryKeys primary keys of tables referenced in the select item list.
     * @return list of primary keys to add to the GROUP BY clause.
     */

    public static List<ColumnInfo> addPrimaryKeysToGroupBy(PlainSelect plainSelect,
                                                           List<ColumnInfo> aggregatedColumnList,
                                                           Collection<ColumnInfo> primaryKeys) {
        List<ColumnInfo> addKeysToGroupBy = new ArrayList<ColumnInfo>();
        if ((plainSelect.getGroupBy() != null) && (plainSelect.getGroupBy().getGroupByExpressions() != null)) {
            for (ColumnInfo primaryKey : primaryKeys) {
                boolean matchPrimaryKey = plainSelect.getGroupBy().getGroupByExpressions().stream()
                        .filter(expr -> primaryKey.equalsColumn(expr))
                        .findAny()
                        .isPresent();

                // does this primary key refer to a table a column of which is used in a SQL aggregation function?
                boolean inAggregationExpression = aggregatedColumnList.stream()
                        .filter(colInfo -> colInfo.equals(primaryKey))
                        .findAny()
                        .isPresent();
                if (!matchPrimaryKey && !inAggregationExpression) {
                    addKeysToGroupBy.add(primaryKey);
                }
            }
            addKeysToGroupBy.stream().forEach(key -> plainSelect.getGroupBy().addGroupByExpression(key.toColumn()));
        }
        return addKeysToGroupBy;
    }

    /**
     * When we add a primary key to the ORDER BY clause, we also need to add them to the select item list
     * so they can be referenced in the PrimaryKeySourceColumns class to determine when new child objects
     * should be created.
     * @param selectItems list of items from the SELECT statement.
     * @param primaryKeys primary keys used to map the resulting rows to the generated JSON.
     * @param aggregatedColumnList list of columns referenced in the GROUP BY clause.
     * @param tableInfoMap tables referenced by the SELECT statement.
     * @param orderByKeys keys present in the ORDER BY clause.
     * @return list of columns to add to the select item list.
     */
    public static List<ColumnInfo> addPrimaryKeysToSelectItems(List<SelectItem> selectItems,
                                                               Collection<ColumnInfo> primaryKeys,
                                                               List<ColumnInfo> aggregatedColumnList,
                                                               TableInfoMap tableInfoMap,
                                                               List<ColumnInfo> orderByKeys) {
        List<ColumnInfo> filteredPrimaryKeys = new ArrayList<ColumnInfo>();
        // The keys are the aliases used in this clause.  TableInfo may or may not refer to a
        // CREATE TABLE statement.
        for (ColumnInfo key : primaryKeys) {
            TableInfo tableInfo = key.getContainingTableInfo();

            // If the table is not referred to by an aggregated column AND a column in the select item list
            // refers to that table, AND the table is not already ordered on another key,
            // then add the primary keys of that table to the select item list.
            if (!ColumnInfo.inTable(aggregatedColumnList, tableInfo) && !ColumnInfo.tableInList(tableInfo, orderByKeys)) {
                // if the primary key isn't in the list of select items, and isn't an aggregated column,
                // then add it to the select item list.  Whether or not the key is added to the list,
                // add a copy of it to the list of returned primary keys, which are used for ORDER BY clause.
                // if it matches an alias, assign the alias.
                List<SelectItem> matchingSelectItems =
                        SelectItemUtils.matchingSelectItemsNoWildcard(selectItems, tableInfoMap, key);
                if (matchingSelectItems.isEmpty() || SelectItemUtils.nameConflict(key, filteredPrimaryKeys)) {
                    selectItems.add(key.toSelectExpressionItem());
                }
                filteredPrimaryKeys.add(key);
            } else {
                Log.debug("removing key: " + key);
            }
        }
        return filteredPrimaryKeys;
    }
}
