package com.apptechnik.appinabox.engine.parser.query;

import net.sf.jsqlparser.parser.ASTNodeAccess;
import net.sf.jsqlparser.parser.CCJSqlParserVisitor;
import net.sf.jsqlparser.parser.Node;
import net.sf.jsqlparser.parser.SimpleNode;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

//NOTE: I don't see this used anywhere.
public class NodeUtils {

    public static class Traverse<T> {
        public interface Function<T> {
            void accept(SimpleNode n, T closure);
        }

        public static <T> void traverse(ASTNodeAccess node, Function func, T closure) {
            if (node != null) {
                func.accept(node.getASTNode(), closure);
                for (ASTNodeAccess child : childList(node.getASTNode())) {
                    traverse(child, func, closure);
                }
            }
        }

        public static List<ASTNodeAccess> childList(Node node) {
            List<ASTNodeAccess> childList = new ArrayList<ASTNodeAccess>();
            for (int i = 0; i < node.jjtGetNumChildren(); i++) {
                if (node.jjtGetChild(i) instanceof SimpleNode) {
                    if (((SimpleNode) node.jjtGetChild(i)).jjtGetValue() instanceof ASTNodeAccess) {
                        childList.add((ASTNodeAccess) ((SimpleNode) node.jjtGetChild(i)).jjtGetValue());
                    }
                }
            }
            return childList;
        }
    }

    private static class FilterVisitor<T> implements Traverse.Function<T> {
        CCJSqlParserVisitor actual;
        private Predicate<Object> pred;

        public FilterVisitor(Predicate<Object> pred, CCJSqlParserVisitor actual) {
            this.pred = pred;
            this.actual = actual;
        }

        @Override
        public void accept(SimpleNode node, T closure) {

            // unfortunately, getASTNode() returns null for some objects.
            if ((node != null) && (node.jjtGetValue() != null) && pred.test(node.jjtGetValue())) {
                actual.visit(node, closure);
            }
        }
    }

    public static <T, S> T childrenAcceptFilter(ASTNodeAccess node,
                                                Predicate<S> pred,
                                                CCJSqlParserVisitor actual,
                                                T closure) {
        FilterVisitor visitor = new FilterVisitor(pred, actual);
        Traverse.traverse(node, visitor, closure);
        return closure;

    }

    public static <T> List<T> recursivelyFilterByType(ASTNodeAccess node, Class<T> c) {
        List<T> list = new ArrayList<T>();
        childrenAcceptFilter(node,
                             x -> (x != null) && c.isAssignableFrom(x.getClass()),
                             (n, o) -> list.add((T) n.jjtGetValue()),
                             list);
        return list;
    }

    /**
     * Sometimes we want to
     * @param node
     * @param carray
     * @param <T>
     * @return
     */
    static <T> boolean matchTypes(SimpleNode node, Class<T>[] carray) {
        return matchTypes(node, carray, 0);
    }

    /**
     * Walk back up from the node and find the matching classes in CArray.  This allows us to do
     * type scanning, where we can do stuff like "Table In a Join Expression", or "column in a select item list"
     * carray is ordered from bottom to top, for example: { Column.class, SelectItemList.class }, which
     * is counterintuitive.
     * @param node
     * @param carray
     * @param index
     * @param <T>
     * @return
     */
    private static <T> boolean matchTypes(SimpleNode node, Class<T>[] carray, int index) {
        if (index == carray.length) {
            return true;
        }

        // ran out of parents before classes.
        if ((node == null) || (node.jjtGetValue() == null)){
            return false;
        }
        if (carray[index].isAssignableFrom(node.jjtGetValue().getClass())) {
            return matchTypes((SimpleNode) node.jjtGetParent(), carray, index + 1);
        } else if (index > 0) {

            // first one must match directly, others can match descendants in order, but
            // not necessarily contiguous.
            return matchTypes((SimpleNode) node.jjtGetParent(), carray, index);
        }
        return false;
    }

    public static <T> List<T> recursivelyFilterByTypes(ASTNodeAccess node, Class<T>[] carray) {
        List<T> list = new ArrayList<T>();
        childrenAcceptFilter(node, n -> matchTypes(((ASTNodeAccess) n).getASTNode(), carray), (n, o) -> list.add((T) n.jjtGetValue()), list);
        return list;
    }
}
