package com.apptechnik.appinabox.engine.parser.query;

import com.apptechnik.appinabox.engine.log.Log;
import com.apptechnik.appinabox.engine.parser.ColumnInfo;
import com.apptechnik.appinabox.engine.parser.TableInfoMap;
import com.apptechnik.appinabox.engine.parser.pojos.QueryFieldsNode;
import com.apptechnik.appinabox.engine.parser.pojos.Tree;
import com.apptechnik.appinabox.engine.util.CheckedConsumer;
import com.apptechnik.appinabox.engine.util.CheckedFunction;
import net.sf.jsqlparser.statement.select.OrderByElement;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.SelectItem;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Utilities for modifying ORDER BY columns.
 */
public class OrderByUtils {

    /**
     * Retrieve the list of columns referred to by a SELECT ORDER BY clause.
     * @param plainSelect list of ORDER BY expressions.
     * @param selectTableInfoMap TableInfoMap (filtered by tables from SELECT statement)
     * @return list of columnInfo.
     * @throws Exception thrown trying to traverse the ORDER BY expressions
     */
    public static List<ColumnInfo> getOrderByColumns(PlainSelect plainSelect,
                                                     TableInfoMap selectTableInfoMap)
    throws Exception {
        List<OrderByElement> orderByElements = plainSelect.getOrderByElements();
        if (orderByElements != null) {
            return orderByElements.stream()
                    .map(CheckedFunction.wrap(el -> ColumnInfo.resolveColumns(el.getExpression(), selectTableInfoMap)))
                    .flatMap(List::stream).collect(Collectors.toList());
        } else {
           return new ArrayList<ColumnInfo>();
        }
    }

    private static class BadOrderByKeys implements Tree.ConsumerException<QueryFieldsNode> {
        private List<SelectItem> selectItemList;
        private List<ColumnInfo> orderByColumns;
        private TableInfoMap selectTableInfoMap;
        private Collection<ColumnInfo> removeOrderBys;

        public BadOrderByKeys(List<SelectItem> selectItemList,
                              List<ColumnInfo> orderByColumns,
                              TableInfoMap selectTableInfoMap) {
            removeOrderBys = new HashSet<ColumnInfo>();
            this.selectItemList = selectItemList;
            this.orderByColumns = orderByColumns;
            this.selectTableInfoMap = selectTableInfoMap;
        }

        @Override
        public void accept(QueryFieldsNode node) throws Exception {
            removeOrderBys.addAll(findBadOrderByKeys(node, selectItemList, orderByColumns, selectTableInfoMap));
        }

        public Collection<ColumnInfo> getRemoveOrderBys() {
            return removeOrderBys;
        }
    }

    public static Collection<ColumnInfo> findBadOrderByKeys(Tree<QueryFieldsNode> jsonMap,
                                                            List<SelectItem> selectItemList,
                                                            List<ColumnInfo> orderByColumns,
                                                            TableInfoMap tableInfoMap)
    throws Exception {
        BadOrderByKeys badOrderByKeys = new BadOrderByKeys(selectItemList, orderByColumns, tableInfoMap);
        jsonMap.traverseNode(badOrderByKeys);
        return badOrderByKeys.getRemoveOrderBys();
    }

    /**
     * To use ORDER BY expression for resultset comparison, either all of the columns in the
     * SELECT item list must occur in the ORDER BY clause, or the ORDER BY column must reference
     * the primary key for that table.
     * If an ORDER BY column does not map to any SELECT items, then it is removed.
     * @param node filter select items by columns in this node
     * @param selectItemList list of items in the SELECT statement.
     * @param orderByColumns colums resolved from the ORDER BY clause by getOrderByColumns()
     * @param tableInfoMap
     */

    private  static Collection<ColumnInfo> findBadOrderByKeys(QueryFieldsNode node,
                                                              List<SelectItem> selectItemList,
                                                              List<ColumnInfo> orderByColumns,
                                                              TableInfoMap tableInfoMap) throws Exception {
        Set<ColumnInfo> removeOrderBys = new HashSet<>();
        if (orderByColumns != null) {
            for (ColumnInfo orderByColumn : orderByColumns) {
                // find the tables associated with the list of select items, and the list of ORDER BY columns.
                Collection<SelectItem> items = node.filterSelectItemList(selectItemList, tableInfoMap);
                SelectItemUtils.filterByTableInfo(items, orderByColumn.getContainingTableInfo());
                List<ColumnInfo> orderBys = ColumnInfo.filterByTableInfo(orderByColumn.getContainingTableInfo(), orderByColumns);
                //  If an ORDER BY column does not map to any SELECT items, then it is removed.
                if (items.isEmpty()) {
                    removeOrderBys.addAll(orderBys);
                } else {
                    for (SelectItem item : items) {
                        if (!orderByColumns.stream()
                                .anyMatch(orderBy -> SelectItemUtils.matchColumnInfo(item, tableInfoMap, orderBy))) {
                            removeOrderBys.addAll(orderBys);
                            break;
                        }
                    }
                }
            }
        }
        return removeOrderBys;
    }

    /**
     * Remove ORDER BY columns which reference columns in the SELECT item list, but there are other columns from
     * the same source table which are not in the ORDER BY clause.
     * Remove ORDER BY columns which do not appear in the SELECT item list
     * @param selectItemList list of select items from the SELECT statement.
     * @param orderByKeys list of columns from the ORDER BY clause
     * @param tableInfoMap TableInfoMap for the SELECT statement
     * @return filtered list of columns from the ORDER BY clause 
     * @throws Exception
     */
    public static  List<ColumnInfo> cleanOrderByKeys(Tree<QueryFieldsNode> jsonMap,
                                                     List<SelectItem> selectItemList,
                                                     List<ColumnInfo> orderByKeys,
                                                     TableInfoMap tableInfoMap)
    throws Exception {
        Collection<ColumnInfo> badOrderByKeys = OrderByUtils.findBadOrderByKeys(jsonMap, selectItemList, orderByKeys, tableInfoMap);
        for (ColumnInfo badKey : badOrderByKeys) {
            Log.warn("All columns from the same table in the SELECT item list must have an ORDER BY key, " +
                    badKey.getColumnName() +
                    " otherwise it is removed, and the primary key of the table " +
                    badKey.getContainingTableInfo().getName() + " is used");
        }
        orderByKeys.removeAll(badOrderByKeys);

        // ORDER BY columns which are not in the select list are removed
        List<ColumnInfo> notInSelectItemKeys = new ArrayList<ColumnInfo>();
        for (ColumnInfo orderByKey : orderByKeys) {
            if (!SelectItemUtils.inSelectItems(selectItemList, tableInfoMap, orderByKey)) {
                Log.warn("ORDER BY column " + orderByKey + " is not in the SELECT item list, and will be removed");
                notInSelectItemKeys.add(orderByKey);
            }
        }
        orderByKeys.removeAll(notInSelectItemKeys);
        return orderByKeys;
    }

    /**
     * Is this column in the list of ORDER BY elements? Then we don't have to add it.
     * @param orderByElements list of ORDER BY elements
     * @param columnInfo column to scan for (probably that we want to order on)
     * @return true if the column is referenced in the ORDER BY clause.
     */
    private static boolean isColumnInOrderBy(List<OrderByElement> orderByElements, ColumnInfo columnInfo) {
        return orderByElements.stream().anyMatch(element -> columnInfo.equalsColumn(element.getExpression()));
    }

}
