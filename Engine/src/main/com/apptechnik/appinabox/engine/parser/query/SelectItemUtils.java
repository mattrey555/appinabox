package com.apptechnik.appinabox.engine.parser.query;

import java.util.*;
import java.util.stream.Collectors;

import com.apptechnik.appinabox.engine.parser.util.FunctionType;
import com.apptechnik.appinabox.engine.exceptions.*;
import com.apptechnik.appinabox.engine.log.Log;
import com.apptechnik.appinabox.engine.parser.*;
import net.sf.jsqlparser.expression.*;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.select.*;

public class SelectItemUtils {

	public static List<ColumnInfo> columnInfoListFromSelectItems(List<SelectItem> selectItemList,
																 TableInfoMap tableInfoMap)
	throws Exception {
		List<ColumnInfo> columnInfoList = new ArrayList<ColumnInfo>();
		for (SelectItem item : selectItemList) {
			columnInfoList.addAll(columnInfoSetFromSelectItem(item, tableInfoMap));
		}
		return columnInfoList;
	}

	/**
	 * return the list of ColumnInfo referenced by this select item.
	 * @param selectItem SELECT item
	 * @param tableInfoMap Map of table names to column definitions.
	 * @return list of ColumnInfo
	 * @throws Exception
	 */
	public static Collection<ColumnInfo> columnInfoSetFromSelectItem(SelectItem selectItem,
																	 TableInfoMap tableInfoMap)
			throws Exception {
		if (selectItem instanceof AllColumns) {
			return tableInfoMap.getAllColumnInfo();
		} else if (selectItem instanceof AllTableColumns) {
			AllTableColumns allTableColumns = (AllTableColumns) selectItem;
			Table sourceTable = allTableColumns.getTable();
			return tableInfoMap.getTableInfo(sourceTable).getColumnInfoSet();
		} else if (selectItem instanceof SelectExpressionItem) {
			List<ColumnInfo> columnInfoList = ColumnInfo.resolveColumns(((SelectExpressionItem) selectItem).getExpression(), tableInfoMap);
			if ((columnInfoList.size() > 1) && (((SelectExpressionItem) selectItem).getAlias() == null)) {
				throw new AmbiguousColumnException("scanning select items: " + selectItem +
													"is a complex expression and must be aliased");
			}
			return columnInfoList;
		} else {
			throw new UnsupportedExpressionException("create column and alias from " + selectItem);
		}
	}

	/**
	 * Given an expression, return the unique set of tables it references.
	 * @param tableInfoMap Map of table names/aliases to table information.
	 * @param expr expression to traverse
	 * @return set of tables referenced by columns in the expression.
	 */
	public static Set<TableInfo> getTables(TableInfoMap tableInfoMap, Expression expr) throws Exception {
		GetTablesFunction getTablesFunction = new GetTablesFunction(tableInfoMap);
		Traverse.traverseExpression(expr, getTablesFunction);
		return getTablesFunction.getTableInfoSet();
	}

	private static class GetTablesFunction implements Traverse.StackFunction<Expression, Boolean> {
    	private Set<TableInfo> tableInfoSet;
    	private TableInfoMap tableInfoMap;

    	public GetTablesFunction(TableInfoMap tableInfoMap) {
			tableInfoSet = new HashSet<TableInfo>();
			this.tableInfoMap = tableInfoMap;
		}

    	public Set<TableInfo> getTableInfoSet() { return tableInfoSet; }
		public Boolean apply(Stack<Expression> callStack, Expression expr) throws Exception {
			if (expr instanceof Column) {
				tableInfoSet.add(tableInfoMap.getTableInfo((Column) expr));
			}
			return null;
		}
	}

	/**
	 * take the select items from a SELECT statement, and expand them out to the actual references. 
	 * "*" refers to all the columns in all the tables in the SELECT from items.
	 * "table.*" referes to all the columns in the referenced table
	 * @param selectItemList list of selectItems from the source SELECT statement.
	 * @param tableInfoMap map of tables from the CREATE TABLE statements.
	 * @return the expanded list of select items.
	 */
    public static List<SelectItem> expandSelectItemList(List<SelectItem> selectItemList, 
														TableInfoMap tableInfoMap)
		throws Exception {
		List<SelectItem> expandedSelectItemList = new ArrayList<SelectItem>();
		for (SelectItem selectItem : selectItemList) {
			expandedSelectItemList.addAll(expandSelectItem(selectItem, tableInfoMap));
		}
		return expandedSelectItemList;
	}
	
	/**
	 * @param selectItem item to expand (AllColumns are all the columns in all the tables in the select expression
	 * @param tableInfoMap map of tables which are in scope to the right of this SELECT expression in the
	 *                     SELECT statement (not the full create-table set of tables)
	 * @throws AmbiguousColumnException more than one table contained the column 
     * @throws ColumnNotFoundException no tables contained the column
	 */
    public static List<SelectItem> expandSelectItem(SelectItem selectItem, 
													TableInfoMap tableInfoMap)
		throws Exception {
		List<SelectItem> selectItemList = new ArrayList<SelectItem>();
		if (selectItem instanceof AllColumns) {
			tableInfoMap.keySet().forEach(tableName -> addTableColumnsToSelectItemList(selectItemList, tableInfoMap.getTableInfo(tableName)));
		} else if (selectItem instanceof AllTableColumns) {
			AllTableColumns allTableColumns = (AllTableColumns) selectItem;
			addTableColumnsToSelectItemList(selectItemList, tableInfoMap.getTableInfo(allTableColumns.getTable()));
		} else if (selectItem instanceof SelectExpressionItem) {
			SelectExpressionItem selectExpressionItem = (SelectExpressionItem) selectItem;
			Expression expr = selectExpressionItem.getExpression();
			List<Column> exprColumns = GetColumns.getColumns(expr);
			for (Column col : exprColumns) {
				if (col.getTable() == null) {
					col.setTable(tableInfoMap.getTable(col));
				}
			}
			selectItemList.addAll(exprColumns.stream().map(col -> new SelectExpressionItem(col)).collect(Collectors.toList()));
		}
		return selectItemList;
    }

	/**
	 * When we expand the select items * and table.*, we add the columns from the referred table to the
	 * select item list.
	 * @param selectItemList list of select items to add to.
	 * @param tableInfo
	 */
	private static void addTableColumnsToSelectItemList(List<SelectItem> selectItemList, TableInfo tableInfo) {
		tableInfo.getColumnInfoSet().forEach(colInfo -> selectItemList.add(colInfo.toSelectExpressionItem(tableInfo)));
	}

	/**
	 * Does this column information match a select item
	 * @param selectItem
	 * @param columnInfo
	 * @return
	 */
	public static boolean matchColumnInfo(SelectItem selectItem, TableInfoMap tableInfoMap, ColumnInfo columnInfo) {
		return matchColumn(selectItem, tableInfoMap, columnInfo.getTableName(),
						   columnInfo.getColumnName(), columnInfo.getAliasName());
	}

	/**
	 * AllColumns matches anything. AllTableColumns matches the column name if the table contains
	 * the column.  If it's a select item expression that is a single column, match on the column name.
	 * @param selectItem select item to match against.
	 * @param tableInfoMap tableInfoMap referred to by selectItem.
	 * @param tableName
	 * @param columnName column name to match against (probably something we're trying to add but we
	 *                   don't want to duplicate).
	 * @param alias
	 * @return true if there was a match, false otherwise.
	 * @param selectItem
	 * @param tableName
	 * @param columnName
	 * @param alias
	 * @return
	 */
	private static boolean matchColumn(SelectItem selectItem,
									   TableInfoMap tableInfoMap,
									   String tableName,
									   String columnName,
									   String alias) {
		if (selectItem instanceof SelectExpressionItem) {
			return matchSelectExpressionItem((SelectExpressionItem) selectItem, tableInfoMap, tableName, columnName, alias);
		} else if (selectItem instanceof AllTableColumns) {
			AllTableColumns allTableColumns = (AllTableColumns) selectItem;
			if (tableName != null) {
				return allTableColumns.getTable().getName().equals(tableName);
			} else {
				TableInfo tableInfo = tableInfoMap.getTableInfo(allTableColumns.getTable());
				String name = (alias != null) ? alias : columnName;
				return tableInfo != null ? tableInfo.getColumnInfo(name) != null : false;
			}
		} else if (selectItem instanceof AllColumns) {
			return true;
		}
		return false;
	}

	/**
	 * Columns can be specified as:
	 * <column>
	 * <table>.<column>
	 * <column> <alias>
	 * <expresion> AS <alias>
	 * @param selectExpressionItem
	 * @param tableInfoMap
	 * @param tableName
	 * @param columnName
	 * @param alias
	 * @return
	 */
	private static boolean matchSelectExpressionItem(SelectExpressionItem selectExpressionItem,
												     TableInfoMap tableInfoMap,
												     String tableName,
												     String columnName,
												     String alias) {
		// only match an alias if the column AND alias match
		if (selectExpressionItem.getAlias() != null) {
			if (alias != null) {
				return alias.equals(selectExpressionItem.getAlias().getName());
			} else if (tableName != null) {
				return columnName.equals(selectExpressionItem.getAlias().getName());
			}
		}
		Expression expr = selectExpressionItem.getExpression();

		// if no alias, only columns are supported.
		// table name: column must have the same table, or unique matching table in FROM clause matches.
		if (expr instanceof Column) {
			Column column = (Column) expr;
			if (tableName != null) {
				if (column.getTable() != null) {
					if (!tableName.equals(column.getTable().getName())) {
						return false;
					}
				} else {
					try {
						TableInfo tableInfo = tableInfoMap.getTableInfo(column);
						if (!tableName.equals(tableInfo.getName())) {
							return false;
						}
					} catch (Exception ex) {
						// TODO: normally, I don't do this, but I'm too lazy to undo all the stream()
						// calls that use this method
						String msg = "failed to find column " + column + " in table map " + tableInfoMap;
						Log.error(msg);
						throw new RuntimeException(msg);
					}

				}
			}

			// if neither has a table, check that the name and column do not conflict.
			return (alias != null) ? column.getColumnName().equals(alias) : column.getColumnName().equals(columnName);
		}
		return false;
	}

	/**
	 * Filter the select items which refer to the TableInfo
	 * @param selectItems list of select items from SELECT statement.
	 * @param tableInfo table containing list of column info.
	 * @return
	 */
	public static Collection<SelectItem> filterByTableInfo(Collection<SelectItem> selectItems, TableInfo tableInfo) {
		return selectItems.stream()
				.filter(item -> matchTableInfo(item, tableInfo, tableInfo.getAlias()))
				.collect(Collectors.toList());
	}

	/**
	 * Is column in selectItems?
	 * @param selectItems
	 * @param columnInfo
	 * @return
	 */
	public static boolean inSelectItems(List<SelectItem> selectItems, TableInfoMap tableInfoMap, ColumnInfo columnInfo) {
		return selectItems.stream().filter(item -> matchColumnInfo(item, tableInfoMap, columnInfo)).findAny().isPresent();
	}

	public static List<SelectItem> matchingSelectItemsNoWildcard(List<SelectItem> selectItems,
																 TableInfoMap tableInfoMap,
																 ColumnInfo columnInfo) {
		return selectItems.stream()
				.filter(item -> item instanceof SelectExpressionItem)
				.filter(item -> matchSelectExpressionItem((SelectExpressionItem) item, tableInfoMap,
						columnInfo.getTableName(), columnInfo.getColumnName(), columnInfo.getAliasName()))
				.collect(Collectors.toList());
	}

	public static SelectItem findColumnNoWildCards(Collection<SelectItem> selectItems,
												   TableInfoMap tableInfoMap,
												   String tableName,
												   String columnName,
												   String alias) {
		return selectItems.stream()
				.filter(item -> item instanceof SelectExpressionItem)
				.filter(item -> matchSelectExpressionItem((SelectExpressionItem) item, tableInfoMap, tableName, columnName, alias))
				.findFirst().orElse(null);
	}

	public static boolean nameConflict(ColumnInfo primaryKey, List<ColumnInfo> primaryKeys) {
		return primaryKeys.stream().filter(key -> key.nameConflict(primaryKey)).findFirst().isPresent();
	}

	/**
	 * Because java.sql.ResultSet is deficient, it doesn't handle table.column syntax to get
	 * values from the result set.  When we add a primary key to the selection list, it may
	 * conflict with another selectItem, so we have to create a unique identifier.
	 * @param selectItems
	 * @param columnName
	 * @return
	 */
	public static String uniqueColumnName(List<SelectItem> selectItems,
										  TableInfoMap tableInfoMap,
										  Collection<ColumnInfo> primaryKeyList,
										  Collection<ColumnInfo> orderByList,
										  String columnName) {
		int index = 0;
		do {
			String tryName = columnName + index;
			if ((findColumnNoWildCards(selectItems, tableInfoMap, null, columnName, tryName) != null) ||
				 ColumnInfo.nameConflict(tryName, primaryKeyList) || ColumnInfo.nameConflict(tryName, orderByList)) {
				index++;
			} else {
				return tryName;
			}
		} while (true);
	}

	public static boolean matchTableInfo(SelectItem selectItem, TableInfo tableInfo, String tableAlias) {
		if (selectItem instanceof AllColumns) {
			return true;
		} else if (selectItem instanceof AllTableColumns) {
			AllTableColumns allTableColumns = (AllTableColumns) selectItem;
			if (tableAlias != null) {
				if (allTableColumns.getTable().getName().equals(tableAlias)) {
					return true;
				}
			}
			return allTableColumns.getTable().getName().equals(tableInfo.getTable().getName());
		} else if (selectItem instanceof SelectExpressionItem) {
			SelectExpressionItem selectExpressionItem = (SelectExpressionItem) selectItem;
			if (selectExpressionItem.getExpression() instanceof Column) {
				Column column = (Column) selectExpressionItem.getExpression();
				return tableInfo.matchColumnInfo(column, tableAlias);
			}
		}
		return false;
	}

	public static List<ColumnInfo> aggregatedColumns(List<SelectItem> selectItems,
													 TableInfoMap subqueryTableInfoMap) throws Exception {
		AggregatedColumns aggregatedColumns = new AggregatedColumns(subqueryTableInfoMap);
		for (SelectItem selectItem : selectItems) {
			if (selectItem instanceof SelectExpressionItem) {
				SelectExpressionItem selectExpressionItem = (SelectExpressionItem) selectItem;
				Traverse.traverseExpression(selectExpressionItem.getExpression(), aggregatedColumns);
			}
		}
		return aggregatedColumns.getColumnInfos();
	}

	/**
	 * Find columns which are used in an aggregation expression.
	 */
	public static class AggregatedColumns extends ColumnInfo.ColumnTraverse implements Traverse.StackFunction<Expression, Object> {
		private Function callingFunction = null;

		public AggregatedColumns(TableInfoMap subqueryTableInfoMap) {
			super(subqueryTableInfoMap);
		}

		public Object apply(Stack<Expression> stack, Expression expr) throws Exception {
			if (expr instanceof Function) {
				Function function = (Function) expr;
				if (FunctionType.isAggregationFunction(function)) {
					callingFunction = function;
				}
			} else if ((expr instanceof Column) && stack.contains(callingFunction)) {
				columnInfos.add(subqueryTableInfoMap.getColumnInfo((Column) expr));
			}
			return null;
		}
	}

	public static class HasAggregationFunction implements Traverse.StackFunction<Expression, Object> {
		private boolean hasAggregationFunction;

		public HasAggregationFunction() {
			hasAggregationFunction = false;
		}

		public Object apply(Stack<Expression> stack, Expression expr) {
			if (expr instanceof Function) {
				Function function = (Function) expr;
				if (FunctionType.isAggregationFunction(function)) {
					hasAggregationFunction = true;
				}
			}
			return null;
		}

		public boolean isHasAggregationFunction() {
			return hasAggregationFunction;
		}
	}
}
