package com.apptechnik.appinabox.engine.parser.query;

import java.util.*;
import java.util.stream.Stream;

import com.apptechnik.appinabox.engine.exceptions.*;
import com.apptechnik.appinabox.engine.log.Log;
import com.apptechnik.appinabox.engine.parser.*;
import com.apptechnik.appinabox.engine.parser.pojos.*;
import com.apptechnik.appinabox.engine.util.CheckedConsumer;
import com.apptechnik.appinabox.engine.util.ListUtil;
import com.apptechnik.appinabox.engine.util.SetUtil;
import com.apptechnik.appinabox.engine.util.StringUtil;

import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.expression.*;
import net.sf.jsqlparser.statement.create.table.Index;
import net.sf.jsqlparser.statement.select.*;

/**
 * Methods for extracting tables from SQL expressions, to resolve explicit and implicit column references
 */
public class SelectTableUtils {
	public static ColumnsAndKeys resolveColumns(PlainSelect plainSelect,
												Tree<QueryFieldsNode> jsonTree,
												ParameterMap parameterMap,
												TableInfoMap selectTableInfoMap) throws Exception {
		// add the primary keys from the tables in the FROM component to the select item list if they
		// are not already present.
		List<ColumnInfo> aggregatedColumnList = SelectItemUtils.aggregatedColumns(plainSelect.getSelectItems(), selectTableInfoMap);
		// When the ORDER_BY expression is added, it's aliased, and so we map to the select items for the alias.
		List<ColumnInfo> orderByKeys = OrderByUtils.getOrderByColumns(plainSelect, selectTableInfoMap);
		Collection<ColumnInfo> columnInfoSet = expandSelectItems(plainSelect.getSelectItems(), parameterMap, selectTableInfoMap);
		List<JsonColumnsReference> unassignedRefs = QueryFieldsNode.assignColumnsAndKeys(jsonTree, plainSelect.getSelectItems(), selectTableInfoMap, columnInfoSet, orderByKeys);
		unassignedRefsCheck(plainSelect, unassignedRefs);
		orderByKeys = OrderByUtils.cleanOrderByKeys(jsonTree, plainSelect.getSelectItems(), orderByKeys, selectTableInfoMap);
		Collection<ColumnInfo> primaryKeyList = QueryFieldsNode.getOrderByKeys(jsonTree);
		ModifySQL.addPrimaryKeysToSelectItems(plainSelect.getSelectItems(), primaryKeyList, aggregatedColumnList, selectTableInfoMap, orderByKeys);
		// this is VERY VERY questionable.  If we're going to mess with the GROUP BY expression, we should be more rigorous.
		ModifySQL.addPrimaryKeysToGroupBy(plainSelect, aggregatedColumnList, primaryKeyList);

		// we add the primary keys to the ORDER BY expression at the top level, because that's the only one that
		// matters for comparing primary key values in the resulting row set. Re-generate the columnInfoList,
		// because we may have altered the SELECT item list.
		ModifySQL.addPrimaryKeysToOrderBy(plainSelect, primaryKeyList);
		columnInfoSet = expandSelectItems(plainSelect.getSelectItems(), parameterMap, selectTableInfoMap);
		return new ColumnsAndKeys(columnInfoSet, primaryKeyList, selectTableInfoMap);
	}

	/**
	 * Throw an exception if any field references were not defined in the select item list.
	 * @param plainSelect
	 * @param unassignedRefs
	 * @throws Exception
	 */
	private static void unassignedRefsCheck(PlainSelect plainSelect, List<JsonColumnsReference> unassignedRefs) throws Exception {
		if (!unassignedRefs.isEmpty()) {
			Log.error("failed references against " + plainSelect);
			unassignedRefs.forEach(ref -> Log.error("reference: " + ref + " could not be resolved"));
			throw new NameNotFoundException("references " + StringUtil.concat(unassignedRefs, ",") +
											" could not be resolved against: " +
											StringUtil.concat(plainSelect.getSelectItems(), ",") + "\n" +
										    " in SELECT statement: " + plainSelect);

		}
	}

	/**
	 * Recursively create a table map filtered by tables referred to in the select statement,
	 * and map them either by alias or name.
	 * @param plainSelect
	 * @param createTableInfoMap - global table map.
	 * @return
	 */
	public static TableInfoMap createSelectTableMap(PlainSelect plainSelect,
													ParameterMap parameterMap,
													TableInfoMap createTableInfoMap)
			throws Exception {
		TableInfoMap selectTableInfoMap = new TableInfoMap();

		// retrieve all of the tables used in the FROM and JOIN clauses.
		FromItem fromItem = plainSelect.getFromItem();
		TableInfo fromItemTable = createSelectTableMap(fromItem,  parameterMap, createTableInfoMap);
		selectTableInfoMap.put(getFromItemName(fromItem), fromItemTable, true);
		List<Join> joins = plainSelect.getJoins();
		if (joins != null) {
			for (Join join : joins) {
				TableInfo joinFromItemTable = createSelectTableMap(join.getRightItem(), parameterMap, createTableInfoMap);
				selectTableInfoMap.put(getFromItemName(join.getRightItem()), joinFromItemTable, true);
			}
		}
		return selectTableInfoMap;
	}


	private static TableInfo createSelectTableMap(FromItem fromItem,
												  ParameterMap parameterMap,
												  TableInfoMap tableInfoMap)
			throws Exception {
		if (fromItem instanceof Table) {
			// the create table map is referenced unaliased.
			return tableInfoMap.getTableInfo(((Table) fromItem).getName());
		} else if (fromItem instanceof SubSelect) {
			SubSelect subSelect = (SubSelect) fromItem;
			if (subSelect.getSelectBody() instanceof PlainSelect) {
				PlainSelect subPlainSelect = (PlainSelect) subSelect.getSelectBody();
				TableInfoMap subQueryTableInfoMap = createSelectTableMap(subPlainSelect, parameterMap,  tableInfoMap);
				if (fromItem.getAlias() == null) {
					throw new UnsupportedExpressionException("subqueries must have aliases: " + fromItem);
				}
				Collection<ColumnInfo> subQueryColumnInfo = expandSelectItems(subPlainSelect.getSelectItems(), parameterMap, subQueryTableInfoMap);

				// map the resulting colums to the newly alias as a table for the caller.
				List<Index> subqueryIndexList =  ColumnInfo.getIndexList(subQueryColumnInfo);
				Table table = new Table(fromItem.getAlias().getName());
				return new TableInfo(table, subQueryColumnInfo, subqueryIndexList, true);
			} else {
				throw new UnsupportedExpressionException("generating primary keys, unsupported expression: " + subSelect.getSelectBody().toString());
			}
		} else {
			throw new UnsupportedExpressionException("generating primary keys, unsupported expression: " + fromItem.toString());
		}
	}

	/**
	 * Expand *, t.*, and expression references from the select item list into a list of columns.
	 * @param selectItemList
	 * @param parameterMap map of JDBC Named Paramters to types.
	 * @param tableInfoMap map of tables used by this SELECT statement
	 * @return
	 * @throws Exception
	 */
	public static Collection<ColumnInfo> expandSelectItems(List<SelectItem> selectItemList,
														    ParameterMap parameterMap,
														    TableInfoMap tableInfoMap) throws Exception {

		// we can have duplicated columns into  the SELECT item list, so generate a unique set.
		Set<ColumnInfo> sourceColumns = new HashSet<ColumnInfo>();
		for (SelectItem selectItem : selectItemList) {
			if (selectItem instanceof AllColumns) {
				List<ColumnInfo> allColumns = tableInfoMap.getAllColumnInfo();
				allColumns.stream().forEach(col -> col.setSelectItem(selectItem));
				sourceColumns.addAll(tableInfoMap.getAllColumnInfo());
			} else if (selectItem instanceof AllTableColumns) {
				// add all the columns from the table of this select item.
				TableInfo tableInfo = tableInfoMap.getTableInfo(((AllTableColumns) selectItem).getTable());
				tableInfo.getColumnInfoSet().stream().forEach(col -> col.setSelectItem(selectItem));
				sourceColumns.addAll(tableInfo.getColumnInfoSet());
			} else if (selectItem instanceof SelectExpressionItem) {
				ColumnInfo sourceColumn = tableInfoMap.fromSelectExpressionItem(parameterMap, (SelectExpressionItem) selectItem);
				sourceColumn.setSelectItem(selectItem);
				sourceColumns.add(sourceColumn);
			} else {
				throw new UnsupportedExpressionException(selectItem.toString() + " not supported");
			}
		}
		return sourceColumns;
	}

	/**
	 * Resolve the conflict between he select item and other items in the SELECT expression.
	 * NOTE: WE DO NOT ADDRESS ALIAS CONFLICTS. THEY ARE ERRORS.
	 * @param col column info to resolve conflict
	 * @param columnInfoSet list of columns (with backreferences to their select items) which may conflict with col
	 * @return true if there was a conflict that had to be resolved.
	 */
	public static boolean resolveConflict(ColumnInfo col, Collection<ColumnInfo> columnInfoSet) {
		// if there's a name conflict with a previous column:
		// only SelectExpressionItems have aliases, test those first.
		ColumnInfo conflict;
		Alias alias;
		SelectItem selectItem = col.getSelectItem();
		Collection<ColumnInfo> otherColumns = SetUtil.except(columnInfoSet, col);
		for (conflict = col.getNameConflict(otherColumns);
			 conflict != null;
			 conflict = col.getNameConflict(otherColumns)) {
			if (selectItem instanceof SelectExpressionItem) {
				alias = ((SelectExpressionItem) selectItem).getAlias();
				String uniqueAliasName;

				//  try prefixing the table name if it's not present before trying incrementing indices. as it's more readable.
				if ((col.getAlias() == null) && (col.getTableName() != null) && !col.getColumnName().startsWith(col.getTableName())) {
					uniqueAliasName = col.getTableName() + "_" + col.getColumnName();
				} else if (conflict.getAlias() != null) {
					uniqueAliasName = StringUtil.incrementVarSuffix(conflict.getAlias().getName());
				} else {
					uniqueAliasName = StringUtil.incrementVarSuffix(conflict.getColumnName());
				}
				if (alias == null) {
					alias = new Alias(uniqueAliasName);
					((SelectExpressionItem) selectItem).setAlias(alias);
				} else {
					alias.setName(uniqueAliasName);
				}

				// let's mark the fact that we created this alias.
				col.setAliasSynthetic(true);
				col.setAlias(alias);
			} else {
				break;
			}
		}
		return conflict != null;
	}

	/**
	 * The the from items in a SELECT statement are added to the map of tables referenced in the select statement
	 * If the from item has an alias, use the alias.  FromItem subqueries must use an alias.
	 * If the From item is a table, use the table name.
	 * @param fromItem SELECT FROM or JOIN right from ite.
	 * @return name to use for the from item.
	 * @throws UnsupportedExpressionException
	 */
	private static String getFromItemName(FromItem fromItem) throws UnsupportedExpressionException {
		if (fromItem.getAlias() != null) {
			return fromItem.getAlias().getName();
		} else if (fromItem instanceof Table) {
			Table table = (Table) fromItem;
			return (table.getAlias() != null) ? table.getAlias().getName() : table.getName();
		} else {
			throw new UnsupportedExpressionException("select from item must be aliased if it's not a table: " + fromItem);
		}
	}

	/**
	 * Filter the table map by the select statement tables referenced, and remap their names
	 * to any aliases used.
	 * @param select
	 * @param createTableInfoMap
	 * @return
	 * @throws Exception
	 */
	public static TableInfoMap selectTables(Select select, TableInfoMap createTableInfoMap)
	throws Exception {
		if (select.getSelectBody() instanceof PlainSelect) {
			return selectTables((PlainSelect) select.getSelectBody(), createTableInfoMap);
		} else {
			throw new UnsupportedExpressionException("select body is not a plain select " + select.toString());
		}
	}

	/**
	 * Given a SELECT statement, and a map of tables from CREATE TABLE, filter the sub-selects and sub-joins
	 * from the SELECT statement into the map of tables. Return the resulting map.
	 * @param plainSelect SELECT statement
	 * @param createTableInfoMap table map from CREATE TABLE statements.
	 * @return merged table map.
	 * @throws Exception
	 */
	public static TableInfoMap selectTables(PlainSelect plainSelect, TableInfoMap createTableInfoMap)
	throws Exception {

		// I'd love to use the TableNamesFinder, but it doesn't do aliases.
		TableInfoMap selectTableInfoMap = new TableInfoMap();
		filterTables(plainSelect, selectTableInfoMap, createTableInfoMap);
		return selectTableInfoMap;
	}

	/**
	 * Iterate backwards over the join list and merge any tables which are created in this select (by subselects)
	 * @param plainSelect SELECT statement
	 * @param selectTableInfoMap
	 * @param createTableInfoMap table map from CREATE TABLE statements.
	 * @throws Exception
	 */
	private static void filterTables(PlainSelect plainSelect,
									 TableInfoMap selectTableInfoMap,
									 TableInfoMap createTableInfoMap)
	throws Exception {
		// reverse the join list, since dependencies are right to left.
		List<Join> joinList = ListUtil.notNull(plainSelect.getJoins());
		Collections.reverse(joinList);
		Stream.concat(joinList.stream().map(join -> join.getRightItem()),
					  Stream.of(plainSelect.getFromItem()))
				.forEach(CheckedConsumer.wrap(fromItem -> filterTables(fromItem, selectTableInfoMap, createTableInfoMap)));
	}

	/**
	 * Filter and add the tables from createTableInfoMap referenced by the fromItem into the selectTableInfoMap
	 * @param fromItem SELECT FROM item.
	 * @param selectTableInfoMap populated from tables used in SELECT statement.
	 * @param createTableInfoMap CREATE TABLE source tables.
	 * @throws Exception if a table couldn't be found.
	 */
	public static void filterTables(FromItem fromItem,
									TableInfoMap selectTableInfoMap,
									TableInfoMap createTableInfoMap) throws Exception {
		if (fromItem instanceof Table) {
			Table table = (Table) fromItem;

			// do not use aliases when searching against the create table map.
			TableInfo tableInfo = createTableInfoMap.getTableInfo(table.getName());
			if (tableInfo == null) {
				throw new TableNotFoundException(table, "not found in select table map");
			}

			// The added table info uses the table reference for THIS select from items which may be aliased.
			// Later operations may change the column info from the table by aliasing, hence we copy it here.
			selectTableInfoMap.put(table, new TableInfo(tableInfo, true, table.getAlias()));
		} else if (fromItem instanceof SubJoin) {
			SubJoin subJoin = (SubJoin) fromItem;

			// add in reverse, because leftmost columns depend on rightmost tables.
			for (int i = subJoin.getJoinList().size() - 1; i >= 0; i--) {
				Join join = subJoin.getJoinList().get(i);
				filterTables(join.getRightItem(), selectTableInfoMap, createTableInfoMap);
			}
		} else if (fromItem instanceof SubSelect) {
			SubSelect subSelect = (SubSelect) fromItem;
			if (subSelect.getSelectBody() instanceof PlainSelect) {
				PlainSelect plainSelect = (PlainSelect) subSelect.getSelectBody();
				TableInfoMap subSelectTableInfoMap = selectTables(plainSelect, createTableInfoMap);
				// create a new synthetic table.
				Alias alias = subSelect.getAlias();
				if (alias == null) {
					throw new MissingRequirementException("when creating the SELECT expression table/column map, " +
							 					          " subselect expressions must be named with an alias " +
														  " subselect: " + subSelect);
				}
				List<SelectItem> expandedSelectItems =
						SelectItemUtils.expandSelectItemList(plainSelect.getSelectItems(), subSelectTableInfoMap);
				List<ColumnInfo> columnInfoList =
						SelectItemUtils.columnInfoListFromSelectItems(expandedSelectItems, subSelectTableInfoMap);
				TableInfo newTableInfo = new TableInfo(new Table(alias.getName()), columnInfoList, null, true);
				selectTableInfoMap.put(alias.getName(), newTableInfo, true);
			}
		}
	}
}
