package com.apptechnik.appinabox.engine.parser.query;
import java.io.File;
import java.io.StringReader;
import java.io.IOException;

import com.apptechnik.appinabox.engine.exceptions.SQLVerifyException;
import com.apptechnik.appinabox.engine.exceptions.TableNotFoundException;
import com.apptechnik.appinabox.engine.parser.create.StatementParser;
import com.apptechnik.appinabox.engine.parser.TableInfoMap;
import com.apptechnik.appinabox.engine.util.FileUtil;
import net.sf.jsqlparser.parser.CCJSqlParserManager;
import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.ParseException;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.select.*;

/**
 * methods to verify SQL expressions
 */
public class VerifySQL {
	private CCJSqlParserManager parseManager;
	private TableInfoMap tableInfoMap;
	private boolean verbose;

	public VerifySQL(File createTableFile, boolean verbose) throws Exception { 
		this.parseManager = new CCJSqlParserManager();
		String createTableText = FileUtil.readFileToString(createTableFile);
        StatementParser statementParser = new StatementParser();
		statementParser.parse(createTableText);
		this.tableInfoMap = statementParser.getTableInfoMap();
		this.verbose = verbose;
	}

	public void verifySelect(String selectText) throws SQLVerifyException, JSQLParserException {
		Select select = (Select) parseManager.parse(new StringReader(selectText));
		SelectBody selectBody = select.getSelectBody();
		if (!(selectBody instanceof PlainSelect)) {
			throw new SQLVerifyException("select statement: " + selectBody.toString() + " is not a Plain Select");
		}
		PlainSelect plainSelect = (PlainSelect) select.getSelectBody();
		for (SelectItem selectItem : plainSelect.getSelectItems()) {
			verifySelectItem(selectItem);
		}
	}

	private void verifySelectItem(SelectItem selectItem) throws SQLVerifyException {
		if (selectItem instanceof SelectExpressionItem) {
			SelectExpressionItem selectExpressionItem = (SelectExpressionItem) selectItem;
			if (selectExpressionItem.getExpression() instanceof Column) {
				if (verbose) {
					System.out.println("select item check: " + selectExpressionItem.getExpression().toString() + " is a column");
				}
			} else {
				if (selectExpressionItem.getAlias() == null) {
					throw new SQLVerifyException("select item expressions must have an alias: " + selectItem.toString());
				}
			}
		} else if (selectItem instanceof AllColumns) {
			if (verbose) {
				System.out.println("select item check: " + selectItem.toString() + " is all columns");
			}
		} else if (selectItem instanceof AllTableColumns) {
			if (verbose) {
				System.out.println("select item check: " + selectItem.toString() + " is all table columns");
			}
		}
	}
}
