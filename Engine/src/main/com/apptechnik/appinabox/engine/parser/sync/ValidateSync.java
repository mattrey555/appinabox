package com.apptechnik.appinabox.engine.parser.sync;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.parser.TableInfo;
import com.apptechnik.appinabox.engine.parser.TableInfoMap;
import com.apptechnik.appinabox.engine.parser.Traverse;
import com.apptechnik.appinabox.engine.parser.query.SelectTableUtils;
import com.apptechnik.appinabox.engine.util.StringUtil;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.util.TablesNamesFinder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class ValidateSync {
    /**
     * If a SELECT call references a sync table in the FROM clause, it must have the expression
     * table.deleted = false in its WHERE clause.
     * @param statement SQL statement.
     * @return
     */
    public static List<String> validateSyncConventions(String sourceFilePath,
                                                       Statement statement,
                                                       TableInfoMap createTableInfoMap,
                                                       Collection<String> syncTableNames)
            throws Exception {
        List<String> errorMessages = new ArrayList<String>();
        TablesNamesFinder tableNamesFinder = new TablesNamesFinder();
        List<String> syncTables =
                tableNamesFinder.getTableList(statement).stream()
                        .filter(name -> syncTableNames.contains(name))
                        .collect(Collectors.toList());
        if (!syncTables.isEmpty()) {
            if ((statement instanceof Select) && ((Select) statement).getSelectBody() instanceof PlainSelect) {
                PlainSelect ps = (PlainSelect) ((Select) statement).getSelectBody();
                if (ps.getWhere() == null) {
                    errorMessages.add("in " + sourceFilePath +
                                      " table(s) " + StringUtil.concat(syncTables, ",") +
                                      " marked as sync, but needs to be referred to with " + Constants.SQLColumns.DELETED +
                                      " flag the WHERE expression of " + ps);
                }
                TableInfoMap selectTableMap = SelectTableUtils.selectTables(ps, createTableInfoMap);
                for (String syncTableName : syncTables) {
                    TableInfo table = selectTableMap.getTableInfo(syncTableName);
                    if (!table.containsName(Constants.SQLColumns.DELETED)) {
                        errorMessages.add("in " + sourceFilePath +
                                          " table " + syncTableName +
                                          " was marked as sync, but needs to be referred to with " + Constants.SQLColumns.DELETED +
                                          " flag the WHERE expression of " + ps);
                    }
                }
            }
        }
        return errorMessages;
    }


    public static List<String> validateSelectTableReferences(String sourceFilePath,
                                                             Select select,
                                                             TableInfoMap createTableInfoMap,
                                                             Collection<String> syncTables) throws Exception {
        List<String> errorList = new ArrayList<String>();
        for (TableInfo table : SelectTableUtils.selectTables(select, createTableInfoMap).getTableInfoList()) {
            if (syncTables.contains(table.getName())) {
                if (!table.containsName(table.getDeletedColumnName())) {
                    errorList.add("table " + table.getName() +
                            " is marked for synchronization in " + sourceFilePath +
                            " and must contain a " + Constants.SQLColumns.DELETED +
                            " boolean flag for soft deletes");
                } else if (!table.isType(table.getDeletedColumnName(), Constants.SQLTypes.BOOLEAN)) {
                    errorList.add("table " + table.getName() +
                            " is marked for synchronization in " + sourceFilePath +
                            " the " + table.getDeletedColumnName() +
                            " column should be " + Constants.SQLTypes.BOOLEAN +
                            " and is " + table.getColDataType(Constants.SQLColumns.DELETED));
                }
                if (!table.containsName(table.getModifiedTimestampColumnName())) {
                    errorList.add("table " + table.getName() +
                            " is marked for synchronization in " + sourceFilePath +
                            " and must contain a " + Constants.SQLColumns.MODIFIED_DATE +
                            " timestamp for deltas");
                } else if (!table.isType(table.getModifiedTimestampColumnName(), Constants.SQLTypes.TIMESTAMP)) {
                    errorList.add("table " + table.getName() +
                            " is marked for synchronization in " + sourceFilePath +
                            " the " + table.getModifiedTimestampColumnName() +
                            " column should be " + Constants.SQLTypes.TIMESTAMP +
                            " and is " + table.getColDataType(table.getModifiedTimestampColumnName()));
                }
            }
        }
        return errorList;
    }


}
