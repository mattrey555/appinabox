package com.apptechnik.appinabox.engine.parser.types;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.exceptions.TypeNotFoundException;
import com.apptechnik.appinabox.engine.util.FileUtil;
import com.apptechnik.appinabox.engine.util.StringUtil;
import com.google.gson.Gson;
import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

/**
 * Map of SQL type to Java and Kotlin types Java.sql.ResultSet accessors and setters,
 * conversion methods, and reasonable default values.
 */
public class SQLMapType {
    protected final String sqlTypeName;					// SQL Type name (VARCHAR, INTEGER, etc)
    protected final int sqlType;						// java.sql.Types constant
    protected final String javaSimpleTypeName;			// int, float, etc
    protected final String javaBoxedTypeName;			// Integer, Float, etc.
    protected final String kotlinSimpleTypeName;        // Int, Float, Short, Long.
    protected final String resultSetFn;					// getInt(), getFloat(), etc.
    protected final String setParamFn;					// setInt(), setFloat(), etc
    protected final String setListParamFn;				// setIntList(), setFloatList(), etc.
    protected final String stringConversionPackage;		// package for String conversion function
    protected final String stringConversionMethod;		// string conversion method.
    protected final String listStringConversionMethod;	// string conversion method.
    protected final boolean isPrimitive;				// is primitive (int, Integer) vs Blob/String
    protected final String reasonableDefault;	        // reasonable default value for script generation

    private static SQLMapType[] SQL_MAP_TYPE_INSTANCE = null;

    public SQLMapType(String sqlTypeName,
                      int sqlType,
                      String javaSimpleTypeName,
                      String javaBoxedTypeName,
                      String kotlinSimpleTypeName,
                      String resultSetFn,
                      String setParamFn,
                      String setListParamFn,
                      String stringConversionPackage,
                      String stringConversionMethod,
                      String listStringConversionMethod,
                      boolean isPrimitive,
                      String reasonableDefault) {
        this.sqlTypeName = sqlTypeName;
        this.sqlType = sqlType;
        this.javaSimpleTypeName = javaSimpleTypeName;
        this.javaBoxedTypeName = javaBoxedTypeName;
        this.kotlinSimpleTypeName = kotlinSimpleTypeName;
        this.resultSetFn = resultSetFn;
        this.setParamFn = setParamFn;
        this.setListParamFn = setListParamFn;
        this.stringConversionPackage = stringConversionPackage;
        this.stringConversionMethod = stringConversionMethod;
        this.listStringConversionMethod = listStringConversionMethod;
        this.isPrimitive = isPrimitive;
        this.reasonableDefault = reasonableDefault;
    }

    public String getSqlTypeName() {
        return sqlTypeName;
    }

    public int getSqlType() {
        return sqlType;
    }

    public String getJavaSimpleTypeName() {
        return javaSimpleTypeName;
    }

    public String getJavaBoxedTypeName() {
        return javaBoxedTypeName;
    }

    public String getStringConversionPackage() {
        return stringConversionPackage;
    }

    public String getStringConversionMethod() {
        return stringConversionMethod;
    }

    public boolean hasStringConversion() {
        return (stringConversionPackage != null) && (stringConversionMethod != null);
    }

    public String getResultSetFn() {
        return resultSetFn;
    }

    public String getSetParamFn() {
        return setParamFn;
    }

    public String getSetListParamFn() {
        return setListParamFn;
    }

    public String getListStringConversionMethod() {
        return listStringConversionMethod;
    }

    public boolean isPrimitive() {
        return isPrimitive;
    }

    public static String getDefault(String sqlTypeName)
    throws TypeNotFoundException, IOException {
        return getSQLMapType(sqlTypeName).reasonableDefault;
    }
    /**
     * The SQL type may actually be mulitple strings, like INTEGER NOT NULL, we want the "INTEGER" part.
     * Validate that white space is the ONLY delimiter.
     * @param sqlTypeName
     * @return
     * @throws TypeNotFoundException
     */
    public static SQLMapType getSQLMapType(String sqlTypeName)
            throws TypeNotFoundException {
        String[] typeParts = StringUtil.splitAndTrim(sqlTypeName,"[ \t]");
        String primaryType = typeParts[0];
        SQLMapType[] sqlMapTypes = null;

        // throws an IOException, but I don't want to propagate that exception type.
        try {
            sqlMapTypes = getInstance();
        } catch (IOException ioex) {
            throw new TypeNotFoundException("The type map could not be loaded from " + Constants.Resources.SQL_MAP_TYPE);
        }

        // A lot of SQL is not case-sensitive, hence equalsIgnoreCase
        Optional<SQLMapType> sqlMapType = Arrays.stream(sqlMapTypes)
                .filter(t -> t.sqlTypeName.equalsIgnoreCase(primaryType))
                .findFirst();
        if (sqlMapType.isPresent()) {
            return sqlMapType.get();
        }
        throw new TypeNotFoundException("failed to find SQL Type " + sqlTypeName);
    }

    /**
     * Generate an expression of the form:
     * {@code nps.set<Type>("<columnName>", <value>) }
     * If the types for the value and expected parameter type aren't the same:
     * {@code nps.set<Type>("<columnName>", (cast-type) <value>) }
     * @param statementVar generated NamedParamStatement variable name
     * @param destSqlTypeName SQL type in statement
     * @param srcSqlTypeName SQL type of named parameter
     * @param namedParam named parameter to set.
     * @param value variable name of the value to set.
     * @return {@code MethodCall <statement-var>.set<type>("<columnName>", value) }
     */
    public MethodCall setNamedParamFromSqlType(CodeGen codegen,
                                               String destSqlTypeName,
                                               String srcSqlTypeName,
                                               Variable statementVar,
                                               StringConstant namedParam,
                                               CodeExpression value) {
        if (!srcSqlTypeName.equals(destSqlTypeName)) {
            value = codegen.castExpr(codegen.simpleType(getJavaSimpleTypeName()), value);
        }
        return codegen.methodCall(statementVar, setParamFn, namedParam, value);
    }

    /**
     * Generate an expression of the form:
     * {@code nps.set<Type>("<columnName>", <value>) }
     * @param statementVar generated NamedParamStatement variable name
     * @param columnName name of the column to set
     * @param value variable name of the value to set.
     * @return {@code MethodCall <statement-var>.set<type>("<columnName>", value) }
     */
    public MethodCall setNamedParamFromSqlType(CodeGen codegen,
                                               boolean isList,
                                               String statementVar,
                                               String columnName,
                                               String value)  {
        return codegen.methodCall(codegen.var(statementVar),
                                  isList ? getSetListParamFn() : getSetParamFn(),
                                  codegen.stringConst(columnName),
                                  codegen.var(value));
    }

    /**
     * Generate the expression which converts from a string to the desired type.
     * @param isList type is a list
     * @param expr source expression
     * @return CodeExpression to convert to data type from string.
     */
    public CodeExpression convertFromString(CodeGen codegen, boolean isList, CodeExpression expr) {
        if (hasStringConversion()) {
            Variable converterPackage = codegen.var(isList ? Constants.Packages.STRING_UTILS : stringConversionPackage);
            String methodName = isList ? listStringConversionMethod : stringConversionMethod;
            return codegen.methodCall(converterPackage, methodName, expr);
        } else {
            return expr;
        }
    }

    public MemberDeclaration getJavaMemberDeclaration(CodeGen codegen, String varName, boolean isNullable) {
        String varType = isNullable ? getJavaBoxedTypeName() : getJavaSimpleTypeName();
        return codegen.memberDecl(Constants.JavaKeywords.PRIVATE, false, codegen.simpleType(varType), codegen.var(varName));
    }
    /**
     * Generate a == b or a.equals(b) dependening on whether the type is primtive.
     * @param left
     * @param right
     * @return
     * @throws TypeNotFoundException
     */
    public CodeExpression comparison(CodeGen codegen,
                                     CodeExpression left,
                                     CodeExpression right) throws TypeNotFoundException {
        if (isPrimitive()) {
            return codegen.binaryExpr(Constants.JavaOperators.EQUALS, left, right);
        } else {
            return codegen.methodCall(left, Constants.Functions.EQUALS, right);
        }
    }

    /**
     * Generate a != b or !a.equals(b) dependening on whether the type is primitive.
     * @param left
     * @param right
     * @return
     * @throws TypeNotFoundException
     */
    public CodeExpression notComparison(CodeGen codegen,
                                        CodeExpression left,
                                        CodeExpression right) throws TypeNotFoundException {
        if (isPrimitive()) {
            return codegen.binaryExpr(Constants.JavaOperators.NOT_EQUALS, left, right);
        } else {
            return codegen.notExpr(codegen.methodCall(left, Constants.Functions.EQUALS, right));
        }
    }

    public Type getJavaType(CodeGen codegen, boolean isNullable)  {
        return codegen.simpleType(isNullable ? javaBoxedTypeName : javaSimpleTypeName);
    }

    public Type getKotlinType(CodeGen codegen, boolean isNullable)  {
        return codegen.simpleType(kotlinSimpleTypeName, isNullable);
    }

    /**
     * Read the list of sql type mappings from widget-converters.json
     * @param resourceName
     * @return
     * @throws IOException
     */
    public static SQLMapType[] readSQLMapType(String resourceName) throws IOException {
        String json = FileUtil.getResource(SQLMapType.class, resourceName);
        Gson gson = new Gson();
        return gson.fromJson(json, SQLMapType[].class);
    }

    public static SQLMapType[] getInstance() throws IOException {
        if (SQL_MAP_TYPE_INSTANCE == null) {
            SQL_MAP_TYPE_INSTANCE = readSQLMapType(Constants.Resources.SQL_MAP_TYPE);
        }
        return SQL_MAP_TYPE_INSTANCE;
    }
}
