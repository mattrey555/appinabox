package com.apptechnik.appinabox.engine.parser.types;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.codegen.pojos.java.*;
import com.apptechnik.appinabox.engine.exceptions.TypeNotFoundException;
import net.sf.jsqlparser.statement.create.table.ColDataType;

public class SQLType {
	
	public SQLType() {
	}

	public static MethodCall setNamedParamFromSqlType(CodeGen codegen,
													  String destSqlTypeName,
													  String srcSqlTypeName,
													  Variable statementVar,
													  StringConstant namedParam,
													  CodeExpression value) throws TypeNotFoundException  {
		SQLMapType sqlMapType = SQLMapType.getSQLMapType(srcSqlTypeName);
		return sqlMapType.setNamedParamFromSqlType(codegen, destSqlTypeName, srcSqlTypeName, statementVar, namedParam, value);
	}

	/**
	 * Generate an expression of the form:
	 * {@code nps.set<Type>("<columnName>", <value>) }
	 * @param sqlTypeName SQLType name from SQLTypes.java
	 * @param statementVar generated NamedParamStatement variable name
	 * @param columnName name of the column to set
	 * @param value variable name of the value to set.
	 * @return {@code MethodCall <statement-var>.set<type>("<columnName>", value) }
	 * @throws TypeNotFoundException
	 */
	public static MethodCall setNamedParamFromSqlType(CodeGen codegen,
													  String sqlTypeName,
													  boolean isList,
													  String statementVar,
													  String columnName,
													  String value) throws TypeNotFoundException  {
		SQLMapType sqlMapType = SQLMapType.getSQLMapType(sqlTypeName);
		return sqlMapType.setNamedParamFromSqlType(codegen, isList, statementVar, columnName, value);
	}

	// Variable var, MethodName method, ArgumentList args

	/**
	 * Generate the expression which converts from a string to the desired type.
	 * @param sqlTypeName SQLType name from SQLTypes.java
	 * @param isList type is a list
	 * @param expr source expression
	 * @return CodeExpression to convert to data type from string.
	 * @throws TypeNotFoundException
	 */
	public static CodeExpression convertFromString(CodeGen codegen,
												   String sqlTypeName,
												   boolean isList,
												   CodeExpression expr) throws TypeNotFoundException {
		return SQLMapType.getSQLMapType(sqlTypeName).convertFromString(codegen, isList, expr);
	}

	public static Type getJavaTypeFromSQLType(CodeGen codegen,
											  String sqlTypeName,
											  boolean isNullable) throws TypeNotFoundException {
		return SQLMapType.getSQLMapType(sqlTypeName).getJavaType(codegen, isNullable);
	}

	public static MemberDeclaration getJavaMemberDeclaration(CodeGen codegen,
															 String sqlTypeName,
															 String varName,
															 boolean isNullable) throws TypeNotFoundException {
		return SQLMapType.getSQLMapType(sqlTypeName).getJavaMemberDeclaration(codegen, varName, isNullable);
	}

	/**
	 * Generate a != b or !a.equals(b) dependening on whether the type is primtive.
	 * @param sqlTypeName
	 * @param left
	 * @param right
	 * @return
	 * @throws TypeNotFoundException
	 */
	public static CodeExpression notComparison(CodeGen codegen,
											   String sqlTypeName,
											   CodeExpression left,
											   CodeExpression right) throws TypeNotFoundException {
		return SQLMapType.getSQLMapType(sqlTypeName).notComparison(codegen, left, right);
	}

	/**
	 * TODO: see what this was supposed to be used for before removing it.
	 * Column autogeneration is defined differently in different SQL implementations
	 * Postgres: SERIAL type
	 * SQLite: AUTOINCREMENT (after INTEGER type)
	 * @param s
	 * @return
	 */
	public static boolean isAutoGenerate(String s) {
		return s.equalsIgnoreCase(Constants.SQLTypes.SERIAL);
	}

	/**
	 * Given the SQL CREATE TABLE field declaration type, return the corresponding java.sql.Type
	 * NOTE: can this be replaced with JDBCType?
	 * @param sqlTypeName SQL type name as a string.
	 * @return java.sql.Type constant.
	 * @throws TypeNotFoundException
	 */
	public static int getJavaSqlTypeFromString(String sqlTypeName) throws TypeNotFoundException {
		// there are suffixes for each type, but the first one is the one we switch on.
		String[] components = sqlTypeName.split("[ \t]");
		String firstComponent = components[0];
		String secondComponent = null;
		if (components.length >= 2) {
			secondComponent = components[1];
		}
		SQLMapType sqlMapType = SQLMapType.getSQLMapType(firstComponent);
		return sqlMapType.getSqlType();
	}

	public static String getJavaSqlTypeNameFromString(String sqlTypeName) throws TypeNotFoundException {
		// there are suffixes for each type, but the first one is the one we switch on.
		String[] components = sqlTypeName.split("[ \t]");
		String firstComponent = components[0];
		String secondComponent = null;
		if (components.length >= 2) {
			secondComponent = components[1];
		}
		SQLMapType sqlMapType = SQLMapType.getSQLMapType(firstComponent);
		return sqlMapType.getSqlTypeName();
	}

	/**
	 * Return the java type as a string which maps best to this java.sql.Type
	 * @param typeName
	 * @param columnName
	 * @return java type string.
	 * @throws TypeNotFoundException
	 */
	public static MethodCall resultSetCallFromSqlType(CodeGen codegen,
													  String resultSetName,
													  String typeName,
													  String columnName) throws TypeNotFoundException {
		String fn =  SQLMapType.getSQLMapType(typeName).getResultSetFn();
		return codegen.methodCall(codegen.var(resultSetName), fn, codegen.stringConst(columnName));
	}
}
