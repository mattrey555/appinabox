package com.apptechnik.appinabox.engine.parser.util;

import com.apptechnik.appinabox.engine.exceptions.FunctionNotFoundException;
import com.apptechnik.appinabox.engine.exceptions.TypeMismatchException;
import com.apptechnik.appinabox.engine.exceptions.TypeNotFoundException;
import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.util.StringUtil;
import net.sf.jsqlparser.expression.BinaryExpression;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.operators.arithmetic.*;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.conditional.OrExpression;
import net.sf.jsqlparser.expression.operators.relational.*;
import net.sf.jsqlparser.statement.create.table.ColDataType;

/**
 * Utilities for inferring data types from expressions.
 */
public class ColDataTypeUtils {
    public final static ColDataType NUMERIC = new ColDataType(Constants.SQLTypes.NUMERIC);
    public final static ColDataType INTEGER = new ColDataType(Constants.SQLTypes.INTEGER);
    public final static ColDataType TEXT = new ColDataType(Constants.SQLTypes.TEXT);
    public final static ColDataType VARCHAR = new ColDataType(Constants.SQLTypes.VARCHAR);
    public final static ColDataType STRING = new ColDataType(Constants.SQLTypes.STRING);
    public final static ColDataType BOOLEAN = new ColDataType(Constants.SQLTypes.BOOLEAN);
    public final static ColDataType SAME = new ColDataType(Constants.SQLTypes.SAME);
    public final static ColDataType DATE = new ColDataType(Constants.SQLTypes.DATE);
    public final static ColDataType DATETIME = new ColDataType(Constants.SQLTypes.DATETIME);
    public final static ColDataType DATETIMEOFFSET = new ColDataType(Constants.SQLTypes.DATETIMEOFFSET);
    public final static ColDataType DOUBLE = new ColDataType(Constants.SQLTypes.DOUBLE);
    public final static ColDataType FLOAT = new ColDataType(Constants.SQLTypes.FLOAT);
    public final static ColDataType TIMESTAMP = new ColDataType(Constants.SQLTypes.TIMESTAMP);
    public final static ColDataType TIME = new ColDataType(Constants.SQLTypes.TIME);
    public final static ColDataType NULL = new ColDataType(Constants.SQLTypes.NULL);
    public final static ColDataType NAMED_PARAMETER = new ColDataType(Constants.SQLTypes.NAMED_PARAMETER);
    public final static ColDataType INTERVAL = new ColDataType(Constants.SQLTypes.INTERVAL);
    public final static ColDataType ANY = new ColDataType(Constants.SQLTypes.ANY);
    public final static ColDataType ANY_NUMBER = new ColDataType(Constants.SQLTypes.ANY_NUMBER);

    // type promotion handy reference:
    // https://i.stack.imgur.com/HKZCS.gif
    private static final String[] NUMERIC_ORDERING = new String[]{
            Constants.SQLTypes.BIT,
            Constants.SQLTypes.TINYINT,
            Constants.SQLTypes.SMALLINT,
            Constants.SQLTypes.INTEGER,
            Constants.SQLTypes.LONG,
            Constants.SQLTypes.BIGINT,
            Constants.SQLTypes.REAL,
            Constants.SQLTypes.DECIMAL,
            Constants.SQLTypes.NUMERIC,
            Constants.SQLTypes.FLOAT,
            Constants.SQLTypes.DOUBLE
    };

    private static final String[] TEXT_ORDERING = new String[] {
            Constants.SQLTypes.CHAR,
            Constants.SQLTypes.NCHAR,
            Constants.SQLTypes.VARCHAR,
            Constants.SQLTypes.NVARCHAR,
            Constants.SQLTypes.LONGVARCHAR,
            Constants.SQLTypes.NLONGVARCHAR,
            Constants.SQLTypes.TEXT,
            Constants.SQLTypes.CHARACTER_VARYING
    };

    private static final String[] DATE_ORDERING = new String[] {
        Constants.SQLTypes.DATE,
        Constants.SQLTypes.TIME,                        // SPECIAL CASE: DATE + TIME -> DATETIME
        Constants.SQLTypes.SMALLDATETIME,
        Constants.SQLTypes.DATETIME,
        Constants.SQLTypes.TIMESTAMP,
        Constants.SQLTypes.DATETIMEOFFSET
    };

    private static final String[] BINARY_ORDERING = new String[]{
        Constants.SQLTypes.CLOB,
        Constants.SQLTypes.NCLOB,
        Constants.SQLTypes.BINARY,
        Constants.SQLTypes.VARBINARY,
        Constants.SQLTypes.LONGVARBINARY
    };

    // type mapping for supported binary operators.
    private static final BinaryOperatorType[] binaryOperatorTypes = new BinaryOperatorType[] {
            new BinaryOperatorType(Addition.class, NUMERIC, NUMERIC, NUMERIC),
            new BinaryOperatorType(BitwiseLeftShift.class, INTEGER, INTEGER, INTEGER),
            new BinaryOperatorType(BitwiseRightShift.class, INTEGER, INTEGER, INTEGER),
            new BinaryOperatorType(BitwiseAnd.class, INTEGER, INTEGER, INTEGER),
            new BinaryOperatorType(BitwiseOr.class, INTEGER, INTEGER, INTEGER),
            new BinaryOperatorType(BitwiseXor.class, INTEGER, INTEGER, INTEGER),
            new BinaryOperatorType(Concat.class, STRING, STRING, STRING),
            new BinaryOperatorType(ComparisonOperator.class, SAME, SAME, BOOLEAN),
            new BinaryOperatorType(Division.class, NUMERIC, NUMERIC, NUMERIC),
            new BinaryOperatorType(IntegerDivision.class, INTEGER, INTEGER, INTEGER),
            new BinaryOperatorType(Multiplication.class, NUMERIC, NUMERIC, NUMERIC),
            new BinaryOperatorType(Subtraction.class, NUMERIC, NUMERIC, NUMERIC),
            new BinaryOperatorType(Modulo.class, INTEGER, INTEGER, INTEGER),
            new BinaryOperatorType(AndExpression.class, BOOLEAN, BOOLEAN, BOOLEAN),
            new BinaryOperatorType(OrExpression.class, BOOLEAN, BOOLEAN, BOOLEAN),
            new BinaryOperatorType(LikeExpression.class, VARCHAR, VARCHAR, BOOLEAN),
            new BinaryOperatorType(SimilarToExpression.class, VARCHAR, VARCHAR, BOOLEAN),
            new BinaryOperatorType(EqualsTo.class, SAME, SAME, BOOLEAN),
            new BinaryOperatorType(GreaterThan.class, SAME, SAME, BOOLEAN),
            new BinaryOperatorType(GreaterThanEquals.class, SAME, SAME, BOOLEAN),
            new BinaryOperatorType(MinorThan.class, SAME, SAME, BOOLEAN),
            new BinaryOperatorType(MinorThanEquals.class, SAME, SAME, BOOLEAN),
            new BinaryOperatorType(NotEqualsTo.class, SAME, SAME, BOOLEAN)
    };

    public static boolean isStringType(ColDataType colDataType) {
        return StringUtil.indexOf(colDataType.getDataType(), TEXT_ORDERING) > 0;
    }

    public static boolean isNumericType(ColDataType colDataType) {
        return StringUtil.indexOf(colDataType.getDataType(), NUMERIC_ORDERING) > 0;
    }

    public static boolean isArithmeticExpression(Expression expr) {
        if (expr instanceof BinaryExpression) {
            BinaryOperatorType op = findBinaryOperator(((BinaryExpression) expr).getClass());
            return op.getReturnType().equals(NUMERIC) || op.getReturnType().equals(INTEGER);
        }
        return false;
    }

    public static boolean isLogicalExpression(Expression expr) {
        if (expr instanceof BinaryExpression) {
            BinaryOperatorType op = findBinaryOperator(((BinaryExpression) expr).getClass());
            return op.getReturnType().equals(BOOLEAN) && op.getLeftType().equals(BOOLEAN) && op.getRightType().equals(BOOLEAN);
        }
        return false;
    }

    public static boolean isBooleanExpression(Expression expr) {
        if (expr instanceof BinaryExpression) {
            BinaryOperatorType op = findBinaryOperator(((BinaryExpression) expr).getClass());
            return op.getReturnType().equals(BOOLEAN);
        }
        return false;
    }

    /**
     * Given two columns used in an expression, return the data type from the "higher" data type, for
     * example, double is higher than float, and long is higher than integer.  If we don't handle the
     * conversion, we return the first argument. NOTE: We should at least show a warning if so.
     * @param a column data type.
     * @param b column data type.
     * @return column data type.
     * @throws TypeMismatchException
     */
    public static ColDataType promoteDataType(ColDataType a, ColDataType b) throws TypeMismatchException {
        if (a.getDataType().equals(b.getDataType())) {
            return a;
        }
        if (a.getDataType().equals(Constants.SQLTypes.SAME)) {
            return b;
        }
        if (b.getDataType().equals(Constants.SQLTypes.SAME)) {
            return a;
        }
        if (a.getDataType().equals(Constants.SQLTypes.NAMED_PARAMETER)) {
            return b;
        }
        if (b.getDataType().equals(Constants.SQLTypes.NAMED_PARAMETER)) {
            return a;
        }
        int numericIndexA = StringUtil.indexOfIgnoreCase(a.getDataType(), NUMERIC_ORDERING);
        if (numericIndexA >= 0) {
            int numericIndexB = StringUtil.indexOfIgnoreCase(b.getDataType(), NUMERIC_ORDERING);
            if (numericIndexB < 0) {
                throw new TypeMismatchException("cannot perform an operation between types ", a, b);
            }
            return new ColDataType(NUMERIC_ORDERING[Math.max(numericIndexA, numericIndexB)]);
        }
        int textIndexA = StringUtil.indexOfIgnoreCase(a.getDataType(), TEXT_ORDERING);
        if (textIndexA >= 0) {
            int textIndexB = StringUtil.indexOfIgnoreCase(b.getDataType(), TEXT_ORDERING);
            if (textIndexB < 0) {
                throw new TypeMismatchException("cannot perform an operation between types ", a, b);
            }
            return new ColDataType(TEXT_ORDERING[Math.max(textIndexA, textIndexB)]);
        }
        int dateIndexA = StringUtil.indexOfIgnoreCase(a.getDataType(), DATE_ORDERING);
        if (dateIndexA >= 0) {
            int dateIndexB = StringUtil.indexOfIgnoreCase(b.getDataType(), DATE_ORDERING);
            if (dateIndexB < 0) {
                throw new TypeMismatchException("cannot perform an operation between types ", a, b);
            }
            return new ColDataType(TEXT_ORDERING[Math.max(dateIndexA, dateIndexB)]);
        }
        int binaryIndexA = StringUtil.indexOfIgnoreCase(a.getDataType(), BINARY_ORDERING);
        if (binaryIndexA >= 0) {
            int binaryIndexB = StringUtil.indexOfIgnoreCase(b.getDataType(), BINARY_ORDERING);
            if (binaryIndexB < 0) {
                throw new TypeMismatchException("cannot perform an operation between types ", a, b);
            }
            return new ColDataType(TEXT_ORDERING[Math.max(binaryIndexA, binaryIndexB)]);
        } else {
            throw new TypeMismatchException("cannot resolve types ", a, b);
        }
    }


    public static <T extends Expression> boolean isBinaryOperator(T obj) throws FunctionNotFoundException {
        if (BinaryExpression.class.isAssignableFrom(obj.getClass())) {
            return findBinaryOperator(((BinaryExpression) obj).getClass()) != null;
        }
        return false;
    }

    public static <T extends BinaryExpression> BinaryOperatorType findBinaryOperator(Class<T> cls) {
        for (int iOp = 0; iOp < binaryOperatorTypes.length; iOp++) {
            if (binaryOperatorTypes[iOp].matchClass(cls)) {
                return binaryOperatorTypes[iOp];
            }
        }
        return null;
    }
    /**
     * Find the binary operator matching cls.
     * @param cls class to query against.
     * @param <T>
     * @return
     */
    public static <T extends BinaryExpression> BinaryOperatorType getBinaryOperator(Class<T> cls)
    throws FunctionNotFoundException {
        for (int iOp = 0; iOp < binaryOperatorTypes.length; iOp++) {
            if (binaryOperatorTypes[iOp].matchClass(cls)) {
                return binaryOperatorTypes[iOp];
            }
        }
        throw new FunctionNotFoundException("Failed to match binary operator for " + cls.getName());
    }

    public static <T extends BinaryExpression> ColDataType inferBinaryOperatorArgumentType(T obj,
                                                                                           ColDataType leftType,
                                                                                           ColDataType rightType)
        throws FunctionNotFoundException, TypeMismatchException {
        Class<T> cls = (Class<T>) obj.getClass();
        BinaryOperatorType opType = getBinaryOperator(cls);
        if (leftType.getDataType().equals(Constants.SQLTypes.NAMED_PARAMETER)) {
            ColDataType argumentColDataType = opType.getLeftType();
            return promoteDataType(argumentColDataType, rightType);
        } else {
            ColDataType argumentColDataType = opType.getRightType();
            return promoteDataType(argumentColDataType, leftType);
        }
    }

    public static <T extends BinaryExpression> ColDataType getBinaryOperatorReturnType(Class<T> cls,
                                                                                       ColDataType left,
                                                                                       ColDataType right)
        throws FunctionNotFoundException, TypeMismatchException {
        BinaryOperatorType opType = getBinaryOperator(cls);
        // not always both same.
        if (opType.getLeftType().equals(SAME) && opType.getRightType().equals(SAME)) {
            return ColDataTypeUtils.promoteDataType(left, right);
        } else {
            left = promoteDataType(left, opType.getLeftType());
            right = promoteDataType(right, opType.getLeftType());
            if (opType.getReturnType().equals(Constants.SQLTypes.SAME)) {
                if (left.equals(right)) {
                    return opType.getReturnType(); // SUSPICIOUS
                } else {
                    throw new TypeMismatchException("operator " + opType.exprType.getClass() +
                            " expects types which are similar to one another but was given: ", left, right);
                }
            } else {
                return opType.getReturnType();
            }
        }
    }
    /**
     * CLass which defines the argument and return type for common binary SQL operators.
     * @param <T>
     */
    private static class BinaryOperatorType<T extends BinaryExpression> {
        private final Class<T> exprType;                // binary expression type from SQL parser.
        private final ColDataType leftType;
        private final ColDataType rightType;
        private final ColDataType returnType;

        public BinaryOperatorType(Class<T> exprType,
                                  ColDataType leftType,
                                  ColDataType rightType,
                                  ColDataType returnType) {
            this.exprType = exprType;
            this.leftType = leftType;
            this.rightType = rightType;
            this.returnType = returnType;
        }

        public Class<? extends BinaryExpression> getExprType() {
            return exprType;
        }

        public ColDataType getLeftType() {
            return leftType;
        }

        public ColDataType getRightType() {
            return rightType;
        }

        public ColDataType getReturnType() {
            return returnType;
        }

        public boolean matchClass(Class<T> cls) {
            return exprType.equals(cls);
        }
    }
}
