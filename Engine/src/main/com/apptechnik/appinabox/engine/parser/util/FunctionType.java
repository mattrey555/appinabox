package com.apptechnik.appinabox.engine.parser.util;

import com.apptechnik.appinabox.engine.Constants;
import net.sf.jsqlparser.expression.Function;
import net.sf.jsqlparser.statement.create.table.ColDataType;

import java.util.Arrays;
import java.util.Optional;

/**
 * Type inference for SQL functions, such as ABS, COUNT, etc.
 */
public class FunctionType {
    private final String functionName;
    private final int sameTypeAsArg;
    private final ColDataType resultType;
    private final ColDataType[] argTypes;

    public FunctionType(String functionName,
                        int sameTypeAsArg,
                        ColDataType resultType,
                        ColDataType[] argTypes) {
        this.functionName = functionName;
        this.sameTypeAsArg = sameTypeAsArg;
        this.resultType = resultType;
        this.argTypes = argTypes;
    }

    public String getFunctionName() {
        return functionName;
    }

    public int isSameTypeAsArg() {
        return sameTypeAsArg;
    }

    public ColDataType getResultType() {
        return resultType;
    }

    public ColDataType[] getArgTypes() {
        return argTypes;
    }

    /**
     * Function the return type associated with this function name.
     * @param functionName name of the function (case insensitive)
     * @return
     */
    public static FunctionType getFunctionType(String functionName) {
        for (int i = 0; i < allFunctions.length; i++) {
            FunctionType[] functionTypes = allFunctions[i];
            Optional<FunctionType> fnType = Arrays.stream(functionTypes)
                    .filter(function -> function.getFunctionName().equalsIgnoreCase(functionName))
                    .findAny();
            if (fnType.isPresent()) {
                return fnType.get();
            }
        }
        return null;
    }

    public static boolean isAggregationFunction(String functionName) {
        return Arrays.stream(aggregationFunctions).anyMatch(function -> function.getFunctionName().equalsIgnoreCase(functionName));
    }


    public static boolean isAggregationFunction(Function function) {
        return isAggregationFunction(function.getName());
    }



    private static ColDataType[] DOUBLE_ARG = new ColDataType[] { ColDataTypeUtils.DOUBLE };
    private static ColDataType[] NO_ARGS = new ColDataType[] { };
    private static ColDataType[] STRING_ARG = new ColDataType[] { ColDataTypeUtils.STRING };
    private static ColDataType[] ANY_ARG = new ColDataType[] { ColDataTypeUtils.STRING };
    private static ColDataType[] ANY_NUMBER_ARG = new ColDataType[] { ColDataTypeUtils.ANY_NUMBER };

    private static FunctionType[] aggregationFunctions = {
        new FunctionType(Constants.SQLFunctions.COUNT, -1, ColDataTypeUtils.INTEGER, ANY_ARG), // Returns the number of records returned by a select query
        new FunctionType(Constants.SQLFunctions.MAX, 0, ColDataTypeUtils.SAME, ANY_NUMBER_ARG), // Returns the maximum value in a set of values
        new FunctionType(Constants.SQLFunctions.MIN, 0, ColDataTypeUtils.SAME, ANY_NUMBER_ARG), // Returns the minimum value in a set of values
        new FunctionType(Constants.SQLFunctions.SUM, 0, ColDataTypeUtils.SAME, ANY_NUMBER_ARG), // Calculates the sum of a set of values
        new FunctionType(Constants.SQLFunctions.AVG, 0, ColDataTypeUtils.SAME, ANY_NUMBER_ARG) // Returns the average value of an expression
    };


    private static FunctionType[] mathFunctions = {
        new FunctionType(Constants.SQLFunctions.ABS, 0, ColDataTypeUtils.SAME, DOUBLE_ARG),         // Returns the absolute value of a number
        new FunctionType(Constants.SQLFunctions.ACOS, 0, ColDataTypeUtils.SAME, DOUBLE_ARG),        // Returns the arc cosine of a number
        new FunctionType(Constants.SQLFunctions.ASIN, 0, ColDataTypeUtils.SAME, DOUBLE_ARG),        // Returns the arc sine of a number
        new FunctionType(Constants.SQLFunctions.ATAN, 0, ColDataTypeUtils.SAME,DOUBLE_ARG),         // Returns the arc tangent of a number
        new FunctionType(Constants.SQLFunctions.ATN2, 0, ColDataTypeUtils.SAME,
                         new ColDataType[] { ColDataTypeUtils.DOUBLE, ColDataTypeUtils.DOUBLE }),    // Returns the arc tangent of two numbers
        new FunctionType(Constants.SQLFunctions.CEILING, -1, ColDataTypeUtils.INTEGER, DOUBLE_ARG),  // Returns the smallest integer value that is >= a number
        new FunctionType(Constants.SQLFunctions.COS, 0, ColDataTypeUtils.SAME, DOUBLE_ARG),          // Returns the cosine of a number
        new FunctionType(Constants.SQLFunctions.COT, 0, ColDataTypeUtils.SAME, DOUBLE_ARG),          // Returns the cotangent of a number
        new FunctionType(Constants.SQLFunctions.DEGREES, 0, ColDataTypeUtils.SAME, DOUBLE_ARG),      // Converts a value in radians to degrees
        new FunctionType(Constants.SQLFunctions.EXP, 0, ColDataTypeUtils.SAME, DOUBLE_ARG),          // Returns e raised to the power of a specified number
        new FunctionType(Constants.SQLFunctions.FLOOR, -1, ColDataTypeUtils.INTEGER, DOUBLE_ARG),    // Returns the largest integer value that is <= to a number
        new FunctionType(Constants.SQLFunctions.LOG, 0, ColDataTypeUtils.SAME, DOUBLE_ARG),          // Returns the natural logarithm of a number, or the logarithm of a number to a specified base
        new FunctionType(Constants.SQLFunctions.LOG10, 0, ColDataTypeUtils.SAME, DOUBLE_ARG),        // Returns the natural logarithm of a number to base 10
        new FunctionType(Constants.SQLFunctions.PI, 0, ColDataTypeUtils.DOUBLE, NO_ARGS),            // Returns the value of PI
        new FunctionType(Constants.SQLFunctions.POWER, 0, ColDataTypeUtils.DOUBLE, DOUBLE_ARG),      // Returns the value of a number raised to the power of another number
        new FunctionType(Constants.SQLFunctions.RADIANS, 0, ColDataTypeUtils.DOUBLE,DOUBLE_ARG),     // Converts a degree value into radians
        new FunctionType(Constants.SQLFunctions.RAND, -1, ColDataTypeUtils.DOUBLE, NO_ARGS),         // Returns a random number
        new FunctionType(Constants.SQLFunctions.ROUND, -1, ColDataTypeUtils.INTEGER, DOUBLE_ARG),    // Rounds a number to a specified number of decimal places
        new FunctionType(Constants.SQLFunctions.SIGN, -1, ColDataTypeUtils.INTEGER, DOUBLE_ARG),     // Returns the sign of a number
        new FunctionType(Constants.SQLFunctions.SIN, 0, ColDataTypeUtils.DOUBLE, DOUBLE_ARG),        // Returns the sine of a number
        new FunctionType(Constants.SQLFunctions.SQRT, 0, ColDataTypeUtils.DOUBLE, DOUBLE_ARG),       // Returns the square root of a number
        new FunctionType(Constants.SQLFunctions.SQUARE, 0, ColDataTypeUtils.DOUBLE, DOUBLE_ARG),     // Returns the square of a number
        new FunctionType(Constants.SQLFunctions.TAN, 0, ColDataTypeUtils.DOUBLE, DOUBLE_ARG)         // Returns the tangent of a number
    };

    // NOTE: these are very server-specific (Postgres vs MySQL)
    private static FunctionType[] dateFunctions = {
        new FunctionType(Constants.SQLFunctions.AGE, -1, ColDataTypeUtils.INTERVAL,                         // difference betwween timestamps
                         new ColDataType[] { ColDataTypeUtils.TIMESTAMP, ColDataTypeUtils.TIMESTAMP }),
        new FunctionType(Constants.SQLFunctions.AGE, -1, ColDataTypeUtils.INTERVAL,                         // difference betwween timestamp and now
                         new ColDataType[] { ColDataTypeUtils.TIMESTAMP }),
        new FunctionType(Constants.SQLFunctions.CURRENT_TIMESTAMP, -1, ColDataTypeUtils.DATETIME,NO_ARGS),  // Returns the current date and time
        new FunctionType(Constants.SQLFunctions.CLOCK_TIMESTAMP, -1, ColDataTypeUtils.DATE, NO_ARGS),       // Returns the current date and time
        new FunctionType(Constants.SQLFunctions.CURRENT_DATE, -1, ColDataTypeUtils.DATE, NO_ARGS),          // Returns the current date and time
        new FunctionType(Constants.SQLFunctions.CURRENT_TIME, -1, ColDataTypeUtils.TIME, NO_ARGS),          // Returns the current date and time
        new FunctionType(Constants.SQLFunctions.DATE_PART, -1, ColDataTypeUtils.INTEGER,
                         new ColDataType[] { ColDataTypeUtils.STRING, ColDataTypeUtils.TIMESTAMP }),
        new FunctionType(Constants.SQLFunctions.DATE_PART, -1, ColDataTypeUtils.INTEGER,
                         new ColDataType[] { ColDataTypeUtils.STRING, ColDataTypeUtils.INTERVAL }),
        new FunctionType(Constants.SQLFunctions.DATE_TRUNC, -1, ColDataTypeUtils.INTEGER,
                        new ColDataType[] { ColDataTypeUtils.STRING, ColDataTypeUtils.TIMESTAMP }),
        new FunctionType(Constants.SQLFunctions.EXTRACT, -1, ColDataTypeUtils.INTEGER,
                         new ColDataType[] { ColDataTypeUtils.STRING, ColDataTypeUtils.INTERVAL }),
        new FunctionType(Constants.SQLFunctions.EXTRACT, -1, ColDataTypeUtils.INTEGER,
                         new ColDataType[] { ColDataTypeUtils.STRING, ColDataTypeUtils.TIMESTAMP }),
        new FunctionType(Constants.SQLFunctions.ISFINITE, -1, ColDataTypeUtils.BOOLEAN,
                         new ColDataType[] { ColDataTypeUtils.DATE }),
        new FunctionType(Constants.SQLFunctions.ISFINITE, -1, ColDataTypeUtils.BOOLEAN,
                         new ColDataType[] { ColDataTypeUtils.TIMESTAMP }),
        new FunctionType(Constants.SQLFunctions.ISFINITE, -1, ColDataTypeUtils.BOOLEAN,
                         new ColDataType[] { ColDataTypeUtils.INTERVAL }),
        new FunctionType(Constants.SQLFunctions.JUSTIFY_DAYS, -1, ColDataTypeUtils.BOOLEAN,
                         new ColDataType[] { ColDataTypeUtils.INTERVAL }),
        new FunctionType(Constants.SQLFunctions.NOW, -1, ColDataTypeUtils.TIMESTAMP, NO_ARGS),
        new FunctionType(Constants.SQLFunctions.STATEMENT_TIMESTAMP, -1, ColDataTypeUtils.TIMESTAMP,NO_ARGS),
        new FunctionType(Constants.SQLFunctions.TRANSACTION_TIMESTAMP, -1, ColDataTypeUtils.TIMESTAMP,NO_ARGS),
        new FunctionType(Constants.SQLFunctions.TIMEOFDAY, -1, ColDataTypeUtils.STRING, NO_ARGS),
    };

    private static FunctionType[] miscFunctions = {
        new FunctionType(Constants.SQLFunctions.COALESCE, -1, ColDataTypeUtils.SAME, NO_ARGS),          // Returns the first non-null value in a list
        new FunctionType(Constants.SQLFunctions.ISNULL, -1, ColDataTypeUtils.BOOLEAN, ANY_ARG),       // Return a specified value if the expression is NULL, otherwise return the expression (NOTE: SPECIAL CASE UNIMPLEMENTED)
        new FunctionType(Constants.SQLFunctions.ISNUMERIC, -1, ColDataTypeUtils.BOOLEAN, ANY_ARG)    // Tests whether an expression is numeric
    };


    private static FunctionType[] stringFunctions = {
            new FunctionType(Constants.SQLFunctions.BIT_LENGTH, -1, ColDataTypeUtils.INTEGER, STRING_ARG),
            new FunctionType(Constants.SQLFunctions.CHAR_LENGTH, -1, ColDataTypeUtils.INTEGER, STRING_ARG),
            new FunctionType(Constants.SQLFunctions.CHARACTER_LENGTH, -1, ColDataTypeUtils.INTEGER, STRING_ARG),
            new FunctionType(Constants.SQLFunctions.OCTET_LENGTH, -1, ColDataTypeUtils.INTEGER, STRING_ARG),
            new FunctionType(Constants.SQLFunctions.LOWER,  0, ColDataTypeUtils.SAME, STRING_ARG),
            new FunctionType(Constants.SQLFunctions.UPPER,  0, ColDataTypeUtils.SAME, STRING_ARG),
    };

    private static FunctionType[][] allFunctions = {
            aggregationFunctions,
            miscFunctions,
            mathFunctions,
            dateFunctions,
            stringFunctions
    };
}
