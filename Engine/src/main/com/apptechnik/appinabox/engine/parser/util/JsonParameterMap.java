package com.apptechnik.appinabox.engine.parser.util;

import com.apptechnik.appinabox.engine.codegen.pojos.*;

import com.apptechnik.appinabox.engine.exceptions.AmbiguousNameException;
import com.apptechnik.appinabox.engine.exceptions.NameNotFoundException;
import com.apptechnik.appinabox.engine.exceptions.TypeMismatchException;
import com.apptechnik.appinabox.engine.exceptions.TypeNotFoundException;
import com.apptechnik.appinabox.engine.parser.pojos.MergedFieldNode;
import com.apptechnik.appinabox.engine.parser.pojos.ParameterMap;
import com.apptechnik.appinabox.engine.parser.pojos.StatementParamMap;
import com.apptechnik.appinabox.engine.parser.pojos.Tree;
import com.apptechnik.appinabox.engine.util.StatementUtil;
import com.apptechnik.appinabox.engine.util.StringUtil;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.select.Select;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Map of statements to JSON paths in the request JSON for their named parameters
 */
public class JsonParameterMap {
    private Map<Statement, List<JsonPathMatcher.PathMatch<MergedFieldNode>>> jsonParameterMap;

    /**
     * Construct the map of SQL statements to lists of paths in the json to the statement JDBC parameters.
     * @param statementList List of statements which might have unspecified JDBC Parameters (dot-syntax allowed)
     *                      from the JSON request (filter select statements only)
     * @param queryParams List of query parameters to filter out parameters that weren't defined
     *                    in the URL path, query, or header parameters.
     * @param statementParamMap map of statements to their JDBC parameter maps + dot-syntax -> JDBC params
     * @throws AmbiguousNameException
     */
    public JsonParameterMap(List<Statement> statementList,
                            ParameterMap queryParams,
                            StatementParamMap statementParamMap,
                            Tree<MergedFieldNode> jsonRequestTree)
        throws AmbiguousNameException, NameNotFoundException, TypeMismatchException {
        jsonParameterMap = new HashMap<Statement, List<JsonPathMatcher.PathMatch<MergedFieldNode>>>();
        for (Select select : StatementUtil.filterSelects(statementList)) {
            ParameterMap parameterMap = statementParamMap.getParamMap(select);
            List<String> undefinedJdbcParameters = parameterMap.undefinedParams(queryParams);
            jsonParameterMap.put(select, getJsonParameters(jsonRequestTree, parameterMap, undefinedJdbcParameters));
        }
    }

    /**
     * Wrapper for JsonPathMatcher.matchPaths, which takes the undefined parameters, and tries to find
     * them in the JSON request
     * @param jsonRequestTree incoming JSON request.
     * @param undefinedJdbcParameters JDBC parameters which have no mappings.
     * @return list of path matches from JSON (may be empty)
     * @throws AmbiguousNameException
     */
    private static List<JsonPathMatcher.PathMatch<MergedFieldNode>> getJsonParameters(Tree<MergedFieldNode> jsonRequestTree,
                                                                                      ParameterMap parameterMap,
                                                                                      List<String> undefinedJdbcParameters)
            throws AmbiguousNameException, NameNotFoundException, TypeMismatchException {
        List<String> jsonRequestParameters = new ArrayList<String>();
        List<JsonPathMatcher.PathMatch<MergedFieldNode>> jsonParameters = new ArrayList<JsonPathMatcher.PathMatch<MergedFieldNode>>();
        if (jsonRequestTree != null) {
            for (String jdbcParam : undefinedJdbcParameters) {
                String dotParam = parameterMap.dotParamName(jdbcParam);
                List<String> pathList = StringUtil.splitAndTrimList(dotParam, "\\.");

                // copy this because we're creating a new list of ParamData and changing the parameter types.
                ParameterMap.ParamData paramData = new ParameterMap.ParamData(parameterMap.getParamData(jdbcParam));
                if (!paramData.isList()) {
                    throw new TypeMismatchException("parameter " + paramData +
                                                   " does not have a list type. Elements in the request JSON must have a list type");
                }
                List<JsonPathMatcher.PathMatch<MergedFieldNode>> matches = JsonPathMatcher.matchPaths(jsonRequestTree, paramData, pathList);
                if (matches.size() > 1) {
                    throw new AmbiguousNameException(jdbcParam + " ambiguously matches: " +
                                                     matches.stream().map(match -> match.toString()).collect(Collectors.joining("\n")));
                } else if (matches.size() == 1) {
                    jsonRequestParameters.add(jdbcParam);
                    jsonParameters.add(matches.get(0));
                } else {
                    throw new NameNotFoundException(jdbcParam + " was not found in " + jsonRequestTree.get());
                }
            }
            undefinedJdbcParameters.removeAll(jsonRequestParameters);
        }
        return jsonParameters;
    }

    /**
     * Return the generated map of statements to lists of paths in the incoming JSON for their parameters.
     * @return map of statements to list of path matches.
     */
    public Map<Statement, List<JsonPathMatcher.PathMatch<MergedFieldNode>>> getMap() {
        return jsonParameterMap;
    }

    public Collection<JsonPathMatcher.PathMatch<MergedFieldNode>> getParamMapSet() {
        return jsonParameterMap.values().stream()
                .flatMap(matchList -> matchList.stream())
                .collect(Collectors.toSet());
    }

    /**
     * The path assignment is called from CreateObjects, and the extract functions are defined in
     * RequestObjects, but the named parameters for the SELECT calls
     * is called from CreateQueryObjects, so after filtering the named parameters from the query and header
     * we also filter them from the incoming JSON extraction, otherwise CreateObjects throws an
     * UndefinedParameterException
     * @param undefinedParams list of JDBC Named Parameters (dot-syntax supported)
     * @return list of parameters still undefined.
     */
    public List<String> filterUndefinedParams(List<String> undefinedParams) {
        // map from matching list to JDBC param name.
        List<String> matchedParams = getParamMapSet().stream()
                .map(item -> StringUtil.concat(item.getSourcePath(), "_"))
                .collect(Collectors.toList());
        return undefinedParams.stream()
                .filter(undefinedParam -> !matchedParams.contains(undefinedParam))
                .collect(Collectors.toList());
    }

    public void mergeIntoParameterMap(ParameterMap parameterMap) {
        Collection<JsonPathMatcher.PathMatch<MergedFieldNode>> paramMapSet = getParamMapSet();
        paramMapSet.stream().forEach(pathMatch -> parameterMap.put(pathMatch.getSourceRef(), pathMatch.getParamData()));
    }

    /**
     * Generate the extraction expression:
     * {@code List<boxed-primitive-type> <extract_var> = <object-class>.<extract_method>(<object-name>)}
     * @param objectClass where the extract method lives
     * @param objectName object to extract from.
     * @return list of typed assignment statements of the above form
     */
    public StatementList parametersFromRequestJson(CodeGen codegen,
                                                   String objectClass,
                                                   String objectName) throws TypeNotFoundException {
        List<CodeStatement> statementList = new ArrayList<CodeStatement>();
        for (JsonPathMatcher.PathMatch<MergedFieldNode> pathMatch : getParamMapSet()) {
            Variable var = codegen.var(pathMatch.getExtractVariableName());
            Type elementType = codegen.typeFromSQLType(pathMatch.getParamData().getDataType().getDataType(), true);
            GenericType listType = codegen.listType(elementType);
            TypedAssignmentStatement extractAssignment;

            // if this is a singleton node, then it's a list of boxed primitives, otherwise list of child type.
            if ((pathMatch.getMatchNodes().size() == 1) && (MergedFieldNode.isSingleField(pathMatch.getMatchNodes().get(0)))) {
                extractAssignment = codegen.typedAssignStmt(listType, var, codegen.var(objectName));
            } else {
                MethodCall extractCall = codegen.methodCall(codegen.var(objectClass),
                                                            pathMatch.getExtractMethodName(), codegen.var(objectName));
                extractAssignment = codegen.typedAssignStmt(listType, var, extractCall);
            }
            statementList.add(extractAssignment);
        }
        return codegen.stmtList(statementList);
    }

    public boolean containsRef(String ref) {
        return jsonParameterMap.values().stream()
                .flatMap(List::stream)
                .anyMatch(m -> m.getSourceRef().equals(ref));
    }

}
