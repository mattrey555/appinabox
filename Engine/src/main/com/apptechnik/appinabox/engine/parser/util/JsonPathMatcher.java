package com.apptechnik.appinabox.engine.parser.util;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.codegen.util.CodegenUtil;
import com.apptechnik.appinabox.engine.exceptions.TypeNotFoundException;
import com.apptechnik.appinabox.engine.parser.pojos.FieldReferenceNode;
import com.apptechnik.appinabox.engine.parser.pojos.JsonFieldReference;
import com.apptechnik.appinabox.engine.parser.pojos.ParameterMap;
import com.apptechnik.appinabox.engine.parser.pojos.Tree;
import com.apptechnik.appinabox.engine.util.ListUtil;
import com.apptechnik.appinabox.engine.util.StringUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class JsonPathMatcher {

    /**
     * Find partial paths of the form a.b.c or b.c, which don't necessarily match rom the root.
     * @param tree tree of JSON nodes to match
     * @param paramData type information on the JDBC Named Parameter (dot-syntax allowed)
     * @param path path to match.
     * @param <T>
     * @return List of lists of matching nodes (from the root)
     */
    public static <T extends FieldReferenceNode> List<PathMatch<T>> matchPaths(Tree<T> tree,
                                                                               ParameterMap.ParamData paramData,
                                                                               List<String> path) {
        Stack<Tree<T>> stack = new Stack<Tree<T>>();
        List<PathMatch<T>> matchLists = new ArrayList<PathMatch<T>>();
        recursivelyMatchPath(tree, stack, path, matchLists);

        // we set the source path and param data after, because recursion consumes the list.
        // set the paramData source to JSON_REQUEST.  Parameters from Request JSON are ALWAYS lists.
        paramData.setSource(ParameterMap.ParamSource.REQUEST_JSON);
        paramData.setIsList(true);
        matchLists.forEach(match -> { match.setSourcePath(path); match.setParamData(paramData); });
        return matchLists;
    }

    /**
     * Generate a function of the form:
     * {@code
     * public List<type> extractChild2Child3(<root-type> root) {}
     * return var.getChild().stream()
     *      .map(child1 -> item.getChild1())
     *      .map(child2 -> item.getChild2())
     *      .map(child3 -> item.getChild3())
     *      .collect(Collectors.toList());
     * }
     * @param pathMatch
     * @return function definition described above
     * @throws TypeNotFoundException a type reference couldn't be resolved.
     * TODO: THIS IS FAILING!  It's not handling the mapping correctly at all.
     */
    public static <T extends FieldReferenceNode> FunctionDefinition extractListFromPath(CodeGen codegen,
                                                                                        PathMatch<T> pathMatch)
    throws TypeNotFoundException {
        Type sqlType = codegen.typeFromSQLType(pathMatch.getParamData().getDataType().getDataType(), true);
        Type returnType = codegen.listType(sqlType);
        // method name is of the form: extract_<pathMatch concat with _>
        String rootClassName = pathMatch.getMatchNodes().get(0).get().getVarName();
        Type listArgType = codegen.listType(codegen.classType(rootClassName));
        ParameterDeclaration arg = codegen.paramDecl(listArgType, codegen.var(rootClassName));
        FunctionDeclaration functionDecl =
            codegen.functionDecl(Constants.JavaKeywords.PUBLIC, true, pathMatch.getExtractMethodName(),
                                 codegen.paramList(arg), returnType);

        Variable item = codegen.var(Constants.Variables.ITEM);
        // construct the chain of .map(item -> item.getField())
        CodeExpression accessorChainCall = codegen.var(rootClassName);

        // if the node has children then generate .map(item -> getField()), otherwise, it's just getField()
        for (Tree<T> treeNode : ListUtil.rest(pathMatch.getMatchNodes())) {
            LambdaExpression lambdaExpression = codegen.lambdaExpr(item, CodegenUtil.accessorCall(codegen, item, treeNode.get().getVarName()));
            accessorChainCall = codegen.dot(accessorChainCall, CodegenUtil.streamMap(codegen, lambdaExpression));
            accessorChainCall = codegen.dot(accessorChainCall, CodegenUtil.flatMap(codegen));
        }

        // the last call is from the JSON field reference, if the last node was not a single field node, then
        // map the accessor and collect the results in a list.
        if (!FieldReferenceNode.isSingleField(ListUtil.last(pathMatch.getMatchNodes()))) {
            String fieldName = pathMatch.getFieldReference().getColumnName();
            LambdaExpression lambdaExpression = codegen.lambdaExpr(item, CodegenUtil.accessorCall(codegen, item, fieldName));
            accessorChainCall = codegen.dot(accessorChainCall, CodegenUtil.streamMap(codegen, lambdaExpression));
        }
        accessorChainCall = codegen.dot(accessorChainCall, CodegenUtil.collectToList(codegen));
        return codegen.functionDef(functionDecl, codegen.stmtList(codegen.returnStmt(accessorChainCall)));
    }

    /**
     * Match a path from the root of this tree, or if the path represents a scalar field reference,
     * match it against the root.
     * @param <T>
     */
    public static class NodeFullPath<T extends FieldReferenceNode> implements Predicate<T> {
        private List<String> path;
        private List<T> nodeList;

        public NodeFullPath(List<String> path) {
            this.path = path;
            this.nodeList = new ArrayList<T>();
        }

        @Override
        public boolean test(T node) {
            if (path.size() == 1) {
                if (node.matchingFieldReference(path.get(0)) != null) {
                    return true;
                }
            }
            if (path.isEmpty()) {
                return true;
            }
            if (path.get(0).equals(node.getVarName())) {
                nodeList.add(node);
                path = ListUtil.rest(path);
                return true;
            }
            return false;
        }

        public List<T> getNodeList() {
            return nodeList;
        }
    }

    /**
     * If the path has one element, it must match against a field reference in this node.
     * Otherwise if the head of the path matches the name of this node, then recurse
     * over this node's children with the rest of the path.
     * Otherwise recurse over this node's children with the full path.
     * TODO: describe, and possibly reduce this ghastly control logic caused by syntactical ambiguities
     */
    public static <T extends FieldReferenceNode> void recursivelyMatchPath(Tree<T> tree,
                                                                           Stack<Tree<T>> stack,
                                                                           List<String> path,
                                                                           List<PathMatch<T>> matchLists) {
        stack.push(tree);
        boolean hasMatched = false;

        // if there are 3 or more components, then match the head, add it to a copy of
        // the current stack
        if (path.size() >= 3) {
            if (path.get(0).equals(tree.get().getVarName())) {
                JsonFieldReference ref = tree.get().matchingFieldReference(ListUtil.rest(path));
                if (ref != null) {
                    matchLists.add(new PathMatch(new ArrayList<Tree<T>>(stack), ref));
                    hasMatched = true;
                }
            }

        // if there are 2 or less components, match against table.column (table may be null)
        // or match against node-name.field-name
        } else if (path.size() <= 2) {
            JsonFieldReference ref = tree.get().matchingFieldReference(path);
            if (ref != null) {
                matchLists.add(new PathMatch(new ArrayList<Tree<T>>(stack), ref));
                hasMatched = true;
            } else if (tree.get().getVarName().equals(path.get(0))) {
                ref = tree.get().matchingFieldReference(ListUtil.rest(path));
                if (ref != null) {
                    matchLists.add(new PathMatch(new ArrayList<Tree<T>>(stack), ref));
                    hasMatched = true;
                }
            }
        }
        // if the path has not matched any field references in this node, but the head of the
        // path has matched this node name, then recursively scan its children with the rest
        // of the path, otherwise search with the entire path.
        if (!path.isEmpty()) {
            if (path.get(0).equals(tree.get().getVarName()) && !hasMatched) {
                for (Tree<T> child : tree.getChildren()) {
                    recursivelyMatchPath(child, stack, ListUtil.rest(path), matchLists);
                }
            } else {
                for (Tree<T> child : tree.getChildren()) {
                    recursivelyMatchPath(child, stack, path, matchLists);
                }
            }
            stack.pop();
        }
    }

    /**
     * For JDBC List parameters which are set from incoming payload.  The path match is
     * the path from the root to the leaf node, and the field reference in the leaf node
     * @param <T>
     */
    public static class PathMatch<T extends FieldReferenceNode> {
        private List<String> sourcePath;
        private ParameterMap.ParamData paramData;
        private final List<Tree<T>> matchNodes;
        private final JsonFieldReference fieldReference;

        /**
         * Constructor
         * @param matchNodes path from root of nodes which matched expression
         * @param fieldReference leaf node field reference
         */
        public PathMatch(List<Tree<T>> matchNodes, JsonFieldReference fieldReference) {
            this.matchNodes = matchNodes;
            this.fieldReference = fieldReference;
            this.sourcePath = null;
        }

        public List<Tree<T>> getMatchNodes() {
            return matchNodes;
        }

        public JsonFieldReference getFieldReference() {
            return fieldReference;
        }

        public List<String> getSourcePath() {
            return sourcePath;
        }

        public ParameterMap.ParamData getParamData() {
            return paramData;
        }

        public void setParamData(ParameterMap.ParamData paramData) {
            this.paramData = paramData;
        }

        public String getSourceRef() {
            return StringUtil.concat(sourcePath, ".");
        }

        public void setSourcePath(List<String> sourcePath) {
            this.sourcePath = sourcePath;
        }

        public String getExtractMethodName() {
            return Constants.Functions.EXTRACT + "_" + StringUtil.concat(getSourcePath(), "_");
        }
        public String getExtractVariableName() {
            return StringUtil.concat(getSourcePath(), "_") + Constants.Variables.LIST;
        }

        @Override
        public String toString() {
            return matchNodes.stream()
                    .map(tree -> tree.get().getVarName())
                    .collect(Collectors.joining(".")) + "." + fieldReference.toString();
        }

        @Override
        public int hashCode() {
            int code = fieldReference.hashCode();
            for (Tree<T> tree : matchNodes) {
                code ^= tree.get().getVarName().hashCode();
            }
            return code;
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof PathMatch) {
                PathMatch<T> b = (PathMatch<T>) o;
                if (getMatchNodes().equals(b.getMatchNodes())) {
                    return getFieldReference().equals(b.getFieldReference());
                }
            }
            return false;
        }
    }
}
