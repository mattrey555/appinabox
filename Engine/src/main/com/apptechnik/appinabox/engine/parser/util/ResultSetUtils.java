package com.apptechnik.appinabox.engine.parser.util;

import com.apptechnik.appinabox.engine.parser.format.Formatter;
import com.apptechnik.appinabox.engine.parser.pojos.QueryFieldsNodeBuffer;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;

public class ResultSetUtils {

    public enum NullState {
        BOTH_NULL,
        NEITHER_NULL,
        LEFT_NULL,
        RIGHT_NULL
    }

    private static QueryFieldsNodeBuffer.NullState sameNull(ResultSet rs1, ResultSet rs2, int columnIndex) throws SQLException {
        Object o1 = rs1.getObject(columnIndex);
        Object o2 = rs2.getObject(columnIndex);
        if ((o1 == null) && (o2 == null)) {
            return QueryFieldsNodeBuffer.NullState.BOTH_NULL;
        } else if (o1 == null) {
            return QueryFieldsNodeBuffer.NullState.LEFT_NULL;
        } else if (o2 == null) {
            return QueryFieldsNodeBuffer.NullState.RIGHT_NULL;
        } else {
            return QueryFieldsNodeBuffer.NullState.NEITHER_NULL;
        }

    }

    public static boolean sameValue(ResultSet rs1, ResultSet rs2, int sqlType, int columnIndex)
            throws SQLException, IOException {

        // quick-check for both, either, or neither null.
        QueryFieldsNodeBuffer.NullState nullState = sameNull(rs1, rs2, columnIndex);
        if (nullState == QueryFieldsNodeBuffer.NullState.BOTH_NULL) {
            return true;
        } else if (nullState != QueryFieldsNodeBuffer.NullState.NEITHER_NULL) {
            return false;
        }
        switch (sqlType) {
            case Types.ARRAY:

                // TODO: do we need to call Array.free()?
                Array array1 = rs1.getArray(columnIndex);
                Array array2 = rs2.getArray(columnIndex);
                return arrayEquals(array1, array2);
            case Types.BIGINT:
                BigDecimal bigDecimal1 = rs1.getBigDecimal(columnIndex);
                BigDecimal bigDecimal2 = rs2.getBigDecimal(columnIndex);
                return bigDecimal1.equals(bigDecimal2);
            case Types.BIT:
            case Types.BOOLEAN:
                boolean bit1 = rs1.getBoolean(columnIndex);
                boolean bit2 = rs2.getBoolean(columnIndex);
                return bit1 == bit2;
            case Types.BINARY:
                InputStream isBinary1 = rs1.getBinaryStream(columnIndex);
                InputStream isBinary2 = rs2.getBinaryStream(columnIndex);
                return compareStream(isBinary1, isBinary2);
            case Types.BLOB:
                InputStream isBlob1 = rs1.getBlob(columnIndex).getBinaryStream();
                InputStream isBlob2 = rs2.getBlob(columnIndex).getBinaryStream();
                return compareStream(isBlob1, isBlob2);
            case Types.CHAR:
                byte b1 = rs1.getByte(columnIndex);
                byte b2 = rs2.getByte(columnIndex);
                return b1 == b2;
            case Types.CLOB:
            case Types.NCLOB:
                InputStream isClob1 = rs1.getClob(columnIndex).getAsciiStream();
                InputStream isClob2 = rs2.getClob(columnIndex).getAsciiStream();
                return compareStream(isClob1, isClob2);
            // TODO: investigate memory reduction using getAsciiStream()
            case Types.DATALINK:
            case Types.NCHAR:
            case Types.LONGNVARCHAR:
            case Types.LONGVARCHAR:
            case Types.VARCHAR:
                String s1 = rs1.getString(columnIndex);
                String s2 = rs2.getString(columnIndex);
                return s1.equals(s2);
            case Types.DATE:
                // NOTE: for internationalization, we'll need to support calendars.
                Date date1 = rs1.getDate(columnIndex);
                Date date2 = rs2.getDate(columnIndex);
                return date1.equals(date2);
            case Types.DECIMAL:
            case Types.DOUBLE:
                double d1 = rs1.getDouble(columnIndex);
                double d2 = rs2.getDouble(columnIndex);
                return d1 == d2;
            case Types.DISTINCT:
            case Types.INTEGER:
            case Types.NUMERIC:
            case Types.ROWID:
            case Types.SMALLINT:
            case Types.TINYINT:
                int i1 = rs1.getInt(columnIndex);
                int i2 = rs2.getInt(columnIndex);
                return i1 == i2;
            case Types.FLOAT:
                float f1 = rs1.getFloat(columnIndex);
                float f2 = rs2.getFloat(columnIndex);
                return f1 == f2;
            case Types.JAVA_OBJECT:

                throw new SQLException("Java Object Type is not supported");
                // TODO: investigate use of getBinaryStream()
            case Types.LONGVARBINARY:
            case Types.VARBINARY:
                byte[] bytes1 = rs1.getBytes(columnIndex);
                byte[] bytes2 = rs2.getBytes(columnIndex);
                return bytesEqual(bytes1, bytes2);
            // TODO: temporary
            case Types.REF:
            case Types.REF_CURSOR:
                throw new SQLException("Ref Type is not supported");
            case Types.SQLXML:
                InputStream is1 = rs1.getSQLXML(columnIndex).getBinaryStream();
                InputStream is2 = rs2.getSQLXML(columnIndex).getBinaryStream();
                return compareStream(is1, is2);
            case Types.STRUCT:
                throw new SQLException("STRUCT types are not supported");
            case Types.TIME:
            case Types.TIME_WITH_TIMEZONE:
                Time time1 = rs1.getTime(columnIndex);
                Time time2 = rs2.getTime(columnIndex);
                return time1.equals(time2);
            case Types.TIMESTAMP:
            case Types.TIMESTAMP_WITH_TIMEZONE:
                Timestamp timestamp1 = rs1.getTimestamp(columnIndex);
                Timestamp timestamp2 = rs2.getTimestamp(columnIndex);
                return timestamp1.equals(timestamp2);
            default:
                throw new SQLException("unsupported java type code " + sqlType);
        }
    }

    private static boolean compareStream(InputStream is1, InputStream is2) throws IOException {
        int size = 256;
        byte[] arr1 = new byte[size];
        byte[] arr2 = new byte[size];
        do {
            int nbytes1 = is1.read(arr1);
            int nbytes2 = is2.read(arr2);
            if (nbytes1 != nbytes2) {
                return false;
            }
            for (int i = 0; i < nbytes1; i++) {
                if (arr1[i] != arr2[i]) {
                    return false;
                }
            }
            if (nbytes1 != size) {
                return true;
            }
        } while (true);
    }

    private static boolean bytesEqual(byte[] a, byte[] b) {
        int sizeA = a.length;
        int sizeB = b.length;
        if (sizeA != sizeB) {
            return false;
        }
        for (int i = 0; i < sizeA; i++) {
            if (a[i] != b[i]) {
                return false;
            }
        }
        return true;
    }

    private static boolean arrayEquals(Array a, Array b) throws SQLException {
        Object objectA = a.getArray();
        Object objectB = b.getArray();
        if ((objectA instanceof byte[] arrayA) && (objectB instanceof byte[] arrayB)) {
            if (arrayA.length != arrayB.length) {
                return false;
            }
            for (int i = 0; i < arrayA.length; i++) {
                if (arrayA[i] != arrayB[i]) {
                    return false;
                }
            }
            return true;
        } else if ((objectA instanceof char[] arrayA) && (objectB instanceof char[] arrayB)) {
            if (arrayA.length != arrayB.length) {
                return false;
            }
            for (int i = 0; i < arrayA.length; i++) {
                if (arrayA[i] != arrayB[i]) {
                    return false;
                }
            }
            return true;
        } else if ((objectA instanceof short[] arrayA) && (objectB instanceof short[] arrayB)) {
            if (arrayA.length != arrayB.length) {
                return false;
            }
            for (int i = 0; i < arrayA.length; i++) {
                if (arrayA[i] != arrayB[i]) {
                    return false;
                }
            }
            return true;
        } else if ((objectA instanceof int[] arrayA) && (objectB instanceof int[] arrayB)) {
            if (arrayA.length != arrayB.length) {
                return false;
            }
            for (int i = 0; i < arrayA.length; i++) {
                if (arrayA[i] != arrayB[i]) {
                    return false;
                }
            }
            return true;
        } else if ((objectA instanceof float[] arrayA) && (objectB instanceof float[] arrayB)) {
            if (arrayA.length != arrayB.length) {
                return false;
            }
            for (int i = 0; i < arrayA.length; i++) {
                if (arrayA[i] != arrayB[i]) {
                    return false;
                }
            }
            return true;
        } else if ((objectA instanceof double[] arrayA) && (objectB instanceof double[] arrayB)) {
            if (arrayA.length != arrayB.length) {
                return false;
            }
            for (int i = 0; i < arrayA.length; i++) {
                if (arrayA[i] != arrayB[i]) {
                    return false;
                }
            }
            return true;
        } else {
            throw new RuntimeException("unsupported type: " + objectA.getClass().getSimpleName());
        }
    }

    /**
     * Get a value from the SQL resultSet and convert it to a string with the
     * appropriate conversion method.
     * @param columnIndex
     * @param sqlType integer type from Java.sql.Types
     * @return
     */
    public static void outputColumnValue(ResultSet rs,
                                         int columnIndex,
                                         int sqlType,
                                         Formatter formatter,
                                         OutputStream os) throws Exception {
        switch (sqlType) {
            case Types.ARRAY:
                // TODO: do we need to free the array?
                Array array = rs.getArray(columnIndex);
                formatter.formatStream(array, os);
            case Types.BIGINT:
                BigDecimal bigDecimal = rs.getBigDecimal(columnIndex);
                formatter.formatStream(bigDecimal, os);
            case Types.BIT:
            case Types.BOOLEAN:
                boolean bit = rs.getBoolean(columnIndex);
                formatter.formatStream(bit, os);
            case Types.BINARY:
                InputStream isBinary = rs.getBinaryStream(columnIndex);
                formatter.formatStream(isBinary, os);
            case Types.BLOB:
                Blob blob = rs.getBlob(columnIndex);
                InputStream isBlob = blob.getBinaryStream();
                formatter.formatStream(isBlob, os);
            case Types.CHAR:
                byte b = rs.getByte(columnIndex);
                formatter.formatStream(b, os);
            case Types.CLOB:
            case Types.NCLOB:
                Clob clob = rs.getClob(columnIndex);
                formatter.formatStream(clob.getAsciiStream(), os);
                // TODO: investigate memory reduction using getAsciiStream()
            case Types.DATALINK:
            case Types.NCHAR:
            case Types.LONGNVARCHAR:
            case Types.LONGVARCHAR:
            case Types.VARCHAR:
                String s = rs.getString(columnIndex);
                formatter.formatStream(s, os);
            case Types.DATE:
                // NOTE: for internationalization, we'll need to support calendars.
                Date date = rs.getDate(columnIndex);
                formatter.formatStream(date, os);
            case Types.DECIMAL:
            case Types.DOUBLE:
                double d = rs.getDouble(columnIndex);
                formatter.formatStream(d, os);
            case Types.DISTINCT:
            case Types.INTEGER:
            case Types.NUMERIC:
            case Types.ROWID:
            case Types.SMALLINT:
            case Types.TINYINT:
                int i = rs.getInt(columnIndex);
                formatter.formatStream(i, os);
            case Types.FLOAT:
                float f = rs.getFloat(columnIndex);
                formatter.formatStream(f, os);
            case Types.JAVA_OBJECT:

                throw new SQLException("Java Object Type is not supported");
                // TODO: investigate use of getBinaryStream()
            case Types.LONGVARBINARY:
            case Types.VARBINARY:
                byte[] bytes = rs.getBytes(columnIndex);
                formatter.formatStream(bytes, os);
                // TODO: temporary
            case Types.REF:
            case Types.REF_CURSOR:
                throw new SQLException("Ref Type is not supported");
            case Types.SQLXML:
                SQLXML sqlxml = rs.getSQLXML(columnIndex);
                formatter.formatStream(sqlxml, os);
            case Types.STRUCT:
                throw new SQLException("STRUCT types are not supported");
            case Types.TIME:
            case Types.TIME_WITH_TIMEZONE:
                Time time = rs.getTime(columnIndex);
                formatter.formatStream(time, os);
            case Types.TIMESTAMP:
            case Types.TIMESTAMP_WITH_TIMEZONE:
                Timestamp timestamp = rs.getTimestamp(columnIndex);
                formatter.formatStream(timestamp, os);
            default:
                throw new SQLException("unsupported java type code " + sqlType);
        }
    }

}
