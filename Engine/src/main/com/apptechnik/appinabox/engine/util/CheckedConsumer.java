package com.apptechnik.appinabox.engine.util;

import java.util.function.Consumer;
import java.util.function.Function;

@FunctionalInterface
public interface CheckedConsumer<T> {
    void accept(T t) throws Exception;

    public static <T> Consumer<T> wrap(CheckedConsumer<T> checkedConsumer) {
        return t -> {
            try {
                checkedConsumer.accept(t);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };
    }
}