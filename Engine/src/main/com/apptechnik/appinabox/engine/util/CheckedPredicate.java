package com.apptechnik.appinabox.engine.util;

import java.util.function.Function;
import java.util.function.Predicate;

@FunctionalInterface
public interface CheckedPredicate<T> {
    boolean test(T t) throws Exception;

    public static <T> Predicate<T> wrap(CheckedPredicate<T> checkedPredicate) {
        return t -> {
            try {
                return checkedPredicate.test(t);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        };
    }
}