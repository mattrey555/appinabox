package com.apptechnik.appinabox.engine.util;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.exceptions.PropertiesException;
import org.w3c.dom.Element;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Workaround for properties because Properties doesn't support null values.
 * Also, properties with exception handling.
 */
public class ExtraProperties extends Properties {
    private final File file;

    public ExtraProperties(File file) throws IOException {
        this.file = file;
        load(new FileInputStream(file));
    }

    public ExtraProperties(ExtraProperties properties) throws IOException {
        this.putAll(properties);
        this.file = properties.getFile();
    }

    public ExtraProperties() {
        super();
        file = null;
    }

    @Override
    public synchronized Object put(Object key, Object value) {
        if (value == null) {
            return super.put(key, Constants.Substitutions.VALUE_UNDEFINED);
        } else {
            return super.put(key, value);
        }
    }

    @Override
    public synchronized Object get(Object key) {
        Object value = super.get(key);
        if (value instanceof String) {
            if (((String) value).equals(Constants.Substitutions.VALUE_UNDEFINED)) {
                return null;
            }
        }
        return value;
    }

    public synchronized String getProp(String key) throws PropertiesException {
        return getProp(key, null);
    }

    public synchronized String getProp(String key, String description) throws PropertiesException {
        String value = getProperty(key);
        if (value == null) {
            if (description != null) {
                throw new PropertiesException(description + " " + key + " not found in properties file " + file.getAbsolutePath());
            } else {
                throw new PropertiesException(key + " not found in properties file " + file.getAbsolutePath());
            }
        }
        return value;
    }

    public File getFile() {
        return file;
    }

    /**
     * convert x: y to x="y"
     * @return
     */
    public String toXMLAttributes() {
        StringBuilder sb = new StringBuilder();
        for (String key : stringPropertyNames()) {
            String value = getProperty(key);
            sb.append(key + "=\"" + value + "\"");
        }
        return sb.toString();
    }

    public void addAttributes(Element element) {
        for (String key : stringPropertyNames()) {
            String value = getProperty(key);
            element.setAttribute(key, value);
        }
    }
}
