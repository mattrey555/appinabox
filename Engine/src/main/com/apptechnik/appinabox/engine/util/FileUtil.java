package com.apptechnik.appinabox.engine.util;

import com.apptechnik.appinabox.engine.Constants;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * General File Utilities
 */
public final class FileUtil {
   	private FileUtil() {}

	/**
	 * Read a file to a string
	 * @param file File to read
	 * @return contents of file in a string.
	 * @throws IOException
	 */
	public static String readFileToString(File file) throws IOException {
		return readString(new FileInputStream(file));
	}

	/**
	 * Read from an input stream to a string.
	 * @param is input stream
	 * @return contents of input stream in a string.
	 * @throws IOException
	 */
	public static String readString(InputStream is) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		StringBuffer sb = new StringBuffer();
		while (true) {
			String s = br.readLine();
			if (s != null) {
				sb.append(s + "\n");
			} else {
				break;
			}
		}
		return sb.toString();
	}

	/**
	 * Read from a file into a list of strings
	 * @param file file to read
	 * @return list of strings.
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public static List<String> readStringList(File file) throws IOException, FileNotFoundException {
		return readStringList(new FileInputStream(file));
	}

	/**
	 * Read from a file into a list of strings (presuming file has newlines)
	 * @param is input stream
	 * @return list of strings (newlines removed)
	 * @throws IOException
	 */
	public static List<String> readStringList(InputStream is) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		List<String> list = new ArrayList<String>();
		while (true) {
			String s = br.readLine();
			if (s != null) {
				list.add(s);
			} else {
				break;
			}
		}
		return list;
	}

	/**
	 * Write a list of strings to a file
	 * @param file output file
	 * @param stringList string list.
	 * @throws IOException failed to write strings.
	 */
	public static void writeStringList(File file, List<String> stringList) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(file));
		for (String s : stringList) {
			bw.write(s + "\n");
		}
		bw.close();
	}

	/**
	 * Write a single string to a file
	 * @param file output file
	 * @param s string to write.
	 * @throws IOException failed to write string.
	 */
	public static void writeString(File file, String s) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(file));
		bw.write(s);
		bw.close();
	}

	/**
	 * Return a file from a name, throw an exception if it does not exist.
	 * @param simpleType
	 * @param filename
	 * @return
	 * @throws FileNotFoundException
	 */
	public static File checkFile(String simpleType, String filename) throws FileNotFoundException {
		File file = new File(filename);
		if (!file.exists()) {
			throw new FileNotFoundException(simpleType + " file " + file.getAbsolutePath() + " does not exist");
		}
		return file;
	}

	/**
	 * Check that a file can be created. If the parent directories don't exist, then create them.
	 * @param simpleType message to display if the directory can't be created.
	 * @param filename name of file to create
	 * @return File or throw exception.
	 * @throws FileNotFoundException
	 */
	public static File checkCreateFile(String simpleType, String filename) throws FileNotFoundException {
		return checkCreateFile(simpleType, new File(filename));
	}

	public static File checkCreateFile(String simpleType, File file) throws FileNotFoundException {
		File dir = file.getParentFile();
		if (dir != null) {
			if (!dir.exists()) {
				if (!dir.mkdirs()) {
					throw new FileNotFoundException("unable to create " + simpleType + " " + dir.getAbsolutePath());
				}
			}
		}
		return file;
	}

	/**
	 * Check that a directory can be created. If the directory doesn't exist, then create it.
	 * @param simpleType message to display if the directory can't be created.
	 * @param directoryName name of file to create
	 * @return File or throw exception.
	 * @throws FileNotFoundException
	 */
	public static File checkDirectory(String simpleType, String directoryName) throws FileNotFoundException {
		File dir = new File(directoryName);
		if (!dir.exists()) {
			if (!dir.mkdirs()) {
				throw new FileNotFoundException("unable to create " + simpleType + " " + dir.getAbsolutePath());
			}
		}
		return dir;
	}

	/**
	 * Check that a directory can be created. If the directory doesn't exist, then create it.
	 * @param simpleType message to display if the directory can't be created.
	 * @param dir directory to create.
	 * @throws FileNotFoundException if the directory couldn't be created.
	 */
	public static File checkDirectory(String simpleType, File dir) throws FileNotFoundException {
		if (!dir.exists()) {
			if (!dir.mkdirs()) {
				throw new FileNotFoundException("unable to create " + simpleType + " " + dir.getAbsolutePath());
			}
		}
		return dir;
	}

	/**
	 * Read a string from a resource file
	 * @param c class so we can get the class loader
	 * @param resourceName name of resource to read
	 * @return text
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public static String getResource(Class c, String resourceName) throws IOException, FileNotFoundException {
		InputStream isResource = c.getClassLoader().getResourceAsStream(resourceName);
		if (isResource == null) {
			throw new FileNotFoundException("failed to open resource " + resourceName);
		}
		String resourceText = null;
		try {
			resourceText = FileUtil.readString(isResource);
		} finally {
			isResource.close();
		}
		return resourceText;
	}

	/**
	 * Read a list of strings from a resource.
	 * @param c to get the ClassLoader to get the resource as a stream.
	 * @param resourceName name of resource to read
	 * @return array of strings, with empty lines filtered out.
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public static String[] getResourceStrings(Class c, String resourceName) throws IOException, FileNotFoundException {
		InputStream isResource = c.getClassLoader().getResourceAsStream(resourceName);
		if (isResource == null) {
			throw new FileNotFoundException("failed to open resource " + resourceName);
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(isResource));
		List<String> resourceTextList = new ArrayList<String>();
		do {
			String s = br.readLine();
			if (s != null) {

				// it's more common that we don't want to read an empty line.
				s = s.trim();
				if (!s.isEmpty()) {
					resourceTextList.add(s);
				}
			} else {
				break;
			}
		} while (true);
		return resourceTextList.toArray(new String[resourceTextList.size()]);
	}

	/**
	 * Copy a resource to a file
	 * @param c class for class loader
	 * @param resourceName relative path to resource
	 * @param file output file
	 * @throws IOException
	 */
	public static void copyResourceToFile(Class c, String resourceName, File file) throws IOException {
		InputStream isResource = c.getClassLoader().getResourceAsStream(resourceName);
		if (isResource == null) {
			throw new FileNotFoundException("failed to open resource " + resourceName);
		}
		FileOutputStream fos = new FileOutputStream(file);
		byte[] buffer = new byte[1024];
		do {
			int length = isResource.read(buffer);
			if (length > 0) {
				fos.write(buffer, 0, length);
			} else {
				break;
			}
		} while (true);
		isResource.close();
		fos.close();
	}

	/**
	 * for copying resources to a target directory (created if needed).
	 * @param c
	 * @param resDirName
	 * @param targetDir
	 * @throws IOException
	 */
	public static void copyResourceDirectory(Class c, String resDirName, String[] resFiles, File targetDir)
	throws IOException {
		if (!targetDir.exists() && !targetDir.mkdirs()) {
			throw new IOException(targetDir.getAbsolutePath() + " does not exist and cannot be created");
		}
		for (String res : resFiles) {
			File targetFile = new File(targetDir, res);
			String resPath = resDirName + "/" + res;
			copyResourceToFile(c, resPath, targetFile);
		}
	}

	/**
	 * Return a list of resource files from a resource directory
	 * @param classLoader
	 * @param path
	 * @return
	 * @throws IOException
	 */
	private List<String> getResourceFiles(ClassLoader classLoader, String path) throws IOException {
		return readStringList(classLoader.getResourceAsStream(path));
	}

	/**
	 * Recurse the files in directory which match extension, and pass them to the consumer.
	 * @param dir  directory to traverse.
	 * @param extension extension to match
	 * @param consumer
	 * @throws IOException
	 */
	public static void walkFiles(File dir, String extension, CheckedConsumer<File> consumer) throws IOException {
		Files.walk(Paths.get(dir.toURI()))
				.filter(Files::isRegularFile)
				.filter(p -> p.getFileName().toString().toLowerCase().endsWith(extension))
				.map(p -> p.toFile())
				.forEach(CheckedConsumer.wrap(consumer));
	}
}
