package com.apptechnik.appinabox.engine.util;

public class FlagUtil {

    /**
     * In an array of flags, are any of the flags other than the indexed flag true.
     * @param flags array of boolean flags.
     * @param index index to ignore
     * @return true if any other flag is true, false otherwise.
     */
    public static boolean anyOtherTrue(boolean[] flags, int index) {
        for (int i = 0; i < flags.length; i++) {
            if ((i != index) && flags[i]) {
                return true;
            }
        }
        return false;
    }

    public static boolean anyTrue(boolean[] flags) {
        for (int i = 0; i < flags.length; i++) {
            if (flags[i]) {
                return true;
            }
        }
        return false;
    }

}
