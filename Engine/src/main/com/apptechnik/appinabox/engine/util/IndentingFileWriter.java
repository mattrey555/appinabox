package com.apptechnik.appinabox.engine.util;

import java.io.File;
import java.io.Writer;
import java.io.FileWriter;
import java.io.IOException;
import java.io.FileNotFoundException;

/**
 * File writer which indents automatically with curly braces and parentheses
 */
public class IndentingFileWriter extends Writer {
    private int mIndent;
    private int mParenCount;
    private int mOpenParenPos;
    private boolean mStartLine;
    private Writer mWriter;

    public IndentingFileWriter(Writer writer) throws IOException {
        this(writer, 0);
    }

    public IndentingFileWriter(Writer writer, int initialIndent) throws IOException {
        mIndent = initialIndent;
        mParenCount = 0;
        mWriter = writer;
        mStartLine = true;
    }

    // return the difference between the number of opening and closing parentheses.
    public int parenCount(String s) {
        int parenCount = 0;
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (ch == '(') {
                parenCount++;
            } else if (ch == ')') {
                parenCount--;
            }
        }
        return parenCount;
    }

    // find the open parenthesis position for the current "parantheses balance" level
    public int openParenPosition(String s, int parenCount) {
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (ch == '(') {
                parenCount--;
            } else if (ch == ')') {
                parenCount++;
            }
            if (parenCount == 0) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void write(char[] s, int start, int len) throws IOException {
    }

    // write a string indented appropriately by the current curly brace and parenthesis level
    @Override
    public void write(String s) throws IOException {

        // shortcut until we do a "proper" split
        if (s.equals("\n")) {
            mWriter.write("\n");
            return;
        }
        String[] lines = s.split("\n");
        for (int iLine = 0; iLine < lines.length; iLine++) {
            boolean inDoubleQuotes = false;
            boolean inQuotes = false;
            int changeIndentAfter = 0;
            String line = lines[iLine].trim();
            for (int i = 0; i < line.length(); i++) {
                char ch = line.charAt(i);
                if (ch == '\\') {
                    i++;
                    continue;
                }
                if (ch == '\"') {
                    inDoubleQuotes = !inDoubleQuotes;
                }
                if (!inDoubleQuotes) {
                    if (ch == '\'') {
                        inQuotes = !inQuotes;
                    }
                }
                if (!inDoubleQuotes && !inQuotes) {
                    if (ch == '{') {
                        changeIndentAfter++;
                    } else if (ch == '}') {
                        mIndent--;
                    }
                }
            }
            for (int i = 0; i < mIndent*4; i++) {
                mWriter.write(" ");
            }
            mParenCount += parenCount(line);
            mOpenParenPos = openParenPosition(line, mParenCount);
            mWriter.write(line);
            mWriter.write("\n");
            mIndent += changeIndentAfter;
        }
    }

    public void writeLines(String s) throws IOException {
        StringBuffer sbLine = new StringBuffer();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            sbLine.append(c);
            if (c == '\n') {
                write(sbLine.toString());
                sbLine = new StringBuffer();
            }
        }
    }


    // do the same, except with a CR at the end
    public void writeln(String s) throws IOException {
        write(s);
        mWriter.write("\n");
    }

    // write output with newlines by splitting lines
    public void writeWithNewlines(String s) throws IOException {
        String[] lines = s.split("\n");
        for (String line : lines) {
            writeln(line.trim());
        }
    }

    public void close() throws IOException {
        mWriter.close();
    }

    public void flush() throws IOException {
        mWriter.flush();
    }

    /**
     * Write a string indented to a file
     * @param s source string
     * @param outputFile output file.
     */
    public static void writeStringToFile(String s, File outputFile) throws IOException, FileNotFoundException {
        IndentingFileWriter ifw = null;
        try {
            ifw = new IndentingFileWriter(new FileWriter(outputFile), 0);
            ifw.writeLines(s);
        } finally {
            ifw.close();
        }
    }
}
