package com.apptechnik.appinabox.engine.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Generic List Utilities
 */
public class ListUtil {
    public static <T> List<T> upTo(List<T> list, T t) {
        return list.subList(0, list.indexOf(t));
    }

    public static <T> List<T> after(List<T> list, T t) {
        return list.subList(list.indexOf(t) + 1, list.size());
    }

    public static <T> List<T> except(List<T> list, T t) {
        int index = list.indexOf(t);
        if (index < 0) {
            return list;
        } else {
            List a = new ArrayList<T>();
            a.addAll(list.subList(0, index));
            a.addAll(list.subList(index + 1, list.size()));
            return a;
        }
    }

    public static <T> List<T> except(List<T> list, T t, Comparator<T> comparator) {
        return list.stream().filter(item -> comparator.compare(item, t) != 0).collect(Collectors.toList());
    }

    /**
     * Return the rest of the list, or empty if it's emoty
     * @param list list
     * @param <T>
     * @return rest of list
     */
    public static <T> List<T> rest(List<T> list) {
        if (list.isEmpty()) {
            return list;
        } else {
            return list.subList(1, list.size());
        }
    }

    public static <T> List<T> exceptLast(List<T> list) {
        if (list.isEmpty()) {
            return list;
        } else {
            return list.subList(0, list.size() - 1);
        }
    }

    public static <T> T last(List<T> list) {
        if (list.isEmpty()) {
            return null;
        } else {
            return list.get(list.size() - 1);
        }
    }

    public static <T> List<T> notNull(List<T> list) {
        if (list == null) {
            return new ArrayList<T>();
        }
        return list;
    }
}
