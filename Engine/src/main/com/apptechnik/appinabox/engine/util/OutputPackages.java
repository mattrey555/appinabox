package com.apptechnik.appinabox.engine.util;

import com.apptechnik.appinabox.engine.Constants;

/**
 * Relative output packages (the Single Source Of Truth)
 */
public class OutputPackages {

    public static String android(String packageName) {
        return packageName + "." + Constants.Packages.ANDROID;
    }

    public static String androidFragments(String packageName) {
        return android(packageName) +  "." + Constants.Packages.FRAGMENTS;
    }

    public static String androidAdapters(String packageName) {
        return android(packageName) +  "." + Constants.Packages.ADAPTERS;
    }

    public static String androidActivities(String packageName) {
        return android(packageName) +  "." + Constants.Packages.ACTIVITIES;
    }

    public static String androidRepositories(String packageName) {
        return android(packageName) +  "." + Constants.Packages.REPOSITORIES;
    }

    public static String androidViewModels(String packageName) {
        return android(packageName) +  "." + Constants.Packages.VIEW_MODELS;
    }

    public static String androidNetwork(String packageName) {
        return android(packageName) +  "." + Constants.Packages.NETWORK;
    }

    public static String androidDao(String packageName) {
        return android(packageName) +  "." + Constants.Packages.DAO;
    }

    public static String getQuerySQLPackageName(String packageName) {
        return packageName + "." + Constants.Packages.QUERY + "." + Constants.Packages.SQL;
    }

    public static String getUpdatePackageName(String packageName) {
        return packageName + "." + Constants.Packages.UPDATE;
    }

    public static String getQueryPackageName(String packageName) {
        return packageName + "." + Constants.Packages.QUERY;
    }

    public static String getUpdateSQLPackageName(String packageName) {
        return packageName + "." + Constants.Packages.UPDATE + "." + Constants.Packages.SQL;
    }

}
