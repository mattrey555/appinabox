package com.apptechnik.appinabox.engine.util;

import com.apptechnik.appinabox.engine.Constants;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.stream.Stream;

public class OutputPaths {
    private static String[] mipmapDirs = new String[] { Constants.AndroidDirs.MIPMAP,
                                                        Constants.AndroidDirs.MIPMAP_HDPI,
                                                        Constants.AndroidDirs.MIPMAP_MDPI,
                                                        Constants.AndroidDirs.MIPMAP_XHDPI,
                                                        Constants.AndroidDirs.MIPMAP_HXXDPI,
                                                        Constants.AndroidDirs.MIPMAP_XXXHDPI };

    public static Stream<File> mipmaps(File outputDir) {
        return Arrays.stream(mipmapDirs).map(CheckedFunction.wrap(s -> new File(androidResDir(outputDir), s)));
    }

    public static File androidDir(File outputDir) throws Exception {
        return FileUtil.checkDirectory("Android Application Directory",
                                        new File(outputDir, Constants.Directories.ANDROID_APP));
    }

    /**
     * OutputDir is "<project>/android"
     * @param outputDir
     * @return
     * @throws Exception
     */
    public static File androidMainDir(File outputDir) throws Exception {
        return FileUtil.checkDirectory("Android Main Directory",
                                       new File(outputDir, Constants.Directories.ANDROID_MAIN));
    }

    public static File androidKotlinDir(File outputDir) throws Exception {
        return FileUtil.checkDirectory("Android Kotlin Directory",
                                        new File(androidMainDir(outputDir), Constants.Directories.ANDROID_KOTLIN));
    }

    public static File androidKotlinDir(File outputDir, String packageName) throws Exception {
        return FileUtil.checkDirectory("Android Kotlin Directory " + packageName,
                                       StringUtil.directoryFromPackage(androidKotlinDir(outputDir), packageName));
    }

    public static File androidResDir(File outputDir) throws Exception {
        return FileUtil.checkDirectory("Android Application Resource Directory",
                                        new File(androidMainDir(outputDir), Constants.AndroidDirs.RES));
    }

    public static File androidNavigationDir(File outputDir) throws Exception {
        return FileUtil.checkDirectory("Android Application Navigation Resource Directory",
                                        new File(androidResDir(outputDir), Constants.AndroidDirs.NAVIGATION));
    }

    public static File androidValuesDir(File outputDir) throws Exception {
        return FileUtil.checkDirectory("Android Application Values Resource Directory",
                                        new File(androidResDir(outputDir), Constants.AndroidDirs.VALUES));
    }

    public static File androidValuesNightDir(File outputDir) throws Exception {
        return FileUtil.checkDirectory("Android Application Night Values Resource Directory",
                                        new File(androidResDir(outputDir), Constants.AndroidDirs.VALUES));
    }

    public static File androidValues21Dir(File outputDir) throws Exception {
        return FileUtil.checkDirectory("Android Application Version 21 Values ResourceDirectory",
                                        new File(androidResDir(outputDir), Constants.AndroidDirs.VALUES_V21));
    }

    public static File androidValuesNight21Dir(File outputDir) throws Exception {
        return FileUtil.checkDirectory("Android Application Version Night 21 Values ResourceDirectory",
                                        new File(androidResDir(outputDir), Constants.AndroidDirs.VALUES_NIGHT_V21));
    }

    public static File androidLayoutDir(File outputDir) throws Exception {
        return FileUtil.checkDirectory("Android Application Layout Resource Directory",
                                        new File(androidResDir(outputDir), Constants.AndroidDirs.LAYOUT));
    }

    /**
     * app/src/main/res/values/strings.xml
     * @param outputDir
     * @return
     */
    public static File androidStringsXml(File outputDir) throws Exception {
        return FileUtil.checkCreateFile("Android Application strings.xml Resource File",
                                        new File(androidValuesDir(outputDir), Constants.Files.ANDROID_STRINGS_XML));
    }
    public static File androidEnvironmentXml(File outputDir) throws Exception {
        return FileUtil.checkCreateFile("Android Application strings.xml Resource File",
                new File(androidValuesDir(outputDir), Constants.Files.ANDROID_ENVIRONMENT_XML));
    }

    public static File androidStylesXml(File outputDir) throws Exception {
        return FileUtil.checkCreateFile("Android Application styles.xml Resource File",
                new File(androidValuesDir(outputDir), Constants.Files.ANDROID_STYLES_XML));
    }

    public static File androidNightStylesXml(File outputDir) throws Exception {
        return FileUtil.checkCreateFile("Android Application night styles.xml Resource File",
                new File(androidValuesDir(outputDir), Constants.Files.ANDROID_STYLES_XML));
    }

    public static File androidStylesV21Xml(File outputDir) throws Exception {
        return FileUtil.checkCreateFile("Android Application V21 styles.xml Resource File",
                new File(androidValues21Dir(outputDir), Constants.Files.ANDROID_STYLES_XML));
    }

    public static File androidNightStylesV21Xml(File outputDir) throws Exception {
        return FileUtil.checkCreateFile("Android Application night V21 styles.xml Resource File",
                new File(androidValuesNight21Dir(outputDir), Constants.Files.ANDROID_STYLES_XML));
    }

    public static File androidManifest(File outputDir) throws Exception {
        return FileUtil.checkCreateFile("Android Manifest",
                                        new File(androidMainDir(outputDir), Constants.Files.ANDROID_MANIFEST));
    }
    /**
     *  app/main/res/navigation/nav_graph.xml
     */
    public static File androidNavGraphXml(File outputDir) throws Exception {
        return FileUtil.checkCreateFile("Android  nav_graph.xml file",
                                       new File(androidNavigationDir(outputDir), Constants.Files.ANDROID_NAV_GRAPH_XML));
    }
    public static File androidEndpointsXml(File outputDir) throws Exception {
        return FileUtil.checkCreateFile("Android Application endpoints.xml file",
                new File(androidValuesDir(outputDir), Constants.Files.ANDROID_ENDPOINT_XML));
    }

    public static File androidMipmapAnyDPI26(File outputDir) throws Exception {
        return FileUtil.checkDirectory("Android Application MipMap AnyDPI v26 Resource Directory",
                                        new File(androidResDir(outputDir), Constants.AndroidDirs.MIPMAP_ANYDPI_V26));
    }

    public static File androidDrawableDir(File outputDir) throws Exception {
        return FileUtil.checkDirectory("Android Application Drawable Resource Directory",
                                        new File(androidResDir(outputDir), Constants.AndroidDirs.DRAWABLE));
    }

    public static File androidDrawableV24Dir(File outputDir) throws Exception {
        return FileUtil.checkDirectory("Android Application Drawable V24 Directory",
                                        new File(androidResDir(outputDir), Constants.AndroidDirs.DRAWABLE_V24));
    }

    public static String androidResourceTemplate(String path) {
        return Constants.Templates.ANDROID_RESOURCE_TEMPLATES + "/" + path;
    }

    public static String androidLayoutResourceTemplate(String path) {
        return Constants.Templates.ANDROID_LAYOUT_TEMPLATES + "/" + path;
    }

    public static File androidLibraryDir(File outputDir) throws Exception {
        return FileUtil.checkDirectory("Android Library Directory",
                                        new File(outputDir, Constants.Directories.ANDROID_LIBRARY));
    }

    public static File javaDir(File outputDir, String packageName) throws Exception {
        return FileUtil.checkDirectory("Java Output Directoru",
                                        new File(outputDir, StringUtil.packageToFilePath(packageName)));
    }
}
