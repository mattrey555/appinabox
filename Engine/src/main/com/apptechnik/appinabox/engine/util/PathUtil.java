package com.apptechnik.appinabox.engine.util;

/**
 * Generic utilities for URL path extraction..
 */
public class PathUtil {

    /**
     * Given a path s, a delimiter delim, and target, find the index of target.
     * i.e. "/this/is/a/path", "/", "is" -> 1
     * @param s
     * @param delim
     * @param target
     * @return
     */
    public static int pathElementIndex(String s, char delim, String target) {
        int delimCount = 0;
        for (int index = 0; index < s.length(); index++) {
            char ch = s.charAt(index);
            if (ch == delim) {
                delimCount++;
            }
            if (s.startsWith(target, index)) {
                return s.charAt(0) == delim ? delimCount - 1 : delimCount;
            }
        }
        return -1;
    }

}
