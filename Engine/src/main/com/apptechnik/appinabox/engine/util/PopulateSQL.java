package com.apptechnik.appinabox.engine.util;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import com.apptechnik.appinabox.engine.util.FileUtil;


public class PopulateSQL {
    private static final String DB_URL = "DB_URL";
    private static final String DB_TEST_DATABASE = "DB_TEST_DATABASE";
    private static final String DB_USER = "DB_USER";
    private static final String DB_PASSWORD = "DB_PASSWORD";

    public static void main(String[] args) throws Exception {
        String createTablesFileName = args[0];
        String propertiesFileName = args[1];
        File createTablesFile = FileUtil.checkFile("create SQL tables", createTablesFileName);
        File propertiesFile = FileUtil.checkFile("properties file", propertiesFileName);
        String createTablesText = FileUtil.readFileToString(createTablesFile);

        // normally, I'd use Connection.setCatalog(database), but it doesn't work in PostGres
        // so there's one db connection for database create/drop, and another to create the
        // tables and insert the values.
        Properties properties = new Properties();
        properties.load(new FileInputStream(propertiesFile));
        String dbUrl = properties.getProperty(DB_URL);
        String dbDatabase = properties.getProperty(DB_TEST_DATABASE);
        String dbUser = properties.getProperty(DB_USER);
        String dbPassword = properties.getProperty(DB_PASSWORD);
        Connection dbConnection = DriverManager.getConnection(dbUrl, dbUser, dbPassword);
        try {
            Statement dropDatabaseStatement = dbConnection.createStatement();
            dropDatabaseStatement.execute("DROP DATABASE " + dbDatabase);
        } catch (SQLException sqlex) {
            System.out.println("it's OK if this happens, just cleaning up before the test");
        }
        Statement createDatabaseStatement = dbConnection.createStatement();
        createDatabaseStatement.execute("CREATE DATABASE " + dbDatabase);

        Connection dbConnectionTest = DriverManager.getConnection(dbUrl + dbDatabase, dbUser, dbPassword);
        Statement createTablesStatements = dbConnectionTest.createStatement();
        createTablesStatements.execute(createTablesText);
        com.apptechnik.appinabox.engine.parser.PopulateSQL populateSQL = new com.apptechnik.appinabox.engine.parser.PopulateSQL(dbConnectionTest, createTablesFile);
        populateSQL.execute(100);
        dbConnectionTest.close();
    }
}
