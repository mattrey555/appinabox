package com.apptechnik.appinabox.engine.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PreprocessorUtil {

    public static List<String> textInStandardComments(File file) throws IOException {
        String text = FileUtil.readFileToString(file);
        return textInComments('\\', "/*", "*/", new String[]{"\"", "'"}, "//", text);
    }

    public static List<String> textInStandardComments(String text) {
        return textInComments('\\', "/*", "*/", new String[]{"\"", "'"}, "//", text);
    }

    /**
     * Retrieve commented text from a source string.
     * Example: Comments may be prefixed by // on the first character of a line, or surrounded by \/* *\/
     *
     * @param escapeChar              let's face it, it's always '\'
     * @param prefixCommentString     String on first line or newline which prefixes a comment
     * @param surroundingCommentStart start of a comment, regardless of previous character.
     * @param surroundingCommentEnd   end of a comment, ditto.
     * @param quoteStrings            strings which start and end a quoted string.
     * @param text                    text to scan.
     * @return list of strings prefixed by the above rules.
     */

    public static List<String> textInComments(char escapeChar,
                                              String surroundingCommentStart,
                                              String surroundingCommentEnd,
                                              String[] quoteStrings,
                                              String prefixCommentString,
                                              String text) {
        List<String> result = new ArrayList<String>();
        boolean[] inSourceQuote = new boolean[quoteStrings.length];
        boolean inSurroundingComment = false;
        boolean isEscaped = false;
        boolean inPrefixComment = false;
        StringBuilder sb = new StringBuilder();
        for (int ich = 0; ich < text.length(); ich++) {
            char ch = text.charAt(ich);

            // if the previous character was an escape, then ignore this character for processing.
            if ((ch == escapeChar) && !isEscaped) {
                isEscaped = true;
                continue;
            }
            if (isEscaped) {
                isEscaped = false;
            } else {
                // newlines are only recognized outside of surrounding comments. If we hit a newline,
                // we are no longer in a prefix comment
                if ((ch == '\n') && !inSurroundingComment) {
                    inPrefixComment = false;
                }

                // quotes are only recognized outside of comments, and if not quoted by another quote string.
                int quoteIndex = StringUtil.startsWith(text, quoteStrings, ich);
                if ((quoteIndex != -1) && !FlagUtil.anyOtherTrue(inSourceQuote, quoteIndex)) {
                    if (!(inSurroundingComment || inPrefixComment)) {
                        inSourceQuote[quoteIndex] = !inSourceQuote[quoteIndex];
                    }
                }

                // If the text is not quoted (ex String s = "/*";), and we are not in a line start comment,
                // if the text starts with a surroundingCommentStart, we are now in a surrounding comment.
                // Skip past the comment start string. Negate the flag if we're in a surrounding comment
                // and not in q quote.
                if (!FlagUtil.anyTrue(inSourceQuote) && !inPrefixComment && text.startsWith(surroundingCommentStart, ich)) {
                    inSurroundingComment = true;
                    ich += surroundingCommentStart.length();
                    ch = text.charAt(ich);
                }
                if (!FlagUtil.anyTrue(inSourceQuote) && !inPrefixComment && text.startsWith(surroundingCommentEnd, ich)) {
                    inSurroundingComment = false;
                    ich += surroundingCommentStart.length();
                    ch = text.charAt(ich);
                }

                // if we are not quoted, and the text starts with the prefix comment string,
                // we are in a prefix comment until the next newline.
                // Skip past the comment start string.
                if (!inSurroundingComment && !FlagUtil.anyTrue(inSourceQuote) && text.startsWith(prefixCommentString, ich)) {
                    inPrefixComment = true;
                    ich += prefixCommentString.length();
                    ch = text.charAt(ich);
                }
            }

            // if we are not in a source quote and in a surrounding comment or a linestart comment, append the character.
            // if not, and we have accumulated characters in the string builder, append it to the list and clear it.
            if (!FlagUtil.anyTrue(inSourceQuote) && (inSurroundingComment || inPrefixComment)) {
                sb.append(ch);
            } else if (sb.length() != 0) {
                result.add(sb.toString());
                sb.setLength(0);
            }
        }

        // if we accumulated any commented characters when we hit the end of the file, append them to the list.
        if (sb.length() != 0) {
            result.add(sb.toString());
        }
        return result;
    }

    public static String stripStandardComments(String text) {
        return stripComments('\\', "/*", "*/", new String[]{"\"", "'"}, "//", text);
    }

    /**
     * Given a source file (probably SQL), return the text without comments.
     * @param escapeChar              let's face it, it's always '\'
     * @param prefixCommentString     String on first line or newline which prefixes a comment
     * @param surroundingCommentStart start of a comment, regardless of previous character.
     * @param surroundingCommentEnd   end of a comment, ditto.
     * @param quoteStrings            strings which start and end a quoted string.
     * @param text                    text to scan.
     * @return text with comments stripped.
     */
    public static String stripComments(char escapeChar,
                                       String surroundingCommentStart,
                                       String surroundingCommentEnd,
                                       String[] quoteStrings,
                                       String prefixCommentString,
                                       String text) {
        boolean[] inSourceQuote = new boolean[quoteStrings.length];
        boolean inSurroundingComment = false;
        boolean isEscaped = false;
        boolean inPrefixComment = false;
        StringBuilder sb = new StringBuilder();
        for (int ich = 0; ich < text.length(); ich++) {
            char ch = text.charAt(ich);

            // if the previous character was an escape, then ignore this character for processing.
            if ((ch == escapeChar) && !isEscaped) {
                isEscaped = true;
                continue;
            }
            if (isEscaped) {
                isEscaped = false;
            } else {
                // newlines are only recognized outside of surrounding comments. If we hit a newline,
                // we are no longer in a prefix comment
                if ((ch == '\n') && !inSurroundingComment) {
                    inPrefixComment = false;
                }

                // quotes are only recognized outside of comments, and if not quoted by another quote string.
                int quoteIndex = StringUtil.startsWith(text, quoteStrings, ich);
                if ((quoteIndex != -1) && !FlagUtil.anyOtherTrue(inSourceQuote, quoteIndex)) {
                    if (!(inSurroundingComment || inPrefixComment)) {
                        inSourceQuote[quoteIndex] = !inSourceQuote[quoteIndex];
                    }
                }

                // If the text is not quoted (ex String s = "/*";), and we are not in a line start comment,
                // if the text starts with a surroundingCommentStart, we are now in a surrounding comment.
                // Skip past the comment start string. Negate the flag if we're in a surrounding comment
                // and not in q quote.
                if (!FlagUtil.anyTrue(inSourceQuote) && !inPrefixComment && text.startsWith(surroundingCommentStart, ich)) {
                    inSurroundingComment = true;
                    ich += surroundingCommentStart.length();
                    ch = text.charAt(ich);
                }
                if (!FlagUtil.anyTrue(inSourceQuote) && !inPrefixComment && text.startsWith(surroundingCommentEnd, ich)) {
                    inSurroundingComment = false;
                    ich += surroundingCommentStart.length();
                    ch = text.charAt(ich);
                }

                // if we are not quoted, and the text starts with the prefix comment string,
                // we are in a prefix comment until the next newline.
                // Skip past the comment start string.
                if (!inSurroundingComment && !FlagUtil.anyTrue(inSourceQuote) && text.startsWith(prefixCommentString, ich)) {
                    inPrefixComment = true;
                    ich += prefixCommentString.length();
                    ch = text.charAt(ich);
                }
            }

            // if we are not in a source quote and in a surrounding comment or a linestart comment, append the character.
            // if not, and we have accumulated characters in the string builder, append it to the list and clear it.
            if (!(inSurroundingComment || inPrefixComment)) {
                sb.append(ch);
            }
        }
        return sb.toString();
    }
}
