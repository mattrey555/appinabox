package com.apptechnik.appinabox.engine.util;

import java.io.*;
import java.util.Properties;

/**
 * Utilities to work with properties files.
 */
public class PropertiesUtil {
    public static void merge(Properties from, Properties into) {
        for (String key : from.stringPropertyNames()) {
            into.put(key, from.getProperty(key));
        }
    }

    public static void mergeResourceToFile(File propertiesFile, String resource) throws IOException {
        InputStream isProperties = PropertiesUtil.class.getClassLoader().getResourceAsStream(resource);
        Properties newProperties = new Properties();
        newProperties.load(isProperties);
        if (propertiesFile.exists()) {
            Properties existingProperties = new Properties();
            existingProperties.load(new FileInputStream(propertiesFile));
            merge(newProperties, existingProperties);
            newProperties = existingProperties;
        }
        newProperties.store(new FileOutputStream(propertiesFile), null);
    }
}
