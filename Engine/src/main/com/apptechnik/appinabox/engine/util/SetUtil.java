package com.apptechnik.appinabox.engine.util;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Generic set utilities.
 */
public class SetUtil {
    public static<T> Set<T> intersect(Set<T> setA, Set<T> setB) {
        Set<T> intersection = new HashSet<T>(setA);
        intersection.retainAll(setB);
        return intersection;
    }

    public static<T> Set<T> except(Collection<T> set, T a) {
        HashSet<T> newSet = new HashSet<T>(set);
        newSet.remove(a);
        return newSet;
    }
}
