package com.apptechnik.appinabox.engine.util;

import com.apptechnik.appinabox.engine.exceptions.UnsupportedExpressionException;
import com.apptechnik.appinabox.engine.parser.query.SelectTableUtils;
import com.apptechnik.appinabox.engine.parser.TableInfo;
import com.apptechnik.appinabox.engine.parser.TableInfoMap;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.delete.Delete;
import net.sf.jsqlparser.statement.insert.Insert;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.select.SelectExpressionItem;
import net.sf.jsqlparser.statement.update.Update;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * General utilities for Insert and Update statements, because jsqlparser doesn't give them
 * a common interface.
 */
public class StatementUtil {

    /**
     * For operations (INSERT, UPDATE, DELETE) which operate on a single table, get the
     * type of the statement, and return the appropriate table expression
     * @param statement
     * @return
     * @throws UnsupportedExpressionException
     */
    public static Table getTable(Statement statement) throws UnsupportedExpressionException {
        if (statement instanceof Insert) {
            Insert insert = (Insert) statement;
            return insert.getTable();
        } else if (statement instanceof Update) {
            Update update = (Update) statement;
            return update.getTable();
        } else if (statement instanceof Delete) {
            Delete delete = (Delete) statement;
            return delete.getTable();
        }
        throw new UnsupportedExpressionException("trying to get table from " + statement);
    }

    /**
     * Return the list of expressions returned by this statement, or NULL if there is none.
     * @param statement
     * @return
     * @throws UnsupportedExpressionException
     */
    public static List<SelectExpressionItem> getReturningExpressionList(Statement statement) throws UnsupportedExpressionException {
        if (statement instanceof Insert) {
            Insert insert = (Insert) statement;
            return insert.getReturningExpressionList();
        } else  if (statement instanceof Update) {
            Update update = (Update) statement;
            return update.getReturningExpressionList();
        } else if (statement instanceof Delete) {
            return null;
        } else if (statement instanceof Select) {
            return null;
        }
        throw new UnsupportedExpressionException("trying to get table from " + statement);
    }

    /**
     * Get the columns from an INSERT or UPDATE statement. Throw an exception otherwise.
     * @param statement
     * @return
     * @throws UnsupportedExpressionException
     */
    public static List<Column> getColumns(Statement statement)
        throws UnsupportedExpressionException {
        if (statement instanceof Insert) {
            Insert insert = (Insert) statement;
            return insert.getColumns();

        } else  if (statement instanceof Update) {
            Update update = (Update) statement;
            return update.getColumns();
        }
        throw new UnsupportedExpressionException("trying to get table from " + statement);
    }

    /**
     * Given a list of SQL statements, filter out the SELECT statements which do not have an INTO clause.
     * @param statementList
     * @return
     */
    public static List<Select> filterSelects(List<Statement> statementList) {
        return (List<Select>) statementList.stream()
                .filter(statement -> statement instanceof Select)
                .map(statement -> (Select) statement)
                .filter(select -> ((((Select) select).getSelectBody() instanceof PlainSelect) &&
                        ((PlainSelect) ((Select) select).getSelectBody()).getIntoTables() == null))
                .collect(Collectors.toList());
    }

    /**
     * Filter INSERT/UPDATE/DELETE
     * @param statementList
     * @return
     */
    public static List<Statement> filterUpdateDeleteInsert(Collection<Statement> statementList) {
        return statementList.stream()
                .filter(statement -> isUpdateDeleteInsert(statement))
                .collect(Collectors.toList());
    }

    private static boolean isUpdateDeleteInsert(Statement statement) {
        return (statement instanceof Insert) || (statement instanceof Update) || (statement instanceof Delete);
    }

    public static List<Statement> filterUpdateInsert(List<Statement> statementList) {
        return statementList.stream()
                .filter(statement -> isUpdateInsert(statement))
                .collect(Collectors.toList());
    }

    private static boolean isUpdateInsert(Statement statement) {
        return (statement instanceof Insert) || (statement instanceof Update);
    }

    /**
     * Filter the table map by the select statement tables referenced, and remap their names
     * to any aliases used.
     * @param statement
     * @param createTableInfoMap
     * @return
     * @throws Exception
     */
    public static TableInfoMap getTables(Statement statement, TableInfoMap createTableInfoMap)
            throws Exception {
        if (statement instanceof Select) {
            return SelectTableUtils.selectTables((Select) statement, createTableInfoMap);
        } else {
            TableInfoMap statementTableInfoMap = new TableInfoMap();

            // for just once, could someone make interface implementations at the function level
            // where two classes with functions with the same signature that those calls
            // are considered equivalent?  I should be able to do
            // (statement.getTable()) as an extension across all 3 classes.
            // I've always hated doing switch statements on types.
            String tableName;
            if (statement instanceof Insert) {
                tableName = ((Insert) statement).getTable().getName();
            } else if (statement instanceof Update) {
                tableName = ((Update) statement).getTable().getName();
            } else if (statement instanceof Delete) {
                tableName = ((Delete) statement).getTable().getName();
            } else {
                throw new UnsupportedExpressionException("while getting tables, stsatement " + statement +
                                                         " is unsupported");
            }
            TableInfo tableInfo = createTableInfoMap.getTableInfo(tableName);
            statementTableInfoMap.put(tableName, tableInfo, false);
            return statementTableInfoMap;
        }
    }
}
