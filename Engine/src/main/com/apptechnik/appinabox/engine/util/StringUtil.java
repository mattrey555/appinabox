package com.apptechnik.appinabox.engine.util;

import java.io.File;;
import java.util.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * General purpose string utilities
 */
public class StringUtil {
	private StringUtil() {
	}

	/**
	 * Remove the extension from a string, hopefully a file
	 * @param s ex foo.bar or foo.
	 * @return foo
	 */
	public static String removeExtension(String s) {
		int ichDot = s.lastIndexOf('.');
		return (ichDot == -1)  ? s : s.substring(0, ichDot);
	}

	public static String replaceExtension(String s, String ext) {
		return removeExtension(s) + "." + ext;
	}

	/**
	 * Concatenate a list of strings with delimiters.
	 * 	 * Given ["a" "list" "of" "strings"], return "a,list,of,strings"
	 * @param coll colletion
	 * @param delim delimiter
	 * @param <T>
	 * @return
	 */
	public static <T> String concat(Collection<T> coll, String delim) {
		StringBuilder sb = new StringBuilder();
		if (coll != null) {
			boolean first = true;
			for (T o : coll) {
				if (!first) {
					sb.append(delim);
				}
				if (o != null) {
					sb.append(o.toString());
				} else {
					sb.append("(null)");
				}
				first = false;
			}
		}
		return sb.toString();
	}

	/**
	 * Concatenate a list of strings with delimiters.
	 * 	 * Given ["a" "list" "of" "strings"], return "a;list;of;strings;"
	 * @param coll colletion
	 * @param delim delimiter
	 * @param <T>
	 * @return
	 */
	public static <T> String delim(Collection<T> coll, String delim) {
		StringBuilder sb = new StringBuilder();
		if (coll != null) {
			for (T o : coll) {
				if (o != null) {
					sb.append(o.toString());
				} else {
					sb.append("(null)");
				}
				sb.append(delim);
			}
		}
		return sb.toString();
	}

	/**
	 * if string is parenthesized, remove the parens and whitespace,
	 * otherwise, just return the original string
	 * TODO: THIS IS WEAK.
	 */
	public static String stripParentheses(String s) {
		String t = s.trim();
		if (t.charAt(0) == '(') {
			return t.substring(1, t.length() - 1);
		} else {
			return t;
		}
	}

	/**
	 * Strip quotes or double quotes from a string.
	 * @param s
	 * @return
	 */
	public static String stripQuotes(String s) {
		String t = s.trim();
		if ((t.charAt(0) == '"') || (t.charAt(0) == '\'')) {
			return t.substring(1, t.length() - 1);
		} else {
			return t;
		}
	}

	/**
	 * is str in strArray?
	 */
    public static boolean in(String[] strArray, String str) {
        if (strArray == null) {
            return false;
        }
        if (str == null) {
            return false;
        }
        for (int i = 0; i < strArray.length; i++) {
            if (strArray[i].equals(str)) {
                return true;
            }
        }
        return false;
    }

	/**
	 * return a list of strings, split by delim, then trim for whitespace.
	 * @param a string to split
	 * @param delim delimiter to split on (treated as regex)
	 * @return
	 */
	public static String[] splitAndTrim(String a, String delim) {
		String[] v = a.split(delim);
		for (int i = 0; i < v.length; i++) {
			v[i] = v[i].trim();
		}
		return v;
	}

	/**
	 * Same but return a list of strings.
	 * @param a string to split
	 * @param delim delimiter to split on (treated as regex, so remember "." should be "\\."
	 * @return list of strings or null if a is null.
	 */
	public static List<String> splitAndTrimList(String a, String delim) {
		if (a == null) {
			return new ArrayList<String>();
		}
		String[] v = a.split(delim);
		List<String> list = new ArrayList<String>(v.length);
		for (int i = 0; i < v.length; i++) {
			list.add(v[i].trim());
		}
		return list;
	}

	/**
	 * TODO: move to OutputPaths.java
	 * Create a directory from a parent dir and a package.name.in.dot.format
	 * @param dir parent directory
	 * @param packageName package.name.in.dot.format
	 * @return parent directory/package/name/in/dot/format
	 */
	public static File directoryFromPackage(File dir, String packageName) {
    	return new File(dir, packageToFilePath(packageName));
	}

	/**
	 * convert a java.package.name to a file/path/name
	 * @param packageName
	 * @return
	 */
	public static String packageToFilePath(String packageName) {
		return packageName.replace('.', '/');
	}

	/**
	 * convert a java.package.name to a java_package_name
	 * @param packageName
	 * @return
	 */
	public static String packageToId(String packageName) { return packageName.replace('.', '_'); }
	/**
	 * "city" -> "City", "CITY" -> "City"
	 * @param s
	 * @return
	 */
	public static String capitalizeFirstChar(String s) {
		return Character.toUpperCase(s.charAt(0)) + s.substring(1).toLowerCase();
	}

	/**
	 * Given a string s, return a string with s repeated n times.
	 * @param s
	 * @param n
	 * @return
	 */
	public static String repeat(String s, int n) {
		StringBuilder sb = new StringBuilder(s.length() * n);
		for (int i = 0; i < n; i++) {
			sb.append(s);
		}
		return sb.toString();
	}

	/**
	 * Given a delimiter and an array of strings, return a string with the elements of the array
	 * concatenated, split by the delimiter.
	 * @param delim delimiter
	 * @param array array of strings.
	 * @return concatenated string
	 */
	public static String concatArray(String delim, String[] array) {
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		for (String s : array) {
			if (!first) {
				sb.append(delim);
			}
			first = false;
			sb.append(s);
		}
		return sb.toString();
	}

	/**
	 * If s at ich starts with any string in cands, return the index of the cand it matches.
	 * @param s target string
	 * @param cands candidate match array
	 * @param ich target string offset
	 * @return index or -1 if not found.
	 */
	public static int startsWith(String s, String[] cands, int ich) {
		for (int i = 0; i < cands.length; i++) {
			if (s.startsWith(cands[i], ich)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Filter text by lines that start with {@code <tag:></tag:>}, ignoring whitespace. return the
	 * @param text lines to scan
	 * @param tag tag to search for
	 * @return list of matching lines
	 */
	public static List<String> filterTag(List<String> text, String tag) {
		return text.stream()
				.filter(line -> line.trim().startsWith(tag + ":"))
				.map(line -> line.substring(line.indexOf(':') + 1).trim())
				.collect(Collectors.toList());
	}

	/**
	 * find text test after first occurrence of tag
	 * @param text lines to scan
	 * @param tag tag to search for
	 * @return first matching tag.
	 */
	public static String findTag(List<String> text, String tag) {
		Optional<String> optStr = text.stream()
				.filter(line -> line.trim().startsWith(tag + ":")).findFirst();
		if (optStr.isPresent()) {
			return optStr.get().substring(optStr.get().indexOf(':') + 1).trim();
		} else {
			return null;
		}
	}
	/**
	 * find text test after first occurrence of tag
	 * @param text lines to scan
	 * @param tag tag to search for
	 * @return first matching tag.
	 */
	public static List<String> findTags(List<String> text, String tag) {
		return text.stream().filter(line -> line.trim().startsWith(tag + ":")).collect(Collectors.toList());
	}

	/**
	 * Strip the empty lines from a file
	 * @param s
	 * @return
	 */
	public static String stripEmptyLines(String s) {
		return s.replaceAll("(?m)^[ \t]*\r?\n", "");
	}

	/**
	 * Split a string s, delimited by split,  quoted by quote, and escaped with escapePrefix
	 * @param s
	 * @param split
	 * @param quote
	 * @param escapePrefix
	 * @return
	 */
	public static String[] splitQuotedEscaped(String s, String split, String quote, String escapePrefix) {
		List<String> splitStrings = new ArrayList<>();
		boolean inQuotes = false;
		boolean isEscaped = false;
		StringBuilder sbChunk = new StringBuilder();
		for (int ich = 0; ich < s.length(); ich++) {
			char ch = s.charAt(ich);
			if (s.startsWith(escapePrefix, ich)) {
				sbChunk.append(escapePrefix);
				ich += escapePrefix.length();
				ch = s.charAt(ich);
				sbChunk.append(ch);
				continue;
			}
			if (s.startsWith(quote, ich)) {
				inQuotes = !inQuotes;
			}
			boolean isSplit = !inQuotes && !isEscaped && s.startsWith(split, ich);
			if (isSplit) {
				splitStrings.add(sbChunk.toString());
				sbChunk.setLength(0);
			} else {
				sbChunk.append(ch);
			}
		}
		if (sbChunk.length() > 0) {
			splitStrings.add(sbChunk.toString());
		}
		return splitStrings.toArray(new String[splitStrings.size()]);
	}

	/**
	 * return the index first space or tab
	 * @param s string
	 * @return index or -1
	 */
	public static int firstWhiteSpace(String s) {
		return firstWhiteSpace(s, 0);
	}

	public static int firstWhiteSpace(String s, int startIndex) {
		for (int index = startIndex; index < s.length(); index++) {
			if (Character.isWhitespace(s.charAt(index))) {
				return index;
			}
		}
		return -1;
	}

	/**
	 * To create a unit variable, add a 0, or if it is suffixed with a number, increment it:
	 * {@code
	 * <str> -> <str>1
	 * <str><int> = <str><int + 1>
	 * }
	 * @param s
	 * @return
	 */
	public static String incrementVarSuffix(String s) {
		int ich;
		for (ich = s.length() - 1; ich >= 0; ich--) {
			if (!Character.isDigit(s.charAt(ich))) {
				break;
			}
		}
		if (ich == s.length() - 1) {
			return s + "1";
		} else {
			int val = Integer.valueOf(s.substring(ich + 1, s.length()));
			return s.substring(0, ich + 1) + (val + 1);
		}
	}


	/**
	 * Given the a string and an array, return the index of the string in the array
	 * @param a
	 * @param array
	 * @return index or -1 if not found.
	 */
	public static int indexOf(String a, String[] array) {
		for (int i = 0; i < array.length; i++) {
			if (array[i].equals(a)) {
				return i;
			}
		}
		return -1;
	}

	public static int indexOfIgnoreCase(String a, String[] array) {
		for (int i = 0; i < array.length; i++) {
			if (array[i].equalsIgnoreCase(a)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Return a list of groups that match regex
	 * @param s target string
	 * @param regex regex to match
	 * @return list of matching groups.
	 */
	public static List<String> matchingRegexes(String s, String regex) {
		Pattern pattern = Pattern.compile(regex);
		List<String> list = new ArrayList<String>();
		Matcher matcher = pattern.matcher(s);
		while (matcher.find()) {
			list.add(matcher.group());
		}
		return list;
	}

	/**
	 * Given a list of strings, return the list of strings which do not completely match
	 * the supplied regular expression, ex:
	 * fn(["a", "B", "C", "123"], "[A-Za-z]") -> ["a", "B", "C"]
	 * @param list
	 * @param regex
	 * @return
	 */
	public static List<String> nonMatchingRegexes(List<String> list, String regex) {
		Pattern pattern = Pattern.compile(regex);
		List<String> result = new ArrayList<String>();
		for (String s : list) {
			Matcher matcher = pattern.matcher(s);
			if (!matcher.find() || (matcher.start() != 0) || (matcher.end() != s.length())) {
				result.add(s);
			}
		}
		return result;
	}

	/**
	 * change ident to ${ident}
	 * @param s
	 * @return
	 */
	public static String decorateKey(String s) {
		return "${" + s + "}";
	}

	/**
	 * change ${ident} to ident.
	 * @param s
	 * @return
	 */
	public static String undecorateKey(String s) {
		return s.substring(2, s.length() - 1);
	}

	/**
	 * generate a numerically suffixed unique variable name.
	 * SIDE EFFECT: new variable is added to set. CALLER ASSUMPTION: does not modify set.
	 * @param set set of previously generated variables.
	 * @param s new variable.
	 * @return s or s with unique numeric suffix.
	 */
	public static String uniqueVarName(Collection<String> set, String s) {
		while (set.contains(s)) {
			s = incrementVarSuffix(s);
		}
		return s;
	}

	/**
	 * Like indexOf, except matches multiple characters.
	 * @param s source string.
	 * @param chars target characters.
	 * @return index or -1 if not found.
	 */
	public static int indexOfAny(String s, char[] chars) {
		for (int ich = 0; ich < s.length(); ich++) {
			char ch = s.charAt(ich);
			for (int ich2 = 0; ich2 < chars.length; ich2++) {
				if (ch == chars[ich2]) {
					return ich;
				}
			}
		}
		return -1;
	}

	/**
	 * "table.col" -> "table"
	 * "col" -> null
	 * @param tableColumnExpr table.column expression
	 * @return table name or null
	 */
	public static String getTableName(String tableColumnExpr) {
		int ichDot = tableColumnExpr.indexOf('.');
		return (ichDot != -1) ? tableColumnExpr.substring(0, ichDot) : null;
	}

	/*
	 * "table.col" -> "col"
	 * "col" -> "col"
	 * @param tableColumnExpr table.column expression
	 * @return table name or null
	 */
	public static String getColumnName(String tableColumnExpr) {
		int ichDot = tableColumnExpr.indexOf('.');
		return (ichDot != -1) ? tableColumnExpr.substring(ichDot + 1) : tableColumnExpr;
	}

	/**
	 * ex: Map from film_query_limitlist_item to FilmQueryLimitListItem)
	 * @param s
	 * @return
	 */
	public static String underscoresToMixedCase(String s, boolean capitalizeFirst) {
		StringBuilder sb = new StringBuilder(s.length());
		boolean underscorePrefix = false;
		for (int ich = 0; ich < s.length(); ich++) {
			char ch = s.charAt(ich);
			if (ch == '_') {
				underscorePrefix = true;
			} else {
				sb.append(underscorePrefix || capitalizeFirst ? Character.toUpperCase(ch) : ch);
				underscorePrefix = false;
				capitalizeFirst = false;
			}
		}
		return sb.toString();
	}

	/**
	 * Convert from MixedCaseChars to mixed_case_chars Also convert SMSframe to sms_frame
	 * @param s string to convert
	 * @return converted string
	 */
	public static String mixedCaseToUnderscores(String s) {
		StringBuilder sb = new StringBuilder(s.length());
		boolean lastCharWasUpperCase = false;
		for (int ich = 0; ich < s.length(); ich++) {
			char ch = s.charAt(ich);
			if (Character.isUpperCase(ch)) {
				if ((ich != 0) && !lastCharWasUpperCase) {
					sb.append("_");
				}
				sb.append(Character.toLowerCase(ch));
				lastCharWasUpperCase = true;
			} else {
				sb.append(ch);
				lastCharWasUpperCase = false;
			}
		}
		return sb.toString();
	}

	public static class StringComparator implements Comparator<String> {
		@Override
		public int compare(String s1, String s2) {
			return s1.compareTo(s2);
		}
	}
}
