package com.apptechnik.appinabox.engine.util;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.exceptions.KeywordNotFoundException;
import com.apptechnik.appinabox.engine.exceptions.PropertiesException;

/**
 * Apply substitutions to a template, typically read from a resource
 * substitution variables are ${format}
 */
public class Substitute {
    /**
     * Used in transformations, where we want to transform regular expressions into the result of a
     * function.
     */
    public interface MakeString {
        public String create(String originalString);
    }

    /**
     * Take an input resource and a transformation, and map the strings in the file which match the
     * keys in the mapping to values and write it to the output file.
     * @param resourceName input resource name
     * @param fileOut output file
     * @param transformation mapping of input to output files.
     * @throws FileNotFoundException
     * @throws IOException
     */

    public static void transformResource(String resourceName, File fileOut, Properties transformation)
            throws IOException, PropertiesException {
        InputStream isTemplate = Substitute.class.getClassLoader().getResourceAsStream(resourceName);
        if (isTemplate == null) {
            throw new FileNotFoundException("failed to open resource file " + resourceName);
        }
        Substitute.transformStream(isTemplate, fileOut, transformation);
    }

    public static void transformResource(String resourceName,
                                         File fileOut,
                                         Properties transformation,
                                         boolean undefinedIsEmpty)
    throws IOException, PropertiesException {
        InputStream isTemplate = Substitute.class.getClassLoader().getResourceAsStream(resourceName);
        if (isTemplate == null) {
            throw new FileNotFoundException("failed to open resource file " + resourceName);
        }
        Substitute.transformStream(isTemplate, new FileWriter(fileOut), transformation, undefinedIsEmpty);
    }

    public static String transformResource(String resourceName, Properties transformation)
            throws IOException, PropertiesException {
        InputStream isTemplate = Substitute.class.getClassLoader().getResourceAsStream(resourceName);
        if (isTemplate == null) {
            throw new FileNotFoundException("failed to open resource file " + resourceName);
        }
        StringWriter sw = new StringWriter();
        Substitute.transformStream(isTemplate, sw, transformation);
        return sw.toString();
    }

    /**
     * Given an input stream, an output file, and a properties map, decorate each key with ${key}
     * and replace the decorated key with the property value in the output stream.
     * @param is input stream
     * @param fileOut output file
     * @param transformation properties to apply.
     * @throws IOException
     */
    public static void transformStream(InputStream is, File fileOut, Properties transformation)
            throws IOException, PropertiesException {
        FileWriter fw = new FileWriter(fileOut);
        try {
            transformStream(is, new FileWriter(fileOut), transformation);
        } finally {
            fw.close();
        }
    }

    public static void transformStream(InputStream is, Writer writer, Properties transformation)
            throws IOException, PropertiesException {
        transformStream(is, writer, transformation, true);
    }

    /**
     * Apply a transform to a string and return a string.
     * @param s
     * @param transformation
     * @return
     * @throws IOException
     * @throws PropertiesException
     */
    public static String transformString(String s, Properties transformation)
            throws IOException, PropertiesException {
        StringWriter sw = new StringWriter();
        transformStream(new ByteArrayInputStream(s.getBytes()), sw, transformation);
        return sw.toString();
    }

    /**
     * transform a string and write the results into a file.
     * @param s
     * @param transformation
     * @param outputFile
     * @throws IOException
     * @throws PropertiesException
     */
    public static void transformString(String s, Properties transformation, File outputFile)
            throws IOException, PropertiesException {
        FileWriter fw = new FileWriter(outputFile);
        try {
            transformStream(new ByteArrayInputStream(s.getBytes()), fw, transformation);
        } finally {
            fw.close();
        }
    }
    /**
     * Same, except takes an output stream
     * @param is input stream (a resource, file, or string)
     * @param os output stream
     * @param transformation properties transformation
     * @param  undefinedIsEmpty true: if properties returns NULL for variable, delete the
     *                          tag from the file, false: throw a PropertiesException
     * @throws IOException failed to read the stream
     * @throws PropertiesException property not found for substitution, and undefined is not empty string.
     */
    public static void transformStream(InputStream is,
                                       OutputStream os,
                                       Properties transformation,
                                       boolean undefinedIsEmpty)
            throws IOException, PropertiesException {
        transformStream(is, new OutputStreamWriter(os), transformation, undefinedIsEmpty);
    }

    /**
     * Same, except takes a writer instead of a file.
     * @param is input stream (a resource, file, or string)
     * @param writer output writer (usually a file or string)
     * @param transformation properties transformation
     * @param  undefinedIsEmpty true: if properties returns NULL for variable, delete the
     *                          tag from the file, false: throw a PropertiesException
     * @throws IOException failed to read the stream
     * @throws PropertiesException property not found for substitution, and undefined is not empty string.
     */
    public static void transformStream(InputStream is,
                                       Writer writer,
                                       Properties transformation,
                                       boolean undefinedIsEmpty)
    throws IOException, PropertiesException {

        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        try {
            while (true) {
                String line = br.readLine();
                if (line == null) {
                    break;
                }
                StringBuffer sb = new StringBuffer();
                for (int ich = 0; ich < line.length(); ich++) {
                    String candidateVar = getKey(line, ich);
                    if (candidateVar != null) {
                        String replacement = transformation.getProperty(candidateVar);
                        if (replacement != null) {
                            sb.append(replacement);
                        } else if (!undefinedIsEmpty) {
                            throw new PropertiesException("variable " + candidateVar + " wss not found in your properties map");
                        }
                        ich += candidateVar.length() + 2;   // ${VAR} (for does +1)
                    } else {
                        sb.append(line.charAt(ich));
                    }
                }
                writer.write(sb.toString() + "\n");
            }
        } finally {
            writer.close();
            br.close();
        }
    }

    private static String getKey(String line, int ich) {
        // must have enough space for ${X}
        if (ich < line.length() - 4) {
            if (line.charAt(ich) == '$') {
                if (line.charAt(ich + 1) == '{') {
                    int ichVarStart = ich + 2;
                    int ichVarEnd;
                    for (ichVarEnd = ichVarStart; ichVarEnd < line.length() - 1; ichVarEnd++) {
                        char ch = line.charAt(ichVarEnd);
                        if (ch == '}') {
                            break;
                        }
                        if (!isVariableChar(ch)) {
                            return null;
                        }

                    }
                    if (line.charAt(ichVarEnd) == '}') {
                        return line.substring(ichVarStart, ichVarEnd);
                    }
                }
            }
        }
        return null;
    }

    private static boolean isVariableChar(char ch) {
        return (Character.isLetter(ch) && Character.isUpperCase(ch)) ||
                Character.isDigit(ch) || (ch == '_');
    }

    public static void transformResourceDefaults(String resourceName,
                                                 File fileOut,
                                                 Properties transformation)
    throws IOException {
        InputStream isTemplate = Substitute.class.getClassLoader().getResourceAsStream(resourceName);
        if (isTemplate == null) {
            throw new FileNotFoundException("failed to open resource file " + resourceName);
        }
        Substitute.transformStreamDefaults(isTemplate, fileOut, transformation);
    }

    /**
     * Given an input stream, an output file, and a properties map, decorate each key with ${key}
     * and replace the decorated key with the property value in the output stream.
     * The stream may contain entries of the form ${key:defaultValue}, and if the key is not matched
     * then the default value is taken.
     * @param is input stream
     * @param fileOut output file
     * @param transformation properties to apply.
     * @throws IOException
     */
    public static void transformStreamDefaults(InputStream is,
                                               File fileOut,
                                               Properties transformation)
    throws IOException {
        String regex =  "\\$\\{(.*?)\\}";
        Pattern pattern = Pattern.compile(regex);
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        FileWriter writer = new FileWriter(fileOut);
        try {
            while (true) {
                String line = br.readLine();
                if (line == null) {
                    break;
                }
                Matcher matcher = pattern.matcher(line);
                while (matcher.find()) {
                    String oldValue = matcher.group();
                    String newValue = valueOrDefault(StringUtil.undecorateKey(oldValue), transformation);
                    if (newValue != null) {
                        line = matcher.replaceFirst(newValue);
                        matcher = pattern.matcher(line);
                    }
                }
                writer.write(line + "\n");
            }
        } finally {
            writer.close();
            br.close();
        }
    }

    private static String valueOrDefault(String oldValue, Properties transformation) {
        String ident = getIdentifier(oldValue);
        String newValue = transformation.getProperty(ident);
        if (newValue != null) {
            return newValue;
        } else {
            String defaultValue = getDefault(oldValue, ':', '}');
            if (defaultValue != null) {
                return defaultValue;
            }
        }
        return null;
    }

    private static String getIdentifier(String value) {
        StringBuilder sb = new StringBuilder();
        for (int ich = 0; ich < value.length(); ich++) {
            char ch = value.charAt(ich);
            if (Character.isLetter(ch) || (ch == '_')) {
                sb.append(ch);
            } else if (sb.length() > 0) {
                if (Character.isLetterOrDigit(ch) || (ch == '_')) {
                    sb.append(ch);
                } else {
                    break;
                }
            }
        }
        return sb.toString();
    }

    private static String getDefault(String value, char prefix, char terminator) {
        int ichColon = value.indexOf(prefix);
        if (ichColon != -1) {
            StringBuilder sb = new StringBuilder();
            for (int ich = ichColon + 1; ich < value.length(); ich++) {
                char ch = value.charAt(ich);
                if (ch != terminator) {
                    sb.append(ch);
                }
            }
            return sb.toString();
        } else {
            return null;
        }
    }

    /**
     * param ${param-name}
     * @param s
     * @return
     */
    public static Set<String> getParams(String s) {
        Set<String> params = new HashSet<String>();
        if (s != null) {
            Pattern pattern = Pattern.compile("\\$\\{.+?\\}");
            Matcher matcher = pattern.matcher(s);
            while (matcher.find()) {
                params.add(StringUtil.undecorateKey(matcher.group()));
            }
        }
        return params;
    }

    /**
      * Return a list of groups that match regex
     * @param file source file
     * @param regex regex to match
     * @return list of matching groups.
     */
    public static List<String> matchingLines(File file, String regex) throws IOException {
        return matchingLines(new FileInputStream(file), regex);
     }

    public static List<String> matchingLines(InputStream is, String regex) throws IOException {
        Pattern pattern = Pattern.compile(regex);
        List<String> list = new ArrayList<String>();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        do {
            String line = br.readLine();
            if (line == null) {
                break;
            } else {
                Matcher matcher = pattern.matcher(line);
                if (matcher.find()) {
                    list.add(line);
                }
            }
        } while (true);
        return list;
    }

    public static List<String> matchStartOfLine(File file, String target) throws IOException {
        return matchStartOfLine(new FileInputStream(file), target);
    }

    public static List<String> matchStartOfLine(InputStream is, String target) throws IOException {
        List<String> list = new ArrayList<String>();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        do {
            String line = br.readLine();
            if (line == null) {
                break;
            } else {
                if (line.startsWith(target)) {
                    list.add(line);
                }
            }
        } while (true);
        return list;
    }

    /**
     * Given a resource file with a package directive of the form:
     * package ${PACKAGE}.foo.bar, and a package name com.blah.blotz create the fully qualified package
     * com.blah.blotz.foo.bar, and convert it to a file path relative to the root path.
     * @param rootPath
     * @param resourceName
     * @param packageName
     * @return
     * @throws IOException
     * @throws KeywordNotFoundException
     */
    public static File pathFromPackage(File rootPath, String resourceName, String packageName)
    throws IOException, KeywordNotFoundException {
        InputStream is = Substitute.class.getClassLoader().getResourceAsStream(resourceName);
        if (is == null) {
            throw new FileNotFoundException("failed to open resource " + resourceName);
        }
        List<String> packageLines = matchStartOfLine(is, "package");
        is.close();
        if (packageLines.size() != 1) {
            throw new KeywordNotFoundException("package keyword not found in " + resourceName);
        }
        int ichSeparator = StringUtil.indexOfAny(packageLines.get(0), new char[] { ' ', '\t' });
        String packageSubst = packageLines.get(0).substring(ichSeparator + 1);
        String fullyQualifiedPackage = packageSubst.replace(StringUtil.decorateKey(Constants.Substitutions.PACKAGE), packageName);
        return new File(rootPath, StringUtil.packageToFilePath(fullyQualifiedPackage));
    }

    /**
     * Change resource/path/template.ext.txt to template.ext
     * @param resourcePath
     * @return
     */
    public static String resourceTemplateToFilename(String resourcePath) {
        int ichSlash = resourcePath.lastIndexOf('/');
        String resourceName = (ichSlash == -1) ? resourcePath : resourcePath.substring(ichSlash + 1);
        int ichDot = resourceName.lastIndexOf('.');
        String fileName = (ichDot == -1) ? resourceName : resourceName.substring(0, ichDot);
        return fileName;
    }
}
