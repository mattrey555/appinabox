package com.apptechnik.appinabox.engine.util;

/**
 * URL parsing utilities.
 * TODO: replace these with proper system routines that actually parse and check
 */
public class UrlUtil {
    public static String getPath(String url) {
        int ichQuery = url.indexOf('?');
        return (ichQuery >= 0) ? url.substring(0, ichQuery) : url;
    }

    public static String getParams(String url) {
        int ichQuery = url.indexOf('?');
        return (ichQuery >= 0) ? url.substring(ichQuery + 1) : url;
    }
}
