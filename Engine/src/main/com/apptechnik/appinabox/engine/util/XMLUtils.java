package com.apptechnik.appinabox.engine.util;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Grab-bag of XML-related utilities.
 */
public class XMLUtils {

    /**
     * Create the output transformer for XML files for pretty printing.
     */
    public static Transformer createTransformer() throws TransformerConfigurationException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
        transformer.setOutputProperty(OutputKeys.STANDALONE, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        return transformer;
    }

    /**
     * Adopt a node list from the web.xml that we want to merge in
     * @param document XML source document
     * @param rootElement root element
     * @param mergeDoc document to merge into.
     * @param tagName tag to merge under in mergedDoc
     * @param debug enable logging.
     */
    public static void importNodeList(Document document,
                                      Element rootElement,
                                      Document mergeDoc,
                                      String tagName,
                                      boolean debug) throws XPathExpressionException {
        XPath xPath = XPathFactory.newInstance().newXPath();
        NodeList nodeList = (NodeList) xPath.compile("//" + tagName).evaluate(mergeDoc, XPathConstants.NODESET);
        if (nodeList != null) {
            if (debug) {
                System.out.println("merging " + nodeList.getLength() + " elements under tag " + tagName);
            }
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);
                Node newNode = document.importNode(node, true);
                if (debug) {
                    System.out.println("importing node: " + newNode);
                }
                rootElement.appendChild(newNode);
            }
        }
    }

    /**
     * Validate XML from a file against a list of XSDs
     * @param xmlFile
     * @param xsds
     * @return
     * @throws IOException
     */
    public static boolean validateXML(File xmlFile, String[] xsds)
        throws IOException {

        Source xmlSource = new StreamSource(xmlFile);
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        for (String xsd: xsds) {
            URL xsdUrl = new URL(xsd);
            try {
                Schema schema = schemaFactory.newSchema(xsdUrl);
                Validator validator = schema.newValidator();
                validator.validate(xmlSource);
            } catch (SAXException saxex) {
                System.out.println("Schema validation failed for " + xmlFile.getAbsolutePath() +
                                   " with XSD " + xsd +
                                   " error message: " + saxex.getMessage());
                return false;
            }
        }
        return true;
    }

    /**
     * Add a element to document under parentNode, with name.
     * @param xmlDoc document used to create ekenents
     * @param parentNode parent node
     * @param name name of new node
     * @return Created and appended element.
     */
    public static Element addNode(Document xmlDoc, Node parentNode, String name) {
        Element element = xmlDoc.createElement(name);
        parentNode.appendChild(element);
        return element;

    }

    public static Element addSingleTextElement(Document xmlDoc, Node parentNode, String name, String value) {
        Element element = addNode(xmlDoc, parentNode, name);
        element.appendChild(xmlDoc.createTextNode(value));
        return element;
    }

    /**
     * Starting from a node, ecursively search for an element by name, and return the first occurrence.
     * @param node root node.
     * @param name name to search for
     * @return first matching node or null.
     */
    public static Node findElementByName(Node node, String name) {
        if (name.equals(node.getLocalName()) || name.equals(node.getNodeName())) {
            return node;
        }
        NodeList nodeList = node.getChildNodes();
        for (int iChild = 0; iChild < nodeList.getLength(); iChild++) {
            Node childNode = nodeList.item(iChild);
            Node foundNode = findElementByName(childNode, name);
            if (foundNode != null) {
                return foundNode;
            }
        }
        return null;
    }

    /**
     * Recursively traverse the DOM at node, and return a list of elements which match
     * the specified name and attribute map
     * @param node root node
     * @param name target element name
     * @param attributes target element attributes
     * @return list of matching elements.
     */
    public static List<Element> findElementsByNameAndAttributes(Node node,
                                                                String name,
                                                                Map<String, String> attributes) {
        List<Element> elementList = new ArrayList<Element>();
        findElementsByNameAndAttributes(node, name, attributes, elementList);
        return elementList;
    }

    private static void findElementsByNameAndAttributes(Node node,
                                                        String name,
                                                        Map<String, String> attributes,
                                                        List<Element> elementList) {
        if ((node.getNodeName() != null) && node.getNodeName().equals(name)) {
            if (node instanceof Element) {
                if ((attributes == null) || matchAttributes((Element) node, attributes)) {
                    elementList.add((Element) node);
                }
            }
        }
        NodeList nodeList = node.getChildNodes();
        for (int iChild = 0; iChild < nodeList.getLength(); iChild++) {
            Node childNode = nodeList.item(iChild);
            findElementsByNameAndAttributes(childNode, name, attributes, elementList);
        }
    }

    public static boolean matchAttributes(Element el, Map<String, String> attributes) {
        for (String key : attributes.keySet()) {
            String matchAttr = attributes.get(key);
            String elAttr = el.getAttribute(key);
            if ((elAttr == null) != (matchAttr == null)) {
                return false;
            }
            if ((elAttr != null) && (matchAttr != null)) {
                if (!matchAttr.equals(elAttr)) {
                    return false;
                }
            }
        }
        return true;
    }



    /**
     * Take an XML resource, apply a transformation, and add it as a child to the document node.
     * @param doc containing XML document
     * @param targetNodeName parent node in the XML document
     * @param resourceName resource to insert.
     * @param transform transformation to apply to resource
     * @return node referring to transformed, inserted child.
     * @throws Exception
     */
    public static Node addChildFromTransformedResource(Document doc,
                                                       String targetNodeName,
                                                       Map<String, String> targetNodeAttributes,
                                                       String resourceName,
                                                       Properties transform) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Node documentNode = doc.getDocumentElement();
        List<Element> parentNodeList = findElementsByNameAndAttributes(documentNode, targetNodeName, targetNodeAttributes);
        String xmlStr = Substitute.transformResource(resourceName, transform);
        Document childDoc = builder.parse(new InputSource(new StringReader(xmlStr)));
        Node importedChild = doc.importNode(childDoc.getDocumentElement(), true);
        parentNodeList.stream().forEach(parentNode -> parentNode.appendChild(importedChild));
        return importedChild;
    }

    /**
     * Save an XML document to a file
     * @param xmlFile
     * @param doc
     * @return
     * @throws Exception
     */
    public static void save(File xmlFile, Document doc) throws IOException, TransformerException {
        FileOutputStream fos = new FileOutputStream(xmlFile);
        try {
            StreamResult streamResult = new StreamResult(fos);
            DOMSource domSource = new DOMSource(doc);
            XMLUtils.createTransformer().transform(domSource, streamResult);
        } finally {
            fos.close();
        }
    }

    /**
     * Take a text stream, apply substitutions from the properties file, craete
     * @param properties transformation to apply
     * @param is source input stream.
     * @return XML Document
     * @throws Exception
     */
    public static Document fromTransformedStream(DocumentBuilder builder,
                                                 InputStream is,
                                                 Properties properties) throws Exception {
        File tempFile = File.createTempFile("temp", "xml");
        tempFile.delete();
        Substitute.transformStream(is, tempFile, properties);
        FileReader fr = new FileReader(tempFile);
        try {
            return builder.parse(new InputSource(fr));
        } finally {
            fr.close();
            tempFile.delete();
        }
    }
    /**
     * Take a resource, apply substitutions from the properties file, craete
     * @param resourceName source input stream.
     * @param properties transformation to apply
     * @return XML Document
     * @throws Exception
     */
    public static Document fromTransformedResource(DocumentBuilder builder,
                                                   String resourceName,
                                                   Properties properties) throws Exception {
        InputStream is = XMLUtils.class.getClassLoader().getResourceAsStream(resourceName);
        try {
            return fromTransformedStream(builder, is, properties);
        } finally {
            is.close();
        }
    }

    /**
     * Read an XML document from a resource.
     * @param resourceName name of the resource to read and parse.
     * @return XML Document
     * @throws Exception
     */
    public static Document fromResource(String resourceName) throws Exception {
        InputStream is = XMLUtils.class.getClassLoader().getResourceAsStream(resourceName);
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            return builder.parse(new InputSource(is));
        } finally {
            is.close();
        }
    }

    /**
     * Given a document,  a parent node, a resource name, and a properties transform, insert the
     * transformed resource in document as a child of the parent node.
     * @param builder XML Document builder.
     * @param doc parent document
     * @param parentNode node within parent document
     * @param resourceName resource to insert
     * @param properties transformation.
     * @return inserted node.
     * @throws Exception
     */
    public static Node insertTransformedResource(DocumentBuilder builder,
                                                 Document doc,
                                                 Node parentNode,
                                                 String resourceName,
                                                 Properties properties) throws Exception {
        return parentNode.appendChild(transformResource(builder, doc, resourceName, properties));
    }

    public static Node transformResource(DocumentBuilder builder,
                                         Document doc,
                                         String resourceName,
                                         Properties properties) throws Exception {
        String xmlStr = Substitute.transformResource(resourceName, properties);
        return doc.importNode(builder.parse(new InputSource(new StringReader(xmlStr))).getDocumentElement(), true);
    }

    public static Node insertTransformedString(DocumentBuilder builder,
                                                 Document doc,
                                                 Node parentNode,
                                                 String str,
                                                 Properties properties) throws Exception {
        String xmlStr = Substitute.transformResource(str, properties);
        Document fragmentDocument = builder.parse(new InputSource(new StringReader(xmlStr)));
        Node importedFragment = doc.importNode(fragmentDocument.getDocumentElement(), true);
        parentNode.appendChild(importedFragment);
        return importedFragment;
    }
}
