package com.apptechnik.appinabox.engine.codegen;

import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.codegen.pojos.kotlin.KotlinCodeGen;
import org.junit.Assert;
import org.junit.Test;

public class TestCodeGen {

    @Test
    public void testAllocation() {
        CodeGen codegen = new KotlinCodeGen();
        Type type = codegen.genericType(codegen.classType("Set"), codegen.simpleType("Value"));
        Allocation alloc = codegen.alloc(type);
        Assert.assertEquals("Set<Value>()", alloc.toString());
        Type type2 = codegen.genericType(codegen.simpleType("List"), type);
        Allocation alloc2 = codegen.alloc(type2);
        Assert.assertEquals("List<Set<Value>>()", alloc2.toString());
        Type type3 = codegen.genericType(codegen.classType("Map"), type, type2);
        Allocation alloc3 = codegen.alloc(type3);
        String s = alloc3.toString();
        Assert.assertEquals("Map<Set<Value>,List<Set<Value>>>()", s);
    }

    @Test
    public void testDefaultValue() {
        CodeGen codegen = new KotlinCodeGen();
        CodeExpression expr = codegen.defaultValue(new SimpleType("String"));
        Assert.assertEquals("\"\"",  expr.toString());

    }
}
