package com.apptechnik.appinabox.engine.codegen;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;
import com.apptechnik.appinabox.engine.parser.create.StatementParser;
import org.junit.Test;

import java.io.File;
import java.net.URL;

public class TestCreateObjects {

    @Test
    public void testCreateObejcts() throws Exception {
        URL createTableRes = TestCreateObjects.class.getClassLoader().getResource("dvdrental_create_tables.sql");
        File createTableFile = new File(createTableRes.toURI());
        URL sqlFileRes = TestCreateObjects.class.getClassLoader().getResource("sqlfiles/dvdrental/sql/GET/select_actors_by_category.sql");
        File sqlFile = new File(sqlFileRes.toURI());
        URL propertiesRes = TestCreateObjects.class.getClassLoader().getResource("dvdrental.properties");
        File propertiesFile = new File(propertiesRes.toURI());
        StatementParser statementParser = CreateObjects.parseSchema(createTableFile);
        CreateObjects createObjects = new CreateObjects(sqlFile,
                propertiesFile,
                createTableFile,
                statementParser,
                new File("output"));
    }

}
