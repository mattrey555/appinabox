package com.apptechnik.appinabox.engine.codegen;
import java.util.List;
import java.util.ArrayList;
import java.io.File;

import com.apptechnik.appinabox.engine.codegen.server.ServletDefinition;
import com.apptechnik.appinabox.engine.codegen.server.PopulateWebXML;
import com.apptechnik.appinabox.engine.util.XMLUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.Before;


public class TestCreateWebXML {
	private final String SCHEMA_CONFIG = "./test/schema.config";
	private final String WEBXML_PROPERTIES = "./test/webxml.properties";
	private final String POSTGRESS_CONFIG = "./test/postgres.webxml.config";
	private final String PACKAGE_NAME = "com.company.project";
	private final String WEB_XML = "web.xml";
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testCreateWebXML() throws Exception {
		String current = new java.io.File( "." ).getCanonicalPath();
		System.out.println("Current dir:"+current);
		File outputFile = new File(WEB_XML);
		List<ServletDefinition> servletDefinitionList = new ArrayList<ServletDefinition>();
		ServletDefinition def1 = new ServletDefinition("servlet1", "servlet1Class", "servlet1-query");
		def1.addParam("nameA", "valueA");
		def1.addParam("nameB", "valueB");
		servletDefinitionList.add(def1);
		ServletDefinition def2 = new ServletDefinition("servlet2", "servlet2Class", "servlet2-query");
		servletDefinitionList.add(def2);
		def2.addParam("nameC", "valueC");
		def2.addParam("nameD", "valueD");
		PopulateWebXML.createWebXML(new File(SCHEMA_CONFIG), new File(WEBXML_PROPERTIES),
								    outputFile, PACKAGE_NAME, new File(POSTGRESS_CONFIG),
									servletDefinitionList, null, true);
		String[] xsds = new String[] { "http://xmlns.jcp.org/xml/ns/javaee/web-app_3_1.xsd" };
		Assert.assertTrue(XMLUtils.validateXML(outputFile,xsds));

	}
}
