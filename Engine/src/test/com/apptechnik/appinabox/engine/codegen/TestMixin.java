package com.apptechnik.appinabox.engine.codegen;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.mixin.CreateMixin;
import com.apptechnik.appinabox.engine.codegen.pojos.CodeGen;
import com.apptechnik.appinabox.engine.codegen.pojos.FunctionDefinition;
import com.apptechnik.appinabox.engine.codegen.pojos.MemberDeclaration;
import com.apptechnik.appinabox.engine.codegen.pojos.Type;
import com.apptechnik.appinabox.engine.codegen.pojos.java.*;
import com.apptechnik.appinabox.engine.codegen.util.CodegenUtil;
import com.apptechnik.appinabox.engine.exceptions.UnsupportedExpressionException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestMixin {
    List<MemberDeclaration> decls;
    List<FunctionDefinition> methods;
    CodeGen codegen;

    @Before
    public void setup() {
        codegen = new JavaCodeGen();
        decls = new ArrayList<>();
        decls.add(codegen.memberDecl(codegen.simpleType(Constants.JavaTypes.STRING),
                codegen.var("name")));
        decls.add(codegen.memberDecl(codegen.simpleType(Constants.JavaTypes.STRING),
                codegen.var("first_name")));
        decls.add(codegen.memberDecl(codegen.simpleType(Constants.JavaTypes.STRING),
                codegen.var("last_name")));
        decls.add(codegen.memberDecl(codegen.classType("address"),
                codegen.var("address")));
        decls.add(codegen.memberDecl(codegen.classType("contact"),
                codegen.var("contact")));
        decls.add(codegen.memberDecl(codegen.classType("corrections"),
                codegen.var("corrections")));
        decls.add(codegen.memberDecl(codegen.listType(codegen.classType("notes")),
                codegen.var("notes")));
        decls.add(codegen.memberDecl(codegen.listType(codegen.classType("friends")),
                codegen.var("friends")));
        decls.add(codegen.memberDecl(codegen.listType(codegen.classType("contacts")),
                codegen.var("contacts")));
        decls.add(codegen.memberDecl(codegen.listType(codegen.classType("credits")),
                codegen.var("credits")));
        methods = new ArrayList<FunctionDefinition>();
        for (MemberDeclaration decl : decls) {
            methods.add(codegen.functionDef("get" + decl.getVariable().getName(),
                    codegen.paramList(), decl.getType(),
                    codegen.stmtList()));
            methods.add(codegen.functionDef("set" + decl.getVariable().getName(),
                    codegen.paramList(codegen.paramDecl(decl.getType(), decl.getVariable())),
                    codegen.simpleType(Constants.JavaTypes.VOID),
                    codegen.stmtList()));
        }
    }

    @Test
    public void test() throws UnsupportedExpressionException {
        Map<Type, String> typeMap = new HashMap<Type, String>();
        List<Type> extendedTypes = CreateMixin.extendedTypes(codegen, decls, typeMap);
        System.out.println(extendedTypes.toString());
        List<MemberDeclaration> extendedMembers = CreateMixin.extendMemberDeclarations(codegen, typeMap, decls);
        System.out.println(extendedMembers.toString());

        List<FunctionDefinition> extendedMethods = CreateMixin.extendSettersAndAccessors(codegen, typeMap, methods);
        System.out.println(extendedMethods.toString());
    }

    @Test
    public void testRemapType() throws UnsupportedExpressionException {
        // TODO: Generic?
        Type genericType = codegen.genericType(codegen.classType(Constants.JavaTypes.LIST),
                                               codegen.classType("X"));
        Map<Type, String> typeMap = new HashMap<Type, String>();
        typeMap.put(codegen.classType(Constants.JavaTypes.LIST), "mutableList");
        Type newType = CreateMixin.remapType(codegen, typeMap, genericType);
        Assert.assertEquals("mutableList<X>", newType.toString());
        System.out.println(newType);

    }
}
