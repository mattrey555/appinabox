package com.apptechnik.appinabox.engine.codegen.android;

import com.apptechnik.appinabox.engine.codegen.TestCreateObjects;
import com.apptechnik.appinabox.engine.codegen.objects.CreateObjects;
import com.apptechnik.appinabox.engine.parser.create.StatementParser;
import org.junit.Test;

import java.io.File;
import java.net.URL;

public class TestCreateQueryFragmentXML {

    @Test
    public void testCreateFragmentXML() throws Exception {
        URL createTableRes = TestCreateObjects.class.getClassLoader().getResource("dvdrental_create_tables.sql");
        File createTableFile = new File(createTableRes.toURI());
        URL sqlFileRes = TestCreateObjects.class.getClassLoader().getResource("sqlfiles/dvdrental/sql/GET/select_actors.sql");
        File sqlFile = new File(sqlFileRes.toURI());
        URL propertiesRes = TestCreateObjects.class.getClassLoader().getResource("dvdrental.properties");
        File propertiesFile = new File(propertiesRes.toURI());
        StatementParser statementParser = CreateObjects.parseSchema(createTableFile);
        CreateObjectsAndroid createObjects = new CreateObjectsAndroid(sqlFile,
                propertiesFile,
                createTableFile,
                statementParser,
                new File("output"));
        CreateQueryFragmentXML createFragment = new CreateQueryFragmentXML(createObjects);
        createFragment.createFragmentXML(new File("layout.xml"));

    }
}
