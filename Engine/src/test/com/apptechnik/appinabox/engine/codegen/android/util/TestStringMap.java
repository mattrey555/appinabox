package com.apptechnik.appinabox.engine.codegen.android.util;

import com.apptechnik.appinabox.engine.util.FileUtil;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

public class TestStringMap {

    @Test
    public void testStringMap() throws Exception {
        File stringsFile = new File("strings.xml");
        FileUtil.copyResourceToFile(TestStringMap.class, "xml/strings.xml", stringsFile);
        StringMap stringMap = new StringMap(stringsFile);
        Assert.assertEquals("Endpoints", stringMap.getEntry("endpoint_fragment_label"));
        stringMap.addEntry("test1", "test value");
        Assert.assertEquals("test value", stringMap.getEntry("test1"));
        stringMap.commit();
        stringMap = new StringMap(stringsFile);
        Assert.assertEquals("test value", stringMap.getEntry("test1"));

    }
}
