package com.apptechnik.appinabox.engine.parser;

public class ErrorMessage {
    public enum Severity {
        EXCEPTION("Exception"),
        ERROR("Error"),
        SERIOUS_WARNING("Serious Warning"),
        WARNING("Warning"),
        NOTE("Note");

        private String description;

        Severity(String s) {
            this.description = s;
        }
    }
    public final int line;
    public final int position;
    public final Severity severity;
    public final String description;

    public ErrorMessage(int line, int position, Severity severity, String description) {
        this.line = line;
        this.position = position;
        this.severity = severity;
        this.description = description;
    }
    public ErrorMessage(Severity severity, String description) {
        this.line = 0;
        this.position = 0;
        this.severity = severity;
        this.description = description;
    }
}
