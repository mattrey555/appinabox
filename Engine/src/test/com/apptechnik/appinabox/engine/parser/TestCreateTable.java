package com.apptechnik.appinabox.engine.parser;

import com.apptechnik.appinabox.engine.parser.create.StatementParser;
import com.apptechnik.appinabox.engine.util.FileUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class TestCreateTable {
    private TableInfoMap tableInfoMap;

    @Before
    public void setup() throws Exception {
        StatementParser statementParser = new StatementParser();
        statementParser.parse(FileUtil.getResource(TestCreateTable.class, "stores_create_tables.sql"));
        tableInfoMap = statementParser.getTableInfoMap();
    }
    @Test
    public void testCreateTables() throws Exception {
        Assert.assertNotNull(tableInfoMap.getTableInfo("stores"));
        Assert.assertNotNull(tableInfoMap.getTableInfo("items"));
        Assert.assertNotNull(tableInfoMap.getTableInfo("customers"));
        Assert.assertNotNull(tableInfoMap.getTableInfo("purchases"));
        Assert.assertNotNull(tableInfoMap.getTableInfo("purchase_notes"));
        Assert.assertNotNull(tableInfoMap.getTableInfo("users"));
        Assert.assertNotNull(tableInfoMap.getTableInfo("auth_tokens"));
    }

    private void testKey(String tableName, String primaryKeyName) {
        TableInfo storesTable = tableInfoMap.getTableInfo(tableName);
        List<ColumnInfo> primaryKeys = storesTable.getPrimaryKeyColumns();
        Assert.assertEquals(primaryKeyName, primaryKeys.get(0).getColumnName());

    }
    @Test
    public void testPrimaryKeys() throws Exception {
        testKey("stores", "store_id");
        testKey("purchases", "purchase_id");
    }
}
