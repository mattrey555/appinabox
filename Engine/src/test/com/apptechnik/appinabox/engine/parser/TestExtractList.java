package com.apptechnik.appinabox.engine.parser;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.codegen.pojos.java.*;
import com.apptechnik.appinabox.engine.parser.json.ParseJSONMapExpression;
import com.apptechnik.appinabox.engine.parser.pojos.*;
import com.apptechnik.appinabox.engine.parser.query.SelectTableUtils;
import com.apptechnik.appinabox.engine.parser.util.JsonPathMatcher;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TestExtractList {
    private TestUtil testUtil;
    private CodeGen codegen;

    @Before
    public void setUp() throws Exception {
        testUtil = new TestUtil();
        testUtil.createTablesDvdrental();
        codegen = new JavaCodeGen();
    }

    // TOOD: check for errors for ambiguous or non-resolving path expressions.
    @Test
    public void testExtractList() throws Exception {
        String fieldsStr = "customer.customer_id, rental.inventory_id, rental.rental_id, rental.rental_date";
        String tablesStr = "customer INNER JOIN rental ON customer.customer_id = rental.customer_id";
        String selectStr = "SELECT " + fieldsStr + "  FROM " + tablesStr + " WHERE customer.customer_id IN (:customer_id);";
        String json = "{ customer: { customer_id, inventory : { inventory_id, rental_id, rental_date }}";
        Tree<QueryFieldsNode> jsonTree = ParseJSONMapExpression.parse(json);
        jsonTree.get().setName("result");
        String s = QueryFieldsNode.toJsonString(jsonTree);
        System.out.println(s);
        Select select = TestUtil.parseSelect(selectStr);
        PlainSelect ps = (PlainSelect) select.getSelectBody();
        TableInfoMap selectTableInfoMap = SelectTableUtils.selectTables(ps, testUtil.getTableInfoMap());
        ParameterMap parameterMap = ExpressionType.getNamedParameterTypes(select, selectTableInfoMap, testUtil.getTableInfoMap());
        SelectTableUtils.resolveColumns(ps, jsonTree, parameterMap, testUtil.getTableInfoMap());
        List<String> path = new ArrayList<>(3);
        path.add("customer");
        path.add("customer_id");
        ParameterMap.ParamData paramData = parameterMap.getParamData("customer_id");
        List<JsonPathMatcher.PathMatch<QueryFieldsNode>> pathMatches = JsonPathMatcher.matchPaths(jsonTree, paramData, path);
        Assert.assertNotEquals(0, pathMatches.size());
        FunctionDefinition fnDef = JsonPathMatcher.extractListFromPath(codegen, pathMatches.get(0));
        System.out.println(fnDef.toString());

        // TODO: deconstruct the declaration and verify it against the path
        Assert.assertEquals("public static List<Integer> extract_customer_customer_id(List<result> result)", fnDef.getFunctionDeclaration().toString());
        Assert.assertEquals(1, fnDef.getStatementlist().getStatementList().size());
        CodeStatement statement = (CodeStatement) fnDef.getStatementlist().getStatementList().get(0);
        Assert.assertTrue(statement instanceof JavaReturnStatement);
        JavaReturnStatement returnStatement = (JavaReturnStatement) statement;
        CodeExpression expr = returnStatement.getExpression();
        Assert.assertTrue(expr instanceof CodeBinaryExpression);
        MethodCall collectCall = (MethodCall) ((CodeBinaryExpression) expr).getRight();
        Assert.assertEquals(Constants.Functions.COLLECT, collectCall.getMethod());
        // TODO: deconstruct the expression and validate it. add more tests
    }
}
