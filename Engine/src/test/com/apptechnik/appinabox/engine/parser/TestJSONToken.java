package com.apptechnik.appinabox.engine.parser;

import com.apptechnik.appinabox.engine.parser.json.JSONToken;
import org.junit.Assert;
import org.junit.Test;
import java.io.StringReader;

public class TestJSONToken {

    @Test
    public void testParse() throws Exception {
        String expr = "{ a.f1, a.f2, b: { f1, f2, c: { f1, f2 }}}";
        StringReader sr = new StringReader(expr);
        JSONToken jsonToken = new JSONToken(sr);
        JSONToken.Token tk = jsonToken.nextToken();
        Assert.assertEquals(tk.type, JSONToken.TokenType.OPEN_CURLY);
        tk = jsonToken.nextToken();
        Assert.assertEquals(tk.type, JSONToken.TokenType.IDENT);
        tk = jsonToken.nextToken();
        Assert.assertEquals(tk.type, JSONToken.TokenType.DOT);
        tk = jsonToken.nextToken();
        Assert.assertEquals(tk.type, JSONToken.TokenType.IDENT);
        tk = jsonToken.nextToken();
        Assert.assertEquals(tk.type, JSONToken.TokenType.COMMA);
        tk = jsonToken.nextToken();
        Assert.assertEquals(tk.type, JSONToken.TokenType.IDENT);
        tk = jsonToken.nextToken();
        Assert.assertEquals(tk.type, JSONToken.TokenType.DOT);
        tk = jsonToken.nextToken();
        Assert.assertEquals(tk.type, JSONToken.TokenType.IDENT);
        tk = jsonToken.nextToken();
        Assert.assertEquals(tk.type, JSONToken.TokenType.COMMA);
        tk = jsonToken.nextToken();
        Assert.assertEquals(tk.type, JSONToken.TokenType.IDENT);
        tk = jsonToken.nextToken();
        Assert.assertEquals(tk.type, JSONToken.TokenType.COLON);
        tk = jsonToken.nextToken();
        Assert.assertEquals(tk.type, JSONToken.TokenType.OPEN_CURLY);
        tk = jsonToken.nextToken();
        Assert.assertEquals(tk.type, JSONToken.TokenType.IDENT);
        tk = jsonToken.nextToken();
        Assert.assertEquals(tk.type, JSONToken.TokenType.COMMA);
        tk = jsonToken.nextToken();
        Assert.assertEquals(tk.type, JSONToken.TokenType.IDENT);
        tk = jsonToken.nextToken();
        Assert.assertEquals(tk.type, JSONToken.TokenType.COMMA);
        tk = jsonToken.nextToken();
        Assert.assertEquals(tk.type, JSONToken.TokenType.IDENT);
        tk = jsonToken.nextToken();
        Assert.assertEquals(tk.type, JSONToken.TokenType.COLON);
        tk = jsonToken.nextToken();
        Assert.assertEquals(tk.type, JSONToken.TokenType.OPEN_CURLY);
        tk = jsonToken.nextToken();
        Assert.assertEquals(tk.type, JSONToken.TokenType.IDENT);
        tk = jsonToken.nextToken();
        Assert.assertEquals(tk.type, JSONToken.TokenType.COMMA);
        tk = jsonToken.nextToken();
        Assert.assertEquals(tk.type, JSONToken.TokenType.IDENT);
        tk = jsonToken.nextToken();
        Assert.assertEquals(tk.type, JSONToken.TokenType.CLOSE_CURLY);
        tk = jsonToken.nextToken();
        Assert.assertEquals(tk.type, JSONToken.TokenType.CLOSE_CURLY);
        tk = jsonToken.nextToken();
        Assert.assertEquals(tk.type, JSONToken.TokenType.CLOSE_CURLY);
    }
}