package com.apptechnik.appinabox.engine.parser;

import com.apptechnik.appinabox.engine.parser.query.JoinUtils;
import net.sf.jsqlparser.parser.CCJSqlParser;
import net.sf.jsqlparser.parser.StringProvider;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.Statements;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class TestJoinUtils {
    private PlainSelect testSelectGenerate(String selectStr) throws Exception {
        CCJSqlParser parser = new CCJSqlParser(new StringProvider(selectStr));
        Statements statements = parser.Statements();
        Statement statement = statements.getStatements().get(0);
        Assert.assertTrue(statement instanceof Select);
        Select select = (Select) statement;
        PlainSelect plainSelect = (PlainSelect) select.getSelectBody();
        return plainSelect;
    }

    private boolean inTableList(String s, List<Table> list) {
        for (Table t : list) {
            if (t.getAlias() != null) {
                if (s.equals(t.getAlias().getName())) {
                    return true;
                }
            }
            if (s.equals(t.getName())) {
                return true;
            }
        }
        return false;
    }

    @Test
    public void testGetJoinTables() throws Exception {
        PlainSelect plainSelect = testSelectGenerate("SELECT a.* FROM a INNER JOIN b ON a.id = b.id");
        List<Table> tableList = JoinUtils.getJoinTables(plainSelect);
        Assert.assertTrue(inTableList("a", tableList));
        Assert.assertTrue(inTableList("b", tableList));
    }

    @Test
    public void testGetJoinTablesAliases() throws Exception {
        PlainSelect plainSelect = testSelectGenerate("SELECT a.*,alias.* FROM a INNER JOIN b AS alias ON a.id = b.id");
        List<Table> tableList = JoinUtils.getJoinTables(plainSelect);
        Assert.assertTrue(inTableList("a", tableList));
        Assert.assertTrue(inTableList("alias", tableList));
    }
}
