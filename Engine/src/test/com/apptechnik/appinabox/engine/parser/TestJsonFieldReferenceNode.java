package com.apptechnik.appinabox.engine.parser;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.parser.create.StatementParser;
import com.apptechnik.appinabox.engine.parser.json.ParseJSONMapExpression;
import com.apptechnik.appinabox.engine.parser.pojos.*;
import com.apptechnik.appinabox.engine.parser.query.SelectTableUtils;
import com.apptechnik.appinabox.engine.util.IndentingFileWriter;
import com.apptechnik.appinabox.engine.util.FileUtil;
import net.sf.jsqlparser.parser.CCJSqlParser;
import net.sf.jsqlparser.parser.StringProvider;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.Statements;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileWriter;

public class TestJsonFieldReferenceNode {
    private TableInfoMap tableInfoMap;

    @Before
    public void before() throws Exception {
        StatementParser statementParser = new StatementParser();
        String createTablesSQL = FileUtil.getResource(TestJsonFieldReferenceNode.class,"stores_create_tables.sql");
        statementParser.parse(createTablesSQL);
        tableInfoMap = statementParser.getTableInfoMap();
    }

    private void testSelectGenerate(String selectStr, String nesting, String name) throws Exception {
        CCJSqlParser parser = new CCJSqlParser(new StringProvider(selectStr));
        Statements statements = parser.Statements();
        Statement statement = statements.getStatements().get(0);
        Assert.assertTrue(statement instanceof Select);
        Select select = (Select) statement;
        Tree<QueryFieldsNode> jsonTree = ParseJSONMapExpression.parse(nesting);
        jsonTree.get().setName(name);
        IndentingFileWriter fw = new IndentingFileWriter(new FileWriter(new File(name + Constants.Extensions.JAVA)));
        PlainSelect plainSelect = (PlainSelect) select.getSelectBody();
        ParameterMap parameterMap = new ParameterMap();
        TableInfoMap selectTableInfoMap = SelectTableUtils.selectTables(select, tableInfoMap);
        ColumnsAndKeys columnsAndKeys = SelectTableUtils.resolveColumns(plainSelect, jsonTree, parameterMap, selectTableInfoMap);
        //QueryObjects.generateObject(fw, jsonTree);
        fw.close();
    }

    public void testGenerateJavaFiles(String selectStr, String nesting, String className) throws Exception {
        CCJSqlParser parser = new CCJSqlParser(new StringProvider(selectStr));
        Statements statements = parser.Statements();
        Statement statement = statements.getStatements().get(0);
        Assert.assertTrue(statement instanceof Select);
        Select select = (Select) statement;
        PlainSelect plainSelect = (PlainSelect) select.getSelectBody();
        Tree<QueryFieldsNode> jsonTree = ParseJSONMapExpression.parse(nesting);
        jsonTree.get().setName(className);
        IndentingFileWriter fw = new IndentingFileWriter(new FileWriter(new File(className + Constants.Extensions.JAVA)));
        ParameterMap parameterMap = new ParameterMap();
        TableInfoMap selectTableInfoMap = SelectTableUtils.selectTables(select, tableInfoMap);
        ColumnsAndKeys columnsAndKeys = SelectTableUtils.resolveColumns(plainSelect, jsonTree, parameterMap, selectTableInfoMap);
    }

    @Test
    public void testSimpleSelect() throws Exception {
        testSelectGenerate("select name,address from customers", "{ name, address }", "simple");
        testSelectGenerate("select c.name, c.address from customers as c", "{ c.name, c.address }", "aliases");
    }

    @Test
    public void testAliasedTable() throws Exception {
        String selectStr = "select price, c.name, purchase_id from purchases right outer join customers as c on purchases.customer_id = c.customer_id";
        String nestStr = "{c.name, prices : { purchases.price, purchase_id }}";
        testGenerateJavaFiles(selectStr, nestStr, "purchases");
    }

    @Test
    public void testMoreNestedSelect() throws Exception {
        String fields = "s.name AS store_name, c.name AS customer_name, p.price as purchase_price, p.purchase_id as purchase_id";
        String join = "purchases AS p RIGHT OUTER JOIN customers AS c ON p.customer_id = c.customer_id " +
                      "RIGHT OUTER JOIN stores AS s ON purchase.store_id = stores.store_id ";
        String selectStr = "SELECT " + fields + " FROM " + join;
        String nestStr = "{store_name, customer : { customer_name, purchases : { purchase_price, purchase_id }}";
        testGenerateJavaFiles(selectStr, nestStr, "purchasesByStore");
    }
}
