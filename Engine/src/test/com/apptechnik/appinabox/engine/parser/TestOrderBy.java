package com.apptechnik.appinabox.engine.parser;

import com.apptechnik.appinabox.engine.parser.query.OrderByUtils;
import com.apptechnik.appinabox.engine.parser.query.SelectTableUtils;
import com.apptechnik.appinabox.engine.codegen.util.SQLFile;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class TestOrderBy {
    private TestUtil testUtil;

    @Before
    public void setUp() throws Exception {
        testUtil = new TestUtil();
        testUtil.createTables("sports_create_tables.sql");
    }

    @Test
    public void testOrderBy() throws Exception {
        String sql = "SELECT" +
                "       events.event_key," +
                "       baseball_pitching_stats.event_credit," +
                "       persons.id as person_id," +
                "       display_names.full_name" +
                " FROM" +
                "       stats, events, baseball_pitching_stats, persons, display_names" +
                " WHERE" +
                "       stats.stat_repository_type = 'baseball_pitching_stats'" +
                "       AND stats.stat_repository_id = baseball_pitching_stats.id" +
                "       AND events.event_key like '%mlb%'" +
                "       AND stats.stat_coverage_id = events.id" +
                "       AND stats.stat_holder_type = 'persons'" +
                "       AND persons.id = stats.stat_holder_id" +
                "       AND baseball_pitching_stats.event_credit IS NOT NULL" +
                "       AND display_names.entity_id = stats.stat_holder_id" +
                "       AND display_names.entity_type = 'persons'" +
                " ORDER BY" +
                "       events.event_key";
        SQLFile sqlFile = new SQLFile(sql, true);
        Select select = (Select) sqlFile.getStatementList().get(0);
        PlainSelect plainSelect = (PlainSelect) select.getSelectBody();
        TableInfoMap selectTableInfoMap = SelectTableUtils.selectTables(plainSelect, testUtil.getTableInfoMap());
        List<ColumnInfo> orderByColumns = OrderByUtils.getOrderByColumns(plainSelect, selectTableInfoMap);
        Assert.assertEquals(1, orderByColumns.size());

        // TEMPORARY: DO NOT CHECK THIS IN.
        // Collection<ColumnInfo> removeColumns = OrderByUtils.findBadOrderByKeys(plainSelect.getSelectItems(), orderByColumns, selectTableInfoMap);
        //Assert.assertEquals(0, removeColumns.size());
    }
}
