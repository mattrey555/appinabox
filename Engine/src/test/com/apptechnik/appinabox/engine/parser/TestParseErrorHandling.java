package com.apptechnik.appinabox.engine.parser;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.parser.json.ParseJSONMapExpression;
import com.apptechnik.appinabox.engine.parser.pojos.*;
import com.apptechnik.appinabox.engine.util.CheckedConsumer;
import com.apptechnik.appinabox.engine.util.FileUtil;
import com.apptechnik.appinabox.engine.codegen.util.SQLFile;
import net.sf.jsqlparser.statement.Statement;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;

public class TestParseErrorHandling {
	private TestUtil testUtil;

	@Before
	public void setUp() throws Exception {
		testUtil = new TestUtil();
		testUtil.createTables("sports_create_tables.sql");
	}

	/**
	 * Test the parameter validation.
	 * @param sqlResource
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public List<ParameterMap.ParamError> testSQLResourceParameterValidation(String sqlResource, String name) throws Exception {
		String sqlData = FileUtil.getResource(TestParseErrorHandling.class, sqlResource);
		SQLFile sqlFile = new SQLFile(sqlData, true);
		String jsonFormat = sqlFile.findTag(Constants.Directives.JSON_RESPONSE);
		TypeMap fixedParamTypeMap = new TypeMap(sqlFile);
		testUtil.getTableInfoMap().resolveAllChildReferences();
		Map<Statement, ParameterMap> statementParameterMapMap = ParameterMap.applyTypes(sqlFile.getStatementList(), testUtil.getTableInfoMap());
		statementParameterMapMap.values().stream().forEach(CheckedConsumer.wrap(paramMap -> paramMap.applyFixedParamTypes(fixedParamTypeMap)));
		Assert.assertEquals(1, statementParameterMapMap.size());
		Tree<QueryFieldsNode> jsonTree = ParseJSONMapExpression.parse(jsonFormat, new QueryFieldsNode.Factory());
		jsonTree.get().setName(name);
		FieldReferenceNode.validate(jsonTree);
		ParameterMap queryParams = new ParameterMap(sqlFile.getComments());
		return ParameterMap.validateParameters(statementParameterMapMap, queryParams, sqlFile.readFixedParamTypeMap());
	}


	/**
	 * Test a SQL expression where the query parameter isn't in a SQL statement.
	 * @throws Exception
	 */
	@Test
	public void testNamedParamInQueryNotSelect() throws Exception {
		List<ParameterMap.ParamError> errorList = testSQLResourceParameterValidation("sqlfiles/get/get_events_bad_param.sql", "bad_param");
		Assert.assertEquals(1, errorList.size());
		Assert.assertTrue(ParameterMap.ParamError.anyFatalError(errorList));
		Assert.assertEquals("event_key", errorList.get(0).getParamName());
	}
}
