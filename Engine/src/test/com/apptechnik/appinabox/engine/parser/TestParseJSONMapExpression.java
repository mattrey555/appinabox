package com.apptechnik.appinabox.engine.parser;

import com.apptechnik.appinabox.engine.parser.json.JSONToken;
import com.apptechnik.appinabox.engine.parser.json.ParseJSONMapExpression;
import com.apptechnik.appinabox.engine.parser.pojos.FieldReferenceNode;
import com.apptechnik.appinabox.engine.parser.pojos.ParameterMap;
import com.apptechnik.appinabox.engine.parser.pojos.QueryFieldsNode;
import com.apptechnik.appinabox.engine.parser.pojos.Tree;
import com.apptechnik.appinabox.engine.parser.util.JsonPathMatcher;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.StringValue;
import net.sf.jsqlparser.statement.create.table.ColDataType;
import org.junit.Assert;
import org.junit.Test;

import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class TestParseJSONMapExpression {

    @Test
    public void testParse() throws Exception {

        Tree<? extends FieldReferenceNode> a = ParseJSONMapExpression.parse("{f1, f2}");
        System.out.println("A: " + FieldReferenceNode.createString(a));
        Tree<? extends FieldReferenceNode> b = ParseJSONMapExpression.parse("{a.f1, a.f2}");
        System.out.println("B: " + FieldReferenceNode.createString(b));
        Tree<? extends FieldReferenceNode> c = ParseJSONMapExpression.parse("{a.f1, a.f2, b : { f3, f4 }}");
        System.out.println("C: " + FieldReferenceNode.createString(c));
        Tree<? extends FieldReferenceNode> d = ParseJSONMapExpression.parse("{a.f1, a.f2, b : { f3, f4 }, c : { f5, f6 }");
        System.out.println("D: " + FieldReferenceNode.createString(d));
        Tree<? extends FieldReferenceNode> e = ParseJSONMapExpression.parse("{ * }");
        System.out.println("E: " + FieldReferenceNode.createString(a));
        String s = "{a.f1, a.f2, b : { f3, f4, c : { f5, f6 } } }";
        JSONToken.TokenList tokenList = JSONToken.tokenList(s);
        System.out.println("TokenList: " + s);
        Tree<? extends FieldReferenceNode> f = ParseJSONMapExpression.parse(s);
        System.out.println("F: " + FieldReferenceNode.createString(f));
    }

    @Test
    public void testSingleRootNode() throws Exception {
        Tree<? extends FieldReferenceNode> d = ParseJSONMapExpression.parse("{a.f1, a.f2, b : { f3, f4 }, c : { f5, f6 }");
        d.get().setName("root");
        List<String> nodeNames = FieldReferenceNode.getNodeNames(d);
        Assert.assertEquals(1, nodeNames.size());
        Assert.assertEquals("root", nodeNames.get(0));
    }

    @Test
    public void testMultipleRootNode() throws Exception {
        Tree<? extends FieldReferenceNode> e = ParseJSONMapExpression.parse("{b : { f3, f4 }, c : { f5, f6 }");
        e.get().setName("root");
        List<String> nodeNames2 = FieldReferenceNode.getNodeNames(e);
        Assert.assertEquals(2, nodeNames2.size());
        Assert.assertEquals("b", nodeNames2.get(0));
        Assert.assertEquals("c", nodeNames2.get(1));
    }

    @Test
    public void testParseError() throws Exception {
        String json = "{ event_key, { start_date_time, person_key, person_dn.full_name, team_key, team_dn.full_name, rushing_stats : { american_football_rushing_stats.* }, stats.context }}";
        try {
            Tree<QueryFieldsNode> a = ParseJSONMapExpression.parse(json);
            Assert.fail("should throw an exception");
        } catch (ParseException pex) {
            Assert.assertEquals("unrecognized token: { in event_key", pex.getMessage());
        }
        json = "{ event_key, event : { start_date_time, person_key, person_dn.full_name, team_key, team_dn.full_name, rushing_stats : { american_football_rushing_stats.* }, stats.context }}";
        Tree<QueryFieldsNode> a = ParseJSONMapExpression.parse(json);
        Assert.assertEquals(1, a.get().getFieldReferenceCount());
    }

    public void testPathExtract(String name, String json, String path) throws Exception {
        List<String> pathList = Arrays.asList(path.split("\\.")).stream().map(s -> s.trim()).collect(Collectors.toList());
        Tree<QueryFieldsNode> a = ParseJSONMapExpression.parse(json);
        a.get().setName(name);
        JsonPathMatcher.NodeFullPath nodeFullPath = new JsonPathMatcher.NodeFullPath(pathList);
        Assert.assertTrue(a.traversePredicateNode(nodeFullPath));
        validatePath(pathList, nodeFullPath.getNodeList());
    }

    @Test
    public void testNestedExpression() throws Exception {
        String json = "{ a, b, c, d: { e, f, g: { h, i, j, k : { m : { n, o }, p }, q : { u }, v : { w: { x, y, z}}}}";
        testPathExtract("top", json, "top.d.g.k");
    }

    public List<JsonPathMatcher.PathMatch<QueryFieldsNode>> testPartialPathMatch(String name, String json, String path) throws Exception {
        List<String> pathList = Arrays.asList(path.split("\\.")).stream().map(s -> s.trim()).collect(Collectors.toList());
        Tree<QueryFieldsNode> a = ParseJSONMapExpression.parse(json);
        a.get().setName(name);
        Expression expr = new StringValue("this is a test");
        ColDataType colDataType = new ColDataType();
        colDataType.setDataType("INTEGER");

        ParameterMap.ParamData paramData = new ParameterMap.ParamData(ParameterMap.ParamSource.REQUEST_JSON, expr, colDataType);
        return JsonPathMatcher.matchPaths(a, paramData, pathList);
    }

    @Test
    public void testPartialPathMatch() throws Exception {
        String json = "{ a, b, c, d: { e, f, g: { h, i, j, k : { m : { n, o }, p }, q : { u }, v : { w: { x, y, z}}}}";
        List<JsonPathMatcher.PathMatch<QueryFieldsNode>> matches = testPartialPathMatch("top", json, "g.k.p");
        Assert.assertEquals(1, matches.size());
        JsonPathMatcher.PathMatch<QueryFieldsNode> match = matches.get(0);
        String res = match.getMatchNodes().stream().map(node -> node.get().getVarName()).collect(Collectors.joining("."));
        Assert.assertEquals("top.d.g.k", res);
        Assert.assertEquals("p", match.getFieldReference().getColumnName());
        String json2 = "{ a.a1, b.b1, c.c1, d1: { e.e1, f.f1, g : { h.h1 i.i1, j.j1, k : { m : { n.n1, o }, p.p1 }, q : { u }, v : { w: { x, y, z}}}}";
        matches = testPartialPathMatch("top", json2, "g.k.p.p1");
        Assert.assertEquals(1, matches.size());
        match = matches.get(0);
        res = match.getMatchNodes().stream().map(node -> node.get().getVarName()).collect(Collectors.joining("."));
        res = res + "." + match.getFieldReference().toString();
        Assert.assertEquals("top.d1.g.k.p.p1", res);
        matches = testPartialPathMatch("top", json2, "p.p1");
        res = match.getMatchNodes().stream().map(node -> node.get().getVarName()).collect(Collectors.joining("."));
        res = res + "." + match.getFieldReference().toString();
        Assert.assertEquals("top.d1.g.k.p.p1", res);

        json = "{ customer: { customer_id, rental : { inventory_id, rental_id, rental_date }}";
        matches = testPartialPathMatch("top", json, "customer_id");
        Assert.assertEquals(1, matches.size());
    }

    private static void validatePath(List<String> path, List<FieldReferenceNode> nodePath) {
        Assert.assertEquals(path.size(), nodePath.size());
        for (int i = 0; i < path.size(); i++) {
            Assert.assertEquals(path.get(i), nodePath.get(i).getVarName());
        }
    }
}
