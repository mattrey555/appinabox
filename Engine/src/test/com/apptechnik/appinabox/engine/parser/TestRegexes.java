package com.apptechnik.appinabox.engine.parser;

import com.apptechnik.appinabox.engine.Constants;
import org.junit.Assert;
import org.junit.Test;

public class TestRegexes {

    @Test
    public void testRegexes() {
        Assert.assertTrue("com.example.test".matches(Constants.Regexes.JAVA_PACKAGE));
        Assert.assertFalse("bogus text with spaces".matches(Constants.Regexes.JAVA_PACKAGE));
        Assert.assertTrue("identifier_09".matches(Constants.Regexes.IDENTIFIER));
        Assert.assertFalse("09number".matches(Constants.Regexes.IDENTIFIER));
    }
}
