package com.apptechnik.appinabox.engine.parser;

import net.sf.jsqlparser.parser.CCJSqlParserManager;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.update.Update;
import org.junit.Test;

import java.io.StringReader;

public class TestSQLStatements {

    @Test
    public void testStatements() throws Exception {
        String insertReturns = "INSERT INTO table (a,b,c) values (1,2,3) returning id;";
        CCJSqlParserManager parseManager = new CCJSqlParserManager();
        Statement stmt = parseManager.parse(new StringReader(insertReturns));
        System.out.println("statement = " + stmt);
    }
}
