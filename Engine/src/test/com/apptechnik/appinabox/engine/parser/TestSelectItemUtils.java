package com.apptechnik.appinabox.engine.parser;

import com.apptechnik.appinabox.engine.parser.create.StatementParser;
import com.apptechnik.appinabox.engine.util.FileUtil;
import net.sf.jsqlparser.parser.CCJSqlParser;
import net.sf.jsqlparser.parser.CCJSqlParserManager;
import net.sf.jsqlparser.parser.StringProvider;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.Statements;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import org.junit.Assert;
import org.junit.Before;

public class TestSelectItemUtils {
    private TableInfoMap tableInfoMap;

    @Before
    public void setup() throws Exception {
        StatementParser statementParser = new StatementParser();
        statementParser.parse(FileUtil.getResource(TestSelectItemUtils.class, "stores_create_tables.sql"));
        CCJSqlParserManager parseManager = new CCJSqlParserManager();
        tableInfoMap = statementParser.getTableInfoMap();
    }

    private PlainSelect parsePlainSelect(String selectStr) throws Exception {
        CCJSqlParser parser = new CCJSqlParser(new StringProvider(selectStr));
        Statements statements = parser.Statements();
        Statement statement = statements.getStatements().get(0);
        Assert.assertTrue(statement instanceof Select);
        Select select = (Select) statement;
        return (PlainSelect) select.getSelectBody();
    }
}
