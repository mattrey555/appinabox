package com.apptechnik.appinabox.engine.parser;


import com.apptechnik.appinabox.engine.parser.query.SelectTableUtils;
import com.apptechnik.appinabox.engine.parser.sync.ValidateSync;
import net.sf.jsqlparser.statement.select.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestSelectTableUtils {
    private final static String SELECT1 = "SELECT store.store_id as the_store_id, city.city, address.address, address.district, inventory_select.inventory_id, film.title, film.release_year \n" +
            "FROM store \n" +
            "INNER JOIN address ON store.address_id = address.address_id \n" +
            "INNER JOIN city ON address.city_id = city.city_id \n" +
            "INNER JOIN (select * FROM inventory) AS inventory_select ON store.store_id = inventory_select.store_id \n" +
            "INNER JOIN film on inventory_select.film_id = film.film_id";

    private final static String SELECT2 = "SELECT store.store_id as the_store_id, city.city, address.address, address.district, inventory_select.inventory_id, film.title, film.release_year \n" +
            "FROM store \n" +
            "INNER JOIN address ON store.address_id = address.address_id \n" +
            "INNER JOIN city ON address.city_id = city.city_id \n" +
            "INNER JOIN (select * FROM inventory) AS inventory_select ON store.store_id = inventory_select.store_id \n" +
            "INNER JOIN film on inventory_select.film_id = film.film_id \n" +
            "WHERE address.address > 'M'";

    private final static String SELECT3 = "SELECT store.*, city.city, address.address, address.district, inventory_select.inventory_id, film.title, film.release_year \n" +
            "FROM store \n" +
            "INNER JOIN address ON store.address_id = address.address_id \n" +
            "INNER JOIN city ON address.city_id = city.city_id \n" +
            "INNER JOIN (select * FROM inventory) AS inventory_select ON store.store_id = inventory_select.store_id \n" +
            "INNER JOIN film on inventory_select.film_id = film.film_id \n" +
            "WHERE address.address > 'M'";

    private final static String SELECT4 = "SELECT city.city, address.address, address.district, inv_sel.inventory_id, inv_sel.title, inv_sel.release_year \n" +
            "FROM store \n" +
            "INNER JOIN address ON store.address_id = address.address_id \n" +
            "INNER JOIN city ON address.city_id = city.city_id \n" +
            "INNER JOIN (select inventory.inventory_id, inventory.store_id, film.title, film.release_year FROM inventory " +
            "INNER JOIN film on inventory.film_id = film.film_id) AS inv_sel ON store.store_id = inv_sel.store_id \n" +
            "WHERE address.address > 'M'";

    private final static String SELECT5 = "SELECT address.address, address.district, inv_sel.inventory_id, inv_sel.title, inv_sel.release_year \n" +
            "FROM store \n" +
            "INNER JOIN address ON store.address_id = address.address_id \n" +
            "INNER JOIN city ON address.city_id = city.city_id \n" +
            "INNER JOIN (select inventory.inventory_id, inventory.store_id, film.title, film.release_year FROM inventory INNER JOIN film on inventory.film_id = film.film_id) AS inv_sel ON store.store_id = inv_sel.store_id \n" +
            "WHERE address.address > 'M'";
    private final static String SELECT6 = "SELECT address.address, address.district, inv_sel.inventory_id, inv_sel.title, inv_sel.release_year \n" +
            "FROM store \n" +
            "INNER JOIN address ON store.address_id = address.address_id \n" +
            "INNER JOIN city ON address.city_id = city.city_id \n" +
            "INNER JOIN (select inventory.inventory_id, inventory.store_id, film.title, film.release_year FROM inventory INNER JOIN film on inventory.film_id = film.film_id) AS inv_sel ON store.store_id = inv_sel.store_id \n" +
            "WHERE ((address.address > 'M') AND (address.deleted IS FALSE))";

    private  TestUtil testUtil;

    @Before
    public void setUp() throws Exception {
        testUtil = new TestUtil();
        testUtil.createTablesDvdrental();

    }

    @Test
    public void testResolveSelectItems() throws Exception {
        String[] primaryKeys = new String[] { "inventory_select.inventory_id",
                                                "address.address_id",
                                                "store.store_id",
                                                "city.city_id",
                                                "inv_sel.film_id" };
        verifySelect(SELECT1, primaryKeys);
    }

    @Test
    public void testResolveSelectItems2() throws Exception {
        String[] primaryKeys = new String[] { "inventory_select.inventory_id",
                "address.address_id",
                "store.store_id",
                "city.city_id",
                "inv_sel.film_id" };
        verifySelect(SELECT2, primaryKeys);
    }

    @Test
    public void testResolveSelectItems3() throws Exception {
        String[] primaryKeys = new String[] { "inventory_select.inventory_id",
                "address.address_id",
                "store.store_id",
                "city.city_id",
                "inv_sel.film_id" };
        verifySelect(SELECT3, primaryKeys);
    }

    @Test
    public void testResolveSelectItems4() throws Exception {
        String[] primaryKeys = new String[] { "inv_sel.inventory_id",
                                                "address.address_id",
                                                "store.store_id",
                                                "city.city_id",
                                                "inv_sel.film_id" };
        verifySelect(SELECT4, primaryKeys);
    }


    @Test
    public void testResolveSelectItems5() throws Exception {
        String[] primaryKeys = new String[] { "inv_sel.inventory_id",
                                                "address.address_id",
                                                "store.store_id",
                                                "inv_sel.film_id" };
        PlainSelect ps = verifySelect(SELECT5, primaryKeys);
        List<String> syncTables = new ArrayList<String>();
        syncTables.add("address");
        // TODO: needs a statement.
        //List<String> errorMessages = ValidateSync.validateSyncConventions("sourceFile", ps, syncTables);
        //Assert.assertEquals(1, errorMessages.size());

    }

    @Test
    public void testResolveSelectItems6() throws Exception {
        String[] primaryKeys = new String[] { "inv_sel.inventory_id",
                "address.address_id",
                "store.store_id",
                "inv_sel.film_id" };
        PlainSelect ps = verifySelect(SELECT6, primaryKeys);
        List<String> syncTables = new ArrayList<String>();
        syncTables.add("address");

        // Needs to be a statement:
        //List<String> errorMessages = ValidateSync.validateSyncConventions("source file", ps, syncTables);
        //Assert.assertTrue(errorMessages.isEmpty());

    }

    private PlainSelect verifySelect(String select, String[] primaryKeys) throws Exception {
        PlainSelect ps = TestUtil.parsePlainSelect(select);
        System.out.println("primary keys:");
        List<String> syncTables = new ArrayList<String>();
        syncTables.add("address");
        return ps;
    }

    private static boolean matchKey(ColumnInfo columnInfo, String key) {
        String[] keyparts = key.split("\\.");
        return columnInfo.getTableName().equals(keyparts[0]) && columnInfo.getColumnName().equals(keyparts[1]);
    }

    private static boolean matchAnyKey(ColumnInfo columnInfo, String[] keys) {
        return Arrays.stream(keys).filter(key -> matchKey(columnInfo, key)).findAny().isPresent();
    }

    private static boolean matchAllKeys(List<ColumnInfo> columnInfoList, String[] keys) {
        return columnInfoList.stream().filter(columnInfo -> matchAnyKey(columnInfo, keys)).findAny().isPresent();
    }
}
