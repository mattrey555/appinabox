package com.apptechnik.appinabox.engine.parser;
import org.junit.Test;
import org.junit.Before;
import org.junit.Assert;

public class TestStatementParser {
	private TestUtil testUtil;

	@Before
	public void setUp() throws Exception {
		testUtil = new TestUtil();
		testUtil.createTablesStores();
	}

 	@Test	
	public void testTables() {
		Assert.assertNotNull(testUtil.getTableInfoMap().getTableInfo("stores"));
		Assert.assertNotNull(testUtil.getTableInfoMap().getTableInfo("purchases"));
		Assert.assertNotNull(testUtil.getTableInfoMap().getTableInfo("customers"));
		Assert.assertNotNull(testUtil.getTableInfoMap().getTableInfo("items"));
		Assert.assertNotNull(testUtil.getTableInfoMap().getTableInfo("users"));
	}
}
