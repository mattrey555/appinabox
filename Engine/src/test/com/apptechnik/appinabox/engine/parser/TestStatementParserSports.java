package com.apptechnik.appinabox.engine.parser;

import com.apptechnik.appinabox.engine.parser.json.ParseJSONMapExpression;
import com.apptechnik.appinabox.engine.parser.pojos.*;
import com.apptechnik.appinabox.engine.parser.query.SelectTableUtils;
import com.apptechnik.appinabox.engine.codegen.util.SQLFile;
import com.apptechnik.appinabox.engine.util.CheckedConsumer;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TestStatementParserSports {
	private TestUtil testUtil;
	private ParameterMap parameterMap;

	@Before
	public void setUp() throws Exception {
		testUtil = new TestUtil();
		testUtil.createTables("sports_create_tables.sql");
		testUtil.getTableInfoMap().resolveAllChildReferences();
	}

	@Test
	public void testTables() {
		TableInfoMap tableInfoMap = testUtil.getTableInfoMap();
		long count = tableInfoMap.getTableInfoList().stream().filter(t -> t.getPrimaryKey() != null).count();
		Assert.assertEquals(96, count);
	}

	@Test
	public void testSelect() throws Exception {
		String sql = "select full_name, american_football_action_participants.* " +
				"from display_names " +
				"inner join american_football_action_participants " +
				"on display_names.id = american_football_action_participants.person_id";
		String jsonFormat = "{ full_name, plays : { american_football_action_participants.* }}";
		testSelect(sql, jsonFormat);
	}

	public ColumnsAndKeys testSelect(String sql, String jsonFormat) throws Exception {
		SQLFile sqlFile = new SQLFile(sql, true);
		TypeMap fixedParamTypeMap = new TypeMap(sqlFile);
		Map<Statement, ParameterMap> statementParameterMapMap = ParameterMap.applyTypes(sqlFile.getStatementList(), testUtil.getTableInfoMap());
		statementParameterMapMap.values().stream().forEach(CheckedConsumer.wrap(paramMap -> paramMap.applyFixedParamTypes(fixedParamTypeMap)));
		Assert.assertEquals(1, statementParameterMapMap.size());
		Tree<QueryFieldsNode> jsonTree = ParseJSONMapExpression.parse(jsonFormat, new QueryFieldsNode.Factory());
		jsonTree.get().setName("football_plays");
		FieldReferenceNode.validate(jsonTree);
		Select select = (Select) sqlFile.getStatementList().get(0);
		PlainSelect plainSelect = (PlainSelect) select.getSelectBody();

		parameterMap = statementParameterMapMap.get(select);

		// TableInfoMap is map of tables referenced in this select statement.
		TableInfoMap selectTableInfoMap = SelectTableUtils.selectTables(select, testUtil.getTableInfoMap());
		ExpressionType.getNamedParameterTypes(select, selectTableInfoMap, testUtil.getTableInfoMap());
		ColumnsAndKeys columnsAndKeys = SelectTableUtils.resolveColumns(plainSelect, jsonTree, parameterMap, selectTableInfoMap);
		validatePrimaryKeyNaming(columnsAndKeys.getPrimaryKeyList());
		return columnsAndKeys;
	}

	private void validatePrimaryKeyNaming(Collection<ColumnInfo> primaryKeys) {
		List<String> names =
				primaryKeys.stream()
						.map(key -> key.getAlias() != null ? key.getAlias().getName() : key.getColumnName()).collect(Collectors.toList());
		for (int i = 0; i < names.size(); i++) {
			String name = names.get(i);
			for (int j = 0; j < names.size(); j++) {
				if (i != j) {
					String otherName = names.get(j);
					Assert.assertNotEquals(otherName, name);
				}
			}
		}
	}

	@Test
	public void testSelectTableAlias() throws Exception {
		String sql = "SELECT full_name, participants.*,plays.* FROM american_football_action_plays AS plays\n" +
				"LEFT OUTER JOIN american_football_action_participants AS participants\n" +
				"ON plays.id=american_football_action_play_id \n" +
				"LEFT OUTER JOIN display_names\n" +
				"ON display_names.id = participants.person_id ORDER BY play_type;";
		String jsonFormat = "{ full_name, plays : { plays.* }}";
		ColumnsAndKeys columnsAndKeys = testSelect(sql, jsonFormat);
		List<ColumnInfo> keyList = new ArrayList<ColumnInfo>(columnsAndKeys.getPrimaryKeyList());
		Assert.assertEquals(2, keyList.size());
		Assert.assertEquals("id", keyList.get(0).getColumnName());
		Assert.assertEquals("display_names_id", keyList.get(0).getAliasName());
		Assert.assertEquals("display_names", keyList.get(0).getTableName());
		Assert.assertEquals("id", keyList.get(1).getColumnName());
		Assert.assertEquals("plays_id", keyList.get(1).getAliasName());
		Assert.assertEquals("plays", keyList.get(1).getTableName());
	}

	@Test
	public void testTableAlias() throws Exception {
		String sql = "SELECT count(plays.id) as count FROM american_football_action_plays AS plays";
		SQLFile sqlFile = new SQLFile(sql, true);
		TypeMap fixedParamTypeMap = new TypeMap(sqlFile);
		Select select = (Select) sqlFile.getStatementList().get(0);
		TableInfoMap selectTableInfoMap = SelectTableUtils.selectTables(select, testUtil.getTableInfoMap());
	}

	@Test
	public void testSelectGroupBy() throws Exception {
		String sql = "SELECT full_name, participants.*, count(plays.id) as count FROM american_football_action_plays AS plays\n" +
				"LEFT OUTER JOIN american_football_action_participants AS participants\n" +
				"ON plays.id=american_football_action_play_id \n" +
				"LEFT OUTER JOIN display_names\n" +
				"ON display_names.id = participants.person_id GROUP BY full_name";
		String jsonFormat = "{ full_name, participants : { participants.* }, plays : { count }}";
		ColumnsAndKeys columnsAndKeys = testSelect(sql, jsonFormat);
		List<ColumnInfo> keyList = new ArrayList<ColumnInfo>(columnsAndKeys.getPrimaryKeyList());
		Assert.assertEquals(2, keyList.size());
		Assert.assertEquals("id", keyList.get(0).getColumnName());
		Assert.assertEquals("display_names_id", keyList.get(0).getAliasName());
		Assert.assertEquals("display_names", keyList.get(0).getTableName());
		Assert.assertEquals("id", keyList.get(1).getColumnName());
		Assert.assertEquals("participants_id", keyList.get(1).getAliasName());
		Assert.assertEquals("participants", keyList.get(1).getTableName());
	}

	@Test
	public void testSelectIn() throws Exception {
		String sql = "SELECT * from american_football_action_participants AS participants where participants.person_id in (:person_ids)";
		String jsonFormat = "{ participants.* }";
		ColumnsAndKeys columnsAndKeys = testSelect(sql, jsonFormat);
		ParameterMap.ParamData paramData = parameterMap.get("person_ids");

	}
}
