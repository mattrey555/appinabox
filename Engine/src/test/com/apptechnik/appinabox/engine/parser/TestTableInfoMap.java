package com.apptechnik.appinabox.engine.parser;

import com.apptechnik.appinabox.engine.parser.create.StatementParser;
import com.apptechnik.appinabox.engine.util.FileUtil;
import net.sf.jsqlparser.parser.CCJSqlParserManager;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestTableInfoMap {
    private TableInfoMap tableInfoMap;

    @Before
    public void setup() throws Exception {
        StatementParser statementParser = new StatementParser();
        statementParser.parse(FileUtil.getResource(TestSelectItemUtils.class, "stores_create_tables.sql"));
        tableInfoMap = statementParser.getTableInfoMap();
    }

    @Test
    public void testDropIndex() {
        Assert.assertTrue(tableInfoMap.dropIndex("auth_tokens_index"));
    }
}
