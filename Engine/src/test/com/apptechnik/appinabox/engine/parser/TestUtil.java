package com.apptechnik.appinabox.engine.parser;
import java.util.List;

import com.apptechnik.appinabox.engine.parser.create.StatementParser;
import com.apptechnik.appinabox.engine.util.FileUtil;
import net.sf.jsqlparser.parser.CCJSqlParser;
import net.sf.jsqlparser.parser.StringProvider;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.Statements;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.parser.CCJSqlParserManager;
import net.sf.jsqlparser.statement.select.Join;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.select.SelectItem;
import org.junit.Assert;

public class TestUtil {
	private TableInfoMap tableInfoMap;
	private CCJSqlParserManager parseManager;
	public TableInfoMap getTableInfoMap() {
		return tableInfoMap;
	}
	public CCJSqlParserManager getParseManager() {
		return parseManager;
	}

	/**
	 * Create a set of tables useful for basic tests.
	 * @throws Exception
	 */
	public void createTables(String createTablesRes) throws Exception {
		parseManager = new CCJSqlParserManager();
		String createTables = FileUtil.getResource(TestUtil.class,createTablesRes);
		StatementParser parser = new StatementParser();
		parser.parse(createTables);
		tableInfoMap = parser.getTableInfoMap();
		System.out.println("table info map");
		printTableInfoMap(tableInfoMap);
	}

	public void createTablesDvdrental() throws Exception {
		createTables("dvdrental_create_tables.sql");
	}

	public void createTablesStores() throws Exception {
		createTables("stores_create_tables.sql");
	}

	public static Select parseSelect(String selectStr) throws Exception {
		CCJSqlParser parser = new CCJSqlParser(new StringProvider(selectStr));
		Statements statements = parser.Statements();
		Statement statement = statements.getStatements().get(0);
		Assert.assertTrue(statement instanceof Select);
		return (Select) statement;
	}

	public static PlainSelect parsePlainSelect(String selectStr) throws Exception {
		return (PlainSelect) (parseSelect(selectStr).getSelectBody());
	}

	public static void printTableInfoMap(TableInfoMap map) {
		for (String key : map.keySet()) {
			System.out.println("table name: " + key + " table info: " + map.getTableInfo(key));
		}
	}

	public static void printSelectItemGroups(List<List<SelectItem>> selectItemGroups) {
		for (List<SelectItem> selectItemGroup : selectItemGroups) {
			System.out.println("select item group: ");
			for (SelectItem selectItem : selectItemGroup) {
				System.out.println(selectItem);
			}
		}
	}

	public static void printSelect(PlainSelect select) {
		if (select.getJoins() != null) {
			System.out.println("joins: " + select.getJoins().size());
			for (Join join : select.getJoins()) {
				System.out.println(join);
			}
		}
		if (select.getWhere() != null) {
			System.out.println("where: " + select.getWhere());
		}
		if (select.getFromItem() != null) {
			System.out.println("fromItem: " + select.getFromItem());
			System.out.println("fromItem class: " + select.getFromItem().getClass().getSimpleName());
			System.out.println("alias: " + select.getFromItem().getAlias());
			System.out.println("pivot: " + select.getFromItem().getPivot());
		}
	}
}
