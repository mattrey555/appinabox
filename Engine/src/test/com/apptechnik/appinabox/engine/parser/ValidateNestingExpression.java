package com.apptechnik.appinabox.engine.parser;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.parser.pojos.JsonFieldReference;
import com.apptechnik.appinabox.engine.parser.pojos.FieldReferenceNode;
import com.apptechnik.appinabox.engine.parser.pojos.QueryFieldsNode;
import com.apptechnik.appinabox.engine.parser.pojos.Tree;
import com.apptechnik.appinabox.engine.parser.query.SelectItemUtils;
import com.apptechnik.appinabox.engine.parser.query.SelectTableUtils;
import com.apptechnik.appinabox.engine.util.StringUtil;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.statement.Statement;
import net.sf.jsqlparser.statement.select.*;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Apply the following rules to a parsed JsonConversionExpression against its parsed
 * SQL select atatement.
 * identifiers of the form <table>.<field> must:
 *  - match a table in the SELECT expression
 *  - reference a field within that table.
 * all identifiers of the form table.* must match table.* in the SELECT expression
 * identifiers in nested expressions must
 *  - be depth-first ordered in the form of their JOIN statements. if identifier a precedes identifier
 *    b in the expression, its referencing table must precede in the JOIN statement.
 *  identifiers of the form <field> must be referenced in the SELECT field list which will
 *  confirm their uniqueness.
 *  warn if a field is duplicated.  It will be removed during execution.
 *  Return a list of strings describing the errors in detail
 */
public class ValidateNestingExpression {
    public List<ErrorMessage> validate(TableInfoMap tableInfoMap,
                                       Tree<QueryFieldsNode> selectItemTree,
                                       Statement statement) throws Exception {
        PlainSelect plainSelect = (PlainSelect) statement;
        List<ErrorMessage> errorMessages = new ArrayList<ErrorMessage>();
        selectItemTree.traverse(new InSelectItems(plainSelect.getSelectItems(), errorMessages));
        selectItemTree.traverse(new VerifyJoinOrder(tableInfoMap, plainSelect, errorMessages));
        return errorMessages;
    }

    public class InSelectItems implements Tree.ConsumerException<Tree<QueryFieldsNode>> {
        private List<SelectItem> selectItems;
        private List<ErrorMessage> errorMessages;

        public InSelectItems(List<SelectItem> selectItems,
                             List<ErrorMessage> errorMessages) {
            this.selectItems = selectItems;
            this.errorMessages = errorMessages;
        }

        public void accept(Tree<QueryFieldsNode> selectItemTree) {
            for (JsonFieldReference targetItem : selectItemTree.get().getFieldReferenceList()) {
                List<SelectItem> matchingItems = filterList(selectItems, targetItem);
                if (matchingItems.isEmpty()) {
                    String msg = "no matching select item for " + targetItem;
                    ErrorMessage err = new ErrorMessage(ErrorMessage.Severity.ERROR, msg);
                    errorMessages.add(err);
                } else if (matchingItems.size() > 1) {
                    String msg = "duplicate matching items for " + targetItem + " : " + StringUtil.concat(matchingItems, ",");
                    ErrorMessage err = new ErrorMessage(ErrorMessage.Severity.ERROR, msg);
                    errorMessages.add(err);
                }
            }
        }
    }

    /**
     * Search for a table which matches a JSON mapping field. The JSON mapping field is field|table.field|table.*
     * @param tableInfoMap
     * @param selectItemList
     * @param fieldReference
     * @return
     */
    public static List<TableInfo> tableInfoFromSelectItem(TableInfoMap tableInfoMap,
                                                          List<SelectItem> selectItemList,
                                                          JsonFieldReference fieldReference) throws Exception {
        if (fieldReference.getTableName() != null) {
            return Collections.singletonList(tableInfoMap.getTableInfo(fieldReference.getTableName()));
        } else {
            // map either the column name or the alias to a select item in the select item clause.
            // assume filterList returna s single element
            Expression expr = ((SelectExpressionItem) filterList(selectItemList, fieldReference).get(0)).getExpression();
            return new ArrayList<TableInfo>(SelectItemUtils.getTables(tableInfoMap, expr));
        }
    }

    public class VerifyJoinOrder implements Tree.ConsumerException<Tree<QueryFieldsNode>> {
        private PlainSelect plainSelect;
        private TableInfoMap tableInfoMap;
        private List<ErrorMessage> errorMessages;

        public VerifyJoinOrder(TableInfoMap tableInfoMap,
                               PlainSelect plainSelect,
                               List<ErrorMessage> errorMessages) {
            this.plainSelect = plainSelect;
            this.tableInfoMap = tableInfoMap;
            this.errorMessages = errorMessages;
        }

        public void accept(Tree<QueryFieldsNode> selectItemTree) {
            for (JsonFieldReference item : selectItemTree.get().getFieldReferenceList()) {
                try {
                    List<TableInfo> tableInfoList = tableInfoFromSelectItem(tableInfoMap, plainSelect.getSelectItems(), item);
                    for (TableInfo tableInfo : tableInfoList) {
                        while (!tableInfoList.isEmpty() && !tableInfoList.get(0).getTable().equals(tableInfo.getTable())) {
                            tableInfoList = tableInfoList.subList(1, tableInfoList.size());
                        }
                        if (tableInfoList.isEmpty()) {
                            String msg = "element " + item + " out of order in join list";
                            errorMessages.add(new ErrorMessage(ErrorMessage.Severity.ERROR, msg));
                        }
                    }
                } catch (Exception ex) {
                    String msg = "threw an exception trying to verify the join order for " + item + ": " + ex.getMessage();
                    errorMessages.add(new ErrorMessage(ErrorMessage.Severity.EXCEPTION, msg));
                }
            }
        }
    }

    private static List<SelectItem> filterList(List<SelectItem> selectItems, JsonFieldReference targetItem) {
        return selectItems.stream().filter(item -> matchItem(item, targetItem)).collect(Collectors.toList());
    }
    /**
     * A select item can refer to:table.field | field | alias.
     * We can't use equals, because the SelectItem in the JSON expression can refer to a column alias.
     */
    private static boolean matchItem(SelectItem a, JsonFieldReference b)  {
        if (a instanceof AllTableColumns) {
            if (Constants.SQLKeywords.ALL_COLUMNS.equals(b.getColumnName())) {
                return ((AllTableColumns) a).getTable().getName().equals(b.getTableName());
            }
        }
        Column aCol = (Column) ((SelectExpressionItem) a).getExpression();
        if ((aCol.getTable() == null) || aCol.getTable().getName().equals(b.getTableName())) {
            return aCol.getName(false).equals(b.getColumnName());
        }
        return false;
    }
}
