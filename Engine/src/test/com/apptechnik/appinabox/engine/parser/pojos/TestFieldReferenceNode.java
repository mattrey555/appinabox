package com.apptechnik.appinabox.engine.parser.pojos;

import com.apptechnik.appinabox.engine.parser.json.ParseJSONMapExpression;
import org.junit.Assert;
import org.junit.Test;

public class TestFieldReferenceNode {
    private final String JSON = "{ store.store_id, city.city, address.address, address.district, " +
                                " staff : { first_name, last_name }, " +
                                " rental : { inventory.inventory_id, film.title, film.release_year, return_date }}";
    @Test
    public void testParseAndPrint() throws Exception {
        Tree<QueryFieldsNode> tree = ParseJSONMapExpression.parse(JSON);
        tree.get().setName("foo");
        String s = FieldReferenceNode.createString(tree);
        Assert.assertEquals("foo : { store.store_id, city.city, address.address, address.district, staff : { first_name, last_name, rental : { inventory.inventory_id, film.title, film.release_year, return_date", s);
        System.out.println("result: " + s);
    }
}
