package com.apptechnik.appinabox.engine.parser.pojos;

import com.apptechnik.appinabox.engine.exceptions.HeaderFormatException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestParameterMap {
    private TypeMap paramTypeMap;


    @Before
    public void setup() throws Exception {
        paramTypeMap = new TypeMap(new ArrayList<String>());
    }
    @Test
    public void testHeaders() throws HeaderFormatException {
        ParameterMap paramMap = new ParameterMap();
        List<String> headers = new ArrayList<String>();
        headers.add("Authorization: bearer ${bearer}");
        headers.add("Modified-Since: modified_since");
        paramMap.addHeaderParams(headers, paramTypeMap);
        ParameterMap.ParamData paramData = paramMap.get("bearer");
        Assert.assertEquals(ParameterMap.ParamSource.HTTP_HEADER, paramData.getSource());
        Assert.assertEquals("Authorization", paramData.getTag());
        Assert.assertEquals("bearer ${bearer}", paramData.getVarFormat());

        paramData = paramMap.get("modified_since");
        Assert.assertEquals(ParameterMap.ParamSource.HTTP_HEADER, paramData.getSource());
        Assert.assertEquals("Modified-Since", paramData.getTag());
    }
}
