package com.apptechnik.appinabox.engine.parser.pojos;

import com.apptechnik.appinabox.engine.util.StringUtil;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class TreeTest {

    private class Node {
        public final int value;
        public final int[] childValues;

        public Node(int value, int[] childValues) {
            this.value = value;
            this.childValues = childValues;
        }
    }

    private Node[] nodes =
            { new Node(1, new int[] { 2, 3, 4}),
              new Node(2, new int[] { 5, 6 }),
              new Node(3, new int[] { 7, 8 }),
              new Node(4, null),
              new Node(5, null),
              new Node(6, null),
              new Node(7, null),
              new Node(8, null) };

    private Node[] nodesNotAllTree =
            { new Node(1, new int[] { 2, 3, 4}),
                    new Node(2, new int[] { 5, 6 }),
                    new Node(3, new int[] { 7, 8 }),
                    new Node(4, null),
                    new Node(5, null),
                    new Node(6, null),
                    new Node(7, null),
                    new Node(8, null),
                    new Node(9, null),
                    new Node(10, null)};

    private static class NodeParentChild implements Tree.ParentChild<Node> {
        public boolean isParent(Node a, Node b) {
            if (a.childValues != null) {
                for (int i = 0; i < a.childValues.length; i++) {
                    if (a.childValues[i] == b.value) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    private static class CheckNodeParentChild implements Tree.ConsumerException<Tree<Node>> {
        public void accept(Tree<Node> tree) {
            for (Tree<Node> child : tree.getChildren()) {
                boolean found = true;
                for (int i = 0; i < tree.get().childValues.length; i++) {
                    if (child.get().value == tree.get().childValues[i]) {
                        return;
                    }
                }
                Assert.fail();
            }
        }
    }

    @Test
    public void testListToTree() throws Exception {
        List<Node> nodeList = new LinkedList<Node>(Arrays.asList(nodes));
        Tree<Node> tree = Tree.listToTree(nodeList, new NodeParentChild());
        tree.traverse(node -> System.out.println("value: " + node.get().value));
        Assert.assertTrue(nodeList.isEmpty());
        tree.traverse(new CheckNodeParentChild());
    }

    @Test
    public void testNotAllListToTree() throws Exception {
        List<Node> nodeList = new LinkedList<Node>(Arrays.asList(nodesNotAllTree));
        Tree<Node> tree = Tree.listToTree(nodeList, new NodeParentChild());
        tree.traverse(node -> System.out.println("value: " + node.get().value));
        Assert.assertEquals(2, nodeList.size());
        List<String> untreedValues = nodeList.stream().map(node -> node.value + " ").collect(Collectors.toList());
        System.out.println("untreed values: " + StringUtil.concat(untreedValues, " "));
        tree.traverse(new CheckNodeParentChild());
    }

    @Test
    public void testTraverseTree() throws Exception {
        List<Node> nodeList = new LinkedList<Node>(Arrays.asList(nodes));
        Tree<Node> treeA = Tree.listToTree(nodeList, new NodeParentChild());
        Tree<Node> treeB = treeA.traverseFn(node -> new Node(node.value + node.value, node.childValues));
        boolean match = testTrees(treeA, treeB, new TestCompare());
        Assert.assertTrue(match);
    }

    @Test
    public void testTraverseTreeToList() throws Exception {
        List<Node> nodeList = new LinkedList<Node>(Arrays.asList(nodes));
        Tree<Node> treeA = Tree.listToTree(nodeList, new NodeParentChild());
        List<Node> list = treeA.traverseFnToList(node -> new Node(node.value + node.value, node.childValues));
        TestListCompare testListCompare = new TestListCompare(list, new TestCompare());
        Assert.assertTrue(treeA.traversePredicateNode(testListCompare));
    }

    private  class TestListCompare<T, U> implements Predicate<T> {
        private List<U> list;
        private int index;
        private CompareNodes<T, U> compareNodes;

        public TestListCompare(List<U> list, CompareNodes<T, U> compareNodes) {
            this.list = list;
            this.index = 0;
            this.compareNodes = compareNodes;
        }

        @Override
        public boolean test(T data) {
            boolean result = this.compareNodes.compare(data, this.list.get(this.index));
            this.index++;
            return result;
        }
    }

    private class TestCompare implements CompareNodes<Node, Node> {
        public boolean compare(Node a, Node b) {
            return b.value == a.value + a.value;
        }
    }

    private interface CompareNodes<T, U> {
        boolean compare(T a, U b);
    }

    public <T, U> boolean testTrees(Tree<T> nodeA, Tree<U> nodeB, CompareNodes compareNodes) {
        if (!compareNodes.compare(nodeA.get(), nodeB.get())) {
            return false;
        }
        if (nodeA.getChildCount() != nodeB.getChildCount()) {
            return false;
        }
        for (int i = 0; i < nodeA.getChildCount(); i++) {
            if (!testTrees(nodeA.getChildAt(i), nodeB.getChildAt(i), compareNodes)) {
                return false;
            }
        }
        return true;
    }
}
