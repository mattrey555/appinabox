package com.apptechnik.appinabox.engine.parser.query;

import com.apptechnik.appinabox.engine.codegen.util.SQLFile;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.parser.ASTNodeAccess;
import net.sf.jsqlparser.parser.SimpleNode;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.select.FromItem;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import org.junit.Test;

import java.util.List;

public class TestNodeUtils {
    String sql = "SELECT" +
            "       events.event_key," +
            "       baseball_pitching_stats.event_credit," +
            "       persons.id as person_id," +
            "       display_names.full_name" +
            " FROM" +
            "       stats, events, baseball_pitching_stats, persons, display_names" +
            " WHERE" +
            "       stats.stat_repository_type = 'baseball_pitching_stats'" +
            "       AND stats.stat_repository_id = baseball_pitching_stats.id" +
            "       AND events.event_key like '%mlb%'" +
            "       AND stats.stat_coverage_id = events.id" +
            "       AND stats.stat_holder_type = 'persons'" +
            "       AND persons.id = stats.stat_holder_id" +
            "       AND baseball_pitching_stats.event_credit IS NOT NULL" +
            "       AND display_names.entity_id = stats.stat_holder_id" +
            "       AND display_names.entity_type = 'persons'" +
            " ORDER BY" +
            "       events.event_key";

    @Test
    public void testNodeUtils() throws Exception {
        SQLFile sqlFile = new SQLFile(sql, true);
        Select select = (Select) sqlFile.getStatementList().get(0);
        ASTNodeAccess node = ((PlainSelect) select.getSelectBody());
        List<Table> tableList = NodeUtils.recursivelyFilterByTypes(node, new Class[] { FromItem.class, Table.class });
        tableList.stream().forEach(table -> System.out.println(table));
        List<Expression> expressionList = NodeUtils.recursivelyFilterByType(node, Expression.class);
        expressionList.stream().forEach(expr -> System.out.println(expr));
        List<Column> columnList = NodeUtils.recursivelyFilterByType(node, Column.class);
        columnList.stream().forEach(col -> System.out.println(col));

    }
}
