package com.apptechnik.appinabox.engine.util;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.pojos.*;
import com.apptechnik.appinabox.engine.codegen.pojos.java.*;
import com.apptechnik.appinabox.engine.codegen.util.CodegenUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CodegenUtilTest {
    private CodeGen codegen;
    @Before
    public void setup() {
        codegen = new JavaCodeGen();
    }
    @Test
    public void testForIterator() {
        ForIteration f = codegen.forIter(codegen.classType("foo"), codegen.var("bar"), codegen.var("list"));
        Assert.assertEquals("for (foo bar : list) ", f.toString());
    }

    @Test
    public void testAssignmentStatement() {
        CodeExpression expr = codegen.binaryExpr(Constants.JavaOperators.PLUS, codegen.var("a"), codegen.var("b"));
        AssignmentStatement assignment = codegen.assignStmt(codegen.var("foo"), expr);
        Assert.assertEquals("foo = a + b", assignment.toString());
        TypedAssignmentStatement typedAssignment =
                codegen.typedAssignStmt(codegen.classType("bar"), codegen.var("foo"), expr);
        Assert.assertEquals("bar foo = a + b", typedAssignment.toString());
    }


    @Test
    public void testAccessorCall() {
        MethodCall call = CodegenUtil.accessorCall(codegen, codegen.var("o"), "value");
        Assert.assertEquals("o.getValue()", call.toString());
    }

    @Test
    public void testParamDeclaration() {
        ParameterDeclaration decl = codegen.paramDecl(codegen.classType("type"), codegen.var("var"));
        Assert.assertEquals("type var", decl.toString());
    }

    @Test
    public void testVarDeclaration() {
        VariableDeclaration decl = codegen.varDecl(codegen.classType("type"), codegen.var("var"));
        Assert.assertEquals("type var", decl.toString());
    }

    @Test
    public void testMemberDeclaration() {
        MemberDeclaration decl = codegen.memberDecl(Constants.JavaKeywords.PUBLIC, false,
                                                    codegen.classType("type"), codegen.var("var"));
        Assert.assertEquals("public type var", decl.toString());
        MemberDeclaration decl2 = codegen.memberDecl(codegen.classType("type"), codegen.var("var"));
        Assert.assertEquals("private type var", decl2.toString());
        CodeExpression expr = codegen.binaryExpr(Constants.JavaOperators.PLUS, codegen.var("a"), codegen.var("b"));
        MemberDeclaration decl3 = codegen.memberDecl(decl2, expr);
        Assert.assertEquals("private type var = a + b", decl3.toString());
    }

    @Test
    public void testConstructorDeclaration() {
        ConstructorDeclaration decl = codegen.constructorDecl("constructor");
        Assert.assertEquals("public constructor()", decl.toString());
    }

    @Test
    public void testAccessorDefinition() {
        FunctionDefinition decl = CodegenUtil.accessorDefinition(codegen, codegen.classType("foo"), codegen.var("field"));
        Assert.assertEquals("public foo getField() {\nreturn field;\n}", decl.toString());
    }

    @Test
    public void testSetterDefinition() {
        FunctionDefinition decl = CodegenUtil.setterDefinition(codegen, codegen.classType("foo"), codegen.var("field"));
        Assert.assertEquals("public void setField(foo field) {\nthis.field = field;\n}", decl.toString());
    }

    @Test
    public void testAccessorFromSQLType() throws Exception {
        FunctionDefinition decl = CodegenUtil.accessorDeclarationFromSQLType(codegen,"field", Constants.SQLTypes.INTEGER, false);
        Assert.assertEquals("public int getField() {\nreturn field;\n}", decl.toString());
        FunctionDefinition decl2 = CodegenUtil.accessorDeclarationFromSQLType(codegen,"field", Constants.SQLTypes.INTEGER, true);
        Assert.assertEquals("public Integer getField() {\nreturn field;\n}", decl2.toString());
    }

    @Test
    public void testSetterFromSQLType() throws Exception {
        FunctionDefinition decl = CodegenUtil.setterDefinition(codegen,"field", Constants.SQLTypes.INTEGER, false);
        Assert.assertEquals("public void setField(int field) {\nthis.field = field;\n}", decl.toString());
        FunctionDefinition decl2 = CodegenUtil.setterDefinition(codegen,"field", Constants.SQLTypes.INTEGER, true);
        Assert.assertEquals("public void setField(Integer field) {\nthis.field = field;\n}", decl2.toString());
    }

    @Test
    public void testMemberFromSQLType() throws Exception {
        MemberDeclaration decl = CodegenUtil.memberDeclarationFromSQLType(codegen,"field", Constants.SQLTypes.INTEGER, true, Constants.JavaKeywords.PRIVATE);
        Assert.assertEquals("private int field", decl.toString());
        MemberDeclaration decl2 = CodegenUtil.memberDeclarationFromSQLType(codegen,"field", Constants.SQLTypes.INTEGER, false, Constants.JavaKeywords.PRIVATE);
        Assert.assertEquals("private Integer field", decl2.toString());

    }

    @Test
    public void testNameFromField() {
        Assert.assertEquals("getFoo", CodegenUtil.accessorNameFromField("foo"));
        Assert.assertEquals("setFoo", CodegenUtil.setterNameFromField("foo"));
    }
}
