package com.apptechnik.appinabox.engine.util;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

/**
 * Tests for file utilities.  You'd be amazed how often basic stuff breaks
 *
 */
public class TestFileUtil {

    @Test
    public void testReadFromResource() throws Exception {
        String[] lines = FileUtil.getResourceStrings(TestFileUtil.class, "create_tables.sql");
        Assert.assertEquals("CREATE TABLE stores (store_id SERIAL,", lines[0]);
        String s = FileUtil.getResource(TestFileUtil.class, "create_tables.sql");
        Assert.assertTrue(s.contains("CREATE TABLE stores (store_id SERIAL,"));
    }

    @Test
    public void testCheckDirectory() {
        try {
            FileUtil.checkDirectory("test directory", new File("."));
        } catch (Exception ex) {
            Assert.fail("should not fail on .");
        }
        try {
            FileUtil.checkDirectory("test directory", new File("/dev/null/no_way"));
            Assert.fail("should throw an exception");
        } catch (Exception ex) {

        }

    }

    @Test
    public void testCreateFile() throws Exception {
        File file = new File("foo");
        File fullPathFile = new File(file.getAbsolutePath());
        FileUtil.checkCreateFile("test file", fullPathFile.getParent());
        FileUtil.checkDirectory("test directory", fullPathFile.getParent());
        FileOutputStream fos = new FileOutputStream(file);
        fos.write("this is a test".getBytes());
        fos.close();
        FileUtil.checkFile("test file", file.getName());
        FileUtil.checkCreateFile("test file", fullPathFile.getParent());
        String s = FileUtil.readFileToString(file);
        Assert.assertEquals("this is a test\n",s);
        List<String> list = FileUtil.readStringList(file);
        Assert.assertEquals(1, list.size());
        Assert.assertEquals("this is a test", list.get(0));
    }
}
