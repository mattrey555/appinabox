package com.apptechnik.appinabox.engine.util;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TestListUtil {

    @Test
    public void testExcept() {
        List<Integer> list = new ArrayList<Integer>();
        list.add(1);
        list.add(2);
        list.add(3);
        List<Integer> res = ListUtil.except(list, 2);
        Assert.assertEquals(3, list.size());
        Assert.assertEquals(2, res.size());
        Assert.assertEquals(Integer.valueOf(1), (Integer) res.get(0));
        Assert.assertEquals(Integer.valueOf(3), (Integer) res.get(1));
    }

    @Test
    public void testExceptCompare() {
        List<Integer> list = new ArrayList<Integer>();
        list.add(1);
        list.add(2);
        list.add(3);
        List<Integer> res = ListUtil.except(list, 2, new CompareInt());
        Assert.assertEquals(3, list.size());
        Assert.assertEquals(2, res.size());
        Assert.assertEquals(Integer.valueOf(1), (Integer) res.get(0));
        Assert.assertEquals(Integer.valueOf(3), (Integer) res.get(1));

    }

    private class CompareInt implements Comparator<Integer> {
        @Override
        public int compare(Integer a, Integer b) {
            return  a - b;
        }
    }
}
