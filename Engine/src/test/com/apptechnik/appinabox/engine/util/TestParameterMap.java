package com.apptechnik.appinabox.engine.util;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.codegen.util.SQLFile;
import com.apptechnik.appinabox.engine.exceptions.ParameterFormatException;
import com.apptechnik.appinabox.engine.exceptions.TypeMismatchException;
import com.apptechnik.appinabox.engine.parser.MapUpdateTree;
import com.apptechnik.appinabox.engine.parser.TestUtil;
import com.apptechnik.appinabox.engine.parser.pojos.*;
import com.apptechnik.appinabox.library.sql.NamedParamStatement;
import net.sf.jsqlparser.statement.Statement;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class TestParameterMap {

    // perform basic validation of the parameters parsed from the .SQL file comments
    // get the query, path, and header params, and hat they match and that their source types are set correctly.
    public void testSQLFile(TestUtil testUtil, String source) throws Exception {
        SQLFile sqlFile = new SQLFile(source, true);
        ParameterMap parameterMap = new ParameterMap(sqlFile.getComments());
        String url = sqlFile.getTag(Constants.Directives.URL);
        List<String> queryParams = parameterMap.keySet().stream()
                .filter(param -> url.contains("${" + param + "}"))
                .collect(Collectors.toList());
        List<String> headers = sqlFile.filterTag(Constants.Directives.HEADER);
        List<String> headerParams = headers.stream().map(header -> header.split(":")[1].trim()).collect(Collectors.toList());

        // validate the query parame.
        for (String param : queryParams) {
            ParameterMap.ParamData paramData = parameterMap.get(param);
            Assert.assertNotNull(paramData);
            boolean inUrl = paramData.getSource().equals(ParameterMap.ParamSource.URL_QUERY) ||
                            paramData.getSource().equals(ParameterMap.ParamSource.URL_PATH);
            Assert.assertTrue(inUrl);
            Assert.assertEquals(Constants.SQLTypes.NAMED_PARAMETER, paramData.getDataType().getDataType());
        }

        // validate the header params
        for (String param : headerParams) {
            ParameterMap.ParamData paramData = parameterMap.get(param);
            Assert.assertNotNull(paramData);
            Assert.assertEquals(ParameterMap.ParamSource.HTTP_HEADER, paramData.getSource());
            Assert.assertEquals(Constants.SQLTypes.NAMED_PARAMETER, paramData.getDataType().getDataType());
        }
        /*
        String varName = sqlFile.findTag(Constants.Directives.NAME_REQUEST);
        if (varName != null) {
            Tree<MergedFieldNode> jsonRequestTree = sqlFile.createJSONTree(varName,
                                                                            Constants.Directives.NAME_REQUEST,
                                                                            Constants.Directives.JSON_REQUEST,
                                                                            new MergedFieldNode.Factory());
            FieldReferenceNode.validateJsonTree(jsonRequestTree);

        }
         */
        // apply the fixed types.
        TypeMap fixedParamTypeMap = new TypeMap(sqlFile);
        Map<Statement, ParameterMap> statementParameterMapMap = ParameterMap.applyTypes(sqlFile.getStatementList(), testUtil.getTableInfoMap());
        // make sure that all the parameter types are defined or in the fixed type map.
        for (Statement statement : statementParameterMapMap.keySet()) {
            ParameterMap paramMap = statementParameterMapMap.get(statement);
            paramMap.applyFixedParamTypes(fixedParamTypeMap);
            for (String paramName : paramMap.keySet()) {
                ParameterMap.ParamData paramData = paramMap.getParamData(paramName);
                boolean sourceOk = paramData.getSource().equals(ParameterMap.ParamSource.QUERY) ||
                        paramData.getSource().equals(ParameterMap.ParamSource.VALUE);
                Assert.assertTrue(sourceOk);
                if (paramData.getDataType().equals(Constants.SQLTypes.NAMED_PARAMETER)) {
                    Assert.assertNotNull(fixedParamTypeMap.get(paramName));
                }
            }
            paramMap.applyFixedParamTypes(fixedParamTypeMap);
            for (String paramName : paramMap.keySet()) {
                Assert.assertNotEquals(Constants.SQLTypes.NAMED_PARAMETER,
                                       paramMap.getColDataType(paramName).getDataType());
            }
        }

        // make sure that the fixed types are applied correctly.
        for (ParameterMap paramMap : statementParameterMapMap.values()) {
            Assert.assertTrue(paramMap.filterByType(Constants.SQLTypes.NAMED_PARAMETER).isEmpty());
        }
    }

    public void recurseFiles(File file, Consumer<File> consumer) {
        if (file.isFile()) {
            consumer.accept(file);
        } else if (file.isDirectory()) {
            Arrays.stream(file.listFiles()).forEach(f -> recurseFiles(f, consumer));
        }
    }

    private class TestSQLFile implements Consumer<File> {
        private TestUtil testUtil;

        public TestSQLFile(String createTableFile) throws Exception {
            testUtil = new TestUtil();
            testUtil.createTables(createTableFile);
        }
        @Override
        public void accept(File f) {
            try {
                String s = FileUtil.readFileToString(f);
                testSQLFile(testUtil, s);
            } catch (Exception ex) {
                Assert.fail("reading resource " + f.getAbsolutePath());
            }
        }
    }

    @Test
    public void test1() throws Exception {
        String s = FileUtil.getResource(TestParameterMap.class, "sqlfiles/dvdrental/sql/GET/film_category_count.sql");
        TestUtil testUtil = new TestUtil();
        testUtil.createTables("dvdrental_create_tables.sql");
        testSQLFile(testUtil, s);
    }

    @Test
    public void testResources() throws Exception {
        recurseFiles(new File("src/test/resources/sqlfiles/dvdrental"), new TestSQLFile("dvdrental_create_tables.sql"));
    }

    @Test
    public void testReplaceDotParameters() {
        String sql = "SELECT *, t.foo FROM table where a = :c1.f1 AND b = :f2 AND c = :c2.f2";
        Map<String, String> varMap = ParameterMap.mapDotParameters(sql);
        String newSql = NamedParamStatement.replaceMapStrings(sql, varMap);
        Assert.assertEquals(newSql, "SELECT *, t.foo FROM table where a = :c1_f1 AND b = :f2 AND c = :c2_f2");
    }

}
