package com.apptechnik.appinabox.engine.util;

import org.junit.Assert;
import org.junit.Test;

public class TestPathUtil {

    @Test
    public void testSplitString() {
        String s = "/${VAR}/to//${VAR2}/dimension/";
        Assert.assertEquals(0, PathUtil.pathElementIndex(s, '/', "${VAR}"));
        Assert.assertEquals(3, PathUtil.pathElementIndex(s, '/', "${VAR2"));
        Assert.assertEquals(-1, PathUtil.pathElementIndex(s, '/', "notfound"));

    }
}
