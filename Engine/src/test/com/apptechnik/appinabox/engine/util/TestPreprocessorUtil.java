package com.apptechnik.appinabox.engine.util;

import com.apptechnik.appinabox.engine.util.PreprocessorUtil;
import com.apptechnik.appinabox.engine.util.FileUtil;
import org.junit.Test;

import java.io.File;
import java.util.List;

public class TestPreprocessorUtil {

    @Test
    public void testComments() throws Exception {
        String s = FileUtil.readFileToString(new File("./src/main/com/apptechnik/appinabox/engine/util/PreprocessorUtil.java"));
        List<String> res = PreprocessorUtil.textInStandardComments(s);
        System.out.println("comment text");
        for (String r : res) {
            System.out.println(r);
        }
        String stripped = PreprocessorUtil.stripStandardComments(s);
        System.out.println("stripped text");
        System.out.println(StringUtil.stripEmptyLines(stripped));

    }

    @Test
    public void testCommentsInSQL() throws Exception {
        String s = "/* format: { name, address } */\n" +
                   "/* name : simple */\n" +
                   "select name,address from customers";
        String stripped = PreprocessorUtil.stripStandardComments(s);
        System.out.println("stripped text");
        System.out.println(StringUtil.stripEmptyLines(stripped));
    }

}
