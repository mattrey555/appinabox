package com.apptechnik.appinabox.engine.util;

import com.apptechnik.appinabox.engine.Constants;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class TestRegex {

    @Test
    public void testSettingsInclude() {
        String s = "\":project1\", ':project2', 'project3', \"project4\"";
        List<String> matches = StringUtil.matchingRegexes(s, Constants.Regexes.SETTINGS_INCLUDE);
        Assert.assertEquals("\":project1\"", matches.get(0));
        Assert.assertEquals("':project2'", matches.get(1));
        Assert.assertEquals("'project3'", matches.get(2));
        Assert.assertEquals("\"project4\"", matches.get(3));
    }

    @Test
    public void testIdentifier() {
        Assert.assertTrue("ident".matches( Constants.Regexes.IDENTIFIER));
        Assert.assertTrue("ident0".matches( Constants.Regexes.IDENTIFIER));
        Assert.assertTrue("ident_0".matches( Constants.Regexes.IDENTIFIER));
        Assert.assertTrue("_ident0".matches( Constants.Regexes.IDENTIFIER));
        Assert.assertTrue("_ident".matches( Constants.Regexes.IDENTIFIER));
        Assert.assertTrue("IDENT0".matches( Constants.Regexes.IDENTIFIER));
    }
}
