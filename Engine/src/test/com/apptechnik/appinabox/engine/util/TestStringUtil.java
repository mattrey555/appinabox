package com.apptechnik.appinabox.engine.util;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestStringUtil {

    @Test
    public void testSplitString() {
        String s = "string split; by semicolons; escaped with \\backslash; and quoted with 'single quotes'";
        String[] array = StringUtil.splitQuotedEscaped(s, ";", "'", "\\");
        Assert.assertEquals(4, array.length);
        Assert.assertEquals("string split", array[0]);
        Assert.assertEquals(" by semicolons", array[1]);
        Assert.assertEquals(" escaped with \\backslash", array[2]);
        Assert.assertEquals(" and quoted with 'single quotes'", array[3]);
    }

    @Test
    public void testIncrement() {
        Assert.assertEquals("a1", StringUtil.incrementVarSuffix("a"));
        Assert.assertEquals("a2", StringUtil.incrementVarSuffix("a1"));
        Assert.assertEquals("a123", StringUtil.incrementVarSuffix("a122"));

    }

    @Test
    public void testConcat() {
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        list.add("c");
        Assert.assertEquals("a,b,c", StringUtil.concat(list, ","));
    }

    @Test
    public void testRemoveExtension() {
        Assert.assertEquals("foo", StringUtil.removeExtension("foo.bar"));
        Assert.assertEquals("foo", StringUtil.removeExtension("foo"));

    }

    @Test
    public void testStripQuptes() {
        Assert.assertEquals("foo", StringUtil.stripQuotes("\"foo\""));
        Assert.assertEquals("foo", StringUtil.stripQuotes("foo"));

    }

    @Test
    public void testSplitAndTrim() {
        String[] arr = StringUtil.splitAndTrim(" a, b ,c,d ", ",");
        Assert.assertEquals("a", arr[0]);
        Assert.assertEquals("b", arr[1]);
        Assert.assertEquals("c", arr[2]);
        Assert.assertEquals("d", arr[3]);
        String[] arr2 = StringUtil.splitAndTrim(" a|| b ||c||d ", "\\|\\|");
        Assert.assertEquals("a", arr2[0]);
        Assert.assertEquals("b", arr2[1]);
        Assert.assertEquals("c", arr2[2]);
        Assert.assertEquals("d", arr2[3]);
        String[] arr3 = StringUtil.splitAndTrim(" a ", " florgplatz");
        Assert.assertEquals("a", arr3[0]);
    }

    @Test
    public void testFilterTag() {
        List<String> list = new ArrayList<String>();
        list.add("this is a test");
        list.add("tag: derp");
        list.add("tag: derpy derp");
        list.add("lerpy: derp");
        list.add("lerpy: derpy lerp lerp");

        List<String> filtered = StringUtil.filterTag(list, "tag");
        Assert.assertEquals("derp", filtered.get(0));
        Assert.assertEquals("derpy derp", filtered.get(1));
        String findTag = StringUtil.findTag(list, "lerpy");
        Assert.assertEquals("derp", findTag);
    }

    @Test
    public void testTableColumn() {
        Assert.assertEquals("table", StringUtil.getTableName("table.column"));
        Assert.assertNull(StringUtil.getTableName("column"));
        Assert.assertEquals("column", StringUtil.getColumnName("table.column"));
        Assert.assertEquals("column", StringUtil.getColumnName("column"));
    }

    @Test
    public void testRegexpFilter() {
        String[] testStrings = new String[]{"A", "B", "C", "d", "123"};
        List<String> result = StringUtil.nonMatchingRegexes(Arrays.asList(testStrings), "[A-Za-z]*");
        Assert.assertEquals("123", result.get(0));
    }

    @Test
    public void testUnderscoresToMixedCase() {
        Assert.assertEquals("FilmQueryLimitListItem", StringUtil.underscoresToMixedCase("film_query_limit_list_item", true));
        Assert.assertEquals("FilmQuEryLiMitLiStItem", StringUtil.underscoresToMixedCase("___film__qu____ery_li_______mit_li__st_item__", true));
    }
}
