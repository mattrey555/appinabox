package com.apptechnik.appinabox.engine.util;

import com.apptechnik.appinabox.engine.Constants;
import com.apptechnik.appinabox.engine.exceptions.PropertiesException;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

public class TestSubstitute {
    static final String test = "c = select * from users where access_token = :access_token;\n" +
            "if (c) {\n" +
            "if (c.expires < c.now) {\n" +
            "update users set access_token = :c.new_access_token where access_token = :access_token;\n" +
            "return select * from purchases where user_id = :c.user_id;\n";

    @Test
    public void testParams() {
        List<String> params = new ArrayList<>(Substitute.getParams("/v1/staff/${store_id}?staff-id=${staff_id}"));
        Assert.assertEquals(2, params.size());
        Assert.assertEquals("store_id", params.get(0));
        Assert.assertEquals("staff_id", params.get(1));
    }

    @Test
    public void testTransformResource2() throws IOException, PropertiesException {
        Properties transform = new Properties();
        transform.setProperty("LIBRARY_VERSION", "test_library_version");
        transform.setProperty("CREATE_TABLES", "test_create_tables");
        transform.setProperty("DONT_MATCH_THIS_STRING", "test_error");
        File testFile = new File("test2.txt");
        Substitute.transformResource(Constants.Templates.SERVER_BUILD_GRADLE, testFile, transform);
        String result = FileUtil.readFileToString(testFile);
        Assert.assertTrue(result.contains("test_library_version"));
        Assert.assertTrue(!result.contains("LIBRARY_VERSION"));
        Assert.assertTrue(result.contains("test_create_tables"));
        Assert.assertFalse(result.contains("test_error"));
    }

    @Test
    public void testTransformStreamDefaults() throws IOException {
        Properties transform = new Properties();
        transform.setProperty("DATABASE_NAME", "dvdrental");
        transform.setProperty("USER", "postgres");
        transform.setProperty("PASSWORD", "FiatX1/9");
        transform.setProperty("SQL_DRIVER", "org.postgresql.Driver");
        transform.setProperty("URL", "jdbc:postgresql://localhost");
        File testOut = new File("testfile.xml");
        Substitute.transformResourceDefaults(Constants.Templates.CONTEXT_DATASOURCE, testOut, transform);
    }

    public class CreateVariable implements Substitute.MakeString {
        int startVal;

        public CreateVariable() {
            startVal = 0;
        }

        public String create(String originalString) {
            startVal++;
            return ":var" + startVal;
        }
    }
}
