package com.apptechnik.appinabox.engine.util;

import com.apptechnik.appinabox.engine.Constants;
import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class TestXMLUtil {

    @Test
    public void test() throws Exception {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Properties properties = new Properties();
        properties.put("NAME", "layout");
        String xml = Substitute.transformResource(Constants.Templates.ANDROID_DISPLAY_FRAGMENT_XML, properties);
        Document doc = documentBuilder.parse(new InputSource(new StringReader(xml)));
        Properties propertiesChild = new Properties();
        propertiesChild.put("NAME", "child");
        String textViewXML = Constants.Templates.ANDROID_LAYOUT_TEMPLATES + "/" +  Constants.LayoutTemplates.TEXT_VIEW_XML;
        Node node = XMLUtils.addChildFromTransformedResource(doc, Constants.UIElements.CONSTRAINT_LAYOUT, null,
                                                             textViewXML, propertiesChild);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        DOMSource domSource = new DOMSource(doc);
        StreamResult streamResult = new StreamResult(stream);
        XMLUtils.createTransformer().transform(domSource, streamResult);
        String result = stream.toString();
        System.out.println("result: " + result);
        Map<String, String> map = new HashMap<>();
        map.put("android:id", "@+id/child");
        List<Element> children = XMLUtils.findElementsByNameAndAttributes(node, "TextView",map);
        Assert.assertEquals(1, children.size());
    }
}
