CREATE TABLE stores (store_id SERIAL,
					 name VARCHAR(64) NOT NULL,  
					 address VARCHAR(256) NOT NULL, 
					 PRIMARY KEY(store_id));

CREATE TABLE items (item_id SERIAL,
				    name VARCHAR(128), 
				    price DECIMAL(8,2), 
				    PRIMARY KEY(item_id));

CREATE TABLE customers (customer_id SERIAL,
						 name VARCHAR(256), 
						 address VARCHAR(256), 
						 PRIMARY KEY(customer_id));

CREATE TABLE purchases (purchase_id SERIAL,
					    price DECIMAL(8,2), 
					    customer_id INTEGER NOT NULL REFERENCES customers(customer_id), 
					    store_id INTEGER NOT NULL REFERENCES stores(store_id), 
					    item_id INTEGER NOT NULL REFERENCES items(item_id),  
					    timestamp TIMESTAMP, 
					    PRIMARY KEY(purchase_id));


CREATE TABLE purchase_notes (purchase_note_id SERIAL,
					         purchase_id INTEGER NOT NULL REFERENCES purchases(purchase_id),
							 timestamp TIMESTAMP,
							 note VARCHAR(1024),
							 PRIMARY KEY(purchase_note_id));

CREATE TABLE services (service_id SERIAL,
					    price DECIMAL(8,2), 
					    customer_id INTEGER NOT NULL REFERENCES customers(customer_id), 
					    store_id INTEGER NOT NULL REFERENCES stores(store_id), 
					    timestamp TIMESTAMP, 
					    PRIMARY KEY(service_id));

-- id is the user's unique identifier
-- username is the user's plaintext username.  
-- hashed_password is the one-way encrypted password, combined with a random salt to increase entropy
-- password_salt is that random number 
-- last_password_attempt_date last date (local to server) the user attempted enter a password for this user.
-- num_attempts number of failed attempts to enter the passwrd
-- last_ip_address last IP Address the user attempted to log in from.
-- token_refreshed_date last time the auth_token was refreshed.

CREATE TABLE users 
    (id SERIAL,
	 username VARCHAR(256) NOT NULL,
	 hashed_password VARCHAR(256) NOT NULL,
	 password_salt VARCHAR(256) NOT NULL,
	 access_token VARCHAR(256) NOT NULL,
	 refresh_token VARCHAR(256) NOT NULL,
	 last_password_attempt_date TIMESTAMP,
	 num_attempts INTEGER NOT NULL,
	 last_ip_address VARCHAR(64), 
	 PRIMARY KEY(id));
CREATE INDEX username_index on users(username);

-- id is the unique identifier for this auth token. A user may have multiple auth tokens,
-- 1 per machine, as identified by IP Address.  The auth token is a one-way encrypted UUID IP Address, without
-- a salt, because we need to search on it.
-- user_id is the id of the user associated with this token
-- hashed_auth_token_and_ip_address is the Auth Token UUID and IP address one-way encrypted.
-- token_refresh
-- last_ip_address last IP Address the user attempted to log in from.
-- hashed_auth_token one-way encrypted authorization token. We don't use a salt because the auth_token
-- is a randomly generated 128-bit UUID, and this dif
-- token_refreshed_date last time the auth_token was refreshed.

CREATE TABLE auth_tokens
    (id SERIAL,
     user_id INTEGER NOT NULL REFERENCES users(id),
     hashed_auth_token_and_ip_address VARCHAR(256) NOT NULL,
     hashed_refresh_token_and_ip_address VARCHAR(256) NOT NULL,
     token_refreshed_date TIMESTAMP,
     expiration_msec BIGINT NOT NULL,
     PRIMARY KEY(id));
CREATE INDEX auth_tokens_index ON auth_tokens(hashed_auth_token_and_ip_address);
