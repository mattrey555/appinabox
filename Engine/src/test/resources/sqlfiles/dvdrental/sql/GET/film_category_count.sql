/* url: /filmcategorycount */
/* header: category_id: CATEGORY_ID */
/* json-response: { category.name, filmCount } */
/* name-response: film_category_count */
/* package: com.appinabox.dvdrental.film_category_count */
SELECT category.name, COUNT(film.film_id) AS filmCount FROM category 
INNER JOIN film_category ON category.category_id = film_category.category_id
INNER JOIN film ON film.film_id = film_category.film_id
WHERE film_category.category_id = :category_id
GROUP BY category.name;
