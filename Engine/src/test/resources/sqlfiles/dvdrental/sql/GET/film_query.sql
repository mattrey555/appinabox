/* url: /film-query */
/* json-response: { category.name, films: [ film.title ]} */
/* name-response: film_query */
/* package: com.appinabox.dvdrental.film_query */
SELECT category.name, film.title FROM category 
INNER JOIN film_category ON category.category_id = film_category.category_id
INNER JOIN film ON film.film_id = film_category.film_id;
