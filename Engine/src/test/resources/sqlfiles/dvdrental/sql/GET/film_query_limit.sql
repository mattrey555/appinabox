/* url: /filmquery_limit?limit=${limit} */
/* json-response: { category.name, films : [ film.title ]} */
/* name-response: film_query_limit */
/* package: com.appinabox.dvdrental.film_query_limit */
SELECT category.name, film.title FROM category 
INNER JOIN film_category ON category.category_id = film_category.category_id
INNER JOIN (SELECT * FROM film LIMIT :limit) AS film ON film.film_id = film_category.film_id;
