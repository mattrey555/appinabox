/* url: /payment */
/* json-response: { customer.first_name, customer.last_name, amount : { double_amount, rental_id }} */
/* name-response: payment */
/* package: com.appinabox.dvdrental.payment */
SELECT customer.first_name, customer.last_name, payment.amount*2 as double_amount, rental_id 
FROM customer INNER JOIN payment ON customer.customer_id = payment.customer_id;
