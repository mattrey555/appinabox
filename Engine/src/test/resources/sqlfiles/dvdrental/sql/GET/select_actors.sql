/* url: /actorlastname?last_name=${last_name} */
/* json-response: { first_name, last_name } */
/* name-response: select_actor */
/* package: com.appinabox.dvdrental.select_actors */
select first_name, last_name from actor where last_name = :last_name
