/* url: /filmcategory */
/* json-response: { film.title, film.release_year, film.description, film.rating, categories : [ category.name ] } */
/* name-response: select_film_category */
/* package: com.appinabox.dvdrental.select_film_category */
SELECT film.title, film.release_year, film.description, film.rating, category.name 
FROM film 
INNER JOIN film_category ON film.film_id = film_category.film_id
INNER JOIN category ON film_category.category_id = category.category_id;
