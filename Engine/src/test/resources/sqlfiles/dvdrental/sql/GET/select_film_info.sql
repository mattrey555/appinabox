/* url: /filminfo */
/* json-response: { film.title, film.release_year, film.description, film.rating, language, actors : { actor.first_name, actor.last_name }, categories : [ category.name ] } */
/* name-response: select_film_info */
/* package: com.appinabox.dvdrental.select_film_info */
SELECT film.title, film.release_year, film.description, film.rating, actor.first_name, actor.last_name, category.name, language.name as language 
FROM film 
INNER JOIN film_actor ON film.film_id = film_actor.film_id
INNER JOIN actor ON film_actor.actor_id = actor.actor_id
INNER JOIN film_category ON film.film_id = film_category.film_id
INNER JOIN category ON film_category.category_id = category.category_id
INNER JOIN language ON film.language_id = language.language_id;
