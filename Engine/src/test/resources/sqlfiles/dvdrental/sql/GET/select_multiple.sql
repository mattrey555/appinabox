/* url: /actorsandcustomers */
/* json-response: { actor : { actor.first_name, actor.last_name }, customer : { customer.first_name, customer.last_name }} */
/* name-response: select_multiple */
/* name: select_multiple */
/* package: com.appinabox.dvdrental.select_multiple */
select first_name, last_name from actor limit 10;
select first_name, last_name from customer limit 10;
