/* url: /storeinfo */
/* json-response: { store.store_id, city.city, address.address, address.district, staff : { first_name, last_name } , rental : { inventory.inventory_id, film.title, film.release_year, return_date }} */
/* name-response: store_info */
/* package: com.appinabox.dvdrental.store_info */
SELECT store.store_id, city.city, address.address, address.district, inventory.inventory_id, film.title, film.release_year, first_name, last_name, rental.return_date 
FROM store 
INNER JOIN address ON store.address_id = address.address_id 
INNER JOIN city ON address.city_id = city.city_id 
INNER JOIN inventory ON store.store_id = inventory.store_id 
INNER JOIN film on inventory.film_id = film.film_id
INNER JOIN staff ON store.store_id = staff.store_id
INNER JOIN rental ON rental.inventory_id = inventory.inventory_id;
