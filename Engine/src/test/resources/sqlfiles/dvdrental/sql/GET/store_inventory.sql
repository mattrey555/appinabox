/* url: /storeinventory */
/* json-response: { store.store_id, city.city, address.address, address.district, inventory : { inventory.inventory_id, film.title, film.release_year }} */
/* name-response: select_inventory */
/* package: com.appinabox.dvdrental.store_inventory */
SELECT store.store_id, city.city, address.address, address.district, inventory.inventory_id, film.title, film.release_year 
FROM store 
INNER JOIN address ON store.address_id = address.address_id 
INNER JOIN city ON address.city_id = city.city_id 
INNER JOIN inventory ON store.store_id = inventory.store_id 
INNER JOIN film on inventory.film_id = film.film_id;
