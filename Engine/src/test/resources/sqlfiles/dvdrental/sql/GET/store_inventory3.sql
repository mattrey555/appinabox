/* url: /store_inventory3 */
/* json-response: { the_store_id, city, address.address, district, film_inventory : { inventory_id, film.title, film.release_year }} */
/* name-response: select_film_inventory */
/* package: com.appinabox.dvdrental.store_inventory3 */
SELECT store.store_id as the_store_id, city.city, address.address, address.district, inventory_select.inventory_id, film.title, film.release_year 
FROM store 
INNER JOIN address ON store.address_id = address.address_id 
INNER JOIN city ON address.city_id = city.city_id 
INNER JOIN (select * FROM inventory) AS inventory_select ON store.store_id = inventory_select.store_id 
INNER JOIN film on inventory_select.film_id = film.film_id;
