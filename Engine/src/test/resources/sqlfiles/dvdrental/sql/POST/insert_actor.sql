/* url: /insert/insert_actor */
/* json-request: { actor_id, first_name, last_name, last_update } */
/* name-request: insert_actor */
/* name: insert_actor */
/* package: com.appinabox.dvdrental.insert_actor */
DELETE FROM actor where actor_id = :actor_id;
INSERT INTO actor (first_name, last_name, last_update) VALUES (:first_name, :last_name, :last_update);
