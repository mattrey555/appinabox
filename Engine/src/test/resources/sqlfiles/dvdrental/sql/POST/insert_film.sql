/* url: /insert/insert_film */
/* json-request: { title, description, release_year, language_id, rental_duration, rental_rate, length, replacement_cost, rating, actor_film : { actor_id }} */
/* name-request: insert_film */
/* name: insert_film */
/* package: com.appinabox.dvdrental.insert_film */
/* type: rating varchar */
INSERT INTO film (title, description, release_year, language_id, rental_duration, rental_rate, length, replacement_cost, rating) VALUES (:title, :description, :release_year, :language_id, :rental_duration, :rental_rate, :length, :replacement_cost, CAST(:rating AS mpaa_rating)) RETURNING film_id;
INSERT INTO film_actor (film_id, actor_id) VALUES (:film_id, :actor_id);
