/* url: /staff/set-active?active=${active} */
/* json-request: { staff_id } */
/* name-request: update_staff */
/* package: com.appinabox.dvdrental.update_staff */
UPDATE staff set active=:active where staff_id=:staff_id
