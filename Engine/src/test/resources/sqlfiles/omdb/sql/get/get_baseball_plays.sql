/* url: /get-baseball-plays */
/* json-response: { event_key, site_id, event_states : { baseball_event_states.* }, action_plays : { baseball_action_plays.* }} */
/* name-response: get_baseball_plays */
/* package: com.appinabox.sports.get_baseball_plays */
SELECT *                                
FROM    
    baseball_event_states,
    events,
    baseball_action_plays
WHERE   
events.event_key like '%mlb%'
AND baseball_event_states.event_id = events.id
AND baseball_action_plays.baseball_event_state_id = baseball_event_states.id;
