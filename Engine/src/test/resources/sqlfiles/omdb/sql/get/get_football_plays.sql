/* url: /get-football-plays */
/* json-response: { full_name, plays : { american_football_action_participants.* }} */
/* name-response: football_plays */
/* package: com.appinabox.sports.get_football_plays */
select full_name, american_football_action_participants.* from american_football_action_participants left join display_names on display_names.id = american_football_action_participants.person_id;
