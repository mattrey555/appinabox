/* url: /get-fumble-stats */
/* json-response: { full_name, fumbles : { fumbles.* }} */
/* name-response: player_fumbles */
/* package: com.appinabox.sports.fumble_stats */
SELECT display_names.full_name, fumbles.* 
FROM american_football_fumbles_stats AS fumbles 
INNER JOIN stats ON stats.stat_repository_id = fumbles.id 
INNER JOIN persons ON stats.stat_holder_id = persons.id 
INNER JOIN display_names ON persons.id=display_names.entity_id 
WHERE stats.stat_holder_type='persons' AND stats.stat_repository_type = 'american_football_fumbles_stats';
