/* url: /get-homers2 */
/* json-response: { batter, pitcher, plays : { rbi, play_type, notation }} */
/* name-response: homers2 */
/* package: com.appinabox.sports.homers2 */
SELECT
	dn1.full_name as batter,
	dn2.full_name as pitcher,
	baseball_action_plays.rbi,
	baseball_action_plays.play_type,
	baseball_action_plays.rbi,
	baseball_action_plays.notation

FROM
	display_names as dn1,
	display_names as dn2,
	baseball_event_states,
	baseball_action_plays,
	events

WHERE
	dn1.entity_type = 'persons'
	AND dn1.entity_id = baseball_event_states.batter_id

	-- get name of pitcher
	AND dn2.entity_type = 'persons'
	AND dn2.entity_id = baseball_event_states.pitcher_id

	-- restrict play type to 'home-run'
	AND baseball_action_plays.baseball_event_state_id = baseball_event_states.id
	AND baseball_action_plays.play_type = 'home-run'
	AND baseball_event_states.event_id = events.id

ORDER BY
	events.start_date_time asc,
	baseball_event_states.inning_value asc,
	baseball_event_states.inning_half desc,
	baseball_event_states.outs asc;
