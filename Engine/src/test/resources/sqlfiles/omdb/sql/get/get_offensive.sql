/* url: /get-offensive-stats */
/* json-response: { full_name, offensive : { offensive.* }} */
/* name-response: player_offensive */
/* package: com.appinabox.sports.offensive_stats */
SELECT names.full_name, offensive.* FROM american_football_offensive_stats AS offensive 
INNER JOIN stats ON stats.stat_repository_id = offensive.id 
INNER JOIN persons ON stats.stat_holder_id = persons.id 
INNER JOIN (SELECT full_name,entity_id FROM display_names WHERE display_names.entity_type='persons') AS names ON persons.id=names.entity_id where stats.stat_holder_type='persons';
