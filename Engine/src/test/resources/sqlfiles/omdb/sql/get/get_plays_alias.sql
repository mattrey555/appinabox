/* url: /get-football-plays-alias */
/* json-response: { guys_full_name, plays : { american_football_action_participants.* }} */
/* name-response: get_plays_alias */
/* package: com.appinabox.sports.get_plays_alias */
select full_name as guys_full_name, american_football_action_participants.* from american_football_action_participants left join display_names on display_names.id = american_football_action_participants.person_id;
