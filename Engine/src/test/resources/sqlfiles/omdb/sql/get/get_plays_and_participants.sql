/* url: /get-plays-and-participants */
/* json-response: { full_name, plays : { plays.* }} */
/* name-response: plays_and_participants */
/* package: com.appinabox.sports.plays_and_participants */
SELECT full_name, participants.*,plays.* FROM american_football_action_plays AS plays
LEFT OUTER JOIN american_football_action_participants AS participants
ON plays.id=american_football_action_play_id 
LEFT OUTER JOIN display_names
ON display_names.id = participants.person_id ORDER BY play_type;
