/* url: /get-football-plays-table-alias */
/* json-response: { guys_full_name, plays : { participants.* }} */
/* name-response: get_plays_table_alias */
/* package: com.appinabox.sports.get_plays_table_alias */
select full_name as guys_full_name, participants.* from american_football_action_participants as participants left join display_names on display_names.id = participants.person_id;
