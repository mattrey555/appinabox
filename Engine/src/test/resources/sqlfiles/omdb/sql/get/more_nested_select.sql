/* url: /get-more-nested-select */
/* json-response: { language, name : { full_name, id }} */
/* name-response: more_nested_select */
/* package: com.appinabox.sports.more_nested_select */
select language, id,full_name from (select * from (select * from display_names where full_name like 'A%') as names1 where entity_type='persons') as names2 order by language;
