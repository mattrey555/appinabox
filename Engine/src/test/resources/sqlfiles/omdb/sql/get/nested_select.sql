/* url: /get-nested-select */
/* json-response: { full_name } */
/* name-response: nested_select */
/* package: com.appinabox.sports.nested_select */
select full_name from (select * from display_names where entity_type='affiliations') as names;
