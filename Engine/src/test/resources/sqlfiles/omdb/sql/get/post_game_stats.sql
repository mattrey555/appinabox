/* url: /post_game_stats/${person_key}?event_key=${event_key} */
/* json-response: { person_key, full_name, stats : { american_football_passing_stats.* }} */
/* name-response: post_game_stats */
/* package: com.appinabox.sports.post_game_stats */
SELECT
	persons.person_key,
	display_names.full_name,
	american_football_passing_stats.passes_attempts,
	american_football_passing_stats.passes_completions,
	american_football_passing_stats.passes_percentage,
	american_football_passing_stats.passes_yards_gross,
	american_football_passing_stats.passes_longest,
	american_football_passing_stats.passes_touchdowns,
	american_football_passing_stats.passes_interceptions
FROM
	stats, persons, american_football_passing_stats, display_names, events
WHERE
	    persons.person_key = :person_key
	AND events.event_key = :event_key
	AND stats.stat_repository_type = 'american_football_passing_stats'
	AND stats.stat_repository_id = american_football_passing_stats.id
	AND stats.stat_holder_type = 'persons'
	AND stats.stat_holder_id = persons.id
	AND stats.stat_coverage_type = 'events'
	AND stats.stat_coverage_id = events.id
	AND stats.context = 'event'
	AND display_names.entity_type = 'persons'
	AND display_names.entity_id = persons.id
	;

