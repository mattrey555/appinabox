/* url: /touchdown_participants?event_key=${event_key} */
/* json-response: { comment, players : { first_name, last_name, participation : { participant_role, yards_gained }}} */
/* name-response: touchdown_participants */
/* package: com.appinabox.sports.touchdown_participants */
SELECT
	display_names.first_name,
	display_names.last_name,
	american_football_action_participants.participant_role,
	american_football_action_participants.yards_gained,
	american_football_action_plays.comment,
	american_football_event_states.period_value,
	american_football_event_states.period_time_remaining
from
	american_football_event_states,
	american_football_action_plays,
	american_football_action_participants,
	events,
	display_names
where
	events.event_key like :event_key
	AND american_football_event_states.event_id = events.id
	AND american_football_action_plays.american_football_event_state_id = american_football_event_states.id
	AND american_football_action_participants.american_football_action_play_id = american_football_action_plays.id
	AND american_football_action_participants.person_id = display_names.entity_id
	AND display_names.entity_type = 'persons'
	AND american_football_action_plays.score_attempt_type = 'touchdown'
ORDER BY
	american_football_event_states.sequence_number asc
	;
