/* url: /get-affiliations */
/* json-response: { event_key, start_date_time } */
/* name-response: affiliations */
/* package: com.appinabox.sports.get_affiliations */
SELECT 
        events.event_key, events.start_date_time
FROM 
        events, affiliations, affiliations_events
WHERE
        affiliations_events.affiliation_id = affiliations.id
        AND affiliations_events.event_id = events.id
        AND affiliations.affiliation_key = 'l.mlb.com'
        AND events.start_date_time > '2007-08-04'
        AND events.start_date_time < '2007-08-05';
