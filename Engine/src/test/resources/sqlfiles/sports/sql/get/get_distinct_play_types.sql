/* url: /get-play-types */
/* json-response: { play_type } */
/* name-response: play_types */
/* package: com.appinabox.sports.distinct_play_type */
select distinct(play_type) from baseball_action_plays;
