/* url: /get-events?key=${event_key} */
/* json-response: { event_key, event_info : { publisher_id, start_date_time, site_id, event_status, duration, attendance, last_update, participant_type, alignment, event_outcome }} */
/* name-response: events */
/* package: com.appinabox.sports.get_events */
SELECT *
FROM events, participants_events
WHERE events.event_key = :event_key AND participants_events.event_id = events.id;
