/* url: /get-game-states-plays */
/* json-response: { event_key, site_id, event_states : { baseball_event_states.* }} */
/* name-response: get_game_states_plays */
/* package: com.appinabox.sports.get_game_states_plays */
SELECT * FROM baseball_event_states, events WHERE events.event_key like '%mlb%';
