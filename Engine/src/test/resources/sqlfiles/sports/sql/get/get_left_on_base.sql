/* url: /get-left-on-base */
/* json-response: { full_name, left_on_base } */
/* name-response: left_on_base */
/* package: com.appinabox.sports.left_on_base */
SELECT
        display_names.full_name,
        baseball_offensive_stats.left_on_base

from
        events,
        stats,
        baseball_offensive_stats,
        display_names
where
        stats.stat_coverage_id = events.id
        AND stats.stat_repository_type = 'baseball_offensive_stats'
        AND stats.stat_repository_id = baseball_offensive_stats.id
        AND stats.stat_holder_type = 'teams'
        AND stats.stat_holder_id = display_names.entity_id
        AND display_names.entity_type = 'teams';
