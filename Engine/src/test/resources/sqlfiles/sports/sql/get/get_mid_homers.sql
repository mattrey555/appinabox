/* url: /mid-homers */
/* json-response: { batter, pitcher } */
/* name-response: mid_homers */
/* package: com.appinabox.sports.mid_homers */
SELECT
	events.start_date_time, 
	dn1.full_name as batter,
	dn2.full_name as pitcher,
	baseball_event_states.inning_value as inning,
	baseball_event_states.inning_half as half,
	baseball_event_states.outs,
	baseball_action_plays.rbi,
	baseball_action_plays.notation
FROM
	display_names as dn1,
	display_names as dn2,
	baseball_event_states,
	baseball_action_plays,
	events,
	events_sub_seasons,
	sub_seasons
WHERE
	dn1.entity_type = 'persons'
	AND dn1.entity_id = baseball_event_states.batter_id
	AND dn2.entity_type = 'persons'
	AND dn2.entity_id = baseball_event_states.pitcher_id
	AND baseball_action_plays.baseball_event_state_id = baseball_event_states.id
	AND baseball_event_states.event_id = events.id
	AND events_sub_seasons.event_id = events.id
	AND events_sub_seasons.sub_season_id = sub_seasons.id
ORDER BY
	events.start_date_time asc,
	baseball_event_states.inning_value asc,
	baseball_event_states.inning_half desc,
	baseball_event_states.outs asc;
