/* url: /get-pitching-stats */
/* json-response: { baseball_pitching_stats.event_credit, events : { event_key, full_name, person_id }} */
/* name-response: get_pitching_stats */
/* package: com.appinabox.sports.get_pitching_stats */
SELECT
	events.event_key,
	baseball_pitching_stats.event_credit,
	persons.id as person_id,
	display_names.full_name
FROM
	stats, events, baseball_pitching_stats, persons, display_names
WHERE
	stats.stat_repository_type = 'baseball_pitching_stats'
	AND stats.stat_repository_id = baseball_pitching_stats.id
	AND events.event_key like '%mlb%'
	AND stats.stat_coverage_id = events.id
	AND stats.stat_holder_type = 'persons'
	AND persons.id = stats.stat_holder_id
	AND baseball_pitching_stats.event_credit IS NOT NULL
	AND display_names.entity_id = stats.stat_holder_id
	AND display_names.entity_type = 'persons'
ORDER BY
       baseball_pitching_stats.event_credit;

