/* url: /get-player-keys?publisher1=${publisher1}&publisher2=${publisher2} */
/* json-response: { p_rs.person_key, p_rs.publisher_id, p_tsn.person_key, p_tsn.publisher_id, dn_tsn.full_name } */
/* name-response: player_keys */
/* package: com.appinabox.sports.get_player_keys */
SELECT 
	p_rs.person_key, 
	p_rs.publisher_id, 
	dn_rs.full_name,
	p_tsn.person_key, 
	p_tsn.publisher_id,
	dn_tsn.full_name
	from 
	persons as p_rs,
	persons as p_tsn,
	display_names as dn_rs,
	display_names as dn_tsn
	where 
	dn_rs.full_name = dn_tsn.full_name

	and dn_rs.entity_type = 'persons'
	and dn_rs.entity_id = p_rs.id
	and p_rs.publisher_id = :publisher1

	and dn_tsn.entity_type = 'persons'
	and dn_tsn.entity_id = p_tsn.id
	and p_tsn.publisher_id = :publisher2

	order by dn_rs.full_name
