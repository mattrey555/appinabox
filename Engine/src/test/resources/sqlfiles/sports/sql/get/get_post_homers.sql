/* url: /post-homers */
/* json-response: { full_name, home_runs } */
/* name-response: post_homers */
/* package: com.appinabox.sports.post_homers */
SELECT
        display_names.full_name,
        baseball_offensive_stats.home_runs
from
        events,
        stats,
        baseball_offensive_stats,
        display_names
where
        stats.stat_coverage_id = events.id
        AND stats.stat_repository_type = 'baseball_offensive_stats'
        AND stats.stat_repository_id = baseball_offensive_stats.id
        AND baseball_offensive_stats.home_runs > 0
        AND stats.stat_holder_type = 'persons'
        AND stats.context = 'event'
        AND stats.stat_holder_id = display_names.entity_id
        AND display_names.entity_type = 'persons'
