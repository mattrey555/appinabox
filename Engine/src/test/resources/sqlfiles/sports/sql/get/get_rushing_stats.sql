/* url: /get-rushing-stats?key=${event_key} */
/* json-response: { event_key, events : { start_date_time, person_key, person_dn.full_name, team_key, team_dn.full_name, rushing_stats : { american_football_rushing_stats.* }, stats.context }} */
/* name-response: stats_info */
/* package: com.appinabox.sports.get_rushing_stats */
SELECT 
        events.event_key,
        events.start_date_time,
        persons.person_key, 
        person_dn.full_name, 
        teams.team_key, 
        team_dn.full_name, 
        american_football_rushing_stats.*,
        stats.context
from 
        stats,
        events,american_football_rushing_stats,
        persons, 
        display_names as person_dn, 
        display_names as team_dn,
        person_event_metadata,
        teams
where
        events.event_key = :event_key
        and stats.stat_coverage_type = 'events'
        and stats.stat_coverage_id = events.id
        and stats.stat_repository_type = 'american_football_rushing_stats'
        and stats.stat_repository_id = american_football_rushing_stats.id
        and stats.stat_holder_type = 'persons'
        and stats.stat_holder_id = persons.id
        and person_dn.entity_type = 'persons'
        and person_dn.entity_id = persons.id
        and person_event_metadata.person_id = persons.id
        and person_event_metadata.event_id = events.id
        and person_event_metadata.team_id = teams.id
        and team_dn.entity_type = 'teams'
        and team_dn.entity_id = teams.id;
