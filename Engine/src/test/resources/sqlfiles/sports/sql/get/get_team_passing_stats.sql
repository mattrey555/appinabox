/* url: /get-team-passing-stats?key=${team_key} */
/* json-response: { team_key, stats : { first_name, last_name, events : { events.event_key, events.start_date_time, passing_stats : { american_football_passing_stats.passes_attempts, american_football_passing_stats.passes_completions, american_football_passing_stats.passes_percentage }}}} */
/* name-response: passing_stats_info */
/* package: com.appinabox.sports.get_team_passing_stats */
SELECT
	teams.team_key,
	display_names.first_name,
	display_names.last_name,
	persons.person_key,
	events.event_key,
	events.start_date_time,
	american_football_passing_stats.passes_attempts,
	american_football_passing_stats.passes_completions,
	american_football_passing_stats.passes_percentage,
	positions.abbreviation

FROM
	stats, teams, persons, american_football_passing_stats, display_names, events, events_sub_seasons, sub_seasons, person_event_metadata, positions
WHERE
	    teams.team_key = :team_key
	AND sub_seasons.sub_season_key = '2007_season_regular'
	AND events_sub_seasons.event_id = events.id
	AND events_sub_seasons.sub_season_id = sub_seasons.id
	AND stats.stat_repository_type = 'american_football_passing_stats'
	AND stats.stat_repository_id = american_football_passing_stats.id
	AND american_football_passing_stats.passes_attempts IS NOT NULL
	AND stats.stat_holder_type = 'persons'
	AND stats.stat_holder_id = persons.id
	AND stats.stat_coverage_type = 'events'
	AND stats.stat_coverage_id = events.id
	AND stats.context = 'event'
	AND display_names.entity_type = 'persons'
	AND display_names.entity_id = persons.id
	AND person_event_metadata.person_id = persons.id
	AND person_event_metadata.event_id = events.id
	AND person_event_metadata.team_id = teams.id
	AND person_event_metadata.position_id = positions.id
