/* url: /top-hitters?season_key=${season_key}&at_bats=${at_bats} */
/* json-response: { person_key, first_name, last_name, season_stats : { season_key, stats : { hits, at_bats, rbi }}} */
/* name-response: top_hitters */
/* package: com.appinabox.sports.top_hitters */
SELECT
	persons.person_key,
	display_names.first_name,
	display_names.last_name,
	baseball_offensive_stats.hits,
	baseball_offensive_stats.at_bats,
	baseball_offensive_stats.rbi,
	seasons.season_key,
	sub_seasons.sub_season_type
FROM
	stats, baseball_offensive_stats, sub_seasons, seasons, persons, display_names
WHERE
        stats.stat_repository_type = 'baseball_offensive_stats'
	AND stats.stat_repository_id = baseball_offensive_stats.id
	AND stats.stat_coverage_type = 'sub_seasons'
	AND stats.stat_coverage_id = sub_seasons.id
	AND stats.stat_holder_type = 'persons'
	AND sub_seasons.season_id = seasons.id
	AND sub_seasons.sub_season_type = 'season-regular'
	AND seasons.season_key = :season_key
	AND display_names.entity_type = 'persons'
	AND display_names.entity_id = persons.id
	AND stats.stat_holder_id = persons.id
	AND baseball_offensive_stats.at_bats > :at_bats
ORDER BY
	baseball_offensive_stats.hits desc
	;

