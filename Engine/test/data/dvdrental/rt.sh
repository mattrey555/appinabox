CLASSPATH='server/libs/Library-1.0.jar:server/libs/gson-2.8.6.jar:server/libs/javax.servlet-api-4.0.1.jar:server/libs/postgresql-42.2.6.jar:server/build/classes/java/main' 
echo "classpath: $CLASSPATH" dvdrental.properties
java -cp $CLASSPATH com.appinabox.dvdrental.store_inventory.query.sql.store_inventoryMain dvdrental.properties > store_inventory.json
java -cp $CLASSPATH com.appinabox.dvdrental.select_film_info.query.sql.select_film_infoMain dvdrental.properties > select_film_info.json
java -cp $CLASSPATH com.appinabox.dvdrental.insert_actor.query.sql.insert_actorMain dvdrental.properties insert_actor_in.json > insert_actor.json
java -cp $CLASSPATH com.appinabox.dvdrental.select_actors.query.sql.select_actorsMain dvdrental.properties "Degeneres" > select_actors.json
java -cp $CLASSPATH com.appinabox.dvdrental.insert_film_actors.query.sql.insert_film_actorsMain dvdrental.properties insert_film_actors_in.json > insert_film_actors.json
java -cp $CLASSPATH com.appinabox.dvdrental.select_post_in_customers_film_name.query.sql.select_post_in_customers_film_nameMain dvdrental.properties customer_ids.json > select_post_in_customers_film_name.json
java -cp $CLASSPATH com.appinabox.dvdrental.payment.query.sql.paymentMain dvdrental.properties > payment.json
java -cp $CLASSPATH com.appinabox.dvdrental.select_all_film_nested.query.sql.select_all_film_nestedMain dvdrental.properties > select_all_film_nested.json
java -cp $CLASSPATH com.appinabox.dvdrental.film_query_limit.query.sql.film_query_limitMain dvdrental.properties 10 > film_query_limit.json
java -cp $CLASSPATH com.appinabox.dvdrental.select_actors_by_category2.query.sql.select_actors_by_category2Main dvdrental.properties > select_actors_by_category2.json
java -cp $CLASSPATH com.appinabox.dvdrental.store_info.query.sql.store_infoMain dvdrental.properties > store_info.json
java -cp $CLASSPATH com.appinabox.dvdrental.insert_query_film.query.sql.insert_query_filmMain dvdrental.properties insert_query_film_in.json > insert_query_film.json
java -cp $CLASSPATH com.appinabox.dvdrental.insert_query_film_multi.query.sql.insert_query_film_multiMain dvdrental.properties insert_query_film_multi_in.json > insert_query_film_multi.json
java -cp $CLASSPATH com.appinabox.dvdrental.film_query.query.sql.film_queryMain dvdrental.properties > film_query.json
java -cp $CLASSPATH com.appinabox.dvdrental.rental_by_date.query.sql.rental_by_dateMain dvdrental.properties > rental_by_date.json
java -cp $CLASSPATH com.appinabox.dvdrental.payment_case.query.sql.payment_caseMain dvdrental.properties > payment_case.json
java -cp $CLASSPATH com.appinabox.dvdrental.update_staff.query.sql.update_staffMain dvdrental.properties update_staff_in.json false > update_staff.json
java -cp $CLASSPATH com.appinabox.dvdrental.store_inventory3.query.sql.store_inventory3Main dvdrental.properties > store_inventory3.json
java -cp $CLASSPATH com.appinabox.dvdrental.store_inventory4.query.sql.store_inventory4Main dvdrental.properties > store_inventory4.json  
java -cp $CLASSPATH com.appinabox.dvdrental.select_post_in_customers.query.sql.select_post_in_customersMain dvdrental.properties customer_ids.json > select_post_in_customers.json
java -cp $CLASSPATH com.appinabox.dvdrental.store_inventory2.query.sql.store_inventory2Main dvdrental.properties > store_inventory2.json
java -cp $CLASSPATH com.appinabox.dvdrental.select_film_category.query.sql.select_film_categoryMain dvdrental.properties > select_film_category.json
java -cp $CLASSPATH com.appinabox.dvdrental.select_all_film_limit.query.sql.select_all_film_limitMain dvdrental.properties 10 > select_all_film_limit.json
java -cp $CLASSPATH com.appinabox.dvdrental.film_category_count.query.sql.film_category_countMain dvdrental.properties > film_category_count.json
java -cp $CLASSPATH com.appinabox.dvdrental.select_multiple.query.sql.select_multipleMain dvdrental.properties > select_multiple.json
java -cp $CLASSPATH com.appinabox.dvdrental.select_actors_by_category.query.sql.select_actors_by_categoryMain dvdrental.properties > select_actors_by_category.json
java -cp $CLASSPATH com.appinabox.dvdrental.select_in_customers.query.sql.select_in_customersMain dvdrental.properties 1 > select_in_customers.json
java -cp $CLASSPATH com.appinabox.dvdrental.select_all_film.query.sql.select_all_filmMain dvdrental.properties 8 > select_all_film.json
