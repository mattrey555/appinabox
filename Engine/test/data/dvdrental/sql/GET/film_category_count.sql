/* url: /filmcategorycount */
/* json-response: { category.name, category.category_id, filmCount } */
/* name-response: film_category_count */
/* package: com.appinabox.dvdrental.film_category_count */
/* endpoints: android */
SELECT category.name, category.category_id, COUNT(film.film_id) AS filmCount FROM category 
INNER JOIN film_category ON category.category_id = film_category.category_id
INNER JOIN film ON film.film_id = film_category.film_id
GROUP BY category.name;
