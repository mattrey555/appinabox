/* url: /film-query */
/* json-response: { category.name, category.category_id, films: { film.title, film.film_id }} */
/* name-response: film_query */
/* package: com.appinabox.dvdrental.film_query */
/* endpoints: android */
SELECT category.name, category.category_id, film.title, film.film_id FROM category 
INNER JOIN film_category ON category.category_id = film_category.category_id
INNER JOIN film ON film.film_id = film_category.film_id;
