/* url: /filmquery_limit?limit=${limit} */
/* json-response: { category.name, category.category_id, films : { film.title, film.film_id }} */
/* name-response: film_query_limit */
/* package: com.appinabox.dvdrental.film_query_limit */
/* endpoints: android */
SELECT category.name, category.category_id, film.title, film.film_id FROM category 
INNER JOIN film_category ON category.category_id = film_category.category_id
INNER JOIN (SELECT * FROM film LIMIT :limit) AS film ON film.film_id = film_category.film_id;
