/* url: /payment */
/* json-response: { customer.first_name, customer.last_name, customer.customer_id, amount : { double_amount, payment.rental_id, payment.payment_id }} */
/* name-response: payment */
/* package: com.appinabox.dvdrental.payment */
/* endpoints: android */
/* repository: paymentTest */
SELECT customer.first_name, customer.last_name, customer.customer_id, payment.amount*2 as double_amount, payment.rental_id, payment.payment_id 
FROM customer INNER JOIN payment ON customer.customer_id = payment.customer_id;
