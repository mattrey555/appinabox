/* url: /paymentcase */
/* json-response: { customer.first_name, customer.last_name, customer.customer_id, amount : { double_amount, rental_id, big_small, payment_id }} */
/* name-response: payment_case */
/* package: com.appinabox.dvdrental.payment_case */
/* endpoints: android */
SELECT customer.first_name, customer.last_name, customer.customer_id, payment.amount*2 as double_amount, rental_id, payment_id,
CASE
    WHEN payment.amount > 5 THEN 'big'
    ELSE 'small'
END AS big_small
FROM customer INNER JOIN payment ON customer.customer_id = payment.customer_id;
