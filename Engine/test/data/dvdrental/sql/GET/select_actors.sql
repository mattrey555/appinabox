/* url: /actorlastname?last_name=${last_name} */
/* json-response: { first_name, last_name, actor_id } */
/* name-response: select_actor */
/* package: com.appinabox.dvdrental.select_actors */
/* endpoints: android */
select first_name, last_name, actor_id from actor where last_name = :last_name
