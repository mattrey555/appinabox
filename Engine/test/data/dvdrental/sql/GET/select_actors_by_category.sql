/* url: /actorbycategory */
/* json-response: { actor.first_name, actor.last_name, actor.actor_id, categories : { category.name, category.category_id }} */
/* name-response: select_actors_by_category */
/* package: com.appinabox.dvdrental.select_actors_by_category */
/* endpoints: android */
SELECT actor.first_name, actor.last_name, actor.actor_id, category.name, category.category_id 
FROM actor 
INNER JOIN film_actor ON actor.actor_id = film_actor.actor_id
INNER JOIN film ON film_actor.film_id = film.film_id
INNER JOIN film_category ON film.film_id = film_category.film_id
INNER JOIN category ON film_category.category_id = category.category_id
ORDER BY actor.last_name;
