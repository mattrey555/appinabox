/* url: /allfilm?film_id=${film_id} */
/* json-response: { * } */
/* name-response: select_all_film */
/* package: com.appinabox.dvdrental.select_all_film */
/* endpoints: android */
SELECT * FROM film where film_id = :film_id;
