/* url: /allfilm_limit?limit=${limit} */
/* json-response: { * } */
/* name-response: select_all_film_limit */
/* package: com.appinabox.dvdrental.select_all_film_limit */
/* endpoints: android */
SELECT * FROM film LIMIT :limit
