/* url: /allfilm_nested */
/* json-response: { film_id, details : { title, description, rating }, rental_duration, rental_rate } */
/* name-response: select_all_film_nested */
/* package: com.appinabox.dvdrental.select_all_film_nested */
/* endpoints: android */
SELECT * FROM film
