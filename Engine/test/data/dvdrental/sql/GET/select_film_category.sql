/* url: /filmcategory */
/* json-response: { film.title, film.release_year, film.description, film.rating, film.film_id, categories : { category.name, category.category_id } } */
/* name-response: select_film_category */
/* package: com.appinabox.dvdrental.select_film_category */
/* endpoints: android */
SELECT film.title, film.release_year, film.description, film.rating, film.film_id, category.name, category.category_id 
FROM film 
INNER JOIN film_category ON film.film_id = film_category.film_id
INNER JOIN category ON film_category.category_id = category.category_id;
