/* url: /filminfo */
/* json-response: { film.title, film.release_year, film.description, film.rating, film.film_id, language, actors : { actor.first_name, actor.last_name, actor.actor_id }, categories : { category.name, category.category_id } } */
/* name-response: select_film_info */
/* package: com.appinabox.dvdrental.select_film_info */
/* endpoints: android */
/* film.release_year.widget: DatePicker */
SELECT film.title, film.release_year, film.description, film.rating, film.film_id, actor.first_name, actor.last_name, actor.actor_id, category.name, category.category_id, language.name as language, language.language_id 
FROM film 
INNER JOIN film_actor ON film.film_id = film_actor.film_id
INNER JOIN actor ON film_actor.actor_id = actor.actor_id
INNER JOIN film_category ON film.film_id = film_category.film_id
INNER JOIN category ON film_category.category_id = category.category_id
INNER JOIN language ON film.language_id = language.language_id;
