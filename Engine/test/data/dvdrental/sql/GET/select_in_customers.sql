/* url: /select_in_customers?customer_id=${customer_id} */
/* json-response: { * } */
/* name-response: customers */
/* package: com.appinabox.dvdrental.select_in_customers */
/* endpoints: android */
SELECT * from customer where customer_id in (:customer_id);
