/* url: /select_post_in_customers */
/* json-request: { customer_id } */
/* name-request: customers_request */
/* json-response: { customer.customer_id, customer.first_name, customer.last_name, rental : { rental.* }} */
/* name-response: customers_response */
/* package: com.appinabox.dvdrental.select_post_in_customers */
/* endpoints: android */
SELECT customer.customer_id, customer.first_name, customer.last_name, rental.inventory_id, rental.rental_id, rental.rental_date  FROM customer INNER JOIN rental ON customer.customer_id = rental.customer_id WHERE customer.customer_id IN (:customer_id);
