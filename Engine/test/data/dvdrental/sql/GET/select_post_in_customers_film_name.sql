/* url: /select_post_in_customers_film_name */
/* json-request: { customer_id } */
/* name-request: customers_request */
/* json-response: { customer.*, rental : { rental.*, film.title }} */
/* name-response: customers_response */
/* package: com.appinabox.dvdrental.select_post_in_customers_film_name */
/* endpoints: android */
SELECT customer.customer_id, customer.first_name, customer.last_name, rental.inventory_id, rental.rental_id, rental.rental_date, film.title
FROM customer 
INNER JOIN rental ON customer.customer_id = rental.customer_id 
INNER JOIN inventory ON rental.inventory_id = inventory.inventory_id
INNER JOIN film ON inventory.film_id = film.film_id 
WHERE customer.customer_id IN (:customer_id);
