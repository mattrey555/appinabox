/* url: /storeinventory4 */
/* json-response: { the_store_id, city, address.address, district, address.address_id, film_inventory : { inventory_id, film.title, film.release_year, film.last_update, film.film_id }} */
/* name-response: select_film_inventory */
/* package: com.appinabox.dvdrental.store_inventory4 */
/* endpoints: android */
/* timestamp: store.last_update */
SELECT store.store_id as the_store_id, city.city, address.address, address.district, address.address_id, inventory_select.inventory_id, film.title, film.release_year, film.last_update, film.film_id 
FROM store 
INNER JOIN address ON store.address_id = address.address_id 
INNER JOIN city ON address.city_id = city.city_id 
INNER JOIN (select * FROM inventory) AS inventory_select ON store.store_id = inventory_select.store_id 
INNER JOIN film on inventory_select.film_id = film.film_id
WHERE store.deleted is false and address.deleted is false
