/* url: /insert/insert_actor */
/* json-request: { original_actor_id, first_name, last_name } */
/* name-request: insert_actor */
/* name: insert_actor */
/* name-response: actor */
/* json-response: { actor_id, first_name, last_name } */
/* package: com.appinabox.dvdrental.insert_actor */
/* endpoints: android */
DELETE FROM actor where actor_id = :original_actor_id;
INSERT INTO actor (first_name, last_name) VALUES (:first_name, :last_name) returning actor_id;
SELECT first_name, last_name, actor_id FROM actor WHERE actor_id IN (:actor_id);
