/* url: /insert/insert_film_actors */
/* json-request: { title, description, release_year, language_id, rental_duration, rental_rate, length, replacement_cost, rating } */
/* name-request: insert_film_actors */
/* name: insert_film_actors */
/* type: rating varchar */
/* package: com.appinabox.dvdrental.insert_film_actors */
/* endpoints: android */
INSERT INTO film (title, description, release_year, language_id, 
		  rental_duration, rental_rate, length, replacement_cost, rating)
VALUES (:title, :description, :release_year, :language_id, 
	:rental_duration, :rental_rate, :length, :replacement_cost, 
	CAST(:rating as mpaa_rating));
