/* url: /insert_query_film */
/* name-request: insert_film */
/* json-request: { title, description, release_year, language_id, rental_duration, rental_rate, length, replacement_cost, rating, actor_film : { actor_id }} */
/* name-response: query_film */
/* json-response: { film_id } */
/* package: com.appinabox.dvdrental.insert_query_film */
/* type: rating varchar */
/* endpoints: android */
INSERT INTO film (title, description, release_year, language_id, rental_duration, rental_rate, length, replacement_cost, rating) VALUES (:title, :description, :release_year, :language_id, :rental_duration, :rental_rate, :length, :replacement_cost, CAST(:rating AS mpaa_rating)) RETURNING film_id;
INSERT INTO film_actor (film_id, actor_id) VALUES (:film_id, :actor_id);
SELECT film_id FROM film WHERE film_id in (:film_id);
