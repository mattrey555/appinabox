/* url: /insert_query_film_multi */
/* name-request: insert_film */
/* json-request: { title, description, release_year, language_id, rental_duration, rental_rate, length, replacement_cost, rating, actor_film : { actor_id }} */
/* name-response: query_film */
/* json-response: { film : { film_id, title, description }, actors : { first_name, last_name }} */
/* package: com.appinabox.dvdrental.insert_query_film_multi */
/* name: insert_query_film_multi */
/* type: rating varchar */
/* endpoints: android */
INSERT INTO film (title, description, release_year, language_id, rental_duration, rental_rate, length, replacement_cost, rating) VALUES (:title, :description, :release_year, :language_id, :rental_duration, :rental_rate, :length, :replacement_cost, CAST(:rating AS mpaa_rating)) RETURNING film_id;
INSERT INTO film_actor (film_id, actor_id) VALUES (:film_id, :actor_id);
SELECT film_id, title, description FROM film WHERE film_id in (:film_id);
SELECT first_name, last_name FROM actor INNER JOIN film_actor ON actor.actor_id = film_actor.actor_id INNER JOIN film on film_actor.film_id = film.film_id where film.film_id in (:film_id);
