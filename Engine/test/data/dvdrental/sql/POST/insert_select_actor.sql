/* url: /insert/insert_select_actor */
/* json-request: { insert : { first_name, last_name }, select : { actor_id }} */
/* name-request: insert_select_actor */
/* name: insert_select_actor */
/* name-response: actor */
/* json-response: { actor_id, first_name, last_name } */
/* package: com.appinabox.dvdrental.insert_select_actor */
/* endpoints: android */
INSERT INTO actor (first_name, last_name) VALUES (:insert.first_name, :insert.last_name);
SELECT first_name, last_name, actor_id FROM actor WHERE actor_id IN (:select.actor_id);
