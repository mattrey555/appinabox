/* url: /staff/set-active?active=${active} */
/* json-request: { staff_id } */
/* name-request: update_staff */
/* json-response: { staff_id, active } */
/* name-response: staff_active */
/* package: com.appinabox.dvdrental.update_staff */
/* endpoints: android */
/* timestamp: staff.last_update */
UPDATE staff SET active=:active WHERE staff_id=:staff_id;
SELECT staff_id, active FROM staff WHERE staff_id IN (:staff_id);
