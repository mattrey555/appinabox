package com.apptechnik.appinabox.library.sql;

import com.apptechnik.appinabox.library.util.StringUtils;

import java.util.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Date;
import java.sql.Blob;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.sql.rowset.serial.SerialBlob;


/**
 * SQL statement which takes : prefixed JdbcNamedParameters and supports lists of primitive
 * values for SELECT IN expressions
 */
public class NamedParamStatement {
	private static final String NAMED_PARAM_REGEXP = ":[_A-Za-z][A-Za-z0-9_]*";
	private PreparedStatement prepStmt;
	private final Connection conn;
	private String originalSql;
	private Map<String, Object> values;

    public NamedParamStatement(Connection conn, String sql) throws SQLException {
 		if (conn == null) {
			throw new RuntimeException("NamedParamStatement: database connection is null");
		}
 		this.conn = conn;
		originalSql = sql;
		values = new HashMap<String, Object>();
        prepStmt = conn.prepareStatement(sql);
    }

	/**
	 * Return the prepared statement wrapped by this class
	 * @return Prepared statement.
	 */
    public PreparedStatement getPreparedStatement() {
        return prepStmt;
    }

	/**
	 * Execute the query and return the result set.
	 * @return ResultSet
	 */
    public ResultSet executeQuery() throws SQLException {
		applyValues();
        return prepStmt.executeQuery();
    }

	public boolean execute() throws SQLException {
		applyValues();
		return prepStmt.execute();
	}

    public void close() throws SQLException {
        prepStmt.close();
    }

    public void setBoolean(String name, boolean value) throws SQLException {        
		values.put(name, value);
    }

	public void setList(String name, List value) throws SQLException {
		values.put(name, value);
	}

    public void setInt(String name, int value) throws SQLException {        
		values.put(name, value);
    }

    public void setLong(String name, long value) throws SQLException {        
		values.put(name, value);
    }

    public void setString(String name, String value) throws SQLException {        
		values.put(name, value);
    }

    public void setFloat(String name, float value) throws SQLException {        
		values.put(name, value);
    }

    public void setDouble(String name, double value) throws SQLException {        
		values.put(name, value);
    }

    public void setShort(String name, short value) throws SQLException {        
		values.put(name, value);
    }

    public void setDate(String name, Date value) throws SQLException {        
		values.put(name, value);
    }

	public void setBlob(String name, Blob blob) throws SQLException {
		values.put(name, blob);
	}

	public void setBytes(String name, byte[] bytes) throws SQLException {
		values.put(name, bytes);
	}

	public void setBytesList(String name, List<byte[]> bytesList) throws SQLException {
		values.put(name, bytesList);
	}

	public void setBytesFromBase64(String name, String encodedBytes) throws SQLException {
		Base64.Decoder decoder = Base64.getDecoder();
		values.put(name, Base64.getDecoder().decode(encodedBytes));
	}

	public void setBytesFromBase64List(String name, List<String> encodedBytesList) throws SQLException {
		Base64.Decoder decoder = Base64.getDecoder();
		List<byte[]> valueList = new ArrayList<byte[]>(encodedBytesList.size());
		for (String encodedBytes : encodedBytesList) {
			valueList.add(decoder.decode(encodedBytes));
		}
		values.put(name, valueList);
	}

	public void setBlobFromBase64(String name, String encodedBytes) throws SQLException {
		Base64.Decoder decoder = Base64.getDecoder();
		Blob blob = new SerialBlob(decoder.decode(encodedBytes));
		values.put(name, blob);
	}

	private void applyValues() throws SQLException {

		// get the indices of the :name parameters.
		Pattern pattern = Pattern.compile(NAMED_PARAM_REGEXP);
		Matcher matcher = pattern.matcher(originalSql);
		String sql = originalSql;
		List<Object> valueList = new ArrayList<Object>();
		while (matcher.find()) {
			String identifier = matcher.group();
			String fieldName = identifier.substring(1); // remove the ":"

			if (!values.containsKey(fieldName)) {
				throw new SQLException(identifier + " is a duplicate named parameter");
			}
			Object value = values.get(fieldName);
			if (value instanceof List) {
				List list = (List) value;
				if (list.isEmpty()) {
					throw new SQLException("parameter " + fieldName + " was empty");
				} else {
					sql = sql.replace(identifier, StringUtils.repeat("?", ",", list.size()));
				}
			} else {
				sql = sql.replace(identifier, "?");
			}
			valueList.add(value);
		}
		prepStmt = conn.prepareStatement(sql);

		// PreparedStatements index starts at 1
		int paramIndex = 1;
		for (Object value : valueList) {
			if (value instanceof List) {
				List list = (List) value;
				for (int listIndex = 0; listIndex < list.size(); listIndex++) {
					Object element = list.get(listIndex);
					setValue(prepStmt, paramIndex, element);
					paramIndex++;
				}
			} else {
				setValue(prepStmt, paramIndex, value);
				paramIndex++;
			}
		}
	}

	private static void setValue(PreparedStatement prepStmt, int index, Object value) throws SQLException {
    	if (value instanceof Boolean) {
			prepStmt.setBoolean(index, (Boolean) value);
		} else if (value instanceof Integer) {
			prepStmt.setInt(index, (Integer) value);
		} else if (value instanceof Long) {
			prepStmt.setLong(index, (Long) value);
		} else if (value instanceof String) {
			prepStmt.setString(index, (String) value);
		} else if (value instanceof Float) {
			prepStmt.setFloat(index, (Float) value);
		} else if (value instanceof Double) {
			prepStmt.setDouble(index, (Double) value);
		} else if (value instanceof Short) {
			prepStmt.setShort(index, (Short) value);
		} else if (value instanceof Date) {
			prepStmt.setDate(index, (Date) value);
		} else if (value instanceof Blob) {
			prepStmt.setBlob(index, (Blob) value);
		} else if (value instanceof byte[]) {
			prepStmt.setBytes(index, (byte[]) value);
		} else {
    		throw new SQLException("parameter at index " + index + " in " + prepStmt +
								   " is of unsuppored type " + value.getClass());
		}
	}

	public void clearParameters() throws SQLException {
		prepStmt.clearParameters();
	}


	public Collection<String> getFields() {
		return values.keySet();
	}

	public ResultSet getResultSet() throws SQLException {
		return prepStmt.getResultSet();
	}


	/**
	 * identifiers start with a : and are a string of letters or digits, or underscores
	 * or characters prefixed with '\', so we can use parameters like X-User-Agent
	 * also "." characters, which we've added so we can reference java classes.
	 * @param sql sql to scan
	 * @param colonIndex start of identifier.
	 * @return index of character past end of identifier.
	 */
	private int identifierEnd(String sql, int colonIndex) {
		for (int i = colonIndex + 1; i < sql.length(); i++) {
			char ch = sql.charAt(i);

			// escape character. TEST THIS
			if (ch == '\\') {
				i++;
				continue;
			}

			// Extend identifier to handle "." syntax for java object references.
			if (!Character.isLetterOrDigit(ch) && (ch != '_') && (ch != '.')) {
				return i;
			}
		}

		// reached end of string.
		return sql.length();
	}

	public static String replaceMapStrings(String sql, Map<String, String> map) {
		for (Map.Entry<String, String> entry : map.entrySet()) {
			sql = sql.replace(entry.getValue(), entry.getKey());
		}
		return sql;
	}
}
