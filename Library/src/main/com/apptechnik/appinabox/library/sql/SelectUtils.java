package com.apptechnik.appinabox.library.sql;

import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * General purpose SQL SELECT statement utilties.
 */
public class SelectUtils {
	private SelectUtils() {
	}

	public static int selectSingleInt(Connection conn, String table, String name) throws SQLException {
		String select = "SELECT " + name + " FROM " + table + " LIMIT 1";
		PreparedStatement ps = conn.prepareStatement(select);
		ResultSet rs = ps.executeQuery();
		try {
			if (rs.next()) {
				return rs.getInt(1);
			} else {
				throw new SQLException("no results for " + select);
			}
		} finally {
			rs.close();
			ps.close();
		}
	}

	public static String selectSingleString(Connection conn, String table, String name) throws SQLException {
		String select = "SELECT " + name + " FROM " + table + " LIMIT 1";
		PreparedStatement ps = conn.prepareStatement(select);
		ResultSet rs = ps.executeQuery();
		try {
			if (rs.next()) {
				return rs.getString(1);
			} else {
				throw new SQLException("no results for " + select);
			}
		} finally {
			rs.close();
			ps.close();
		}
	}

	public static float selectSingleFloat(Connection conn, String table, String name) throws SQLException {
		String select = "SELECT " + name + " FROM " + table + " LIMIT 1";
		PreparedStatement ps = conn.prepareStatement(select);
		ResultSet rs = ps.executeQuery();
		try {
			if (rs.next()) {
				return rs.getFloat(1);
			} else {
				throw new SQLException("no results for " + select);
			}
		} finally {
			rs.close();
			ps.close();
		}
	}

	public static double selectSingleDouble(Connection conn, String table, String name) throws SQLException {
		String select = "SELECT " + name + " FROM " + table + " LIMIT 1";
		PreparedStatement ps = conn.prepareStatement(select);
		ResultSet rs = ps.executeQuery();
		try {
			if (rs.next()) {
				return rs.getDouble(1);
			} else {
				throw new SQLException("no results for " + select);
			}
		} finally {
			rs.close();
			ps.close();
		}
	}

	public static byte[] selectSingleByteArray(Connection conn, String table, String name) throws SQLException {
		String select = "SELECT " + name + " FROM " + table + " LIMIT 1";
		PreparedStatement ps = conn.prepareStatement(select);
		ResultSet rs = ps.executeQuery();
		try {
			if (rs.next()) {
				return rs.getBytes(1);
			} else {
				throw new SQLException("no results for " + select);
			}
		} finally {
			rs.close();
			ps.close();
		}
	}

	public static byte selectSingleByte(Connection conn, String table, String name) throws SQLException {
		String select = "SELECT " + name + " FROM " + table + " LIMIT 1";
		PreparedStatement ps = conn.prepareStatement(select);
		ResultSet rs = ps.executeQuery();
		try {
			rs.next();
			return rs.getByte(1);
		} finally {
			rs.close();
			ps.close();
		}
	}

	public static boolean selectSingleBoolean(Connection conn, String table, String name) throws SQLException {
		String select = "SELECT " + name + " FROM " + table + " LIMIT 1";
		PreparedStatement ps = conn.prepareStatement(select);
		ResultSet rs = ps.executeQuery();
		try {
			rs.next();
			return rs.getBoolean(1);
		} finally {
			rs.close();
			ps.close();
		}
	}

	public static Date selectSingleDate(Connection conn, String table, String name) throws SQLException {
		String select = "SELECT " + name + " FROM " + table + " LIMIT 1";
		PreparedStatement ps = conn.prepareStatement(select);
		ResultSet rs = ps.executeQuery();
		try {
			rs.next();
			return rs.getDate(1);
		} finally {
			rs.close();
			ps.close();
		}
	}


	public static List<Integer> readIntList(ResultSet rs, String columnName) throws SQLException {
		List<Integer> list = new ArrayList<Integer>();
		while (rs.next()) {
			int val = rs.getInt(columnName);
			list.add(val);
		}
		return list;
	}

	public static List<Integer> selectIntList(Connection conn, String table, String columnName) throws SQLException {
		String selectStr = "SELECT " + columnName + " FROM " + table;
		PreparedStatement ps = conn.prepareStatement(selectStr);
		ResultSet rs = ps.executeQuery();
		return readIntList(rs, columnName);
	}

	public static List<String> readStringList(ResultSet rs, String columnName) throws SQLException {
		List<String> list = new ArrayList<String>();
		while (rs.next()) {
			String val = rs.getString(columnName);
			list.add(val);
		}
		return list;
	}

	public static List<String> selectStringList(Connection conn, String table, String columnName) throws SQLException {
		String selectStr = "SELECT " + columnName + " FROM " + table;
		PreparedStatement ps = conn.prepareStatement(selectStr);
		ResultSet rs = ps.executeQuery();
		return readStringList(rs, columnName);
	}

}
			
