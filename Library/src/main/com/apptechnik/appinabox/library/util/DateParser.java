package com.apptechnik.appinabox.library.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Parses a date from a collection of different date formats
 */
public class DateParser {
	private static final String FORMAT_DATE_UTC_MILLIS = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	private static final String FORMAT_DATE_UTC_SECONDS = "yyyy-MM-dd'T'HH:mm:ss'Z'";
	private static final String FORMAT_DATE_ZULU_SECONDS = "yyyy-MM-dd'T'HH:mm:ssZ";
	private static final String FORMAT_DATE_YMD = "yyyy-MM-dd";
	private static final String FORMAT_DATE_YM = "yyyy-MM";
	private static final String FORMAT_DATE_Y = "yyyy";
	private static final String FORMAT_DATE_MDY = "MM-dd-yyyy";
	private static final String FORMAT_DATE_MY = "MM-yyyy";
	private static final String FORMAT_DATE_YMD_HMS = "yyyy-MM-dd' 'HH:mm:ss";

    // "2015-08-31T15:56:05Z"
    private static final String REG_8061_SSZ = "\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}Z";

    // "2015-08-31T15:56:05.000Z"
    private static final String REG_8061_SSSZ = "\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}\\.\\d{3}Z";

    // "2015-09-23T20:10:12+00:00"
    private static final String REG_8061_zz_zz = "\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}[\\+\\-]\\d{2}:\\d{2}";

    // "2015-09-23T20:10:12+0000"
    private static final String REG_8061_zzzz = "\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}[\\+\\-]\\d{2}\\d{2}";

    // "2015-09-23"
    private static final String REG_YYYY_MM_DD = "\\d{4}-\\d{1,2}-\\d{1,2}";

    // "2015-09"
    private static final String REG_YYYY_MM = "\\d{4}-\\d{1,2}";

    // "2015"
    private static final String REG_YYYY = "\\d{4}";

    // "01-29-2015"
    private static final String REG_MM_DD_YYYY = "\\d{2}-\\d{2}-\\d{4}";

    // "01-2015"
    private static final String REG_MM_YYYY = "\\d{2}-\\d{4}";

    // "2016-06-13 20:11:04
    private static final String REG_YYYY_MM_DD_HH_MM_SS = "\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}";

    public static Date parse(String value) throws Exception {
        SimpleDateFormat format = new SimpleDateFormat();
		boolean useDateFormat = true;
        if (value.matches(REG_8061_SSSZ)) {
            format.applyPattern(FORMAT_DATE_UTC_MILLIS);
            return format.parse(value);
        } else if (value.matches(REG_8061_zz_zz)) {
            int last = value.lastIndexOf(":");
            String s1 = value.substring(0, last);
            String s2 = value.substring(last+1);
            value = s1 + s2;
            format.applyPattern(FORMAT_DATE_ZULU_SECONDS);
            return format.parse(value);
        } else if (value.matches(REG_8061_SSZ)) {
            format.applyPattern(FORMAT_DATE_UTC_SECONDS);
            return format.parse(value);
        } else if (value.matches(REG_8061_zzzz)) {
            format.applyPattern(FORMAT_DATE_ZULU_SECONDS);
            return format.parse(value);
        } else if (value.matches(REG_YYYY_MM_DD)) {
            format.applyPattern(FORMAT_DATE_YMD);
            return format.parse(value);
        } else if (value.matches(REG_YYYY_MM)) {
            format.applyPattern(FORMAT_DATE_YM);
            return format.parse(value);
        } else if (value.matches(REG_YYYY)) {
            format.applyPattern(FORMAT_DATE_YM);
            return format.parse(value);
        } else if (value.matches(REG_MM_DD_YYYY)) {
            format.applyPattern(FORMAT_DATE_MDY);
            return format.parse(value);
        } else if (value.matches(REG_MM_YYYY)) {
            format.applyPattern(FORMAT_DATE_MY);
            return format.parse(value);
        } else if (value.matches(REG_YYYY_MM_DD_HH_MM_SS)) {
            format.applyPattern(FORMAT_DATE_YMD_HMS);
            return format.parse(value);
        } else {
            useDateFormat = false;
            long d = Long.parseLong(value);
            return new Date(d);
        }
    }
}
