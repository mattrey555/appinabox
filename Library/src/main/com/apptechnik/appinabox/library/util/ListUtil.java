package com.apptechnik.appinabox.library.util;

import java.util.List;
import java.util.stream.Collectors;

public class ListUtil {
    public interface Converter<FROM, TO> {
        public TO convert(FROM from);
    }

    public static<FROM, TO> List<TO> convert(List<FROM> list, Converter<FROM, TO> converter) {
        return list.stream().map(item -> converter.convert(item)).collect(Collectors.toList());
    }

    public static class ConvertIntToByte implements Converter<Integer, Byte> {
        public Byte convert(Integer i) {
            return i.byteValue();
        }
    }

    public static class ConvertIntToShort implements Converter<Integer, Short> {
        public Short convert(Integer i) {
            return i.shortValue();
        }
    }

    public static class ConvertIntToLong implements Converter<Integer, Long> {
        public Long convert(Integer i) {
            return i.longValue();
        }
    }

    public static class ConvertIntToFloat implements Converter<Integer, Float> {
        public Float convert(Integer i) {
            return i.floatValue();
        }
    }

    public static class ConvertIntToDouble implements Converter<Integer, Double> {
        public Double convert(Integer i) {
            return i.doubleValue();
        }
    }

    public static class ConvertShortToByte implements Converter<Short, Byte> {
        public Byte convert(Short i) {
            return i.byteValue();
        }
    }

    public static class ConvertShortToInteger implements Converter<Short, Integer> {
        public Integer convert(Short i) {
            return i.intValue();
        }
    }

    public static class ConvertShortToLong implements Converter<Short, Long> {
        public Long convert(Short i) {
            return i.longValue();
        }
    }

    public static class ConvertShortToFloat implements Converter<Short, Float> {
        public Float convert(Short i) {
            return i.floatValue();
        }
    }

    public static class ConvertShortToDouble implements Converter<Short, Double> {
        public Double convert(Short i) {
            return i.doubleValue();
        }
    }

    public static class ConvertByteToShort implements Converter<Byte, Short> {
        public Short convert(Byte i) {
            return i.shortValue();
        }
    }

    public static class ConvertByteToInteger implements Converter<Byte, Integer> {
        public Integer convert(Byte i) {
            return i.intValue();
        }
    }

    public static class ConvertByteToLong implements Converter<Byte, Long> {
        public Long convert(Byte i) {
            return i.longValue();
        }
    }

    public static class ConvertByteToFloat implements Converter<Byte, Float> {
        public Float convert(Byte i) {
            return i.floatValue();
        }
    }

    public static class ConvertByteToDouble implements Converter<Byte, Double> {
        public Double convert(Byte i) {
            return i.doubleValue();
        }
    }


    public static class ConvertFloatToShort implements Converter<Float, Short> {
        public Short convert(Float i) {
            return i.shortValue();
        }
    }

    public static class ConvertFloatToInteger implements Converter<Float, Integer> {
        public Integer convert(Float i) {
            return i.intValue();
        }
    }

    public static class ConvertFloatToLong implements Converter<Float, Long> {
        public Long convert(Float i) {
            return i.longValue();
        }
    }

    public static class ConvertFloatToByte implements Converter<Float, Byte> {
        public Byte convert(Float i) {
            return i.byteValue();
        }
    }

    public static class ConvertFloatToDouble implements Converter<Float, Double> {
        public Double convert(Float i) {
            return i.doubleValue();
        }
    }


    public static class ConvertDoubleToShort implements Converter<Double, Short> {
        public Short convert(Double i) {
            return i.shortValue();
        }
    }

    public static class ConvertDoubleToInteger implements Converter<Double, Integer> {
        public Integer convert(Double i) {
            return i.intValue();
        }
    }

    public static class ConvertDoubleToLong implements Converter<Double, Long> {
        public Long convert(Double i) {
            return i.longValue();
        }
    }

    public static class ConvertDoubleToByte implements Converter<Double, Byte> {
        public Byte convert(Double i) {
            return i.byteValue();
        }
    }

    public static class ConvertDoubleToFloat implements Converter<Double, Float> {
        public Float convert(Double i) {
            return i.floatValue();
        }
    }


}
