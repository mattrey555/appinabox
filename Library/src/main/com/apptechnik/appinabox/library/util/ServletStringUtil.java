package com.apptechnik.appinabox.library.util;

public class ServletStringUtil {

    /**
     * Given a string s, for example /of/this/form/with/delimeters, and a delimeter,
     * return the "indexethed" string.  No escapes. No quotes. No Capes!
     * @param s source string
     * @param delim delimiting string.
     * @param index referred index.
     * @return indexed stirng or null
     */
    public static String getDelimElement(String s, String delim, int index) {
        int ichStart;
        for (ichStart = 0; ichStart < s.length(); ichStart++) {
            if (s.startsWith(delim, ichStart)) {
                if (index-- <= 0) {
                    break;
                }
            }
        }
        if (ichStart < s.length()) {
            int ichEnd;
            for (ichEnd = ichStart + 1; ichEnd < s.length(); ichEnd++) {
                if (s.startsWith(delim, ichEnd)) {
                    break;
                }
            }
            return s.substring(ichStart + 1, ichEnd);
        }
        return null;
    }
}
