package com.apptechnik.appinabox.library.util;

public class ServletUtils {
	private ServletUtils() {
	}

	public static boolean nullTestEquals(Object a, Object b) {
		if ((a == null) && (b == null)) {
			return true;
		}
		if ((a == null) || (b == null)) {
			return false;
		}
		return a.equals(b);
	}

	public static String jsonException(String errorCode, Exception ex) {
		return "{ \"errorCode\" : \"" + errorCode + "\"" + "\n" +
				  "\"message\" : \"" + ex.getMessage() + "\"" + "}"; 
	}
}

