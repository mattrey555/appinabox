package com.apptechnik.appinabox.library.util;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * String utilities used for parsing command-line arguments.
 */
public class StringUtils {
	private StringUtils() {
	}

	public static List<Integer> parseIntegerList(String s, String delim) throws NumberFormatException {
		return Arrays.stream(s.split(delim)).map(el -> Integer.valueOf(el.trim())).collect(Collectors.toList());
	}

	public static List<Integer> parseIntegerList(String s) throws NumberFormatException {
		return parseIntegerList(s, ",");
	}

	public static List<Boolean> parseBooleanList(String s, String delim) throws NumberFormatException {
		return Arrays.stream(s.split(delim)).map(el -> Boolean.valueOf(el.trim())).collect(Collectors.toList());
	}

	public static List<Boolean> parseBooleanList(String s) throws NumberFormatException {
		return parseBooleanList(s, ",");
	}

	public static List<Short> parseShortList(String s, String delim) throws NumberFormatException {
		return Arrays.stream(s.split(delim)).map(el -> Short.valueOf(el.trim())).collect(Collectors.toList());
	}

	public static List<Short> parseShortList(String s) throws NumberFormatException {
		return parseShortList(s, ",");
	}

	public static List<Long> parseLongList(String s, String delim) throws NumberFormatException {
		return Arrays.stream(s.split(delim)).map(el -> Long.valueOf(el.trim())).collect(Collectors.toList());
	}

	public static List<Long> parseLongList(String s) throws NumberFormatException {
		return parseLongList(s, ",");
	}

	public static List<Float> parseFloatList(String s, String delim) throws NumberFormatException {
		return Arrays.stream(s.split(delim)).map(el -> Float.valueOf(el.trim())).collect(Collectors.toList());
	}

	public static List<Float> parseFloatList(String s) throws NumberFormatException {
		return parseFloatList(s, ",");
	}

	public static List<Double> parseDoubleList(String s, String delim) throws NumberFormatException {
		return Arrays.stream(s.split(delim)).map(el -> Double.valueOf(el.trim())).collect(Collectors.toList());
	}

	public static List<Double> parseDoubleList(String s) throws NumberFormatException {
		return parseDoubleList(s, ",");
	}

	public static List<byte[]> parseByteArrayList(String s, String delim) {
		Base64.Decoder decoder = Base64.getDecoder();
		return Arrays.stream(s.split(delim)).map(el -> decoder.decode(el.trim())).collect(Collectors.toList());
	}

	public static List<byte[]> parseByteArrayList(String s) {
		return parseByteArrayList(s, ",");
	}

	public static List<Date> parseDateList(String s, String delim) throws Exception {
		String[] vec = s.split(delim);
		List<Date> dateList = new ArrayList<Date>();
		for (String el : vec) {
			dateList.add(DateParser.parse(el.trim()));
		}
		return dateList;
	}

	public static List<Date> parseDateList(String s) throws Exception {
		return parseDateList(s, ",");
	}

	/**
	 * Used for extracting values from headers, for example:
	 * Authorization: bearer ${bearer-token}
	 * actual string to match is bearer ${bearer-token}
	 * so "bearer WFUHIUWEFUUWEF" maps "bearer-token" to WFUHIUWEFUUWE.
	 * Given a var format of the form: text ${var1} ${var2} more-text and a string of the form:
	 * text value1 value2 more-text, create a map with var1=value1, var2=value2
	 * The scan is for the text before and text after the ${variable}, hardcoded, no reg
	 * @param format variable format
	 * @param s string to match against
	 * @return
	 */
	public static Map<String, String> extractValues(String format, String s) {
		Map<String, String> map = new HashMap<String, String>();
		int ich = 0;
		int ichFormat = 0;
		while (ich < s.length() && ichFormat < format.length()) {
			if (format.startsWith("${", ichFormat)) {
				int endVar = format.indexOf('}', ichFormat + 2);
				String variable = format.substring(ichFormat + 2, endVar == -1 ? format.length() : endVar);
				ichFormat = endVar + 1;
				int endVal = ichFormat < format.length() ? s.indexOf(format.charAt(ichFormat), ich) : s.length();
				String value = s.substring(ich, endVal);
				ich = endVal;
				map.put(variable, value);
			} else {
				ich++;
				ichFormat++;
			}
		}
		return map;
	}

	/**
	 * Same, except with a named varable.
	 * @param format variable format
	 * @param var variable to match
	 * @param s string to match against
	 * @return
	 */
	public static String extractValue(String format, String var, String s) {
		Map<String, String> map = new HashMap<String, String>();
		int ich = 0;
		int ichFormat = 0;
		while (ich < s.length() && ichFormat < format.length()) {
			if (format.startsWith("${", ichFormat)) {
				int endVar = format.indexOf('}', ichFormat + 2);
				String varCand = format.substring(ichFormat + 2, endVar == -1 ? format.length() : endVar);
				ichFormat = endVar + 1;
				int endVal = ichFormat < format.length() ? s.indexOf(format.charAt(ichFormat), ich) : s.length();
				String value = s.substring(ich, endVal);
				ich = endVal;
				if (var.equals(varCand)) {
					return value;
				}
			} else {
				ich++;
				ichFormat++;
			}
		}
		return null;
	}

	/**
	 * Given a string s, return a string with s repeated n times.
	 * @param s
	 * @param n
	 * @return
	 */
	public static String repeat(String s, String delim, int n) {
		StringBuilder sb = new StringBuilder(s.length() * n);
		for (int i = 0; i < n; i++) {
			if (i != 0) {
				sb.append(delim);
			}
			sb.append(s);
		}
		return sb.toString();
	}
}
