package com.apptechnik.appinabox.sql;

import com.apptechnik.appinabox.library.sql.NamedParamStatement;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/*
 * Database initialization for a servlet
 * ex jdbc:postgresql://localhost
 * postgresql://[user[:password]@][netloc][:port][/dbname][?param1=value1&...]

 */
public class NamedParamStatementTest {
    private static final String DB_CONN = "jdbc:postgresql://localhost";
    private static final String DB_DATABASE = "purchases";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "FiatX1/9";
    private static final String STATEMENT1 =
        "SELECT SUM(purchases.price) as purchase_total, COUNT(purchases.purchase_id) as purchase_count, customers.name as customer_name, customers.customer_id, stores.name as store_name " +
        " FROM purchases INNER JOIN stores ON purchases.store_id = stores.store_id " +
        " INNER JOIN customers on purchases.customer_id = customers.customer_id " +
        " WHERE customers.name = :customer_name GROUP BY stores.store_id, customers.customer_id;";
    private static final String STATEMENT2 =
            "SELECT SUM(purchases.price) as purchase_total, COUNT(purchases.purchase_id) as purchase_count, customers.name as customer_name, customers.customer_id, stores.name as store_name " +
                    " FROM purchases INNER JOIN stores ON purchases.store_id = stores.store_id " +
                    " INNER JOIN customers on purchases.customer_id = customers.customer_id " +
                    " WHERE customers.name = :customer_name AND purchases.store_id = :store_id " +
                    " GROUP BY stores.store_id, customers.customer_id;";

    private static final String STATEMENT3 =
            "INSERT INTO film (title, description, release_year, language_id, rental_duration, rental_rate, length, replacement_cost, rating) " +
            "VALUES (:title, :description, :release_year, :language_id, :rental_duration, :rental_rate, :length, :replacement_cost, CAST(:rating AS mpaa_rating)) RETURNING film_id";

    @Test
    public void testNamedParamStatement() throws Exception {
        Connection dbConnection = null;
        try {
            dbConnection = DriverManager.getConnection(DB_CONN + "/" + DB_DATABASE, DB_USER, DB_PASSWORD);
        } catch (Exception ex) {
            System.out.println("database server connection failed, short-circuiting test");
            return;
        }
        NamedParamStatement nps1 = new NamedParamStatement(dbConnection, STATEMENT1);
        nps1.setString("customer_name", "Darth Vader");
        ResultSet rs1 = nps1.executeQuery();

        NamedParamStatement nps2 = new NamedParamStatement(dbConnection, STATEMENT2);
        nps2.setString("customer_name", "Boba Fett");
        nps2.setInt("store_id", 101);
        ResultSet rs2 = nps2.executeQuery();
        try {
            NamedParamStatement nps3 = new NamedParamStatement(dbConnection, STATEMENT2);
            nps3.setString("non-existent-field", "Obi-Wan Kenobi");
            ResultSet rs3 = nps3.executeQuery();
            Assert.fail();
        } catch (Exception ex) {

        }
        try {
            NamedParamStatement nps4 = new NamedParamStatement(dbConnection, STATEMENT2);
            nps2.setString("customer_name", "Boba Fett");
            ResultSet rs4 = nps4.executeQuery();
            Assert.fail();
        } catch (Exception ex) {

        }

        NamedParamStatement nps3 = new NamedParamStatement(dbConnection, STATEMENT3);
        nps3.setString("title", "title1");
        nps3.setString("description", "description1");
        nps3.setInt("release_year", 2021);
        nps3.setInt("language_id", 1);
        nps3.setInt("rental_duration", 3);
        nps3.setFloat("rental_rate", 3.99F);
        nps3.setInt("length", 180);
        nps3.setFloat("replacement_cost", 23.99F);
        nps3.setString("rating", "PG");

        Collection<String> fields = nps3.getFields();
        Assert.assertTrue(fields.contains("title"));
        Assert.assertTrue(fields.contains("description"));
        Assert.assertTrue(fields.contains("release_year"));
        Assert.assertTrue(fields.contains("language_id"));
        Assert.assertTrue(fields.contains("rental_duration"));
        Assert.assertTrue(fields.contains("rental_rate"));
        Assert.assertTrue(fields.contains("length"));
        Assert.assertTrue(fields.contains("replacement_cost"));
        Assert.assertTrue(fields.contains("rating"));
    }


    @Test
    public void testSelectIn() throws Exception {
        String sql = "SELECT * from customers where customer_id in (:foo)";
        Connection dbConnection = null;
        try {
            dbConnection = DriverManager.getConnection(DB_CONN + "/" + DB_DATABASE, DB_USER, DB_PASSWORD);
        } catch (Exception ex) {
            System.out.println("database server connection failed, short-circuiting test");
            return;
        }
        NamedParamStatement nps3 = new NamedParamStatement(dbConnection, sql);
        List<Integer> paramList = new ArrayList<>(2);
        paramList.add(1);
        paramList.add(2);
        nps3.setList("foo", paramList);
        ResultSet rs = nps3.executeQuery();
        rs.next();
        int cust_id = rs.getInt("customer_id");
        Assert.assertEquals(1, cust_id);
        rs.next();
        cust_id = rs.getInt("customer_id");
        Assert.assertEquals(2, cust_id);
    }
}
