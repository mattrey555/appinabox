package com.apptechnik.appinabox.util;

import com.apptechnik.appinabox.library.util.DateParser;
import org.junit.Test;

import java.util.Date;

public class TestDateParser {
    private static final String FORMAT_DATE_UTC_MILLIS = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    private static final String FORMAT_DATE_UTC_SECONDS = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    private static final String FORMAT_DATE_ZULU_SECONDS = "yyyy-MM-dd'T'HH:mm:ssZ";
    private static final String FORMAT_DATE_YMD = "yyyy-MM-dd";
    private static final String FORMAT_DATE_YM = "yyyy-MM";
    private static final String FORMAT_DATE_Y = "yyyy";
    private static final String FORMAT_DATE_MDY = "MM-dd-yyyy";
    private static final String FORMAT_DATE_MY = "MM-yyyy";
    private static final String FORMAT_DATE_YMD_HMS = "yyyy-MM-dd' 'HH:mm:ss";

    @Test
    public void testParse() throws Exception {
        Date date1 = DateParser.parse("2019-03-03T12:12:12.000Z");
        Date date2 = DateParser.parse("2019-03-03T12:12:12Z");
        Date date3 = DateParser.parse("2019-03-03");
        Date date4 = DateParser.parse("2019-03");
        Date date5 = DateParser.parse("03-03-2019");
        Date date6 = DateParser.parse("03-2019");
    }
}
