package com.apptechnik.appinabox.util;

import com.apptechnik.appinabox.library.util.ServletStringUtil;
import org.junit.Assert;
import org.junit.Test;

public class TestServletStringUtil {
    public static String PASSWORD = "test password with lots of text";

    @Test
    public void testArguments() {
        String s = "/a/path/to//another/dimension/";
        Assert.assertEquals("a", ServletStringUtil.getDelimElement(s, "/", 0));
        Assert.assertEquals("path", ServletStringUtil.getDelimElement(s, "/", 1));
        Assert.assertEquals("to", ServletStringUtil.getDelimElement(s, "/", 2));
        Assert.assertEquals("", ServletStringUtil.getDelimElement(s, "/", 3));
        Assert.assertEquals("another",ServletStringUtil.getDelimElement(s, "/", 4));
        Assert.assertEquals("dimension", ServletStringUtil.getDelimElement(s, "/", 5));
        Assert.assertEquals("", ServletStringUtil.getDelimElement(s, "/", 6));
        Assert.assertNull(ServletStringUtil.getDelimElement(s, "/", 7));

    }
}
