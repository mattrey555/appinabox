package com.apptechnik.appinabox.util;

import com.apptechnik.appinabox.library.util.StringUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.Map;

public class TestStringUtils {

    @Test
    public void testParseIntegerLists() {
        String s = "1, 2, 3 , 4";
        List<Integer> listInt = StringUtils.parseIntegerList(s, ",");
        Assert.assertEquals(1, listInt.get(0).intValue());
        Assert.assertEquals(2, listInt.get(1).intValue());
        Assert.assertEquals(3, listInt.get(2).intValue());
        Assert.assertEquals(4, listInt.get(3).intValue());
        List<Short> listShort = StringUtils.parseShortList(s, ",");
        Assert.assertEquals(1, listInt.get(0).shortValue());
        Assert.assertEquals(2, listInt.get(1).shortValue());
        Assert.assertEquals(3, listInt.get(2).shortValue());
        Assert.assertEquals(4, listInt.get(3).shortValue());
    }

    @Test
    public void testParseBooleanLists() {
        String s = "true, false, False , True";
        List<Boolean> listBool = StringUtils.parseBooleanList(s, ",");
        Assert.assertTrue(listBool.get(0));
        Assert.assertFalse(listBool.get(1));
        Assert.assertFalse(listBool.get(2));
        Assert.assertTrue(listBool.get(3));
    }

    @Test
    public void testParseFloatLists() {
        String s = "1, 2.01, 3.3432 , 4.61";
        List<Float> listFloat = StringUtils.parseFloatList(s, ",");
        Assert.assertEquals(1.0F, listFloat.get(0).floatValue(), 0.00001);
        Assert.assertEquals(2.01F, listFloat.get(1).floatValue(), 0.00001);
        Assert.assertEquals(3.3432F, listFloat.get(2).floatValue(), 0.00001);
        Assert.assertEquals(4.61F, listFloat.get(3).floatValue(), 0.00001);
    }

    @Test
    public void testExtractValues() {
        String varFormat = "bearer ${bearer_token}";
        String s = "bearer @#$*UEWFOWEF";
        Map<String, String> map = StringUtils.extractValues(varFormat, s);
        Assert.assertEquals("@#$*UEWFOWEF", map.get("bearer_token"));

        varFormat = "bearer ${bearer_token} and more text";
        s = "bearer @#$*UEWFOWEF and more text";
        map = StringUtils.extractValues(varFormat, s);
        Assert.assertEquals("@#$*UEWFOWEF", map.get("bearer_token"));

        varFormat = "${bearer_token}";
        s = "@#$*UEWFOWEF";
        map = StringUtils.extractValues(varFormat, s);
        Assert.assertEquals("@#$*UEWFOWEF", map.get("bearer_token"));

    }

    @Test
    public void testExtractValue() {
        String varFormat = "bearer ${bearer_token}";
        String s = "bearer @#$*UEWFOWEF";
        String value=  StringUtils.extractValue(varFormat, "bearer_token", s);
        Assert.assertEquals("@#$*UEWFOWEF", value);

        varFormat = "bearer ${bearer_token} and more text";
        s = "bearer @#$*UEWFOWEF and more text";
        value = StringUtils.extractValue(varFormat, "bearer_token", s);
        Assert.assertEquals("@#$*UEWFOWEF", value);

        varFormat = "${bearer_token}";
        s = "@#$*UEWFOWEF";
        value = StringUtils.extractValue(varFormat, "bearer_token", s);
        Assert.assertEquals("@#$*UEWFOWEF", value);

        varFormat = "${bearer_token} ${bearer_value}";
        s = "value1 value2";
        value =  StringUtils.extractValue(varFormat, "bearer_token", s);
        Assert.assertEquals("value1", value);
        value =  StringUtils.extractValue(varFormat, "bearer_value", s);
        Assert.assertEquals("value2", value);
    }
}
