# Yet other Relational-Object-Model "AppInABox"
Most Object-Relational Models are driven by code, and generate the SQL tables and indices to match, but fitting these systems to SQL has some difficulties:

* If the SQL schema exists, the code has to match, including table and query structure and types.
* Systems depend on the JDBC Named Parameter statement which has syntactic and semantic limitations
* Type information in the SQL expressions is "lost in translation" when converting to the target language.
* The ORM does not map complex SQL expressions to resulting hierarchical data types.
* SQL sequences of statements may not be supported.

AppInABox infers the SQL types from queries, updates, and the database schema to generate code.  
The generated code maps result sets from SELECT statements to a Java object hierarchy, and conversely 
maps a Java object hierarchy to INSERT, UPDATE, and DELETE statements.  The system takes a JSON mapping 
which is applied to SQL ResultSets, and as parameters for INSERT, UPDATE, and DELETE statements.  
Our goal is to create a servlet generator which works from existing SQL databases with a minimal 
specification. AppInABox generates Java code to work with PostGreSQL and Apache/Tomcat.  
Support for other databases, code generation, and Web Servers will be implemented in the future.

## Dependencies
AppInABox has minimal dependencies

* AppInABox depends on Java 1.8.0
* PostgreSQL is the only database supported (https://www.postgresql.org/)
* The .war is J2EE compliant, but the gradle deployment scripts only work with Apache/Tomcat
* JSQLParser (https://github.com/JSQLParser)
* GSON is used for JSON->POJO transformation.  
 
AppInABox depends on Java, PostGreSQL, and Apache.  

AppInABox uses (JSQLParser) https://github.com/JSQLParser extensively, and this project would not have been successful without their efforts.
There are scripts to download PostGres and Apache appropriate for your MacOSX and Linux in the root directory, ex. install_postgres_debian.sh


AppInABox parses the schema from a file containing CREATE TABLE, ALTER TABLE, CREATE INDEX, and CREATE VIEW statements.
The queries are parsed from a .sql file, similar to the following example:

```
/* url: /storeinventory2&the_store_id=${the_store_id} */
/* json-response: { the_store_id, city, address, district, inventory : { inventory_id, film.title, film.release_year }} */
/* name-response: select_inventory */
/* package: com.mycompany.dvdrental.store_inventory2 */
SELECT store.store_id as the_store_id, city.city, address.address, address.district, inventory.inventory_id, film.title, film.release_year 
FROM store 
INNER JOIN address ON store.address_id = address.address_id 
INNER JOIN city ON address.city_id = city.city_id 
INNER JOIN inventory ON store.store_id = inventory.store_id 
INNER JOIN film on inventory.film_id = film.film_id;
WHERE the_store_id = :the_store_id
```

Multiple statements can be used in the same file, generating a transaction which combines UPDATE, INSERT, DELETE and SELECT statements.
Directives are given in `/* */` comments.

* url: database URL
* database: database to use in generated code
* password: password for the database
* sql_driver: Driver for the SQL database

```
This generates the following files:
├── query						<-- POJOs and SQL method classes for SELECT statements
│   ├── select_inventory				
│   │   ├── inventory.java				<-- POJOs
│   │   ├── select_inventory.java
│   │   └── sql
│   │       ├── PrimaryKeySourceColumns.java		<-- class to compare primary keys in the ResultSet
│   │       ├── inventorySQLFns.java			<-- SQL method classes
│   │       └── select_inventorySQLFns.java
│   └── sql
│       └── store_inventory2Main.java			<-- program takes query parameters from command-line and files
└── servlet
    └── store_inventory2Servlet.java			<-- Servlet

```
## Setup
Create two files, a SQL file with the CREATE TABLES, CREATE VIEWS and other SQL statements for your schema, and a _.properties_ file used for code generation.  The properties file contains the database parameters and versions for libraries used in code generation:

For example:

```
url:jdbc:postgresql://localhost
database: bugs
user: postgres
password: FiatX1/9
sql_driver: org.postgresql.Driver
JAVA_VERSION: 1.8
GRADLE_VERSION: 6.7.1
LIBRARY_VERSION: 0.1
JUNIT_VERSION: 4.12
ENGINE_VERSION: 0.1
POSTGRES_VERSION: 42.2.6
GSON_VERSION: 2.8.6
SERVLET_API_VERSION: 4.0.1
LOG4J_VERSION: 2.12.0
```

The library versions (in ALL CAPS) override the defaults in the build.gradle template file, and are optional.

Required Parameters:

* url: database URL
* database: database to use in generated code
* password: password for the database
* sql_driver: Driver for the SQL database
* gradle: The system was developed with gradle 7.0. The earliest system built against was 6.7.1

## Build

_NOTE_: on Debian, the normal "sudo apt-get upgrade gradle" didn't work, so do the following to get 7.0:
```
sudo add-apt-repository ppa:cwchien/gradle
sudo apt-get update
sudo apt upgrade gradle
```
To build on Linux or MacOS (Windows not tested yet)
```
gradle jar
gradle publishToMavenLocal
```

## Code Generation Setup

To generate your gradle build files for code generation:

```
gradle makeGradleFile -Pdir=<project-directory> -Psqldir=<SQL-files-directory> -PcreateTables=<SQL-schema-file> -PpropFile=<properties-file>
```

* _dir_: root directory of your project
* _sqldir_: subdirectory containing SQL files, with 4 subdirectories: GET, POST, PUT, DELETE, which specify the HTTP method for each servlet
* _SQL-schema-file_: file in root directory containing CREATE TABLE, ALTER, CREATE INDEX, and CREATE VIEW statements for your SQL schema
* _properties-file_: properties file in root directory described above

Example:
```
gradle makeGradleFile -Pdir=test/data/bugs -Psqldir=sql -PcreateTables=bugs_create_tables.sql -PpropFile=bugs.properties
```

This generates the gradle files for your project.

Once you've generated the gradle files, you can generate code from your input SQL files. To generate a .jar file which you can execute from the command line:

```
gradle jar
```

Then copy the .jar files from maven to your local libs directory:

```
gradle copyRuntimeLibs
```

To execute the generated code from the command line:

```
CLASSPATH=libs/Library-1.0.jar:libs/gson-2.8.6.jar:libs/postgresql-42.2.6.jar:libs/javax.servlet-api-4.0.1.jar:build/classes/java/main
java com.mycompany.dvdrental.select_post_in_customers_film_name.query.sql.store_inventory2Main dvdrental.properties 
```

## Syntax
AppInABox uses the following directives in comments for code generation:

Required:

* _request-json/response-json_: format of the request and response JSON.
* _name-request/name-response_: name of request and response (name of top-level POJO).
* _url_: URL of the request.
* _package_: Java package of generated code.

Optional:

* _name_: suffixed to _package_ for generated code (default is filename without extension).
* _type_: fixed SQL type to apply to the named parameter, used when type cannot be inferred.  
_WARNING_: it will override inferred types.
* _optional_: specifies an optional parameter.
* _header_: specifies a parameter from the HTTP header, rather than the URL query.
* _method_: specifies the HTTP method (GET/POST/PUT/DELETE) to use for the generated servlet.


Other than the SQL statements, directives are specified in \/* \*/ style comments: 
* _JSON_: The JSON mapper represents a named hierarchy used to transform the ResultSet from the SQL 
statement.  A field can be denoted by:

SELECT:

* _column_: matches to a column names which are unique to the top-level of a SQL statement.
* _table_._column_: matches a column with a table prefix.
* _table_.*: matches all the columns in the specified table.
* *: wildcard to match all columns.
 
INSERT/UPDATE/DELETE:

* _field_: A JDBC Named Parameter
* _prefix_._field_: JDBC Named Parameter of the format _prefix_._field_

Example:
```
json-response: { the_store_id, city, address, district, inventory : { inventory_id, film.title, film.release_year }}
```
This creates a JSON mapping with the following structure:
```
{ "the_store_id" : <store_id>,
  "city" : <city>,
  "address" : <address>,
  "district" : <district>,
  "inventory": [
    {
      "inventory_id": <inventory>,
      "title": <title>,
      "release_year": <release-year>
    },
...
}
```
_SQL_: The SQL format is the same as normal, with :name used for JDBC Parameters,
with 2 exceptions:

* "dot-prefix" variables are allowed to address naming collision issues, where a JSON name is unique
in its hierarchy, but may not be unique in the SQL statement.  "dot-prefix" matches to any path of nodes 
in the JSON hierarchy, so it does not need to be specified from the root.
* NamedParameters in SELECT IN clauses ("SELECT * FROM T where a in (:a)") are treated
as lists of boxed primitives (List<Integer>, List<String>), if the parameter occurs in the 
request JSON, otherwise if it is in the query or header, it's treated as a scalar value.

Example:
```
SELECT store.store_id as the_store_id, city.city, address.address, address.district, inventory.inventory_id, film.title, film.release_year 
FROM store 
INNER JOIN address ON store.address_id = address.address_id 
INNER JOIN city ON address.city_id = city.city_id 
INNER JOIN inventory ON store.store_id = inventory.store_id 
INNER JOIN film on inventory.film_id = film.film_id;
WHERE the_store_id = :the_store_id;
```
Generates the following output:
```
[
  {
    "the_store_id": 1,
    "city": "Lethbridge",
    "address": "47 MySakila Drive",
    "district": "Alberta",
    "inventory": [
      {
        "inventory_id": 1,
        "title": "Academy Dinosaur",
        "release_year": 2006
      }, ...
}
```
The SQL statement is modified in the actual query, with the JSON defined above:
```
SELECT store.store_id AS the_store_id, 
       city.city, 
       address.address, 
       address.district, 
       inventory.inventory_id, 
       film.title, 
       film.release_year, 
       city.city_id AS city_city_id, 
       address.address_id AS address_address_id, 
       store.store_id AS store_store_id, 
       inventory.inventory_id AS inventory_inventory_id, 
       film.film_id AS film_film_id 
FROM store INNER JOIN address ON store.address_id = address.address_id 
INNER JOIN city ON address.city_id = city.city_id 
INNER JOIN inventory ON store.store_id = inventory.store_id 
INNER JOIN film ON inventory.film_id = film.film_id 
WHERE the_store_id = :the_store_id
ORDER BY city_city_id, address_address_id, store_store_id, inventory_inventory_id, film_film_id
```
To add the primary keys for each table referenced in the SELECT item list.


_URL_: The query specifies parameters with param=${variable} syntax, for example:
```
/* url: /storeinventory2&the_store_id=${the_store_id} */
```
_the_store_id_ maps to :the_store_id in the SQL query.


## How it works
For queries, SQL joins multiple tables into a single ResultSet, with either NULL values or 
duplicates.  This is converted to JSON by "folding" duplicate values based on the primary key
of each table, or a 1:1 matching set of keys in a JSON node.

The code generator creates PrimaryKeySourceColumns.java, which looks like this:
```
public class PrimaryKeySourceColumns{
    public int film_film_id;
    public int film_film_idPrevious;
    public boolean film_film_idChanged = true;

    public void comparePrimaryKeys(ResultSet rs) throws SQLException {
        film_film_id = rs.getInt("film_film_id");
        film_film_idChanged = !rs.wasNull() && film_film_id != film_film_idPrevious;
        film_film_idPrevious = film_film_id;
    }

    public boolean getFilm_film_idChanged() {
        return film_film_idChanged;
    }
}
```
As each row is read, it compares the primary keys for each table to the previous row. 
As the rowset is mapped to the JSON, add elements to each node's list of values if the flag is true.
The primary key is guaranteed to be unique for each referenced table, but columns specified
in the ORDER BY clause can be used as comparison keys.  If any column in a JSON node is
specified in the ORDER BY clause, _all_ of the keys in that NODE must be in the ORDER BY clause.
While primary keys are added automatically, currently unspecified ORDER BY columns in a JSON
node cause an error.

For the following example:
```
/* url: /storeinventory2?store_id=${store_id} */
/* json-response: { the_store_id, city, address, district, inventory : { inventory_id, film.title, film.release_year }} */
/* name-response: select_inventory */
/* package: com.appinabox.dvdrental.store_inventory2 */
SELECT store.store_id as the_store_id, 
        city.city, 
        address.address, 
        address.district, 
        inventory.inventory_id, 
        film.title, 
        film.release_year 
FROM store 
INNER JOIN address ON store.address_id = address.address_id 
INNER JOIN city ON address.city_id = city.city_id 
INNER JOIN inventory ON store.store_id = inventory.store_id 
INNER JOIN film on inventory.film_id = film.film_id
WHERE store.store_id=:store_id
```
The SQL query returns:
```
 the_store_id |    city    |      address      | district | inventory_id |            title            | release_year 
--------------+------------+-------------------+----------+--------------+-----------------------------+--------------
            1 | Lethbridge | 47 MySakila Drive | Alberta  |            1 | Academy Dinosaur            |         2006
            1 | Lethbridge | 47 MySakila Drive | Alberta  |            2 | Academy Dinosaur            |         2006
            1 | Lethbridge | 47 MySakila Drive | Alberta  |            3 | Academy Dinosaur            |         2006
            1 | Lethbridge | 47 MySakila Drive | Alberta  |            4 | Academy Dinosaur            |         2006
            1 | Lethbridge | 47 MySakila Drive | Alberta  |           16 | Affair Prejudice            |         2006
            1 | Lethbridge | 47 MySakila Drive | Alberta  |           17 | Affair Prejudice            |         2006
            1 | Lethbridge | 47 MySakila Drive | Alberta  |           18 | Affair Prejudice            |         2006
            1 | Lethbridge | 47 MySakila Drive | Alberta  |           19 | Affair Prejudice            |         2006
```
The columns _the_store_id_, _city_, _address_, and _district_ are repeated for each inventory item,
and thus have the same primary key.  The engine _modifies_ the SQL query into:
```
SELECT store.store_id AS the_store_id, 
        city.city, 
        address.address, 
        address.district, 
        inventory.inventory_id, 
        film.title, 
        film.release_year, 
        city.city_id AS city_city_id, 
        address.address_id AS address_address_id, 
        store.store_id AS store_store_id, 
        inventory.inventory_id AS inventory_inventory_id, 
        film.film_id AS film_film_id 
FROM store INNER JOIN address ON store.address_id = address.address_id 
INNER JOIN city ON address.city_id = city.city_id 
INNER JOIN inventory ON store.store_id = inventory.store_id 
INNER JOIN film ON inventory.film_id = film.film_id 
ORDER BY city_city_id, 
        address_address_id, 
        store_store_id, 
        inventory_inventory_id, 
        film_film_id
```
The engine adds the primary keys for each table (_city_, _address_, _store_, _inventory_, and film_) 
to the ORDER BY clause and the SELECT item list, aliased to prevent collisions.  These keys are 
compared by the PrimaryKeySourceColumns class and flagged when they change between rows.  If the primary
key for a JSON node changes, a new node is added to the list (or sublist), generating a JSON result:
```
[
  {
    "the_store_id": 1,
    "city": "Lethbridge",
    "address": "47 MySakila Drive",
    "district": "Alberta",
    "inventory": [
      {
        "inventory_id": 1,
        "title": "Academy Dinosaur",
        "release_year": 2006
      },
      {
        "inventory_id": 2,
        "title": "Academy Dinosaur",
        "release_year": 2006
      },

```

When the JSON is mapped, the primary keys are added to the ORDER BY clause if they are
not already present. 

INSERT/UPDATE/DELETE:  Is the inverse of query, where tags from the incoming JSON are 
mapped to NamedParameters in the statement.  Traverse the incoming JSON, and populate 
the SQL statements.  Execute the SQL statement when it's fully populated. 

Example:
```
/* url: /insert/insert_film */
/* json-request: { title, description, release_year, language_id, rental_duration, rental_rate, length, replacement_cost, rating, actor_film : { actor_id }} */
/* name-request: insert_film */
/* name: insert_film */
/* package: com.appinabox.dvdrental.insert_film */
/* type: rating varchar */
INSERT INTO film (title, description, release_year, language_id, rental_duration, rental_rate, length, replacement_cost, rating) 
VALUES (:title, :description, :release_year, :language_id, :rental_duration, :rental_rate, :length, :replacement_cost, CAST(:rating AS mpaa_rating)) 
RETURNING film_id;
INSERT INTO film_actor (film_id, actor_id) VALUES (:film_id, :actor_id);
```
This takes a request JSON of the form:
```
[{ "title" : "Star Wars",
   "description" : "A long time ago in a Galaxy far, far away",
   "release_year" : 1978,
   "language_id" :  1,
   "rental_duration" : 3,
   "rental_rate" : 2.99,
   "length" : 182, 
   "replacement_cost" : 19.99,
   "rating" : "PG",
   "lastUpdate": "2013-05-26 14:50:58.951",
   "specialFeatures": "Director's commentary",
   "test_fulltext": "Fulltext test values",
   "actor_film" : [{ "actor_id" : 1 }, { "actor_id" : 2 }, { "actor_id" : 3 }, { "actor_id" : 4 }, { "actor_id" : 5 }]
}]
```
And inserts the film into the film table, and the associated actor_ids into the actor_film table.
Note that the RETURNING clause (which is Postgres-specific) is allowed.  Values returned from
preceding UPDATE/INSERT/DELETE statements can be used as values in successive statements.
_returning these values in the output JSON is not currently supported, but will be implemented shortly_

### Restrictions
* Scalar values in a JSON node _must_ come from the same table or subselect.
* To override primary key ordering, you can use columns in the ORDER BY statement, but
_all_ of the fields in the JSON node _must_ occur in the ORDER BY list.
* Results from subselects are considered unique, even if they are from a JOIN, causing 
duplicates in the resulting JSON.
* JSON Requests into UPDATE/INSERT/DELETE statements can only share NamedParameters between a node 
and its ancestor.
* Expressions in the SELECT item list must be aliased
* Parameters in CAST expressions must have their types specified on the command line.
* SQL UNION statements are not supported

### Behaviors
JSON nodes with a single element and no children map to arrays of primitives, so:
```
/* json-response { name, description, ids : { thing_id }} */
/* name: purchases */
SELECT name, description, thing_id from names INNER JOIN things on names.id = things.id
```
maps to:

```
class purchases { 
    String name;
    String description;
    List<Integer> ids
}
```
If all of the node's elements are in the same table as the node's parent, 
then the field is mapped to a single value, instead of a list:
```
/* json-response { name, description, address : { street, city, zip, state }} */
```
maps to:
```
class purchases { 
     String name;
     String description;
     address address;
}

class address {
    String street;
    String city;
    String zip;
    String state;
}
```
If the address fields are in the same table.  If any address field is in a different table
then it maps to:
The URL query parameters, and header parameters specified in the .SQL file are mapped to 
NamedParameters in the SQL statement (with dot-syntax allowed)
```
class purchases { 
     String name;
     String description;
     List<address> address;
}
```
With multiple inserts/updates/deletes, the input data is mapped to the JDBC Named Parameters for 
each statement, which controls how the statements are nested.  For example:
```
/* name-request: insert_film */
/* json-request: { title, description, release_year, language_id, rental_duration, rental_rate, length, replacement_cost, rating, actor_film : { actor_id }} */
INSERT INTO film (title, description, release_year, language_id, rental_duration, rental_rate, length, replacement_cost, rating) VALUES (:title, :description, :release_year, :language_id, :rental_duration, :rental_rate, :length, :replacement_cost, CAST(:rating AS mpaa_rating)) RETURNING film_id;
INSERT INTO film_actor (film_id, actor_id) VALUES (:film_id, :actor_id);
```
maps to:
```
class film {
    short rental_duration;
    double rental_rate;
    short release_year;
    short length;
    double replacement_cost;
    String rating;
    String description;
    short language_id;
    String title;
    int film_id;
    List<inventory> inventory;
}
class inventory {
    private short store_id;
    private short film_id;
    private int inventory_id;
}
```
AppInABox generates code which iterates over each film and inserts it, with a nested
iteration to insert inventory.

Multiple SELECT statements are not nested in the input. Instead, a top-level object is generated with 
a list for each select call.  
```
/* json-response: { actor : { actor.first_name, actor.last_name }, customer : { customer.first_name, customer.last_name }} */
select first_name, last_name from actor limit 10;
select first_name, last_name from customer limit 10;

```
maps to:
```
class select_multiple {
    List<actor> actor;
    List<customer> customer;
}

class actor {
    String first_name;
    String last_name;
}

class customer {
    private String first_name;
    private String last_name;
}

```
For nested results, use a JOIN in the SELECT call, mapped to nested JSON:
```
/* json-response: { store.store_id, city.city, address.address, address.district, staff : { first_name, last_name } , rental : { inventory.inventory_id, film.title, film.release_year, return_date }} */
SELECT store.store_id, city.city, address.address, address.district, inventory.inventory_id, film.title, film.release_year, first_name, last_name, rental.return_date 
FROM store 
INNER JOIN address ON store.address_id = address.address_id 
INNER JOIN city ON address.city_id = city.city_id 
INNER JOIN inventory ON store.store_id = inventory.store_id 
INNER JOIN film on inventory.film_id = film.film_id
INNER JOIN staff ON store.store_id = staff.store_id
INNER JOIN rental ON rental.inventory_id = inventory.inventory_id;
```
Generates nested objects for store, staff, and rental from the SELECT statement.

SQL IN clause expressions are mapped to list types, so the following expression:
```
/* json-request: { ids } */
SELECT * from film WHERE film.id IN (:ids)
```
creates an incoming payload which is a list of boxed integers.  If the parameter is specified as a
URL query or HTTP header parameter, then it is assumed to be a comma-separated list of values 
(with quotes for strings)


## Library

_NamedParameterStatement_ has been extended so it can take lists of boxed primitives 
for the SELECT statement IN clause.  

```
/* json-request: { customer_id } */
SELECT customer.id, customer.name, FROM customer WHERE id IN (:id)
```
Will cause _id_ to map to List\<Integer\> in the request, not int.  SQL parameters which 
map to lists are extracted from the incoming JSON, rather than query or header parameters, so 
if you have a SELECT statement with an IN clause, it expects _json-request_ to be specified.

### Workarounds/Issues
Columns in SQL statements can use table names, but SQL ResultSets must use unique identifiers,
so we map _table.column_ to "table_column" in the resulting JSON, even if _column_ is unique

## Examples
The sample project includes the schema for the dvdrental database and files which 
exercise AppInABox features.

```
/* url: /staff/set-active?active=${active} */
/* json-request: { staff_id } */
/* name-request: update_staff */
/* json-response: { staff_id, active } */
/* name-response: staff_active */
/* package: com.appinabox.dvdrental.update_staff */
UPDATE staff SET active=:active WHERE staff_id=:staff_id;
SELECT staff_id, active FROM staff WHERE staff_id IN (:staff_id);
```
The named parameter ${_active_} is passed as a query parameter (_true/false_).  The JSON request is an array of
staff_ids _[1, 2]_. The resulting code updates the active flag for each _staff_id_, then returns the
result of selecting _staff_id_ and _active_ for the list of _staff_ids_.

To call the generated program from the command-line:
```
java -cp $CLASSPATH com.appinabox.dvdrental.update_staff.query.sql.update_staffMain dvdrental.properties update_staff.json true
```
which modifies the staff table as follows:
```
staff_id | active 
----------+--------
        1 | t
        2 | t
```

and returns:
```
[
  {
    "staff_id": 1,
    "active": true
  },
  {
    "staff_id": 2,
    "active": true
  }
]
```
### Servlet Generation
AppInABox also generates servlets which can be used with Apache/Tomcat. Each .sql file is placed
in a subdirectory named _get_, _post_, _delete_, or _put_, to generate their HTTP methods.

To generate the servlet, you must be running Apache/Tomcat, and have the environment variable 
_CATALINA_BASE_ defined.  To deploy the servlets, type:
```
gradle deploy
```
which will generate the .WAR file and copy it to _${CATALINA_BASE}/webapps/_.  Note that the web.xml 
file is generated with _transport-guarantee=CONFIDENTIAL_ for all servlets, so you have to 
configure Tomcat to use SSL, and connect on port 8443:
```
https://localhost:8443/dvdrental/actorlastname?last_name=Cage
```
which returns:
```
[{"first_name":"Zero","last_name":"Cage"},{"first_name":"Johnny","last_name":"Cage"}]
```

Then to make the query for 
### DVDRental database

There are gradle commands in Engine to download and import the _DVDRental_ example database
for postgres.  They assume that the user _postgres_ exists, and that either the 
_/etc/postgresql/<version>/main/pg_hba.conf_ file has been edited for an empty password 
for the _postgres_ user, or that the _PG_PASSWORD_ environment variable has been set.
To import the DVDRental database:
```
gradle createDvdRental
```
Create the gradle files for dvdrental queries in the _test/data/dvdrental_ directory:
```
gradle dvdRentalObjectGradle
```
Generate the .jar files
```
cd Engine/test/data/dvdrental
gradle jar
```
deploy to your local Tomcat server:
```
gradle deploy
```
## Servlet Extension
AppInABox generates a class _RequestResponse_, which contains fields for the parameters,
request and response objects, and URL query/header parameters. For example:
```
public class RequestResponse {
    private List<insert_film> input;
    private List<Integer> output;
    private int film_id;
    ... constructors and accessors ...
}
```
_RequestResponse_ is constructed at the start of the servlet call. 
Servlets can be extended using the following functions:
```
    boolean beforeParse(HttpServletRequest request, HttpServletResponse response)
```
Called before the incoming URL query/header parameters and JSON payload are parsed. Return true
to continue processing, false to exit the servlet. Useful for setting defaults, validating 
input data, or parsing values not used in SQL.

```
    boolean beforeUpdate(RequestResponse requestResponse)
```
Called before any INSERT/UPDATE/DELETE calls.  The URL query/headers and JSON payload
are parsed at this point. Return true to continue processing, false to exit the servlet.
Useful for massaging/validating data before input.
```
    boolean beforeQuery(RequestResponse requestResponse)
```
Called before any SELECT calls. Return true to continue processing, false to exit the servlet.

```
    boolean beforeResult(HttpServletResponse response, RequestResponse requestResponse) 
```
Called before the result is returned.  The result of any SELECT calls are populated in the
_RequestResponse_ object.




