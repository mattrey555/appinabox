DROP TRIGGER film_fulltext_trigger ON film;
ALTER TABLE film RENAME fulltext TO test_fulltext;
CREATE TRIGGER film_fulltext_trigger BEFORE INSERT OR UPDATE ON film FOR EACH ROW EXECUTE FUNCTION tsvector_update_trigger('test_fulltext', 'pg_catalog.english', 'title', 'description');
