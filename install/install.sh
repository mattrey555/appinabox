#!/bin/sh
# install apache
TOMCAT_VERSION=9.0.24
if [ -d $HOME/apache ]; then 
	echo "apache already installed" 
else
	mkdir $HOME/apache
	cd $HOME/apache
	curl http://apache.spinellicreations.com/tomcat/tomcat-9/v$TOMCAT_VERSION/bin/apache-tomcat-$TOMCAT_VERSION.tar.gz --output apache-tomcat.tar.gz
	gunzip apache-tomcat.tar.gz
	tar -xvf apache-tomcat.tar
fi
cd $HOME/apache/apache-tomcat-$TOMCAT_VERSION/bin
sh startup.sh
open -a "Google Chrome" http://localhost:8080
#curl =https://sbp.enterprisedb.com/getfile.jsp?fileid=11945&_ga=2.41142626.1349799534.1566798258-1662500037.1566798258 
